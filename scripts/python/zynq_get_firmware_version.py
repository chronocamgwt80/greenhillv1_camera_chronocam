#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.get_firmware_version()
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()