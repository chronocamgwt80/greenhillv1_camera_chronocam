#! /bin/bash

#This script launch the c code ./gwt_size_symbol to determine the
#number of bits per symbol and then run the python script to configure
#the zynq

pyt_fil="zynq_gwt_configuration.py"

path=$(pwd)
cd $path
echo "parameters : $@"
echo
./gwt_size_symbol "$@" #1&>/dev/null
ret=$?

cmd="$pyt_fil $@ -s $ret"
#echo "cmd=$cmd"
echo "$pyt_fil $@ -s $ret"
python $(echo $cmd)
#python zynq_gwt_parse_options.py "$@ -s $ret"
