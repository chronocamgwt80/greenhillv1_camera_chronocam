from card import *
from zynq_def import *
from csv_access import *
import time
import re 
import struct
from iqSampleAnalyzer import hexIQSample2Dec, checkIQsample

class Zynq(Card, csv_access):
    
    def __init__(self, protocol):
        self.baseAddr = ZYNQ_CMD_BASEADDR
        Card.__init__(self, self.baseAddr, protocol)
        csv_access.__init__(self)
        self.modulation_ready = 0
        self.modulated_data_len = 0
    
    def reset(self):
        self.rf_tx_reset()
        self.rf_rx_reset()
        
    # Control GW user led
    def gw_power_user_leds(self,config_gw_power_user_leds):
        print "Zynq> control gw power user led..."
        print "la config pour les leds en decimal vaut : " 
        print config_gw_power_user_leds
        Card.write(self,ZYNQ_CMD_GW_POWER_USER_LEDS,config_gw_power_user_leds )
    
    # turn on user led
    def turn_on_user_led(self):
        print "Zynq> turn on Zynq user led..."
        Card.write(self,ZYNQ_CMD_USER_LED, ZYNQ_USER_LED_ON)
        
    # turn off user led
    def turn_off_user_led(self):
        print "Zynq> turn off Zynq user led..."
        Card.write(self,ZYNQ_CMD_USER_LED, ZYNQ_USER_LED_OFF)
        
    # start RF transmission with IQ samples stored in pattern.h (see csvToC python script)
    def start_rf_tx_self_test(self):
        print "Zynq> start RF transmission with IQ samples stored in pattern.h ..."
        Card.write(self,ZYNQ_CMD_RF_TX_START_SELF_TEST, 0)
    
    # start RF transmission with IQ samples send first by ZYNQ_CMD_RF_TX_FILL_BUFFER
    def start_rf_transmission(self):
        print "Zynq> start RF transmission with samples stored in RF TX buffer..."
        Card.write(self,ZYNQ_CMD_START_RF_TX_TRANSMISSION, 0)
                
    # stop current RF transmission 
    def stop_rf_transmission(self):
        print "Zynq> stop current RF transmission..."
        Card.write(self,ZYNQ_CMD_STOP_RF_TX_TRANSMISSION, 0)
        
    # get Zynq firmware version
    def get_firmware_version(self):
        data = Card.read(self,ZYNQ_CMD_GET_HARDWARE_VERSION, ZYNQ_HARDWARE_FIRMWARE_NBBYTES, 0)
        hw_version = data['val']
        print "Zynq> HWv: " + hex(hw_version)   
        data = Card.read(self,ZYNQ_CMD_GET_SOFTWARE_VERSION, ZYNQ_SOFTWARE_FIRMWARE_NBBYTES, 0)
        sw_version = data['val']
        print "Zynq> SWv: " + hex(sw_version)
        return  hw_version, sw_version
        
    # reset all RF TX elements (RF TX FIFO + SERIALIZER + ARM RF TX tables)
    def rf_tx_reset(self):
        print "Zynq> reset all RF TX elements..."
        Card.write(self,ZYNQ_CMD_RF_TX_RESET, 0)
        
    # set RF TX mode as loop (each sample read from FIFO is rewritten into the FIFO) or burst mode (each sample read from FIFO is sent once only)
    # mode -> '0' = burst, '1' = loop
    def rf_tx_set_loop_burst_mode(self, mode):
        print "Zynq> set RF TX mode as loop (each sample read from FIFO is rewritten into the FIFO) or burst mode (each sample read from FIFO is sent once only)..."
        Card.write(self,ZYNQ_CMD_RF_TX_LOOP_BURST_MODE, mode)
        
    # send to Zynq, word to modulate
    def rf_tx_prepare_data_to_modulate(self, csv_pattern_file_name):
        print "Zynq> send to Zynq word to modulate..."
        buffer = ""
        buffer = csv_access.get_sample(self, csv_pattern_file_name)
        print str(len(buffer)/4) + " words was sent to Zynq"
        Card.write_buf(self,ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE, buffer, len(buffer))
        
    # read data which are stored in Zynq and ready to modulate
    def rf_tx_read_data_to_modulate(self, nb_word, csv_pattern_file_name):
        time.sleep(1) # required to sequence it with previous commands 
        nbbytes = nb_word*4
        print "Zynq> ask to Zynq to read data which are stored in Zynq and ready to modulate..."
        print "Zynq> you want to read " + str(nbbytes/4) + " data (signed integers)" + " or " + str(nbbytes) + " bytes"
        list = []
        # get data from zynq
        t = Card.read(self,ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE, nbbytes, 0)
        # bytes to signed int
        for index in range (0, (len(t['buf']) - FRAME_OVERHEAD_SIZE), +4):
            data = t['buf'][index + FRAME_HEADER_SIZE + 0] + (t['buf'][index + FRAME_HEADER_SIZE + 1] << 8)  + (t['buf'][index + FRAME_HEADER_SIZE + 2] << 16)  + (t['buf'][index + FRAME_HEADER_SIZE + 3] << 24)
            if (data > ((2**32)/2)):
                data -= 2**32
            list.append(data)
        # write data to csv file
        csv_access.write_sample(self, csv_pattern_file_name, list, 1)
    
    # start modulation with data stored in Zynq (modulation_debug_mode = 1 -> GWT algorithm enable, modulation_debug_mode = 0 -> GWT algorithm is by pass)  
    def rf_tx_start_modulation(self, modulation_debug_mode):
        print "Zynq> start modulation with data stored in Zynq..."
        Card.write(self,ZYNQ_CMD_RF_TX_START_MODULATION, modulation_debug_mode)
        raw_input("Check you received from zynq UART: MODULATION IS OVER then Press Enter to continue...")
        print "key Enter detected"
    
    # read data modulated by the GWT algorithm
    def rf_tx_read_modulated_data(self, nb_word, csv_pattern_file_name):
        time.sleep(1) # required to sequence it with previous commands
        nbbytes = nb_word*4
        print "Zynq> ask to Zynq to read data modulated by the GWT algorithm..."
        list = []
        data = Card.read(self,ZYNQ_CMD_RF_TX_START_MODULATION, nbbytes, 0)
        # bytes to signed int16_t
        for index in range (0, (len(data['buf']) - FRAME_OVERHEAD_SIZE), +8):
            i = data['buf'][index + FRAME_HEADER_SIZE + 0] + (data['buf'][index + FRAME_HEADER_SIZE + 1] << 8) + (data['buf'][index + FRAME_HEADER_SIZE + 2] << 16) + (data['buf'][index + FRAME_HEADER_SIZE + 3] << 24)
            q = data['buf'][index + FRAME_HEADER_SIZE + 4] + (data['buf'][index + FRAME_HEADER_SIZE + 5] << 8) + (data['buf'][index + FRAME_HEADER_SIZE + 6] << 16) + (data['buf'][index + FRAME_HEADER_SIZE + 7] << 24)
            if i == 0:
                i_float = 0.0
            else:                    
                i_hex = hex(i)
                i_hex = re.sub('0x', '', i_hex)
                try:
                    i_float = struct.unpack('!f', str(i_hex).decode('hex'))[0]
                except:
                    i_float = 0.0  
            if q == 0:
                q_float = 0.0
            else:                    
                q_hex = hex(q)
                q_hex = re.sub('0x', '', q_hex)
                try:
                    q_float = struct.unpack('!f', str(q_hex).decode('hex'))[0]
                except:
                    i_float = 0.0  
            
#             if (i > ((2**16)/2)):
#                 i -= 2**16
#             if (q > ((2**16)/2)):
#                 q -= 2**16
            list.append(i_float)
            list.append(q_float)
        csv_access.write_sample(self, csv_pattern_file_name, list, 2)
    
    # get modulated data length
    def rf_tx_get_modulated_data_length(self):
        print "Zynq> get RF TX modulated data length..."
        data = Card.read(self,ZYNQ_CMD_RF_TX_MODULATED_DATA_LENGTH, 4, 0)
        buffer_index = data['buf'][FRAME_HEADER_SIZE + 0] + (data['buf'][FRAME_HEADER_SIZE + 1] << 8) + (data['buf'][FRAME_HEADER_SIZE + 2] << 16) + (data['buf'][FRAME_HEADER_SIZE + 3] << 24)
        print "Zynq> GWT algorithm generated word number:" + str(buffer_index)
        return buffer_index
    
    # get RF TX buffer index
    def rf_tx_get_buffer_index(self):
        print "Zynq> get RF TX buffer index..."
        data = Card.read(self,ZYNQ_CMD_RF_TX_GET_BUFFER_INDEX, 4, 0)
        buffer_index = data['buf'][FRAME_HEADER_SIZE + 0] + (data['buf'][FRAME_HEADER_SIZE + 1] << 8) + (data['buf'][FRAME_HEADER_SIZE + 2] << 16) + (data['buf'][FRAME_HEADER_SIZE + 3] << 24)
        print "Zynq> Words number in RF TX BUFFER:" + str(buffer_index)
        return buffer_index
    
    #def get modulation_ready flag
    def rf_tx_get_modulation_ready(self):
        data = Card.read(self,ZYNQ_CMD_GET_MODULATION_READY_FLAG, 8, 0)
        self.modulation_ready = data['buf'][FRAME_HEADER_SIZE + 0] + (data['buf'][FRAME_HEADER_SIZE + 1] << 8) + (data['buf'][FRAME_HEADER_SIZE + 2] << 16) + (data['buf'][FRAME_HEADER_SIZE + 3] << 24)
        self.modulated_data_len = data['buf'][FRAME_HEADER_SIZE + 4] + (data['buf'][FRAME_HEADER_SIZE + 5] << 8) + (data['buf'][FRAME_HEADER_SIZE + 6] << 16) + (data['buf'][FRAME_HEADER_SIZE + 7] << 24)
        
    #def resert the modulation ready flag (write command on rf_tx_get_modulation_ready
    def rf_tx_reset_modulation_ready(self):
        Card.write(self,ZYNQ_CMD_GET_MODULATION_READY_FLAG, 0)

    #
    def rf_tx_enable_check_modulation_data(self):
        print "Zynq> enable to store output modulation in DDR and print data send to the RF"
        Card.write(self, ZYNQ_CMD_EN_CHECK_MODULATION_DATA, 0)
        
    # create a csv pattern
    def create_csv_pattern(self, nb_element):
        list = []
        word = 0xFF
        for i in range (nb_element):
            list.append(i)
#             if (i > (nb_element/2)):
#                 list.append((word<<i))
#             else:
#                 list.append((word<<i))
#         print list
        csv_access.write_sample(self, "pattern.csv", list, 1)
        
    def rf_rx_acq_apply_config(self, selected_module, acquisition_time_ns):
        print "Zynq> apply rf rx acquisition configuration"
        samples_nb = acquisition_time_ns/RF_RX_SAMPLE_TRANSFERT_TIME_NS
        buffer = chr(selected_module & 0xFF)
        buffer = buffer + chr((samples_nb >> 0) & 0xFF) + chr((samples_nb >> 8) & 0xFF) + chr((samples_nb >> 16) & 0xFF) + chr((samples_nb >> 24) & 0xFF)
        Card.write_buf(self,ZYNQ_CMD_RF_RX_ACQ_CONFIG, buffer, len(buffer))
    
    def rf_rx_acq_read_config(self):
        buffer = Card.read(self,ZYNQ_CMD_RF_RX_ACQ_CONFIG, BRAM_CONTROLLER_CONFIG_NBBYTES, 0)
        index = FRAME_HEADER_SIZE
        bram_acquisition_en = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_controller_irq_counter = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_fill_threshold = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_irq_clear = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_overflow_flag = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_overflow_flag_clear = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_packet_timestamp = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_polarity = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_resetb = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_src_address = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_timestamp = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        cdma_error = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        cdma_is_running = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        cdma_transfert_done = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        ddr_buf_start_address = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        dma_transfert_cnt = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        emulator_en = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        last_half_bram_samples_nb = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        nb_half_bram_per_acq = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        bram_error = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        index = index + 4
        cdma_irq_counter = buffer['buf'][index + 0] + (buffer['buf'][index + 1] << 8) + (buffer['buf'][index + 2] << 16) + (buffer['buf'][index + 3] << 24)
        
        
        print "Zynq> *** RF RX Bram controller current config"
        print "Zynq> .bram_acquisition_en = " + str(hex(bram_acquisition_en))
        print "Zynq> .bram_controller_irq_counter = " + str(hex(bram_controller_irq_counter))
        print "Zynq> .bram_fill_threshold = " + str(hex(bram_fill_threshold))
        print "Zynq> .bram_irq_clear = " + str(hex(bram_irq_clear))
        print "Zynq> .bram_overflow_flag = " + str(hex(bram_overflow_flag))
        print "Zynq> .bram_overflow_flag_clear = " + str(hex(bram_overflow_flag_clear))
        print "Zynq> .bram_packet_timestamp = " + str(hex(bram_packet_timestamp))
        print "Zynq> .bram_polarity = " + str(hex(bram_polarity))
        print "Zynq> .bram_resetb = " + str(hex(bram_resetb))
        print "Zynq> .bram_src_address = " + str(hex(bram_src_address))
        print "Zynq> .bram_timestamp = " + str(hex(bram_timestamp))
        print "Zynq> .cdma_error = " + str(hex(cdma_error))
        print "Zynq> .cdma_is_running = " + str(hex(cdma_is_running))
        print "Zynq> .cdma_transfert_done = " + str(hex(cdma_transfert_done))
        print "Zynq> .ddr_buf_start_address = " + str(hex(ddr_buf_start_address))
        print "Zynq> .dma_transfert_cnt = " + str(hex(dma_transfert_cnt))
        print "Zynq> .emulator_en = " + str(hex(emulator_en))
        print "Zynq> .last_half_bram_samples_nb = " + str(hex(last_half_bram_samples_nb))
        print "Zynq> .nb_half_bram_per_acq = " + str(hex(nb_half_bram_per_acq))
        print "Zynq> .bram_error = " + str(hex(bram_error))
        print "Zynq> .cdma_irq_counter = " + str(hex(cdma_irq_counter))
        
    
    def rf_rx_acq_start(self):
        print "Zynq> start rf rx acquisition"
        Card.write(self,ZYNQ_CMD_RF_RX_ACQ_START, 0)

        
    def rf_rx_acq_read_samples_acq(self, acquisition_time_ns, output_selection):
        samplesList = []
        bytesList = []
        samplesListI = []
        samplesListQ = []
        #samplesListIQ_i = []
        #samplesListIQ_q = []
        print "Zynq> read rf rx acquisition (reliable acquisition maximum time = 250ms)"
        samples_nb = acquisition_time_ns/RF_RX_SAMPLE_TRANSFERT_TIME_NS
        sample_nb_nbbytes = samples_nb * 4
        chr_bytes = chr( (sample_nb_nbbytes) & 0xFF) + chr( (sample_nb_nbbytes >> 8) & 0xFF) + chr( (sample_nb_nbbytes >> 16) & 0xFF) + chr( (sample_nb_nbbytes >> 24) & 0xFF) 
        Card.write_buf(self,ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES, chr_bytes, 4)
        
        # get bytes from zynq
        index = 0
        while( (sample_nb_nbbytes - index) >= USB_PACKET_DIVIDER):
            bytesList = bytesList + Card.read_data(self,USB_PACKET_DIVIDER)
            index += USB_PACKET_DIVIDER;

        if((sample_nb_nbbytes - index) != 0):
            bytesList = bytesList + Card.read_data(self,sample_nb_nbbytes - index)
        
        # bytes to samples
        for index in range (0, len(bytesList), +4):
            data = bytesList[index] + (bytesList[index + 1] << 8) + (bytesList[index + 2] << 16) + (bytesList[index + 3] << 24) 
            samplesList.append(data)
#             samplesList.append(hex(data))
            
            #remove the synchro bits and extend the data MSBit for the sign
              #put the 16 bits in I and Q
            i_int = ((bytesList[index + 2]) + (bytesList[index + 3] << 8))
            q_int = ((bytesList[index]) + (bytesList[index + 1] << 8))
            #samplesListIQ_i.append(hex((i_int & 0xFFFF)))
            #samplesListIQ_q.append(hex((q_int & 0xFFFF)))
              #remove the synchro 2 bits bits 15 and 14
            if (i_int & 0x8000) and ((~i_int) & 0x4000): # 0x10
                i_int &= 0x3FFF
            if ((~q_int) & 0x8000) and (q_int & 0x4000):# 0x01
                q_int &= 0x3FFF
              #remove the LSbit bit 0
            i_int = i_int >> 1
            q_int = q_int >> 1
              #extend the data MSbit as the sign, data is on bits 12-0
            if (i_int & 0x1000):
                i_int |= 0xE000
            if (q_int & 0x1000):
                q_int |= 0xE000
            samplesListI.append(hex((i_int & 0xFFFF)))
            samplesListQ.append(hex((q_int & 0xFFFF)))
            #samplesListI.append(int((i_int & 0xFFFF)))
            #samplesListQ.append(int((q_int & 0xFFFF)))
                
        # print samples

        #for element in samplesList[:1000000]:
        #    s = '0x' + str(element[2:].zfill(8)) + "\t"

        cnt = 0
        iSyncError = 0
        qSyncError = 0
        for element in samplesList:
#             hexIQSample = int()
#             string_hex_data = re.sub('0x', '', element) # do not forget to import re
#             hexIQSample = struct.unpack('!i', str(string_hex_data).decode('hex'))[0]
            #if output_selection == ACQ_OUTPUT_STANDARD:
            hexIQSample2Dec(element, output_selection)
            """
            else: 
              if output_selection == ACQ_OUTPUT_DEC4CSV:
                hexIQSample2Dec_CSV(element)
            """
            (iErr, qErr) = checkIQsample(element)
            iSyncError = iSyncError + iErr
            qSyncError = qSyncError + qErr
            #s = '0x' + str(element[2:].zfill(8)) + "\n"
            #s = '0x' + str(element[2:].zfill(8)) + " 0x" + str(samplesListI[cnt].zfill(8)) + " 0x" + samplesListQ[cnt] + "\n"
#             s = '0x' + str(element[2:].zfill(8)) + " " + samplesListI[cnt] + " " + samplesListQ[cnt] + "\n"
            #s = '0x' + str(element[2:].zfill(8)) + " " + str(samplesListI[cnt]) + " " + str(samplesListQ[cnt]) + "\n"
            #s = '0x' + str(element[2:].zfill(8)) + " " + samplesListI[cnt] + " " + samplesListQ[cnt] + " " + samplesListIQ_i[cnt] + " " + samplesListIQ_q[cnt] + "\n"
#             cnt += 1
# 
#             print s,
        print ""
        print "number of samples received %d " % len(samplesList)
        print "number of bad iSync %d " % iSyncError
        print "number of bad qSync %d " % qSyncError 
        
    def rf_rx_acq_samples_validing(self, acquisition_time_ns):
        samplesList = []
        bytesList = []
        print "Zynq> validate increment between two samples (use when atmel emulator is enable, reliable acquisition maximum time = 250ms)"
        samples_nb = acquisition_time_ns/RF_RX_SAMPLE_TRANSFERT_TIME_NS
        sample_nb_nbbytes = samples_nb * 4
        chr_bytes = chr( (sample_nb_nbbytes) & 0xFF) + chr( (sample_nb_nbbytes >> 8) & 0xFF) + chr( (sample_nb_nbbytes >> 16) & 0xFF) + chr( (sample_nb_nbbytes >> 24) & 0xFF) 
        Card.write_buf(self,ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES, chr_bytes, 4)
        
        # get bytes from zynq
        index = 0
        while( (sample_nb_nbbytes - index) >= USB_PACKET_DIVIDER):
            bytesList = bytesList + Card.read_data(self,USB_PACKET_DIVIDER)
            index += USB_PACKET_DIVIDER;

        if((sample_nb_nbbytes - index) != 0):
            bytesList = bytesList + Card.read_data(self,sample_nb_nbbytes - index)
        
        # bytes to samples
        for index in range (0, len(bytesList), +4):
            data = bytesList[index] + (bytesList[index + 1] << 8) + (bytesList[index + 2] << 16) + (bytesList[index + 3] << 24) 
            samplesList.append(int(data))

        # validate samples
        previous_item = 0;
        packetstamp_error = 0
        for index, item in enumerate(samplesList):
            if index == 0:
                pass
            elif ((item - previous_item) != 1):
                packetstamp_error = packetstamp_error + 1
                print "Zynq> item = " + str(hex(item)) + " previous_item = " + str(hex(previous_item))
            else:
                pass
            previous_item = item
        print "Zynq> packetstamp_error: " + str(packetstamp_error)
            
    def rf_rx_read_sync_flag(self):
        print "Zynq> read RF RX sync flag..."
        reg = self.read(ZYNQ_CMD_RF_RX_SYNC_FLAG, 1, 0)
        print "Zynq> RF RX sync flag = %d" % (reg['val'])
        return (reg['val'])
    
    def rf_rx_force_emulator(self, value):
        self.write(ZYNQ_CMD_RF_RX_FORCE_EMULATOR, value)
        
    def rf_rx_reset(self):
        print "Zynq> reset RF RX..."
        self.write(ZYNQ_CMD_RF_RX_RESET, 0)
        time.sleep(0.5)
        self.write(ZYNQ_CMD_RF_RX_RESET, 1)

        while (self.rf_rx_read_sync_flag() != RF_RX_DESERIALIZER_NOT_SYNC):
            self.write(ZYNQ_CMD_RF_RX_SYNC_FLAG, 1)  
            
    def rd_CSV_buffer(self):  
        print "Zynq> read buffer that contains previous data from CSV file..."
        self.write(ZYNQ_CMD_READ_CSV_BUFFER, 0)
    
    def get_cmd_from_python(self):
        print "Zynq> read mode_python flag..."
        reg = self.read(ZYNQ_CMD_FROM_PYTHON, 1, 1)
        return (reg['val'])
    
    #expect param 0 for cmd not from python and 1 for cmd from python
    def set_cmd_from_python(self, param):
        print "Zynq> set mode_python flag..."
        buf = str()
        buf = chr((param) & 0xFF) + chr((param >> 8) & 0xFF) + chr((param >> 16) & 0xFF) + chr((param >> 24) & 0xFF)
        buf_size = 4
        add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_FROM_PYTHON
        checksum = 0
        stop_byte = 0x55
        cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        + chr(buf_size&0xFF) + chr((buf_size>>8)&0xFF)
        + chr((buf_size>>16)&0xFF) + chr((buf_size>>24)&0xFF)
        + buf
        + chr(checksum&0xFF) + chr(stop_byte&0xFF))
        self.write_raw(cmd)
