#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    
    print "get RF TX modulation ready flag and output modulation size"
    gw.zynq.rf_tx_get_modulation_ready()
    
    print "modulation_ready={} modulated_data_len={}".format(gw.zynq.modulation_ready, gw.zynq.modulated_data_len)

    exit_script()
    
except KeyboardInterrupt:
    exit_script()