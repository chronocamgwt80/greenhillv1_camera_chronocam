#!/usr/bin/python

#This file run the GWT modulation, get the parameter to start a second one (not started)
#run RF.
#should be used by example: ./TransmitterInIQRadioMode.py ;./zynq_gwt_configure.sh -a 1; ./zynq_gwt_modulation.py
#other example to test the RTL : ./zynq_gwt_configure.sh -d 0 -n 64 -t 20 -m 8 -c 2 -d 0 -n 64 -t 20 -m 8 -c 2; ./zynq_gwt_modulation.py

from greenhill import *
from zynq_def import *
import time

def exit_script():
    gw.kill()
    
try:
    # config
    output_csv_modulated = "pattern_modulated.csv"
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_DISABLE
    debug = 0
    nb_burst = 10
    
    # begin
    gw = Greenhill(USB_PROTOCOL)
    
    # reset RF TX element and wait for reset time
    gw.zynq.rf_tx_reset()
    time.sleep(0.5)
    
    #ensure modulation_ready, no more needed with CMD_from_python
    """
    if (not gw.zynq.modulation_ready):
        print "modulation is not ready, script abort"
        exit_script()
    """
          
    # 2. modulate the pattern and fill RF TX buffer with modulated words 
    gw.zynq.rf_tx_start_modulation(ZYNQ_RF_TX_GWT_MOD_ENABLE)
        
    # wait for all modulated words are written into RF TX buffer 
    #while (gw.zynq.rf_tx_get_buffer_index() < word_nb):
    #    pass
    
    #print "all data tranfered in the tx buffer"
    #time.sleep(10)

    """    
    print "wait for RF TX modulation ready flag then get output modulation size"
    gw.zynq.rf_tx_get_modulation_ready()
     
    while (not gw.zynq.modulation_ready):
        time.sleep(1)
        gw.zynq.rf_tx_get_modulation_ready()
        print "modulation_ready={}".format(gw.zynq.modulation_ready)
    
    print "modulation_ready={} modulated_data_len={}".format(gw.zynq.modulation_ready, gw.zynq.modulated_data_len)
    """
    
    # read modulated pattern which was written into DDR
    if debug:
        gw.zynq.rf_tx_read_modulated_data(gw.zynq.modulated_data_len, output_csv_modulated)
        
    # 3. start RF TX transmission
    gw.zynq.start_rf_transmission()
    
    # wait for all words was sent
    while (gw.zynq.rf_tx_get_buffer_index() != 0):
        pass
    time.sleep(0.0000005) # wait for two last samples transmission (2*250ns)
    
    # 4. stop the RF TX transmission
    gw.zynq.stop_rf_transmission()


#         while 1:
#             pass


#        # required if you want to play a pattern in loop
#         gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)

    exit_script()
    
except KeyboardInterrupt:
    exit_script()
