#!/usr/bin/python
import usb.core
import usb.util
import sys
import time 
import Atreb215_def

#AT86RF215_RF_RST=0x0005
HEADER_SIZE = 6
FOOTER_SIZE = 2
OVERHEAD_SIZE = HEADER_SIZE + FOOTER_SIZE

def exit():
    gw.kill()
    sys.exit()



to_console = 1
not_to_console = 0

class GWBoard:
    def __init__(self, idVendor=0x5151, idProduct=0x0001):
        # find our device
        self.dev = usb.core.find(idVendor=0x5151, idProduct=0x0001)
        if self.dev is None:
            raise ValueError('USB- Device not found')
        else:
            print 'USB- Device found'
        if self.dev.is_kernel_driver_active(0):
            try:
                self.dev.detach_kernel_driver(0)
                print "USB- kernel driver detached"
            except usb.core.USBError as e:
                sys.exit("USB- Could not detach kernel driver: %s" % str(e)) 
        else:
            print "USB- kernel already driver detached"
        try:
            usb.util.claim_interface(self.dev, 0)
            print "USB- device claimed"
        except:
            sys.exit("USB- Could not claim the device: %s" % str(e)) 
#    def write_reg(self,add,value):
#        write_cmd = chr((add>>24)&0xFF) + chr((add>>16)&0xFF) + chr((add>>8)&0xFF) + chr((add)&0xFF)
#        nbbytes = chr(0) + chr(4)
#        data = chr((value>>24)&0xFF) + chr((value>>16)&0xFF) + chr((value>>8)&0xFF) + chr((value)&0xFF)
#        end_cmd = chr(0x55)
#        cmd = write_cmd + nbbytes + data + end_cmd; 
#        self.dev.write(2,cmd,interface=0,timeout=1)
#        time.sleep(0.1)
    def write_reg(self,add,value,regnbbytes):
        write_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        nbbytes = chr(1) + chr(0)
        data = chr((value)&0xFF)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = write_cmd + nbbytes + data + checksum + end_cmd; 
        self.dev.write(2,cmd,interface=0,timeout=100)
        time.sleep(0.1)
           
    def read_reg(self,add,regnbbytes,toconsole): 
        read_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr(0x80 + ((add >> 24)&0xFF))
        nbbytes = chr((regnbbytes)&0xFF) + chr((regnbbytes>>8)&0xFF)
#        data = chr(0) + chr(0) + chr(0) + chr(0)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = read_cmd + nbbytes + checksum + end_cmd;
        self.dev.write(2,cmd,interface=0,timeout=100)
        time.sleep(0.1)
        d=self.dev.read(0x82,OVERHEAD_SIZE+regnbbytes,timeout=100)
        time.sleep(0.1)
        if (toconsole):
            add = int(d[0]) + int(d[1]<<8) + int(d[2]<<16) + int(d[3]<<24)
            nbbytes = int(d[4]) + int(d[5]<<8)
            val = 0
            valhex = ''
            for k in range(0,regnbbytes):
                valhex = valhex + hex(d[k+HEADER_SIZE])[2:] + ' '
                val = val + (d[k+HEADER_SIZE] << k) 
            print "register (" + hex(add) + ") = 0x" + valhex
        return {'add':add,'val':val}
    def kill(self):
        usb.util.release_interface(self.dev, 0)
        del self


# atreb215 reset via special register access (dedicated pin is not used to perform a reset operation)
def at86rf215_reset_register():
    print "Atmel Chip reset..."
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF_RST), 0x07,1)
    # [2:0] : CMD, writing 0x07 in this register triggers the reset procedure
    my_data=0x00
    while(my_data!=0x01):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQS), 1, 1)
        my_data=(reg['val'])&0x01
        # [5:0] : interruption request indicator (end of reset is expected)
    print "Chip reset completed."






#####################################################
# Main process below
#####################################################
try:
    # greenhill board instance
    gw = GWBoard()

    at86rf215_reset_register()
           
           
    #enable iq
#     data = 0x0
#     reg = gw.read_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF_IQIFC1), 1, 1)
#     data = (reg['val'])
#     data |= Atreb215_def.RF_MODE_RF

#     gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF_IQIFC1), Atreb215_def.RF_MODE_RF | 0x2, 1)
#     reg = gw.read_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF_IQIFC1), 1, 1)
           
    # set as example register configuration (table 7.1 at86rf215 datasheet)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_IRQM), (0x1F), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CS), (0x30), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CCF0L), (0x20), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CCF0H), ( 0x8D), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CNL), 0x00, 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CNM), 0x00, 1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_RXBWC), ( 0x09), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_RXDFE), ( 0x83), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_EDD), ( 0x7A), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_IRQM), ( 0x1F), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_PC), ( 0xD6), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_OFDMPHRTX), ( 0x03), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_TXCUTC), ( 0x0B), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_TXDFE), ( 0x83), 1)
    time.sleep(0.1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_PAC), ( 0x7C), 1)
    
    
    # Set frame length
    frame_length = 0x7FF
    FCS_length = 32/4
    frame_length = (frame_length + FCS_length) & 0x7FF
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_TXFLL), (frame_length), 1)
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_TXFLH), (frame_length > 8), 1)
    
    # Write frame in frame buffer
    sample = 0xAA
    for index in range(0, frame_length):
        gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_BBC0_FBTXS + index), sample, 1)
        
    
    # Go to TXPREP STATE
    gw.write_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_CMD), Atreb215_def.RF_STATE_TXPREP, 1)
    
    # waiting for txprep transition end
    my_data=0x00
    while(my_data!=Atreb215_def.RF_STATE_TXPREP):
        reg=gw.read_reg(Atreb215_def.AT86RF215_BASEADDR +(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
        my_data=(reg['val'])
    
    # go to TX state
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), Atreb215_def.RF_STATE_TX, 1)
      
    reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
    my_data=(reg['val'])
    print "RF09 current state : " + hex(my_data)

        
    exit()

except KeyboardInterrupt:
    gw.kill()
