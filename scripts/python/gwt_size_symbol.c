// 
// green plus V1
//  started 20/06/2015
//
//Green-OFDM M=4 / 2048 tones

//compile with: gcc gwt_size_symbol.c -o gwt_size_symbol -lm

//#define LINUX 1
//#define ZYNQ 1

#ifdef ZYNQ
#include "Greenhill.h"
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
//#include <complex.h>

//#include "gw_mod_and_autotest_prep_porting.h"



#ifdef LINUX
#include <fftw3.h>
#endif


//#define TEST_PN

#define DEB_DMOD 1
#define SKIP_MOD 0

// skip the "noisy" portion at beginning of rx phase
#define FIRST_SAMPLES_SKIPPED 4096

//#define CC_CODE_ON 

// to set if waveform TX requested (float and csv are exclusive)
#define DUMP_CSV 1
#define READ_CSV 1

// spectrum is centered on 0
#define CENTER_SPECTRUM_0 1
// number of null tones forced on each side of DC (DC-ZTONE_RADIUS DC+ZTONE_RADIUS) are set to 0
#define MAX_POW_VAL 60

// substract input offset
#define SUBSTR_OFF 1

// margin for start of symbol decision
#define EPS_DIST 10

// interpolation des fcteurs de correction des tones
#define INTERP_CORR_FACTORS 1
#define FORCE_SIDE_INFO_RX 1 

#define FIND_FIRST_PEAK_ONLY 1
#define DETECT_PEAK 1

#define GEN_FIXED_POINT 1
#define MEASURE_RX_POWER 1
#define TX_GAIN (1.0/5.0)
#define MIN_INPUT_POWER (1.0)

#define FORCE_SYMBOL_DETECTOR 1
#define TRY_WAVE 0

//#define GREEN_MOD 1
#define SCALE (0.8) // scaling factor for rand 
#define RANDOM_START 0 // length of the first symbol
#define PI 3.14159
// flag for source selection
#define READ_SAME_BITLOAD 0
#define GEN_RANDOM_BITSTREAM 1
#define READ_INPUT_FROM_FILE 0
#define READ_PN_FROM_FILE 0

// OFDM symbols parameters
#define    NB_OF_SYMBOLS 10 // nb of symbols to modulate to generate the modulated file
#define LAST_SYMBS ((int)floor(0.1*NB_OF_SYMBOLS))

#define BIG_INT 0x7ffffff
#define OVSMPL 0

#define NFFT 256 // default FFT size
#define NFFT_MAX 2048 // default FFT size
#define NCP 64 // default NCP size
#define NB_TONES_OFF 120 // default off tones at side of symbol
#define MSUB_SECT 4    // default  Number of Sections
// default constellation
#define CONSTELL QPSKC

#define      N_MIN 2
#define      N_MAX 2048 // max Number of tones
#define      M_MAX 128    // max Number of Sections
//Green-OFDM M=4 / 2048 tones
#define      NCP_MIN 0 // max cyclic prefix
#define      NCP_MAX (N_MAX/2) // max cyclic prefix
#define NB_TONES_OFF_MIN 0 // min off tones at side of symbol
#define NB_TONES_OFF_MAX 150 // max off tones at side of symbol
#define ZTONE_RAD_L 0
#define ZTONE_RAD_R 0
//#define ZTONE_RAD_L 2
//#define ZTONE_RAD_R 1
//#define   N 2048 
//#define NCP 256 
//#define NB_TONES_OFF 512
//#define ZTONE_RAD_L 7
//#define ZTONE_RAD_R 6

#define SNRDB 80
#define snr (pow(10,(-SNRDB/10.0)))
// Generation of Green-OFDM Sections sq(n')

#define      N1 (N_MAX/4)
#define      N2 (N_MAX/2)
#define      N3 (3*N_MAX/4)

#define SIZEOF_BYTE 8
#define NACTIVE_TONES (N_MAX-NB_TONES_OFF_MIN)
//#define NPILOTS NACTIVE_TONES/5 // pilots
#define NPILOTS ((NACTIVE_TONES/5)+1) // pilots
#define FFT_PILOT 256
// tones carryoing data: all active except pilots and DC (no pilot on DC)
#define NDATA_TONES_MAX (NACTIVE_TONES)
//#define NDATA_TONES (NACTIVE_TONES-NPILOTS-CENTER_SPECTRUM_0*(1+ZTONE_RAD_L+ZTONE_RAD_R))
#define NBITS_PER_TONE_MAX 4 // 16QAM
#define NB_BITS_IN_SYMBOL_MAX ((NDATA_TONES_MAX)*(NBITS_PER_TONE_MAX))
#define NB_CHAR_IN_SYMBOL_MAX ((NB_BITS_IN_SYMBOL_MAX>>3)+1)
#define RAD_SEARCH_PEAK 20
#define PEAK_DETECTOR_FACT 0.95
#define DEPTH_MAX_AVERAGE 5
#define FACTOR_COARSE_SYNC 1.2 // threshold for coarse sync detection
#define POW_BUFF_SIZE (N_MAX)
// struct to store all peaks detected in the current buffer
#define MAX_NB_PEAK 10 
#define AVERAGE_PEAK_POSITION 0
// nb of records for power measurement : 16Msmples with N+NCP symbol length
//#define MAX_POW_INDX ((int)floor(((float)(1<<24)/(float)(N_MAX+NCP)) )+1)
#define MAX_HISTO_BINS 40

// negative offset for start f symbol, to prevent interference between i/i+1 if start index estimated by coarse sync is below start of CP
#define MINBACKOFF -40
#define MAXBACKOFF 40  
#define DEF_BACKOFF_INDX 4
#define SEARCH_START 1
//#define FFT_NORM (1.0/sqrt((float)N*M))

// variance of the in signal to be considered as noise
#define DEF_NOISE_LEVEL 6
// min ratio between in signal and DEF_NOISE_LEVEL to consider there is signal 
#define MIN_SNR 10
// define the min peak value to consider the signal can be an OFDM symbol
// ==> must be related to the average signal value at receiver (signal to noise)
#define MIN_PEAK_VAL 300000.0

//
#define NORM_QAM16 (1.0/sqrt(10.0))
#define NORM_QPSK (1.0/sqrt(2.0))
#define NORM_BPSK (1.0)

// fixed point format at TX input and Rx output
//#define FIXED_POINT_FRAC 11
#define FIXED_POINT_FRAC 11
#define FIXED_POINT_INT 1

// new params for grenn++
#define L_PNSEQ 16
#define NB_SIGNAL_TONES 32
#define FIRST_SIGNAL_POS 2 // tone number after null sidetones
//#define SPACE_BW_SIGNAL_TONES ((int)floor((float)(N_MAX-NB_TONES_OFF-2*FIRST_SIGNAL_POS)/(float)(NB_SIGNAL_TONES-1)))

// parameters for  papr histogram
#define MIN_PAPR 2
#define MAX_PAPR 17
#define NB_BINS_HISTO_PAPR 100
#define BIN_WIDTH ((float)(MAX_PAPR-MIN_PAPR)/(float)NB_BINS_HISTO_PAPR)
 
typedef enum {UNASSIGNED_TONE,SIG_TONE,NULL_TONE,DATA_TONE,PILOT_TONE} tone_type;

struct peak_info {
  int index;
  int smpl_indx;
  double val_max;
  double val_phase;
};

struct peak_info list_of_peaks[MAX_NB_PEAK];

/* float complex point */
/*
struct complex {
	double	r;
	double	i;
};
*/

struct complex_f {
	float	r;
	float	i;
};

const struct complex_f BPSK[2] = {-1,1};

// Gray mapping
const struct complex_f QPSK[4] = {{-1,1},{1,1},{-1,-1},{1,-1}};

const struct complex_f pilot[2] = {{1,1},{-1,-1}};


/*
const struct complex QAM16[16] = {{-3,3},{-3,1},{-3,-3},{-3,-1},
			      {-1,3},{-1,1},{-1,-3},{-1,-1},
			      {3,3},{3,1},{3,-3},{3,-1},
			      {1,3},{1,1},{1,-3},{1,-1}};
*/
// Gray mapping
const struct complex_f QAM16[16] = {{-3,-3}, {-3,-1}, {-3,3}, {-3,1},
				  {-1,-3}, {-1,-1}, {-1,3}, {-1,1},
				  {3,-3}, {3,-1}, {3,3}, {3,1}, 
				  {1,-3}, {1,-1}, {1,3}, {1,1}};

// list of available constellation
typedef  enum  {BPSKC,QPSKC,QAM16C} lconstell;

// available modes
typedef enum {DEFAULT = -1, CONV_OFDM = 0 ,GREEN_GREEN = 1, GREEN_CONV = 2} mode_ofdm;

// contains nb of bits for each constellation
char NBITSCONST[4];

#ifdef LINUX
FILE *dump_in_mod;
#endif
int sample_nb;
int indx_max_average,indx_avpow;
int last_index_value;
int symb_to_process;
int partial_buffer;
int index_peak;
int nb_smples_in_buff;
double *phi_sum;
double *gm_sum;
double av_pow,av_max_value;
double av_max[DEPTH_MAX_AVERAGE],powinst[POW_BUFF_SIZE];
double sigma_autocorr_ref[2];
double min_input_power_v;
double fft_norm_v;
struct complex_f pilot_seq[NPILOTS];

// dependant on input parameters
int nsub_sect, nfft, ncp, constell = CONSTELL, null_tones, nb_sigtones;//TODO TBC the init value, done to avoid unizialized variable as used as condition
int SPACE_BW_SIGNAL_TONES;
int nfft1, nfft2, nfft3;
int n1;


void plot(int Npoint, struct complex_f *data);
void plot_qam(int Npoint, struct complex_f *data);

void id_fft(struct complex_f *in, struct complex_f *out, uint32_t width)
{
  //uint32_t i = 0;
  int i;
	//out = in;
	for(i=0; i < width; i++)
	{
		out[i].r = in[i].r;
		out[i].i = in[i].i;
	}
	//for(i=0; i<width; i++) printf("id_FFT: i=%d in.r=%f in.i=%f out_fft.r=%f out_fff.i=%f\n",   i, (in+i)->r, (in+i)->i, (out+i)->r, (out+i)->i);
}

void build_symbol_struct_0(char *symb_struct,mode_ofdm modeofdm) {

  int i,j;
  int next_sig_tone;

  next_sig_tone = nsub_sect * FIRST_SIGNAL_POS + nsub_sect * (null_tones >> 1);

  printf("buid symb structure\n");
  printf("nb sig tones %d, space bw signal tones %d\n", nb_sigtones, SPACE_BW_SIGNAL_TONES);
  // 0-> null tone, 1-> data , 2-> pilot 3-> signalling

  for(i=0; i < nfft*nsub_sect ;i++)
    symb_struct[i] = UNASSIGNED_TONE;

  // describe one subsymbol k%M then copy the config for interleaving: result is one M*nfft symbol containing M symbols of nfft tones

  for(i=0;i<(null_tones>>1)*nsub_sect;i+=nsub_sect)
    symb_struct[i]=NULL_TONE;


  for(i=nsub_sect*nfft-((null_tones+1)>>1)*nsub_sect;i<nfft*nsub_sect;i+=nsub_sect)
    symb_struct[i]=NULL_TONE;

  //scatters pilot every 4 tones
  for(i = nsub_sect*(null_tones>>1), j=0; i < nsub_sect * nfft - nsub_sect * ((null_tones+1)>>1); i += nsub_sect, j++) {
 
    assert(symb_struct[i]==UNASSIGNED_TONE);

    if (((modeofdm!=CONV_OFDM)) && (i>=next_sig_tone) && (j%5) && !(CENTER_SPECTRUM_0 && (i>=nsub_sect*(nfft/2-ZTONE_RAD_L))&&(i<=nsub_sect*(nfft/2+ZTONE_RAD_R)))) { 
      symb_struct[i]=SIG_TONE;
      next_sig_tone+=SPACE_BW_SIGNAL_TONES*nsub_sect;
    }
    else if ( (j%5) && !(CENTER_SPECTRUM_0 && (i>=nsub_sect*(nfft/2-ZTONE_RAD_L))&&(i<=nsub_sect*(nfft/2+ZTONE_RAD_R)))) { 
    // map data on this tone
      symb_struct[i]=DATA_TONE;
     
    } 
    else if(CENTER_SPECTRUM_0 && (i>=nsub_sect*(nfft/2-ZTONE_RAD_L)) && (i<=nsub_sect*(nfft/2+ZTONE_RAD_R))) {
      // map null tone at DC
      symb_struct[i]=NULL_TONE;
    }
    else{ 
      symb_struct[i]=PILOT_TONE;
    }
  }

  // interleave
  if (1) for (i=0;i<nfft*nsub_sect;i+=nsub_sect)
    for (j=1;j<nsub_sect;j++) {
      symb_struct[i+j]=symb_struct[i];
    }
/*
  for (j=0;j<nfft;j++){
    printf("%d\t",j);
    for(i=0;i<nsub_sect;i++)
      printf("%d ",symb_struct[j*nsub_sect+i]);
    printf("\n");
  }
  for (j=0;j<nfft*nsub_sect;j++){
      printf("%d ",symb_struct[j]);
      if (j&&!(j%50)) printf("\n");
  }
  printf("\n");
*/
}// end of build_symbol_struct

void init_autocorr_ref(int mode_v) {

  // for ref[0]=sigma we get  ref[1]=autocorr energy
  // must be done for each CP  value and constellation

  if (nfft == 256) {
    if (constell == QPSKC)
      if (mode_v) {
	sigma_autocorr_ref[0] = 0.714115;
	sigma_autocorr_ref[1] = 15.191148;
      } else {
	sigma_autocorr_ref[0] = 0.718070;
	sigma_autocorr_ref[1] = 22.484635;
      }
    
    if (constell == QAM16C)
      if (mode_v) {
	sigma_autocorr_ref[0] = 0.710920;
	sigma_autocorr_ref[1] = 21.134869;
      } else {
	sigma_autocorr_ref[0] = 0.709313;
	sigma_autocorr_ref[1] = 26.317043;
      }
  } else 
    printf("init autocorr ref not supported for this config of nfft/n");


} // end of init_autocorr_ref


void norm(struct complex_f *in, double factor){
  int i;

  for (i=0;i<nfft+ncp;i++) {
    in[i].r = factor * in[i].r;
    in[i].i = factor * in[i].i;
  }

}
void norm_v(struct complex_f *in, double factor,int nb){
  int i;

  for (i=0;i<nb;i++) {
    in[i].r = factor * in[i].r;
    in[i].i = factor * in[i].i;
  }

}
// compute a/b (a and b complex)
struct complex_f cpx_div(struct complex_f a,struct complex_f b) {

  struct complex_f c;
  double sqmod;

  sqmod= b.r*b.r+b.i*b.i;
  c.r=(a.r*b.r+a.i*b.i)/sqmod;
  c.i=(-a.r*b.i+a.i*b.r)/sqmod;
  return c;

}// end of cpx_div

// compute a*b (a and b complex)
struct complex_f cpx_mul(struct complex_f a, struct complex_f b) {

  struct complex_f c;

  c.r=(a.r*b.r-a.i*b.i);
  c.i=(a.r*b.i+a.i*b.r);
  return c;

}// end of cpx_mul

double modcpx(struct complex_f a){

  return sqrt(a.r*a.r+a.i*a.i);

}// modcpx

void get_pilot(struct complex_f *pilots,struct complex_f *tones) {

  int i,j;
  for(i=(null_tones>>1),j=0;i<nfft-(null_tones>>1);i+=5,j++) {
    assert(j<NPILOTS);
    pilots[j]=tones[i];
  }

}// end of get_pilot

//
double  get_delta(struct complex_f *pilots1,struct complex_f *pilots2){

  int i;
  struct complex_f pdp;
  double delta,av_delta=0.0;

  // for pilots 
  for(i=0;i<NPILOTS;i++) {
    pdp = cpx_div(pilots2[i], pilots1[i]);
    delta = acos(pdp.r/modcpx(pdp));
    av_delta += delta;
    if (0) printf("%d =>p1=%f %f | p2=%f %f | pdp= %f %f |delta=%f\n",i,pilots1[i].r,pilots1[i].i,pilots2[i].r,pilots2[i].i,pdp.r,pdp.i,delta);
  }
  return av_delta/(float)NPILOTS;

}// get_delta

void demod_tones(struct complex_f *tones,struct complex_f *correc) {

  int i,j,k,togpil=0,trnd;
  struct complex_f pilot_ref[N_MAX],pilot_rec[N_MAX];
  double mod,tones_r,tones_i;

  k=0;
  for(i=(null_tones>>1),j=0;i<nfft-(null_tones>>1);i++,j++) {
    
    if (j%5) { // not a pilot
      //k++;tog=(tog+1)%2;
      pilot_ref[i].r = 0.0;
      pilot_ref[i].r = 0.0;
      pilot_rec[i].r = 0.0;
      pilot_rec[i].r = 0.0;
    } else {
      togpil++;
      togpil = togpil%8;
      //      printf("j=%d npil=%d\n", j, NPILOTS);
      if (k >= NPILOTS) break;
      trnd = (togpil >= 4) ? 0 : 1;
      pilot_ref[i] = pilot[trnd];
      pilot_rec[i] = tones[i];
      k++;
    }
  }

  // compute correction factor  for channel compensation and coarse timing estimation error
  // invese of (a+jb is (a-jb)/(a2+b2)
  if (0) 
    for(i=(null_tones>>1); i < nfft - (null_tones >> 1); i++) {
      mod=pilot_rec[i].r*pilot_rec[i].r+pilot_rec[i].i*pilot_rec[i].i;
      correc[i].r=(pilot_rec[i].r*pilot_ref[i].r+pilot_rec[i].i*pilot_ref[i].i)/mod;
      correc[i].i=(pilot_rec[i].r*pilot_ref[i].i-pilot_rec[i].i*pilot_ref[i].r)/mod;
  }

  // interpolate correction factor
  if (0)
    for(i=(null_tones>>1);i<nfft-(null_tones>>1);i+=5) {
      for(k=1;k<5;k++) {
	correc[i+k].r=((float) k/5.0) *correc[i].r+((float) (5-k)/5.0) * correc[i+5].r;
	correc[i+k].i=((float) k/5.0) *correc[i].i+((float) (5-k)/5.0) * correc[i+5].i;
      }
    }

  if (0) 
    for(i=(null_tones>>1);i<nfft-(null_tones>>1);i++) {
      tones_r=tones[i].r*correc[i].r-tones[i].i*correc[i].i;
      tones_i=tones[i].r*correc[i].i+tones[i].i*correc[i].r;
      tones[i].r=tones_r;
      tones[i].i=tones_i;
    }


  if (0) {
  printf("=========== DUMP CORRECTED TONES\n");
  for(i=0;i<nfft;i++)
    printf("tones %d  %f %f\n",i,tones[i].r,tones[i].i);
  }



} // end of demod_tones

char demap_tone(struct complex_f tone,lconstell constel,double *dist) {

  char i,imin;
  struct complex_f diff;
  double distmin=(double) BIG_INT,distc;
  char nb;
  struct complex_f *qam;


  nb=(1<<NBITSCONST[constel]);

   for(i=0; i < nb;i++) {
     if (constel == QAM16C){ 
       diff.r = tone.r - QAM16[i].r;    
       diff.i = tone.i - QAM16[i].i;
     }  
     if (constel == QPSKC){ 
       diff.r=tone.r-QPSK[i].r;    
       diff.i=tone.i-QPSK[i].i;
     }  
     if (constel == BPSKC){ 
       diff.r = tone.r - BPSK[i].r;    
       diff.i = tone.i - BPSK[i].i;
     }  
      distc = modcpx(diff);
      if (0) printf("%d %f\n", i, distc); 
      if (distc < distmin) { distmin = distc; imin = i;}
      
    }

   *dist=distmin;

  return (char) imin;


}// end of demap_tone

void compare_char(char c1,char c2, int *nbdiff) {

  int j;
  char one=0x1;
  int ndiff_i;

  ndiff_i=*nbdiff;

  for (j=0;j<8;j++) {
    if ((c1&(one<<j)) != (c2&(one<<j))) ndiff_i++;
    if (0) printf("%d %d %d %d\n",j,c1&(one<<j),c2&(one<<j),ndiff_i);
  }
  *nbdiff=ndiff_i;

}// end of compare_char

void compare_bs(char *bs1,char *bs2,int nbits,int *ndiff){

  int i;
  unsigned char b1,b2;
  unsigned char one=0x1;
  int nchar;

  nchar=nbits>>3;

  for(i=0;i<nchar;i++) {
    b1=bs1[i];
    b2=bs2[i];
    if (0)printf("i %d: %x %x\n",i,b1,b2);
    compare_char(b1,b2,ndiff);
  }


}// end of compare_bs

// only tones forming the first subset i%M=0 are extracted and processed
double bit_extract(struct complex_f *tones,char *bitstream,int *nbit, unsigned int *last,int *indxbs,lconstell constel,char* struct_symb,char *pn_seq, mode_ofdm mode){

  int i,j,k,l;
  unsigned char bits,lastbits; 
  int nbit_i,lastpos;
  int nbits_per_tone;
  double dist,totdist=0.0;
  int mm;

  if (mode==GREEN_GREEN) mm=nsub_sect; else mm=1;

  nbits_per_tone=NBITSCONST[constel];
  lastpos=*last;
  nbit_i=0;
  if (0)printf("enter bit_extract: last=%d nbit=%d\n",lastpos,*nbit);

  // extract the first subset i%nsub_sect=0
  for(i=0,j=0,k=0,l=0;i<nfft*nsub_sect;i+=nsub_sect,k+=mm) {
    
    if (struct_symb[i]==DATA_TONE) { // 
 
      bits=demap_tone(tones[k],constel,&dist);
      totdist+=dist;

      bitstream[l]|=((bits<<(lastpos))&0xff);

      if (0) printf("i %d bits %x l=%d  bs=%x\n",k,bits,l,bitstream[l]);

      lastpos+=nbits_per_tone;

      if (lastpos>=SIZEOF_BYTE) {
	lastpos-=SIZEOF_BYTE;
	lastbits=bits>>(nbits_per_tone -lastpos);
	l++;
	bitstream[l]=0;
	bitstream[l]|=lastbits;	
      } 
      nbit_i+=nbits_per_tone;

      if (0) printf("\t l %d lastbits %x lastpos %d bs %x nbiti=%d\n",l,lastbits,lastpos,bitstream[l],nbit_i);

    }

    j++;

  }

  *nbit=*nbit+nbit_i;
  *last=lastpos;
  *indxbs=l;

  return totdist;

}// end of bit_extract

// pilot_rec : received tones. The function corrects according to pilot and modify pilot_rec


int count_tone(char *symb_struct,tone_type tone_type_id){
  int i,j,k;

  for(i=0,k=0;i<nfft*nsub_sect;i++)
    if (symb_struct[i]==tone_type_id ) k++;

  return k/nsub_sect;   

}// end of count_tone

// pilot sequence for receiver
void build_pilot_seq(char *struct_symb,mode_ofdm mode,char *pn_seq) {

  int i,npil;
  char mm;

  if (mode==GREEN_GREEN) mm=nsub_sect; else mm=1;

  // construct pilots
  npil=0;
  for(i=0;i<nfft*nsub_sect;i+=nsub_sect)
    if (struct_symb[i]==PILOT_TONE) {
	pilot_seq[npil].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
	pilot_seq[npil].i = NORM_QPSK*pilot[0].i*pn_seq[npil];
	npil++;
    }

}// end of build pilot_seq

double dump_tones(struct complex_f *pilot_rec, struct complex_f *ref, int bckoff, struct complex_f *correc,
		          char flag_compute_corr, int *diffnb, char flag_plot, lconstell constel, double *maxd,
		          char *struct_symb, mode_ofdm mode, int nb_data_tone, char *pn_seq1, char *pn_seq2) {

  int i, j = 0, nbiterr, k, ii; 
  struct complex_f pilot_corr;
  double av_angle = 0.0,anglec,mod;
  //struct complex pilot[NPILOTS];
  struct complex_f sig_tone[NB_SIGNAL_TONES];
  double maxp1=0.0, minp1=(float)BIG_INT ,maxp2 = 0.0, minp2 = (float)BIG_INT ;
  int indmin1 = -1, indmax1 = -1;
  char bits, bitsref;
  int ndiff, indx_sig = 0;
  double dist, totdist = 0.0, maxdist = 0.0;
  int mm,tone_nb;
  //int num_section;
  struct complex_f pn_seq_comb;
#ifdef LINUX
  fftw_complex *fft_pil;
  fftw_plan p;
#endif
  int num_section1, num_section2;
  int pil_nb = 0, num_sigtone_mapped = 0;
  int numsect1_ref, numsect2_ref;
  int npil = 0;

  printf("FLAG_CORR=%d\n",flag_compute_corr);
  
  if (mode == GREEN_GREEN) mm = nsub_sect; else mm = 1;

  if (1) for(i=0,ii=0;i<nfft*mm;i++,ii+=nsub_sect)printf("!!!!tones %s %d  %f %f | %f %f\n",(struct_symb[ii]==SIG_TONE)?"sig":"",i,pilot_rec[i].r,pilot_rec[i].i,ref[i].r,ref[i].i);

  // compute correction factors ref/rec : select the first subset (i%nsub_sect=0)
  for(i=0,j=0,k=0;i<nfft*nsub_sect;i+=nsub_sect,j++,k+=mm) {
    if (struct_symb[i]!=NULL_TONE){
      if (flag_compute_corr) {
	if ((mode!=CONV_OFDM && struct_symb[i]==PILOT_TONE)) {
	    pn_seq_comb.r=(double)pn_seq1[pil_nb];
	    pn_seq_comb.i=(double)pn_seq2[pil_nb];
	    printf("%d %f %f %f %f",k,pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);
	    //pilot_rec[k].r*=(double)pn_seq1[pil_nb];
	    //pilot_rec[k].i*=(double)pn_seq1[pil_nb];
	    //pilot_rec[i]=cpx_div(pilot_rec[k],pn_seq_comb);

	    //	    pil_nb++;
	    // ref pilots are modulated by pn_seq1
	    printf("==  %f %f\n",pilot_rec[k].r,pilot_rec[k].i);
	    printf("\trr %f %f\n",ref[k].r,ref[k].i);
	    //ref[k].r=pn_seq1[pil_nb]*ref[k].r;
	    //ref[k].i=pn_seq1[pil_nb]*ref[k].i;
	    pil_nb++;
	  }

	if (struct_symb[i]==PILOT_TONE){
	  correc[j]=cpx_div(pilot_seq[npil],pilot_rec[k]);
	  printf("pilot");
	  printf("\t%d %f %f rr %f %f cc %f %f\n",j,pilot_rec[k].r,pilot_rec[k].i,pilot_seq[npil].r,pilot_seq[npil].i,correc[j].r,correc[j].i);
	  npil++;
	} 
	else { 
	  correc[j]=cpx_div(ref[k],pilot_rec[k]);
	  printf("\t%d %f %f rr %f %f cc %f %f\n",j,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);
	}
      }
      
      if (0&& (struct_symb[i]==SIG_TONE)){
	printf("indx sig %d %d (%f %f)\n",i,indx_sig,pilot_rec[k].r,pilot_rec[k].i);
	sig_tone[indx_sig++]=pilot_rec[k];
      }
      
      if (struct_symb[i]==PILOT_TONE){
	if (!flag_compute_corr) {
	  // pilots are modulated by pn_seq1 at transmit => necessary to demodulate (in green and conventional)
	  //pilot_rec[k].r=pn_seq1[pil_nb]*pilot_rec[k].r;
	  //pilot_rec[k].i=pn_seq1[pil_nb]*pilot_rec[k].i;
	  if (mode!=CONV_OFDM) {
	    pn_seq_comb.r=(double)pn_seq1[pil_nb];
	    pn_seq_comb.i=(double)pn_seq2[pil_nb];
	    printf("%f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[i].r,pilot_rec[i].i);
	    pilot_rec[i]=cpx_div(pilot_rec[k],pn_seq_comb);
	  }
	  correc[j]=cpx_div(ref[k],pilot_rec[k]);
	  pil_nb++;
	}
      }
    }
  }

    //plot(dump_in_mod,j,pilot);

    //plot(dump_in_mod,FFT_PILOT,(struct complex*)fft_pil);
    

    //plot(dump_pilots,(j-1)/5,pilot);

    // interpolate correction factors
    if (INTERP_CORR_FACTORS)
      for(i=(null_tones>>1);i<nfft-((null_tones+1)>>1);i+=5) {
	for(j=1;j<5;j++) {
	  correc[i+j].r=(float)(5-j)/5.0*correc[i].r+(float)j/5.0*correc[i+5].r;
	  correc[i+j].i=(float)(5-j)/5.0*correc[i].i+(float)j/5.0*correc[i+5].i;
	}
	
      }

    // ZF channel correction of signal tones using pilots
    indx_sig=0;
    num_section1=0;
    num_section2=0;
    num_sigtone_mapped=0;
    if (mode!=CONV_OFDM) {
      for(i=0,j=0,k=0;i<nfft*nsub_sect;i+=nsub_sect,j++,k+=mm) {
	if (struct_symb[i]==SIG_TONE) {

	  numsect1_ref=(int)ref[j].r;
	  numsect2_ref=(int)ref[j].i;
	  
	  if (num_sigtone_mapped==(nb_sigtones/2))  {num_section1=num_section2;num_section2=0;num_sigtone_mapped=0;}
	  sig_tone[indx_sig] = cpx_mul(correc[j], pilot_rec[k]);
	  // concatenate the 2bits info to buid numsection
	  num_section2|=demap_tone(sig_tone[indx_sig],QPSKC,&dist)<<(2*num_sigtone_mapped);
	  //num_section1<<=2;
	  num_sigtone_mapped++;
	  printf("sig indx %d %f %f cc %f %f ns %d\n",indx_sig,sig_tone[indx_sig].r,sig_tone[indx_sig].i,correc[j].r,correc[j].i,num_section2);
	  /*
	  // first sig tone gives "left" numsection second gives "right" numsection
	  if (indx_sig==0) num_section1=demap_tone(sig_tone[indx_sig],QPSKC,&dist);
	  if (indx_sig==1) num_section2=demap_tone(sig_tone[indx_sig],QPSKC,&dist);
	  if ((indx_sig==0) || (indx_sig==1)) printf("corrected sig tone %i %x\n",i,demap_tone(sig_tone[indx_sig],QPSKC,&dist));
	  */
	  indx_sig++;
	  
	}
      }
      // !!! force num section
      //num_section1=1;
      //num_section2=2;
    } else {
      num_section1=0;
      num_section2=0;
    }
    
    printf("detected num section %d(%d) %d(%d)\n",num_section1,numsect1_ref,num_section2,numsect2_ref);

      if(num_section1!=numsect1_ref || num_section2!=numsect2_ref)
	printf("side info decode mismatch\n"); 

      if (!FORCE_SIDE_INFO_RX) {
	num_section1=numsect1_ref;
	num_section2=numsect2_ref;
      }

    // ZF correction of data tones and descrambles using PN seq indicated by num_section
    nbiterr=0;
    tone_nb=0;
    npil=0;
    for(i=0,j=0,k=0;i<nfft*nsub_sect;i+=nsub_sect,j++,k+=mm) {
      if (struct_symb[i]!=NULL_TONE) {

	pilot_corr=cpx_mul(correc[j],pilot_rec[k]);
	pilot_rec[k]=pilot_corr;

	// compare bits with reference 
	if(struct_symb[i]==DATA_TONE){
	  // not a pilot or the DC tone
	  if (mode!=CONV_OFDM) {
	    // for green mode: divide data by pn_seq1+j*pn_seq2
	    pn_seq_comb.r=(double)pn_seq1[tone_nb+nb_data_tone*(num_section1)];
	    pn_seq_comb.i=(double)pn_seq2[tone_nb+nb_data_tone*(num_section2)];
	    if (1) printf("%f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);
	    pilot_rec[k]=cpx_div(pilot_rec[k],pn_seq_comb);
	    if (1) printf("&& %f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);	  
	  }
#ifdef LINUX
	  if (flag_plot) fprintf(dump_in_mod,"%f %F\n",pilot_rec[k].r,pilot_rec[k].i);
#endif
	  if (DEB_DMOD) printf("corrected %d: %f %f |r %f %f |c %f %f",k,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);

	  bits=demap_tone(pilot_rec[k],constel,&dist);
	  totdist+=dist;
	  if(dist>maxdist) maxdist=dist;
	  bitsref=demap_tone(ref[k],constel,&dist);
	  if (0) printf("\t%d %d\n",bits,bitsref);
	  ndiff=0;
	  compare_char(bits,bitsref,&ndiff);
	  nbiterr+=ndiff;
	  if (DEB_DMOD){
	    if (ndiff) {printf("$$$$$$$$$$$$$$$$\n");}
	    else  printf("\n");
	  }
	  tone_nb++;
	} else if (DEB_DMOD) 
	  if (struct_symb[i]==PILOT_TONE) {
	    printf("pilot %d: %f %f |r %f %f |c %f %f\n",k,pilot_rec[k].r,pilot_rec[k].i,pilot_seq[npil].r,pilot_seq[npil].i,correc[j].r,correc[j].i);
	    npil++;
	  }
	  else  printf("sigtone %d: %f %f |r %f %f |c %f %f\n",k,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);
      }
    }   
      
    *diffnb=nbiterr;

    printf("finished demod: nb error=%d\n",nbiterr);
    //if (flag_plot) plot_qam(dump_in_mod,nfft*nsub_sect,pilot_rec);
    *maxd=maxdist;

    return totdist;

} // end of dump_tones

// partition into 2 subsets
void partition(double val, double *av1,int *n01,  double *av2, int *n02) {

  double av1_i,av2_i;
  int n1_i,n2_i;

  n1_i=*n01;
  av1_i=*av1;
  n2_i=*n02;
  av2_i=*av2;


  if(abs(val-av1_i)<abs(val-av2_i)) {
    *av1=n1_i*av1_i/(float)(n1_i+1)+val/(float)(n1_i+1);
    *n01=n1_i+1;
  } else {
    *av2=n2_i*av2_i/(float)(n2_i+1)+val/(float)(n2_i+1);
    *n02=n2_i+1;
  }

}// end of partition

// compute correction factors on the training sequence
void calc_correction_coeffs(struct complex_f *pilot_rec,struct complex_f *pilot_ref,struct complex_f *correc) {

  int i,j,k,trnd;
  double mod;

  if(0 && (pilot_ref==NULL)) {
    for(i=0;i<nfft;i++)printf("!!!!tones %d  %f %f\n",i,pilot_rec[i].r,pilot_rec[i].i);

    //return;
  }

  for(i=0;i<nfft;i++) {correc[i].r=0.0;correc[i].i=0.0;}

  // compute correction factor  for channel compensation and coarse timing estimation error
  // invese of (a+jb is (a-jb)/(a2+b2)
  for(i=(null_tones>>1);i<nfft-(null_tones>>1);i++) {
    mod=pilot_rec[i].r*pilot_rec[i].r+pilot_rec[i].i*pilot_rec[i].i;
    correc[i].r=(pilot_rec[i].r*pilot_ref[i].r+pilot_rec[i].i*pilot_ref[i].i)/mod;
    correc[i].i=(pilot_rec[i].r*pilot_ref[i].i-pilot_rec[i].i*pilot_ref[i].r)/mod;
  }

  if (1) {
  printf("=========== DUMP CORRECTED TONES (training)\n");
  for(i=0;i<nfft;i++)
    printf("tones %d  %f %f | %f %f | %f %f\n",i,pilot_rec[i].r*correc[i].r-pilot_rec[i].i*correc[i].i,pilot_rec[i].r*correc[i].i+pilot_rec[i].i*correc[i].r,pilot_rec[i].r,pilot_rec[i].i,correc[i].r,correc[i].i);
  }


} // end of calc_correction_coeffs

void substr_offset(struct complex_f *in_data,float av_re,float av_im,int nb){

  int i;
  for(i=0;i<nb;i++) {
    in_data[i].r-=av_re;
    in_data[i].i-=av_im;
  }

}// end of substr_offset

//symetrical rounding (prevent offset)
void gen_fixed_point_sym(struct complex_f *in_data,struct complex_f *out_data) {

 //  1.0 => 1<<11 = 2048
  {
    // check here that max fixed point value is not reached
    if (0) assert(fabs((*in_data).r)<1.0 && fabs((*in_data).i)<1.0 );

    if ((*in_data).r>=0) 
      (*out_data).r=(int) floor(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    else 
      (*out_data).r=(int) ceil(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));

    if ((*in_data).i>=0) 
      (*out_data).i=(int) floor(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    else 
      (*out_data).i=(int) ceil(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));

 

  }

}// end of gen_fixed_point_sym

void gen_fixed_point(struct complex_f *in_data,struct complex_f *out_data) {

 //  1.0 => 1<<11 = 2048
  {
    // check here that max fixed point value is not reached
    if (0) assert(fabs((*in_data).r)<1.0 && fabs((*in_data).i)<1.0 );

    (*out_data).r=(int) floor(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    (*out_data).i=(int) floor(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    //fprintf(sigfile,"%d, %d\n",cosfp,sinfp);

  }

}// end of gen_fixed_point

void green_ofdm_mod_pp(struct complex_f *xn1,struct complex_f *xn2) {
  // this function performs green modulation of the conventional OFDM
  // this function adds the cyclic prefix to the output symbol
  int i,j;
  struct complex_f x_int1,x_int2,x_int3,a;


  for (i=ncp,j=0;i<nfft1*nsub_sect+ncp;i++,j++) {
    //sn0=(xn1(1:nfft1)+xn1(1+nfft1:nfft2)+xn1(1+nfft2:nfft3)+xn1(1+nfft3:nfft))/4;
    
    xn2[i].r=(xn1[j].r+xn1[nfft1*nsub_sect+j].r+xn1[nfft2*nsub_sect+j].r+xn1[nfft3*nsub_sect+j].r);
    xn2[i].i=(xn1[j].i+xn1[nfft1*nsub_sect+j].i+xn1[nfft2*nsub_sect+j].i+xn1[nfft3*nsub_sect+j].i);

      x_int1.r=4.0*xn1[nfft1*nsub_sect+j].r-xn2[i].r;
      x_int2.r=4.0*xn1[nfft2*nsub_sect+j].r-xn2[i].r;
      x_int3.r=4.0*xn1[nfft3*nsub_sect+j].r-xn2[i].r;

      x_int1.i=4.0*xn1[nfft1*nsub_sect+j].i-xn2[i].i;
      x_int2.i=4.0*xn1[nfft2*nsub_sect+j].i-xn2[i].i;
      x_int3.i=4.0*xn1[nfft3*nsub_sect+j].i-xn2[i].i;

      // sn1=((-1-%i).*x_int1-2.*x_int2+(-1+%i).*x_int3)/4;
      // sn2=(-2.*x_int1-2.*x_int3)/4;
      // sn3=((-1+%i).*x_int1-2.*x_int2-(1+%i).*x_int3)/4;

       xn2[nfft1*nsub_sect+i].r=(-x_int1.r+x_int1.i-2.*x_int2.r-x_int3.r-x_int3.i)/8.0;
       xn2[nfft2*nsub_sect+i].r=(-2.*x_int1.r-2.*x_int3.r)/8.0;
       xn2[nfft3*nsub_sect+i].r=(-x_int1.r-x_int1.i-2.*x_int2.r-x_int3.r+x_int3.i)/8.0;
 
       xn2[nfft1*nsub_sect+i].i=(-x_int1.i-x_int1.r-2.*x_int2.i-x_int3.i+x_int3.r)/8.0;
       xn2[nfft2*nsub_sect+i].i=(-2.*x_int1.i-2.*x_int3.i)/8.0;
       xn2[nfft3*nsub_sect+i].i=(-x_int1.i+x_int1.r-2.*x_int2.i-x_int3.i-x_int3.r)/8.0;

       xn2[i].r=xn2[i].r/2.0;
       xn2[i].i=xn2[i].i/2.0;


       //S(1:N)=0;
       //size(S);
       //S(1:N1)=sn0;
       //S(1+N1:N2)=%i*sn1;
       //S(1+N2:N3)=sn2;
       //S(1+N3:N)=%i*sn3;
       /* 
       a.r= xn2[N1+i].r;
       xn2[N1+i].r=-xn2[N1+i].i;
       xn2[N1+i].i= a.r;
       a.r= xn2[N3+i].r;
       xn2[N3+i].r=-xn2[N3+i].i;
       xn2[N3+i].i= a.r;
       */
       // printf("%d xn1: %f %f %f %f\n",i,xn1[j].r,xn1[N1+j].r,xn1[N2+j].r,xn1[N3+j].r );
       //printf("   xn2: %f %f %f %f\n",i,xn2[i].r,xn2[N1+i].r,xn2[N2+i].r,xn2[N3+i].r );
      }

  // copy the cyclic prefix part
  //for(i=0;i<ncp;i++) xn2[i]=xn2[N1+i];

} // function green_ofdm_mod_pp

void green_ofdm_dmod(struct complex_f *Rr,struct complex_f *Vn) {
  // suppress the cyclic prefix
  int k,j;

  for (k=0,j=ncp;k<nfft1*nsub_sect;k++,j++) {
    //vn0(1:nfft1)=Rr(1:nfft1)+Rr(1+nfft1:nfft2)+Rr(1+nfft2:nfft3)+Rr(1+nfft3:nfft);
    //vn1=Rr(1:nfft1)+%i*Rr(1+nfft1:nfft2)-Rr(1+nfft2:nfft3)-%i*Rr(1+nfft3:nfft);
    //vn2=Rr(1:nfft1)-Rr(1+nfft1:nfft2)+Rr(1+nfft2:nfft3)-Rr(1+nfft3:nfft);
    //vn3=Rr(1:nfft1)-%i*Rr(1+nfft1:nfft2)-Rr(1+nfft2:nfft3)+%i*Rr(1+nfft3:nfft);

     
    Vn[k].r=(Rr[j].r+Rr[nfft1+j].r+Rr[nfft2+j].r+Rr[nfft3+j].r)/1.0;
    Vn[k].i=(Rr[j].i+Rr[nfft1+j].i+Rr[nfft2+j].i+Rr[nfft3+j].i)/1.0;
    
    
    Vn[nfft1*nsub_sect+k].r=(Rr[j].r-Rr[nfft1+j].i-Rr[nfft2+j].r+Rr[nfft3+j].i)/1.0;
    Vn[nfft1*nsub_sect+k].i=(Rr[j].i+Rr[nfft1+j].r-Rr[nfft2+j].i-Rr[nfft3+j].r)/1.0;
    
    Vn[nfft2*nsub_sect+k].r=(Rr[j].r-Rr[nfft1+j].r+Rr[nfft2+j].r-Rr[nfft3+j].r)/1.0;
    Vn[nfft2*nsub_sect+k].i=(Rr[j].i-Rr[nfft1+j].i+Rr[nfft2+j].i-Rr[nfft3+j].i)/1.0;
    
    Vn[nfft3*nsub_sect+k].r=(Rr[j].r+Rr[nfft1+j].i-Rr[nfft2+j].r-Rr[nfft3+j].i)/1.0;
    Vn[nfft3*nsub_sect+k].i=(Rr[j].i-Rr[nfft1+j].r-Rr[nfft2+j].i+Rr[nfft3+j].r)/1.0;
    
    //printf("-- %d %f %f %f %f\n",k,Vn[k].r,Vn[nfft1+k].r,Vn[nfft2+k].r,Vn[nfft3+k].r);
    //printf(">> %d %f %f %f %f\n",k,Rr[j].r,Rr[nfft1+j].r,Rr[nfft2+j].r,Rr[nfft3+j].r);
 

      }
} // function green_ofdm_dmod

void map_tone(unsigned char bits,struct complex_f *tone,lconstell constell){

  int nbits;

  assert(bits<(1<<NBITSCONST[constell]));

  if (constell==QAM16C) {
    nbits=4;
    //  printf("bits=%d\n",bits);
    (*tone).r=NORM_QAM16*QAM16[bits].r;
    (*tone).i=NORM_QAM16*QAM16[bits].i;
  }
  else if(constell=QPSKC) {
    nbits=2;
    //  printf("bits=%d\n",bits);
    (*tone).r=NORM_QPSK*QPSK[bits].r;
    (*tone).i=NORM_QPSK*QPSK[bits].i;
  }  else if(constell=BPSKC) {
    nbits=1;
    //  printf("bits=%d\n",bits);
    (*tone).r=NORM_BPSK*BPSK[bits].r;
    (*tone).i=NORM_BPSK*BPSK[bits].i;
  } else { printf("no support for this constellation\n");exit(0);}

}// end of map_tones

void calc_nbit_const(){
  int i;

  NBITSCONST[QAM16C]=4;
  NBITSCONST[QPSKC]=2;
  NBITSCONST[BPSKC]=1;

  //for(i=0;i<3;i++) printf("i %d nb %d\n",i,NBITSCONST[i]);

} // end of calc_nbit_const

void  map_bits_to_tones(char *bitstream, struct complex_f* symbol, char *last, int *indxbs, lconstell constel, int num_sect,char *symb_struct,char* pn_seq,mode_ofdm mode,int ndata,char flag_conj_pnseq) {

  int i,j,k=0,l=0,togpil=0,npil=0;
  int nn;
  int irnd;
  int nbit_i,lastpos;
  unsigned char mask,bits;
  int sig_bits;
  int mm;
  double tmp;
  char first_sig_tone=1,second_sig_tone=0;
  int num_sect_symb=0,nsub_sect_symb,nb_sigtones_mapped=0;
  lastpos=*last;
  nbit_i=0;
  sig_bits=num_sect;

  mask=0x0;
  for(i=0;i<NBITSCONST[constel];i++) mask|=(1<<i);

  printf("mask=%d\n",mask);

  if(OVSMPL) nn=nfft/2; else nn=nfft;

  mm=1;
  num_sect_symb=0;
  nsub_sect_symb=1;

  if (mode==GREEN_GREEN) {
    mm=nsub_sect;
    num_sect_symb=num_sect;
    nsub_sect_symb=nsub_sect;
  }
  if (mode==GREEN_CONV) {
    nsub_sect_symb=1;
    num_sect_symb=0;
  }
  // pilot structure + + + + - - - - 
  //                 0 1 2 3 4 5 6 7 
  // s0: 0,4,8,12,...   s1 1,5,9,13,...

  //scatters pilot every 4 tones
  for(i=0;i<nfft;i++) {
    
    printf("%d %d\n",i*nsub_sect+num_sect,symb_struct[i*nsub_sect+num_sect]);
    // check no pilot is mapped on DC when spectrum centered on 0
    // if spectrum not centered, then DC is tone 0 and then check that it is a null tone
    //assert((CENTER_SPECTRUM_0 && !(!(j%5) && (i>=(nfft/2-1)) && (i<=(nfft/2-1))))  || (!CENTER_SPECTRUM_0 && (null_tones>2)));
    
    if ( symb_struct[i*nsub_sect+num_sect]==DATA_TONE) { 
      // map data on this tone
      bits=(bitstream[l]>>lastpos)&mask;
      lastpos+=NBITSCONST[constel];
      if(lastpos>=SIZEOF_BYTE) {
	bits|=(bitstream[l+1]<<(SIZEOF_BYTE-(lastpos-NBITSCONST[constel])));
	lastpos-=SIZEOF_BYTE;
	bits&=mask;
	l++;
      }
      if (0) printf("i %d bits %d lastpos %d l %d\n",i,bits,lastpos,l);

      //if (0) map_tone((bitstream[k]>>(4*tog))&(0xf),symbol+i,QAM16C); // reads 4 bits from bitstream to map on the QAM16
      if (1) {
	map_tone(bits,symbol+i*nsub_sect_symb+num_sect_symb,constel); // map bits to tone
	if (mode!=CONV_OFDM) {
	  // modulate by PN sequence
	  if (!flag_conj_pnseq) {
	    symbol[i*nsub_sect_symb+num_sect_symb].r=pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].r;
	    symbol[i*nsub_sect_symb+num_sect_symb].i=pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].i;
	  } else { // mult by j*pn_seq
	    tmp=symbol[i*nsub_sect_symb+num_sect_symb].r;
	    symbol[i*nsub_sect_symb+num_sect_symb].r=-pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].i;
	    symbol[i*nsub_sect_symb+num_sect_symb].i=pn_seq[k+num_sect*ndata]*tmp;
	  }
	} 
      }
      if (1) printf("xxxx%d %d %f %f k %d pn %d bits %x\n",num_sect,i*nsub_sect_symb+num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r,symbol[i*nsub_sect_symb+num_sect_symb].i,k,pn_seq[k+num_sect*ndata],bits);
      k++;    
    } 
    else if(symb_struct[i*nsub_sect+num_sect]==NULL_TONE) {
      // map null tone at DC
      symbol[i*nsub_sect_symb+num_sect_symb].r=0.0;symbol[i*nsub_sect_symb+num_sect_symb].i=0.0;
    }
    else if (symb_struct[i*nsub_sect+num_sect]==PILOT_TONE){ 
      //this is a pilot
      //if (!(i%4)) { //s0
      //	symbol[i].r=pilot[0].r;
      //	symbol[i].i=pilot[0].i;
      //}
      //if (i%4) {
      //irnd=rand()&0x1;
      //printf("#### %d\n",irnd);
      togpil++;
      togpil=togpil%2;
      irnd=(togpil>=1)?0:1;
      // modulate pilots with pnseq to reduce the PAPR: pilot modulation is independent of section number
	  if (!flag_conj_pnseq) {
	    symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
	    symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*pilot[0].i*pn_seq[npil];
	  } else {
	    symbol[i*nsub_sect_symb+num_sect_symb].r =0.0;
	    symbol[i*nsub_sect_symb+num_sect_symb].i =0.0;

	    //symbol[i*mm+num_sect].r = -NORM_QPSK*pilot[0].i*pn_seq[npil];
	    //symbol[i*mm+num_sect].i =  NORM_QPSK*pilot[0].r*pn_seq[npil];
	  }	
      //symbol[i*mm+num_sect].r=0.0;
      //symbol[i*mm+num_sect].i=0.0;	
      if (1) printf("xxxxpil %d %f %f\n",i*nsub_sect_symb+num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r,symbol[i*nsub_sect_symb+num_sect_symb].i);
      npil++;
      //}
    }
    else if(symb_struct[i*nsub_sect+num_sect]==SIG_TONE) {
      // coding of side information
      // for "left" section (flag_conj_pnseq=0) code section number on 1st tone, for "right" section code on right tone
      /*
	  if (!flag_conj_pnseq && first_sig_tone) {
	    symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	    symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	  } else 
	    if (flag_conj_pnseq && second_sig_tone) {
	      symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	      symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	    } else {
	      symbol[i*nsub_sect_symb+num_sect_symb].r=0.0;
	      symbol[i*nsub_sect_symb+num_sect_symb].i=0.0;
	    }
      */
      // sig tones generated for right / left symbols so that they can be added wwithout interfering (if sigtone on left then null on right)
      printf("numsec %d\n",sig_bits);
      if (!flag_conj_pnseq && ( nb_sigtones_mapped<(nb_sigtones/2)) ) {
	symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	// QPSK mod of sig tones => shift by 2
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      } 
      else if (flag_conj_pnseq && ( nb_sigtones_mapped>=(nb_sigtones/2))) {
	symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      } else {
	symbol[i*nsub_sect_symb+num_sect_symb].r=0.0;
	symbol[i*nsub_sect_symb+num_sect_symb].i=0.0;
	nb_sigtones_mapped++;
      }

      if (1) 
        printf("xxxxsig %d(%d) -- numsect=%d(%d) => %f %f (%d %d)\n", i*nsub_sect_symb+num_sect_symb, flag_conj_pnseq,
                sig_bits&0x3, num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r, symbol[i*nsub_sect_symb+num_sect_symb].i,
                first_sig_tone,second_sig_tone);
      
      if (second_sig_tone) {second_sig_tone=0;}
      if (first_sig_tone) {first_sig_tone=0;second_sig_tone=1;}
      

    }
    else
      printf("WARNING(map_bits_to_tones: tone %d unassigned\n",i);
  }
  
  *last=lastpos;
  *indxbs=l;

  printf("finished mapping %d tones and %d pilots\n",k,npil);

} // end of map_bits_to_tones

#ifdef LINUX
void dump_csv(FILE *dumpfile, int Npoint, struct complex_f *data){
  int i;

  for (i=0; i < Npoint; i++) {
    fprintf(dumpfile, "%d, %d\n", (int)data[i].r, (int)data[i].i);
  }
}// end of dump_csv
#else
struct complex_f* dump_csv(struct complex_f *p_buf, int Npoint, struct complex_f *data){
  int i;
    for (i=0; i < Npoint; i++) {
	//p_buf[i] = (double) data[i];	
	p_buf[i].r = (double) data[i].r;	
	p_buf[i].i = (double) data[i].i;	
	
  }
  return p_buf + Npoint;
}// end of dump_csv
#endif

void plot_qam(int Npoint,struct complex_f *data){
  int i;

  if (0) for (i=(null_tones>>1);i<Npoint-(null_tones>>1);i+=4) if (CENTER_SPECTRUM_0&&((i<nfft/2-ZTONE_RAD_L)||(i>nfft/2+ZTONE_RAD_R)) || !CENTER_SPECTRUM_0) printf("dumpfile:\n%f %f\n",data[i].r,data[i].i);
    for (i=(null_tones>>1);i<Npoint-(null_tones>>1);i+=4)  printf("dumpfile:\n%f %f\n",data[i].r,data[i].i);

}
void plot(int Npoint,struct complex_f *data){
  int i;

  for (i=0;i<Npoint;i++) printf("dumpfile:\n%f %f\n",data[i].r,data[i].i);

}
 void plot_double(int Npoint,double *data){
  int i;

  for (i=0;i<Npoint;i++) printf("dumpfile\n%f\n",data[i]);

}   
 void plot_cpxf(int Npoint, struct complex_f *data){
  int i;

  for (i=0;i<Npoint;i++) printf("dumpfile\n%f %f\n",data[i].r,data[i].i);

}   


// skips data in csv format
#ifdef LINUX
int skip_csv(int nb_data,FILE* inputfile) {

  int i,nbdata_read=0;
  float dr,di;

  for (i=0;i<nb_data;i++) {
    if (fscanf(inputfile,"%f,%f",&dr,&di)!=2) break;
    nbdata_read++;
  }

  return(nbdata_read);

}// end of skip_csv
#else
struct complex_f* skip_csv(int nb_data, struct complex_f * input_buf) {
  return (input_buf + nb_data);
}// end of skip_csv

#endif

// reads data in csv format
#ifdef LINUX
int fread_csv(struct complex_f* indata, int nb_data, FILE* inputfile) {

  int i, nbdata_read = 0;
  float dr, di;

  for (i=0; i < nb_data; i++) {
    if (fscanf(inputfile, "%f,%f", &dr, &di) != 2) break;
    indata[i].r = (double) dr;
    indata[i].i = (double) di;
    if (0) printf("%f %f\n", dr, di);
    nbdata_read++;
  }

  return (nbdata_read);

}// end of fread_csv
#else
struct complex_f* fread_csv(struct complex_f *indata, int nb_data, struct complex_f *p_buf) {
  int i;

  for (i=0; i < nb_data; i++) {
    indata[i] = p_buf[i];
    printf("indata %d %f %f\n",i,indata[i].r,indata[i].i);
    //if (0) printf("%f %f\n", dr, di);
  }

  return (p_buf + nb_data);
}// end of fread_csv

#endif

void conv_float2double(struct complex_f *in_float,struct complex_f *out_double,int nb){

  int i;
  for(i=0;i<nb;i++) {
    out_double[i].r=(double) in_float[i].r;
    out_double[i].i=(double) in_float[i].i;
    printf("@@@@ %f %f\n",in_float[i].r,in_float[i].i);
  }
} // end of conv_float2double

// shift spectrum by nfft/2 units (centered aon 0)
void shift_spectrum(struct complex_f *insig, int nb, float k){
  int i;
  float cr,ci;

  for(i=0;i<nb;i++) {
    printf("%f\n",PI);
    printf("%f\n", cos(1.5));
    printf("%f\n", cos(PI));	/*  */
    printf("%f\n",i*PI);
    printf("%f\n", cos((float)i*PI));	/*  */
     printf("%f\n", cos((float)i*k*PI));	/*  */
   
    printf("%d %f %f %f\n",i,insig[i].r,insig[i].i,cos(i*k*PI));
    cr = insig[i].r*cos(i*k*PI) - insig[i].i*sin(i*k*PI);
    ci = insig[i].i*cos(i*k*PI) + insig[i].r*sin(i*k*PI);
    insig[i].r = cr;
    insig[i].i = ci;
  }

}// end shift_spectrum 

void print_usage() {
  
  printf("options list\n");
  printf("\t -g  : digital gain value (prior to DA input)\n");
  printf("\t -M : green mode [%d %d] (0:conventional,1:SLM green,2:SLM_conv)\n",0,2);
  printf("\t -n  : fft size [%d %d] (%d)\n",N_MIN,N_MAX,NFFT);
  printf("\t -p : cyclic prefix size [%d %d] (%d)\n",NCP_MIN,NCP_MAX,NCP);
  printf("\t -t : nb of nul tones [%d %d] must be even\n",0,NFFT);
  printf("\t -d  : demodulation [0 1] \n");
  printf("\t -c  : QAM type (0=> BPSK 1=>QPSK 2=>QAM16) \n");
  printf("\t -b  : backoff value [%d %d] (%d)\n",MINBACKOFF,MAXBACKOFF,DEF_BACKOFF_INDX);
  printf("\t -h  : print usage\n");
  exit(0);

}// end of print usage

void parse_options(int argc, char *argv[], int *backoff,float *gain, mode_ofdm *mode, char *dmod,
		           int *nfft_v,int *ncp_v, int *null_tones_v, int *nsub_sect_v, char* constell_v) {

  int i;
  char error=0, backoff_flag=0, gain_flag=0, mode_flag=0, dmod_flag=0, char_mode;
  *mode=DEFAULT;

  for(i=1; i < argc; i++) {
    if (!strcmp(argv[i],"-b")) {
      if (i+1 < argc) {
    	  *backoff = atoi(argv[i+1]);
	printf("backoff set to %d. ", atoi(argv[i+1]));
    	  backoff_flag = 1; i++;
      }
      else {error = 1; break;}
    }
    else if (!strcmp(argv[i],"-g")) {
      if (i+1<argc) {
    	  *gain=atof(argv[i+1]);
	printf("gain set to %f. ", atof(argv[i+1]));
    	  gain_flag=1;i++;
      }
      else { error = 2; break; }
    }  
    else if (!strcmp(argv[i],"-M")) {
      if (i+1<argc) {
    	  char_mode=atoi(argv[i+1]);
    	  if (char_mode == -1) *mode = DEFAULT;
		  else if (char_mode == 0) *mode = CONV_OFDM;
		  else if (char_mode == 1) *mode = GREEN_GREEN;
		  else if (char_mode == 2) *mode = GREEN_CONV;
	printf("Mode set to %d. ", atoi(argv[i+1]));
	      mode_flag=1;i++;
      }
      else {error = 3; break;}
    }  
    else if (!strcmp(argv[i],"-d")) {
      if (i+1<argc) {
    	  *dmod=atoi(argv[i+1]);
    	  dmod_flag=1;i++;
      }
      else { error = 4; break; }
    } 
    else if (!strcmp(argv[i],"-n")) {
    		if (i+1<argc) {
    			*nfft_v=atoi(argv[i+1]);
    			i++;
    		}
    		else  { error = 5; break;}
    } 
    else if (!strcmp(argv[i],"-p")) {
      if (i+1 < argc) {
    	  *ncp_v=atoi(argv[i+1]);
    	  i++;
      }
      else  { error = 6; break; }
    }
    else if (!strcmp(argv[i],"-t")) {
      if (i+1 < argc) {
    	  *null_tones_v=atoi(argv[i+1]);
    	  i++;
      }
      else  { error = 7; break; }
    } 
    else if (!strcmp(argv[i],"-m")) {
      if (i+1 < argc) {
    	  *nsub_sect_v = atoi(argv[i+1]);
    	  i++;
      }
      else  { error = 8; break; }
    }
    else if (!strcmp(argv[i],"-c")) {
      if ( i+1 <argc) {
	*constell_v=atoi(argv[i+1]);
	i++;
      }
      else  { error = 9; break;}
    }
    else if (!strcmp(argv[i],"-a")) {
		;
	}
    else if (!strcmp(argv[i],"-N")) {
		;
	}
    else if (!strcmp(argv[i],"-h") ||  !strcmp(argv[i],"--help")) {
      print_usage();
      }
      else  { error = 100; break; }
    //fprintf(stderr,"bad option in command line %s\n",argv[i]);
    //exit(0);
  }
#ifdef LINUX  
  if (error) {fprintf(stderr,"error in command line options, error=%d\n", error);exit(0);}
#else
  if (error) {printf("error in command line options, error=%d\n", error);exit(0);}
#endif

  printf("ncon %d\n",*constell_v);

  //return 0; //added to avoid compile issue on zynq => -fpermissise used instead
}// end of parse_options


void calc_mean_variance(struct complex_f *indat, int ndat, double *offr, double *offi, double *var){

  int i;
  double offi_r,offi_i,var_i,sigma;

  assert(ndat!=0);

  // mean calculation
  offi_r=0.0;
  offi_i=0.0;

  for (i=0;i<ndat;i++) {

    offi_r+=indat[i].r;
    offi_i+=indat[i].i;

  }

  offi_r/=(float) ndat;
  offi_i/=(float) ndat;

  // variance calculation
  sigma=0.0;
  for (i=0;i<ndat;i++) {
    sigma+=(offi_r-indat[i].r)*(offi_r-indat[i].r)+(offi_i-indat[i].i)*(offi_i-indat[i].i);
  }

  sigma/=(float) ndat;

  sigma=sqrt(sigma);

  *var=sigma;
  *offr=offi_r;
  *offi=offi_i;

}// end of calc_mean_variance

#ifndef LINUX
int demod_symbol(struct complex_f *in_dmod_buff, struct complex_f *tx_ref, int symbol_nb, int backoff_v, char flag_iter,
		struct complex_f *out_dmod, struct complex_f * corr_facts, struct complex_f *out_fft, char mode_v, int *bckoffmin,
		char *struct_symb, int ndata_tones, char *pn_seq, char *pn_seq2) {

  int i,j;
  float distsymb_min, tot_distsymb, distsymb;
  int ndiffmin,ndiffnb;
  int lasti;
  double av_re,av_im,sigma;
  int bckoffminc;
//#ifdef LINUX
//  fftw_plan p;
//#endif
  double maxd;
  int num_section;

  bckoffminc = *bckoffmin;


  // normalize
  norm_v(in_dmod_buff,fft_norm_v,(nfft+ncp+2*backoff_v));

  // symbol demodulation
  ndiffmin = BIG_INT;
  distsymb_min = (float) BIG_INT;
  tot_distsymb = 0.0;

  lasti=(flag_iter)?(backoff_v):0;

  if (flag_iter)
    for (i=0;i<=2*lasti;i++) {
      //plot(dumpout,nfft+ncp,in_dmod_buff+i);

      calc_mean_variance(in_dmod_buff+i+ncp,nfft,&av_re,&av_im,&sigma);
      //substr_offset(in_dmod_buff+i+ncp,av_re,av_im,nfft);
      printf("333for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);

      if (0&&mode_v) {
	printf("do green dmod\n");
	green_ofdm_dmod(in_dmod_buff+i,out_dmod);
      }
      else {
	memcpy(out_dmod,in_dmod_buff+ncp+i,nfft*sizeof(struct complex_f));
	norm_v(out_dmod,2.0,nfft);
      }
      //if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod);

      if (1) {
    	  if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect,1.0);

    	  if (1) for(j=0;j<nfft*nsub_sect;j++) printf("rrr66 %d %d %f %f\n",i,j,out_dmod[j].r,out_dmod[j].i);
//#ifdef LINUX
//	p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex_f *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
//	fftw_execute(p);
//#endif
      }
	  //TODO replace& the id by a FFT
      printf("idfft1\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);

//#ifdef LINUX
      distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,i,corr_facts,1,&ndiffnb,1,constell,&maxd,struct_symb,mode_v,ndata_tones,pn_seq,pn_seq2);
//#endif
      // search minimize max dist
      if (maxd<distsymb_min) {distsymb_min=maxd;bckoffminc=i;}

      //if (0&&(ndiffnb<ndiffmin)) {ndiffmin=ndiffnb;bckoffminc=i;}

      printf("for backoff index %d err is %d dist=%f max %f\n",i,ndiffnb,distsymb,maxd);

    }

  if (SEARCH_START) printf("min backoff index is %d distmin=%f\n",bckoffminc,distsymb_min);

  // final modulation
  calc_mean_variance(in_dmod_buff+bckoffminc+ncp,nfft,&av_re,&av_im,&sigma);
  //substr_offset(in_dmod_buff+bckoffminc+ncp,av_re,av_im,nfft);
  printf("444for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);

  if (0&&mode_v){
    printf("do green dmod\n");
    green_ofdm_dmod(in_dmod_buff+bckoffminc,out_dmod);
    //norm(in_dmod_buff,2.0);
  }
  else {
    memcpy(out_dmod,in_dmod_buff+ncp+bckoffminc,nfft*sizeof(struct complex_f));
    norm_v(out_dmod,2.0,nfft);
  }
  if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect,1);

  if (0) for(j=0;j<nfft;j++) printf("rrr67 %d %d %f %f\n",bckoffminc,j,out_dmod[j].r,out_dmod[j].i);

  if (mode_v==GREEN_GREEN) {
//#ifdef LINUX
//    p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
	  //TODO replace id by fft
	  printf("idfft2\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);
  }
//#endif
  else
//#ifdef LINUX
   //p = fftw_plan_dft_1d(nfft, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
	  //TODO replace id by fft
	  printf("idfft3\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);
  //fftw_execute(p);
//#endif
  distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,bckoffminc,corr_facts,1,&ndiffnb,1,constell,&maxd,struct_symb,mode_v,ndata_tones,pn_seq,pn_seq2);
  tot_distsymb+=distsymb;

  printf("==>for backoff index %d err is %d dist=%f maxdistance=%f\n",bckoffminc,ndiffnb,distsymb,maxd);
  *bckoffmin=bckoffminc;
  return ndiffnb;

} // end of demod_symbol
#endif

#ifdef LINUX
int demod_symbol(struct complex_f *in_dmod_buff, struct complex_f *tx_ref, int symbol_nb, int backoff_v, char flag_iter,
		struct complex_f *out_dmod, struct complex_f * corr_facts, fftw_complex_f *out_fft, char mode_v, int *bckoffmin,
		char *struct_symb, int ndata_tones, char *pn_seq, char *pn_seq2) {
//#endif

  int i,j;
  float distsymb_min, tot_distsymb, distsymb;
  int ndiffmin,ndiffnb;
  int lasti;
  double av_re,av_im,sigma;
  int bckoffminc;
//#ifdef LINUX
  fftw_plan p;
//#endif
  double maxd;
  int num_section;

  bckoffminc = *bckoffmin;

  
  // normalize 
  norm_v(in_dmod_buff,fft_norm_v,(nfft+ncp+2*backoff_v));
  
  // symbol demodulation
  ndiffmin = BIG_INT;
  distsymb_min = (float) BIG_INT;
  tot_distsymb = 0.0;
  
  lasti=(flag_iter)?(backoff_v):0;
  
  if (flag_iter) 
    for (i=0;i<=2*lasti;i++) {
      //plot(dumpout,nfft+ncp,in_dmod_buff+i);
      
      calc_mean_variance(in_dmod_buff+i+ncp,nfft,&av_re,&av_im,&sigma);
      //substr_offset(in_dmod_buff+i+ncp,av_re,av_im,nfft);	      
      printf("333for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);
      
      if (0&&mode_v) {
	printf("do green dmod\n");
	green_ofdm_dmod(in_dmod_buff+i,out_dmod);
      }
      else {
	memcpy(out_dmod,in_dmod_buff+ncp+i,nfft*sizeof(struct complex_f));
	norm_v(out_dmod,2.0,nfft);
      }
      //if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod);
      
      if (1) {
	if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect,1.0);
      
	if (1) for(j=0;j<nfft*nsub_sect;j++) printf("rrr66 %d %d %f %f\n",i,j,out_dmod[j].r,out_dmod[j].i);
//#ifdef LINUX
//	p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex_f *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
//	fftw_execute(p);
	  //TODO replace the id by a FFT
		printf("idfft4\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);
//#endif
      } 
  
//#ifdef LINUX
      distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,i,corr_facts,1,&ndiffnb,1,constell,&maxd,struct_symb,mode_v,ndata_tones,pn_seq,pn_seq2);
//#endif
      // search minimize max dist
      if (maxd<distsymb_min) {distsymb_min=maxd;bckoffminc=i;}
      
      //if (0&&(ndiffnb<ndiffmin)) {ndiffmin=ndiffnb;bckoffminc=i;}

      printf("for backoff index %d err is %d dist=%f max %f\n",i,ndiffnb,distsymb,maxd);
      
    }
  
  if (SEARCH_START) printf("min backoff index is %d distmin=%f\n",bckoffminc,distsymb_min);

  // final modulation
  calc_mean_variance(in_dmod_buff+bckoffminc+ncp,nfft,&av_re,&av_im,&sigma);
  //substr_offset(in_dmod_buff+bckoffminc+ncp,av_re,av_im,nfft);	      
  printf("444for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);

  if (0&&mode_v){
    printf("do green dmod\n");
    green_ofdm_dmod(in_dmod_buff+bckoffminc,out_dmod);
    //norm(in_dmod_buff,2.0);
  }
  else {
    memcpy(out_dmod,in_dmod_buff+ncp+bckoffminc,nfft*sizeof(struct complex_f));
    norm_v(out_dmod,2.0,nfft);
  }
  if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect,1);
  
  if (0) for(j=0;j<nfft;j++) printf("rrr67 %d %d %f %f\n",bckoffminc,j,out_dmod[j].r,out_dmod[j].i);
  
  if (mode_v==GREEN_GREEN) 
//#ifdef LINUX
    //p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
	  //TODO replace the id by a FFT
	  printf("idfft5\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);
//#endif
  else  
//#ifdef LINUX
   //p = fftw_plan_dft_1d(nfft, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
  //fftw_execute(p);
	  //TODO replace the id by a FFT
	  printf("idfft6\n");
	  id_fft(out_dmod, out_fft, nfft * nsub_sect);
//#endif
  distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,bckoffminc,corr_facts,1,&ndiffnb,1,constell,&maxd,struct_symb,mode_v,ndata_tones,pn_seq,pn_seq2);
  tot_distsymb+=distsymb;

  printf("==>for backoff index %d err is %d dist=%f maxdistance=%f\n",bckoffminc,ndiffnb,distsymb,maxd);
  *bckoffmin=bckoffminc;
  return ndiffnb;
  
} // end of demod_symbol
#endif

void detect_peak(struct complex_f *in_dmod,int *index_of_peak,int *last_peak_indx,  char *first_peak_found,char *found_peak,char mode_v) {

  int i,j,k;
  struct complex_f phi,gm;
  char not_a_max=0;
  double max_score=0.0,phase_for_max;
  double av_re,av_im,sigma;
  int index_of_peak_i;
  int last_peak_indx_i;
  char first_peak_found_i;
  char found_peak_i;
  double min_peak_val_v;

  index_of_peak_i=*index_of_peak;
  last_peak_indx_i=*last_peak_indx;
  first_peak_found_i=*first_peak_found;
  found_peak_i=*found_peak;

	assert(ncp!=0);

	//else sample_nb=tot_read-2*nfft+n1+2*RAD_SEARCH_PEAK;
	// calculation of expected min correlation from ref levels of training seq autocorr
	calc_mean_variance(in_dmod,2*nfft+ncp,&av_re,&av_im,&sigma);
	sigma=(sigma>min_input_power_v)?sigma:min_input_power_v;      
	// compute timing (phi_sum) and freq offset (gm_sum)
	// phi_sul 0..2nfft-n1
	for (j=n1;j<2*nfft;j++) {
	  
	  if (0) printf("---%d ir %f ii %f\n",sample_nb+j-n1-RAD_SEARCH_PEAK,in_dmod[j-n1].r,in_dmod[j-n1].i);
	  phi.r=0;gm.r=0;gm.i=0;
	  for(i=0;i<ncp;i++){
	    
	    phi.r+=(in_dmod[j+i].r-av_re)*(in_dmod[j+i].r-av_re)+(in_dmod[j+i].i-av_im)*(in_dmod[j+i].i-av_im)+(in_dmod[j+i-n1].r-av_re)*(in_dmod[j+i-n1].r-av_re)+(in_dmod[j+i-n1].i-av_im)*(in_dmod[j+i-n1].i-av_im);
	    
	    gm.r+=((in_dmod[j+i].r-av_re)*(in_dmod[j+i-n1].r-av_re))+((in_dmod[j+i].i-av_im)*(in_dmod[j+i-n1].i-av_im));
							 
	    gm.i+=((in_dmod[j+i].r-av_re)*(in_dmod[j+i-n1].i-av_im))-((in_dmod[j+i].i-av_im)*(in_dmod[j+i-n1].r-av_re));
	  }
	  phi_sum[j-n1]=abs(sqrt(gm.r*gm.r+gm.i*gm.i)-(snr/(snr+1))*phi.r);
	  gm_sum[j-n1]= -atan(gm.i/gm.r)/(2.0*PI);
	  //printf("correlation score at %d : %f %f\n",j-n1,phi_sum[j-n1],gm_sum[j-n1]);
	}
	
	if (1) {plot_double(2*nfft-n1-RAD_SEARCH_PEAK,phi_sum);}
	
	if (first_peak_found_i) printf ("search first peak\n");
	else printf("search next peak\n");
	
	found_peak_i=0;
	//printf("scan buffer for maximum, sample nb:  %d\n",tot_read);
	// phisum is defined from 0 to 2N-N1
	
	if ((!FIND_FIRST_PEAK_ONLY) || (FIND_FIRST_PEAK_ONLY&&first_peak_found_i)){
	  for (i=RAD_SEARCH_PEAK;i<2*nfft-n1-RAD_SEARCH_PEAK;i++){ 

	    // average power on a buffer of size POW_BUFF_SIZE
	    av_pow-=powinst[indx_avpow]/(float)(POW_BUFF_SIZE);
	    powinst[indx_avpow]=phi_sum[i];
	    av_pow+=powinst[indx_avpow++]/(float)(POW_BUFF_SIZE);
	    if (indx_avpow>=POW_BUFF_SIZE) indx_avpow=0;


	    if (mode_v)
	      min_peak_val_v=(sigma/sigma_autocorr_ref[0])*(sigma/sigma_autocorr_ref[0])*(sigma_autocorr_ref[1]*0.6);
	    else 
	      min_peak_val_v=(sigma/sigma_autocorr_ref[0])*(sigma/sigma_autocorr_ref[0])*(sigma_autocorr_ref[1]*0.6);

	    //min_peak_val_v=MIN_PEAK_VAL;
	      
	      // slow decay of max score (allow detection of successive peaks in buffer)
	    max_score-=(max_score-av_pow)*0.05; 
	    if (1) {
	      printf("%%%%%% min peak val %f sigma %f\n",min_peak_val_v,sigma);
	      printf("smpl %d >> maxscore=%f avpow=%f phisum=%f av_max_val %f\n",sample_nb,max_score,av_pow,phi_sum[i],0.8*av_max_value);
	      printf("\t%f %f\n",in_dmod[i].r,in_dmod[i].i);
	    }
	    
	    // sample counter
	    sample_nb++;
	    
	    if ((phi_sum[i]>max_score) && (phi_sum[i]>min_peak_val_v)) { // check this is a peak
	      max_score=phi_sum[i];
	      phase_for_max=gm_sum[i];
	      printf("### %f %f\n",phi_sum[i],min_peak_val_v);
	      // this to eliminate peak detection on not a local maximum (cause error when cthe actual local max is within the search radius)  
	      not_a_max=0;
	      for (j=i;j<i+RAD_SEARCH_PEAK;j++) if (phi_sum[j]>max_score) not_a_max=1;
	      for (j=i;j>i-RAD_SEARCH_PEAK;j--) if (phi_sum[j]>max_score) not_a_max=1;
	      
	      if (!not_a_max) {
		printf("enter peak detector %d\n",i);
		for (j=i;(j>=0)&&((i-j)<RAD_SEARCH_PEAK);j--) 
		  {
		    if (1) printf("%d %f,",j,phi_sum[j]);
		    if (phi_sum[j]<=PEAK_DETECTOR_FACT*max_score) break;
		  }
		if (0)printf("\n");
		for (k=i;(k<2*nfft+RAD_SEARCH_PEAK-n1)&&((k-i)<RAD_SEARCH_PEAK);k++) 
		  {
		    if (1) printf("%d %f,",k,phi_sum[k]);
		    if (phi_sum[k]<=PEAK_DETECTOR_FACT*max_score) break;
		  }
		if (1) printf("\n");
		if (((i-j)<RAD_SEARCH_PEAK)&&((k-i)<RAD_SEARCH_PEAK)&&
		    //&& (i!=j)&&(i!=k)&&
		    (max_score>(av_pow*FACTOR_COARSE_SYNC))
		    //&&(max_score>=0.8*last_max_peak_val)) 
		    )
		  {	// found a peak
		    
		    // peak index is either the middle of Max-10%, or the max according to AVERAGE_PEAK_POS
		    if (AVERAGE_PEAK_POSITION) 
		      index_of_peak_i=(j+k)>>1;
		    else index_of_peak_i=i;
		    
		    assert(found_peak_i<MAX_NB_PEAK);
		    list_of_peaks[found_peak_i].index=index_of_peak_i;
		    list_of_peaks[found_peak_i].val_max=max_score;	  
		    list_of_peaks[found_peak_i].smpl_indx=sample_nb;
		    list_of_peaks[found_peak_i].val_phase=phase_for_max;	  
	  
		    printf("found peak at index %d(%d) val %f\n",index_of_peak_i,i,max_score);
		    
		    // store detected max value
		    av_max[indx_max_average++]=max_score;
		    if (indx_max_average>=DEPTH_MAX_AVERAGE) indx_max_average=0;
		    
		    // compute average max value
		    av_max_value=0.0;
		    for(j=0;j<DEPTH_MAX_AVERAGE;j++) av_max_value+=av_max[j];
		    av_max_value=av_max_value/(float)DEPTH_MAX_AVERAGE;
		    
		    found_peak_i+=1;
		    //break;
		    // jump a little further to continue search, but take care when at end of buffer to count samples (sample_nb)
		    i+=2*RAD_SEARCH_PEAK; 
		    if (i<2*nfft-n1-RAD_SEARCH_PEAK) sample_nb+=2*RAD_SEARCH_PEAK;
		    else sample_nb+=2*nfft-n1-RAD_SEARCH_PEAK-(i-2*RAD_SEARCH_PEAK);
		  } 
	      }
	      if (0) printf("i=%d j=%d k=%d\n",i,j,k);
	    }
	  }
	  printf("max_score=%f avpower=%f\n\t\t index of peak found %d j=%d k=%d,nb_peaks %d\n",max_score,av_pow,index_of_peak_i,j,k,found_peak_i);
	} else {
	  sample_nb+=2*nfft-n1-2*RAD_SEARCH_PEAK;
	  printf("!!!!! mode free running is active last smpl in  buff: %d lastpeak %d indx in buff %d\n",sample_nb,last_peak_indx_i,last_index_value);
	  found_peak_i=0;
	  while (((last_index_value+(found_peak_i+1)*(nfft+ncp)) >= RAD_SEARCH_PEAK) && (last_index_value+(found_peak_i+1)*(nfft+ncp)<(2*nfft-n1-RAD_SEARCH_PEAK))) {
	    // index in buffer
	    list_of_peaks[found_peak_i].index=last_index_value+(found_peak_i+1)*(nfft+ncp);
	    // index of sample
	    list_of_peaks[found_peak_i].smpl_indx=last_peak_indx_i+nfft+ncp;
	    printf("free running: one start symbol is in buffer  indx=%d/smpl=%d\n",list_of_peaks[found_peak_i].index,list_of_peaks[found_peak_i].smpl_indx);
	    last_peak_indx_i+=nfft+ncp;
	    found_peak_i++;
	  }
	}

  *index_of_peak=index_of_peak_i;
  *last_peak_indx=last_peak_indx_i;
  *first_peak_found=first_peak_found_i;
  *found_peak=found_peak_i;
	

}// end of detect_peak


void process_peak_list(char found_peak,char first_peak_found,int *last_peak_indx) {

  // compute average distance between peaks
  
  int distpeak;
  int last_start_index,index_max1,index_max2,index_max;
  int i,j;
  int last_peak_indx_i;
  double maxpeakval1,maxpeakval2;	  

  last_peak_indx_i=*last_peak_indx;

	  maxpeakval1=0.0;
	  maxpeakval2=0.0;
	  index_max1=-1;
	  index_max2=-1;
	  //found_start_symbol=0;
	  
	  // detect max peak value 
	  for(i=0;i<found_peak;i++){
	    if (list_of_peaks[i].val_max>maxpeakval1 ) {
	      maxpeakval1=list_of_peaks[i].val_max;index_max1=i;
	    }
	  }
	  
	  // detect 2nd max peak value 
	  for(i=0;i<found_peak;i++){
	    if ((i!=index_max1)&&list_of_peaks[i].val_max>maxpeakval2  ) {
	      maxpeakval2=list_of_peaks[i].val_max;index_max2=i;
	    }
	  }
	  
	  if (found_peak>=2) printf("2 higher peaks %d (%f) %d (%f)\n",index_max1,maxpeakval1,index_max2,maxpeakval2);
	  else if (found_peak==1) printf("one peak  %d (%f)\n",index_max1,maxpeakval1);
	  else printf("no peak found in this buffer\n");
	  
	  
	  // if first peak => take the max
	  // else take the max with distance closest to the searched symbol length
	  if (first_peak_found) {
	    // select indexmax as first valid peak, (move to 0), invalidate index_max
	    if(!FIND_FIRST_PEAK_ONLY){
	      list_of_peaks[0].val_max=list_of_peaks[index_max].val_max;
	      list_of_peaks[0].index=list_of_peaks[index_max].index;
	      list_of_peaks[0].smpl_indx=list_of_peaks[index_max].smpl_indx;
	      list_of_peaks[0].val_phase=list_of_peaks[index_max].val_phase;
	      for(i=1;i<found_peak;i++){
		if (list_of_peaks[i].index<=list_of_peaks[0].index)
		  list_of_peaks[i].index=-1;
		
	      }
	    } else {
	      
	      // case double peak
	      if (found_peak>=2 && (((maxpeakval1-maxpeakval2)/(float)maxpeakval1)<0.2)){
		if (index_max1<index_max2) index_max=index_max1;
		else  index_max=index_max2;
	      } else index_max=index_max1;
	      
	      
	      list_of_peaks[0].val_max=list_of_peaks[index_max].val_max;
	      list_of_peaks[0].index=list_of_peaks[index_max].index;
	      list_of_peaks[0].smpl_indx=list_of_peaks[index_max].smpl_indx;
	      list_of_peaks[0].val_phase=list_of_peaks[index_max].val_phase;
	      last_peak_indx_i=sample_nb;
	      printf("\t free running : peak %d  indx %d first_peak  %d\n",i,list_of_peaks[i].index,first_peak_found);
	      distpeak=nfft+ncp;
	      found_peak=1;
	      printf("free running: first peak found\n");
	      if (list_of_peaks[0].index+nfft+ncp<2*nfft-n1-RAD_SEARCH_PEAK) {
		// next peak in buffer: force next peak posiiton 
		printf("free running: next start is in current buffer\n");
		list_of_peaks[1].index=list_of_peaks[0].index+nfft+ncp;
		list_of_peaks[1].smpl_indx=list_of_peaks[0].smpl_indx+nfft+ncp;
		list_of_peaks[1].val_max=list_of_peaks[0].val_max;
		list_of_peaks[1].val_phase=list_of_peaks[0].val_phase;
		found_peak++;
		last_peak_indx_i+=nfft+ncp;
	      }
	    }
	    
	    printf("first peak found %d\n",list_of_peaks[0].index);
	  } // first_peak_found
	  
	  // check peaks for qualification against criteria:
	  // estimated symbol length
	  
	  last_start_index=-1;
	  for(i=0;i<found_peak;i++){
	    
	    printf(">>>detected peak at %d peakval=%f smplindx=%d phase=%f\n",list_of_peaks[i].index,list_of_peaks[i].val_max,list_of_peaks[i].smpl_indx,list_of_peaks[i].val_phase);
	    
	    if ((i==0) && (!first_peak_found) &&(list_of_peaks[0].index!=-1)  ) {
	      distpeak=(list_of_peaks[i].index-last_index_value);
	      printf("distance btwn first peak and last peak (%d) %d\n",last_index_value,distpeak );
	      
	    } else if(first_peak_found && (i>0) &&(list_of_peaks[i].index!=-1)) {
	      distpeak=(list_of_peaks[i].index-list_of_peaks[0].index);
	      printf("first peak:  distance btwn peak and first peak %d\n",distpeak );
	      
	    } else if(i==0 && first_peak_found){
	      // force first peak with distance
	      distpeak=nfft+ncp;
	    } else if ((i>0) && (list_of_peaks[i].index!=-1)) {
	      distpeak=(list_of_peaks[i].index-list_of_peaks[i-1].index);
	      printf("distance between  peaks %d\n",distpeak );
	    } else if (list_of_peaks[i].index==-1) {}
	    else {printf("i %d indx %d first_peak %d\n",i,list_of_peaks[i].index,first_peak_found);assert(0);}
	    
	    if (distpeak<(nfft+ncp-EPS_DIST)) { // disqualify the peak
	      printf("\t\t disqualified\n"); 
	      list_of_peaks[i].index=-1;
	    } else last_start_index=list_of_peaks[i].index;	   
	    
	    /*
	      
	     */
	  }
	  j=0;
	  // remove from list the disqualified peaks
	  for (i=0;i<found_peak;i++){
	    printf("%d %d\n",i,found_peak);
	    if (list_of_peaks[i].index!=-1) {
	      list_of_peaks[j].val_max=list_of_peaks[i].val_max;
	      list_of_peaks[j].val_phase=list_of_peaks[i].val_phase;
	      list_of_peaks[j].index=list_of_peaks[i].index;
	      list_of_peaks[j].smpl_indx=list_of_peaks[i].smpl_indx;
	      j++;
	    }
	  }
	  
	  found_peak=j;
	  
	  // check the list of peaks
	  printf("peak dump: left %d peaks in list after selection\n",found_peak);
	  for (i=0;i<found_peak;i++){
	    printf("\t\tpeak dump %d val=%f indx=%d smpl=%d\n",i,list_of_peaks[i].val_max,list_of_peaks[i].index,list_of_peaks[i].smpl_indx);
	  }
	  
	  
	  // compute how much symbol to process
	  symb_to_process=found_peak;
	  printf("for this buffer: \n");
	  printf("\t%d peaks found in buffer\n",symb_to_process);
	  
	  if (partial_buffer) {
	    printf("\tone partial symbol to complete\n"); 
	    symb_to_process++;
	  }

	  *last_peak_indx=last_peak_indx_i;

} // end of process_peak_list

char fill_in_buffer(struct complex_f *in_dmod_buff,struct complex_f *in_dmod,char *first_peak_found,int backoff_v) {

  char first_peak_found_i;
  char flag_break=0;

  first_peak_found_i=*first_peak_found;

	    assert(list_of_peaks[index_peak].index-backoff_v>=0);
	    
	    if (first_peak_found_i) {
	      // init last_index_val
	      assert(list_of_peaks[0].index!=-1);
	      printf("\tfirst peak detected at %d\n",list_of_peaks[0].index);
	      last_index_value=list_of_peaks[0].index;
	      first_peak_found_i=0;
	    } else {
	      printf("\tindex peak %d last_index=%d index=%d detected symbol length= %d\n",index_peak,last_index_value,list_of_peaks[index_peak].index,list_of_peaks[index_peak].index-last_index_value);
	    }

	    //found_start_symbol=1;		

	    // fill in the data for demodulation in the dmod buffer
	    if(partial_buffer) {
	      printf("&&&partial symbol in buffer => copy the remaining samples \n");
	      memcpy(in_dmod_buff+nb_smples_in_buff,in_dmod+ncp+n1+2*RAD_SEARCH_PEAK,(nfft+ncp+2*backoff_v-nb_smples_in_buff)*sizeof(struct complex_f));
	      printf("\t complete partial buffer with %d samples\n",(nfft+ncp+2*backoff_v-nb_smples_in_buff));
	      partial_buffer=0;
	      if (index_peak+1==symb_to_process) last_index_value=-((2*nfft+ncp-last_index_value)-(n1+ncp+2*RAD_SEARCH_PEAK));
	      //else last_index_value=list_of_peaks[0];
	    }
	    else 
	      if ((list_of_peaks[index_peak].index+backoff_v+nfft+ncp<2*nfft+ncp)){
		//if ((list_of_peaks[index_peak].index-backoff_v+nfft+ncp<2*nfft+ncp)){
		  printf("&&&full symbol in buffer (lastindex=%d)\n",last_index_value);
		  memcpy(in_dmod_buff,in_dmod+list_of_peaks[index_peak].index-backoff_v,(nfft+ncp+2*backoff_v)*sizeof(struct complex_f));
		  if (index_peak+1==symb_to_process) last_index_value=-((2*nfft+ncp-list_of_peaks[index_peak].index)-(n1+ncp+2*RAD_SEARCH_PEAK));
		  else last_index_value=list_of_peaks[index_peak].index;
		  printf("\t copy complete symbol to demod buffer last_index=%d\n",last_index_value);
		  index_peak++;
		}

	      else {
		printf("&&& partial copy %d samples for demod, last_index=%d\n",(2*nfft+ncp-list_of_peaks[index_peak].index+backoff_v),list_of_peaks[index_peak].index);
		// fill in the partial symbol for future demodulation in the dmod buffer
		memcpy(in_dmod_buff,in_dmod+list_of_peaks[index_peak].index-backoff_v,(2*nfft+ncp-last_index_value+backoff_v)*sizeof(struct complex_f));
		//printf("&&& partial copy %d samples for demod\n",(2*nfft+ncp-last_index_value+backoff_v));
		partial_buffer=1;
		nb_smples_in_buff=(2*nfft+ncp-list_of_peaks[index_peak].index+backoff_v);
		last_index_value=-((2*nfft+ncp-list_of_peaks[index_peak].index)-(n1+ncp+2*RAD_SEARCH_PEAK));
		flag_break=1;
	      }

	    *first_peak_found=first_peak_found_i;
	    return flag_break;

}// end of fill_in_buffer

double compute_papr(struct complex_f *symbol) {

  int j;
  double mod,peak_mod=0.0,peak_r=0.0,peak_i=0.0;
  double av,papr;
  av=0.0;
    for(j=0;j<nfft;j++) {
      printf("$$$%d %f %f\n",j,symbol[j].r,symbol[j].i);
      mod=(symbol[j].r*symbol[j].r+symbol[j].i*symbol[j].i);
      av+=mod;
      if (fabs(symbol[j].r)>peak_r) peak_r=fabs(symbol[j].r);
      if (fabs(symbol[j].i)>peak_i) peak_i=fabs(symbol[j].i);
      if (fabs(mod)>peak_mod) {peak_mod=fabs(mod);}	
    }

    av=av/(float)(nfft);
    papr=10*log10(peak_mod/av);

    return papr;
}

//  select best papr among nsub_sect sub sections
void select_best_papr(struct complex_f *symbol, int *num_section) {

  int i,j,indx;
  double mod,peak_mod=0.0,peak_r=0.0,peak_i=0.0;
  double av,papr,paprmin=(float) BIG_INT;
  int num_sect_min=-1;

  for(i=0;i<nsub_sect;i++) {
    av=0.0;
    peak_mod=0.0;peak_r=0.0;peak_i=0.0;
    for(j=0;j<nfft;j++) {
      printf("$$$%d %f %f\n",j,symbol[j+i*nfft].r,symbol[j+i*nfft].i);
      mod=(symbol[j+i*nfft].r*symbol[j+i*nfft].r+symbol[j+i*nfft].i*symbol[j+i*nfft].i);
      av+=mod;
      if (fabs(symbol[j+i*nfft].r)>peak_r) peak_r=fabs(symbol[j+i*nfft].r);
      if (fabs(symbol[j+i*nfft].i)>peak_i) peak_i=fabs(symbol[j+i*nfft].i);
      if (fabs(mod)>peak_mod) {peak_mod=fabs(mod);indx=j;}	
    }
    av=av/(float)(nfft);
    papr=10*log10(peak_mod/av);

    if (papr<paprmin) {paprmin=papr;num_sect_min=i;}
    printf("for section %d papr=%f min=%f peak=%f indx=%d\n",i,papr,paprmin,peak_mod,indx);
  }
  

  printf("found_sectmin=%d\n",num_sect_min);

  *num_section=num_sect_min;

}// end of select_best_papr 

// log the result in the paprtaable
void histo_log_papr(double *histo,double val) {

  
}// end of histo_log_papr

void histo_log(double histo[MAX_HISTO_BINS][2],double val,int *bin_nbi) {

  int i;
  int bin_nb;
  
  bin_nb=*bin_nbi;

  assert(bin_nb<MAX_HISTO_BINS);

  for (i=0;i<bin_nb;i++) {
    if ((val>(histo[i][0]*0.9)) && (val<(histo[i][0]*1.1))) {
      histo[i][0]=(histo[i][1]*histo[i][0]+val)/(histo[i][1]+1.0);histo[i][1]=histo[i][1]+1.0;    
      printf("i %d val %f av %f\n",i,histo[i][0],histo[i][1]);
      break;}
  }
  // create a new bin
  if (i>=bin_nb) {
    printf("created bin %d\n",bin_nb);
    histo[bin_nb][0]=val;histo[bin_nb][1]=1.0;bin_nb=bin_nb+1;
  }

  *bin_nbi=bin_nb;

}// end of histo_log

void merge_bins(double histo[MAX_HISTO_BINS][2], int *bin_nbi){

  int i,j;
  int bin_nb;
  
  bin_nb=*bin_nbi;

  assert(bin_nb<MAX_HISTO_BINS);

  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for(j=i+1;j<bin_nb;j++) {
      if (histo[j][1]==-1.0) continue;
      if (((histo[i][0]<histo[j][0]) && (1.1*histo[i][0]>=0.9*histo[j][0])) || ((histo[i][0]>histo[j][0])&& (0.9*histo[i][0]<=1.1*histo[j][0]))) {
	printf("merge %d %d %f %f\n",i,j,histo[i][0],histo[j][0]);
	// merge bins
	histo[i][0]=(histo[i][0]*histo[i][1]+histo[j][0]*histo[j][1])/(histo[i][1]+histo[j][1]);
	histo[i][1]+=histo[j][1];histo[j][1]=-1.0;
      }
    }
  }

  *bin_nbi=bin_nb;

} // end of merge_bins

int get_best_bins(double histo[MAX_HISTO_BINS][2],int *bin_nbi, double *av1,int *n01,  double *av2, int *n02){
  double av1_i,av2_i;
  int n1_i,n2_i;
  int i,j,bin_nb,indx1,indx2;
  double max1, max2;

  bin_nb=*bin_nbi;
  max1=histo[0][1];indx1=0;

  // get max1
  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }

  indx2=-1;
  //get bin closer to the nb of symbols
  for (i=0;i<bin_nb;i++) {
    if  (histo[i][1]==-1.0) continue;
    if  ((abs(histo[i][1]-(float)NB_OF_SYMBOLS)<=1.0) && (i!=indx1)){
      indx2=i; break;
    }
  }

  printf("get_best_bins: %d %d\n",indx1, indx2);
  *av1=histo[indx1][0];
  if (indx2!=-1) *av2=histo[indx2][0]; else return 0;
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];
  return 1;

}// end of get_best_bins

int get_best_bins_2(double histo[MAX_HISTO_BINS][2],int *bin_nbi, double *av1,int *n01,  double *av2, int *n02){
  double av1_i,av2_i;
  int n1_i,n2_i;
  int i,j,bin_nb,indx1,indx2;
  double max1, max2;

  bin_nb=*bin_nbi;
  max1=histo[0][1];indx1=0;

  // get max1 starting from smaller
  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }

  indx2=-1;
  //get bin closer to the nb of symbols starting from higher
  for (i=bin_nb;i>indx1;i--) {
    if  (histo[i][1]==-1.0) continue;
    if  ((abs(histo[i][1]-(float)NB_OF_SYMBOLS)<=1.0) && (i!=indx1)){
      indx2=i; break;
    }
  }

  printf("get_best_bins(2): %d %d\n",indx1, indx2);
  *av1=histo[indx1][0];
  if (indx2!=-1) *av2=histo[indx2][0]; else return 0;
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];
  return 1;

}// end of get_best_bins_2

void get_big_bins(double histo[MAX_HISTO_BINS][2],int *bin_nbi, double *av1,int *n01,  double *av2, int *n02) {

  double av1_i,av2_i;
  int n1_i,n2_i;
  int i,j,bin_nb,indx1,indx2;
  double max1, max2;

  bin_nb=*bin_nbi;

  max1=histo[0][1];indx1=0;

  // get max1
  for (i=0;i<bin_nb;i++) {
    //if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }


  indx2=0;
  if (indx1!=0) {indx2=0;max2=histo[0][1];}
  else max2=0.0;

  // get max2
  for (i=0;i<bin_nb;i++) {
    //if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if ((histo[j][1]>max2) && (histo[j][1]<max1)) {max2=histo[j][1];indx2=j;}
    }
  }
  printf("%d %d\n",indx1, indx2);
  *av1=histo[indx1][0];
  *av2=histo[indx2][0];
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];


}// end of get_big_bins

void sort_bins(double histo[MAX_HISTO_BINS][2],int bin_nb) {

  int i,j;
  int indx;
  double min=(double) BIG_INT,tmp;

  for(i=0;i<bin_nb;i++){
    min=(double) histo[i][0];indx=i;
    for(j=i;j<bin_nb;j++){
      if (histo[j][0] < min) {min=histo[j][0];indx=j;}
    }
    tmp=histo[i][0];
    histo[i][0]=histo[indx][0];
    histo[indx][0]=tmp;
    tmp=histo[i][1];
    histo[i][1]=histo[indx][1];
    histo[indx][1]=tmp;
  }

  printf("sorted bins\n");
  for(i=0;i<bin_nb;i++) printf("%d %f %f\n",i,histo[i][0],histo[i][1]);

}// end of sort bins

void init_pn(char *seq,char *initpn, char *pol) {

  int i;
  char x;

  for(i=0;i<L_PNSEQ-1;i++) {
    seq[i]=initpn[i];
    //printf("*%d",initpn[i]); 
  }
  //printf("\n");

  x=seq[0];
  for(i=1;i<L_PNSEQ-1;i++) {
    if (pol[i]==1) x = (x^seq[i])&0x1;
  }

  seq[L_PNSEQ-1]=x;

  if (0) {
    for(i=0;i<L_PNSEQ;i++) printf("#%d",seq[i]);
    printf("\n");
  }

}// end of init_pn

char step_pn(char *seq,char* pol) {

  int i;
  char x;

  x=seq[0];

  for(i=1;i<L_PNSEQ-1;i++) {
    if (pol[i]==1) x = ((x^seq[i]))&0x1;
  }

  // shift
  for(i=0;i<L_PNSEQ-2;i++) seq[i]=seq[i+1];

  seq[L_PNSEQ-2]=x;

  #ifdef TEST_PN
  for(i=0;i<L_PNSEQ-1;i++) printf("%d",seq[i]);
  printf("\n");
  #endif

  return (x==0)?-1:1;

}// end of step_pn

// compute ccdf using papr histog
int nb_greater_val(int *histog,int val) {

  int i;
  int nb=0;

  for (i=0;i<NB_BINS_HISTO_PAPR;i++) {
    if (i>=val) nb+=histog[i];
    
  }

  return nb;

} // end of nb_greater_val

int main(int argc, char*argv[]) {
//void main_mod_demod(int32_t *inbitstream, int len) {

	//int argc=0;
	//char *argv[] = {};

#ifdef ZYNQ
	Greenhill greenhill_app;
#endif

  struct complex_f *in_mod, *in_mod2, *out_mod, *out_mod2, *out_mod_comb, *out_dmod, *in_dmod;
  struct complex_f *learn_symbol, *in_dmod_buff;
  struct complex_f *corr_facts, *pilots, *pilots_ref;
#ifdef LINUX
  FILE *in_file, *out_file, *odmod, *file_csv, *berfile;
  FILE *in_bitstr_file, *out_mod_file, *in_dmod_file, *ref_tx_file;
  FILE *dumpout, *dumpoutfft, *dumpoutfftdem, *dump_papr;
  FILE *histo_file;
#endif
  //p_in_buf is a pointer to keep the reference of the input buffer beginning
  struct complex_f *p_input_buf, *p_in_buf; //output of tx
  //p_out_buf is a pointer to keep the reference of the output buffer beginning
  struct complex_f *p_output_buf, p_out_buf; //not currently used, was the rx bitsteam

  //char *inbitstream,*inbitstreamc;
  char *inbitstreamc;
  int i, j, k, l, jj;
  double sumdiffmod = 0;
  double sumdiffdmod = 0;
  double mod_corr, last_max_peak_val = 0;
  int index_of_max, index_of_peak, nb_symb = 0, av_index = 0, tot_read = 0;
  int read_before = 0;
  char coarse_sync = 0, first = 1; // flag detection coarse sync is ok
  // buffer to store average of maxima
  int symbol_nb, nb_smples_in_buff;
  char found_start_symbol;
  struct complex_f *out_modf, *out_csv;
  struct complex_f *tmp;
  double tx_pow_symb = 0.0, peak_r_symb, peak_i_symb, mod, tx_pow_dbm, peak_mod_symb, av_pow_buff;
  double tx_pow_tot = 0.0, peak_r_tot, peak_i_tot, peak_mod_tot;
  int nb_read;
  int lasti;
  char *bitstream,lastpos,lastpos_i;
  char *bitstream_c;
  int last_c,last_ref,indxbs_c,indxbs_ref;
  int last_peak_indx,indxbs_i=0,indx;
  float tx_gain_v,gain=-1;
  char dmod=-1;
  int backoff_v,backoff=-1;
  char skip_mod_v = 0;//TODO TBC the init value, done to avoid unizialized variable as used as condition
  int nb_bits_in_symbol, nb_char_in_symbol, nb_bits_in_symbol_max;
  int ncenter, ndata_tones;
  int ndiffnb, ndiffmin, bckoffmin;
  char flag_iter, first_peak_found=1, found_peak=0;
  char flag_signal_detected;
  struct complex_f gm, phi; //TODO check if phi should be a complex struct
  double sigma;
  double rx_pow,max_pow,av_re,av_im;
  struct complex_f *tx_ref;
  double maxd;
  mode_ofdm mode_v = DEFAULT, mode = DEFAULT;//TODO TBC the init value, done to avoid unizialized variable as used as condition
  int nchar_stream, nbits_stream, num_section;
  char symb_struct[N_MAX*M_MAX];
  char pn_seq[M_MAX*N_MAX];
  char pn_seq2[M_MAX*N_MAX];
  char pn_reg[L_PNSEQ];
  double papr;
  int nfft_v=-1, ncp_v=-1, null_tones_v=-1, nsub_sect_v=-1;
  char  constell_v=-1;
  // 2 pn seqs initialization vector: one is the reverse of the other
  char pn_init2[L_PNSEQ-1]={1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0};
  char pn_init[L_PNSEQ-1]={0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1};
  //polynom is (15,14,0)
  //char pn_pol[L_PNSEQ]= {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
  char pn_pol[L_PNSEQ]= {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1};
  int bin_nb;
  int histo_papr[NB_BINS_HISTO_PAPR];
  int index_sig[NB_SIGNAL_TONES];

  int jmin,lmin;
  double papr_sec,papr_sec1,papr_min;

  memset(pn_reg,0,L_PNSEQ*sizeof(char));
  memset(pn_seq,0,M_MAX*N_MAX*sizeof(char));
  memset(pn_seq2,0,M_MAX*N_MAX*sizeof(char));
  memset(histo_papr,0.0,NB_BINS_HISTO_PAPR*sizeof(int));


// types for fftw 
#ifdef LINUX
  fftw_complex *out_fft,*out_fft2;
  fftw_plan p;
#endif
  struct complex_f *out_fft, *out_fft2;

//printf("%f %d %f %d %f %d\n",3.5,(int) floor(3.5+0.5),-3.6,(int) floor(-3.6+0.5),3.6,(int)floor(3.6+0.5));
// exit(0);
 // test PN seq

#ifdef TEST_PN
 init_pn(pn_reg, pn_init,pn_pol);
 for (i=0;i<20;i++)
   step_pn(pn_reg,pn_pol);
 exit(0);
#endif

 backoff = 0;
// I) parsing of the options and system parameters determination
 //printf("const %d\n",constell_v);
 parse_options(argc, argv, &backoff, &gain, &mode, &dmod, &nfft_v, &ncp_v, &null_tones_v, &nsub_sect_v, &constell_v);
printf("parse_options : backoff=%d gain=%f mode=%d dmod=%d nfft_v=%d ncp_v=%d null_tones_v=%d nsub_sect_v=%d constell_v=%c\n",
       backoff, gain, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v);

 // compute the bits per constellation
 calc_nbit_const();

 if((backoff >= MINBACKOFF) && (backoff <= MAXBACKOFF)){
   backoff_v = backoff;
#ifdef LINUX
   fprintf(stderr, "backoff specified: %d\n", backoff_v);
#else
   printf("backoff specified: %d\n", backoff_v);
#endif
 } else {
#ifdef LINUX
   fprintf(stderr, "default backoff index used: %d\n", DEF_BACKOFF_INDX);
#else
   printf("default backoff index used: %d\n", DEF_BACKOFF_INDX);
#endif
   backoff_v = DEF_BACKOFF_INDX;
 }

 if(mode != DEFAULT) {
   mode_v = mode;
#ifdef LINUX
   fprintf(stderr, "signal type specified is %s\n", (mode_v == GREEN_GREEN) ? "green SLM" : ((mode_v == GREEN_CONV) ? "green conv" : "conv"));
#else
   printf("signal type specified is %s\n", (mode_v == GREEN_GREEN) ? "green SLM" : ((mode_v == GREEN_CONV) ? "green conv" : "conv"));
#endif
 } else {
   mode=CONV_OFDM;
#ifdef LINUX
   fprintf(stderr, "default modulation is used: %s\n", (mode_v == GREEN_GREEN) ? "green SLM" : ((mode_v == GREEN_CONV) ? "green conv" : "conv"));
#else
   printf("default modulation is used: %s\n", (mode_v == GREEN_GREEN) ? "green SLM" : ((mode_v == GREEN_CONV) ? "green conv" : "conv"));
#endif
 }

 if(gain != -1){
   tx_gain_v = gain;
   if (!skip_mod_v) {
#ifdef LINUX
     fprintf(stderr, "TX gain specified: %f\n", tx_gain_v);
#else
     printf("TX gain specified: %f\n", tx_gain_v);
#endif
   }
 } else {
   tx_gain_v = TX_GAIN;
   if (!skip_mod_v) {
#ifdef LINUX
     fprintf(stderr, "default TX gain value used: %f\n", TX_GAIN);
#else
     printf("default TX gain value used: %f\n", TX_GAIN);
#endif
   }
 }

 if (nfft_v != -1) {
   nfft = nfft_v;
#ifdef LINUX
   fprintf(stderr, "FFT size is specified %d\n", nfft);
#else
   printf("FFT size is specified %d\n", nfft);
#endif
 } else {
   nfft = NFFT;
#ifdef LINUX
   fprintf(stderr, "FFT size is default %d\n", NFFT);
#else
   printf("FFT size is default %d\n", NFFT);
#endif
 }
 
if (ncp_v != -1) {
   ncp = ncp_v;
#ifdef LINUX
   fprintf(stderr, "cp size is specified %d\n", ncp);
#else
   printf("cp size is specified %d\n", ncp);
#endif
 } else {
   ncp = NCP;
#ifdef LINUX
   fprintf(stderr, "cp size is default %d\n", NCP);
#else
   printf("cp size is default %d\n", NCP);
#endif
 }

if (null_tones_v != -1) {
   null_tones = null_tones_v;
   if (null_tones_v%2) {
#ifdef LINUX
     fprintf(stderr, "nb of null tone must be even\n");
#else
     printf("nb of null tone must be even\n");
#endif
     exit(0);
   }
#ifdef LINUX
   fprintf(stderr, "nb of null tones  is specified %d\n", null_tones);
#else
   printf("nb of null tones  is specified %d\n", null_tones);
#endif
 } else {
   null_tones = NB_TONES_OFF;
#ifdef LINUX
   fprintf(stderr, "nb of null tones  is default %d\n", null_tones);
#else
   printf("nb of null tones  is default %d\n", null_tones);
#endif
 }

if (nsub_sect_v != -1) {
   nsub_sect = nsub_sect_v;
   //fprintf(stderr,"nb of subsections is specified %d\n",nsub_sect);
 } else {
   nsub_sect = 1;
   //fprintf(stderr,"nb of subsections is default %d\n",nsub_sect);
 }

if (constell_v != -1) {
  // 0 bpsk, 1 qpsk, 2 qam16
   constell = constell_v;
   //fprintf(stderr,"cp size is specified %d\n",constell);
 } else {
   constell = CONSTELL;
   //fprintf(stderr,"cp size is default %d\n",nsub_sect);
 }

 if (constell == QPSKC) {
#ifdef LINUX
   fprintf(stderr, "modulation type is QPSK\n");
#else
   printf("modulation type is QPSK\n");
#endif
 }
 else if (constell == QAM16C) {
#ifdef LINUX
       fprintf(stderr, "modulation type is QAM16\n");
#else
       printf("modulation type is QAM16\n");
#endif
      }
 else {
#ifdef LINUX
   fprintf(stderr, "modulation type is BPSK\n");
#else
   printf("modulation type is BPSK\n");
#endif
 }

 float nb_sigtones_float;
 // number of signal tones to code sub section number
 // assuming QPSK modulation for sigtones, 2bits/sigtones: actual nb of sigtones is sigtones/2 for one symbol, but double it for Gossyc (left and right symbols both with sidetones)
 nb_sigtones_float =  log2(nsub_sect);
 nb_sigtones = (int) floor(nb_sigtones_float);
 if (nb_sigtones_float - (float)nb_sigtones) {
#ifdef LINUX
   fprintf(stderr, "the number of subsections (%d) is not a power of 2\n", nsub_sect);
#else
   printf("the number of subsections (%d) is not a power of 2\n", nsub_sect);
#endif
   exit(0);
 }
 // assuming QPSK 
 nb_sigtones = (nb_sigtones%2) ? 2*(nb_sigtones/2+1) : nb_sigtones;
 //printf("%f %d\n",nb_sigtones_float,nb_sigtones);
 assert(nb_sigtones < NB_SIGNAL_TONES);

 assert(nfft <= N_MAX);
 // for case M=4
 nfft1 = nfft/4;
 nfft2 = 2*nfft1;
 nfft3=3*nfft1;

 if (null_tones >= nfft) { 
   fprintf(stderr, "nb of null tones(%d) should be less than fft size(%d): try other parameters\n", null_tones,nfft); exit(0);
 }

if((nfft - (null_tones)) % 5) {
   printf("pilots should be right after (before) first (last) null tone\n");
   // compute the closest null tone number to specified in order to fulfill this requirement
   //exit(0);
   null_tones += (nfft - (null_tones))%5;
   null_tones -= 1; // extra pilot at the end (nfft-null-tone)/5+1 pilot in frame
#ifdef LINUX
   fprintf(stderr, "null tone nb is set to %d \n", null_tones);
#else
   printf("null tone nb is set to %d \n", null_tones);
#endif
 }

// check DC is not on a pilot
 if (!(((nfft<<1) - (null_tones<<1))%5)) {
#ifdef LINUX
   fprintf(stderr, "pilot should not be on a DC: select an other value\n"); 
#else
   printf("pilot should not be on a DC: select an other value\n"); 
#endif
   exit(0);
 }

// set nsub_sect to 1 when in conventional
 if (mode_v == CONV_OFDM) {
   nsub_sect=1;
 }
 else {
#ifdef LINUX
  fprintf(stderr, "nb of subsections is specified %d\n", nsub_sect);
#else
  printf("nb of subsections is specified %d\n", nsub_sect);
#endif
  }

 //TODO retrofit on PC version static declaration that goes on stack to
  //be transform in malloc to avoid stack overflow (not input_buf and output_buf
  // as specific for embedded
   // 5=> QAM32
   //#define SIZE_INPUT_BUFFER ((NFFT_MAX + NCP) * NB_OF_SYMBOLS * sizeof(struct complex))
   //#define SIZE_OUTPUT_BUFFER (((NFFT_MAX * 5 * NB_OF_SYMBOLS)>>3) * sizeof(char))
    printf("nfft=%d nsub_sect=%d ncp=%d\n", nfft, nsub_sect, ncp);
    printf("parse_options : backoff=%d gain=%f mode=%d dmod=%d nfft=%d ncp_v=%d null_tones=%d nsub_sect=%d constell=%c\n",
            backoff, gain, mode, dmod, nfft, ncp, null_tones, nsub_sect, constell);

    //int size_input_buf = ((NFFT_MAX + NCP) * NB_OF_SYMBOLS * sizeof(struct complex));
    int size_input_buf = (nfft + ncp) * NB_OF_SYMBOLS * sizeof(struct complex_f);//p_input_buf
    //int size_output_buf = ((nfft * 5 * NB_OF_SYMBOLS) >> 3) * sizeof(char);
    //struct complex input_buf[SIZE_INPUT_BUFFER];
    //struct complex output_buf[SIZE_OUTPUT_BUFFER];
    p_in_buf = malloc((size_t) size_input_buf);
    p_input_buf = p_in_buf;
    if (p_input_buf == NULL) printf("p_input buf out of memory\n");
    //if ((p_output_buf = malloc((size_t) size_output_buf)) == NULL) printf("p_output_buf out of memory\n");
    //char *symb_struct, *pn_seq, *pn_seq2, *pn_reg;
    //char symb_struct[N_MAX*M_MAX];
    /*
    if ((symb_struct = malloc((size_t) (nfft * nsub_sect))) == NULL) printf("symb_struct out of memory\n");

    //char pn_seq[M_MAX*N_MAX];
    if ((pn_seq = malloc((size_t) (nfft * nsub_sect))) == NULL) printf("pn_seq out of memory\n");
    //char pn_seq2[M_MAX*N_MAX];
    if ((pn_seq2 = malloc((size_t) (nfft * nsub_sect))) == NULL) printf("pn_seq2 out of memory\n");
    //char pn_reg[L_PNSEQ];
    if ((pn_reg = malloc(L_PNSEQ)) == NULL) printf("pn_reg out of memory\n");
    */


    SPACE_BW_SIGNAL_TONES = ((int)floor((float)(nfft - (null_tones+2)-2 * FIRST_SIGNAL_POS) / (float)(nb_sigtones - 1)));

  // describes the symbol structure (pilot,signal,data,null)
    // build a M*N tones OFDM signal, with M subsymbol of N tones interleaved
    build_symbol_struct_0(symb_struct,mode);
    ndata_tones = count_tone(symb_struct, DATA_TONE);

    // list of sig tone indexes
    k = 0;
    for(i=0, j=0; i < nfft; i += nsub_sect, j++) if (symb_struct[i] == SIG_TONE) index_sig[k++] = j;

// parameter consistency
    if ((ndata_tones + count_tone(symb_struct, PILOT_TONE) + count_tone(symb_struct, NULL_TONE) + count_tone(symb_struct, SIG_TONE) != nfft)) {
#ifdef LINUX 
      fprintf(stderr, "check consistency\n"); 
      fprintf(stderr, "nb  tones carying data %d nb pilots %d(%d) nb_tones_off %d(including DC)\n", ndata_tones, count_tone(symb_struct,PILOT_TONE), count_tone(symb_struct, PILOT_TONE), count_tone(symb_struct, NULL_TONE));
#else
      printf("check consistency\n"); 
      printf("nb  tones carying data %d nb pilots %d(%d) nb_tones_off %d(including DC)\n", ndata_tones, count_tone(symb_struct,PILOT_TONE), count_tone(symb_struct, PILOT_TONE), count_tone(symb_struct, NULL_TONE));
#endif
   exit(0);
 }
 
 // select position of prefix green/conventional
 if (0 && mode_v) 
   n1 = nfft1;
 else
   n1 = nfft;

 rx_pow = 0.0; max_pow = 0.0; av_re = 0.0; av_im = 0.0;

 nb_bits_in_symbol_max = NB_BITS_IN_SYMBOL_MAX;
 //nb_char_in_symbol= NB_CHAR_IN_SYMBOL;
 nb_bits_in_symbol = ((ndata_tones) * (NBITSCONST[constell]));
 nb_char_in_symbol = ((nb_bits_in_symbol_max >> 3) + 1);
 
 printf("nb_bits_in_symbol=%d\n", nb_bits_in_symbol);

 return nb_bits_in_symbol;
}
/* 
 if (CENTER_SPECTRUM_0 && (ncenter = ((nfft / 2 - ZTONE_RAD_L - 1 - (null_tones >> 1)) % 5)) 
     && (ZTONE_RAD_L != 0) && (ZTONE_RAD_R != 0)) {
   printf("pilots should be rignt before center null tones %d\n", ncenter); exit(0);
 }
 if (CENTER_SPECTRUM_0 && (ncenter = ((nfft / 2 + ZTONE_RAD_R + 1 - (null_tones >> 1)) % 5)) 
     && (ZTONE_RAD_L != 0) && (ZTONE_RAD_R != 0)) {
   printf("pilots should be rignt after center null tones %d\n", ncenter); 
   exit(0);
 }
 
 // define FFT_NORM
 fft_norm_v = (mode_v == GREEN_GREEN) ? (1.0 / sqrt((float)nfft * nsub_sect)) : (1.0 / sqrt((float) nfft));
*/
//I) end parsing and parameters generation

