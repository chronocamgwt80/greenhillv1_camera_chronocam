#!/usr/bin/python
from greenhill import *
from zynq import *
import os
import argparse
from os.path import basename
from ofdm import *
import math

#DEBUG = True
DEBUG = False
debugFileRef = open("debug.bin",'w')

#read from a csv file a number of byte and send it to the zynq

def cfor(first,test,update):
    while test(first):
        yield first
        first = update(first)
        
def send_nbr_frame(chunkNbr):
        add_prep = ZYNQ_CMD_ADVISE_DATA_ON_FEW_FRAMES
        cmd_advise = (chr((add_prep)&0xFF) + chr((add_prep>>8)&0xFF) 
                    + chr((add_prep>>16)&0xFF) + chr((add_prep>>24)&0xFF)
                    + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                    + chr((nbrByteBuf&0xFF)>>16) + chr((nbrByteBuf&0xFF)>>24)
                    + chr(chunkNbr&0xFF))
        gw.zynq.write_raw(cmd_advise)

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
        
    parser = argparse.ArgumentParser()
    parser.add_argument("fileName", type=str,
                    help="the .csv modulation file")
    args = parser.parse_args()
    INPUT_FILE = args.fileName
    
    #read a csv file return a buffer buf and its number of byte nbrByteBuf
    ofdm=Ofdm()
    buf=""
    nbrByteBuf = 0
    ofdm.rd_file_ret_buf(INPUT_FILE)
    buf = ofdm.getBuf()
    nbrByteBuf = ofdm.getNbrByteBuf()

    # Command to send the IQ data read from a file
    #protocole is:
    #4B addr, 2B nbr of B in the frame, payload data, 1B checksum, 1B stop_byte
    #so protocol load is 8B
    PROTOCOL_TAIL_SIZE = 2
    PROTOCOL_HEAD_SIZE = 8
    PROTOCOL_FRAME_NBR_B = PROTOCOL_HEAD_SIZE + PROTOCOL_TAIL_SIZE
    MAX_USB_PKT_SIZE = 1024 * 16

    MAX_1ST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_HEAD_SIZE
    MAX_LAST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_TAIL_SIZE
    
    add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_RF_TX_FILL_BUFFER
    data = 0
    checksum = 0
    stop_byte = 0x55
    
    print "MAX_USB_PKT_SIZE: {}".format(MAX_USB_PKT_SIZE)
    print "nbrByteBuf: {}".format(nbrByteBuf)
    #print "buf: {}".format(buf)
    if nbrByteBuf + PROTOCOL_FRAME_NBR_B < MAX_USB_PKT_SIZE: #MAX_1_CHUNK_SIZE:
        #only 1 chunk for the whole frame to send
        #send_nbr_frame(1)
        cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
            + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
            + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF)
            + buf
            + chr(checksum&0xFF) + chr(stop_byte&0xFF))
        gw.zynq.write_raw(cmd)
        #print hex(ord(cmd))
        #print "", ', '.join(str(hex(ord(c))) for c in cmd[-1040:-1])
    else:
    #send several chunks
        #calculate a float nbrByteBuf then rounding it upper
        chunkNbr = math.ceil((nbrByteBuf + PROTOCOL_FRAME_NBR_B) / float(MAX_USB_PKT_SIZE))
        print "chunkNBR={}".format(chunkNbr)
        #check that the protocol byte cost request 1 more chunk
        #if chunkNbr*MAX_USB_PKT_SIZE < nbrByteBuf:
        #    chunkNbr += 1
        #print "chunkNBR={}".format(chunkNbr)
        
         #send command to advise next command is for a payload split on few chunks
        #send_nbr_frame(nbrByteBuf)
            
        for i in cfor(0,lambda i: i < chunkNbr,lambda i: i + 1):
        #for i in cfor(0: i < chunkNBR: i + 1):
            slice1 = (i - 1) * MAX_USB_PKT_SIZE + MAX_1ST_CHUNK_SIZE   #MAX_1_CHUNK_SIZE + MAX_1ST_CHUNK_SIZE
            #1st chunk
            if i == 0:
                cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
                        + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                        + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF))
                #print "", ', '.join(str(hex(ord(c))) for c in cmd[-20:-1])
                #sys.exit()
                if MAX_1ST_CHUNK_SIZE >= 1:
                       cmd = cmd + buf[0 : MAX_1ST_CHUNK_SIZE] # end slice is n - 1
                       #for g in cfor(0,lambda g: g < MAX_1ST_CHUNK_SIZE,lambda g: g + 1):
                       #    print "buf[{}]={}".format(g, hex(ord(buf[g])))
                if DEBUG:
                    #debugFileRef.write("ASCII value 1st chunk:")
                    print "ASCII value 1st chunk: {} {}".format(hex(ord(cmd[-1])), hex(ord(cmd[-2])))
                    #print "", ', '.join(str(hex(ord(c))) for c in cmd[-10:-1])
                    debugFileRef.write(cmd)
                    #print "ASCII value 1st chunk: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else:
                    print "PROTOCOL_HEAD_SIZE={} MAX_1ST_CHUNK_SIZE={}".format(PROTOCOL_HEAD_SIZE, MAX_1ST_CHUNK_SIZE)
                    print "send chunk 0 to USB"
                    gw.zynq.write_raw(cmd)
                    #sys.exit()
            #last chunk
            elif i == (chunkNbr - 1):
            #TODO seems case +1 chunk to mangage checksum and stopbyte not taken into account here
                if i == 1: #only 2 chunks
                    cmd = (buf[MAX_1ST_CHUNK_SIZE : nbrByteBuf]
                       + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                else:
                    cmd = (buf[slice1 : nbrByteBuf]
                           + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                if DEBUG:
                    print "ASCII value last chunk: chunk N{}".format(i)
                                        #debugFileRef.write("ASCII value last chunk:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send last chunk to USB"
                    gw.zynq.write_raw(cmd)
            #chunk other than 1st or last
            else:
                #print "i={} slice1={} slice2={}".format(i, (i - 1) * MAX_1_CHUNK_SIZE) + MAX_1ST_CHUNK_SIZE,)
                slice2 = slice1 + MAX_USB_PKT_SIZE
                cmd = (buf[slice1 : slice2])
                if DEBUG:
                    print "ASCII value mains chunks: chunk N{}".format(i)
                    #debugFileRef.write("ASCII value mains chunks:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send chunk {} to USB".format(i)
                    gw.zynq.write_raw(cmd)
    """
    # send second packet
    cmd = chr(data&0xFF) + chr(checksum&0xFF) + chr(stop_byte&0xFF)
    gw.zynq.write_raw(cmd)   
    # send first packet
    cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
    cmd = cmd + chr(nbrByteBuf&0xFF) + chr(nbrByteBuf&0xFF>>8)
    gw.zynq.write_raw(cmd)
    
    # send second packet
    cmd = chr(data&0xFF) + chr(checksum&0xFF) + chr(stop_byte&0xFF)
    gw.zynq.write_raw(cmd)
    """
    debugFileRef.close()
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
