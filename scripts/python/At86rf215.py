from card import *
from At86rf215_def import *
from time import sleep

class At86rf215(Card):
    
    def __init__(self, baseAddr, protocol):
        self.baseAddr = baseAddr
        Card.__init__(self, self.baseAddr, protocol)
        
    # atreb215 reset via special register access (dedicated pin is not used to perform a reset operation)
    def reset(self):
        print "RF Module> reset..."
        Card.write(self, AT86RF215_RF_RST, AT86RF215_RF_RST_MASK)
        sleep(0.01)
        current_state = self.read_current_state()
        print "rf09 current state : " + hex(current_state)
        
    # switch current state
    def go_state(self, next_state):
        Card.write(self, AT86RF215_RF09_CMD, next_state)
        sleep(0.01)
        current_state = self.read_current_state()
        print "rf09 current state : " + hex(current_state)
        
    # get the current state
    def read_current_state(self):
        reg = Card.read(self, AT86RF215_RF09_STATE, 1, 0)
        current_state = (reg['val'])
        return current_state
    
    # read interrupts register
    def read_irq_reg(self):
        reg = Card.read(self, AT86RF215_RF09_IRQS, 1, 1)
        irq = (reg['val'])
        return irq
        
    # enable all interrupts
    def irq_enable(self):
        print "RF Module> enable interrupts..."
        Card.write(self, AT86RF215_RF09_IRQM, IRQM_ALL_MASK)
          
    # Enable I/Q radio mode
    def IQ_radio_enable(self):
        print "RF Module> enable I/Q radio mode..."
        Card.write(self, AT86RF215_RF_IQIFC1, 0x12)
        
    # Configure the Transmitter Frontend
    def transmitter_frontend_config(self):
        print "RF Module> transmitter analog frontend configuration..."
        Card.write(self, AT86RF215_RF09_TXCUTC, 0x0B)
        print "RF Module> transmitter digital frontend configuration..."
        Card.write(self, AT86RF215_RF09_TXDFE, 0x41)

    def receiver_frontend_config(self):
        print "RF Module> Receiver frontend configuration"
        Card.write(self, AT86RF215_RF09_RXBWC, 0x0B)
        Card.write(self, AT86RF215_RF09_RXDFE, 0x41)
        reg = self.read_agcc_register()
        Card.write(self, AT86RF215_RF09_AGCC, (reg & 0x80) | 0x01)
        Card.write(self, AT86RF215_RF09_AGCS, 0x77)
        
    def read_agcc_register(self):
        reg = Card.read(self, AT86RF215_RF09_AGCC, 1, 0)
        conf = (reg['val'])
        return conf
    
    def read_a_register(self, register):
        reg = Card.read(self, register, 1, 0)
        conf = (reg['val'])
        return conf

    # Configure the channel parameters : If register RFn_CNM is not written the channel frequency is not changed. The
    # register RFn_CNM must always be written last.  
    # -> suggested prog. sequence is CS, CCF0, CNL, CNM
    def channel_config(self):  
        print "RF Module> channel configuration..."
        Card.write(self, AT86RF215_RF09_CS, 0x30)
        Card.write(self, AT86RF215_RF09_CCF0H, 0x8D)
        Card.write(self, AT86RF215_RF09_CCF0L, 0x20)
        Card.write(self, AT86RF215_RF09_CNL, 0x00)
        Card.write(self, AT86RF215_RF09_CNM, 0x00)
                
    # Configure the channel parameters
    def transmit_power_config(self, pacur, txpwr):  
        print "RF Module> power configuration..."
        value = ((pacur << 5) & 0x60) | (txpwr & 0x1F)
        Card.write(self, AT86RF215_RF09_PAC, value)
        Card.write(self, AT86RF215_RF09_AUXS, 0x02)
        
        
    # configure I/Q data interface
    def configure_IQ_data_interface(self):  
        print "RF Module> configure IQ data interface..."
        Card.write(self, AT86RF215_RF_IQIFC0, 0x2e)      
        
    # active loop back only to test I/Q data interface, If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.
    def loopback_enable(self, flag):
        if flag == EXTLB_LOOPBACK_ENABLE:
            print "RF Module> enable loop-back..."
            reg = Card.read(self, AT86RF215_RF_IQIFC0, 1, 0)
            data = (reg['val']) | EXTLB_MASK
            Card.write(self, AT86RF215_RF_IQIFC0, data)
        else:
            print "RF Module> disable loop-back..."
            reg = Card.read(self, AT86RF215_RF_IQIFC0, 1, 0)
            data = (reg['val']) & (~EXTLB_MASK)
            Card.write(self, AT86RF215_RF_IQIFC0, data)
    # A value of one forces the AGC to freeze to its current value. A value of zero releases the AGC. 
    def agc_freeze_control(self, flag):
        if flag == FREEZE_AGC:
            print "RF Module> force the AGC freeze to its current value..."
            reg = Card.read(self, AT86RF215_RF09_AGCC, 1, 0)
            data = (reg['val']) | FRZC_MASK
            Card.write(self, AT86RF215_RF09_AGCC, data)
        else:    
            print "RF Module> releases the AGC..."
            reg = Card.read(self, AT86RF215_RF09_AGCC, 1, 0)
            data = (reg['val']) & (~FRZC_MASK)
            Card.write(self, AT86RF215_RF09_AGCC, data)
            
    def set_transmitter_as_iq_radio_mode(self):
        # config
        pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
        txpwr = 0xF                         # TXPWR controls the transmit output power setting
        loopback = EXTLB_LOOPBACK_DISABLE   # If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.
        
        self.reset()
        self.irq_enable()
        self.IQ_radio_enable()
        self.transmitter_frontend_config()
        self.channel_config()
        self.transmit_power_config(pacur, txpwr)
        self.configure_IQ_data_interface()
        self.loopback_enable(loopback)
        
        self.go_state(RF_STATE_TXPREP)
        self.go_state(RF_STATE_TX)
        current_state = self.read_current_state()
        print "Transmitter current state : " + hex(current_state)
        
        
    def set_receiver_as_iq_radio_mode(self):
        # config
        agc_status = FREEZE_AGC
        
        self.reset()
        self.irq_enable()
        self.IQ_radio_enable()
        self.receiver_frontend_config()
        self.channel_config()
        self.configure_IQ_data_interface()
            
        self.go_state(RF_STATE_TXPREP)
        while (self.read_irq_reg() != IRQS_TRXRDY_MASK):
            pass
        
        self.go_state(RF_STATE_RX)
        current_state = self.read_current_state()
        print "rf19 current state : " + hex(current_state)
        self.agc_freeze_control(agc_status)
        
    def read_rx_config_registers(self):
        RXBWC_val = self.read_a_register(AT86RF215_RF09_RXBWC)
        RXDFE_val = self.read_a_register(AT86RF215_RF09_RXDFE)
        AGCC_val = self.read_a_register(AT86RF215_RF09_AGCC)
        AGCS_val = self.read_a_register(AT86RF215_RF09_AGCS)
        CCF0L_val = self.read_a_register(AT86RF215_RF09_CCF0L)
        CCF0H_val = self.read_a_register(AT86RF215_RF09_CCF0H)
        print "RXBWC_val = " + hex(RXBWC_val)
        print "RXDFE_val = " + hex(RXDFE_val)
        print "AGCC_val = " + hex(AGCC_val)
        print "AGCS_val = " + hex(AGCS_val)
        print "CCF0H_val = " + hex(CCF0H_val) + " CCF0L_val = " + hex(CCF0L_val) + " in frequency = {}".format((int(CCF0H_val * 256) + int(CCF0L_val))*25000)
        
    def set_manual_AGC_gain(self, gain):
        # get data from zynq
        AGCC_val = self.read_a_register(AT86RF215_RF09_AGCC)
        AGCC_val = AGCC_val & (~(AGCC_EN_MASK) &0xFF)
        Card.write(self, AT86RF215_RF09_AGCC, AGCC_val)
        AGCS_val = self.read_a_register(AT86RF215_RF09_AGCS)
        if gain > 23 or gain < 0:
            print "WARNING gain is out of its range [0:23], currently set to 3dB"
            gain = 3
        AGCS_val = (AGCS_val & ~AGCS_GCW_MASK) | (gain & AGCS_GCW_MASK)#set to gain
        Card.write(self, AT86RF215_RF09_AGCS, AGCS_val)
    

    
