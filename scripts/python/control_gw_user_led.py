#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    config_gw_power_user_leds = 0b11110110
    gw.zynq.gw_power_user_leds(config_gw_power_user_leds)
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()