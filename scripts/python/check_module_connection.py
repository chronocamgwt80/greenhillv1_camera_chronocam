#!/usr/bin/python

import time
from greenhill import *
    
def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.check_module_connection()
except KeyboardInterrupt:
    exit_script()
