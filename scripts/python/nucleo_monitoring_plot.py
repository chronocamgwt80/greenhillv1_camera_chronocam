#!/usr/bin/python
from greenhill import *
from nucleo_def import *
import time

def exit_script():
    gw.kill()
    
try:
    # Monitoring config
    monitoring_acquistion_time_ms = MONITORING_ACQ_TIME_MAX_MS
    # RF modules config
    rf1_pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    rf1_txpwr = 0xF                         # TXPWR controls the transmit output power setting
    rf1_loopback = EXTLB_LOOPBACK_DISABLE    # If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.
    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE
    
    gw = Greenhill(USB_PROTOCOL)
    gw.pa1.reset()
    gw.zynq.reset()    
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(rf1_pacur, rf1_txpwr)
    gw.rf1.configure_IQ_data_interface()
    print "Debug> Start Monitoring acquisition..."
    gw.nucleo.start_monitoring_acq(monitoring_acquistion_time_ms)
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    gw.rf1.go_state(RF_STATE_TX)
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    gw.zynq.start_rf_tx_self_test()
    
    acq_flag = MONITORING_ACQ_ENABLE
    time.sleep(monitoring_acquistion_time_ms/1000) # monitoring acquisition estimation time
    while (acq_flag == MONITORING_ACQ_ENABLE):
        acq_flag, dots_index = gw.nucleo.read_monitoring_acq_flag()
        print "Debug> dots index = " + str(dots_index) + "/" + str(monitoring_acquistion_time_ms)
    gw.nucleo.plot_monitoring_acq(monitoring_acquistion_time_ms)
    

    exit_script()
    while(1):
        pass
    
except KeyboardInterrupt:
    exit_script()