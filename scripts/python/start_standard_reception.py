#!/usr/bin/python

import time
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    # RF RX config
    module_selected = ATMEL
    acquisition_time_ns = RF_RX_ACQ_TIME_NS
    # RF modules config
    agc_status = FREEZE_AGC
    # PA config
    pa1_mode = PA_RECEIVE_BYPASS
    pa1_antenna = PA_ANT1_EN
    # Zynq config
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_RX_MODE
    
    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set RF modules as RX
    # 3. Set PA as TR
    # 4. Set Antenna Switch as RX
    # 5. Start Reception
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)

    # 1. Reset all modules/elements
    gw.antenna_switch1.set_antenna_switch_config(ANTENNA_SWITCH_TX_MODE)
    gw.pa1.reset()
    gw.zynq.reset()
    
    # 2. Set RF modules as RX
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.receiver_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.configure_IQ_data_interface()
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    gw.rf1.go_state(RF_STATE_RX)
    current_state = gw.rf1.read_current_state()
    print "rf19 current state : " + hex(current_state)
    gw.rf1.agc_freeze_control(agc_status)
    
    
    # 3. Set PA as TR
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    
    # 4. Set Antenna Switch as RX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 5. Start Reception
    gw.zynq.rf_rx_acq_apply_config(module_selected, acquisition_time_ns) 
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_acq_start()
    time.sleep(1)
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_read_sync_flag()
    gw.zynq.rf_rx_acq_read_samples_acq(acquisition_time_ns, ACQ_OUTPUT_STANDARD)       
    exit_script()
     
except KeyboardInterrupt:
    exit_script()