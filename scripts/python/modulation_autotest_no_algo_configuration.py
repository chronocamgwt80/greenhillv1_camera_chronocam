#!/usr/bin/python

#This script execute the modulation algorithm but do not send them to the RF
#It could be use to test the specification step 0:
#Step0: automatic generation of a bitstream (No PC load), implement modulate bitstream and transmit frame commands
# in the configuration where the bitstream is bigger or equal than the generated frame needs, and only one frame 
#is sent. In this config, some bitstream bits are not sent.
#Steps :
#0 compile the zynq firmware, file gw_modulator_and_autotest.c with this define set : TEST_MATCH_PC_MOD
#1 execute: bash zynq_gwt_configure.sh -M 2 -n 64 -p 16 -t 8 -c 1 -d 0 -m 8 -g 0.32
#2 execute: python modulation_autotest_no_algo_configuration.py

# import
#from sys import *
import sys
#from At86rf215 import *
#from zynq import *
from greenhill import *
from zynq_def import *
from ofdm import *
import math, getopt

def exit_script():
    gw.kill()

try:
    #dictionary for the TX power 
    #AT86RF215 p206: Figure 11-2. Output power vs. RF09_PAX.TXPWR register for several modulations at f channel =900MHz
    #curve of OFDM n 3 and n 4
    pwr = {'m19': 0, 'm18': 1, 'm17': 2, 'm16': 3, 'm15': 4, 'm14': 5, 'm13': 6, 'm12': 7, 'm11': 8, 'm10': 9, 'm9': 10, 'm8': 12, 'm7': 13, 'm6': 14, 'm5': 15, 'm4': 16, 'm3': 17, 'm2': 18, 'm1': 19, '0': 20, '1': 21, '2': 22, '3': 23, '4': 24, '5': 25, '6': 26, '7': 27, '8': 28, '9': 29, '10': 30, '11': 31} 
    pacur = 3
    tx_pwr = 0
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_ENABLE

    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE
    
    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set Antenna Switch as TX
    # 3. Set PA as TX
    # 4. Set RF modules as TX
    # 5. Start Transmission
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_get_buffer_index()
    # 1. reset
    gw.rf1.reset()
    gw.pa1.reset()
    gw.zynq.rf_tx_get_buffer_index()
    #gw.zynq.reset()
   
    # 2. Set Antenna Switch as TX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 3. Set PA as TX
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    
    # 4. Set RF modules as TX
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(pacur, tx_pwr)
    gw.rf1.loopback_enable(0)#desactive the loopback
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    #reset the TX block after the RF clock availability
    #other the part of the buffer with the RF clock will not be reseted
    gw.zynq.rf_tx_reset()
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "RF09 current state : " + hex(current_state)
    #print "value auxs regiter=%d" %(gw.rf1.read_auxs_register())
    #transmitter.kill()

    gw.zynq.reset()
    
    #3 read IQ file and fill the fifo with it
    #zynq = Zynq()
    #read_IQ_file_fill_fifo(csv_file)
    
    #4 Configure in loop mode the buffer
    #zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
    time.sleep(2)
    #configure()
    time.sleep(1)
    #gw.zynq.rf_tx_enable_check_modulation_data()
    time.sleep(1)
    gw.zynq.rf_tx_start_modulation(modulation_mode)
    time.sleep(10)
    
    #5 start RF TX
    #gw.zynq.start_rf_transmission()
    
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
