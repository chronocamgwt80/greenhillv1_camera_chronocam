from card import *
from pa_def import *
from nucleo_def import *

class Pa(Card):
    
    def __init__(self, pa_id, protocol):
        self.baseAddr = NUCLEO_CMD_BASEADDR  
        Card.__init__(self, self.baseAddr, protocol)
        self.pa_id = pa_id
    
    def reset(self):
        self.set_io(PA_SLEEP, PA_ANT1_EN)
        
    def set_io(self, mode, ant_sel):
        # see Table 9. SKY65362-11 Mode Control Logic (Note 1)
        # Bit -   IO
        # ---------------
        # [0]     - ANT_SEL (R/W)
        # [1 . 3] - MODE    (R/W)
        # [4]     - IALARM  (read only)
        # ---------------
        # PA_SLEEP = 0x0
        # PA_RECEIVE_BYPASS = 0x2
        # PA_RECEIVE_LNA = 0x6
        # PA_TRANSMIT = 0x3 or 0x7
        # ---------------
        # PA_ANT1_EN = 0x0
        # PA_ANT2_EN = 0x1

        config = (ant_sel & 0x1) | ((mode & 0x7) << 1)
        if self.pa_id == PA_0:
            Card.write(self, NUCLEO_CMD_PA0_IO, config)
        elif self.pa_id == PA_1:
            Card.write(self, NUCLEO_CMD_PA1_IO, config)
        else:
            print "PA> bad PA ID..."   
    
    def read_io(self): 
        # see Table 9. SKY65362-11 Mode Control Logic (Note 1)
        # Bit -   IO
        # ---------------
        # [0]     - ANT_SEL (R/W)
        # [1 . 3] - MODE    (R/W)
        # [4]     - IALARM  (read only)
        # ---------------
        # PA_SLEEP = 0x0
        # PA_RECEIVE_BYPASS = 0x2
        # PA_RECEIVE_LNA = 0x6
        # PA_TRANSMIT = 0x3 or 0x7
        # ---------------
        # PA_ANT1_EN = 0x0
        # PA_ANT2_EN = 0x1
        if self.pa_id == PA_0:
            data = Card.read(self, NUCLEO_CMD_PA0_IO, 1, 0)
        elif self.pa_id == PA_1:
            data = Card.read(self, NUCLEO_CMD_PA1_IO, 1, 0)
        else:
            print "PA> bad PA ID..." 
        ant_sel = (data['buf'][FRAME_HEADER_SIZE] & 0x1)
        mode = ((data['buf'][FRAME_HEADER_SIZE] >> 1) & 0x7)
        ialarm = ((data['buf'][FRAME_HEADER_SIZE] >> 4) & 0x1)
        print 'PA> PA_' + str(self.pa_id) 
        print 'PA> .ant_sel: ' + str(ant_sel)
        print 'PA> .mode: ' + str(mode)
        print 'PA> .ialarm: ' + str(ialarm)
        return {'ant_sel':ant_sel, 'mode':mode, 'ialarm':ialarm }
            
            
        
        

        
          
        
    