#!/usr/bin/python
from greenhill import *
from zynq_def import *
import sys
import math, getopt, locale

def exit_script():
    gw.kill()

def usage():
  print "USAGE:"
  print "This script configure the flag in the zynq indicated if command executed by the zynq comes from python."
  print "Purpose is for long time running commands such as start_modulation to have a mecanism to warn when this command is over"
  print "This flag allows a UART printf to warn the modulation is over and so that the zynq is ready to accept other command. MUTEX should have been implemented but pyhton command are only for debug so the effort on this is limited."
  print "options list"
  print "\t -p  : expect a value of 0 for command not from python or 1 for command coming from python"
  print "\t -h or -help  : print this usage"
  print "\t example: ./rf_set_mod_python.py -p 1"
  exit(0)

try:
    myopts, args = getopt.getopt(sys.argv[1:], "p:h", ["help"])
except getopt.GetoptError as e:
    print "bad option in command line"
    print (str(e));print ""
    usage()
    sys.exit(2)
try:
    if len(myopts) == 0:
        usage()
        exit(2) 
    else:
        for opt, arg in myopts:
            if opt in ('-p'):
                param = locale.atoi(arg)
                if param != 1 and param != 0:
                  print "parameters -p %d is different from 0 or 1" %(param)
                  usage()
                  exit(3)
            elif opt in ('-h', '--help'):
                usage()
            else:
                print "bad option in command line"
                for a in sys.argv: 
                    print a
                exit(2)
    
    gw = Greenhill(USB_PROTOCOL)
    #program the cmd_from_python flag
    gw.zynq.set_cmd_from_python(param)
    #read the cmd_from python flag to print it is well set
    gw.zynq.get_cmd_from_python()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
