MAX_FRAME_NBBYTES             = 1024 * 1024 * 2
FRAME_HEADER_ADDRESS_SIZE     = 4
FRAME_HEADER_NBBYTES_SIZE     = 4
FRAME_FOOTER_CHECKSUM_SIZE    = 1
FRAME_FOOTER_STOP_BYTE_SIZE   = 1
FRAME_HEADER_SIZE             = (FRAME_HEADER_ADDRESS_SIZE + FRAME_HEADER_NBBYTES_SIZE)
FRAME_FOOTER_SIZE             = (FRAME_FOOTER_CHECKSUM_SIZE + FRAME_FOOTER_STOP_BYTE_SIZE)
FRAME_OVERHEAD_SIZE           = (FRAME_HEADER_SIZE+FRAME_FOOTER_SIZE)
FRAME_DEFAULT_CHECKSUM        = 0
FRAME_STOP_BYTE               = 0x55
FRAME_RD_WRB_BIT_MASK         = 0x80000000

class Card():
    def __init__(self, baseAddr, protocol):
        self.baseAddr = baseAddr
        self.protocol = protocol
    
    # send a read command then wait for the response
    def read(self, cmd_id, nbbytes, toconsole):
        return self.protocol.read( (self.baseAddr + cmd_id), nbbytes, toconsole)
    
    # wait for "nbbytes" data
    def read_data(self, nbbytes):
        return self.protocol.read_data(nbbytes)

    # send a write command where data = 1 byte
    def write(self, cmd_id, value):
        self.protocol.write( (self.baseAddr + cmd_id), value) 

    # send a write command where data = 'nbbytes' bytes
    def write_buf(self, cmd_id, data_chr, nbbytes):
        self.protocol.write_buf( (self.baseAddr + cmd_id), data_chr, nbbytes) 
        
    # send data supplied by the user
    def write_raw(self, data_chr): 
        self.protocol.write_raw(data_chr)
        