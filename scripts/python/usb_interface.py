#!/usr/bin/python
import usb.core
import usb.util
import sys
import time 
import Atreb215_def

#AT86RF215_RF_RST=0x0005
HEADER_SIZE = 6
FOOTER_SIZE = 2
OVERHEAD_SIZE = HEADER_SIZE + FOOTER_SIZE

def exit_script():
    gw.kill()
    sys.exit()
#
class GWBoard:
    def __init__(self, idVendor=0x5151, idProduct=0x0001):
        # find our device
        self.dev = usb.core.find(idVendor=0x5151, idProduct=0x0001)
        if self.dev is None:
            raise ValueError('USB- Device not found')
        else:
            print 'USB- Device found'
        if self.dev.is_kernel_driver_active(0):
            try:
                self.dev.detach_kernel_driver(0)
                print "USB- kernel driver detached"
            except usb.core.USBError as e:
                sys.exit("USB- Could not detach kernel driver: %s" % str(e)) 
        else:
            print "USB- kernel already driver detached"
        try:
            usb.util.claim_interface(self.dev, 0)
            print "USB- device claimed"
        except:
            sys.exit("USB- Could not claim the device: %s" % str(e)) 
#    def write_reg(self,add,value):
#        write_cmd = chr((add>>24)&0xFF) + chr((add>>16)&0xFF) + chr((add>>8)&0xFF) + chr((add)&0xFF)
#        nbbytes = chr(0) + chr(4)
#        data = chr((value>>24)&0xFF) + chr((value>>16)&0xFF) + chr((value>>8)&0xFF) + chr((value)&0xFF)
#        end_cmd = chr(0x55)
#        cmd = write_cmd + nbbytes + data + end_cmd; 
#        self.dev.write(2,cmd,interface=0,timeout=1)
#        time.sleep(0.1)
    def write_reg(self,add,value,regnbbytes):
        write_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        nbbytes = chr(1) + chr(0)
        data = chr((value)&0xFF)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = write_cmd + nbbytes + data + checksum + end_cmd; 
        self.dev.write(2,cmd,interface=0,timeout=100)
        time.sleep(0.1)   
    def read_reg(self,add,regnbbytes,toconsole): 
        read_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr(0x80 + ((add >> 24)&0xFF))
        nbbytes = chr((regnbbytes)&0xFF) + chr((regnbbytes>>8)&0xFF)
#        data = chr(0) + chr(0) + chr(0) + chr(0)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = read_cmd + nbbytes + checksum + end_cmd;
        self.dev.write(2,cmd,interface=0,timeout=100)
        time.sleep(0.1)
        d=self.dev.read(0x82,OVERHEAD_SIZE+regnbbytes,timeout=100)
        time.sleep(0.1)
        add = int(d[0]) + int(d[1]<<8) + int(d[2]<<16) + int(d[3]<<24)
        nbbytes = int(d[4]) + int(d[5]<<8)
        val = 0
        valhex = ''
        for k in range(0,regnbbytes):
            valhex = valhex + hex(d[k+HEADER_SIZE])[2:] + ' '
            val = val + (d[k+HEADER_SIZE] << k) 
        if (toconsole):
            print "register (" + hex(add) + ") = 0x" + valhex
        return {'add':add,'val':val}
    def kill(self):
        usb.util.release_interface(self.dev, 0)
        del self


# atreb215 reset via special register access (dedicated pin is not used to perform a reset operation)
def at86rf215_reset_register():
    print "Atmel Chip reset..."
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF_RST), 0x07,1)
    # [2:0] : CMD, writing 0x07 in this register triggers the reset procedure
    my_data=0x00
    while(my_data!=0x01):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQS), 1, 1)
        my_data=(reg['val'])&0x01
        # [5:0] : interruption request indicator (end of reset is expected)
    print "Chip reset completed."

# atreb215 general initialization
# - I/Q operation mode without using the baseband cores
# - both radio cores are put in TRXOFF state (idle)
# - frontend configuration
# - frequency settings for both radio cores (local oscillator, channel settings)
def at86rf215_general_init():
    print "Chip general initialization..."
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC1), 0x12, 1)
    # [7] : FAILSF, indicates that the LVDS receiver is in failsafe mode, READ only
    # [6:4] : CHPM, working mode of the chip (I/Q interface only, both baseband cores are disabled)
    # [1:0] : SKEWDRV, alignement of the I/Q data interface RXD signal edges relative to the RXCLK clock edges (3.906 ns)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC0), 0x16, 1)
    # [7] : EXTLB, enables the loopback functionality (disabled)
    # [6] : SF, indicates whether the data stream of the I/Q data interface is synchronized correctlty, READ only
    # [5:4] : DRV, configures the I/Q data interface driver output current (2 mA)
    # [3:2] : CMV, common mode voltage of the I/Q data interface signals (has no effect here, according to [1])
    # [1] : CMV1V2, common mode voltage of the I/Q data interface signals is set to 1.2 V
    # [0] : EEC, embedded control (disabled)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), 0x02, 1)
    # [2:0] : CMD, transceiver state order (go to TRXOFF)
    my_data=0x00
    while(my_data!=0x02):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
        my_data=int(reg['val'])
        # [2:0] : current transceiver state (TRXOFF is expected)
    print "  RF09 current state : " + hex(my_data)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CMD), 0x02, 1)
    # [2:0] : CMD, transceiver state order (go to TRXOFF)
    my_data=0x00
    while(my_data!=0x02):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_STATE), 1, 1)
        my_data=int(reg['val'])
        # [2:0] : current transceiver state (TRXOFF is expected)
    print "  RF24 current state : " + hex(my_data)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_AUXS), 0xC2, 1)
    # [7] : EXTLNABYP, AGC scheme where the external LNA can be bypassed using FEAn/FEBn (enabled)
    # [6:5] : AGCMAP, specific AGC gain mapping according to the external LNA gain (+12 dB)
    # [4] : AVEXT, external AVDD supply (disabled)
    # [3] : AVEN, analog voltage regulator turns on in state TRXOFF (disabled)
    # [2] : AVS, this bit indicates that the analog voltage has settled, READ only
    # [1:0] : PAVC, supply voltage of the internal power amplifier (2.4 V)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_PADFE), 0x40, 1)
    # [7:6] : PADFE, configuration of the frontend control pins FEAn/FEBn (configuration 1)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQM), 0x00, 1)
    # [5:0] : see IRQS register, this is a mask to display the interrupts on the IRQ pin (output is disabled)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CS), 0x08, 1)
    # [7:0] : CS, channel spacing (0x08 * 25 kHz) (200 kHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CCF0H), 0x43, 1)
    # [7:0] : CCF0H, local oscillator frequency (433 MHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CCF0L), 0xA8, 1)
    # [7:0] : CCF0L, local oscillator frequency (433 MHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CNL), 0x00, 1)
    # [7:0] : CNL, channel index (0)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CNM), 0x00, 1)
    # [7:6] : CM, channel mode (IEEE compliant scheme)
    # [0] : CNH, channel index (0)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_AUXS), 0x02, 1)
    # [7] : EXTLNABYP, AGC scheme where the external LNA can be bypassed using FEAn/FEBn (disabled)
    # [6:5] : AGCMAP, specific AGC gain mapping according to the external LNA gain (disabled)
    # [4] : AVEXT, external AVDD supply (disabled)
    # [3] : AVEN, analog voltage regulator turns on in state TRXOFF (disabled)
    # [2] : AVS, this bit indicates that the analog voltage has settled, READ only
    # [1:0] : PAVC, supply voltage of the internal power amplifier (2.4 V)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_PADFE), 0x00, 1)
    # [7:6] : PADFE, configuration of the frontend control pins FEAn/FEBn (configuration 0)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_IRQM), 0x00, 1)
    # [5:0] : see IRQS register, this is a mask to display the interrupts on the IRQ pin (output is disabled)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CS), 0x08, 1)
    # [7:0] : CS, channel spacing (0x08 * 25 kHz) (200 kHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CCF0H), 0x8C, 1)
    # [7:0] : CCF0H, local oscillator frequency (2404 MHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CCF0L), 0xF8, 1)
    # [7:0] : CCF0L, local oscillator frequency (2404 MHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CNL), 0x00, 1)
    # [7:0] : CNL, channel index (0)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF24_CNM), 0x00, 1)
    # [7:6] : CM, channel mode (IEEE compliant scheme)
    # [0] : CNH, channel index (0)
    print "Chip general initialization completed."

# atreb215 sub-1 GHz radio transmitter initialization
# - various PA settings
# - radio core is put in TXPREP state (ready to transmit)
# - digital link configuration   
def at86rf215_tx09_init():
    
    print "sub-1 GHz transmitter initialization..."
   
    print "chip reseting, go to TRXOFF mode"
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), 0x02, 1)
    # [2:0] : CMD, transceiver state order (go to TRXOFF)
    my_data=0x00
    while(my_data!=Atreb215_def.RF_TRXOFF):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
        my_data=(reg['val'])
        # [2:0] : current transceiver state (TRXOFF is expected)
    print "  RF09 current state : " + hex(my_data)
   
    print "I/Q radio enabling"
    reg = gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC0), 0, 1)
    my_data=(reg['val'])
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC0), (my_data | 0x10), 1)
   
   
    print "Transmitter analog frontend configuration"
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_TXCUTC), 0x08, 1)
    # [7:6] : PARAMP, ramp up/down time of the PA (4 us)
    # [3:0] : LPFCUT, transmitter LP filter cut-off frequency (500 kHz)
   
    print "Transmitter digital frontend configuration"
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_TXDFE), 0x01, 1)
    # [7:5] : RCUT, relative cut-off frequency
    # [4] : DM, direct modulation (disabled)
    # [3:0] : SR, sampling frequency of I/Q data stream (4000 kHz)
   
    print "Channel configuration: none"
   
    print "Power amplifier configuration"
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_PAC), 0x0, 1) # current reduction (22mA) and min output power
#     gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_PAC), 0x7F)
    # [6:5] : PACUR, PA DC current (no current reduction)
    # [4:0] : TXPWR, transmitter output power (maximum power)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), 0x03, 1)
    # [2:0] : CMD, transceiver state order (go to TXPREP)
    my_data=0x00
    while((my_data&0x12)==0):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQS), 1, 1)
        my_data=(reg['val'])
        # [5:0] : interruption request indicator (error or TXPREP transisition expected)
    print "  Entering TXPREP with interrupt : " + hex(my_data)
    reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
    my_data=(reg['val'])
    print "  RF09 current state : " + hex(my_data)
    print "sub-1 GHz transmitter initialization completed."

# atreb215 sub-1 GHz radio receiver initialization
# - various LNA settings
# - radio core is put in TXPREP state (ready to receive)
# - digital link configuration
def at86rf215_rx09_init():
    print "sub-1 GHz receiver initialization..."
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), Atreb215_def.RF_TRXOFF, 1)
    # [2:0] : CMD, transceiver state order (go to TRXOFF)
    my_data=0x00
    while(my_data != Atreb215_def.RF_TRXOFF):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
        my_data=(reg['val'])
        # [2:0] : current transceiver state (TRXOFF is expected)
    print "  RF09 current state : " + hex(my_data)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_RXBWC), 0x0B, 1)
    # [5] : IFI, inverted-sign IF frequency (disabled)
    # [4] : IFS, the receiver shifts the IF frequency by factor of 1.25 (disabled)
    # [3:0] : BW, receiver filter bandwidth (fbw = 2000 kHz and fif = 2000 kHz)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_RXDFE), 0x01, 1)
    # [7:5] : RCUT, relative cut-off frequency
    # [3:0] : SR, sampling frequency of I/Q data stream (4000 kHz)
    reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_AGCC), 1, 1)
    my_data=(reg['val'])
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_AGCC), (my_data&0x80)|0x01, 1)
    # [6] : AGCI, input signal of the AGC, if zero, the filtered frontend signal is used
    # [5:4] : AVGS, time of averaging RX data samples of the AGC values (8 samples)
    # [3] : RST, resets the AGC and sets the maximum receiver gain (disabled)
    # [2] : FRZS, indicates that the AGC is on hold, READ only
    # [1] : FRZC, forces the AGC to freeze to its current value (disabled)
    # [0] : EN, enables the AGC (enabled)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_AGCS), 0x77, 1)
    # [7:5] : TGT, AGC target level relative to ADC full scale (-30 dB)
    # [4:0] : GCW, receiver gain setting (maximum gain)
    gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), Atreb215_def.RF_TXPREP, 1)
    # [2:0] : CMD, transceiver state order (go to TXPREP)
    my_data=0x00
    while((my_data&0x12)==0):
        reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQS), 1, 1)
        my_data=(reg['val'])
        # [5:0] : interruption request indicator (error or TXPREP transisition expected)
    print "  Entering TXPREP with interrupt : " + hex(my_data)
    reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
    my_data=(reg['val'])
    print "  RF09 current state : " + hex(my_data)
    print "sub-1 GHz receiver initialization completed."



to_console = 1
not_to_console = 0
try:
    ## greenhill board instance
    gw = GWBoard()
    ## FPGA interface, switch green led on
    #gw.write_reg(0x00100002, 0x0, 1)  
    #gw.write_reg(0x00100001, 0x12, 1)  
    #gw.read_reg(0x00100001,7, to_console)
#     keyb=raw_input("Press enter switch the led off")
    #gw.write_reg(0x01000000, 0x01, 1)
    #time.sleep(1)
    #exit()
    #while 1:
    #    pass
    ## Atmel interface
#     at86rf215_reset_register()
#     at86rf215_general_init()
#     at86rf215_tx09_init()
#     time.sleep(5.0)
#     
#     
# #   # go to TX state
#     gw.write_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_CMD), Atreb215_def.RF_TX, 1)
#     reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_STATE), 1, 1)
#     
#     my_data=(reg['val'])
#     print "RF09 current state : " + hex(my_data)
#

#     while 1:
#         reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_TXCI), 1, 1)
#         my_data = (reg['val'])
#         print "TXCI = : " + hex(my_data)  
#         reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_TXCQ), 1, 1)
#         my_data = (reg['val'])
#         print "TXCQ = : " + hex(my_data)  
#         time.sleep(1)

    while 1:
        gw.write_reg(0x100002, 0x0, 1)
        time.sleep(1) 
#         while 1: 
#             pass
#         
#     reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC0), 1, not_to_console)
#     my_data = ((reg['val']) >> 6) & 0x1
#     print "IQIFC0.SF = : " + hex(my_data)  
#     reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF_IQIFC2), 1, not_to_console)
#     my_data = ((reg['val']) >> 7) & 0x1
#     print "IQIFC2.SYNC = : " + hex(my_data)  
#     reg=gw.read_reg(0x00010000+(Atreb215_def.AT86RF215_RF09_IRQS), 1, not_to_console)
#     my_data = ((reg['val']) >> 5) & 0x1
#     print "IRQS.IQIFSF = : " + hex(my_data)
    
    exit_script()
    
    
#     while(1):
#         pass
    
except KeyboardInterrupt:
    exit_script()
#  dev.detach_kernel_driver(0)
    #usb.util.release_interface(dev, 0)
    
    
    #usb.util.release_interface(dev, 0)
    #dev.set_configuration()
#     cfg=dev.get_active_configuration()
#     intf = cfg[(0,0)]
#     ep = usb.util.find_descriptor(intf,custom_match = \
#                                       lambda e: \
#                                       usb.util.endpoint_direction(e.bEndpointAddress) == \
#                                       usb.util.ENDPOINT_OUT) 
#     print ep
#    dev.libusb_bulk_transfer(dev)
    #endpoint = dev[0][(0,0)][1]
    #endpoint.write(">",timeout = None)
    #dev.write(2, ">",interface=0,timeout=1)
    #time.sleep(0.1)
    #d = dev.read(endpoint.bEndpointAddress,7,timeout=1)