#!/usr/bin/python

import time
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    # RF RX config
    module_selected = ATMEL#ATMEL_EMULATOR
    acquisition_time_ns = RF_RX_ACQ_TIME_NS
    #acquisition_time_ns = 2500 #1 samples is 250ns
    # RF module config
    pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    txpwr = 0x0                         # TXPWR controls the transmit output power setting
    loopback = EXTLB_LOOPBACK_ENABLE    # If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.

    # create API top object
    gw = Greenhill(USB_PROTOCOL)
    
    # reset RF modules
    gw.rf1.reset()
    gw.pa1.reset()
    
    # set RF module as Transmitter
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(pacur, txpwr)
    gw.rf1.configure_IQ_data_interface()
    gw.rf1.loopback_enable(loopback)
    gw.rf1.go_state(RF_STATE_TXPREP)
    #while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
    #    pass
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "Transmitter current state : " + hex(current_state)
    # RF clock is now sent to the FPGA, TX FIFO could be filled  
    
    # start acquisition before sending samples to start to get the first one which is pop out from the TX FIFO
    gw.zynq.reset()
    gw.zynq.rf_rx_acq_apply_config(module_selected, acquisition_time_ns) 
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_acq_start()

    # check synchronization of the deserializer of the FPGA (should be not sync)
    gw.zynq.rf_rx_read_sync_flag()
    
    # launch transmission with Zynq stored CSV pattern
    gw.zynq.start_rf_tx_self_test() 

    # check synchronization of the deserializer of the FPGA (should be sync)
    gw.zynq.rf_rx_read_sync_flag()

    # wait the end of the acquisition
    time.sleep(1)
    
    # read samples acquired
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_acq_read_samples_acq(acquisition_time_ns, ACQ_OUTPUT_STANDARD)
       
    exit()
     
except KeyboardInterrupt:
    exit_script()
