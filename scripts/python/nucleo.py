from card import *
from nucleo_def import *
from ofdm import *
import struct
import re
import pyqtgraph.multiprocess as mp 
import pyqtgraph as pg
import threading

class Nucleo(Card):
    
    def __init__(self, protocol):
        self.baseAddr = NUCLEO_CMD_BASEADDR
        Card.__init__(self, self.baseAddr, protocol)
        
    # send buffer of ofdm IQ to nucleo
    def ofdm_buffer(self):
        #ofdm is an IQ stream object
        ofdm = Ofdm()
        #ensure that buf is a string (could be useless TODO)
        buf=""
        #maybe more interesting from PC to zynk to determine the size of the file
        #and sent everything in 1 packet if not too big versus a USB packet
        nbrByte = 64
        #read from a csv file nbrByte of the IQ stream and send it to the nucleo
        ofdm.ofdm_read_file_return_buffer(buf, nbrByte)
        Card.write_buf(self, NUCLEO_CMD_OFDM_BUFFER, buf, nbrByte)
        
    def get_firmware_version(self):
        data = Card.read(self, NUCLEO_CMD_GET_FIRMWARE_VERSION, 4, 0)
        sw_version = data['val']
        print "Nucleo> SWv: " + hex(sw_version)   
        return sw_version
    
    def start_monitoring_acq(self, acquisition_time_ms):
        dots_nb = int(acquisition_time_ms/MONITORING_TIME_FOR_ONE_DOT_MS)
        data = chr(dots_nb & 0xFF) + chr((dots_nb >> 8) & 0xFF) + chr((dots_nb >> 16) & 0xFF) + chr((dots_nb >> 24) & 0xFF)
        Card.write_buf(self, NUCLEO_CMD_ENABLE_MON_ACQ, data, 4)
        
    def read_monitoring_acq_flag(self):
        # Byte    -  Field
        # --------------------
        # [0]     - acq_flag
        # [1 . 4] - dots_index
        t = Card.read(self, NUCLEO_CMD_ENABLE_MON_ACQ, 5, 0)
        acq_flag = t['buf'][FRAME_HEADER_SIZE + 0]
        dots_index = t['buf'][FRAME_HEADER_SIZE + 1] + (t['buf'][FRAME_HEADER_SIZE + 2] << 8)  + (t['buf'][FRAME_HEADER_SIZE + 3] << 16)  + (t['buf'][FRAME_HEADER_SIZE + 4] << 24)
        return acq_flag, dots_index
    
    def read_monitoring_acq(self, acquisition_time_ms):
        dots_nb = int(acquisition_time_ms/MONITORING_TIME_FOR_ONE_DOT_MS)
        rf_imon_list = []
        pa_imon_list = []
        pa_vmon_list = []
        index_list = []
        mon_table = Card.read(self, NUCLEO_CMD_READ_MON_ACQ, dots_nb*MONITORING_NB_OUTPUT*2, 0)
        
        for index in range(FRAME_HEADER_SIZE, dots_nb*MONITORING_NB_OUTPUT*2+6, +6):
            index_list.append(index)
            rf_imon_list.append(self.rf_imon_monitoring_normalized_to_ampere((mon_table['buf'][index] | (mon_table['buf'][index + 1] << 8))))
            pa_imon_list.append(self.pa_imon_monitoring_normalized_to_ampere((mon_table['buf'][index + 2] | (mon_table['buf'][index + 3] << 8))))
            pa_vmon_list.append(self.pa_vmon_monitoring_normalized_to_voltage((mon_table['buf'][index + 4] | (mon_table['buf'][index + 5] << 8))))
        
        rf_imon_average = reduce(lambda x, y: x + y, rf_imon_list) / len(rf_imon_list)
        pa_imon_average = reduce(lambda x, y: x + y, pa_imon_list) / len(pa_imon_list)
        pa_vmon_average = reduce(lambda x, y: x + y, pa_vmon_list) / len(pa_vmon_list)

        rf_imon = rf_imon_average
        pa_imon = pa_imon_average
        pa_vmon = pa_vmon_average
        print "Nucleo> RF Imon = " + str(rf_imon) + "A"
        print "Nucleo> PA Imon = " + str(pa_imon) + "A"
        print "Nucleo> PA Vmon = " + str(pa_vmon) + "V"
        
        return rf_imon, pa_imon, pa_vmon
    
    def plot_monitoring_acq(self, acquisition_time_ms):
        pg.mkQApp()
        proc = mp.QtProcess()
        rpg = proc._import('pyqtgraph')
        # prepare plot
        rf_imon_plotwin = rpg.plot()
        rf_imon_plotwin.setTitle('RF Module: i vs t')
        rf_imon_plotwin.setLabels(left = ('Current', 'A'))
        rf_imon_plotwin.setLabels(bottom = ('Time', 's'))
        rf_imon_plotwin.showGrid(True, True, 0.5)
        prf_imon_plotwin = []
        prf_imon_plotwin.append(rf_imon_plotwin.plot())
        prf_imon_plotwin[-1].setPen({'color': "F00"})
        prf_imon_plotwin.append(rf_imon_plotwin.plot())
        prf_imon_plotwin[-1].setPen({'color': "FFF"})
        
        pa_imon_plotwin = rpg.plot()
        pa_imon_plotwin.setTitle('PA Module: i vs t')
        pa_imon_plotwin.setLabels(left = ('Current', 'A'))
        pa_imon_plotwin.setLabels(bottom = ('Time', 'ms'))
        pa_imon_plotwin.showGrid(True, True, 0.5)
        ppa_imon_plotwin = []
        ppa_imon_plotwin.append(pa_imon_plotwin.plot())
        ppa_imon_plotwin[-1].setPen({'color': "F00"})
        ppa_imon_plotwin.append(pa_imon_plotwin.plot())
        ppa_imon_plotwin[-1].setPen({'color': "FFF"})
        
        pa_vmon_plotwin = rpg.plot()
        pa_vmon_plotwin.setTitle('PA Module: v vs t')
        pa_vmon_plotwin.setLabels(left = ('Voltage', 'V'))
        pa_vmon_plotwin.setLabels(bottom = ('Time', 'ms'))
        pa_vmon_plotwin.showGrid(True, True, 0.5)
        ppa_vmon_plotwin = []
        ppa_vmon_plotwin.append(pa_vmon_plotwin.plot())
        ppa_vmon_plotwin[-1].setPen({'color': "F00"})
        ppa_vmon_plotwin.append(pa_vmon_plotwin.plot())
        ppa_vmon_plotwin[-1].setPen({'color': "FFF"})
        
        dots_nb = int(acquisition_time_ms/MONITORING_TIME_FOR_ONE_DOT_MS)
        rf_imon_list = []
        pa_imon_list = []
        pa_vmon_list = []
        index_list = []
        mon_table = Card.read(self, NUCLEO_CMD_READ_MON_ACQ, dots_nb*MONITORING_NB_OUTPUT*2, 0)
        for index in range(FRAME_HEADER_SIZE, dots_nb*MONITORING_NB_OUTPUT*2+6, +6):
            index_list.append(index)
            rf_imon_list.append(self.rf_imon_monitoring_normalized_to_ampere((mon_table['buf'][index] | (mon_table['buf'][index + 1] << 8))))
            pa_imon_list.append(self.pa_imon_monitoring_normalized_to_ampere((mon_table['buf'][index + 2] | (mon_table['buf'][index + 3] << 8))))
            pa_vmon_list.append(self.pa_vmon_monitoring_normalized_to_voltage((mon_table['buf'][index + 4] | (mon_table['buf'][index + 5] << 8))))
        
        prf_imon_plotwin[0].setData(index_list, rf_imon_list, _callSync='off')
        ppa_imon_plotwin[0].setData(index_list, pa_imon_list, _callSync='off')
        ppa_vmon_plotwin[0].setData(index_list, pa_vmon_list, _callSync='off')
        
        rf_imon_average = reduce(lambda x, y: x + y, rf_imon_list) / len(rf_imon_list)
        pa_imon_average = reduce(lambda x, y: x + y, pa_imon_list) / len(pa_imon_list)
        pa_vmon_average = reduce(lambda x, y: x + y, pa_vmon_list) / len(pa_vmon_list)
        print "Nucleo> RF Imon = " + str(rf_imon_average) + "A"
        print "Nucleo> PA Imon = " + str(pa_imon_average) + "A"
        print "Nucleo> PA Vmon = " + str(pa_vmon_average) + "V"

    
    
            
    def rf_imon_monitoring_normalized_to_ampere(self, imon_normalized):
        return (imon_normalized)*MONITORING_NUCLEO_REFERENCE_VOLTAGE/MONITORING_SAMPLE_NORMALIZED_RANGE/MONITORING_RMON_RF*MONITORING_LDO_MON_SCALE_COEF
    
    def pa_imon_monitoring_normalized_to_ampere(self, imon_normalized):
        return (imon_normalized)*MONITORING_NUCLEO_REFERENCE_VOLTAGE/MONITORING_SAMPLE_NORMALIZED_RANGE/MONITORING_RMON_PA*MONITORING_LDO_MON_SCALE_COEF
    
    def pa_vmon_monitoring_normalized_to_voltage(self, vmon_normalized):
        return (vmon_normalized)*MONITORING_NUCLEO_REFERENCE_VOLTAGE/MONITORING_SAMPLE_NORMALIZED_RANGE*(MONITORING_VOLTAGE_DIVIDER_R32 + MONITORING_VOLTAGE_DIVIDER_R33)/MONITORING_VOLTAGE_DIVIDER_R33
    

        
        
        
    