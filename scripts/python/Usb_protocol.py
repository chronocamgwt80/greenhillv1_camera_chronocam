#!/bin/python
# import
import sys
import usb.core
import usb.util
import time 
from Usb_protocol_def import *
# import maths

# define
HEADER_SIZE = 8
FOOTER_SIZE = 2
OVERHEAD_SIZE = HEADER_SIZE + FOOTER_SIZE

class Usb_protocol:
    def __init__(self, idVendor = UsbIdVendor, idProduct = UsbIdProduct):
        self.idVendor = idVendor
        self.idProduct = idProduct
        self.seek_device()
        self.detach_driver_kernel()
        self.claim_device()
    def seek_device(self):
        self.dev = usb.core.find(idVendor = UsbIdVendor, idProduct = UsbIdProduct)
#         self.dev = usb.core.find(self.idVendor, self.idProduct)
        if self.dev is None:
            raise ValueError('USB- Device not found')
        else:
            print 'USB Protocol> USB- Device found'
        
    def detach_driver_kernel(self):
        if self.dev.is_kernel_driver_active(0):
            try:
                self.dev.detach_kernel_driver(0)
                print "USB Protocol> USB- kernel driver detached"
            except usb.core.USBError as e:
                sys.exit("USB- Could not detach kernel driver: %s" % str(e)) 
        else:
            print "USB Protocol> USB- kernel already driver detached"
                    
    def claim_device(self):
        try:
            usb.util.claim_interface(self.dev, 0)
            print "USB Protocol> USB- device claimed"
        except usb.core.USBError as e:
            sys.exit("USB- Could not claim the device: %s" % str(e))
            
    def write(self, add, value):
        write_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        nbbytes = chr(1) + chr(0) + chr(0) + chr(0)
        data = chr((value)&0xFF)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = write_cmd + nbbytes + data + checksum + end_cmd;
        #comment likely due to usb library difference between GWT and Asygn
        self.dev.write(USB_BULK_WRITE_ENDPOINT,cmd)#,interface=0) #,timeout=100)
        time.sleep(0.1)   
#         print hex(ord(cmd[0]))
     
    # send a packet with its width data = regnbbytes
    def write_buf(self, add, value, regnbbytes):
        write_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        nbbytes = chr(regnbbytes&0xFF) + chr((regnbbytes>>8)&0xFF) + chr((regnbbytes>>16)&0xFF) + chr((regnbbytes>>24)&0xFF)
        data = value
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = write_cmd + nbbytes + data + checksum + end_cmd;
        cmd_tmp = cmd[0]
        for i in range (1, len(cmd), +1):
            if ((i%200) == 0):
                self.dev.write(USB_BULK_WRITE_ENDPOINT, cmd_tmp)#,interface=0) #,timeout=100)
                cmd_tmp = cmd[i]
            else :
                cmd_tmp = cmd_tmp + cmd[i]
            
        self.dev.write(USB_BULK_WRITE_ENDPOINT, cmd_tmp)#,interface=0) #,timeout=100)
                
    
        
        #comment likely due to usb library difference between GWT and Asygn
#         self.dev.write(USB_BULK_WRITE_ENDPOINT,cmd)#,interface=0) #,timeout=100)
        time.sleep(0.1)
        
    def write_raw(self, data):
        self.dev.write(USB_BULK_WRITE_ENDPOINT,data)#,interface=0) #,timeout=100)
        time.sleep(0.1)
                
    def read(self, add, regnbbytes, toconsole): 
        read_cmd = chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr(0x80 + ((add >> 24)&0xFF))
        nbbytes = chr((regnbbytes)&0xFF) + chr((regnbbytes>>8)&0xFF) + chr((regnbbytes>>16)&0xFF) + chr((regnbbytes>>24)&0xFF)
#        data = chr(0) + chr(0) + chr(0) + chr(0)
        checksum = chr(0)
        end_cmd = chr(0x55)
        cmd = read_cmd + nbbytes + checksum + end_cmd;
        self.dev.write(USB_BULK_WRITE_ENDPOINT,cmd)#,interface=0,timeout=100)
        time.sleep(0.2) #do not decrease it to guarantee timing between usb (pc-zynq) and spi (zynq-nucleo) transfert
        d=self.dev.read(USB_BULK_READ_ENDPOINT,OVERHEAD_SIZE+regnbbytes, timeout=1000000)
        time.sleep(0.1)
        val = 0
        add = int(d[0]) + int(d[1]<<8) + int(d[2]<<16) + int(d[3]<<24)
        nbbytes = int(d[4]) + int(d[5]<<8) + int(d[6]<<16) + int(d[7]<<24)

        valhex = ''
        for k in range(0,regnbbytes):
            valhex = valhex + hex(d[k+HEADER_SIZE])[2:] + ' '
            val = val + (d[k+HEADER_SIZE] << k) 
        if (toconsole):
            print "USB Protocol> register (" + hex(add) + ") = 0x" + valhex
        return {'add':add,'val':val, 'buf':d}
    
    def read_data(self, nbbytes):
        data = self.dev.read(USB_BULK_READ_ENDPOINT, nbbytes)# interface=0,timeout=1000000)
        bytesList = []
        for index in range (len(data)):
            bytesList.append(int(data[index]))
        return bytesList
        
        
    def kill(self):
        usb.util.release_interface(self.dev, 0)
        del self
