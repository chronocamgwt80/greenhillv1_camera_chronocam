#!/usr/bin/python
from greenhill import *
from At86rf215 import *

def exit_script():
    gw.kill()
    
try:
    # config
    agc_status = FREEZE_AGC
    
    gw = Greenhill(USB_PROTOCOL)
    gw.rf1.set_receiver_as_iq_radio_mode()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()