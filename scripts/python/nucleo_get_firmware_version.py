#!/usr/bin/python
from greenhill import *
import time

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.nucleo.get_firmware_version()
    time.sleep(0.1)
      
    exit()
    
except KeyboardInterrupt:
    exit_script()