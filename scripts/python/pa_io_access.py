#!/usr/bin/python
from greenhill import *
from pa_def import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    
    mode_list = []
    mode_list.append(PA_SLEEP)
    mode_list.append(PA_RECEIVE_BYPASS)
    mode_list.append(PA_RECEIVE_LNA)
    mode_list.append(PA_TRANSMIT)
    
    antenna_list = []
    antenna_list.append(PA_ANT1_EN)
    antenna_list.append(PA_ANT2_EN)
    
    pa_list = []
    pa_list.append(gw.pa0)
    pa_list.append(gw.pa1)
    
    # set and read all available settings for each pa
    for pa in pa_list:
        for antenna in antenna_list:
            for mode in mode_list:
                pa.set_io(mode, antenna)
                pa.read_io()

    exit_script()
    
except KeyboardInterrupt:
    exit_script()