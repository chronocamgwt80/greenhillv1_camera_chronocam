#!/usr/bin/python
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)
#     gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()