from Usb_protocol import Usb_protocol
from abc import abstractmethod
    
USB_PROTOCOL = "usb"
ETHERNET_PROTOCOL = "ethernet"


class Protocol():
    def __init__(self):
        pass
    
    @abstractmethod
    def write(self, command):
        """
        to send write command
        """
        
    @abstractmethod
    def read(self, command):
        """
        to send read command
        """
        
    @abstractmethod
    def kill(self):
        """
        to close properly the protocol
        """
        
class ProtocolFactory():
    # Create based on class name:
    def factory(protocolType):
        if protocolType == USB_PROTOCOL: return Usb_protocol()
#         if protocolType == ETHERNET_PROTOCOL: return EthernetProtocol()
        assert 0, "Bad protocol creation: " + type
    factory = staticmethod(factory)   