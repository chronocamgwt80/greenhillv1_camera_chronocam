#!/usr/bin/python
from greenhill import *
from nucleo_def import *
import time

def exit_script():
    gw.kill()
    
try:
    print "Warning> Before launch this script, ensure that all modules are connected together and communication between them works..."
    # config
    monitoring_acquistion_time_ms = MONITORING_ACQ_TIME_MAX_MS
    
    gw = Greenhill(USB_PROTOCOL)
    
    print "Debug> reset RF and PA modules"
    #gw.rf0.reset()
    #gw.pa0.set_io(PA_SLEEP, PA_ANT1_EN)
    gw.rf1.reset()
    gw.pa1.set_io(PA_SLEEP, PA_ANT1_EN)
    
    print "Debug> first monitoring acquisition"
    gw.nucleo.start_monitoring_acq(monitoring_acquistion_time_ms)
    acq_flag = MONITORING_ACQ_ENABLE
    time.sleep(monitoring_acquistion_time_ms/1000) # monitoring acquisition estimation time
    while (acq_flag == MONITORING_ACQ_ENABLE):
        acq_flag, dots_index = gw.nucleo.read_monitoring_acq_flag()
        print "Debug> dots index = " + str(dots_index) + "/" + str(monitoring_acquistion_time_ms)
    gw.nucleo.read_monitoring_acq(monitoring_acquistion_time_ms)
    
    print "Debug>  set rf modules as transmitter"
    #gw.rf0.reset()
    #gw.rf0.irq_enable()
    #gw.rf0.IQ_radio_enable()
    #gw.rf0.transmitter_frontend_config()
    #gw.rf0.channel_config()
    #gw.rf0.transmit_power_config(0, 0)
    #gw.rf0.configure_IQ_data_interface()
    
    #gw.rf0.go_state(RF_STATE_TXPREP)
    #while (gw.rf0.read_irq_reg() != IRQS_TRXRDY_MASK):
    #    pass
    
    #gw.rf0.go_state(RF_STATE_TX)
    #current_state = gw.rf0.read_current_state()
    #print "Transmitter current state : " + hex(current_state)
    
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(0, 0)
    gw.rf1.configure_IQ_data_interface()
    
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "Transmitter current state : " + hex(current_state)
    
    print "Debug>  set pa modules as transmitter" 
    #gw.pa0.set_io(PA_TRANSMIT, PA_ANT1_EN)
    gw.pa1.set_io(PA_TRANSMIT, PA_ANT1_EN)
    
    print "Debug>  second monitoring acquisition"
    gw.nucleo.start_monitoring_acq(monitoring_acquistion_time_ms)
    acq_flag = MONITORING_ACQ_ENABLE
    time.sleep(monitoring_acquistion_time_ms/1000) # monitoring acquisition estimation time
    while (acq_flag == MONITORING_ACQ_ENABLE):
        acq_flag, dots_index = gw.nucleo.read_monitoring_acq_flag()
        print "Debug> dots index = " + str(dots_index) + "/" + str(monitoring_acquistion_time_ms)
    gw.nucleo.read_monitoring_acq(monitoring_acquistion_time_ms)
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
