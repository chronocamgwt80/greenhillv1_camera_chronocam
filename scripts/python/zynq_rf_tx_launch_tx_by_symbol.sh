#! /bin/bash

#This script launch the c code ./gwt_size_symbol to determine the
#number of bits per symbol and then run the python script to configure
#the zynq

path=$(pwd)
cd $path
echo "parameters : $@"
./gwt_size_symbol "$@"
ret=$?

cmd="zynq_rf_tx_launch_tx_by_symbol.py $@ -s $ret"
#echo "cmd=$cmd"
echo "zynq_gwt_parse_options.py $@ -s $ret"
python $(echo $cmd)
#python zynq_gwt_parse_options.py "$@ -s $ret"
