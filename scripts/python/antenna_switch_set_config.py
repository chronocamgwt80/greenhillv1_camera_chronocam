#!/usr/bin/python
from greenhill import *
from antenna_switch_def import *
import time

def exit_script():
    gw.kill()
    
try:
    # config
    sw0 = ANTENNA_SWITCH_RX_MODE
    sw1 = ANTENNA_SWITCH_RX_MODE
    
    gw = Greenhill(USB_PROTOCOL)

    gw.antenna_switch0.set_antenna_switch_config(sw0)
    gw.antenna_switch1.set_antenna_switch_config(sw1)
    time.sleep(0.1)
    sw0 = gw.antenna_switch0.read_antenna_switch_config()
    sw1 = gw.antenna_switch1.read_antenna_switch_config()
      
    exit()
    
except KeyboardInterrupt:
    exit_script()