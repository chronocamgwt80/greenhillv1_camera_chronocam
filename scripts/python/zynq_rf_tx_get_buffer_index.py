#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_get_buffer_index()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()