#!/usr/bin/python
from greenhill import *
from At86rf215 import *
from pa_def import *
from antenna_switch_def import *
from zynq_def import * 
import time



def exit_script():
    gw.kill()
    
try:
    # Transmission config
    word_nb = 1024#RF_TX_BUFFER_WR_MAX_DEPTH
    input_csv = "pattern.csv"                       # existing file, format: one column of 32bits words
    input_csv_read = "pattern_debug.csv"            # will be created, format: one column of 32bits words
    output_csv_modulated = "pattern_modulated.csv"  # will be created, one column of I, one column of Q
    debug = 0
    nb_burst = 1    
    # RF modules config
    rf1_pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    rf1_txpwr = 0xF                         # TXPWR controls the transmit output power setting
    rf1_loopback = EXTLB_LOOPBACK_DISABLE    # If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.
    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    # Zynq config
    zynq_modulation_mode = ZYNQ_RF_TX_GWT_MOD_DISABLE     # enable/disable GWT modulator
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE

    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set Antenna Switch as TX
    # 3. Set PA as TX
    # 4. Set RF modules as TX
    # 5. Start Transmission
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)
    
    # 1. reset
    gw.rf1.reset()
    gw.pa1.reset()
    gw.zynq.reset()

    
    # 2. Set Antenna Switch as TX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 3. Set PA as TX
    gw.pa1.set_io(pa1_mode, pa1_antenna)

    # 4. Set RF modules as TX
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(rf1_pacur, rf1_txpwr)
    gw.rf1.configure_IQ_data_interface()
    gw.rf1.loopback_enable(rf1_loopback)
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "Transmitter current state : " + hex(current_state)
                
    # 5. Start Transmission
    gw.zynq.start_rf_tx_self_test()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
