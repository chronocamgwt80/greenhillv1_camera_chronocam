#!/usr/bin/python

# import
#from sys import *
import sys
#from At86rf215 import *
#from zynq import *
from greenhill import *
from zynq_def import *
from ofdm import *
import math, getopt

#DEBUG = True
DEBUG = False
#debugFileRef = open("debug.bin",'w')
debugFileRef = open("600kB.bin",'w')

def cfor(first,test,update):
    while test(first):
        yield first
        first = update(first)
        
def send_nbr_frame(chunkNbr, nbrByteBuf):
        add_prep = ZYNQ_CMD_ADVISE_DATA_ON_FEW_FRAMES
        cmd_advise = (chr((add_prep)&0xFF) + chr((add_prep>>8)&0xFF) 
                    + chr((add_prep>>16)&0xFF) + chr((add_prep>>24)&0xFF)
                    + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                    + chr((nbrByteBuf&0xFF)>>16) + chr((nbrByteBuf&0xFF)>>24)
                    + chr(chunkNbr&0xFF))
        gw.zynq.write_raw(cmd_advise)

def read_IQ_file_fill_fifo(INPUT_FILE):
    #read a csv file return a buffer buf and its number of byte nbrByteBuf
    ofdm=Ofdm()
    buf=""
    nbrByteBuf = 0
    ofdm.rd_file_ret_buf(INPUT_FILE)
    buf = ofdm.getBuf()
    nbrByteBuf = ofdm.getNbrByteBuf()
    
    # Command to send the IQ data read from a file
    #protocole is:
    #4B addr, 2B nbr of B in the frame, payload data, 1B checksum, 1B stop_byte
    #so protocol load is 8B
    PROTOCOL_TAIL_SIZE = 2
    PROTOCOL_HEAD_SIZE = 8
    PROTOCOL_FRAME_NBR_B = PROTOCOL_HEAD_SIZE + PROTOCOL_TAIL_SIZE
    MAX_USB_PKT_SIZE = 1024 * 16

    MAX_1ST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_HEAD_SIZE
    MAX_LAST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_TAIL_SIZE
    
    add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_RF_TX_FILL_BUFFER
    data = 0
    checksum = 0
    stop_byte = 0x55
    
#     zynq = Zynq()
    
    print "MAX_USB_PKT_SIZE: {}".format(MAX_USB_PKT_SIZE)
    print "nbrByteBuf: {}".format(nbrByteBuf)
    #print "buf: {}".format(buf)
    if nbrByteBuf + PROTOCOL_FRAME_NBR_B < MAX_USB_PKT_SIZE: #MAX_1_CHUNK_SIZE:
        #only 1 chunk for the whole frame to send
        #send_nbr_frame(1)
        cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
            + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
            + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF)
            + buf
            + chr(checksum&0xFF) + chr(stop_byte&0xFF))
        gw.zynq.write_raw(cmd)
        #print hex(ord(cmd))
        #print "", ', '.join(str(hex(ord(c))) for c in cmd[-1040:-1])
    else:
    #send several chunks
        #calculate a float nbrByteBuf then rounding it upper
        chunkNbr = math.ceil((nbrByteBuf + PROTOCOL_FRAME_NBR_B) / float(MAX_USB_PKT_SIZE))
        print "chunkNBR={}".format(chunkNbr)
        #check that the protocol byte cost request 1 more chunk
        #if chunkNbr*MAX_USB_PKT_SIZE < nbrByteBuf:
        #    chunkNbr += 1
        #print "chunkNBR={}".format(chunkNbr)
        
         #send command to advise next command is for a payload split on few chunks
        #send_nbr_frame(nbrByteBuf)
            
        for i in cfor(0,lambda i: i < chunkNbr,lambda i: i + 1):
        #for i in cfor(0: i < chunkNBR: i + 1):
            slice1 = (i - 1) * MAX_USB_PKT_SIZE + MAX_1ST_CHUNK_SIZE   #MAX_1_CHUNK_SIZE + MAX_1ST_CHUNK_SIZE
            #1st chunk
            if i == 0:
                cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
                        + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                        + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF))
                #print "", ', '.join(str(hex(ord(c))) for c in cmd[-20:-1])
                #sys.exit()
                if MAX_1ST_CHUNK_SIZE >= 1:
                       cmd = cmd + buf[0 : MAX_1ST_CHUNK_SIZE] # end slice is n - 1
                       #for g in cfor(0,lambda g: g < MAX_1ST_CHUNK_SIZE,lambda g: g + 1):
                       #    print "buf[{}]={}".format(g, hex(ord(buf[g])))
                if DEBUG:
                    #debugFileRef.write("ASCII value 1st chunk:")
                    print "ASCII value 1st chunk: {} {}".format(hex(ord(cmd[-1])), hex(ord(cmd[-2])))
                    #print "", ', '.join(str(hex(ord(c))) for c in cmd[-10:-1])
                    debugFileRef.write(cmd)
                    #print "ASCII value 1st chunk: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else:
                    print "PROTOCOL_HEAD_SIZE={} MAX_1ST_CHUNK_SIZE={}".format(PROTOCOL_HEAD_SIZE, MAX_1ST_CHUNK_SIZE)
                    print "send chunk 0 to USB"
                    gw.zynq.write_raw(cmd)
                    #sys.exit()
            #last chunk
            elif i == (chunkNbr - 1):
            #TODO seems case +1 chunk to mangage checksum and stopbyte not taken into account here
                if i == 1: #only 2 chunks
                    cmd = (buf[MAX_1ST_CHUNK_SIZE : nbrByteBuf]
                       + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                else:
                    cmd = (buf[slice1 : nbrByteBuf]
                           + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                if DEBUG:
                    print "ASCII value last chunk: chunk N{}".format(i)
                                        #debugFileRef.write("ASCII value last chunk:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send last chunk to USB"
                    gw.zynq.write_raw(cmd)
            #chunk other than 1st or last
            else:
                #print "i={} slice1={} slice2={}".format(i, (i - 1) * MAX_1_CHUNK_SIZE) + MAX_1ST_CHUNK_SIZE,)
                slice2 = slice1 + MAX_USB_PKT_SIZE
                cmd = (buf[slice1 : slice2])
                if DEBUG:
                    print "ASCII value mains chunks: chunk N{}".format(i)
                    #debugFileRef.write("ASCII value mains chunks:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send chunk {} to USB".format(i)
                    gw.zynq.write_raw(cmd)

    debugFileRef.close()

def exit_script():
    gw.kill()
    sys.exit()
    #zynq.kill()
    #transmitter.kill()

def convert32bits(word32b, buffer):
    buffer = buffer + (chr((word32b) & 0xFF) + chr((word32b >> 8) & 0xFF) + chr((word32b >> 16) & 0xFF) + chr((word32b >> 24) & 0xFF))
    return buffer

def modulation_configuration():
    N_MIN = 2
    N_MAX = 2048 # max Number of tones
    NCP_MIN = 0 # max cyclic prefix
    NCP_MAX = (N_MAX/2) # max cyclic prefix
    MINBACKOFF = -40
    MAXBACKOFF = 40
    DEF_BACKOFF_INDX = 4

    backoff_v = 0
    if backoff_v <= MINBACKOFF or backoff_v >= MAXBACKOFF:
      print "backoff out of range"
      exit_script()

    gain_float = 0.32
    gain_rd_2_digit = int((gain_float * 100) + 0.5) / 100.0
    gain_bef_pt = math.floor(gain_rd_2_digit)
    #gain_aft_pt = (gain_rd_2_digit - gain_bef_pt) * 100
    #seems previous calcul could have 0.01 error while not seen on the below one
    gain_aft_pt = int(gain_rd_2_digit *100 - gain_bef_pt *100)
    gain = gain_float

    mode = 2
    dmod = 0
    nfft_v = 64
    ncp_v = 16
    null_tones_v = 8
    nsub_sect_v = 8
    constell_v = 1
    GEN_RANDOM_BITSTREAM = 0

    nbr_bits_symbol = -1
    nbr_symbol = 1

    nbr_bits_tx = nbr_symbol * nbr_bits_symbol
    nbr_bytes_tx = int(math.ceil(nbr_bits_tx / 8))
    
    print "Modulation configuration: backoff=%s gain=%s gain_bef_pt=%s gain_aft_pt=%s mode=%s dmod=%s nfft_v=%s ncp_v=%s null_tones_v=%s nsub_sect_v=%s constell_v=%s GEN_RANDOM_BITSTREAM=%s" %(backoff_v, gain_float, gain_bef_pt, gain_aft_pt, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v, GEN_RANDOM_BITSTREAM)

    checksum = 0
    stop_byte = 0x55
    cmd = chr(checksum&0xFF) + chr(stop_byte&0xFF)
  
    #buf = [backoff_v, gain_bef_pt, gain_aft_pt, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v, GEN_RANDOM_BITSTREAM, nbr_bits_symbol, nbr_symbol nbr_bytes_tx]
    buf_size = 4 +      4 +     +	4 	   +4 + 4 +     4 +     4 +     4 +         4 +         4 +         4 +                 4                   + 4          + 4
    print "buf_size={}".format(buf_size)
    buf = str()
    buf = (convert32bits(backoff_v, buf) + convert32bits(int(gain_bef_pt), buf) + convert32bits(int(gain_aft_pt), buf)  + convert32bits(mode, buf)
                        + convert32bits(dmod, buf)
                        + convert32bits(nfft_v, buf) + convert32bits(ncp_v, buf) + convert32bits(null_tones_v, buf)
                        + convert32bits(nsub_sect_v, buf) + convert32bits(constell_v, buf) + convert32bits(GEN_RANDOM_BITSTREAM, buf)
                        + convert32bits(nbr_bits_symbol, buf) + convert32bits(nbr_symbol, buf) + convert32bits(nbr_bytes_tx, buf))
    #print "buf.length=%s" %(len(buf))
        
    add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_GWT_CONFIGURE
    data = 0
    checksum = 0
    stop_byte = 0x55

    cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        + chr(buf_size&0xFF) + chr((buf_size>>8)&0xFF)
        + chr((buf_size>>16)&0xFF) + chr((buf_size>>24)&0xFF)
        + buf
        + chr(checksum&0xFF) + chr(stop_byte&0xFF))
    gw.zynq.write_raw(cmd)
    
def usage():
  print "USAGE:"
  print "This script configure the radio, can configure the modulator with a default configuration to modulate a csv bitstream. The modulator will do through uart a printf at the end of modulation. You could get it by example to run prior to this script, by example in another shell:\n 'miniterm.py -b 115200 /dev/ttyUSBx'.\nHere x is the number of the device usually 0 or 1. Take care, it could happen that USB buffer is not enough full for the PC to display the message.\n Notice that the buffer feeded the radio is in loop mode so this does a continuous Tx of the modulated data."
  print "options list"
  print "\t -c  : the default modulation configuration is disable. So the modulator should already be configurated"
  print "\t -f  : the csv bitstream to transmit"
  print "\t -p  : the dbm value between [m19, m8] with Power Amplifier, [m19, 11] without Power Amplifier, m19 means -19dBm, 11 means 11dBm"
  print "\t -w  : meaning without Power Amplifier allowing an the full range for the RF215 amplifier, [m19, 11]"
  print "\t -h or -help  : print this usage"
  print "\t example: ./zynq_rf_tx_bitstreamCSV.py -f 100kB.csv -p 11 -w"
  exit(0)

######################
# main
######################
    
try:
    #dictionary for the TX power 
    #AT86RF215 p206: Figure 11-2. Output power vs. RF09_PAX.TXPWR register for several modulations at f channel =900MHz
    #curve of OFDM n 3 and n 4
    pwr = {'m19': 0, 'm18': 1, 'm17': 2, 'm16': 3, 'm15': 4, 'm14': 5, 'm13': 6, 'm12': 7, 'm11': 8, 'm10': 9, 'm9': 10, 'm8': 12, 'm7': 13, 'm6': 14, 'm5': 15, 'm4': 16, 'm3': 17, 'm2': 18, 'm1': 19, '0': 20, '1': 21, '2': 22, '3': 23, '4': 24, '5': 25, '6': 26, '7': 27, '8': 28, '9': 29, '10': 30, '11': 31} 
    pacur = 3
    tx_pwr = 0
    pa_opt = False
    DISABLE_MODULATION_CONFIGURATION = False
    #0 parameters decoding
    myopts, args = getopt.getopt(sys.argv[1:], "cf:p:wh", ["help"])
except getopt.GetoptError as e:
    print "bad option in command line"
    print (str(e));print ""
    usage()
    sys.exit(2)
try:
    if len(myopts) == 0:
        usage()
        exit(2) 
    else:
        for opt, arg in myopts:
            if opt in ('-f'):
                csv_file = arg
            elif opt in ('-w'):
                pa_opt = True
            elif opt == '-p':
                pw = arg
                tx_pwr = pwr[pw]
            elif opt == '-c':
                DISABLE_MODULATION_CONFIGURATION = True
                """
                if pa_opt and tx_pwr > 12:
                  tx_pwr = 12
                  print "the power is limited to -8dB with the Power Amplifier, use option -w to avoir this limit"
                """
            elif opt in ('-h', '--help'):
                usage()
            else:
                print "bad option in command line"
                for a in sys.argv: 
                    print a
                exit(2)
    
    if (not pa_opt) and (tx_pwr > 12):
        tx_pwr = 12
        print "the power is limited to -8dB (m8 with the Power Amplifier, use option -w to avoid this limit"
    
    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE
    
    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set Antenna Switch as TX
    # 3. Set PA as TX
    # 4. Set RF modules as TX
    # 5. Start Transmission
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_get_buffer_index()
    # 1. reset
    gw.rf1.reset()
    gw.pa1.reset()
    gw.zynq.rf_tx_get_buffer_index()

    #configure the modulator with configuration embeded in this file
    if not DISABLE_MODULATION_CONFIGURATION:
      modulation_configuration()
    #enable cmd_from_python flag if not done
    gw.zynq.set_cmd_from_python(1)
    gw.zynq.get_cmd_from_python()
   
    # 2. Set Antenna Switch as TX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 3. Set PA as TX
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    
    # 4. Set RF modules as TX
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(pacur, tx_pwr)
    gw.rf1.loopback_enable(0)#desactive the loopback
    gw.rf1.go_state(RF_STATE_TXPREP)
    while ((gw.rf1.read_irq_reg()&IRQS_TRXRDY_MASK) != IRQS_TRXRDY_MASK):
    #while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    #reset the TX block after the RF clock availability
    #other the part of the buffer with the RF clock will not be reseted
    gw.zynq.rf_tx_reset()
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "RF09 current state : " + hex(current_state)
    #print "value auxs regiter=%d" %(gw.rf1.read_auxs_register())
    #transmitter.kill()
    
    gw.zynq.reset()
    #3 read bitstream and prepare the buffer to modulate
    print "**execute prepare data to modulate="
    gw.zynq.rf_tx_prepare_data_to_modulate(csv_file)
    time.sleep(5)
    #import pdb; pdb.set_trace()
    
    #4 Configure in burst mode the buffer to load it
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)

    #ensure modulation_ready
    gw.zynq.rf_tx_get_modulation_ready()
    if (not gw.zynq.modulation_ready):
        print "modulation is not ready={}, script abort".format(gw.zynq.modulation_ready)
        exit_script()

    #5 modulate the bistream
    #gw.zynq.rf_tx_enable_check_modulation_data()
    #time.sleep(1)
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_ENABLE
    gw.zynq.rf_tx_start_modulation(modulation_mode)
    
    ''' not needed with cmd_from_python flag
    gw.zynq.rf_tx_get_modulation_ready()
    while (not gw.zynq.modulation_ready):
        time.sleep(1)
        gw.zynq.rf_tx_get_modulation_ready()
        print "modulation_ready={}".format(gw.zynq.modulation_ready)
    '''

    #time.sleep(60)
    #modulated data are in the buffer then set the buffer in LOOP MODE
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
    
    #6 start RF TX
    gw.zynq.start_rf_transmission()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
