#!/usr/bin/python

# import
#from sys import *
import sys
#from At86rf215 import *
#from zynq import *
from greenhill import *
from zynq_def import *
from ofdm import *
import math, getopt

#DEBUG = True
DEBUG = False
debugFileRef = open("debug.bin",'w')

def cfor(first,test,update):
    while test(first):
        yield first
        first = update(first)
        
def send_nbr_frame(chunkNbr, nbrByteBuf):
        add_prep = ZYNQ_CMD_ADVISE_DATA_ON_FEW_FRAMES
        cmd_advise = (chr((add_prep)&0xFF) + chr((add_prep>>8)&0xFF) 
                    + chr((add_prep>>16)&0xFF) + chr((add_prep>>24)&0xFF)
                    + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                    + chr((nbrByteBuf&0xFF)>>16) + chr((nbrByteBuf&0xFF)>>24)
                    + chr(chunkNbr&0xFF))
        gw.zynq.write_raw(cmd_advise)

def read_IQ_file_fill_fifo(INPUT_FILE):
    #read a csv file return a buffer buf and its number of byte nbrByteBuf
    ofdm=Ofdm()
    buf=""
    nbrByteBuf = 0
    ofdm.rd_file_ret_buf(INPUT_FILE)
    buf = ofdm.getBuf()
    nbrByteBuf = ofdm.getNbrByteBuf()
    
    # Command to send the IQ data read from a file
    #protocole is:
    #4B addr, 2B nbr of B in the frame, payload data, 1B checksum, 1B stop_byte
    #so protocol load is 8B
    PROTOCOL_TAIL_SIZE = 2
    PROTOCOL_HEAD_SIZE = 8
    PROTOCOL_FRAME_NBR_B = PROTOCOL_HEAD_SIZE + PROTOCOL_TAIL_SIZE
    MAX_USB_PKT_SIZE = 1024 * 16

    MAX_1ST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_HEAD_SIZE
    MAX_LAST_CHUNK_SIZE = MAX_USB_PKT_SIZE - PROTOCOL_TAIL_SIZE
    
    add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_RF_TX_FILL_BUFFER
    data = 0
    checksum = 0
    stop_byte = 0x55
    
#     zynq = Zynq()
    
    print "MAX_USB_PKT_SIZE: {}".format(MAX_USB_PKT_SIZE)
    print "nbrByteBuf: {}".format(nbrByteBuf)
    #print "buf: {}".format(buf)
    if nbrByteBuf + PROTOCOL_FRAME_NBR_B < MAX_USB_PKT_SIZE: #MAX_1_CHUNK_SIZE:
        #only 1 chunk for the whole frame to send
        #send_nbr_frame(1)
        cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
            + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
            + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF)
            + buf
            + chr(checksum&0xFF) + chr(stop_byte&0xFF))
        gw.zynq.write_raw(cmd)
        #print "data send in 1 USB pkt"
        #print hex(ord(cmd))
        #print "", ', '.join(str(hex(ord(c))) for c in cmd[-5130:-1])
        #print "", ', '.join(str(hex(ord(c))) for c in cmd[0:nbrByteBuf+10])
    else:
    #send several chunks
        #calculate a float nbrByteBuf then rounding it upper
        chunkNbr = math.ceil((nbrByteBuf + PROTOCOL_FRAME_NBR_B) / float(MAX_USB_PKT_SIZE))
        print "chunkNBR={}".format(chunkNbr)
        #check that the protocol byte cost request 1 more chunk
        #if chunkNbr*MAX_USB_PKT_SIZE < nbrByteBuf:
        #    chunkNbr += 1
        #print "chunkNBR={}".format(chunkNbr)
        
         #send command to advise next command is for a payload split on few chunks
        #send_nbr_frame(nbrByteBuf)
            
        for i in cfor(0,lambda i: i < chunkNbr,lambda i: i + 1):
        #for i in cfor(0: i < chunkNBR: i + 1):
            slice1 = (i - 1) * MAX_USB_PKT_SIZE + MAX_1ST_CHUNK_SIZE   #MAX_1_CHUNK_SIZE + MAX_1ST_CHUNK_SIZE
            #1st chunk
            if i == 0:
                cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
                        + chr(nbrByteBuf&0xFF) + chr((nbrByteBuf>>8)&0xFF)
                        + chr((nbrByteBuf>>16)&0xFF) + chr((nbrByteBuf>>24)&0xFF))
                #print "", ', '.join(str(hex(ord(c))) for c in cmd[-20:-1])
                #sys.exit()
                if MAX_1ST_CHUNK_SIZE >= 1:
                       cmd = cmd + buf[0 : MAX_1ST_CHUNK_SIZE] # end slice is n - 1
                       #for g in cfor(0,lambda g: g < MAX_1ST_CHUNK_SIZE,lambda g: g + 1):
                       #    print "buf[{}]={}".format(g, hex(ord(buf[g])))
                if DEBUG:
                    #debugFileRef.write("ASCII value 1st chunk:")
                    print "ASCII value 1st chunk: {} {}".format(hex(ord(cmd[-1])), hex(ord(cmd[-2])))
                    #print "", ', '.join(str(hex(ord(c))) for c in cmd[-10:-1])
                    debugFileRef.write(cmd)
                    #print "ASCII value 1st chunk: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else:
                    print "PROTOCOL_HEAD_SIZE={} MAX_1ST_CHUNK_SIZE={}".format(PROTOCOL_HEAD_SIZE, MAX_1ST_CHUNK_SIZE)
                    print "send chunk 0 to USB"
                    gw.zynq.write_raw(cmd)
                    #sys.exit()
            #last chunk
            elif i == (chunkNbr - 1):
            #TODO seems case +1 chunk to mangage checksum and stopbyte not taken into account here
                if i == 1: #only 2 chunks
                    cmd = (buf[MAX_1ST_CHUNK_SIZE : nbrByteBuf]
                       + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                else:
                    cmd = (buf[slice1 : nbrByteBuf]
                           + chr(checksum&0xFF) + chr(stop_byte&0xFF))
                if DEBUG:
                    print "ASCII value last chunk: chunk N{}".format(i)
                                        #debugFileRef.write("ASCII value last chunk:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send last chunk to USB"
                    gw.zynq.write_raw(cmd)
            #chunk other than 1st or last
            else:
                #print "i={} slice1={} slice2={}".format(i, (i - 1) * MAX_1_CHUNK_SIZE) + MAX_1ST_CHUNK_SIZE,)
                slice2 = slice1 + MAX_USB_PKT_SIZE
                cmd = (buf[slice1 : slice2])
                if DEBUG:
                    print "ASCII value mains chunks: chunk N{}".format(i)
                    #debugFileRef.write("ASCII value mains chunks:")
                    debugFileRef.write(cmd)
                    #print "ASCII value main chunks: ", ', '.join(str(hex(ord(c))) for c in cmd)
                    #print ord(cmd)
                else :
                    print "send chunk {} to USB".format(i)
                    gw.zynq.write_raw(cmd)

    debugFileRef.close()

def exit_script():
    gw.kill()
    #zynq.kill()
    #transmitter.kill()
    
def usage():
  print "USAGE:"
  print "This script transmit a modulation csv file through the RF215.\n The format of the csv file is 'I, Q'. Without respect of the comma and the following space, the script won't work correctly."
  print "options list"
  print "\t -f  : the IQ csv to transmit"
  print "\t -p  : the dbm value between [m19, m8] with Power Amplifier, [m19, 11] without Power Amplifier, m19 means -19dBm, 11 means 11dBm"
  print "\t -w  : meaning without Power Amplifier allowing an the full range for the RF215 amplifier, [m19, 11]"
  print "\t -h or -help  : print this usage"
  print "\t example: ./rf_tx_caract.py -f FIR_out_green_8_1sym_x10.csv -p m19"
  exit(0)
    
try:
    #dictionary for the TX power 
    #AT86RF215 p206: Figure 11-2. Output power vs. RF09_PAX.TXPWR register for several modulations at f channel =900MHz
    #curve of OFDM n 3 and n 4
    pwr = {'m19': 0, 'm18': 1, 'm17': 2, 'm16': 3, 'm15': 4, 'm14': 5, 'm13': 6, 'm12': 7, 'm11': 8, 'm10': 9, 'm9': 10, 'm8': 12, 'm7': 13, 'm6': 14, 'm5': 15, 'm4': 16, 'm3': 17, 'm2': 18, 'm1': 19, '0': 20, '1': 21, '2': 22, '3': 23, '4': 24, '5': 25, '6': 26, '7': 27, '8': 28, '9': 29, '10': 30, '11': 31} 
    pacur = 0 #0x0 Power amplifier current reduction by about 22mA (3dB reduction of max. small signal gain)
    tx_pwr = 0
    pa_opt = False
    #0 parameters decoding
    myopts, args = getopt.getopt(sys.argv[1:], "f:p:wh", ["help"])
except getopt.GetoptError as e:
    print "bad option in command line"
    print (str(e));print ""
    usage()
    sys.exit(2)
try:
    if len(myopts) == 0:
        usage()
        exit(2) 
    else:
        for opt, arg in myopts:
            if opt in ('-f'):
                csv_file = arg
            elif opt in ('-w'):
                pa_opt = True
            elif opt == '-p':
                pw = arg
                tx_pwr = pwr[pw]
                """
                if pa_opt and tx_pwr > 12:
                  tx_pwr = 12
                  print "the power is limited to -8dB with the Power Amplifier, use option -w to avoir this limit"
                """
            elif opt in ('-h', '--help'):
                usage()
            else:
                print "bad option in command line"
                for a in sys.argv: 
                    print a
                exit(2)
    
    if (not pa_opt) and (tx_pwr > 12):
        tx_pwr = 12
        print "the power is limited to -8dB (m8 with the Power Amplifier, use option -w to avoid this limit"
    
    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE
    
    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set Antenna Switch as TX
    # 3. Set PA as TX
    # 4. Set RF modules as TX
    # 5. Start Transmission
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_get_buffer_index()
    # 1. reset
    gw.rf1.reset()
    gw.pa1.reset()
    gw.zynq.reset()
    gw.zynq.rf_tx_get_buffer_index()
   
    # 2. Set Antenna Switch as TX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 3. Set PA as TX
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    
    # 4. Set RF modules as TX
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(pacur, tx_pwr)
    gw.rf1.loopback_enable(0)#desactive the loopback
    gw.rf1.go_state(RF_STATE_TXPREP)
    while ((gw.rf1.read_irq_reg()&IRQS_TRXRDY_MASK) != IRQS_TRXRDY_MASK):
    #while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    #reset the TX block after the RF clock availability
    #other the part of the buffer with the RF clock will not be reseted
    gw.zynq.rf_tx_reset()
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "RF09 current state : " + hex(current_state)
    #print "value auxs regiter=%d" %(gw.rf1.read_auxs_register())
    #transmitter.kill()

    
    gw.zynq.reset()

    #4 Configure in loop mode the buffer
    #zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)

    #3 read IQ file and fill the fifo with it
    #zynq = Zynq()
    print"read_IQ_file_fill_fifo(csv_file)"
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)
    read_IQ_file_fill_fifo(csv_file)
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
    #time.sleep(10)
    
    #5 start RF TX
    gw.zynq.start_rf_transmission()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()
