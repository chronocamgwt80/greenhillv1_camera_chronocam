#!/usr/bin/python
from protocol import *
from zynq import Zynq
from nucleo import Nucleo
from At86rf215 import At86rf215
from At86rf215_def import *
from antenna_switch import Antenna_switch
from antenna_switch_def import *
from pa import Pa
from pa_def import *
from nucleo_def import *
import time
# from antenna_switch import Antenna_switch

class Greenhill():
    
    def __init__(self, protocolType = USB_PROTOCOL):
        # claim protocol of the communication with the greenhill board
        self.protocolType = protocolType
        self.protocol = ProtocolFactory.factory(self.protocolType)
        # claim all objects to communicate with all modules
        self.zynq = Zynq(self.protocol)
        self.nucleo = Nucleo(self.protocol)
        self.rf0 = At86rf215(RF0_CMD_BASEADDR, self.protocol)
        self.rf1 = At86rf215(RF1_CMD_BASEADDR, self.protocol)
        self.pa0 = Pa(PA_0, self.protocol)
        self.pa1 = Pa(PA_1, self.protocol)
        self.antenna_switch0 = Antenna_switch(ANT_SW_0, self.protocol)
        self.antenna_switch1 = Antenna_switch(ANT_SW_1, self.protocol)
        
    def kill(self):
        self.protocol.kill()
    
    def check_module_connection(self):
        print "Debug> Zynq module connection checking..."
        self.zynq.get_firmware_version()
        print "Debug> Zynq module is connected and available"
        
        print "Debug> Nucleo module connection checking..."
        self.nucleo.get_firmware_version()
        print "Debug> Nucleo is connected and available"
        
        #print "Debug> RF0 connection checking..."
        #self.rf0.reset()
        #self.rf0.irq_enable()
        #self.rf0.go_state(RF_STATE_TXPREP)
        #while (self.rf0.read_irq_reg() != IRQS_TRXRDY_MASK):
        #    pass
        #self.rf0.go_state(RF_STATE_TX)
        #current_state = self.rf0.read_current_state()
        #if current_state == RF_STATE_TX:
        #    print "Debug> RF0 is connected and available"
        #    self.rf0.reset()
        #else:
        #    print "Debug> RF0 is not connected"
        
        print "Debug> RF1 connection checking..."
        self.rf1.reset()
        self.rf1.irq_enable()
        self.rf1.go_state(RF_STATE_TXPREP)
        while (self.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
            pass
        self.rf1.go_state(RF_STATE_TX)
        current_state = self.rf1.read_current_state()
        if current_state == RF_STATE_TX:
            print "Debug> RF1 is connected and available"
            self.rf1.reset()
        else:
            print "Debug> RF1 is not connected"
            
        #print "Debug> PA0 connection checking..." 
        #self.pa0.set_io(PA_TRANSMIT, PA_ANT1_EN)
        #self.nucleo.start_monitoring_acq(250)
        #time.sleep(250/1000) # monitoring acquisition estimation time
        #rf_imon, pa_imon, pa_vmon = self.nucleo.read_monitoring_acq(250)
        #if (pa_imon > 0.050):
        #    print "Debug> PA0 is connected and available"
        #    self.pa0.set_io(PA_SLEEP, PA_ANT1_EN)
        #else:
        #    print "Debug> PA0 is not connected"
        
        print "Debug> PA1 connection checking..."
        self.pa1.set_io(PA_TRANSMIT, PA_ANT1_EN)
        self.nucleo.start_monitoring_acq(250)
        time.sleep(250.0/1000.0) # monitoring acquisition estimation time
        rf_imon, pa_imon, pa_vmon = self.nucleo.read_monitoring_acq(250)
        if (pa_imon > 0.070):
            print "Debug> PA1 is connected and available"
            self.pa1.set_io(PA_SLEEP, PA_ANT1_EN)
        else:
            print "Debug> PA1 is not connected"
            
        

        
        

        
  
# try:
#     gw = Greenhill(USB_PROTOCOL)
#     gw.zynq.get_firmware_version()
# 
# except KeyboardInterrupt:
#     gw.kill()
