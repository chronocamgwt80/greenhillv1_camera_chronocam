#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    #read from a csv file a number of byte and send it to the nucleo
    #TODO this is only for the beginning of the file until this number of byte
    #    to be extended for the whole file by few packet sent.
    gw.nucleo.ofdm_buffer()
      
    exit()
    
except KeyboardInterrupt:
    exit_script()