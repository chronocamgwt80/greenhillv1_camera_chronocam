import os

class Ofdm():
    
    def __init__(self):
        self.buf = ""
        self.nbrByteBuf = 0
        
    def getBuf(self):
        return self.buf
    
    def getNbrByteBuf(self):
        return self.nbrByteBuf
    
    def ofdm_read_file_return_buffer(self, buf, nbrByte):
        INPUT_FILE  = "outmod_green.csv"   ##"outmod_conv_100_100.csv"
        OUTPUT_FILE = "outmod_green.bin"

        inputFileRef  = open(INPUT_FILE,'r')
        outputFileRef = open(OUTPUT_FILE,'w')

        #zero_samples_nb = 0#1000
        rows = list(inputFileRef)
        
        """if isinstance(buf, basestring):
            print "buf is a string"
        else:
            if isinstance(buf, list):
                print "buf is a list"
            else:
                print "buf is not a string neither a list"
        """

        i = 0
        #print rows#ex of 1 element of rows : '282, -87\n'
        for el in rows:
            try:
                line = el.replace(',','').split()
                #print line
                real = int(line[0])
                img = int(line[1])
                #print "real=%d img=%d" %(real, img)
                buf += (chr((real)&0xFF) + chr((real>>8)&0xFF) + chr((img)&0xFF) + chr((img>>8)&0xFF))
                outputFileRef.write(chr((real)&0xFF) + chr((real>>8)&0xFF) + chr((img)&0xFF) + chr((img>>8)&0xFF))
                i = i + 1
                if i == nbrByte:
                    break
            except ValueError:
                print "el error"
        inputFileRef.close()
        outputFileRef.close()
        
        #read a csv file return a buffer buf and its nunber of byte nbrByteBuf
    #also create a file with same basename and .bin extension (not needed more for debug)
    #TODO confirm the hypothesis of: INPUT_FILE size modulo 4=0 
    def rd_file_ret_buf(self, INPUT_FILE):
        inBaseName = os.path.splitext(INPUT_FILE)
        OUTPUT_FILE = inBaseName[0] + ".bin"
    
        inputFileRef  = open(INPUT_FILE,'r')
        outputFileRef = open(OUTPUT_FILE,'w')
    
        statFile = os.stat(INPUT_FILE)
        nbrByte = statFile.st_size
        print "{} size in byte: {}".format(INPUT_FILE, statFile.st_size)
    
        #zero_samples_nb = 0#1000
        rows = list(inputFileRef)
            
        i = 0
        #print rows#ex of 1 element of rows : '282, -87\n'
        for el in rows:
            try:
                line = el.replace(',','').split()
                #print line
                real = int(line[0])
                img = int(line[1])
                #print "real=%d img=%d" %(real, img)
                self.buf += (chr((real)&0xFF) + chr((real>>8)&0xFF) + chr((img)&0xFF) + chr((img>>8)&0xFF))
                outputFileRef.write(chr((real)&0xFF) + chr((real>>8)&0xFF) + chr((img)&0xFF) + chr((img>>8)&0xFF))
                i = i + 1 # i is the number of 32bits words written to buf
                """if i == nbrByte / 4:
                    break
                """
            except ValueError:
                print "el error"
            
        self.nbrByteBuf = i * 4
        inputFileRef.close()
        outputFileRef.close()
        statOutF = os.stat(OUTPUT_FILE)
        print "{} size in byte: {}".format(OUTPUT_FILE, statOutF.st_size)
        
    #read a csv file return a buffer buf and its nunber of byte nbrByteBuf
    #also create a file with same basename and .bin extension (not needed more for debug)
    #TODO confirm the hypothesis of: INPUT_FILE size modulo 4=0 
    def rd_bitstream_file_ret_buf(self, INPUT_FILE):
        inBaseName = os.path.splitext(INPUT_FILE)
        OUTPUT_FILE = inBaseName[0] + ".bin"
    
        inputFileRef  = open(INPUT_FILE,'r')
        outputFileRef = open(OUTPUT_FILE,'w')
    
        statFile = os.stat(INPUT_FILE)
        nbrByte = statFile.st_size
        print "{} size in byte: {}".format(INPUT_FILE, statFile.st_size)
    
        #zero_samples_nb = 0#1000
        rows = list(inputFileRef)
            
        i = 0
        #print rows#ex of 1 element of rows : '282, -87\n'
        for el in rows:
            try:
                #line = el.replace(',','').split()
                #print line
                #real = int(line[0])
                real = int(el)
                #img = int(line[1])
                #print "real=%d img=%d" %(real, img)
                self.buf += (chr((real)&0xFF) + chr((real>>8)&0xFF))# + chr((img)&0xFF) + chr((img>>8)&0xFF))
                outputFileRef.write(chr((real)&0xFF) + chr((real>>8)&0xFF))# + chr((img)&0xFF) + chr((img>>8)&0xFF))
                i = i + 1 # i is the number of 32bits words written to buf
                """if i == nbrByte / 4:
                    break
                """
            except ValueError:
                print "el error"
            
        self.nbrByteBuf = i * 4
        inputFileRef.close()
        outputFileRef.close()
        statOutF = os.stat(OUTPUT_FILE)
        print "{} size in byte: {}".format(OUTPUT_FILE, statOutF.st_size)