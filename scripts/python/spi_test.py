#!/usr/bin/python

import time
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    # RF RX config
    module_selected = ATMEL_EMULATOR
    acquisition_time_ns = RF_RX_ACQ_TIME_NS
    # RF module config
    pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    txpwr = 0x0                         # TXPWR controls the transmit output power setting
    
    # create API top object
    gw = Greenhill(USB_PROTOCOL)
 
#     current_state = gw.rf1.read_current_state()
#     print "rf19 current state : " + hex(current_state)

#     gw.rf1.go_state(RF_STATE_TXPREP)
    
#     while 1:
        
    gw.rf1.reset()
    gw.rf1.go_state(RF_STATE_TXPREP)
    gw.rf1.go_state(RF_STATE_TX)
    gw.rf1.go_state(RF_STATE_RESET)
        
        
#         current_state = gw.rf1.read_current_state()
#         print "rf19 current state : " + hex(current_state)
#          
#         gw.rf1.write(AT86RF215_RF_RST, AT86RF215_RF_RST_MASK)
# #         time.sleep(1)
#           
#         current_state = gw.rf1.read_current_state()
#         print "rf19 current state : " + hex(current_state)
#           
#         gw.rf1.read(AT86RF215_RF09_IRQS, 1, 1)
#          
#         current_state = gw.rf1.read_current_state()
#         print "rf19 current state : " + hex(current_state)
          
#         gw.rf1.read(AT86RF215_RF09_IRQS, 1, 1)
#           
#         gw.rf1.go_state(RF_STATE_TXPREP)
#         time.sleep(1)
#         time.sleep(0.1)
#          
#         # set RF module as Receiver
#         gw.rf1.reset()
#         gw.rf1.irq_enable()
#         gw.rf1.IQ_radio_enable()
#         gw.rf1.receiver_frontend_config()
#         gw.rf1.channel_config()
#         gw.rf1.configure_IQ_data_interface()      
#         gw.rf1.go_state(RF_STATE_TXPREP)
#         while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
#             pass
#             gw.rf1.go_state(RF_STATE_RX)
#         current_state = gw.rf1.read_current_state()
#         print "rf19 current state : " + hex(current_state)

    exit_script()
     
except KeyboardInterrupt:
    exit_script()
