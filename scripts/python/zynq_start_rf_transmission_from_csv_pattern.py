#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    print "debug> do not forget to configure the AT86RF215 chip !"
    gw.zynq.start_rf_tx_self_test()
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()