#!/usr/bin/python
from greenhill import *
from zynq_def import *
import time
import argparse
import math

def exit_script():
    gw.kill()
    
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("nbr_bits_symbol", type=int,
                    help="the number of bit per symbol")
    args = parser.parse_args()
    nbr_bits_symbol = args.fileName
    
    nbr_bytes_symbol = math.ceil(nbr_bits_symbol / 8)
    #if nbr_bits_symbol % 8 != 0:
    #    nbr_bytes_symbol = nbr_bytes_symbol + 1
    
    # config
    word_nb = 1024#RF_TX_BUFFER_WR_MAX_DEPTH
    output_csv_modulated = "pattern_modulated.csv"
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_ENABLE
    debug = 1
    nb_burst = 10
    
    # begin
    gw = Greenhill(USB_PROTOCOL)
    
    # reset RF TX element and wait for reset time
    gw.zynq.rf_tx_reset()
    time.sleep(0.5)
    
    # 2. modulate the pattern and fill RF TX buffer with modulated words 
    gw.zynq.rf_tx_start_modulation(ZYNQ_RF_TX_GWT_MOD_ENABLE)
        
    # wait for all modulated words are written into RF TX buffer 
    while (gw.zynq.rf_tx_get_buffer_index() < nbr_bytes_symbol *10):
        pass
        
    # read modulated pattern which was written into DDR
    if debug:
        gw.zynq.rf_tx_read_modulated_data(word_nb, output_csv_modulated)
            
    # 3. start RF TX transmission
    gw.zynq.start_rf_transmission()
        
    # wait for all words was sent
    while (gw.zynq.rf_tx_get_buffer_index() != 0):
        pass
    time.sleep(0.0000005) # wait for two last samples transmission (2*250ns)
        
    # 4. stop the RF TX transmission
    gw.zynq.stop_rf_transmission()
    
 


#         while 1:
#             pass


#        # required if you want to play a pattern in loop
#         gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)

    exit_script()
    
except KeyboardInterrupt:
    exit_script()