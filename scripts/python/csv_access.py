#!/usr/bin/python

# This python script is used to read xlsx file and store the content into a csv file.
# The library xlrd is necessary to read xlsx file. It can be install with the following command : sudo yum install python-xlrd

import os


class csv_access():
    def __init__(self):
        self.PROJECT_DIR  = os.path.dirname(os.path.realpath(__file__))+"/"
        
    # get sample from a csv file
    def get_sample(self, INPUT_FILE):
        # open and read csv file
        inputFileRef = open(self.PROJECT_DIR + INPUT_FILE,'r')
        rows = list(inputFileRef)
        
        # csv rows parsing
        sample_counter = 0
        buffer = ""
        for line in rows: 
            sample_counter += 1
            # get the sample
            data = int(line)
            buffer += chr(data & 0xFF) + chr((data>>8) & 0xFF) + chr((data>>16) & 0xFF) + chr((data>>24) & 0xFF)
        inputFileRef.close()
        return buffer
    
    # write a csv file
    def write_sample(self, OUTPUT_FILE, data_list, column_nb):
        outputFileRef = open(self.PROJECT_DIR + OUTPUT_FILE,'w')
        element = iter(data_list)
        for index in range(0, len(data_list), +column_nb):
            for column in range (0, (column_nb - 1), +1):
                outputFileRef.write(str(element.next()) + ', ')
            outputFileRef.write(str(element.next()) + '\n')
        outputFileRef.close()
        