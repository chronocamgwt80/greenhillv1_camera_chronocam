#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    print "debug> the FPGA user led will turn off"
    gw.zynq.stop_rf_transmission()
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()