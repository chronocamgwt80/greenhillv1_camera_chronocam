from card import *
from antenna_switch_def import *
from nucleo_def import *

class Antenna_switch(Card):
    
    def __init__(self, antenna_switch_id, protocol):
        self.baseAddr = NUCLEO_CMD_BASEADDR  
        Card.__init__(self, self.baseAddr, protocol)
        self.antenna_switch_id = antenna_switch_id
            
    def set_antenna_switch_config(self, config):
        # Bit -   IO
        # ---------------
        # [0] - RF SW V1
        # [1] - RF SW V2
        print "Ant Switch> Antenna Switch_" + str(self.antenna_switch_id) + " configuration which will be applied is: " + str(hex(config))
        if self.antenna_switch_id == ANT_SW_0:
            Card.write(self, NUCLEO_CMD_ANTENNA_SWITCH_0_CONFIG, config);
        elif self.antenna_switch_id == ANT_SW_1:
            Card.write(self, NUCLEO_CMD_ANTENNA_SWITCH_1_CONFIG, config);
        else:
            print "Ant Switch> bad Antenna Switch ID..."   
        
    def read_antenna_switch_config(self): 
        # Bit -   IO
        # ---------------
        # [0] - RF SW V1
        # [1] - RF SW V2
        if self.antenna_switch_id == ANT_SW_0:
            config = Card.read(self, NUCLEO_CMD_ANTENNA_SWITCH_0_CONFIG, 1, 0);
        elif self.antenna_switch_id == ANT_SW_1:
            config = Card.read(self, NUCLEO_CMD_ANTENNA_SWITCH_1_CONFIG, 1, 0);
        else:
            print "Ant Switch> bad Antenna Switch ID..."
            return 0x55
        antenna_sw = config['buf'][FRAME_HEADER_SIZE] & 0x3
        print "Ant Switch> Antenna Switch_" + str(self.antenna_switch_id) + " configuration is:" + str(hex(antenna_sw)) 
        return antenna_sw
        
        

        