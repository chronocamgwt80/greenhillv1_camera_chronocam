#!/bin/python

# imports
import sys
from tabulate import tabulate
import re
import struct
from zynq_def import *

#defines
iSyncPattern = 0x2
iSyncMask = 0xC0000000
iSyncOffset = 30
iSampleMask = 0x3FFE0000
iSampleOffset = 17
iCtrlBitMask = 0x00010000
iCtrlBitPattern = 1
iCtrlBitOffset = 16

qSyncPattern = 0x1
qSyncMask = 0x0000C000
qSyncOffset = 14
qSampleMask = 0x00003FFE
qSampleOffset = 1
qCtrlBitMask = 0x00000001
qCtrlBitPattern = 0
qCtrlBitOffset = 0

def hexIQSample2Dec(hexIQSample, output_selection):
    """
    Convert a hexadecimal IQ Sample to Decimal I,Q sample couple and print the conversion result
    """
    # I sample bits parsing
    iSync = (hexIQSample & iSyncMask) >> iSyncOffset
    decIsample = (hexIQSample & iSampleMask) >> iSampleOffset
    iCtrlBit = (hexIQSample & iCtrlBitMask) >> iCtrlBitOffset
    # Q sample bits parsing
    qSync = (hexIQSample & qSyncMask) >> qSyncOffset
    decQsample = (hexIQSample & qSampleMask) >> qSampleOffset
    qCtrlBit = (hexIQSample & qCtrlBitMask) >> qCtrlBitOffset
    # unsigned integer to integer
    decIsample = uint13_t_to_int13_t(decIsample)
    decQsample = uint13_t_to_int13_t(decQsample)
    # check iSync and qSync Pattern
    if iSync != iSyncPattern:
        print "bad iSync pattern"
    if qSync != qSyncPattern:
        print "bad qSync pattern"    
    # print conversion result
    table = [[iSync, decIsample, iCtrlBit, qSync, decQsample, qCtrlBit]]
    if output_selection == ACQ_OUTPUT_STANDARD:
      print "hexIQSample : 0x%08X" % (hexIQSample & 0xffffffff)
      print tabulate(table, headers=["iSync", "decIsample", "iCtrlBit", "qSync", "decQsample", "qCtrlBit"])
      print "0b" + "{0:032b}".format(hexIQSample)
      print ""
    else:
        if output_selection == ACQ_OUTPUT_DEC4CSV:
          print "{}, {}".format(decIsample, decQsample)
    
def hexIQSample2Dec_CSV(hexIQSample):
    """
    Convert a hexadecimal IQ Sample to Decimal I,Q sample couple and print the conversion result
    """
    # I sample bits parsing
    iSync = (hexIQSample & iSyncMask) >> iSyncOffset
    decIsample = (hexIQSample & iSampleMask) >> iSampleOffset
    iCtrlBit = (hexIQSample & iCtrlBitMask) >> iCtrlBitOffset
    # Q sample bits parsing
    qSync = (hexIQSample & qSyncMask) >> qSyncOffset
    decQsample = (hexIQSample & qSampleMask) >> qSampleOffset
    qCtrlBit = (hexIQSample & qCtrlBitMask) >> qCtrlBitOffset
    # unsigned integer to integer
    decIsample = uint13_t_to_int13_t(decIsample)
    decQsample = uint13_t_to_int13_t(decQsample)
    # check iSync and qSync Pattern
    if iSync != iSyncPattern:
        print "bad iSync pattern"
    if qSync != qSyncPattern:
        print "bad qSync pattern"    
    # print conversion result
    print "{}, {}".format(decIsample, decQsample)
    """
    table = [[iSync, decIsample, iCtrlBit, qSync, decQsample, qCtrlBit]]
    print "hexIQSample : 0x%08X" % (hexIQSample & 0xffffffff)
    print tabulate(table, headers=["iSync", "decIsample", "iCtrlBit", "qSync", "decQsample", "qCtrlBit"])
    print "0b" + "{0:032b}".format(hexIQSample)
    print ""
    """

def checkIQsample(IQsample):
    """
    Checkif iSync an qSync bits are correct
    """
    # I sample bits parsing
    iSync = (IQsample & iSyncMask) >> iSyncOffset
    # Q sample bits parsing
    qSync = (IQsample & qSyncMask) >> qSyncOffset
    # check iSync and qSync Pattern
    if iSync != iSyncPattern:
        print "bad iSync pattern"
        iSyncError = 1
    else:
        iSyncError = 0
    if qSync != qSyncPattern:
        print "bad qSync pattern"  
        qSyncError = 1
    else:
        qSyncError = 0
    return iSyncError, qSyncError
    
def decIQSample2Hex(DecISample, DecQSample):
    """
    Convert a decimal I,Q sample couple to a hexadecimal IQ Sample and print conversion result
    """
    hexIQSample = ((iSyncPattern << iSyncOffset) & iSyncMask) | ((DecISample << iSampleOffset) & iSampleMask) | ((iCtrlBitPattern << iCtrlBitOffset) & iCtrlBitMask) | ((qSyncPattern << qSyncOffset) & qSyncMask) | ((DecQSample << qSampleOffset) & qSampleMask) | ((qCtrlBitPattern << qCtrlBitOffset) & qCtrlBitMask) 
    table = [["%d"%(DecISample), "%d"%(DecQSample), "0x%08X"%(hexIQSample & 0xffffffff)]]
    print tabulate(table, headers=["DecISample", "DecQSample", "hexIQSample"])

    
def uint13_t_to_int13_t(X):
    if X > (2**(13-1)):
        X = X - (2**13)
    return X

# main     
if __name__ == '__main__':
    try:
        # get conversion type
        if len(sys.argv) > 1:
            conversionType = sys.argv[1]
        else:
            print "Please enter the wanted conversion and the sample to convert .eg: dec2hex or hex2dec"
            sys.exit(1)
        
        # hex2dec conversion, get hexSample
        if conversionType == "hex2dec":
            if len(sys.argv) > 2:
                hexIQSample = int()
                string_hex_data = re.sub('0x', '', sys.argv[2]) # do not forget to import re
                hexIQSample = struct.unpack('!i', str(string_hex_data).decode('hex'))[0]  
                hexIQSample2Dec(hexIQSample)
            else:
                print "Please enter the hexadecimal IQ sample to convert .eg: ./iqSampleAnalyzer.py hex2dec 0x81fe7f98"
                sys.exit(1)
        
        # dec2hex conversion, get decSample I and Q
        if conversionType == "dec2hex":
            if len(sys.argv) > 3:
                DecISample = int(sys.argv[2])
                DecQSample = int(sys.argv[3])
                decIQSample2Hex(DecISample, DecQSample)
            else:
                print "Please enter the decimal I and Q samples to convert .eg: ./iqSampleAnalyzer.py dec2hex -10 508"
                sys.exit(1)    
            
    except KeyboardInterrupt:
        pass
        
        