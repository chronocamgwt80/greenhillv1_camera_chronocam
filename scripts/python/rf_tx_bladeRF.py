#!/usr/bin/python

# import
#from sys import *
import sys
#from At86rf215 import *
#from zynq import *
from greenhill import *
from zynq_def import *
from ofdm import *
import math, getopt

def convert32bits(word32b, buffer):
    buffer = buffer + (chr((word32b) & 0xFF) + chr((word32b >> 8) & 0xFF) + chr((word32b >> 16) & 0xFF) + chr((word32b >> 24) & 0xFF))
    return buffer

def configure():
    word_nb = 1024#RF_TX_BUFFER_WR_MAX_DEPTH
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_DISABLE
    debug = 1
    nb_burst = 10
    output_csv_modulated = "pattern_modulated.csv"
    
    DEFAULT = -1
    CONV_OFDM = 0
    GREEN_GREEN = 1
    GREEN_CONV = 2
    NFFT = 256 # default FFT size
    NCP = 64 # default NCP size
    N_MIN = 2
    N_MAX = 2048 # max Number of tones
    NCP_MIN = 0 # max cyclic prefix
    NCP_MAX = (N_MAX/2) # max cyclic prefix
    MINBACKOFF = -40
    MAXBACKOFF = 40
    DEF_BACKOFF_INDX = 4
    
    nbr_symbol2send = 1

    backoff_v = 0
    mode = 2
    nfft_v = 64
    ncp_v = 16
    null_tones_v = 8
    nsub_sect_v = -1
    constell_v = 2
    dmod = 0
    nbr_bits_symbol = 156
    gain_float = -1.0
    gain_bef_pt = 0
    gain_aft_pt = 32  
    nbr_symbol = 10
    GEN_RANDOM_BITSTREAM = 1
  
    nbr_bits_tx = nbr_symbol * nbr_bits_symbol
    nbr_bytes_tx = int(math.ceil(nbr_bits_tx / 8))
    
    print "Python parse_options (-1 means default): backoff=%s gain=%s gain_bef_pt=%s gain_aft_pt=%s mode=%s dmod=%s nfft_v=%s ncp_v=%s null_tones_v=%s nsub_sect_v=%s constell_v=%s GEN_RANDOM_BITSTREAM=%s nbr_bits_symbol=%s nbr_symbol=%s nbr_bytes_tx=%s" %(backoff_v, gain_float, gain_bef_pt, gain_aft_pt, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v, GEN_RANDOM_BITSTREAM, nbr_bits_symbol, nbr_symbol, nbr_bytes_tx)

    
    checksum = 0
    stop_byte = 0x55
    cmd = chr(checksum&0xFF) + chr(stop_byte&0xFF)
  
    #buf = [backoff_v, gain_bef_pt, gain_aft_pt, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v, GEN_RANDOM_BITSTREAM, nbr_bits_symbol, nbr_symbol nbr_bytes_tx]
    buf_size = 4 +      4 +     +	4 	   +4 + 4 +     4 +     4 +     4 +         4 +         4 +         4 +                 4                   + 4          + 4
    print "buf_size={}".format(buf_size)
    buf = str()
    buf = (convert32bits(backoff_v, buf) + convert32bits(int(gain_bef_pt), buf) + convert32bits(int(gain_aft_pt), buf)  + convert32bits(mode, buf)
                        + convert32bits(dmod, buf)
                        + convert32bits(nfft_v, buf) + convert32bits(ncp_v, buf) + convert32bits(null_tones_v, buf)
                        + convert32bits(nsub_sect_v, buf) + convert32bits(constell_v, buf) + convert32bits(GEN_RANDOM_BITSTREAM, buf)
                        + convert32bits(nbr_bits_symbol, buf) + convert32bits(nbr_symbol, buf) + convert32bits(nbr_bytes_tx, buf))
    #print "buf.length=%s" %(len(buf))
        
    add = ZYNQ_CMD_BASEADDR + ZYNQ_CMD_GWT_CONFIGURE
    data = 0
    checksum = 0
    stop_byte = 0x55
    
    # begin
    #gw = Greenhill(USB_PROTOCOL)
    
    cmd = (chr((add)&0xFF) + chr((add>>8)&0xFF) + chr((add>>16)&0xFF) + chr((add>>24)&0xFF)
        + chr(buf_size&0xFF) + chr((buf_size>>8)&0xFF)
        + chr((buf_size>>16)&0xFF) + chr((buf_size>>24)&0xFF)
        + buf
        + chr(checksum&0xFF) + chr(stop_byte&0xFF))
    gw.zynq.write_raw(cmd)


def exit_script():
    gw.kill()

try:
    #dictionary for the TX power 
    #AT86RF215 p206: Figure 11-2. Output power vs. RF09_PAX.TXPWR register for several modulations at f channel =900MHz
    #curve of OFDM n 3 and n 4
    pwr = {'m19': 0, 'm18': 1, 'm17': 2, 'm16': 3, 'm15': 4, 'm14': 5, 'm13': 6, 'm12': 7, 'm11': 8, 'm10': 9, 'm9': 10, 'm8': 12, 'm7': 13, 'm6': 14, 'm5': 15, 'm4': 16, 'm3': 17, 'm2': 18, 'm1': 19, '0': 20, '1': 21, '2': 22, '3': 23, '4': 24, '5': 25, '6': 26, '7': 27, '8': 28, '9': 29, '10': 30, '11': 31} 
    pacur = 0 #0x0 Power amplifier current reduction by about 22mA (3dB reduction of max. small signal gain)
    tx_pwr = 31
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_ENABLE

    # PA config
    pa1_mode = PA_TRANSMIT
    pa1_antenna = PA_ANT1_EN
    
    # Antenna Switch Config                                   
    sw1_mode = ANTENNA_SWITCH_TX_MODE
    
    # Sequence (to launch a transmission)
    # 0. Create Main object
    # 1. Reset all modules/elements
    # 2. Set Antenna Switch as TX
    # 3. Set PA as TX
    # 4. Set RF modules as TX
    # 5. Start Transmission
    
    # 0. Create Main object
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.rf_tx_get_buffer_index()
    # 1. reset
    gw.rf1.reset()
    gw.pa1.reset()
    gw.zynq.rf_tx_get_buffer_index()
    #gw.zynq.reset()
   
    # 2. Set Antenna Switch as TX
    gw.antenna_switch1.set_antenna_switch_config(sw1_mode)
    time.sleep(0.1)
    gw.antenna_switch1.read_antenna_switch_config()
    
    # 3. Set PA as TX
    gw.pa1.set_io(pa1_mode, pa1_antenna)
    
    # 4. Set RF modules as TX
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.transmitter_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.transmit_power_config(pacur, tx_pwr)
    gw.rf1.loopback_enable(0)#desactive the loopback
    gw.rf1.go_state(RF_STATE_TXPREP)
    while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
        pass
    #reset the TX block after the RF clock availability
    #other the part of the buffer with the RF clock will not be reseted
    gw.zynq.rf_tx_reset()
    gw.rf1.go_state(RF_STATE_TX)
    current_state = gw.rf1.read_current_state()
    print "RF09 current state : " + hex(current_state)
    #print "value auxs regiter=%d" %(gw.rf1.read_auxs_register())
    #transmitter.kill()

    gw.zynq.reset()
    
    #3 read IQ file and fill the fifo with it
    #zynq = Zynq()
    #read_IQ_file_fill_fifo(csv_file)
    
    #4 Configure in loop mode the buffer
    #zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_BURST_MODE)
    gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
    time.sleep(2)
    #configure()
    time.sleep(1)
    gw.zynq.rf_tx_enable_check_modulation_data()
    time.sleep(1)
    gw.zynq.rf_tx_start_modulation(modulation_mode)
    time.sleep(10)
    
    #5 start RF TX
    gw.zynq.start_rf_transmission()
    
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()

