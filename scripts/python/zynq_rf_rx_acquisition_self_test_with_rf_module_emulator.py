#!/usr/bin/python

import time
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    # RF RX config
    module_selected = ATMEL_EMULATOR
    acquisition_time_ns = RF_RX_ACQ_TIME_NS
    # RF module config
    pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    txpwr = 0x0                         # TXPWR controls the transmit output power setting
    
    # create API top object
    gw = Greenhill(USB_PROTOCOL)

    # reset du module RF
    gw.rf1.reset()
    
    # reset RF RX
    gw.zynq.rf_rx_reset()
    
    # start acquisition
    gw.zynq.rf_rx_acq_apply_config(module_selected, acquisition_time_ns) 
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_acq_start() # acquisition will start when the RF module will send the RF clock and the first sample to the deserializer of the FPGA

    # check synchronization of the deserializer of the FPGA (should be not sync)
    gw.zynq.rf_rx_read_sync_flag()

    # set RF module as Receiver
    gw.rf1.reset()
    gw.rf1.irq_enable()
    gw.rf1.IQ_radio_enable()
    gw.rf1.receiver_frontend_config()
    gw.rf1.channel_config()
    gw.rf1.configure_IQ_data_interface()      
    gw.rf1.go_state(RF_STATE_TXPREP)
    #while (gw.rf1.read_irq_reg() != IRQS_TRXRDY_MASK):
    #    pass
    gw.rf1.go_state(RF_STATE_RX)
    current_state = gw.rf1.read_current_state()
    print "rf19 current state : " + hex(current_state)
    # RF clock is now sent to the FPGA, emulateur generates samples...

    # wait the end of the acquisition
    time.sleep(1)

    # read samples acquired
    gw.zynq.rf_rx_acq_read_config()
    gw.zynq.rf_rx_acq_read_samples_acq(acquisition_time_ns, ACQ_OUTPUT_STANDARD)       
    exit_script()
     
except KeyboardInterrupt:
    exit_script()
