#!/usr/bin/python
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    # Command to send in two times
    add = ZYNQ_CMD_USER_LED
    regnbbytes = 1
    data = ZYNQ_USER_LED_ON
    checksum = 0
    stop_byte = 0x55
    
    # send first packet
    cmd = chr(add & 0xFF) + chr((add >> 8) & 0xFF) + chr((add >> 16) & 0xFF) + chr((add >> 24) & 0xFF)
    cmd = cmd + chr(regnbbytes & 0xFF) + chr((regnbbytes >> 8) & 0xFF) + chr((regnbbytes >> 16) & 0xFF) + chr((regnbbytes >> 24) & 0xFF)
    cmd = cmd + chr(data & 0xFF) + chr(checksum & 0xFF) + chr(stop_byte & 0xFF)
    gw.zynq.write_raw(cmd)
    
    time.sleep(1)
    # send second packet
#     gw.zynq.write_raw(cmd)
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()