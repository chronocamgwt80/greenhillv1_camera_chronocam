#!/usr/bin/python

# import
from greenhill import *
from zynq_def import *

def exit_script():
    gw.kill()
    
try:
    # config
    word_nb = 1024#RF_TX_BUFFER_WR_MAX_DEPTH
    input_csv = "pattern.csv"                       # existing file, format: one column of 32bits words
    input_csv_read = "pattern_debug.csv"            # will be created, format: one column of 32bits words
    output_csv_modulated = "pattern_modulated.csv"  # will be created, one column of I, one column of Q
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_ENABLE     # enable/disable GWT modulator
    debug = 1                                       
    nb_burst = 1
    
    # begin
    gw = Greenhill(USB_PROTOCOL)
    
    # reset RF TX element and wait for reset time
    gw.zynq.rf_tx_reset()
    time.sleep(0.5)
    
    for i in range (nb_burst):
        # 1. create word pattern to modulate and send it to zynq
        gw.zynq.create_csv_pattern(word_nb)
        gw.zynq.rf_tx_prepare_data_to_modulate(input_csv)
        
        # read pattern which was written into DDR
        if debug:
            #1st configure the zynq for this
            gw.zynq.rf_tx_enable_check_modulation_data()
            print "==DEBUG read pattern which was written into DDR"
            gw.zynq.rf_tx_read_data_to_modulate(word_nb, input_csv_read)
             
        # 2. modulate the pattern and fill RF TX buffer with modulated words
        gw.zynq.rf_tx_start_modulation(modulation_mode)
        time.sleep(25)
         
        print "wait for RF TX modulation ready flag then get output modulation size"
        gw.zynq.rf_tx_get_modulation_ready()
        while (not gw.zynq.modulation_ready):
            time.sleep(1)
            gw.zynq.rf_tx_get_modulation_ready()
 
        print "modulation_ready={} modulated_data_len={}".format(gw.zynq.modulation_ready, gw.zynq.modulated_data_len)
         
        # wait for all modulated words are written into RF TX buffer 
        #while (gw.zynq.rf_tx_get_buffer_index() < gw.zynq.modulated_data_len):
        #    pass
         
        # read modulated pattern which was written into DDR
        if debug:
            gw.zynq.rf_tx_enable_check_modulation_data()
            print "==DEBUG read modulated pattern which was written into DDR"
            gw.zynq.rf_tx_read_modulated_data(gw.zynq.modulated_data_len, output_csv_modulated)
             
#         zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)
         
        # 3. start RF TX transmission
        gw.zynq.start_rf_transmission()
         
        # wait for all words was sent
        while (gw.zynq.rf_tx_get_buffer_index() != 0):
            pass
        time.sleep(0.0000005) # wait for two last samples transmission (2*250ns)
         
        # 4. stop the RF TX transmission
        gw.zynq.stop_rf_transmission()


#         while 1:
#             pass


#        # required if you want to play a pattern in loop
#         gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)

    exit_script()
    
except KeyboardInterrupt:
    exit_script()
