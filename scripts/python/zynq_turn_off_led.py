#!/usr/bin/python
from greenhill import *

def exit_script():
    gw.kill()
    
try:
    gw = Greenhill(USB_PROTOCOL)
    gw.zynq.turn_off_user_led()
      
    exit_script()
    
except KeyboardInterrupt:
    exit_script()