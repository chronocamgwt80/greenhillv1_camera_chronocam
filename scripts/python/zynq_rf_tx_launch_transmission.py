#!/usr/bin/python
from greenhill import *
from zynq_def import *
import time

def exit_script():
    gw.kill()
    
try:
    # config
    word_nb = 1024#RF_TX_BUFFER_WR_MAX_DEPTH
    input_csv = "pattern.csv"
    input_csv_read = "pattern_debug.csv"
    output_csv_modulated = "pattern_modulated.csv"
    modulation_mode = ZYNQ_RF_TX_GWT_MOD_DISABLE
    debug = 1
    nb_burst = 1
    
    # begin
    gw = Greenhill(USB_PROTOCOL)
    
    # reset RF TX element and wait for reset time
    gw.zynq.rf_tx_reset()
    time.sleep(0.5)
    
    for i in range (nb_burst):
        # 1. create word pattern to modulate and send it to zynq
        gw.zynq.create_csv_pattern(word_nb)
        gw.zynq.rf_tx_prepare_data_to_modulate(input_csv)
        
        # read pattern which was written into DDR
        if debug:
            gw.zynq.rf_tx_read_data_to_modulate(word_nb, input_csv_read)
            
        # 2. modulate the pattern and fill RF TX buffer with modulated words 
        gw.zynq.rf_tx_start_modulation(modulation_mode)
        
        # wait for all modulated words are written into RF TX buffer 
        time.sleep(30.0) # wait modulation and perfect modulator to be finished
        while (gw.zynq.rf_tx_get_buffer_index() < word_nb):
            pass
        
        # read modulated pattern which was written into DDR
        if debug:
            gw.zynq.rf_tx_read_modulated_data(word_nb, output_csv_modulated)
            
        # 3. start RF TX transmission
        gw.zynq.start_rf_transmission()
        
        # wait for all words was sent
        while (gw.zynq.rf_tx_get_buffer_index() != 0):
            pass
        time.sleep(0.0000005) # wait for two last samples transmission (2*250ns)
        
        # 4. stop the RF TX transmission
        gw.zynq.stop_rf_transmission()


#         while 1:
#             pass


#        # required if you want to play a pattern in loop
#         gw.zynq.rf_tx_set_loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE)

    exit_script()
    
except KeyboardInterrupt:
    exit_script()
