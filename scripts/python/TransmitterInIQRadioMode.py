#!/usr/bin/python
from greenhill import *
from At86rf215 import *

def exit_script():
    gw.kill()
    
try:
    # config
    pacur = 0x0                         # PACUR configures the power amplifier DC current (useful for low power settings).
    txpwr = 0xF                         # TXPWR controls the transmit output power setting
    loopback = EXTLB_LOOPBACK_DISABLE   # If the bit is set to 1, the received data of the I/Q IF pin TXDn/p is fed back via the pins RXDxxn/p.
    
    gw = Greenhill(USB_PROTOCOL)
    gw.rf1.set_transmitter_as_iq_radio_mode()
    
    exit_script()
    
except KeyboardInterrupt:
    exit_script()