#!/bin/bash

ls */*.tex > file_list 

while read file_list_line
do
	echo "convert file :"  ${file_list_line} 
	file_to_convert=$(basename ${file_list_line} | sed "s/\.tex//g")
	dir_to_convert=$(echo "${file_list_line}" | sed 's#/[^/]*$##')
	cd ${dir_to_convert}
	pdflatex -interaction "nonstopmode" ${file_to_convert}.tex > log_file
	pdfcrop --margins "0 0 0 0" --hires --clip ${file_to_convert}.pdf ${file_to_convert}.pdf >> log_file
	convert -density 200 ${file_to_convert}.pdf ${file_to_convert}.jpg >> log_file
        rm -f *.aux *.log .log log_file

	cd ..
done < file_list

rm -f file_list 







