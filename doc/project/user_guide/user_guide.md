% Greenhill - USER GUIDE
% MB, AV, CLB, AL

# Introduction
This user guide describes how to getting started with the low power WAN physical demonstrator realized by Asygn. It is designed in order to develop and characterize the GreenWaves's specific modulation.  

This platform provided two Radio Frequency (RF) channels for 900 $MHz$ and 2400 $MHz$ bands. The hardware is built to integrate two channels but according to the initial specification, the embedded firmware is fully functional for only switchable channel.  

Following sections contain all information for users to understand the content of each part of the demonstrator.  
\pagebreak  

# Getting started
## Hardware overview
The platform designed by ASYGN could be used as a single or dual channels transceiver at 900 $MHz$ or 2400 $MHz$.
It is composed by several components:  
- the ASYGN mother board named **"gw_power_rev0"** : this board is used to interconnect each board of the demonstrator and to power it.  
- the mechanical support : it is design specifically for this application. The rear holder can be easily removed without any tool.  
- One (or two) Radio Frequency modulator : AT86RF215 provided by ATMEL.  
- One (or two) Radio Frequency switch : AS211-334 provided by Skyworks.  
- One (or two) Radio Frequency amplifier : SKY65362_11 for the 900 $MHz$ mode or SKY85309_11 for the 2400 $MHz$ mode. Both modules are provided by Skyworks.  
- the power supply : 230 $V_{AC}$ to 6 $V_{DC}$ up to 3 $A$.  

![ Hardware Overview ](/nfs/work/board/pictures/greenwaves/hardware/hardware_overview_reshape.jpg "Title")  

The figure 1 corresponds to the one channel sub gigahertz platform.

\pagebreak  

### Zynq
A FPGA board call Zturn is plugged on the mother board by using connectors J34 and J35.  
**Warning :** The Zturn could be physically placed in both sides but only one is functional. If the board is connected in a wrong side it could damage the Fpga.
To avoid any trouble, the board must be plugged as follow:

![ Zynq connection ](/nfs/work/board/pictures/greenwaves/hardware/zynq_placement.jpg "Title")  

The Ethernet connector must face the nucleo board.

\pagebreak  

In order to download all the embedded code (firmware + bitstream) of the Ztrun board, we are using the Xilinx Jtag probe with dedicated flying wires.
The following figure shows how to plug it:

![ Zynq programmation ](/nfs/work/board/pictures/greenwaves/hardware/zturn_jtag.jpg "Title")

Zturn J1 Jtag signal       Flying wire color
-------- ----------------- -----------------
1        GND               Black
3        Not connected     -
5        Not connected     -
7        Not connected     -
9        Not connected     -
11       Not connected     -
13       Not connected     -
2        Vref              Red
4        Tms / Prog / SS   Green
6        Tck / Cclk / Sck  Yellow
8        Tdo / Done / Miso Purple
10       Tdi / Din / Mosi  White
12       Not connected     -
14       Halt / Init / WP  Grey

This adaptator can be find on common distributor like Digikey (122-1530-ND).

The Zturn board is a standard platform designed by MyIR company.
It integrates a Zynq from Xilinx and all external components necessary to use it in various mode.
However, to reach the Greenwaves application needs several components must be removed:  
- FB5 : to allow external power supply for the bank 35.  
- D12 / U3: to avoid USB self power supply issue.  
- D27: to avoid UART self power supply issue.  


\pagebreak  

### Nucleo

The nucleo board contains a dedicated firmware loaded by the STlink through the connector CN1.  

![ Nucleo connection ](/nfs/work/board/pictures/greenwaves/hardware/nucleo_placement.jpg "Title")  

In order to properly works, Jumpers must be settled as follow:

Nucleo connector State                   Comments
---------------- ----------------------- -------------------------
JP5              1-2: open / 2-3: short  External power selected
JP6              1-2: short              Consumption measurement (Nucleo board only)
CN2              1-2: short / 2-3: short STlink
CN3              1-2: open               STlink
CN4              Not connected           debug connector
CN11             1-2: short              Ground Connector
CN12             1-2: short              Ground Connector

It is also necessary to short SB49 (bottom side).

\pagebreak  

### Atmel
The Atmel module is connected to the Nucleo board through a simple SPI for to set all registers, whereas the I/Q data came from the Zturn board through SATA bus.

![ Atmel connection ](/nfs/work/board/pictures/greenwaves/hardware/IMG_0133.jpg "Title")  

Setting (SPI) connection:  

Atmel connector | Mother board channel 1 | Mother board channel 2
:-------------: | :--------------------: | :--------------------:
J100            | J7                     | J4

I/Q connection:  

Atmel connector | Mother board channel 1 | Mother board channel 2
:-------------: | :--------------------: | :--------------------:
J101            | J21                    | J22
J102            | J20                    | J23
J103            | J19                    | J24 


**Note:** due to the FPGA input clock limitation, two SATA cables must be swapped regarding the first delivery.
This limitation was highlighted by the RX mode were the input clock must be routed to an input capable clock (MRCC or SRCC) pin.
The table I/Q connection shows the proper way to connect the ATMEL board and the GW_power board.



\pagebreak  

### Switch
In order to avoid destructive RF settings for the Atmel module. A standard RF switch is used turn the platform into the receiver mode or the transmitter mode.
This component is driven by the Nucleo and it is wired as follow:

![ Radio frequency switch command connection ](/nfs/work/board/pictures/greenwaves/hardware/switch_command.jpg "Title")  

\pagebreak  

![ Radio frequency switch data connection ](/nfs/work/board/pictures/greenwaves/hardware/IMG_0130.jpg "Title")  

\pagebreak  

### Power amplifier
The power amplifier is used to add some gain to the RF signal.
Depending on the mode that we use,900 $MHz$ or 2400 $MHz$, two power amplifiers can be placed on each channel (but only once at a time for each channel).  
For the 900 $MHz$ mode, the amplifier is connected as follow:

![ Power amplifier connection ](/nfs/work/board/pictures/greenwaves/hardware/IMG_0132.jpg "Title")  

The embedded power supply of the power amplifier can be settled with a potentiometer placed on the bottom side of the mother board or forced to the nominal value by a Jumper.  

![ Power amplifier supply ](/nfs/work/board/pictures/greenwaves/hardware/IMG_0225.jpg "Title")  

For more details, see the section [Electrical characteristic](#elecLink).


\pagebreak  

## Standard usage
Run the script ./check_module_connection.py to try to detect connection issues between all boards.

### transmitter mode
Programming Sequence for a quick start.  
- Configure modules of the RF TX chain (it is recommended to configure respect this sequence) :
  1. Reset all modules/elements
  2. Set Antenna Switch as TX
  3. Set SKY65362 as TX
  4. Set AT86RF215 as TX

An example of programming sequence is provided in this python script: start_standart_transmission.py
- Launch a transmission using the GWT modulator:
  1. Reset RF TX module of the Zturn board,
  2. Prepare data to modulate,
  3. Read data stored in DDR (optional),
  4. Enable modulator and start modulation,
  5. Read modulated data (optional),
  6. Start transmission (enable the play in loop mode: optional).

An example of programming sequence is provided in this python script: zynq_rf_tx_launch_transmision_algo_gw.py

### receiver mode
Programming Sequence for a quick start:  
- Configure modules of the RF RX chain (it is recommended to configure respect this sequence) :
  1. Reset all modules/elements
  2. Set AT86RF215 as RX
  3. Set SKY65362 as RX
  4. Set Antenna Switch as RX

  An example of programming sequence is provided in this python script: start_standart_reception.py  

- Set the Zturn board:
  1. Reset RF RX module of the Zturn board,
  2. Configure the RF RX module of the Zturn board,
  3. Start RF RX acquisition,
  4. Wait for the end of the acquisition (time configured)
  5. Read configuration of the RF RX module of the Zturn board (optional)
  6. Read the samples acquired.

An example of programming sequence is provided in this python script: zynq_rf_rx_acquisition.py  

\pagebreak  

# Embedded firmware
## Zturn board
### Zturn In/Out
Zturn is the master board of the Greenhill platform. It carries out the communication between the User API and the other boards. The following figure shows the mains In/Out of the Zturn board:  

![ Zturn InOut ](img/zturn/zturninout.png "Title")  

In/Out:  
- USB: Communication between the User API executed on the PC and the program executed on the Zynq of the Zturn board. Exchanged data type = config and data commands. The PC is a master and the Zturn is a slave.  
- (RF_RXCLK, RF_RXD09) & (RF_RXD24) & (RF_TXD, RF_TXCLK) are the signals of the Serial I/Q Data Interface (J101, J102, J103). cf. ATREB215 extension board user guide for more information.  
- SPI: Communication between program executed on the Zynq of the Zturn board and the program executed on the STM32 of the Nucleo board. Exchanged data type = config and data commands. Zturn is a master SPI and Nucleo is a slave SPI. The Zturn board is a hub for the communication between the PC and the Nucleo.  

\pagebreak  

### Firmware Diagram
The following figure shows mains features implemented in the Zynq of the Zturn board:

![ Zturn Firmware ](img/zturn/zturnfirmware.png "Title")  

Features:  
- USB Peripheral and Driver: carry out the USB communication with the User API run on the PC. Send/receive commands and data transfer to/from PC,  
- SPI Peripheral and Driver: carry out the communication with the Nucleo board. Send/receive commands to/from Nucleo.  
- App Controller: decode commands,  
- Modulator + RF TX Interface + Buffer + Serializer: Transmitter chain,  
- BRAM Controller + CDMA + BRAM + Deserializer: Receiver chain.  
- Emulator: Generate I,Q samples when enabled. It is used for debug to test the acquisition chain (BRAM Controller + CDMA + BRAM)
- In the FPGA, the Transmitter chain and the Receiver chain require the RF_RXCLK to work. RF_RXCLK is supplied by ATREB215 board. Set the AT86RF215 chip of the ATREB215 board in TXPREP mode and the RF_RXCLK will be transfer to the zturn.

\pagebreak  

### Examples of transmitting sequences
The following examples assume that all RF TX elements of the firmware are reseted and the ATREB215 is set in transmitter mode.

#### Transmission with modulation
The following figure shows the steps to follow to use modulator and start a transmission:

![ Transmission using modulator ](img/zturn/tx_modulation_example.png "Title")

Sequence:  
1. Send a Write Command with words to modulate. A word consists of 4 bytes. This words are stored in DDR and can be read from User API by sending a Read Command. Command ID to send = ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE. The CSV consists of a list of 4 bytes words.  
2. Send a Write Command to modulate words and fill the buffer. Modulated words consists of I,Q samples. I = 2 bytes, Q = 2 bytes.  I,Q samples are also stored in DDR. Command ID to send = ZYNQ_CMD_RF_TX_START_MOUDULATION. The argument modulation_debug_mode (or data[0] of the frame) has to be set as 0x1 to enable the modulator.  
3. Send a Read Command to read the  I,Q samples stored in DDR. Command ID to send = ZYNQ_CMD_RF_TX_START_MOUDULATION.  
4. Send a Write Command to send  I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.  

The size of the pattern to modulate is different than the size of the modulated pattern. Send a Read Command with Command ID = ZYNQ_CMD_RF_TX_MODULATED_DATA_LENGTH to know the size of the modulated pattern.  

\pagebreak  

#### Transmission with modulator by pass
The following figure shows the steps to follow to by pass the modulator and start a transmission:

![ Transmission with modulator by pass ](img/zturn/tx_bypassmodulation_example.png "Title")

Modulator by pass feature was implemented for the debug to test the process without the modulator.
Sequence:  
1. Send a Write Command with words to modulate. A word consists of 4 bytes. This words are stored in DDR and can be read from User API by sending a Read Command. Command ID to send = ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE. The CSV consists of a list of 4 bytes words.  
2. Send a Write Command to by pass the modulator and fill the buffer with the words. The first two bytes of the Word will be a I and the last two word will be a Q. Words are also stored in DDR. The argument modulation_debug_mode (or data[0] of the frame) has to be set as 0x0 to disable the modulator and by pass it.  
3. Send a Read Command to read the I,Q samples stored in DDR. Command ID to send = ZYNQ_CMD_RF_TX_START_MOUDULATION. The CSV consists of a list of I,Q samples. All I are written on the first column and all Q are written on the second column.  
4. Send a Write Command to send  I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.  

\pagebreak  

#### Transmission by filling buffer with I,Q samples
The following figure shows the steps to follow to fill buffer with I,Q samples and start a transmission:

![ Transmission by filling buffer with I,Q samples](img/zturn/tx_fill_buffer.png "Title")  

Sequence:  
1. Send a Write Command with I,Q samples. The Command ID to send : ZYNQ_CMD_RF_TX_FILL_BUFFER. Check the number of I,Q samples stored buffer with the following Command ID : ZYNQ_CMD_RF_TX_GET_BUFFER_INDEX.  
2. Send a Write Command to send I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.

\pagebreak  

#### Transmission with pattern stored in pattern.h
The following figure shows the steps to follow to start a transmission with the pattern stored in pattern.h file of the Zturn Firmware:  

![ Transmission with pattern stored in pattern.h ](img/zturn/tx_pattern_h.png "Title")

This feature was implemented to test quickly the transmission. Check the OFDM spectrum of the transmit signal with a Spectrum Analyzer.<br>Sequence:  
1. Send a Write Command to fill buffer with I,Q samples stored in pattern.h. Command ID to send = ZYNQ_CMD_RF_TX_START_SELF_TEST.  
2. Send a Write Command to send  I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.  

\pagebreak  

#### Transmission in burst mode
The following figure shows the steps to follow to start a transmission and play the pattern stored in buffer once:  

![ Tranmission with playing once the pattern stored in buffer ](img/zturn/tx_no_playloop.png "Title")

Sequence:  
1. Fill buffer a I,Q samples pattern. Send a Write Command to enable the burst mode (each sample read from FIFO is sent only once). Command ID to send = ZYNQ_CMD_RF_TX_LOOP_BURST_MODE. The argument mode (or data[0] of the frame) has to be set as 0x0 to enable the burst mode.  
2. Send a Write Command to send I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.  

\pagebreak  

#### Transmission in play in loop mode
The following figure shows the steps to follow to start a transmission and play in loop the pattern stored in buffer:  

![ Transmission with playing in loop the pattern stored in buffer ](img/zturn/tx_playloop.png "Title")

Sequence:  
1. Fill buffer with an I,Q samples pattern. Send a Write Command to enable the loop mode (each sample read from FIFO is rewritten into the FIFO). Command ID to send = ZYNQ_CMD_RF_TX_LOOP_BURST_MODE. The argument mode (or data[0] of the frame) has to be set as 0x1 to enable the loop mode.  
2. Send a Write Command to send  I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.  

\pagebreak  

### Examples of receiving sequences
The following examples assume that all RF RX elements of the firmware are reseted and the ATREB215 is set in receiver mode. Acquisition time is limited to 250ms (1.000.000 I,Q samples)

#### Standard RF RX acquisition
The following figure shows the steps to follow to start a standard RF RX acquisition:  

![ Standard RF RX acquisition](img/zturn/rx_standard.png "Title")

Sequence:  
1. Send a Write Command to configure the BRAM Controller. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_CONFIG. The argument 'module_selected' (or data[0] of the frame) has to be set as 0x0 to disable Emulator and the argument 'samples_nb' (or data[1] & data[2] & data[3] & data[4] of the frame) as to be as 1000000 at maximum (not reliable over this value).
2. Send a Write Command to start the acquisition. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_START. Then wait for the acquisition time.
3. Send a Write Command to read acquired I,Q samples. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES. (It is a bug in the program to send a Write Command to read I,Q samples)

\pagebreak  

#### RF RX acquisition with emulator
The following figure shows the steps to follow to start a RF RX acquisition with emulator:  

![ RF RX acquisition with emulator](img/zturn/rx_emulator.png "Title")

This feature was implemented to test quickly the RF RX acquisition. Check the value of received I,Q samples are the value of a counter.<br>Sequence:  
1. Send a Write Command to configure the BRAM Controller. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_CONFIG. The argument 'module_selected' (or data[0] of the frame) has to be set as 0x1 to enable Emulator and the argument 'samples_nb' (or data[1] & data[2] & data[3] & data[4] of the frame) as to be as 1000000 at maximum (not reliable over this value).
2. Send a Write Command to start the acquisition. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_START. Then wait for the acquisition time.
3. Send a Write Command to read acquired I,Q samples. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES. (It is a bug to in the program to send a Write Command to read I,Q samples)

\pagebreak  

#### RF RX acquisition with loopback
The following figure shows the steps to follow to start a RF RX acquisition with loopback:  

![ RF RX acquisition with loopback](img/zturn/rx_loopback.png "Title")

This feature was implemented to test quickly the RF RX acquisition. Check the values of received I,Q samples are the value of the sent I,Q samples.<br>Sequence:<br> (before start)Fill buffer with an I,Q samples pattern and enable loopback mode of the AT86RF215 chip of the ATREB215 board.
1. Send a Write Command to send I,Q samples to IQ Radio Interface of the ATREB215 board and start the transmission. Command ID to send = ZYNQ_CMD_START_RF_TX_TRANSMISSION.
2. Send a Write Command to configure the BRAM Controller. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_CONFIG. The argument 'module_selected' (or data[0] of the frame) has to be set as 0x1 to disable Emulator and the argument 'samples_nb' (or data[1] & data[2] & data[3] & data[4] of the frame) as to be as 1000000 at maximum (not reliable over this value).
3. Send a Write Command to start the acquisition. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_START. Then wait for the acquisition time.
4. Send a Write Command to read acquired I,Q samples. Command ID to send = ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES. (It is a bug to in the program to send a Write Command to read I,Q samples)

\pagebreak  

## Nucleo
The Nucleo firmware consists of four features:
- carry out the configuration of the ATREB215 board,
- drive the IOs pins of the SKY65362 board,
- drive the IOs pins of the Antenna Switch board,
- monitoring the current of the ATREB215 and SKY65362 boards and the voltage of the SKY65362 board.

### Nucleo In/Out
The following figure shows the mains In/Out of the Nucleo Board:

![ Nucleo InOut ](img/nucleo/nucleoinout.png "Title")  

SKY65362 IOs pin driving:

Mode              Value
----------------- ----------
PA_SLEEP          0x0
PA_RECEIVE_BYPASS 0x2
PA_RECEIVE_LNA    0x6
PA_TRANSMIT       0x3 or 0x7

Mode[0] = CTX
Mode[1] = CSD
Mode[2] = CPS

ANT_SEL           Value
----------------- -----
ANT1 port enabled 0x0
ANT2 port enabled 0x1

cf. SKY65362 for more information.

Antenna Switch IOs pin driving:

RF SW V1 RF SW V2 Value
-------- -------- ----------------------
0x1      0x0      ANTENNA_SWITCH_RX_MODE
0x0      0x1      ANTENNA_SWITCH_TX_MODE

\pagebreak  

### Firmware diagram
The following figure shows the mains features implemented in Nucleo firmware:

![ Nucleo Firmware ](img/nucleo/nucleofirmware.png "Title")
- SPI Drivers: a first SPI driver carries out the communication with the Zturn. It is a slave SPI. A second SPI driver carries out the communication with both ATREB215. It is a master SPI. Two slave chip selects are available to switch the communication between each ATREB215.
- App Controller: decode commands
- SKY65362 IOs Driver: drive the IOs pins of the SKY65362 board.
- Antenna switch Driver: drive the IOs pins of the Antenna switch board.
- Current/Voltage monitoring: get the current of ATREB215 and SKY65362 boards and the voltage of the SKY65362 board every 1ms for 1s. One Nucleo ADC pins are used for each measure.

### ATREB215 and SKY65362 Power Supply Monitoring
Monitoring is a feature which acquires the value of ATREB215 boards current, SKY65362 boards current and SKY65362 board voltage every ms for the acquisition time. The maximum of the acquisition time is 1s.
1. Send a Write Command to start monitoring acquisition. Command ID to send = NUCLEO_CMD_ENABLE_MON_ACQ. The argument 'samples number' (or data[0] & data[1] & data[2] & data[3]) has to be set at 1000 at maximum.
2. Send a Read Command to read samples acquired for acquisition. Command ID to send = NUCLEO_CMD_READ_MON_ACQ. The number of sample to read has to be set at 1000 at maximum. Set this value in the 'command size in bytes' field of the frame.

cf. ./nucleo_monitoring.py for more information.

\pagebreak  

# API
A python control API is provided to:  
- Write / Read STM32 registers,
- Write / Read ATREB215 registers,
- Write / Read FPGA registers,
- Write / Read data Transfers.

## USB
USB         ID
----------- ------
Product ID  0x5151
Revision ID 0x0001

## Modules base address
Start address Size       Target
------------- ---------- ----------
0x00000000    0x0000FFFF Nucleo
0x00010000    0x0000FFFF ATREB215_0
0x00020000    0x0000FFFF ATREB215_1
0x00100000    0x000FFFFF Zynq

## Frame
### Format
The frame format is the following:  

![ Frame format ](img/frame/frame.pdf "Title")

### Read/Write command
Address field MSB bit (31) Address
-------------------------- -------------
1                          Read Command
0                          Wirte Command

### Command examples
![ Nucleo write command ](img/frame/nucleo_write_cmd_example.pdf "Title")  

![ Zynq Read command ](img/frame/zynq_write_cmd_example.pdf "Title")  

### Other features
Burst access is supported in read and write modes considering consecutive addresses. The checksum is available but not use in the source code.

\pagebreak  

## List of available commands
### Zynq
\fontsize{6}{6}\selectfont

Command Name                            ID Write                                       Read
--------------------------------------- -- ------------------------------------------- -----------------------------------------------
ZYNQ_CMD_GET_HARDWARE_VERSION           1  -                                           FPGA code version (4 bytes)
ZYNQ_CMD_GET_SOFTWARE_VERSION           2  -                                           ARM code version(4 bytes)
ZYNQ_CMD_USER_LED                       3  Turn On/Off the User Led D29                Read the value of the GPIO driving the User Led
                                           data[0] = 0x0/0x1
ZYNQ_CMD_RF_TX_START_SELF_TEST          4  Start RF transmission with                  -
                                           IQ samples stored in pattern.h
ZYNQ_CMD_RF_TX_FILL_BUFFER              5  -                                           -
ZYNQ_CMD_START_RF_TX_TRANSMISSION       6  Start RF transmission with                  -
                                           IQ samples stored in the buffer
ZYNQ_CMD_STOP_RF_TX_TRANSMISSION        7  Stop current RF transmission                -
ZYNQ_CMD_GET_IQ_FROM_FILE               8  -                                           -
ZYNQ_CMD_ADVISE_DATA_ON_FEW_FRAMES      9  -                                           -
ZYNQ_CMD_RF_TX_RESET                    10 Reset all RF TX elements:                   -
                                           -> RF_TX_FIFO                                            
                                           -> SERIALIZER                                            
                                           -> ARM_RF_TX  
ZYNQ_CMD_RF_TX_LOOP_BURST_MODE          11 Set RF TX mode as loop                      -
                                           (each sample read from FIFO is rewritten
                                           into the FIFO) or burst mode (each
                                           sample read from FIFO is sent once only),
                                           mode -> '0' = burst, '1' = loop
ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE 12 Send to Zynq, word to modulate              Read data which are stored in Zynq and ready
                                           (parameters are buffer & len(buffer))       to modulate (read parameter nb_work_4)
ZYNQ_CMD_RF_TX_START_MOUDULATION        13 Start modulation with data stored in Zynq   Read data modulated by the GWT algorithm:
                                           (parameter is modulation_debug_mode)        0 -> GWT algorithm is by pass
                                                                                       1 -> GWT algorithm enable
                                                                                       (read parameter nb_work_4)
ZYNQ_CMD_RF_TX_GET_BUFFER_INDEX         14 -                                           Read RF TX buffer index (4 bytes)
ZYNQ_CMD_GWT_CONFIGURE                  15 -                                           -
ZYNQ_CMD_GET_MODULATION_READY_FLAG      16 -                                           Read modulation_ready flag (8 bytes)
ZYNQ_CMD_RF_TX_MODULATED_DATA_LENGTH    17 -                                           Read modulated data length (4 bytes)
ZYNQ_CMD_RF_RX_ACQ_CONFIG               18 Apply RF RX acquisition configuration       Read RF RX acquisition configuration (84 bytes)
                                           (parameters are buffer & len(buffer))
ZYNQ_CMD_RF_RX_ACQ_START                19 Start rf rx acquisition                     -
ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES         20 Read rf rx acquisition                      -
                                           reliable acquisition maximum time: 250ms
                                           parameters are chr_bytes & 4bytes
ZYNQ_CMD_RF_RX_SYNC_FLAG                21 -                                           Read RF RX sync flag (flag = 1, RF RX deserializer
                                                                                       is sync with received data)(1bytes & 1bytes)
ZYNQ_CMD_GW_POWER_USER_LEDS             22 Turn On/Off GW_power User Leds              -
                                           (parameter is config_gw_power_user_leds)

- [-] = Command is read or write only

\normalsize

\pagebreak  

### Nucleo
\fontsize{6}{6}\selectfont

Command Name                     ID Write                                  Read
-------------------------------- -- -------------------------------------- --------------------------------------
NUCLEO_CMD_GET_FIRMWARE_VERSION  1  -                                      Get the version of the NUCLEO firmware
                                                                           (4 bytes)
NUCLEO_CMD_ENABLE_MON_ACQ        2  Start the monitoring with an enable    -
                                    (parameters are data & 4bytes)
NUCLEO_CMD_READ_MON_ACQ          3  -                                      Read the value of the PA consumption
                                                                           (read dots_nb*MONITORING_NB_OUTPUTx2)
NUCLEO_CMD_ANTENNA_SWITCH_CONFIG 4  Set the value of antenna switch config Read the value of antenna switch config
NUCLEO_CMD_PA0_IO                5  Set the value of PA0 config            Read the value of PA0 config
                                    (parameter is config)                  (parameter is config)
NUCLEO_CMD_PA1_IO                6  Set the value of PA1 config            Read the value of PA1 config
                                    (parameter is config)                  (parameter is config)
NUCLEO_CMD_OFDM_BUFFER           7  Send buffer of ofdm IQ to nucleo
                                    parameters are buf & 64 bytes

\normalsize

### ATREB215
Cf Register Map of ATREB215

\pagebreak  

# Driving scripts examples
## GW_POWER
- Check that all modules are connected: "./check_module_connection.py"
- Drive the GW_POWER user leds: "./control_gw_user_led.py"  
- Start a standard transmission : "./start_standart_transmission.py"
- Start a standard reception : "./start_standart_reception.py"

## ZYNQ
- Read the version of the FPGA RTL firmware: "./zynq_get_firmware_version.py"
- Turn On the User Led D29: "./zynq_turn_on_led.py"
- Turn Off the User Led D29: "./zynq_turn_off_led.py"
- Reset all RF TX elements: "./zynq_rf_tx_reset.py"
- Reset the serializer: "./zynq_rf_rx_reset_deserializer.py"
- Launch RF Transmission: "./zynq_start_rf_transmission.py"
- Stop RF Transmission: "./zynq_stop_rf_transmission.py"
- TEST -> Launch a RF RX acquisition with AT86RF215 emulator integrated in the FPGA: "./zynq_rf_rx_acquisition_self_test_with_rf_module_emulator.py"
- TEST -> Launch a transmission by enabling the loopback mode of AT86RF215 chip and then launch a RF RX acquisition: "./zynq_rf_rx_acquisition_self_test_with_loopback.py"
- Launch a RF RX acquisition: "./zynq_rf_rx_acquisition_self_test_with_rf_noise.py"

## NUCLEO
- Get the version of nucleo firmware: "./nucleo_get_firmware_version.py"
- Read the value of PA consumption: "./nucleo_monitoring.py"

## RF
- Set AT86RF215 chip of the ATREB215 board as transmitter: "./TransmitterInIQRadioMode.py"
- Set AT86RF215 chip of the ATREB215 board as receiver: "./ReceiverInIQRadioMode.py"

## PA
- Read/Set the value of PA IO: "./pa_io_access.py"

## ANTENNA SWITCH
- Set the configuration of antenna switch: "./antenna_switch_set_config.py"

\pagebreak  

#Electrical characteristique {#elecLink}

Power Supply Voltage (V) Current (mA)          Comments
------------ ----------- --------------------- ---------
VDD_ZYNQ     5.0         500mA                 DC-DC
VDD_NUCLEO   5.0         500mA                 DC-DC
VDD_AT       3.0         Deep Sleep: 30nA      LDO
                         Rx Listen [6:28mA]    
                         Rx Active [28mA]     
                         Tx: 62mA
VDD_SKY      5.0 or 4.5  Up to 600mA           LDO
                         Pout: 30.dBm => 515mA
                         Pout: 27dBm => 300mA                          
                         Pout: 24dBm => 230mA

\pagebreak  

# Tools
- Script Python : reliable with Python 2.7.5 (default, Nov  3 2014, 14:33:39)
- USB Communication : pyusb.noarch
- Monitoring : PyQtGraph ([http://www.pyqtgraph.org](http://www.pyqtgraph.org))
  - Python 2.7 and 3+
  - PyQt 4.8+ or PySide
  - NumPy
  - python-opengl bindings are required for 3D graphics

# Annexe1 - Data Path
![ Hardware Overview ](/nfs/work-crypt/board/avialletelle/workspace/greenhill_public/doc/project/user_guide/img/dataPath.png "Data Path")  
