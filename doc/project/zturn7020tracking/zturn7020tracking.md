# ZTURN 01
Location: ASYGN<br>Modifications:<br>14/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- D27 Removed

# ZTURN 03
Location: ASYGN<br>Modifications:<br>14/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- D27 Removed

# ZTURN 04
Location: ASYGN<br>Modifications:<br>14/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- USB connector unsoldered : repaired

# ZTURN 05
Location: ASYGN<br>Modifications:<br>14/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- D27 Removed
- USB connector unsoldered : repaired

# ZTURN 07
Location: ASYGN<br>Modifications:<br>19/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- USB connector unsoldered : repaired

# ZTURN 08
Location: ASYGN<br>Modifications:<br>19/01/2016
- FB5 removed
- D12 / U3 removed (USB modification)
- USB connector unsoldered : repaired
