/*
 * app_controller.cpp
 *
 *  Created on: Dec 29, 2015
 *      Author: avialletelle
 */

#include "app_controller.h"
#include "frame.h"
#include "command.h"
#include "config.h"
#include <string.h>
#include <stdio.h>
#include "ps_usb/usb_ps.h"
#include "rf/Rf_tx_interface.h"

extern XUsbPs usb_com;
extern FRAME_STRUCTURE frame;
extern Axi_spi spi_com;
extern Rf_tx_interface rf_tx_controller;

//beg debug1.part1.sect1 added for debug TX loop mode, to be deleted
int16_t buffer_csv[100000][2];
uint32_t sample_nbr = 0;
//end debug1.part1.sect1

App_controller::App_controller()
{
	this->init();

}

App_controller::~App_controller()
{

}

/*
 * @brief
 */
void App_controller::init()
{
    this->frame_init();
}

/*
 * @brief
 */
void App_controller::frame_init()
{
    frame.address = 0x0;
    frame.nbbytes = 0x0;
    frame.rd_wrb = 0x0;
    frame.index = 0x0;
    frame.full = 0x0;
    frame.to_decode = 0x0;
    memset( &frame.data[0], 0, sizeof(frame.data[0]) * MAX_FRAME_NBBYTES);
}

/*
 * @brief
 */
void App_controller::start()
{
	printf("Start application controller, waiting for user command...\r\n");
	while(1)
	{
		// decode if necessary
		if (frame.to_decode)
		{
			this->decode();
		}
	}
}


/*
 * @brief
 */
void App_controller::decode()
{
	uint32_t frame_address = (frame.address & 0x7FFFFFFF);


	if (frame_address <= NUCLEO_CMD_HIGHADDR)
	{
		// NUCLEO command
		//rd command, transfer data and null transaction to get the spi slave response
		spi_com.transfer((uint8_t*) &frame.address, (uint32_t) (frame.nbbytes + FRAME_OVERHEAD_SIZE));

	} else if ((RF_CMD_BASEADDR <= frame_address) && (frame_address <= RF_CMD_HIGHADDR))
	{
		// RF command (send to Nucleo before)
		//rd command, transfer data and null transaction to get the spi slave response
		spi_com.transfer_slow((uint8_t*) &frame.address, (uint32_t) (frame.nbbytes + FRAME_OVERHEAD_SIZE));

	} else if (frame_address > ZYNQ_CMD_HIGHADDR)
	{
		// not a ZYNQ command ...
		printf("Bad address!! (0x%x\n)", (unsigned int) frame.address);

	} else
	{
		switch (frame_address)
		{
			case ZYNQ_CMD_USER_LED:
				cmd_user_led();
				break;
			case ZYNQ_CMD_GET_HARDWARE_VERSION:
				get_hw_version();
				break;
			case ZYNQ_CMD_GET_SOFTWARE_VERSION:
				get_sw_version();
				break;
			case ZYNQ_CMD_RF_TX_START_SELF_TEST:
				rf_tx_start_self_test();
				break;
			case ZYNQ_CMD_RF_TX_FILL_BUFFER:
				rf_tx_controller.fill_fifo((int16_t*) frame.data, (uint32_t) (frame.nbbytes/4), (uint32_t) 0);
				//beg debug1.part1.sect2 added for debug TX loop mode, to be deleted
				sample_nbr = (uint32_t) (frame.nbbytes/4);
				for (uint32_t index = 0; index < (sample_nbr*2); index+=2)
				{
					buffer_csv[index][0] = *(int16_t*) &(((int16_t*) frame.data) + index)[0];
					buffer_csv[index][1] = *(int16_t*) &(((int16_t*) frame.data) + index)[1];
					//printf("csv data send to RF : index=%d I=%d Q=%d\n",
					//		(int) index, (*(int16_t*) &(samples + index)[0]), (*(int16_t*) &(samples + index)[1]));
				}
				break;
			case ZYNQ_CMD_READ_CSV_BUFFER:
				printf("In ZYNQ_CMD_READ_CSV_BUFFER\n");
				for (uint32_t index = 0; index < (sample_nbr*2); index+=2)
				{
				  printf("READ csv data send to RF : index=%d I=%d Q=%d\n",
						  (int) index, buffer_csv[index][0], buffer_csv[index][1]);
				}
				printf("End of ZYNQ_CMD_READ_CSV_BUFFER\n");
				break;
				//end debug1.part1.sect2 to be deleted
			case ZYNQ_CMD_START_RF_TX_TRANSMISSION:
				rf_tx_start_transmission();
				break;
			case ZYNQ_CMD_STOP_RF_TX_TRANSMISSION:
				rf_tx_stop_transmission();
				break;
			case ZYNQ_CMD_RF_TX_RESET:
				rf_tx_reset();
				break;
			case ZYNQ_CMD_RF_TX_LOOP_BURST_MODE:
				rf_tx_loop_burst_mode(frame.data[0]);
				break;
			case ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE:
				prepare_data_to_modulate((int8_t*) frame.data, frame.nbbytes);
				break;
			case ZYNQ_CMD_RF_TX_START_MODULATION:
				start_modulation();
				break;
			case ZYNQ_CMD_RF_TX_MODULATED_DATA_LENGTH:
				get_modulated_data_length();
				break;
			case ZYNQ_CMD_FROM_PYTHON:
			    get_set_cmd_from_python();
			    break;
			case ZYNQ_CMD_RF_TX_GET_BUFFER_INDEX:
				rf_tx_get_buffer_index();
				break;
			case ZYNQ_CMD_GWT_CONFIGURE:
				gwt_configure();
				break;
				//gwt_configure((int32_t*) frame.data, frame.nbbytes);
			case ZYNQ_CMD_GET_MODULATION_READY_FLAG:
				get_modulation_ready_flag();
				break;
			case ZYNQ_CMD_EN_CHECK_MODULATION_DATA:
				rf_tx_controller.set_check_data_send((bool) 1);
				break;
			case ZYNQ_CMD_RF_RX_ACQ_CONFIG:
				rf_rx_acq_config();
				break;
			case ZYNQ_CMD_RF_RX_ACQ_START:
				rf_rx_acq_start();
				break;
			case ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES:
				rf_rx_acq_read_samples();
				break;
			case ZYNQ_CMD_RF_RX_SYNC_FLAG:
				rf_rx_sync_flag();
				break;
			case ZYNQ_CMD_RF_RX_RESET:
				rf_rx_reset(frame.data[0]);
				break;
			case ZYNQ_CMD_RF_RX_FORCE_EMULATOR:
				rf_rx_force_emulator(frame.data[0]);
				break;
			case ZYNQ_CMD_GW_POWER_USER_LEDS:
				drive_gw_power_user_leds();
				break;
			default:
				// todo
				break;
		}
	}
	// send response back if it is a read frame
	if (frame.rd_wrb)
	{
		this->send_rd_cmd_response();

	}

	// command decoding ending, reset frame
	this->frame_init();
}

/*
 * @brief This function send data that user wants to read by sending the current read command
 */
void App_controller::send_rd_cmd_response()
{


	uint32_t cmd_size = FRAME_OVERHEAD_SIZE + frame.nbbytes;
	uint8_t buffer[cmd_size];

	memcpy((uint8_t*) buffer, ((uint8_t*)&frame.address), sizeof(uint8_t) * (FRAME_HEADER_SIZE + frame.nbbytes));
	*(buffer + FRAME_HEADER_SIZE + frame.nbbytes) = FRAME_DEFAULT_CHECKSUM;
	*(buffer + FRAME_HEADER_SIZE + frame.nbbytes + FRAME_FOOTER_CHECKSUM_SIZE) = FRAME_STOP_BYTE;
	XUsbPs_EpBufferSend(&usb_com, ZYNQ_BULK_ENDPOINT, buffer, cmd_size);
}
