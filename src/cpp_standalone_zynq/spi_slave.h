/*
 * spi.hpp
 *
 *  Created on: Oct 5, 2015
 *      Author: cleblanc
 */

#ifndef SPI_HPP_
#define SPI_HPP_

#include "xparameters.h"
#include "axi_registers.h"
#include "frame.h"

/*
 * @brief
 */
class spi_slave
{
public:
	spi_slave();
	~spi_slave();
	void wait_for_data(unsigned nbbytes, FRAME_STRUCTURE *frame);
	void get_data(FRAME_STRUCTURE *frame);
	void tx_fifo_write(uint8_t data);


protected:
	void reset_tx_fifo(void);
	void reset_rx_fifo(void);
	void rx_fifo_read(void);
};

#endif /* SPI_HPP_ */
