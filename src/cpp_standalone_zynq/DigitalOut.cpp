/*
 * \file DigitalOut.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: avialletelle
 */

#include "DigitalOut.h"

/*
 * @brief
 */
DigitalOut::DigitalOut(uint32_t gpio_id) : Ps_gpio(gpio_id)
{
	this->set_as_output();
}

/*
 * @brief
 */
DigitalOut::~DigitalOut()
{

}

