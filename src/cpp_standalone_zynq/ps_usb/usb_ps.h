/*
 * xusbps.h
 *
 *  Created on: Nov 4, 2015
 *      Author: avialletelle
 */

#ifndef XUSBPS_H_
#define XUSBPS_H_

/*
 * Includes
 */
#include "xusbps.h"			/* USB controller driver */
#include "xscugic.h"

/*
 * Defines
 */
#define ZYNQ_CONTROL_ENDPOINT		0x0
#define ZYNQ_BULK_ENDPOINT			0x1

/*
 * Prototypes
 */
int usb_ps_start(XUsbPs *UsbInstancePtr, u16 UsbDeviceId, XScuGic *IntcInstancePtr);
void usb_irq_handler(void *CallBackRef, u32 Mask);
void usb_ps_control_ep_event_handler(void *CallBackRef, u8 EpNum, u8 EventType, void *Data);
void usb_ps_bulk_ep_event_handler(void *CallBackRef, u8 EpNum, u8 EventType, void *Data);
int UsbSetupIntrSystem(XScuGic *IntcInstancePtr, XUsbPs *UsbInstancePtr, u16 UsbIntrId);
void UsbDisableIntrSystem(XScuGic *IntcInstancePtr, u16 UsbIntrId);


#endif /* XUSBPS_H_ */
