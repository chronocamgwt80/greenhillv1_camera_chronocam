/**
 * \file main.cpp Top file of the Greenhill project. It contains the main process.
 * Copyright (c) ASYGN S.A.S 2015
 */

/*
 * Includes
 */
#include "app_controller.h"
#include "platform/Interrupt.h"
#include "ps_usb/usb_ps.h"
#include "Axi_spi.h"
#include "axi_ext_spi.h"
#include "rf/Rf_tx_interface.h"
#include "frame.h"
#include "platform/platform.h"
#include "rf/gwt_modulator.h"
#include "DigitalOut.h"
#include <stdio.h>
#include "bram_controller/bram_controller.h"
#include "bram_controller/cdma.h"

/*
 * Global variables
 */
App_controller greenhill_controller;
XScuGic interrupt_controller;
XUsbPs usb_com;
Axi_spi spi_com(is_master, CONFIG_SPI_BASEADDR, ZERO_PHASE, ZERO_POLARITY);
Rf_tx_interface rf_tx_controller;
gwt_modulator modulator;
FRAME_STRUCTURE frame;
DigitalOut led(USER_LED);
uint32_t hw_version = AXI_REG_FIRMWARE_VERSION;
uint32_t sw_version = ARM_FIRMWARE_VERSION;
/*
 * @brief Main
 */
#include "sleep.h"
int main()
{
	// Chronocam tests: uncomment line below
	Axi_ext_spi ext_spi_com(is_master, ONE_PHASE, ONE_POLARITY);
	Axi_spi spi_com(is_master, CONFIG_SPI_BASEADDR, ZERO_PHASE, ZERO_POLARITY);
	 uint32_t wr_data = 0x0;
	 uint32_t wr_data1 = 0x0;
	 uint32_t wr_data2= 0x0;
	 uint32_t rd_data2= 0x0;


	uint8_t cmdinit[] = {0xFF, 0xFF, 0xFF, 0x0D, 0x00, 0x00, 0x00, 0x01};
    uint32_t cmd32_1 = 0x00460000;
    uint32_t cmd32_2 = 0x0;
    uint64_t rd_data = 0x0;


	while(1)
	{
		// Chronocam tests: uncomment line below

//				 ecriture dans 0x0E
//				wr_data = 0x000E0009;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture de 0x0E
//				wr_data = 0x004E0000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 0, 0x%x \n", (unsigned)rd_data);

//               ecriture dans 0x10
//				wr_data = 0x0010FFFF;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture de 0x10
//				wr_data = 0x00500000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 2, 0x%x \n", (unsigned)rd_data);


				// ecriture dans 0x12
//				wr_data = 0x00120061;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture dans 0x12
//				wr_data = 0x00520000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 4, 0x%x \n", (unsigned)rd_data);


				// ecriture dans 0x18
//				wr_data = 0x00180001;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture de 0x18
//				wr_data = 0x00580000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 8, 0x%x \n", (unsigned)rd_data);


				//ecriture dans 0x00
//				wr_data = 0x00000081;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture de 0x00
//				wr_data = 0x0040000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 8, 0x%x \n", (unsigned)rd_data);



				// ecriture dans 0xA0
//				wr_data = 0x00A00040;
//				ext_spi_com.ext_spi_write(wr_data);
				// lecture de 0xA0
//				wr_data = 0x04A00000;
//				rd_data = ext_spi_com.ext_spi_read(wr_data);
//				printf("Results, addr: 8, 0x%x \n", (unsigned)rd_data);



		 uint64_t cmd = 0xFFFFFF0D00000000;
		 wr_data1 = (cmd >> 32) & 0xFFFFFFFF;
		 wr_data2 = cmd & 0xFFFFFFFF;

		rd_data = ext_spi_com.ext_spi_read(wr_data1);
		rd_data2 = ext_spi_com.ext_spi_read(wr_data2);
		printf("Results, addr: 0, 0x%llx \n", (unsigned) rd_data | rd_data2);
//
//	     			  cmd32_1 = (uint32_t)cmdinit[0] << 24 |
//	     			            (uint32_t)cmdinit[1] << 16 |
//	     			            (uint32_t)cmdinit[2] << 8  |
//	     			            (uint32_t)cmdinit[3];
//
//	     			  cmd32_2 = (uint32_t)cmdinit[4] << 24 |
//	     			            (uint32_t)cmdinit[5] << 16 |
//	     			            (uint32_t)cmdinit[6] << 8  |
//	     			            (uint32_t)cmdinit[7];


	}

	printf("Starting the embedded firmware\r\n");
	disable_caches(); // required when we need high performance (eg. cdma use)
    interrupt_init(INTERRUPT_CONTROLLER_ID, &interrupt_controller);
    usb_ps_start(&usb_com, USB_COM_ID, &interrupt_controller);
    bram_controller_init(&interrupt_controller);
    cdma_init(&interrupt_controller);
    greenhill_controller.start();

	// never reached, but just in case, let the µC in a known state
	while(1);
}
