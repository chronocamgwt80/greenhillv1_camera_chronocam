//#define TEST_PN
#define LENGTH_DEL_LINE 20 // NB_OF_SYMBOLS in delline for accu

// coeff for data tones in mapper
#define COEFF_TONE 1.0

// offset for Tx
#define OFF_RE 0.1
#define OFF_IM -0.1

// threshold for confidence in repetition mode 
#define CONF_THRESH 0.6

#define DEB_DMOD 1
#define SKIP_MOD 1
#define NB_CONST ((int32_t)(exp2((float)NBITSCONST[constell])))
// skip the "noisy" portion at beginning of rx phase
#define FIRST_SAMPLES_SKIPPED 0

//#define CC_CODE_ON 

// to set if waveform TX requested (float and csv are exclusive)
#define DUMP_FLOAT_TX 0
#define READ_FLOAT 0
#define DUMP_CSV 1
#define READ_CSV 1
//#define INTERP 
//#define GEN_PREAMBLE

// spectrum is centered on 0
#define CENTER_SPECTRUM_0 1
// number of null tones forced on each side of DC (DC-ZTONE_RADIUS DC+ZTONE_RADIUS) are set to 0
#define MAX_POW_VAL 80

// substract input offset
#define SUBSTR_OFF 1

// margin for start of symbol decision
#define EPS_DIST 10

// interpolation des facteurs de correction des tones
#define INTERP_CORR_FACTORS 1
#define FORCE_SIDE_INFO_RX 1  

#define FIND_FIRST_PEAK_ONLY 1
#define DETECT_PEAK 1

#define GEN_FIXED_POINT 1
#define MEASURE_RX_POWER 1
#define TX_GAIN (1.0/5.0)
#define MIN_INPUT_POWER (1.0)

#define FORCE_SYMBOL_DETECTOR 1
#define TRY_WAVE 0

//#define GREEN_MOD 1
#define SCALE (0.8) // scaling factor for rand 
#define RANDOM_START 0 // length of the first symbol
#define PI 3.14159
// flag for source selection (always repeat first symbol data)
//#define GEN_RANDOM_BITSTREAM 1
#define READ_INPUT_FROM_FILE 0
#define READ_PN_FROM_FILE 0

// OFDM symbols parameters
//#define NB_OF_SYMBOLS 40 // nb of symbols to modulate to generate the modulated file
//#define LAST_SYMBS ((int32_t)floor(0.1*NB_OF_SYMBOLS))

#define BIG_INT 0x7ffffff
#define OVSMPL 0

#define NFFT 256 // default FFT size
#define NCP 64 // default NCP size
#define NB_TONES_OFF 120 // default off tones at side of symbol
#define MSUB_SECT 4    // default  Number of Sections
#define FS 1.024e3 // AFE sampling freq (used to compute bit rate)
#define TS 1.0/FS // AFE sampling freq (used to compute bit rate)

// default constellation
#define CONSTELL QPSKC

#define      N_MIN 2
#define      N_MAX 512 // max Number of tones
#define      M_MAX 16    // max Number of Sections
//Green-OFDM M=4 / 2048 tones
#define      NCP_MIN 0 // max cyclic prefix
#define      NCP_MAX (N_MAX/2) // max cyclic prefix
#define NB_TONES_OFF_MIN 0 // min off tones at side of symbol
#define NB_TONES_OFF_MAX 150 // max off tones at side of symbol
#define ZTONE_RAD_L 0
#define ZTONE_RAD_R 0
//#define ZTONE_RAD_L 2
//#define ZTONE_RAD_R 1
//#define   N 2048 
//#define NCP 256 
//#define NB_TONES_OFF 512
//#define ZTONE_RAD_L 7
//#define ZTONE_RAD_R 6

#define SNRDB 80
#define snr (pow(10,(-SNRDB/10.0)))
// Generation of Green-OFDM Sections sq(n')

#define      N1 (N_MAX/4)
#define      N2 (N_MAX/2)
#define      N3 (3*N_MAX/4)

// parameters affecting the "weak signal mode" (2 1 1)
//#define PILOT_SPACING 3
//#define SET_CONTIG_PILOTS 1
//#define READ_SAME_BITLOAD 1

#define PILOT_SPACING 5
#define SET_CONTIG_PILOTS 0
#define READ_SAME_BITLOAD 0

#define SIZEOF_BYTE 8
#define NACTIVE_TONES (N_MAX-NB_TONES_OFF_MIN)
//#define NPILOTS NACTIVE_TONES/5 // pilots
#define NPILOTS ((NACTIVE_TONES/PILOT_SPACING)+1) // pilots
#define FFT_PILOT 256
// tones carryoing data: all active except pilots and DC (no pilot on DC)
#define NDATA_TONES_MAX (NACTIVE_TONES)
//#define NDATA_TONES (NACTIVE_TONES-NPILOTS-CENTER_SPECTRUM_0*(1+ZTONE_RAD_L+ZTONE_RAD_R))
#define NBITS_PER_TONE_MAX 4 // 16QAM
#define NB_BITS_IN_SYMBOL_MAX ((NDATA_TONES_MAX)*(NBITS_PER_TONE_MAX))
#define NB_CHAR_IN_SYMBOL_MAX ((NB_BITS_IN_SYMBOL_MAX>>3)+1)
#define RAD_SEARCH_PEAK 20
#define PEAK_DETECTOR_FACT 0.95
#define DEPTH_MAX_AVERAGE 5
#define FACTOR_COARSE_SYNC 1.2 // threshold for coarse sync detection
#define POW_BUFF_SIZE (N_MAX)
// struct to store all peaks detected in the current buffer
#define MAX_NB_PEAK 10 
#define AVERAGE_PEAK_POSITION 0
// nb of records for power measurement : 16Msmples with N+NCP symbol length
//#define MAX_POW_INDX ((int32_t)floor(((float)(1<<24)/(float)(N_MAX+NCP)) )+1)
#define MAX_HISTO_BINS 40

// negative offset for start f symbol, to prevent interference between i/i+1 if start index estimated by coarse sync is below start of CP
#define MINBACKOFF -40
#define MAXBACKOFF N_MAX-1  
#define DEF_BACKOFF_INDX 4
#define SEARCH_START 1
//#define FFT_NORM (1.0/sqrt((float)N*M))

// variance of the in signal to be considered as noise
#define DEF_NOISE_LEVEL 6
// min ratio between in signal and DEF_NOISE_LEVEL to consider there is signal 
#define MIN_SNR 10
// define the min peak value to consider the signal can be an OFDM symbol
// ==> must be related to the average signal value at receiver (signal to noise)
#define MIN_PEAK_VAL 300000.0

//
#define NORM_QAM16 (1.0/sqrt(10.0))
#define NORM_QPSK (1.0/sqrt(2.0))
#define NORM_BPSK (1.0)

// fixed point format at TX input and Rx output
//#define FIXED_POINT_FRAC 11
// bladerf is 12bits ATMEL is 13bits
#define FIXED_POINT_FRAC 12
#define FIXED_POINT_INT 1

// new params for grenn++
#define L_PNSEQ 16
#define NB_SIGNAL_TONES 32
#define FIRST_SIGNAL_POS 1 // tone number after null sidetones
//#define SPACE_BW_SIGNAL_TONES ((int32_t)floor((float)(N_MAX-NB_TONES_OFF-2*FIRST_SIGNAL_POS)/(float)(NB_SIGNAL_TONES-1)))

// parameters for  papr histogram
#define MIN_PAPR 2
#define MAX_PAPR 17
#define NB_BINS_HISTO_PAPR 100
#define BIN_WIDTH ((float)(MAX_PAPR-MIN_PAPR)/(float)NB_BINS_HISTO_PAPR) 

#define RATIO_BITS_ERR 0.1

// flag enable convolutive coding
#define ENC_EN 0

//check in the .c for the conditional declaration
#ifdef INTERP
#define OVSMPL_FACTOR 4
#define NMAX 1024
//int FIR_ORDER;
//int indx_fir=0;
//float coeff[NMAX],delline1[NMAX],delline2[NMAX];
#include "../FIR/fir_func.c"                         
#endif

#ifdef GEN_PREAMBLE
#include "../PREAMBLE/preamble.c"
#define OVSMP 4
#endif
