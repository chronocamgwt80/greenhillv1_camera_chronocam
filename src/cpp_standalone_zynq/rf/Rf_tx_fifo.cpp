/**
 * \file Rf_tx_fifo.cpp Supplies methods to write I/Q samples into FIFO
 * Copyright (c) ASYGN S.A.S 2015
 */

#include "Rf_tx_fifo.h"
#include "../axi_registers.h"

/*
 * @brief Constructor
 */
Rf_tx_fifo::Rf_tx_fifo()
{

}

/*
 * @brief Destructor
 */
Rf_tx_fifo::~Rf_tx_fifo()
{

}

/*
 * @brief append a new IQ sample into FIFO
 */
void Rf_tx_fifo::write_sample_iq(unsigned short i, unsigned short q)
{
	AXI_REG_RF_TX_FIFO_CTRL = ((i << AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_din_i_OFFSET) & AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_din_i_MASK) \
			 | ((q << AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_din_q_OFFSET) & AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_din_q_MASK) \
			 | ((RF_TX_FIFO_WRITE_EN << AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_write_en_OFFSET) & AXI_REG_RF_TX_FIFO_CTRL_rf_tx_fifo_write_en_MASK);
}

/*
 * @brief return 0x1 if the FIFO is full
 */
uint32_t Rf_tx_fifo::is_full()
{
	return ((AXI_REG_RF_TX_FIFO_STATUS & AXI_REG_RF_TX_FIFO_STATUS_rf_tx_fifo_full_flag_MASK) >> AXI_REG_RF_TX_FIFO_STATUS_rf_tx_fifo_full_flag_OFFSET);
}

/*
 * @brief return 0x1 if the FIFO is empty
 */
uint32_t Rf_tx_fifo::is_empty()
{
	return ((AXI_REG_RF_TX_FIFO_STATUS & AXI_REG_RF_TX_FIFO_STATUS_rf_tx_fifo_empty_flag_MASK) >> AXI_REG_RF_TX_FIFO_STATUS_rf_tx_fifo_empty_flag_OFFSET);
}
