/*
 * \file gwt_modulator.h GWT algorithm to modulate data
 * Copyright (c) ASYGN S.A.S 2015
 */

#ifndef GWT_MODULATOR_H_
#define GWT_MODULATOR_H_

/*
 * includes
 */
#include "stdint.h"
#include "../config.h"
#include "gw_mod_gossyc.h"

/*
 * Defines
 */
#define GWT_MOD_INPUT_TO_OUTPUT_LEN_FACTOR		100

/*
 * @brief
 */
class gwt_modulator
{
public:
	gwt_modulator();
	virtual ~gwt_modulator();
	void modulation(int8_t modulation_debug_mode, int32_t* data, uint32_t len);
	void configure(uint32_t* data, uint32_t len);

	int16_t modulated_data[RF_TX_BUFFER_WR_MAX_DEPTH][2];
	uint32_t modulated_data_len;
	uint32_t previous_modulated_data_len;
	int32_t modulation_en;
	//int32_t end_modulation_flag;
	/*struct complex_f {
		float	r;
		float	i;
	};*/
	struct complex_f *out_mod;
	uint32_t modulation_ready;
	uint32_t mode_python;
	//buffer to read from DDR the output of the modulation
	int32_t res_mod[RF_TX_BUFFER_WR_MAX_DEPTH];
};

#endif /* GWT_MODULATOR_H_ */
