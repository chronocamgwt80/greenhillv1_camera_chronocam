#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>

//uncomment TEST and for debug of the circular buffer
//use with the main in the .c
//#define TEST

#ifdef TEST
  #define SIZE_BITSTREAM_BUF 7
#else
  #define SIZE_BITSTREAM_BUF 500 * 1024
#endif

enum { CIRC_ERR_OVERFLOW = -1, CIRC_ERR_TOOMUCH = -2 };

struct circ
{
  uint8_t circ_buf[SIZE_BITSTREAM_BUF];
  uint8_t tail;
  uint8_t head;
  uint32_t count;
};

/*
* give the current number of byte free in the buffer
*/
uint32_t available_circ(struct circ *pt_cbuf);

/*
* copy a buffer in the circular buffer if enough space other else return ERR_OVERFLOW
*/
int32_t push_circ(struct circ *pt_cbuf, uint8_t *buf, uint32_t len);

/*
* copy to a buffer n data from the circular buffer
* if request more data that in the buffer return ERR_TOOMUCH 
*/
int32_t pop_circ(struct circ *pt_cbuf, uint8_t * buf, uint32_t len);

/*
*reset and initialize the circular buffer. Mandatory before using it
*/
void reset_circ(struct circ *pt_cbuf);

/*
* print all the data in the buffer. used for the debug
*/
void print_circ(struct circ *pt_cbuf);

#endif // CIRCULAR_BUFFER_H
