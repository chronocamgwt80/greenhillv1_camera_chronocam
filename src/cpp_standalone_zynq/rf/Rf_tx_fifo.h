/**
 * \file Rf_tx_fifo.h Supplies methods to write I/Q samples into FIFO
 * Copyright (c) ASYGN S.A.S 2015
 */

#ifndef RF_TX_FIFO_H_
#define RF_TX_FIFO_H_

/*
 * Includes
 */
#include "stdint.h"

enum
{
	RF_TX_FIFO_WRITE_EN = 0x1,
	RF_TX_FIFO_IS_EMPTY = 0x1,
	RF_TX_FIFO_IS_NOT_EMPTY = 0x0,
	RF_TX_FIFO_IS_FULL = 0x1,
	RF_TX_FIFO_IS_NOT_FULL = 0x0
};

/*
 * @brief Object to control FIFO writes access
 */
class Rf_tx_fifo
{
public:
	Rf_tx_fifo();
	virtual ~Rf_tx_fifo();
	void write_sample_iq(unsigned short i, unsigned short q);
	uint32_t is_full();
	uint32_t is_empty();


};

#endif /* RF_TX_FIFO_H_ */
