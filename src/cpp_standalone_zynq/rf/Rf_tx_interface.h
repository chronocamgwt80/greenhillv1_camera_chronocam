/*
 * \file Rf_tx_interface.h Methods to control RF TX interface
 * Copyright (c) ASYGN S.A.S 2015
 */

#ifndef RF_TX_INTERFACE_H_
#define RF_TX_INTERFACE_H_

/*
 * Includes
 */
#include "Rf_tx_fifo.h"
#include "../config.h"
#include "gw_mod_gossyc.h"
/*
 * defines
 */


enum
{
	i_bit_0_start = 0x1,
	i_bit_0_stop = 0x0,
	q_bit_0_set_low = 0x0,
	q_bit_0_set_high = 0x1,
	RF_TX_INTERFACE_IS_ENABLE = 0x1,
	RF_TX_INTERFACE_IS_NOT_ENABLE = 0x0,
	TRANSMIT_DONE = 0x1,
	TRANSMIT_PROCESSING = 0x0
};


/*
 * @brief
 */
class Rf_tx_interface
{
public:
	Rf_tx_interface();
	virtual ~Rf_tx_interface();
	void reset();
	void start_transmit();
	void stop_transmit();
	void fill_fifo(int16_t* samples, uint32_t sample_nb, uint32_t zero_samples_n);
	void fill_fifo_GWT(complex_f * mod, uint32_t sample_nb, uint32_t zero_samples_nb);
	uint32_t is_enable();
	uint32_t transmit_status();
	void self_test();
	void loop_burst_mode(uint8_t mode);
	uint32_t buffer_is_full();
	uint32_t buffer_is_empty();
	uint32_t get_data_count();
	bool get_check_data_send();
	void set_check_data_send(bool en);
	int32_t frame_to_modulate[RF_TX_DATA_FRAME_MAX_NBBYTES];
	uint32_t frame_to_modulate_len;
	uint32_t buffer_index;
	uint32_t buffer_full_counter;
	uint32_t transmit_enable;


protected:
	Rf_tx_fifo rf_tx_fifo;
	uint32_t i_bit_0;
	uint32_t q_bit_0;
	void reset_data_frame();
	bool check_data_send;
};

#endif /* RF_TX_INTERFACE_H_ */
