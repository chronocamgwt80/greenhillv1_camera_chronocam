/*
 * fft.c
 * 
 * Copyright 2015 gulfomoj <gulfomoj@gulfomoj-X550JK>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef FFT_GWT
  #define FFT_GWT
  #define FFT_FORWARD -1
  #define FFT_BACKWARD 1

  #include <complex.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <assert.h>
  #include <math.h>

struct complex_f{
	float r;
	float i;
	};

  /*
   * @brief FFT code for an n-points input *in. This function writes in *out the result of a fft or IFFT
     @param sign: if sign = -1 performs a FFT, either performs an IFFT. It comes from the sign of the DFT equation
     @param norm: if norm = 1, it does a sqrt(n) normalisation on the result of the FFT/IFFT
   * */
  void fft_code(int32_t n, struct complex_f *in, struct complex_f *out, int32_t sign, int32_t norm);

#endif //FFT_GWT
