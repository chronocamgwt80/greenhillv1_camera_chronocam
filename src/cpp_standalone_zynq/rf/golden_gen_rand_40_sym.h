#include <stdint.h>

#ifndef GOLDEN_GEN_RAND_40_SYM
  #define GOLDEN_GEN_RAND_40_SYM

  #define NB_GOLDEN_BYTES 3428 * 6 //3428 lines of 48 bits

  extern uint8_t golden_gen_bitstream[];
 
#endif //eof GOLDEN_GEN_RAND_40_SYM
