//
//
// green plus V1
//  started 20/06/2015
//
//to compile

//gcc -g -Wall fft.c gw_mod_gossyc.c ../VITERBI/viterbi.c ../VITERBI/tab.c golden_gen_rand_40_sym.c -lm -o modem_plus; ll modem_plus
///gcc -g -Wall //with the most relevant warnings
//  gcc -fsanitize=address -fno-omit-frame-pointer -g gw_mod_gossyc.c fft.c golden_FIR_bitstream_fbso_out_320x4.c -lm -o modem_plus // to find some buffer overflow
// gcc -fsanitize=address -fno-omit-frame-pointer -Wunreachable-code -Wconversion -Wswitch-enum -Wswitch-default -Wcast-qual -Waggregate-return -Wwrite-strings -Wstrict-overflow=5 -Wstrict-prototypes -Wcast-align -Wpointer-arith -Wshadow -Wundef -Wfloat-equal -Wextra -Wall -g gw_mod_gossyc.c fft.c golden_FIR_bitstream_fbso_out_320x4.c -lm -o modem_plus //to help debug
//Green-OFDM M=4 / 2048 tones
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

//comment the define for the embedded version
//#define LINUX 1
#ifdef LINUX
  #define PC 1
#endif
//uncomment the next lines to run on a PC with embed version
/*
#ifndef PC
  #define PC 1
//FAKE_EMBED to simulate the parameters configuration in the zynq
  #define FAKE_EMBED
#endif
*/
#ifndef PC
  #include "../config.h"
#endif

#include "gw_mod_gossyc.h"
//#include "../VITERBI/viterbi.c"

//#include "../VITERBI/metrics.c"

typedef enum {UNASSIGNED_TONE=0, SIG_TONE=1, NULL_TONE=2, DATA_TONE=3, PILOT_TONE=4} tone_type;

struct peak_info
{
  int32_t index;
  int32_t smpl_indx;
  float val_max;
  float val_phase;
};

struct peak_info list_of_peaks[MAX_NB_PEAK];

const struct complex_f BPSK[2] = {{-1,0}, {1, 0}};

// Gray mapping
const struct complex_f QPSK[4] = {{-1, 1}, {1, 1}, {-1, -1}, {1, -1}};
//const struct complex_f QPSK[4] = {{-1,0},{0,1},{0,-1},{1,0}};

const struct complex_f pilot[2] = {{1, 1},{-1, -1}};


/*
const struct complex_f QAM16[16] = {{-3,3},{-3,1},{-3,-3},{-3,-1},
			      {-1,3},{-1,1},{-1,-3},{-1,-1},
			      {3,3},{3,1},{3,-3},{3,-1},
			      {1,3},{1,1},{1,-3},{1,-1}};
*/
// Gray mapping
const struct complex_f QAM16[16] = {{-3,-3}, {-3,-1}, {-3,3}, {-3,1},
				  {-1,-3}, {-1,-1}, {-1,3}, {-1,1},
				  {3,-3}, {3,-1}, {3,3}, {3,1}, 
				  {1,-3}, {1,-1}, {1,3}, {1,1}};

// list of available constellation
typedef enum  {BPSKC=0, QPSKC=1, QAM16C=2} lconstell;

// available modes
typedef enum {DEFAULT=-1,CONV_OFDM=0, GREEN_GREEN=1, GREEN_CONV=2} mode_ofdm;

// contains nb of bits for each constellation
int8_t NBITSCONST[4];
#ifdef LINUX
FILE *dump_in_mod,*dump_pilots,*dumpphisum;
#endif
int32_t sample_nb;
int32_t indx_max_average, indx_avpow;
int32_t last_index_value;
int32_t symb_to_process;
int32_t partial_buffer;
int32_t index_peak;
int32_t nb_smples_in_buff;
float *phi_sum;
float *gm_sum;
float av_pow,av_max_value;
float av_max[DEPTH_MAX_AVERAGE],powinst[POW_BUFF_SIZE];
float sigma_autocorr_ref[2];
float min_input_power_v;
float fft_norm_v;
int32_t nb_bits_in_symbol, nb_char_in_symbol, nb_bits_in_symbol_max;
struct complex_f pilot_seq[NPILOTS];

// dependant on input parameters
int32_t nsub_sect,nfft, ncp, constel, null_tones, nb_sigtones, ndata_tones;
int32_t SPACE_BW_SIGNAL_TONES;
int32_t nfft1, nfft2, nfft3;
int32_t n1;
int32_t rad_search_peak_v;
int8_t detect_weaksig;
int32_t backoff_v;
float tx_gain_v;
mode_ofdm mode_v;
int8_t skip_mod_v,force_symbol_detect_v;

int32_t nsub_sect_sig=1;

int8_t symb_struct[N_MAX*M_MAX];
int8_t pn_seq[M_MAX*N_MAX];
int8_t pn_seq2[M_MAX*N_MAX];

// viterbi parameters
int32_t mettab[2][256];	/* Metric table, [sent sym][rx symbol] */
uint32_t metric;

#ifdef INTERP_FIR
int FIR_ORDER;
int indx_fir=0;
float coeff[NMAX],delline1[NMAX],delline2[NMAX];
#endif

void shift_spectrum(struct complex_f *insig, int32_t nb, float k);
#ifdef LINUX
void plot(FILE *dumpfile, int32_t Npoint, struct complex_f *data);
void plot_qam(FILE *dumpfile, int32_t Npoint, struct complex_f *data);
#endif
void calc_mean_variance(struct complex_f *indat, int32_t ndat, float *offr, float *offi, float *var);

void dbg_p_input_buf(uint32_t line, struct complex_f *p_input_buf)
{
 int32_t dbg_lp;
#ifndef EMBED_PERF
 printf("LINE=%d\n", (int) line);
#endif
 for(dbg_lp=0; dbg_lp < (nfft + ncp) * NB_OF_SYMBOLS; dbg_lp++)
 {
#ifndef EMBED_PERF
   printf("out mod, p_input_buf[%d].r=%f p_input_buf[%d].i=%f\n", (int) dbg_lp, p_input_buf[dbg_lp].r, (int) dbg_lp, p_input_buf[dbg_lp].i);
#endif
 }
}

void print_complex(struct complex_f * pt, int32_t count, const int8_t *str, int32_t line)
{
  int32_t count_dmod;
#ifndef EMBED_PERF
  printf("from Line %d\n", (int) line);
#endif
  for(count_dmod=0; count_dmod < count; count_dmod++)
  {
#ifndef EMBED_PERF
    printf("\t%s %d: %f %f\n", (int8_t*) str,
            (int) count_dmod, pt[count_dmod].r, pt[count_dmod].i);
#endif
  }
}

void puncture(uint8_t *o1,uint8_t *o2, uint8_t *i) {
  /*
    X0 X1 X2 X3 X4 X5

puncturing 802.15.4g
rate 3/4
A0 A1 a2 A3 A4 a5
B0 b1 B2 B3 b4 B5

data sent : A0 B0 A1 B2 A3 B3 A4 B5


  */

  int8_t ii;

  ii=0;

  ii|=*o1&0x1;
  ii|=(*o2&0x1)<<1;
  ii|=(*o1&(0x1<<1))<<1;
  ii|=(*o2&(0x1<<2))<<1;
  ii|=(*o1&(0x1<<3))<<1;
  ii|=(*o2&(0x1<<3))<<2;
  ii|=(*o1&(0x1<<4))<<2;
  ii|=(*o2&(0x1<<5))<<2;

  *i=ii;

} // puncture

void depuncture(uint8_t *o1, uint8_t *i) {
  /*
    X0 X1 X2 X3 X4 X5

puncturing 802.15.4g
rate 3/4, data sent to viterbi
A0 A1 a2 A3 A4 a5
B0 b1 B2 B3 b4 B5

data received : A0 B0 A1 B2 A3 B3 A4 B5
  */
  *o1=i[0];
  *(o1+2)=i[2];
  *(o1+4)=OFF_CLIP;
  *(o1+6)=i[4];
  *(o1+8)=OFF_CLIP;
  *(o1+10)=i[6];

  *(o1+1)=i[1];
  *(o1+3)=OFF_CLIP;
  *(o1+5)=i[3];
  *(o1+7)=i[5];
  *(o1+9)=OFF_CLIP;
  *(o1+11)=i[7];

  //*o1=o1i;
  //*o2=o2i;  

} // depuncture

void build_symbol_struct(mode_ofdm modeofdm) {

  int32_t nfft_sig=nfft/OVSMPL_FACTOR;
  int32_t i, j;
  int32_t next_sig_tone, fill_center=0;;

  next_sig_tone=nsub_sect_sig*FIRST_SIGNAL_POS+nsub_sect_sig*(null_tones>>1);

#ifndef EMBED_PERF
  printf("buid symb structure\n");
  printf("nb sig tones %d, space bw signal tones %d first sig tone %d nullt %d\n", (int) nb_sigtones, (int) SPACE_BW_SIGNAL_TONES, (int) next_sig_tone, (int) null_tones);
#endif
  // 0-> null tone, 1-> data , 2-> pilot 3-> signalling

  for (i=0;i<nfft_sig*nsub_sect_sig;i++) symb_struct[i]=UNASSIGNED_TONE;

  // describe one subsymbol k%M then copy the config for interleaving: result is one M*nfft symbol containing M symbols of nfft tones

  for (i=0;i<(null_tones>>1)*nsub_sect_sig;i+=nsub_sect_sig) symb_struct[i]=NULL_TONE;


  for (i=nsub_sect_sig*nfft_sig-((null_tones+1)>>1)*nsub_sect_sig;i<nfft_sig*nsub_sect_sig;i+=nsub_sect_sig) symb_struct[i]=NULL_TONE;

  //allocate tones
  for(i=nsub_sect_sig*(null_tones>>1),j=0;i<nsub_sect_sig*nfft_sig-nsub_sect_sig*((null_tones+1)>>1);i+=nsub_sect_sig,j++) {
 
    assert(symb_struct[i]==UNASSIGNED_TONE);
#ifndef EMBED_PERF
    printf("==> indx %d j %d mode %d\n", (int) i, (int) j, (int) CONV_OFDM);
    printf("==> indx %d j %d mode %d\n", (int) i, (int) j, (int) CONV_OFDM);
#endif

    if ( ((modeofdm!=CONV_OFDM)) && ((i>=next_sig_tone) && (j%PILOT_SPACING) && !(CENTER_SPECTRUM_0 && (i>=nsub_sect_sig*(nfft_sig/2-ZTONE_RAD_L))&&(i<=nsub_sect_sig*(nfft_sig/2+ZTONE_RAD_R))))) { 
#ifndef EMBED_PERF
      printf("==> insert sig tone %d\n", (int) i);
#endif
      symb_struct[i]=SIG_TONE;
      next_sig_tone+=SPACE_BW_SIGNAL_TONES*nsub_sect_sig;
    }
    else if ( (j%PILOT_SPACING) && !(CENTER_SPECTRUM_0 && (i>=nsub_sect_sig*(nfft_sig/2-ZTONE_RAD_L))&&(i<=nsub_sect_sig*(nfft_sig/2+ZTONE_RAD_R)))) { 
    // map data on this tone
      symb_struct[i]=DATA_TONE;
    } 
    else if(CENTER_SPECTRUM_0 && (i>=nsub_sect_sig*(nfft_sig/2-ZTONE_RAD_L)) && (i<=nsub_sect_sig*(nfft_sig/2+ZTONE_RAD_R))) {
      // map null tone at DC
      symb_struct[i]=NULL_TONE; 
      // detect if a nulltone is on a pilot: in this case, run the filling procedure: 
      if(!(j%PILOT_SPACING)) fill_center=1;
    }
    else{ 
      symb_struct[i]=PILOT_TONE;
    }
  }

#ifndef EMBED_PERF
  for(i=nsub_sect_sig*(null_tones>>1);i<nsub_sect_sig*nfft_sig-nsub_sect_sig*((null_tones+1)>>1);i+=nsub_sect_sig) {
    printf(">%d %d\n", (int) i, (int) symb_struct[i]);
  }
#endif

  // create a consistent set of null tones around null tone center of spectrum:
  // fill the space between 2 pilots with null tones
  int32_t last_is_zero=1,there_is_one_sigtone=0,nb_center_null=0;

  if (fill_center) {
    for(i=nsub_sect_sig*(nfft_sig/2);i<nsub_sect_sig*nfft_sig-nsub_sect_sig*((null_tones+1)>>1);i+=nsub_sect_sig) {
      assert(i<nsub_sect_sig*nfft_sig);
      if (last_is_zero && symb_struct[i]==DATA_TONE) {
	symb_struct[i]=NULL_TONE;nb_center_null++;
      }
      if (last_is_zero && symb_struct[i]==SIG_TONE) {
      symb_struct[i]=NULL_TONE;
      there_is_one_sigtone=1;
      }
      if (last_is_zero && symb_struct[i]==PILOT_TONE) {
	last_is_zero=0;
#ifndef EMBED_PERF
	if (there_is_one_sigtone && symb_struct[i+nsub_sect_sig]!=DATA_TONE) {
	  fprintf(stderr,"cant implement center null tones: sig tone unaffected");exit(0);
	}
#endif
	if (there_is_one_sigtone && symb_struct[i+nsub_sect_sig]==DATA_TONE) {
	  symb_struct[i+nsub_sect_sig]=SIG_TONE;
	}
	break;
      }
    }
#ifndef EMBED_PERF
    printf("you\n");
#endif
    last_is_zero=1,there_is_one_sigtone=0;
    for(i=nsub_sect_sig*(nfft_sig/2);i>=0;i-=nsub_sect_sig) {
      assert(i>0);
      if (last_is_zero && symb_struct[i]==DATA_TONE) {
	symb_struct[i]=NULL_TONE;
	nb_center_null++;
      }
      if (last_is_zero && symb_struct[i]==SIG_TONE) {
	symb_struct[i]=NULL_TONE;
	there_is_one_sigtone=1;
      }
#ifndef EMBED_PERF
      printf("i %d %d %d %d\n", (int) i, (int) symb_struct[i], (int) symb_struct[i-1], (int) there_is_one_sigtone);
#endif
      if (last_is_zero && symb_struct[i]==PILOT_TONE) {
	last_is_zero=0;
#ifndef EMBED_PERF
	if (there_is_one_sigtone && symb_struct[i-nsub_sect_sig]!=DATA_TONE) {
	  fprintf(stderr,"cant implement center null tones: sig tone unaffected");exit(0);
	}
#endif
	if (there_is_one_sigtone && symb_struct[i-nsub_sect_sig]==DATA_TONE) {
	symb_struct[i-nsub_sect_sig]=SIG_TONE;
	}
	break;
      }
    }
  }

  // create contiguous sets of pilot tones on the spectrum edges (left and right)
  if (SET_CONTIG_PILOTS) {
    there_is_one_sigtone=0;
    i=((null_tones)>>1)*nsub_sect_sig+nsub_sect_sig;
    while(symb_struct[i]==DATA_TONE||symb_struct[i]==SIG_TONE) {
      if (symb_struct[i]==SIG_TONE) there_is_one_sigtone=1;
      symb_struct[i]=PILOT_TONE;
      i+=nsub_sect_sig;
    }

    if (there_is_one_sigtone) {
      if (symb_struct[i+nsub_sect_sig]==DATA_TONE) symb_struct[i+nsub_sect_sig]=SIG_TONE;
#ifndef EMBED_PERF
      else {fprintf(stderr,"impossible to move sig_tone during contiguous pilot generation(left)\n");exit(0);}
#endif
    }
#ifndef EMBED_PERF
    fprintf(stderr,"added pilot at left : %d\n",(int) (i-((null_tones)>>1)*nsub_sect_sig));
    printf("nul tones %d null tone+1>>2 %d\n", (int) null_tones, (int) ((null_tones+1)>>1));
#endif
    there_is_one_sigtone=0;
    i=nsub_sect_sig*nfft_sig-((null_tones+1)>>1)*nsub_sect_sig-2*nsub_sect_sig;
    while(symb_struct[i]==DATA_TONE||symb_struct[i]==SIG_TONE) {
      if (symb_struct[i]==SIG_TONE) there_is_one_sigtone=1;
      symb_struct[i]=PILOT_TONE;
      i-=nsub_sect_sig;
    }

    if (there_is_one_sigtone) {
      if (symb_struct[i-nsub_sect_sig]==DATA_TONE) symb_struct[i-nsub_sect_sig]=SIG_TONE;
#ifndef EMBED_PERF
      else {fprintf(stderr,"impossible to move sig_tone during contiguous pilot generation(right)\n");exit(0);}
#endif
    }
#ifndef EMBED_PERF
    fprintf(stderr,"added pilot at right : %d\n", (int) nsub_sect*nfft-((null_tones+1)>>1)*nsub_sect-nsub_sect-i);
#endif
#ifndef EMBED_PERF
    fprintf(stderr,"added pilot at right : %d\n", (int) nsub_sect_sig*nfft_sig-((null_tones+1)>>1)*nsub_sect_sig-nsub_sect_sig-i);
#endif
  }
#ifndef EMBED_PERF
  fprintf(stderr,"added null tone around center %d\n", (int) nb_center_null);
#endif
  // check sig tones are inserted 
  int8_t nbsigtones=0;
  if (modeofdm!=CONV_OFDM) {
    for (i=0;i<nfft_sig*nsub_sect_sig;i+=nsub_sect_sig) {

      if (symb_struct[i]==SIG_TONE) nbsigtones++;
    }
#ifndef EMBED_PERF
    if (nbsigtones!=nb_sigtones) fprintf(stderr,"cant insert %d sigtones\n", (int) nb_sigtones);
#endif
    assert(nbsigtones==nb_sigtones);
  }

  // complete with null tones when ovsmpling
  for (i=nfft_sig;i<nfft;i++) symb_struct[i]=NULL_TONE;


  // interleave
  if (1) for (i=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig)
    for (j=1;j<nsub_sect_sig;j++) {
      symb_struct[i+j]=symb_struct[i];
    }

#ifndef EMBED_PERF
  for (j=0;j<nfft;j++){
    printf("%d\t", (int) j);
    for(i=0;i<nsub_sect_sig;i++)
      printf("%d ", (int) symb_struct[j*nsub_sect_sig+i]);
    printf("\n");
  }
  for (j=0;j<nfft*nsub_sect_sig;j++){
      printf("%d ", (int) symb_struct[j]);
      if (j&&!(j%50)) printf("\n");
  }
#endif
#ifndef EMBED_PERF
  printf("\n");
#endif
}// end of build_symbol_struct

void init_autocorr_ref(int32_t mode_v)
{
  // for ref[0]=sigma we get  ref[1]=autocorr energy
  // must be done for each CP  value and constellation

  if (1||(nfft==256))
  {
    if (constel==QPSKC)
    {
      if (mode_v)
      {
	sigma_autocorr_ref[0]=0.714115;
	sigma_autocorr_ref[1]=15.191148;
      } else {
	sigma_autocorr_ref[0]=0.718070;
	sigma_autocorr_ref[1]=22.484635;
      }
    }
    
    if (constel==QAM16C)
    {
      if (mode_v)
      {
	sigma_autocorr_ref[0]=0.710920;
	sigma_autocorr_ref[1]=21.134869;
      } else {
	sigma_autocorr_ref[0]=0.709313;
	sigma_autocorr_ref[1]=26.317043;
      }
    }
  }
#ifndef EMBED_PERF
  else 
    printf("init autocorr ref not supported for this config of nfft/n");
#endif
} // end of init_autocorr_ref


void norm(struct complex_f *in, float factor){
  int32_t i;

  for (i=0;i<nfft+ncp;i++) {
    in[i].r=factor*in[i].r;
    in[i].i=factor*in[i].i;
  }

}
void norm_v(struct complex_f *in, float factor, int32_t nb){
  int32_t i;

  for (i=0;i<nb;i++) {
    in[i].r=factor*in[i].r;
    in[i].i=factor*in[i].i;
  }

}
// compute a/b (a and b complex)
struct complex_f cpx_div(struct complex_f a, struct complex_f b) {

  struct complex_f c;
  float sqmod;

  sqmod= b.r*b.r+b.i*b.i;
  c.r=(a.r*b.r+a.i*b.i)/sqmod;
  c.i=(-a.r*b.i+a.i*b.r)/sqmod;
  return c;

}// end of cpx_div

// compute a*b (a and b complex)
struct complex_f cpx_mul(struct complex_f a, struct complex_f b) {

  struct complex_f c;

  c.r=(a.r*b.r-a.i*b.i);
  c.i=(a.r*b.i+a.i*b.r);
  return c;

}// end of cpx_mul

float modcpx(struct complex_f a){

  return sqrt(a.r*a.r+a.i*a.i);

}// modcpx

void get_pilot(struct complex_f *pilots, struct complex_f *tones) {

  int32_t i,j;
  for(i=(null_tones>>1),j=0;i<nfft-(null_tones>>1);i+=PILOT_SPACING,j++) {
    assert(j<NPILOTS);
    pilots[j]=tones[i];
  }

}// end of get_pilot

//
float get_delta(struct complex_f *pilots1, struct complex_f *pilots2){

  int32_t i;
  struct complex_f pdp;
  float delta,av_delta=0.0;

  // for pilots 
  for(i=0;i<NPILOTS;i++) {
    pdp=cpx_div(pilots2[i],pilots1[i]);
    delta=acos(pdp.r/modcpx(pdp));
    av_delta+=delta;
#ifndef EMBED_PERF
    if (0) printf("%d =>p1=%f %f | p2=%f %f | pdp= %f %f |delta=%f\n", (int) i,pilots1[i].r,pilots1[i].i,pilots2[i].r,pilots2[i].i,pdp.r,pdp.i,delta);
#endif
  }
  return av_delta/(float)NPILOTS;
}// get_delta

void demod_tones(struct complex_f *tones, struct complex_f *correc)
{
  int32_t i,j,k,togpil=0,trnd;
  struct complex_f  pilot_ref[N_MAX],pilot_rec[N_MAX];
  float mod,tones_r,tones_i;

  k=0;
  for(i=(null_tones>>1),j=0;i<nfft-(null_tones>>1);i++,j++)
  {
    if (j%PILOT_SPACING) { // not a pilot
      //k++;tog=(tog+1)%2;
      pilot_ref[i].r=0.0;
      pilot_ref[i].r=0.0;
      pilot_rec[i].r=0.0;
      pilot_rec[i].r=0.0;
    } else {
      togpil++;
      togpil=togpil%8;
      //      printf("j=%d npil=%d\n", (int) j, (int) NPILOTS);
      if (k>=NPILOTS) break;
      trnd=(togpil>=4)?0:1;
      pilot_ref[i]=pilot[trnd];
      pilot_rec[i]=tones[i];
      k++;
    }
  }

  // compute correction factor  for channel compensation and coarse timing estimation error
  // invese of (a+jb is (a-jb)/(a2+b2)
  if (0) 
    for(i=(null_tones>>1);i<nfft-(null_tones>>1);i++) {
    mod=pilot_rec[i].r*pilot_rec[i].r+pilot_rec[i].i*pilot_rec[i].i;
    correc[i].r=(pilot_rec[i].r*pilot_ref[i].r+pilot_rec[i].i*pilot_ref[i].i)/mod;
    correc[i].i=(pilot_rec[i].r*pilot_ref[i].i-pilot_rec[i].i*pilot_ref[i].r)/mod;
  }

  // interpolate correction factor
  if (0)
    for(i=(null_tones>>1);i<nfft-(null_tones>>1);i+=PILOT_SPACING) {
      for(k=1;k<5;k++) {
	correc[i+k].r=((float) k/5.0) *correc[i].r+((float) (5-k)/5.0) * correc[i+5].r;
	correc[i+k].i=((float) k/5.0) *correc[i].i+((float) (5-k)/5.0) * correc[i+5].i;
      }
    }

  if (0) 
    for(i=(null_tones>>1);i<nfft-(null_tones>>1);i++) {
      tones_r=tones[i].r*correc[i].r-tones[i].i*correc[i].i;
      tones_i=tones[i].r*correc[i].i+tones[i].i*correc[i].r;
      tones[i].r=tones_r;
      tones[i].i=tones_i;
    }
#ifndef EMBED_PERF
  if (0) {
  printf("=========== DUMP CORRECTED TONES\n");
  for(i=0;i<nfft;i++)
    printf("tones %d  %f %f\n", (int) i, tones[i].r,tones[i].i);
  }
#endif
} // end of demod_tones

// compute approx llr for   qam16 according to HPL-2001 paper
// apply for I and Q
void llr_approx_16qam(float y, float *di1, float *di2)
{
  if (abs(y)<=2) *di1=y;
  else if (y>2) *di1=2.0*(y-1);
  else if (y<-2) *di1=2.0*(y+1);

  *di2=-abs(y)+2;
} //end of llr_approx_16qam 

// compute approx llr for qpsk according to HPL-2001 paper
// apply for I and Q
void llr_approx_qpsk(float y, float *di1, float *di2)
{

  *di1=y;

} //end of llr_approx_16qam 
uint8_t clip(float x)
{
#ifndef EMBED_PERF
  printf("%f \n",x);
#endif
  if (x>1) return 0x7f+OFF_CLIP;
  if (x<-1) return (uint8_t) (0x80+OFF_CLIP);
  return (int32_t) floor(x*128)+OFF_CLIP;
}

void soft_demap_tone(struct complex_f tone,lconstell constel,float *dist,uint8_t* soft_bits)
{
  //int8_t nb;
  float di1,di2;

  // scale soft values to the integer values

  //nb=(1<<NBITSCONST[constel]);
  if (constel==BPSKC) {
    soft_bits[0]=clip(tone.r*1.0/NORM_BPSK);
  } 
  else if (constel==QPSKC) {
    soft_bits[0]=clip(tone.r*1.0/NORM_QPSK);
#ifndef EMBED_PERF
    printf("sft_dmap 0: %f %d\n",tone.r, (int) soft_bits[0]);
#endif
    soft_bits[1]=clip(-tone.i*1.0/NORM_QPSK);
#ifndef EMBED_PERF
    printf("sft_dmap 1: %f %d\n",tone.i, (int) soft_bits[1]);
#endif
  }
  else if (constel==QAM16C) {
    llr_approx_16qam(tone.r,&di1,&di2);
    soft_bits[0]=clip(di1*1.0/NORM_QAM16);
    soft_bits[1]=clip(di2*1.0/NORM_QAM16);
    llr_approx_16qam(tone.i,&di1,&di2);
    soft_bits[2]=clip(di1*1.0/NORM_QAM16);
    soft_bits[3]=clip(di2*1.0/NORM_QAM16);
  }

}// end of soft_demap_tone

int8_t demap_tone(struct complex_f tone, lconstell constel, float *dist)
{
  uint8_t i,imin;
  struct complex_f diff;
  float distmin=(float) BIG_INT,distc;
  int8_t nb;

  nb=(1<<NBITSCONST[constel]);
  
   for(i=0;i<nb;i++) {
     if (constel==QAM16C){ 
       diff.r=tone.r-QAM16[i].r;    
       diff.i=tone.i-QAM16[i].i;
     }  
     if (constel==QPSKC){ 
       diff.r=tone.r-QPSK[i].r;    
       diff.i=tone.i-QPSK[i].i;
     }  
     if (constel==BPSKC){ 
       diff.r=tone.r-BPSK[i].r;    
       diff.i=tone.i-BPSK[i].i;
     }  
      distc=modcpx(diff);
#ifndef EMBED_PERF
      if (0) printf("%d %f\n", (int) i,distc); 
#endif
      if (distc<distmin) {distmin=distc;imin=i;}
    }

   *dist=distmin;

  return (int8_t) imin;

}// end of demap_tone

void compare_char(uint8_t c1, uint8_t c2, uint32_t *nbdiff)
{

  int32_t j;
  int8_t one=0x1;
  int32_t ndiff_i;

  ndiff_i=*nbdiff;

  for (j=0;j<8;j++) {
    if ((c1&(one<<j)) != (c2&(one<<j))) ndiff_i++;
#ifndef EMBED_PERF
    if (0) printf("%d %d %d %d\n", (int) j, (int) c1&(one<<j), (int) c2&(one<<j), (int) ndiff_i);
#endif
  }
  *nbdiff=ndiff_i;

}// end of compare_char

void compare_bs(uint8_t *bs1,uint8_t *bs2,uint32_t nbits,uint32_t *ndiff)
{
  int32_t i;
  uint8_t b1,b2;
  int32_t nchar;

  nchar=nbits>>3;

  for(i=0;i<nchar;i++)
  {
    b1=bs1[i];
    b2=bs2[i];
#ifndef EMBED_PERF
    if (1)printf("i %d: %x %x\n", (int) i, b1, b2);
#endif
    compare_char(b1,b2,ndiff);
  }

}// end of compare_bs

// only tones forming the first subset i%M=0 are extracted and processed
float bit_extract(struct complex_f *tones,uint8_t *bitstream, uint32_t *nbit, int8_t *last,int32_t *indxbs,lconstell constel,int8_t* symb_struct,int8_t *pn_seq, mode_ofdm mode)
{
  int32_t i,j,k,l;
  uint8_t bits,lastbits; 
  int32_t nbit_i,lastpos;
  int32_t nbits_per_tone;
  float dist,totdist=0.0;
  int32_t mm;

  mm=1;
  if (*indxbs==0) bitstream[0]=0;
  nbits_per_tone=NBITSCONST[constel];
  lastpos=*last;
  nbit_i=0;
#ifndef EMBED_PERF
  if (1)printf("enter bit_extract: last=%d nbit=%d\n", (int) lastpos, (int) *nbit);
#endif

  // extract the first subset i%nsub_sect=0
  for(i=0,j=0,k=0,l=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,k+=mm)
  {
    if (symb_struct[i]==DATA_TONE) { // 
 
      bits=demap_tone(tones[k],constel,&dist);
      totdist+=dist;

      bitstream[l]|=((bits<<(lastpos))&0xff);
#ifndef EMBED_PERF
      if (0) printf("i %d bits %x l=%d  bs=%x\n", (int) k, bits, (int) l, bitstream[l]);
#endif
      lastpos+=nbits_per_tone;

      if (lastpos>=SIZEOF_BYTE) {
	lastpos-=SIZEOF_BYTE;
	lastbits=bits>>(nbits_per_tone -lastpos);
#ifndef EMBED_PERF
	if (1) printf("\t l %d lastbits %x lastpos %d bs %x nbiti=%d\n", (int) l, lastbits, (int) lastpos, bitstream[l], (int) nbit_i);
#endif
	l++;
	bitstream[l]=0;
	bitstream[l]|=lastbits;	
      } 
      nbit_i+=nbits_per_tone;
    }
    j++;
  }
#ifndef EMBED_PERF
  printf("demapped bits (%d)\n", (int) nbit_i);
  for(i=0;i<=l;i++) printf(" %x",bitstream[i]);printf("\n");
#endif

  *nbit=*nbit+nbit_i;
  *last=lastpos;
  *indxbs=l;

  return totdist;

}// end of bit_extract

// soft demap of demodulated sybol
// nb_soft_bits => remaining soft bits from last symbol shifted at the begining of current symbol
void soft_bit_extract(struct complex_f *tones,uint8_t*soft_bits,uint32_t *nbit,lconstell constel,int8_t* symb_struct,  mode_ofdm mode, uint32_t *nb_soft_bits)
{
  int32_t i,j,k;
  int32_t nbit_i;
#ifndef EMBED_PERF
  int32_t lastpos;
#endif
  int32_t nbits_per_tone;
  int32_t mm;
  float dist;

  //  const struct complex pi_4={ cos(PI/4.0),sin(PI/4.0)};
  mm=1;

  nbits_per_tone=NBITSCONST[constel];
  nbit_i=0;
#ifndef EMBED_PERF
  if (0)printf("enter bit_extract: last=%d nbit=%d\n", (int) lastpos, (int) *nbit);
#endif
  // extract the first subset i%nsub_sect=0
  for(i=0,j=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,k+=mm) {
    
    if (symb_struct[i]==DATA_TONE) { // 
      //if (mode!=CONV_OFDM)  tones[k]=cpx_mul(tones[k],pi_4);
      soft_demap_tone(tones[k],constel,&dist,soft_bits+nbit_i);
      // when byte is ready => decode
      nbit_i+=nbits_per_tone;
    }
    j++;
  }
#ifndef EMBED_PERF
  printf("dump soft tones\n");
  for(i=0;i<nbit_i-nbits_per_tone;i++) printf("%d ",(soft_bits[i]>=OFF_CLIP)?1:0); printf("\n");
#endif

  *nbit=*nbit+nbit_i;

}// end of soft_bit_extract

// puncture and decode the soft bits
void soft_decode(uint8_t *soft_bits,uint8_t *bitstream, int32_t *nbits_left, int32_t nbits, int32_t *last, int32_t *indx_bs)
{
  int32_t i,j,l,nbits_tot;
  //uint8_t nbits_left_i;
  int32_t nbit_i;
  uint8_t decdata[8];
  uint8_t opunct[16];
  uint32_t nbits_decode;
  uint8_t bit;

   // pointer on the first data bit in the bitstream
  nbit_i=0;
  //nbits_left_i=*nbits_left;
#ifndef EMBED_PERF
  if (0)printf("enter bit_extract: last=%d nbit=%d\n", (int) *last, (int) nbits);
#endif
  // extract the first subset i%nsub_sect=0
  //nbits_to_go=  (nbits+*nbits_left)%nbits_decode;
 
  if (ENC_EN) 
    for (i=0,l=0;i<nbits+*nbits_left;i+=nbits_decode) {
      // consumes 8 bits and produces 6 bits (code rate 3/4)
      opunct[0]=soft_bits[0];
      opunct[1]=soft_bits[1];
      opunct[2]=soft_bits[2];
      opunct[3]=OFF_CLIP;
      opunct[4]=OFF_CLIP;
      opunct[5]=soft_bits[3];
      opunct[6]=soft_bits[4];
      opunct[7]=soft_bits[5];
      opunct[8]=OFF_CLIP;
      opunct[9]=OFF_CLIP;
      opunct[10]=soft_bits[6];
      opunct[11]=soft_bits[7];
      
     viterbi(&metric,decdata,opunct,6,mettab);

    } 
  else { // soft bits => hard  decision: dump all symbol bits in the bitstream

    nbits_tot=0;
    j=*last;
    l=0;
    // copy all bits
    while(nbits_tot<nbits)  {
      bit=((soft_bits[nbits_tot]>OFF_CLIP)?1:0)<<(j);
#ifndef EMBED_PERF
      printf("%d %d\n", (int) j, (int) soft_bits[nbits_tot]);
#endif
      bitstream[l]|=bit;
      j++;
      nbits_tot++;
      if (j>=SIZEOF_BYTE) {
	j=0;
#ifndef EMBED_PERF
	if (1) printf("\t l %d lastpos %d bs %x nbiti=%d indx_bs %d\n", (int) l, (int) *last,bitstream[l], (int) nbit_i, (int) *indx_bs);
#endif
	l++;
	bitstream[l]=0;
      }
    }
    *last=j;
    *indx_bs=l;
  }
}// end of soft_decode

int32_t count_tone(int8_t *symb_struct,tone_type tone_type_id)
{
  int32_t i,k;

  for(i=0,k=0;i<nfft*nsub_sect_sig;i++)
    if (symb_struct[i]==tone_type_id ) k++;

  return k/nsub_sect_sig;   

}// end of count_tone

// pilot sequence for receiver
void build_pilot_seq(int8_t *symb_struct,mode_ofdm mode,int8_t *pn_seq)
{
  int32_t i,npil;

  // construct pilots
  npil=0;
  for(i=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig)
    if (symb_struct[i]==PILOT_TONE) {
	pilot_seq[npil].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
	pilot_seq[npil].i = NORM_QPSK*pilot[0].i*pn_seq[npil];
	npil++;
    }

}// end of build_pilot_seq

float dump_tones(struct complex_f *pilot_rec,
		  struct complex_f *ref,
		  int32_t bckoff,
		  struct complex_f *correc,
		  int8_t flag_compute_corr,
		  uint32_t *diffnb,
		  int8_t flag_plot,
		  //lconstell constel,
		  float *maxd,
		  //		  int8_t *symb_struct,
		  mode_ofdm mode,
		  //int nb_data_tone,
		  //		  int8_t *pn_seq1,
		  //		  int8_t *pn_seq2,
		  int32_t *shift_est,
		  int8_t *vote
		  ) {
  struct complex_f sig_tone[NB_SIGNAL_TONES];
  int32_t i,j=0,nbiterr,k;
#ifndef EMBED_PERF
  int32_t ii; 
#endif
  struct complex_f pilot_corr;
  int8_t bits,bitsref;
  uint32_t ndiff,indx_sig=0;
  float dist,totdist=0.0,maxdist=0.0;
  int32_t mm,tone_nb;
  //int num_section;
  struct complex_f pn_seq_comb;
  //struct complex_f *fft_pil;
  //fftw_plan p;
  int32_t num_section1,num_section2;
  int32_t pil_nb=0,num_sigtone_mapped=0;
  int32_t numsect1_ref,numsect2_ref;
  int32_t npil=0;
  float theta_est,dtheta_est;
  // store the pilots angles
  float *theta1, av_dtheta=0.0, dtheta,last_theta,last_dtheta_est;
  int32_t *index_pil;
  float *shift_stored;
  typedef enum {LEFT,RIGHT} quad_pos;
  quad_pos  quad,lastquad;
  int32_t shift,shiftc;
  float inc;
#ifndef EMBED_PERF
  printf("FLAG_CORR=%d\n", (int) flag_compute_corr);
#endif
  
  mm=1;

  pil_nb=count_tone(symb_struct,PILOT_TONE);

  /*
  for(i=0,k=0;i<nfft*nsub_sect;i+=nsub_sect) {
#ifndef EMBED_PERF
    printf("%d", (int) symb_struct[i]);
#endif
    if (symb_struct[i]==PILOT_TONE) {
      pilot[k]=pilot_rec[k++];
    }
  }
#ifndef EMBED_PERF
  printf("\n");b
#endif
  */

#ifndef EMBED_PERF
  if (1) for(i=0,ii=0;i<nfft*mm;i++,ii+=nsub_sect_sig)printf("!!!!tones %s %d  %f %f | %f %f\n",(symb_struct[i]==SIG_TONE)?"sig":"", (int) i, pilot_rec[i].r, pilot_rec[i].i,ref[i].r,ref[i].i);
#endif

  //estimate the receiver offset with side pilots
  
  shift=*shift_est;

  if (SET_CONTIG_PILOTS)
  {
    index_pil = (int32_t *) malloc(pil_nb*sizeof(int32_t)  );
    shift_stored = (float *) malloc(pil_nb*sizeof(float)  );
    theta1 = (float *) malloc(pil_nb*sizeof(float)  );

    pil_nb=0;
    inc=0.0;
    last_theta=0.0;
    last_dtheta_est=0.0;
    //for (i=nfft*nsub_sect-((null_tones+1)>>1)-(PILOT_SPACING+1),k=nfft*nsub_sect-((null_tones+1)>>1)-(PILOT_SPACING+1);i<nfft*nsub_sect;i+=nsub_sect,k++) {
    for (i=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,k++) {
#ifndef EMBED_PERF
      printf("%d\n", (int) i);
#endif
      if (symb_struct[i]==PILOT_TONE){    

	index_pil[pil_nb]=k;
	theta1[pil_nb]=atan(pilot_rec[k].i/pilot_rec[k].r);
	// when in left quadrans / right quadran
	if ((pn_seq[pil_nb]*pilot_rec[k].r<0 ))
	  quad=LEFT;
	else quad=RIGHT;

	// atan is modulo pi: we need the angle modulo 2pi
	if (quad==LEFT) theta1[pil_nb]-=PI;
	//if (theta1[pil_nb]<-PI) theta1[pil_nb]+=2.0*PI;
	//while(theta1[pil_nb]<0) theta1[pil_nb]+=PI;
	
	//if (theta1[pil_nb]>PI ) theta1[pil_nb]=theta1[pil_nb]-PI;
	//if (theta1[pil_nb]<-PI) theta1[pil_nb]=theta1[pil_nb]+PI;

	// rotate in the same direction (accumulate 2PI each time the left quadran is passed

#ifndef EMBED_PERF
	printf(">>> %d(%d) %f %f --- theta=%f indx=%d last_theta %f\n", (int) i, (int) pil_nb,pn_seq[pil_nb]*pilot_rec[k].r,pn_seq[pil_nb]*pilot_rec[k].i,theta1[pil_nb],index_pil[pil_nb],last_theta);
#endif
	if (pil_nb>0 ) {
	  // when round is done, add(substr) 2*PI according to direction
	  if (pil_nb>0 && lastquad==LEFT && quad==RIGHT && last_theta<0) inc-=2*PI;
	  if (pil_nb>0 && lastquad==RIGHT && quad==LEFT && last_theta>0) inc+=2*PI;
	  // what happens ?
	  //assert(last_theta==0.0);

	  theta1[pil_nb]+=inc;
	  if (pil_nb>1) {
	    theta_est=theta1[pil_nb-1]+(float)(index_pil[pil_nb]-index_pil[pil_nb-1])*dtheta_est;
	    // compute new value estimate of dtheta and add to average
	    dtheta_est=dtheta_est+(theta1[pil_nb]-theta_est)/(float)(index_pil[pil_nb]-index_pil[pil_nb-1]);
#ifndef EMBED_PERF
	    printf("==>dtheta_est=%f\n",dtheta_est);
#endif
	  }	  
	  else {
	    // init theta estimate for second pilot (pil_nb=1)
	    theta_est=theta1[pil_nb];
	    dtheta=(theta1[pil_nb]-theta1[pil_nb-1])/(float)(index_pil[pil_nb]-index_pil[pil_nb-1]);
	    dtheta_est=dtheta;
#ifndef EMBED_PERF
	    printf("==>init dtheta(est)=%f\n",dtheta_est);
#endif
	  }

	  //if (abs(dtheta_est)>abs(dtheta_est+2.0*PI)) dtheta_est+=2.0*PI;
	  //else if (abs(dtheta_est)>abs(dtheta_est-2.0*PI)) dtheta_est-=2.0*PI;

	  // !!! PROCESS the case dtheta_est is ~ lastdtheta+2pi
	  // take the min dtheta_est-last_dtheta_est,dtheta_est-last_dtheta_est+2pi,dtheta_est-last_dtheta_est-2pi
	  if(( abs(dtheta_est-last_dtheta_est + 2.0*PI ) < abs(dtheta_est-last_dtheta_est) ) &&
	     (abs(dtheta_est-last_dtheta_est + 2.0*PI) < abs(dtheta_est-last_dtheta_est-2.0*PI)) )
	    dtheta_est +=2.0*PI;
	  else 
	    if (( abs(dtheta_est-last_dtheta_est - 2.0*PI ) < abs(dtheta_est-last_dtheta_est) ) &&
		(abs(dtheta_est-last_dtheta_est - 2.0*PI) < abs(dtheta_est-last_dtheta_est+2.0*PI)) )
	      dtheta_est -=2.0*PI;
#ifndef EMBED_PERF
	  printf("==>dtheta_est1=%f\n",dtheta_est);
#endif

	  last_theta=dtheta_est;
	 
	  av_dtheta +=dtheta_est;
	  dtheta_est=av_dtheta/(float) (pil_nb);
	  last_dtheta_est=dtheta_est;
	  // theta=2*k*pi/n
	  shiftc=(int32_t)floor(nfft*dtheta_est/(2.0*PI)+0.5);
	  shift_stored[pil_nb]=dtheta_est;
#ifndef EMBED_PERF
	  printf("\tdtheta(est)=%f shift(prev)=%d shift(curr)=%d\n",dtheta_est, (int) shift, (int) shiftc);
	  //printf("av_dtheta(est)=%f\n",dtheta_est);
#endif
	}
#ifndef EMBED_PERF
	if (pil_nb>0) {	  
	  printf("## dtheta=%f theta_est %f theta_measured %f\n",dtheta_est,theta_est,theta1[pil_nb]);
	  printf("delta=%f\n",theta_est-theta1[pil_nb]);
	}
#endif
	lastquad=quad;
	pil_nb++;
      }
      if ((pil_nb>PILOT_SPACING)) break;    
    }
    av_dtheta=av_dtheta/(float) (pil_nb-1);

    free(theta1);
    free(index_pil);
    free(shift_stored);
  }

  struct complex_f exp_cpx;

  // using average dthata
  
 *shift_est=(int)floor(nfft*av_dtheta/(2.0*PI)+0.5);
#ifndef EMBED_PERF
 //for(i=0;i<pil_nb;i++) printf("### %d dtheta=%f\n",i,shift_stored[i]);
#endif
  
#ifndef EMBED_PERF
  printf("final theta estimate %f corresponding step %d\n",av_dtheta, (int) *shift_est);
#endif

  // performs correction by avtheta on all tones : avtheta is angle correction by tone (0 by def)
  for (i=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,k++) {
    exp_cpx.r=cos((float)k*-av_dtheta);
    exp_cpx.i=sin((float)k*-av_dtheta);
#ifndef EMBED_PERF
    printf("pilot_rec[%d].r=%f pilot_rec[%d].i=%f exp_cpx.r=%f exp_cpx.i=%f\n", (int) k, pilot_rec[k].r, (int) k, pilot_rec[k].i, exp_cpx.r, exp_cpx.i);
#endif
    pilot_rec[k]=cpx_mul(pilot_rec[k],exp_cpx);
#ifndef EMBED_PERF
    printf("shifted %d: %f %f\n",(int) k,pilot_rec[k].r,pilot_rec[k].i);
#endif
  }

  // compute correction factors ref/rec : select the first subset (i%nsub_sect=0)
  pil_nb=0;
  for(i=0,j=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,j++,k+=mm) {
    if (symb_struct[i]!=NULL_TONE){
      if (flag_compute_corr) {
	if ((mode!=CONV_OFDM && symb_struct[i]==PILOT_TONE)) {
	    pn_seq_comb.r=(float)pn_seq[pil_nb];
	    pn_seq_comb.i=(float)pn_seq2[pil_nb];
#ifndef EMBED_PERF
	    printf("%d %f %f %f %f", (int) k,pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);
#endif
	    //pilot_rec[k].r*=(double)pn_seq[pil_nb];
	    //pilot_rec[k].i*=(double)pn_seq[pil_nb];
	    //pilot_rec[i]=cpx_div(pilot_rec[k],pn_seq_comb);

	    //	    pil_nb++;
	    // ref pilots are modulated by pn_seq
#ifndef EMBED_PERF
	    printf("==  %f %f\n",pilot_rec[k].r,pilot_rec[k].i);
	    printf("\trr %f %f\n",ref[k].r,ref[k].i);
#endif
	    //ref[k].r=pn_seq[pil_nb]*ref[k].r;
	    //ref[k].i=pn_seq[pil_nb]*ref[k].i;
	    pil_nb++;
	  }

	if (symb_struct[i]==PILOT_TONE){
	  correc[j]=cpx_div(pilot_seq[npil],pilot_rec[k]);
#ifndef EMBED_PERF
	  printf("pilot");
	  printf("\t%d %f %f rr %f %f cc %f %f\n", (int) j,pilot_rec[k].r,pilot_rec[k].i,pilot_seq[npil].r,pilot_seq[npil].i,correc[j].r,correc[j].i);
#endif
	  npil++;
	} 
	else { 
	  correc[j]=cpx_div(ref[k],pilot_rec[k]);
#ifndef EMBED_PERF
	  printf("\t%d %f %f rr %f %f cc %f %f\n", (int) j,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);
#endif
	}
      }

      /*      
      if (0&& (symb_struct[i]==SIG_TONE)){
#ifndef EMBED_PERF
	printf("indx sig %d %d (%f %f)\n", (int) i, (int) indx_sig,pilot_rec[k].r,pilot_rec[k].i);
#endif
	sig_tone[indx_sig++]=pilot_rec[k];
      }
      */
      if (symb_struct[i]==PILOT_TONE){
	if (!flag_compute_corr) {
	  // pilots are modulated by pn_seq at transmit => necessary to demodulate (in green and conventional)
	  //pilot_rec[k].r=pn_seq[pil_nb]*pilot_rec[k].r;
	  //pilot_rec[k].i=pn_seq[pil_nb]*pilot_rec[k].i;
	  if (mode!=CONV_OFDM) {
	    pn_seq_comb.r=(float)pn_seq[pil_nb];
	    pn_seq_comb.i=(float)pn_seq2[pil_nb];
#ifndef EMBED_PERF
	    printf("%f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[i].r,pilot_rec[i].i);
#endif
	    pilot_rec[i]=cpx_div(pilot_rec[k],pn_seq_comb);
	  }
	  correc[j]=cpx_div(ref[k],pilot_rec[k]);
	  pil_nb++;
	}
      }
    }
  }

    //plot(dump_in_mod,j,pilot);

    //plot(dump_in_mod,FFT_PILOT,(struct complex_f*)fft_pil);
    

    //plot(dump_pilots,(j-1)/5,pilot);

    // interpolate correction factors
    if (INTERP_CORR_FACTORS)
      for(i=(null_tones>>1);i<nfft-((null_tones+1)>>1);i+=PILOT_SPACING) {
	if (i+PILOT_SPACING>=nfft) break;
	for(j=1;j<PILOT_SPACING;j++) {
	  correc[i+j].r=(float)(PILOT_SPACING-j)/(float)PILOT_SPACING*correc[i].r+(float)j/(float)PILOT_SPACING*correc[i+PILOT_SPACING].r;
	  correc[i+j].i=(float)(PILOT_SPACING-j)/(float)PILOT_SPACING*correc[i].i+(float)j/(float)PILOT_SPACING*correc[i+PILOT_SPACING].i;
	}
      }

    // ZF channel correction of signal tones using pilots
    indx_sig=0;
    num_section1=0;
    num_section2=0;
    num_sigtone_mapped=0;
    if (mode!=CONV_OFDM) {
      for(i=0,j=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,j++,k+=mm) {
	if (symb_struct[i]==SIG_TONE) {

	  numsect1_ref=(int32_t)ref[k].r;
	  numsect2_ref=(int32_t)ref[k].i;
	  
	  if (num_sigtone_mapped==(nb_sigtones/2))  {num_section1=num_section2;num_section2=0;num_sigtone_mapped=0;}
	  sig_tone[indx_sig]=cpx_mul(correc[j],pilot_rec[k]);
	  // concatenate the 2bits info to buid numsection
	  num_section2|=demap_tone(sig_tone[indx_sig],QPSKC,&dist)<<(2*num_sigtone_mapped);
	  //num_section1<<=2;
	  num_sigtone_mapped++;
#ifndef EMBED_PERF
	  printf("sig indx %d %f %f cc %f %f ns %d\n", (int) indx_sig,sig_tone[indx_sig].r,sig_tone[indx_sig].i,correc[j].r,correc[j].i, (int) num_section2);
#endif
	  /*
	  // first sig tone gives "left" numsection second gives "right" numsection
	  if (indx_sig==0) num_section1=demap_tone(sig_tone[indx_sig],QPSKC,&dist);
	  if (indx_sig==1) num_section2=demap_tone(sig_tone[indx_sig],QPSKC,&dist);
	  if ((indx_sig==0) || (indx_sig==1)) printf("corrected sig tone %i %x\n",i,demap_tone(sig_tone[indx_sig],QPSKC,&dist));
	  */
	  indx_sig++;
	  
	}
      }
      // !!! force num section
      //num_section1=1;
      //num_section2=2;
    } else {
      num_section1=0;
      num_section2=0;
    }
    

#ifndef EMBED_PERF
      if(num_section1!=numsect1_ref || num_section2!=numsect2_ref)
	printf("side info decode mismatch\n"); 
#endif
      if (FORCE_SIDE_INFO_RX) {
#ifndef EMBED_PERF
	printf("!!!!!side info is extracted from ref\n");
#endif
	num_section1=numsect1_ref;
	num_section2=numsect2_ref;
      }
#ifndef EMBED_PERF
    printf("detected num section %d(%d) %d(%d)\n", (int) num_section1, (int) numsect1_ref, (int) num_section2, (int) numsect2_ref);
#endif

    // ZF correction of data tones and descrambles using PN seq indicated by num_section
    nbiterr=0;
    tone_nb=0;
    npil=0;
    for(i=0,j=0,k=0;i<nfft*nsub_sect_sig;i+=nsub_sect_sig,j++,k+=mm) {
      if (symb_struct[i]!=NULL_TONE) {

	pilot_corr=cpx_mul(correc[j],pilot_rec[k]);
	pilot_rec[k]=pilot_corr;

	// compare bits with reference 
	if(symb_struct[i]==DATA_TONE){
	  // not a pilot or the DC tone
	  if (mode!=CONV_OFDM) {
	    // for green mode: divide data by pn_seq+j*pn_seq2
	    pn_seq_comb.r=(float)pn_seq[tone_nb+ndata_tones*(num_section1)];
	    pn_seq_comb.i=(float)pn_seq2[tone_nb+ndata_tones*(num_section2)];
#ifndef EMBED_PERF
	    if (1) printf("%f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);
#endif
	    pilot_rec[k]=cpx_div(pilot_rec[k],pn_seq_comb);
#ifndef EMBED_PERF
	    if (1) printf("&& %f %f %f %f\n",pn_seq_comb.r,pn_seq_comb.i,pilot_rec[k].r,pilot_rec[k].i);	  
#endif
	  }
#ifdef LINUX
	  if (flag_plot) fprintf(dump_in_mod,"%f %F\n",pilot_rec[k].r,pilot_rec[k].i);
#endif
#ifndef EMBED_PERF
	  if (DEB_DMOD) printf("corrected %d: %f %f |r %f %f |c %f %f", (int) k,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);
#endif
	  bits=demap_tone(pilot_rec[k], (lconstell) constel,&dist);
	  //printf("@@@vote %ld\n",(long)vote);

	  totdist+=dist;
	  if(dist>maxdist) maxdist=dist;
	  bitsref=demap_tone(ref[k], (lconstell) constel,&dist);
#ifndef EMBED_PERF
	  if (0) printf("\t%d %d\n", (int) bits, (int) bitsref);
#endif
	  ndiff=0;
	  compare_char(bits,bitsref,&ndiff);
	  nbiterr+=ndiff;
#ifndef EMBED_PERF
	  if (DEB_DMOD){
	    if (ndiff) {printf("$$$$$$$$$$$$$$$$\n");}
	    else  printf("\n");
	  }
#endif
	  if (vote) {
	    vote[tone_nb]=bits;
#ifndef EMBED_PERF
	    printf("##vote %d %d\n", (int) vote[tone_nb], (int) bits);
#endif
	  }
	  tone_nb++;
	} else if (DEB_DMOD)
        { 
	  if (symb_struct[i]==PILOT_TONE)
          {
#ifndef EMBED_PERF
	    printf("pilot %d: %f %f |r %f %f |c %f %f\n", (int) k,pilot_rec[k].r,pilot_rec[k].i,pilot_seq[npil].r,pilot_seq[npil].i,correc[j].r,correc[j].i);
#endif
	    npil++;
	  }
#ifndef EMBED_PERF
	  else  printf("sigtone %d: %f %f |r %f %f |c %f %f\n", (int) k,pilot_rec[k].r,pilot_rec[k].i,ref[k].r,ref[k].i,correc[j].r,correc[j].i);
#endif
        }
      }
    }   
      
    *diffnb=nbiterr;

#ifndef EMBED_PERF
    printf("finished demod: nb error=%d\n", (int) nbiterr);
#endif
    //if (flag_plot) plot_qam(dump_in_mod,nfft*nsub_sect,pilot_rec);
    *maxd=maxdist;
    return totdist;

} // end of dump_tones

void perfect_decoder(mode_ofdm mode_v,struct complex_f *out_dmod,struct complex_f *out_mod,struct complex_f *tx_ref,struct complex_f *corr_facts,uint8_t *bitstream_c,uint8_t *bitstream_ref_c,
		  int32_t *last_c,
		  int8_t *last_ref,
		  int32_t *indxbs_c,int32_t *indxbs_ref,
		     int32_t *nbits_left,uint32_t *nbits,uint32_t *nbits_ref) {
  int32_t j;
  int8_t lastpos_symb;
  uint32_t nbits_symb;
  int32_t indxbs_symb;
  // types for fftw 
  //fftw_complex  out_fft[N_MAX];
  struct complex_f out_fft[N_MAX];
  //fftw_plan p;
  float distsymb;
  float maxd;
  int32_t time_shift;
  uint32_t ndiffnb;
  uint32_t nb_soft_bits;
  uint8_t soft_bits[N_MAX*MAX_NBIT_TONE];
  uint8_t bitstream_symb[((N_MAX*MAX_NBIT_TONE)>>3) + 1];
  int32_t nbits_i;

  nbits_i=*nbits;
  if ((mode_v==GREEN_CONV)) {
    // use out_dmod where first subsect contains received data and all others 0
    if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect_sig,1.0);
#ifndef EMBED_PERF
    for(j=0;j<nsub_sect_sig*nfft;j++) printf("rrrr%d %f %f\n", (int) j,out_dmod[j].r,out_dmod[j].i);    
    
    if (0) for(j=0;j<nfft*nsub_sect_sig;j++) printf("rrryy%d %f %f-- %f %f -- %f %f\n", (int) j,out_dmod[j].r,out_dmod[j].i,out_mod[j].r,out_mod[j].i,out_mod[j].r/out_dmod[j].r,out_mod[j].i/out_dmod[j].i);
#endif    
    
    {
      //p = fftw_plan_dft_1d(nfft, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
      fft_code(nfft, out_dmod, out_fft, FFT_FORWARD, FFT_NO_NORM);
      //fftw_execute(p);
      
      // read reference
      //fread(tx_ref,sizeof(struct complex_f),nfft,ref_tx_file);
    }
    
    distsymb=dump_tones(out_fft,tx_ref,1,corr_facts,1,&ndiffnb,1,&maxd,mode_v,&time_shift,NULL);
#ifndef EMBED_PERF
    printf("distance on symbol(GREEN) %f\n",distsymb);
#endif    
    
    // the ref_file contains the pnseq modulated data: so no need to demodulate the received data by the pnseq
    
    nbits_symb=0;
    lastpos_symb=0;
    indxbs_symb=0;
    
    bit_extract(out_fft,bitstream_symb,&nbits_symb,&lastpos_symb,&indxbs_symb,constel,symb_struct,pn_seq,mode_v);

    nbits_symb=0;
    soft_bit_extract(out_fft,soft_bits,&nbits_symb,constel,symb_struct,mode_v,&nb_soft_bits);
    
    soft_decode(soft_bits,bitstream_c,nbits_left,nbits_symb,last_c,indxbs_c);
    nbits_i+=nbits_symb;
    //soft_bit_extract((struct complex_f *)out_fft,bitstream_c+=indxbs_c,&nbits,&last_c,&indxbs_c,constel,symb_struct,mode_v,&nb_soft_bits);
    
    bit_extract(tx_ref,bitstream_ref_c,nbits_ref,last_ref,indxbs_ref,constel,symb_struct,pn_seq,mode_v);
    //calc_correction_coeffs((struct complex_f *)out_fft, learn_symbol,corr_facts);
	//get_pilot(pilots_ref,(struct complex_f *)out_fft);
    
  }  // GREEN demodulator
  else {
    
    if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft,1.0);
#ifndef EMBED_PERF
    for(j=0;j<nsub_sect;j++) printf("rrrr%d %f %f\n", (int) j,out_dmod[j].r,out_dmod[j].i);
#endif
    
    //p = fftw_plan_dft_1d(nfft, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    fft_code(nfft, out_dmod, out_fft, FFT_FORWARD, FFT_NO_NORM);
    //fftw_execute(p);
    
    //fread(tx_ref,sizeof(struct complex_f),nfft,ref_tx_file);
    
    distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,1,corr_facts,1,&ndiffnb,1,&maxd,mode_v,&time_shift,NULL);
#ifndef EMBED_PERF
    printf("distance on symbol(CONV) %f\n",distsymb);
#endif    
    
    // the ref_file contains the pnseq modulated data: so no need to demodulate the received by the pnseq
    
    
    //bit_extract((struct complex_f *)out_fft,bitstream_c+=indxbs_c,&nbits,&last_c,&indxbs_c,constel,symb_struct,pn_seq,mode_v);
    nbits_symb=0;
    lastpos_symb=0;
    indxbs_symb=0;
    
    bit_extract((struct complex_f *)out_fft,bitstream_symb,&nbits_symb,&lastpos_symb,&indxbs_symb,constel,symb_struct,pn_seq,mode_v);
    nbits_symb=0;
    
    soft_bit_extract((struct complex_f *)out_fft,soft_bits,&nbits_symb,constel,symb_struct,mode_v,&nb_soft_bits);
    soft_decode(soft_bits,bitstream_c,nbits_left,nbits_symb,last_c,indxbs_c);
    nbits_i+=nbits_symb;
    bit_extract((struct complex_f *)tx_ref,bitstream_ref_c,nbits_ref,last_ref,indxbs_ref,constel,symb_struct,pn_seq,mode_v);
  } // CONVENTIONAL demodulator

  *nbits=nbits_i;
} // end of perfect_decode

// partition into 2 subsets
void partition(float val, float *av1,int32_t *n01,  float *av2, int32_t *n02)
{

  float av1_i,av2_i;
  int32_t n1_i,n2_i;

  n1_i=*n01;
  av1_i=*av1;
  n2_i=*n02;
  av2_i=*av2;


  if(abs(val-av1_i)<abs(val-av2_i)) {
    *av1=n1_i*av1_i/(float)(n1_i+1)+val/(float)(n1_i+1);
    *n01=n1_i+1;
  } else {
    *av2=n2_i*av2_i/(float)(n2_i+1)+val/(float)(n2_i+1);
    *n02=n2_i+1;
  }

}// end of partition

// compute correction factors on the training sequence
void calc_correction_coeffs(struct complex_f *pilot_rec,struct complex_f *pilot_ref,struct complex_f *correc) {

  int32_t i;
  float mod;

  if(0 && (pilot_ref==NULL)) {
#ifndef EMBED_PERF
    for(i=0;i<nfft;i++)printf("!!!!tones %d  %f %f\n", (int) i,pilot_rec[i].r,pilot_rec[i].i);
#endif
    return;
  }

  for(i=0;i<nfft;i++) {correc[i].r=0.0;correc[i].i=0.0;}

  // compute correction factor  for channel compensation and coarse timing estimation error
  // invese of (a+jb is (a-jb)/(a2+b2)
  for(i=(null_tones>>1);i<nfft-(null_tones>>1);i++) {
    mod=pilot_rec[i].r*pilot_rec[i].r+pilot_rec[i].i*pilot_rec[i].i;
    correc[i].r=(pilot_rec[i].r*pilot_ref[i].r+pilot_rec[i].i*pilot_ref[i].i)/mod;
    correc[i].i=(pilot_rec[i].r*pilot_ref[i].i-pilot_rec[i].i*pilot_ref[i].r)/mod;
  }

#ifndef EMBED_PERF
  if (1) {
  printf("=========== DUMP CORRECTED TONES (training)\n");
  for(i=0;i<nfft;i++)
    printf("tones %d  %f %f | %f %f | %f %f\n", (int) i,pilot_rec[i].r*correc[i].r-pilot_rec[i].i*correc[i].i,pilot_rec[i].r*correc[i].i+pilot_rec[i].i*correc[i].r,pilot_rec[i].r,pilot_rec[i].i,correc[i].r,correc[i].i);
  }
#endif
} // end of calc_correction_coeffs

void substr_offset(struct complex_f *in_data,float av_re,float av_im,int32_t nb){

  int32_t i;
  for(i=0;i<nb;i++) {
    in_data[i].r-=av_re;
    in_data[i].i-=av_im;
  }

}// end of substr_offset

//symetrical rounding (prevent offset)
void gen_fixed_point_sym(struct complex_f *in_data,struct complex_f *out_data) {

 //  1.0 => 1<<11 = 2048
  {
    // check here that max fixed point value is not reached
    if (0) assert(fabs((*in_data).r)<1.0 && fabs((*in_data).i)<1.0 );


    if ((*in_data).r>=0) 
      (*out_data).r=(int32_t) floor(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    else 
      (*out_data).r=(int32_t) ceil(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));

    if ((*in_data).i>=0) 
      (*out_data).i=(int32_t) floor(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    else 
      (*out_data).i=(int32_t) ceil(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));

#ifndef EMBED_PERF
    printf("gen fixed point: %f %f == %f %f\n",(*in_data).r,(*in_data).i,(*out_data).r,(*out_data).i);
#endif
  }
}// end of gen_fixed_point_sym

void gen_fixed_point(struct complex_f *in_data,struct complex_f *out_data)
{
 //  1.0 => 1<<11 = 2048
  {
    // check here that max fixed point value is not reached
    if (0) assert(fabs((*in_data).r)<1.0 && fabs((*in_data).i)<1.0 );

    (*out_data).r=(int32_t) floor(((*in_data).r)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    (*out_data).i=(int32_t) floor(((*in_data).i)*(float) (1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)));
    //fprintf(sigfile,"%d, %d\n",cosfp,sinfp);

  }

}// end of gen_fixed_point

void green_ofdm_mod_pp(struct complex_f *xn1,struct complex_f *xn2) {
  // this function performs green modulation of the conventional OFDM
  // this function adds the cyclic prefix to the output symbol
  int32_t i,j;
  struct complex_f x_int1,x_int2,x_int3;
  //struct complex_f a;

  for (i=ncp,j=0;i<nfft1*nsub_sect+ncp;i++,j++) {
    //sn0=(xn1(1:nfft1)+xn1(1+nfft1:nfft2)+xn1(1+nfft2:nfft3)+xn1(1+nfft3:nfft))/4;
    
    xn2[i].r=(xn1[j].r+xn1[nfft1*nsub_sect+j].r+xn1[nfft2*nsub_sect+j].r+xn1[nfft3*nsub_sect+j].r);
    xn2[i].i=(xn1[j].i+xn1[nfft1*nsub_sect+j].i+xn1[nfft2*nsub_sect+j].i+xn1[nfft3*nsub_sect+j].i);

      x_int1.r=4.0*xn1[nfft1*nsub_sect+j].r-xn2[i].r;
      x_int2.r=4.0*xn1[nfft2*nsub_sect+j].r-xn2[i].r;
      x_int3.r=4.0*xn1[nfft3*nsub_sect+j].r-xn2[i].r;

      x_int1.i=4.0*xn1[nfft1*nsub_sect+j].i-xn2[i].i;
      x_int2.i=4.0*xn1[nfft2*nsub_sect+j].i-xn2[i].i;
      x_int3.i=4.0*xn1[nfft3*nsub_sect+j].i-xn2[i].i;

      // sn1=((-1-%i).*x_int1-2.*x_int2+(-1+%i).*x_int3)/4;
      // sn2=(-2.*x_int1-2.*x_int3)/4;
      // sn3=((-1+%i).*x_int1-2.*x_int2-(1+%i).*x_int3)/4;

       xn2[nfft1*nsub_sect+i].r=(-x_int1.r+x_int1.i-2.*x_int2.r-x_int3.r-x_int3.i)/8.0;
       xn2[nfft2*nsub_sect+i].r=(-2.*x_int1.r-2.*x_int3.r)/8.0;
       xn2[nfft3*nsub_sect+i].r=(-x_int1.r-x_int1.i-2.*x_int2.r-x_int3.r+x_int3.i)/8.0;
 
       xn2[nfft1*nsub_sect+i].i=(-x_int1.i-x_int1.r-2.*x_int2.i-x_int3.i+x_int3.r)/8.0;
       xn2[nfft2*nsub_sect+i].i=(-2.*x_int1.i-2.*x_int3.i)/8.0;
       xn2[nfft3*nsub_sect+i].i=(-x_int1.i+x_int1.r-2.*x_int2.i-x_int3.i-x_int3.r)/8.0;

       xn2[i].r=xn2[i].r/2.0;
       xn2[i].i=xn2[i].i/2.0;


       //S(1:N)=0;
       //size(S);
       //S(1:N1)=sn0;
       //S(1+N1:N2)=%i*sn1;
       //S(1+N2:N3)=sn2;
       //S(1+N3:N)=%i*sn3;
       /* 
       a.r= xn2[N1+i].r;
       xn2[N1+i].r=-xn2[N1+i].i;
       xn2[N1+i].i= a.r;
       a.r= xn2[N3+i].r;
       xn2[N3+i].r=-xn2[N3+i].i;
       xn2[N3+i].i= a.r;
       */
       // printf("%d xn1: %f %f %f %f\n",i,xn1[j].r,xn1[N1+j].r,xn1[N2+j].r,xn1[N3+j].r );
       //printf("   xn2: %f %f %f %f\n",i,xn2[i].r,xn2[N1+i].r,xn2[N2+i].r,xn2[N3+i].r );
      }

  // copy the cyclic prefix part
  //for(i=0;i<ncp;i++) xn2[i]=xn2[N1+i];

} // function green_ofdm_mod_pp

void green_ofdm_dmod(struct complex_f *Rr,struct complex_f *Vn) {
  // suppress the cyclic prefix
  int32_t k,j;

  for (k=0,j=ncp;k<nfft1*nsub_sect;k++,j++) {
    //vn0(1:nfft1)=Rr(1:nfft1)+Rr(1+nfft1:nfft2)+Rr(1+nfft2:nfft3)+Rr(1+nfft3:nfft);
    //vn1=Rr(1:nfft1)+%i*Rr(1+nfft1:nfft2)-Rr(1+nfft2:nfft3)-%i*Rr(1+nfft3:nfft);
    //vn2=Rr(1:nfft1)-Rr(1+nfft1:nfft2)+Rr(1+nfft2:nfft3)-Rr(1+nfft3:nfft);
    //vn3=Rr(1:nfft1)-%i*Rr(1+nfft1:nfft2)-Rr(1+nfft2:nfft3)+%i*Rr(1+nfft3:nfft);

     
    Vn[k].r=(Rr[j].r+Rr[nfft1+j].r+Rr[nfft2+j].r+Rr[nfft3+j].r)/1.0;
    Vn[k].i=(Rr[j].i+Rr[nfft1+j].i+Rr[nfft2+j].i+Rr[nfft3+j].i)/1.0;
    
    
    Vn[nfft1*nsub_sect+k].r=(Rr[j].r-Rr[nfft1+j].i-Rr[nfft2+j].r+Rr[nfft3+j].i)/1.0;
    Vn[nfft1*nsub_sect+k].i=(Rr[j].i+Rr[nfft1+j].r-Rr[nfft2+j].i-Rr[nfft3+j].r)/1.0;
    
    Vn[nfft2*nsub_sect+k].r=(Rr[j].r-Rr[nfft1+j].r+Rr[nfft2+j].r-Rr[nfft3+j].r)/1.0;
    Vn[nfft2*nsub_sect+k].i=(Rr[j].i-Rr[nfft1+j].i+Rr[nfft2+j].i-Rr[nfft3+j].i)/1.0;
    
    Vn[nfft3*nsub_sect+k].r=(Rr[j].r+Rr[nfft1+j].i-Rr[nfft2+j].r-Rr[nfft3+j].i)/1.0;
    Vn[nfft3*nsub_sect+k].i=(Rr[j].i-Rr[nfft1+j].r-Rr[nfft2+j].i+Rr[nfft3+j].r)/1.0;
    
    //printf("-- %d %f %f %f %f\n",k,Vn[k].r,Vn[nfft1+k].r,Vn[nfft2+k].r,Vn[nfft3+k].r);
    //printf(">> %d %f %f %f %f\n",k,Rr[j].r,Rr[nfft1+j].r,Rr[nfft2+j].r,Rr[nfft3+j].r);
 

      }
} // function green_ofdm_dmod

void map_tone(uint8_t bits,struct complex_f *tone,lconstell constel)
{
  //uint32_t nbits;
  assert(bits<(1<<NBITSCONST[constel]));
  if (constel==QAM16C)
  {
    //nbits=4;
    //  printf("bits=%d\n",bits);
    (*tone).r=NORM_QAM16*QAM16[bits].r;
    (*tone).i=NORM_QAM16*QAM16[bits].i;
  }
  else
  {
    if (constel==QPSKC)
    {
      //nbits=2;
      //  printf("bits=%d\n",bits);
      (*tone).r=NORM_QPSK*QPSK[bits].r;
      (*tone).i=NORM_QPSK*QPSK[bits].i;
    } else
    {
       if (constel==BPSKC) 
       {
         //nbits=1;
         //  printf("bits=%d\n",bits);
         (*tone).r=NORM_BPSK*BPSK[bits].r;
         (*tone).i=NORM_BPSK*BPSK[bits].i;
       } else 
       { 
#ifndef EMBED_PERF
         printf("no support for this constelation\n");
#endif
         exit(0);
       }
    }
  }

}// end of map_tones

void calc_nbit_const(){
  int32_t i;

  NBITSCONST[QAM16C]=4;
  NBITSCONST[QPSKC]=2;
  NBITSCONST[BPSKC]=1;

#ifndef EMBED_PERF
  for(i=0;i<3;i++) printf("i %d nb %d\n", (int) i, (int) NBITSCONST[i]);
#endif
} // end of calc_nbit_const

void  map_bits_to_tones(uint8_t *bitstream, struct complex_f* symbol, uint8_t *last, uint32_t *indxbs, lconstell constel, int32_t num_sect,int8_t *symb_struct,int8_t* pn_seq,mode_ofdm mode,int32_t ndata,int8_t flag_conj_pnseq) {

  int32_t i,k=0,l=0,togpil=0,npil=0;
  int32_t lastpos;
  uint8_t mask,bits;
  int32_t sig_bits;
  //int32_t mm;
  float tmp;
  int8_t first_sig_tone=1,second_sig_tone=0;
  int32_t num_sect_symb=0,nsub_sect_symb,nb_sigtones_mapped=0;
  lastpos=*last;
  sig_bits=num_sect;

  mask=0x0;
  for(i=0;i<NBITSCONST[constel];i++) mask|=(1<<i);
#ifndef EMBED_PERF
  printf("mask=%d\n", (int) mask);
#endif

  num_sect_symb=0;
  nsub_sect_symb=1;


  if (mode==GREEN_CONV) {
    nsub_sect_symb=1;
    num_sect_symb=0;
  }
  // pilot structure + + + + - - - - 
  //                 0 1 2 3 4 5 6 7 
  // s0: 0,4,8,12,...   s1 1,5,9,13,...

  //scatters pilot every 4 tones
  for(i=0;i<nfft;i++) {
    
#ifndef EMBED_PERF
    printf("%d %d\n", (int) i*nsub_sect_sig+num_sect, (int) symb_struct[i*nsub_sect_sig+num_sect]);
#endif
    // check no pilot is mapped on DC when spectrum centered on 0
    // if spectrum not centered, then DC is tone 0 and then check that it is a null tone
    //assert((CENTER_SPECTRUM_0 && !(!(j%PILOT_SPACING) && (i>=(nfft/2-1)) && (i<=(nfft/2-1))))  || (!CENTER_SPECTRUM_0 && (null_tones>2)));
    
    if ( symb_struct[i*nsub_sect_sig]==DATA_TONE) { 
      // map data on this tone
      bits=(bitstream[l]>>lastpos)&mask;
      lastpos+=NBITSCONST[constel];
      if(lastpos>=SIZEOF_BYTE) {
	bits|=(bitstream[l+1]<<(SIZEOF_BYTE-(lastpos-NBITSCONST[constel])));
	lastpos-=SIZEOF_BYTE;
	bits&=mask;
	l++;
      }
#ifndef EMBED_PERF
      if (0) printf("i %d bits %d lastpos %d l %d\n", (int) i, (int) bits, (int) lastpos,l);
#endif
      //if (0) map_tone((bitstream[k]>>(4*tog))&(0xf),symbol+i,QAM16C); // reads 4 bits from bitstream to map on the QAM16
      if (1) {
	map_tone(bits,symbol+i*nsub_sect_symb+num_sect_symb,constel); // map bits to tone
	if (mode!=CONV_OFDM) {
	  // when green: modulate by PN sequence and APPLY SCALING FACTOR 0.5 (since 2 symbols will be added together=> normalize energy to 1
	  if (!flag_conj_pnseq) {
	    symbol[i*nsub_sect_symb+num_sect_symb].r=pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].r*COEFF_TONE;
	    symbol[i*nsub_sect_symb+num_sect_symb].i=pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].i*COEFF_TONE;
	  } else { // mult by j*pn_seq
	    tmp=symbol[i*nsub_sect_symb+num_sect_symb].r;
	    symbol[i*nsub_sect_symb+num_sect_symb].r=-pn_seq[k+num_sect*ndata]*symbol[i*nsub_sect_symb+num_sect_symb].i*COEFF_TONE;
	    symbol[i*nsub_sect_symb+num_sect_symb].i=pn_seq[k+num_sect*ndata]*tmp*COEFF_TONE;
	  }
	} 
      }
#ifndef EMBED_PERF
      if (1) printf("xxxx%d %d %f %f k %d pn %d bits %x\n", (int) num_sect, (int) i*nsub_sect_symb+num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r, symbol[i*nsub_sect_symb+num_sect_symb].i, (int) k, (int) pn_seq[k+num_sect*ndata],bits);
#endif
      k++;    
    } 
    else if(symb_struct[i*nsub_sect_sig]==NULL_TONE) {
      // map null tone at DC
      symbol[i*nsub_sect_symb+num_sect_symb].r=0.0;symbol[i*nsub_sect_symb+num_sect_symb].i=0.0;
    }
    else if (symb_struct[i*nsub_sect_sig]==PILOT_TONE){ 
      togpil++;
      togpil=togpil%2;
      // modulate pilots with pnseq to reduce the PAPR: pilot modulation is independent of section number
	  if (!flag_conj_pnseq) {
	    symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
	    symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*pilot[0].i*pn_seq[npil];
	  } else {
	    symbol[i*nsub_sect_symb+num_sect_symb].r =0.0;
	    symbol[i*nsub_sect_symb+num_sect_symb].i =0.0;

	    //symbol[i*mm+num_sect].r = -NORM_QPSK*pilot[0].i*pn_seq[npil];
	    //symbol[i*mm+num_sect].i =  NORM_QPSK*pilot[0].r*pn_seq[npil];
	  }	
#ifndef EMBED_PERF
	  if (1) printf("xxxxpil %d %f %f flag %d\n", (int) i*nsub_sect_symb+num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r,symbol[i*nsub_sect_symb+num_sect_symb].i, (int) flag_conj_pnseq);
#endif
      npil++;
    }
    else if(symb_struct[i*nsub_sect_sig]==SIG_TONE) {

      // sig tones generated for right / left symbols so that they can be added wwithout interfering (if sigtone on left then null on right)
#ifndef EMBED_PERF
      printf("numsec %d\n", (int) sig_bits);
#endif
      if (!flag_conj_pnseq && ( nb_sigtones_mapped<(nb_sigtones/2)) ) {
	symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	// QPSK mod of sig tones => shift by 2
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      } 
      else if (flag_conj_pnseq && ( nb_sigtones_mapped>=(nb_sigtones/2))) {
	symbol[i*nsub_sect_symb+num_sect_symb].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol[i*nsub_sect_symb+num_sect_symb].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      } else {
	symbol[i*nsub_sect_symb+num_sect_symb].r=0.0;
	symbol[i*nsub_sect_symb+num_sect_symb].i=0.0;
	nb_sigtones_mapped++;
      }

#ifndef EMBED_PERF
      if (1) printf("xxxxsig %d(%d) -- numsect=%d(%d) => %f %f (%d %d)\n", (int) i*nsub_sect_symb+num_sect_symb, (int) flag_conj_pnseq, (int) sig_bits&0x3, (int) num_sect_symb,symbol[i*nsub_sect_symb+num_sect_symb].r,symbol[i*nsub_sect_symb+num_sect_symb].i, (int) first_sig_tone, (int) second_sig_tone);
#endif
      
      if (second_sig_tone) {second_sig_tone=0;}
      if (first_sig_tone) {first_sig_tone=0;second_sig_tone=1;}
    }
#ifndef EMBED_PERF
    else
      printf("WARNING(map_bits_to_tones: tone %d unassigned\n", (int) i);
#endif
  }

  *last=lastpos;
  *indxbs=l;
#ifndef EMBED_PERF
  printf("finished mapping %d tones and %d pilots\n", (int) k, (int) npil);
#endif
} // end of map_bits_to_tones

void  map_bits_to_tones_native(uint8_t *bitstream, struct complex_f* symbol, uint8_t *last, uint32_t *indxbs, lconstell constel, int32_t num_sect,int8_t *symb_struct,int8_t* pn_seq,mode_ofdm mode,int32_t ndata,int8_t flag_conj_pnseq) {

  int32_t i,k=0,l=0,npil=0;
  int32_t lastpos;
  uint8_t mask,bits;
  int32_t sig_bits;
  //int32_t mm;
  int32_t num_sect_symb=0,nsub_sect_symb;
  lastpos=*last;
  sig_bits=num_sect;

  mask=0x0;
  for(i=0;i<NBITSCONST[constel];i++) mask|=(1<<i);
#ifndef EMBED_PERF
  printf("mask=%d\n", (int) mask);
#endif

  num_sect_symb=0;
  nsub_sect_symb=1;


  if (mode==GREEN_CONV) {
    nsub_sect_symb=1;
    num_sect_symb=0;
  }
  // pilot structure + + + + - - - - 
  //                 0 1 2 3 4 5 6 7 
  // s0: 0,4,8,12,...   s1 1,5,9,13,...

  //scatters pilot every 4 tones
  for(i=0;i<nfft;i++) {
    
#ifndef EMBED_PERF
    printf("%d %d\n", (int) i*nsub_sect_sig+num_sect, (int) symb_struct[i*nsub_sect_sig+num_sect]);
#endif
    // check no pilot is mapped on DC when spectrum centered on 0
    // if spectrum not centered, then DC is tone 0 and then check that it is a null tone
    //assert((CENTER_SPECTRUM_0 && !(!(j%PILOT_SPACING) && (i>=(nfft/2-1)) && (i<=(nfft/2-1))))  || (!CENTER_SPECTRUM_0 && (null_tones>2)));
    
    if ( symb_struct[i*nsub_sect_sig]==DATA_TONE) { 
      // map data on this tone
      bits=(bitstream[l]>>lastpos)&mask;
      lastpos+=NBITSCONST[constel];
      if(lastpos>=SIZEOF_BYTE) {
	bits|=(bitstream[l+1]<<(SIZEOF_BYTE-(lastpos-NBITSCONST[constel])));
	lastpos-=SIZEOF_BYTE;
	bits&=mask;
	l++;
      }
#ifndef EMBED_PERF
      if (1) printf("i %d bits %d lastpos %d l %d\n", (int) i, (int) bits, (int) lastpos, (int) l);
#endif

      //if (0) map_tone((bitstream[k]>>(4*tog))&(0xf),symbol+i,QAM16C); // reads 4 bits from bitstream to map on the QAM16
      map_tone(bits,symbol+i,constel); // map bits to tone 

#ifndef EMBED_PERF
      if (1) printf("xxxx%d %d %f %f k %d bits %x\n", (int) num_sect, (int) i,symbol[i].r,symbol[i].i, (int) k, (int) bits);
#endif
      k++;    
    } 
    else if(symb_struct[i]==NULL_TONE) {
      // map null tone at DC
      symbol[i].r=0.0;symbol[i].i=0.0;
    }
    else if (symb_struct[i]==PILOT_TONE){ 
      // modulate pilots with pnseq to reduce the PAPR: pilot modulation is independent of section number
      symbol[i].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
      symbol[i].i = NORM_QPSK*pilot[0].i*pn_seq[npil];

#ifndef EMBED_PERF
      if (1) printf("xxxxpil %d %f %f flag %d\n", (int) i*nsub_sect_symb+num_sect_symb,symbol[i].r,symbol[i].i, (int) flag_conj_pnseq);
#endif
      npil++;
    }
    else if(symb_struct[i]==SIG_TONE) {

      // sig tones generated for right / left symbols so that they can be added wwithout interfering (if sigtone on left then null on right)
#ifndef EMBED_PERF
      printf("numsec %d\n", (int) sig_bits);
#endif
      symbol[i].r=0.0;
      symbol[i].i=0.0;
    }

#ifndef EMBED_PERF
    else
      printf("WARNING(map_bits_to_tones: tone %d unassigned\n", (int) i);
#endif
  }

  *last=lastpos;
  *indxbs=l;

#ifndef EMBED_PERF
  printf("(native)finished mapping %d tones and %d pilots\n", (int) k, (int) npil);
#endif

} // end of map_bits_to_tones_native

// modulate the pn_seq
void	pn_seq_symb(int nfft, struct complex_f *symbol1,struct complex_f *symbol2,struct complex_f *symbol, int32_t num_sect, mode_ofdm mode, int32_t ndata){

  int32_t i,k=0,npil=0;
  int32_t sig_bits=num_sect;
  //int32_t mm;
  float tmp;
  int32_t nb_sigtones_mapped=0;

  for(i=0;i<nfft;i++) {
    if ( symb_struct[i]==DATA_TONE) { 
	if (mode!=CONV_OFDM) {
	  // when green: modulate by PN sequence and APPLY SCALING FACTOR 0.5 (since 2 symbols will be added together=> normalize energy to 1
	    symbol1[i].r=pn_seq[k+num_sect*ndata]*symbol[i].r*COEFF_TONE;
	    symbol1[i].i=pn_seq[k+num_sect*ndata]*symbol[i].i*COEFF_TONE;

	    tmp=symbol[i].r;
	    symbol2[i].r=-pn_seq2[k+num_sect*ndata]*symbol[i].i*COEFF_TONE;
	    symbol2[i].i=pn_seq2[k+num_sect*ndata]*tmp*COEFF_TONE;
	  }
#ifndef EMBED_PERF
	if (1) printf("1xxxx%d %d %f %f k %d pn %d\n", (int) num_sect, (int) i,symbol1[i].r,symbol1[i].i, (int) k, (int) pn_seq[k+num_sect*ndata]);
#endif
	k++;    
    } 
    else if(symb_struct[i]==NULL_TONE) {
      // map null tone at DC
      symbol[i].r=0.0;symbol[i].i=0.0;
      symbol1[i].r=0.0;symbol1[i].i=0.0;
      symbol2[i].r=0.0;symbol2[i].i=0.0;
    }
    else if (symb_struct[i]==PILOT_TONE){ 
      // modulate pilots with pnseq to reduce the PAPR: pilot modulation is independent of section number
      symbol1[i].r = NORM_QPSK*pilot[0].r*pn_seq[npil];
      symbol1[i].i = NORM_QPSK*pilot[0].i*pn_seq[npil];
      
      symbol2[i].r =0.0;
      symbol2[i].i =0.0;
      
      //symbol[i*mm+num_sect].r = -NORM_QPSK*pilot[0].i*pn_seq[npil];
      //symbol[i*mm+num_sect].i =  NORM_QPSK*pilot[0].r*pn_seq[npil];
      
#ifndef EMBED_PERF
      if (1) printf("xxxxpil %d %f %f\n", (int) i,symbol[i].r,symbol[i].i);
#endif
      npil++;
    }
    else if(symb_struct[i]==SIG_TONE) {

      // sig tones generated for right / left symbols so that they can be added wwithout interfering (if sigtone on left then null on right)
#ifndef EMBED_PERF
      printf("numsec %d\n", (int) sig_bits);
#endif

      symbol1[i].r=0.0;
      symbol1[i].i=0.0;
      symbol2[i].r=0.0;
      symbol2[i].i=0.0;

      if (( nb_sigtones_mapped<(nb_sigtones/2)) ) {
	symbol1[i].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol1[i].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	// QPSK mod of sig tones => shift by 2
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      } 
      else if (( nb_sigtones_mapped>=(nb_sigtones/2))) {
	symbol2[i].r = NORM_QPSK*QPSK[sig_bits&0x3].r;
	symbol2[i].i = NORM_QPSK*QPSK[sig_bits&0x3].i;
	sig_bits = sig_bits>>2;
	nb_sigtones_mapped++;
      }
 
#ifndef EMBED_PERF
      if (1) printf("xxxxsig %d -- numsect=%d(%d) => %f %f\n", (int) i, (int) sig_bits&0x3, (int) num_sect,symbol1[i].r,symbol1[i].i);
#endif
     
    }
#ifndef EMBED_PERF
    else
      printf("WARNING(map_bits_to_tones: tone %d unassigned\n", (int) i);
#endif
  }



}// end of pn_seq_symb

#ifdef LINUX
void dump_csv(FILE *dumpfile, int32_t Npoint,struct complex_f *data){
  int32_t i;
#ifdef INTERP_FIR
  struct complex_f IQ_o[OVSMPL_FACTOR];
 #endif

  // case where there is an interpolator
  for (i=0;i<Npoint;i++) {
#ifndef INTERP_FIR 
      fprintf(dumpfile,"%d, %d\n",(int)data[i].r,(int)data[i].i);
#endif
#ifdef INTERP_FIR 
      fir_x8_IQ(coeff,delline1,delline2,FIR_ORDER,&indx_fir,data[i],IQ_o);    
      for (j=0;j<OVSMPL_FACTOR;j++) {
	fprintf(dumpfile,"%d, %d \n",(int)IQ_o[j].r,(int)IQ_o[j].i);
      }
 #endif   
  }
}// end of dump_csv
#endif

// function to dump into a buffer
struct complex_f* dump_csv_buff(struct complex_f *p_buf, int32_t Npoint, struct complex_f *data){
  int32_t i;
    for (i=0; i < Npoint; i++) {
	//p_buf[i] = (float) data[i];	
	p_buf[i].r = (float) data[i].r;	
	p_buf[i].i = (float) data[i].i;	
	
  }
  return p_buf + Npoint;
}// end of dump_csv_buff

#ifdef LINUX
void plot_qam(FILE *dumpfile, int32_t Npoint,struct complex_f *data)
{
  int32_t i;

  if (0) for (i=(null_tones>>1);i<Npoint-(null_tones>>1);i+=4)
    if ((CENTER_SPECTRUM_0 && ((i<nfft/2-ZTONE_RAD_L) || (i>nfft/2+ZTONE_RAD_R))) || !CENTER_SPECTRUM_0)
      fprintf(dumpfile,"%f %f\n",data[i].r,data[i].i);
    for (i=(null_tones>>1);i<Npoint-(null_tones>>1);i+=4)  fprintf(dumpfile,"%f %f\n",data[i].r,data[i].i);

}

void plot(FILE *dumpfile, int32_t Npoint,struct complex_f *data){
  int32_t i;

  for (i=0;i<Npoint;i++) fprintf(dumpfile,"%f %f\n",data[i].r,data[i].i);

}

 void plot_double(FILE *dumpfile, int32_t Npoint,float *data){
  int32_t i;

  for (i=0;i<Npoint;i++) fprintf(dumpfile,"%f\n",data[i]);

}   

void plot_cpxf(FILE *dumpfile, int32_t Npoint, struct complex_f *data){
  int32_t i;

  for (i=0;i<Npoint;i++) fprintf(dumpfile,"%f %f\n",data[i].r,data[i].i);

}   
#endif

#ifdef LINUX
// skips data in csv format
int32_t skip_csv(int32_t nb_data,FILE* inputfile) {

  int32_t i,nbdata_read=0;
  float dr,di;

  for (i=0;i<nb_data;i++) {
    if (fscanf(inputfile,"%f,%f",&dr,&di)!=2) break;
    nbdata_read++;
  }

  return(nbdata_read);

}// end of skip_csv
#else
struct complex_f* skip_csv(int32_t nb_data, struct complex_f * input_buf) {
  return (input_buf + nb_data);
}// end of skip_csv
#endif

#ifdef LINUX
// reads data in csv format
int32_t fread_csv(struct complex_f* indata,int32_t nb_data,FILE* inputfile) {


  int32_t i,nbdata_read=0;
  float dr,di;

  for (i=0;i<nb_data;i++) {
#ifndef EMBED_PERF
    printf("==%d\n", (int) i); 
#endif
    if (fscanf(inputfile,"%f,%f",&dr,&di)!=2) break;
    indata[i].r=(float)dr;
    indata[i].i=(float)di;
#ifndef EMBED_PERF
    if (1) printf("fread_csv: indata %d %f %f\n", (int) i, dr, di);
#endif
    nbdata_read++;
  }

  return(nbdata_read);

}// end of fread_csv
#endif

//#ifdef LINUX
// function to read from a buffer : generate a double buffer
struct complex_f* fread_csv_buf(struct complex_f *indata, uint32_t nb_data, struct complex_f *p_buf) {
  int32_t i;

// fprintf(stderr, "pbuf=%p nb_data=%d\n", p_buf, (int) nb_data);
  for (i=0; i < nb_data; i++) {
    indata[i].r = (float) p_buf[i].r;
    indata[i].i = (float) p_buf[i].i;
#ifndef EMBED_PERF
    printf("fread_csv: indata %d %f %f\n", (int) i,indata[i].r,indata[i].i);
#endif
// fprintf(stderr, "pbuf=%p nb_data=%d\n", p_buf, (int) nb_data);
 }

  return (p_buf + nb_data);
}// end of fread_csv_file
/*#else
struct complex_f* fread_csv(struct complex_f *indata, int32_t nb_data, struct complex_f *p_buf) {
  int32_t i;

  for (i=0; i < nb_data; i++) {
    indata[i] = p_buf[i];
    //embed perf//printf("indata %d %f %f\n",i,indata[i].r,indata[i].i);
    //if (0) printf("%f %f\n", dr, di);
  }

  return (p_buf + nb_data);
}// end of fread_csv
#endif
*/

void conv_float2double(struct complex_f *in_float,struct complex_f *out_double, int32_t nb)
{

  int32_t i;
  for(i=0;i<nb;i++) {
    out_double[i].r=(float) in_float[i].r;
    out_double[i].i=(float) in_float[i].i;
#ifndef EMBED_PERF
    printf("@@@@ %f %f\n",in_float[i].r,in_float[i].i);
#endif
  }

} // end of conv_float2double

// shift spectrum by nfft/2 units (centered on 0)
// k = 1.0 =>  (2/N * N/2 )*PI =PI
void shift_spectrum(struct complex_f *insig, int32_t nb, float k){
  int32_t i;
  float cr,ci;

  for(i=0;i<nb;i++) {
    cr=insig[i].r*cos(i*k*PI)-insig[i].i*sin(i*k*PI);
    ci=insig[i].i*cos(i*k*PI)+insig[i].r*sin(i*k*PI);
    insig[i].r=cr;
    insig[i].i=ci;
  }

}// end shift_spectrum 

void print_usage() {
  
  printf("options list\n");
  printf("\t -g  : digital gain value (prior to DA input)\n");
  printf("\t -gm : green mode [%d %d] (0:conventional,1:SLM green,2:SLM_conv)\n",0,2);
  printf("\t -n  : fft size [%d %d] (%d)\n",N_MIN,N_MAX,NFFT);
  printf("\t -cp : cyclic prefix size [%d %d] (%d)\n",NCP_MIN,NCP_MAX,NCP);
  printf("\t -nt : nb of nul tones [%d %d] must be even\n",0,NFFT);
  printf("\t -d  : demodulation [0 1] \n");
  printf("\t -ws : activate weak signal detection\n");
  printf("\t -c  : QAM type (0=> BPSK 1=>QPSK 2=>QAM16) \n");
  printf("\t -b  : backoff value [%d %d] (%d)\n",MINBACKOFF,MAXBACKOFF,DEF_BACKOFF_INDX);
  printf("\t -h  : print usage\n");
  exit(0);

}// end of print usage

void parse_options(int32_t argc,int8_t *argv[],int32_t *backoff,float *gain,mode_ofdm *mode,int8_t *dmod,int32_t *nfft_v,int32_t *ncp_v, int32_t *null_tones_v,int32_t *nsub_sect_v, int8_t* constell_v,int8_t *ws){

  int32_t i;
  int8_t error=0, char_mode;
  *mode=DEFAULT;

  *ws=0;

  for(i=1;i<argc;i++){
    if (!strcmp(argv[i],"-b")) {
      if (i+1<argc) {
	*backoff=atoi(argv[i+1]);
	i++;
      }
      else {error=1;break;}
    }
    else if (!strcmp(argv[i],"-g")) {
      if (i+1<argc) {
	*gain=atof(argv[i+1]);
	i++;
      }
      else {error=1;break;}
    }  
    else if (!strcmp(argv[i],"-gm")) {
      if (i+1<argc) {
	char_mode=atoi(argv[i+1]);
	if (char_mode==-1) *mode=DEFAULT;
	else if (char_mode==0) *mode=CONV_OFDM;
	else if (char_mode==1) *mode=GREEN_GREEN;
	else if (char_mode==2) *mode=GREEN_CONV;
	i++;
      }
      else {error=1;break;}
    }  
    else if (!strcmp(argv[i],"-d")) {
      if (i+1<argc) {
	*dmod=atoi(argv[i+1]);
	i++;
      }
      else {error=1;break;}
    } 
    else if (!strcmp(argv[i],"-ws")) {
      *ws=1;
    }
    else if ( !strcmp(argv[i],"-n")) {
      if (i+1<argc) {
	*nfft_v=atoi(argv[i+1]);
	i++;
      }
      else  {error=1;break;}
    } 
    else if ( !strcmp(argv[i],"-cp")) {
      if (i+1<argc) {
	*ncp_v=atoi(argv[i+1]);
	i++;
      }
      else  {error=1;break;}
    } 
    else if ( !strcmp(argv[i],"-nt")) {
      if (i+1<argc) {
	*null_tones_v=atoi(argv[i+1]);
	i++;
      }
      else  {error=1;break;}
    } 
    else if ( !strcmp(argv[i],"-m")) {
      if (i+1<argc) {
	*nsub_sect_v=atoi(argv[i+1]);
	i++;
      }
      else  {error=1;break;} 
    }
    else if ( !strcmp(argv[i],"-c")) {
      if (i+1<argc) {
	*constell_v=atoi(argv[i+1]);
	i++;
      }
      else  {error=1;break;} 
    }
    else if ( !strcmp(argv[i],"-h") ||  !strcmp(argv[i],"--help")) {
      print_usage();
      }
      else  {error=1;break;} 
    //fprintf(stderr,"bad option in command line %s\n",argv[i]);
    //exit(0);
  }
#ifdef DEBUG  
  if (error) {fprintf(stderr,"error in command line options\n");exit(0);}
#endif

}// end of parse_options


void calc_mean_variance(struct complex_f *indat, int32_t ndat, float *offr, float *offi, float *var){

  int32_t i;
  float offi_r,offi_i,sigma;

  assert(ndat!=0);

  // mean calculation
  offi_r=0.0;
  offi_i=0.0;

  for (i=0;i<ndat;i++) {

    offi_r+=indat[i].r;
    offi_i+=indat[i].i;

  }

  offi_r/=(float) ndat;
  offi_i/=(float) ndat;

  // variance calculation
  sigma=0.0;
  for (i=0;i<ndat;i++) {
    sigma+=(offi_r-indat[i].r)*(offi_r-indat[i].r)+(offi_i-indat[i].i)*(offi_i-indat[i].i);
  }

  sigma/=(float) ndat;

  sigma=sqrt(sigma);

  *var=sigma;
  *offr=offi_r;
  *offi=offi_i;

}// end of calc_mean_variance

int32_t demod_symbol(struct complex_f *in_dmod_buff,struct complex_f *tx_ref,int32_t symbol_nb,int32_t backoff_v, int8_t flag_iter, struct complex_f *out_dmod,struct complex_f * corr_facts, struct complex_f *out_fft,int8_t mode_v,int32_t *bckoffmin,int8_t *symb_struct,int32_t ndata_tones,int8_t *pn_seq,int8_t *pn_seq2) {

  int32_t i,j;
  float distsymb_min,tot_distsymb,distsymb;
  //int32_t ndiffmin;
  uint32_t ndiffnb;
  uint32_t lasti;
  float av_re,av_im,sigma;
  int32_t bckoffminc;
  //fftw_plan p;
  float maxd;
  int32_t time_shift;

  bckoffminc=*bckoffmin;

  
  // normalize 
  norm_v(in_dmod_buff,fft_norm_v,(nfft+ncp+2*backoff_v));
  
  // symbol demodulation
  //ndiffmin=BIG_INT;
  distsymb_min=(float) BIG_INT;
  tot_distsymb=0.0;
  
  lasti=(flag_iter)?(backoff_v):0;
  
  if (flag_iter) 
    for (i=0;i<=2*lasti;i++) {
      //plot(dumpout,nfft+ncp,in_dmod_buff+i);
      
      calc_mean_variance(in_dmod_buff+i+ncp,nfft,&av_re,&av_im,&sigma);
      //substr_offset(in_dmod_buff+i+ncp,av_re,av_im,nfft);	      
#ifndef EMBED_PERF
      printf("333for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);
#endif
      
      if (0&&mode_v) {
#ifndef EMBED_PERF
	printf("do green dmod\n");
#endif
	green_ofdm_dmod(in_dmod_buff+i,out_dmod);
      }
      else {
	memcpy(out_dmod,in_dmod_buff+ncp+i,nfft*sizeof(struct complex_f));
	norm_v(out_dmod,2.0,nfft);
      }
      //if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod);
      
      if (1) {
	if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect_sig,1.0);
      
#ifndef EMBED_PERF
	if (1) for(j=0;j<nfft*nsub_sect;j++) printf("rrr66 %d %d %f %f\n", (int) i, (int) j,out_dmod[j].r,out_dmod[j].i);
#endif
	//p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
	//fftw_execute(p);
        fft_code(nfft*nsub_sect, out_dmod, out_fft, FFT_FORWARD, FFT_NO_NORM);
      } 
  
    
      distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,i,corr_facts,1,&ndiffnb,1,&maxd,mode_v,&time_shift,NULL);
      // search minimize max dist
      if (maxd<distsymb_min) {distsymb_min=maxd;bckoffminc=i;}
      
      //if (0&&(ndiffnb<ndiffmin)) {ndiffmin=ndiffnb;bckoffminc=i;}
#ifndef EMBED_PERF
      printf("for backoff index %d err is %d dist=%f max %f\n", (int) i, (int) ndiffnb,distsymb,maxd);
#endif
    }
  
#ifndef EMBED_PERF
  if (SEARCH_START) printf("min backoff index is %d distmin=%f\n", (int) bckoffminc,distsymb_min);
#endif

  // final modulation
  calc_mean_variance(in_dmod_buff+bckoffminc+ncp,nfft,&av_re,&av_im,&sigma);
  //substr_offset(in_dmod_buff+bckoffminc+ncp,av_re,av_im,nfft);	      
#ifndef EMBED_PERF
  printf("444for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);
#endif

  if (0&&mode_v){
#ifndef EMBED_PERF
    printf("do green dmod\n");
#endif
    green_ofdm_dmod(in_dmod_buff+bckoffminc,out_dmod);
    //norm(in_dmod_buff,2.0);
  }
  else {
    memcpy(out_dmod,in_dmod_buff+ncp+bckoffminc,nfft*sizeof(struct complex_f));
    norm_v(out_dmod,2.0,nfft);
  }
  if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_dmod,nfft*nsub_sect_sig,1);
  
#ifndef EMBED_PERF
  if (0) for(j=0;j<nfft;j++) printf("rrr67 %d %d %f %f\n", (int) bckoffminc, (int) j,out_dmod[j].r,out_dmod[j].i);
#endif
  
  if (mode_v==GREEN_GREEN) 
    //p = fftw_plan_dft_1d(nfft*nsub_sect, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    fft_code(nfft*nsub_sect, out_dmod, out_fft, FFT_FORWARD, FFT_NO_NORM);
  else  //p = fftw_plan_dft_1d(nfft, (fftw_complex *)out_dmod, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    fft_code(nfft, out_dmod, out_fft, FFT_FORWARD, FFT_NO_NORM);

  //fftw_execute(p);
  
  distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,bckoffminc,corr_facts,1,&ndiffnb,1,&maxd, (mode_ofdm) mode_v,&time_shift,NULL);
  tot_distsymb+=distsymb;

#ifndef EMBED_PERF
  printf("==>for backoff index %d err is %d dist=%f maxdistance=%f\n", (int) bckoffminc, (int) ndiffnb,distsymb,maxd);
#endif
  *bckoffmin=bckoffminc;
  return ndiffnb;
  
} // end of demod_symbol


void detect_peak(struct complex_f *in_dmod,uint32_t *index_of_peak,uint32_t *last_peak_indx,  int8_t *first_peak_found,int8_t *found_peak,int8_t mode_v) {

  int32_t i,j,k;
  struct complex_f phi,gm;
  int8_t not_a_max=0;
  float max_score=0.0,phase_for_max;
  float av_re,av_im,sigma;
  int32_t index_of_peak_i;
  int32_t last_peak_indx_i;
  int8_t first_peak_found_i;
  uint8_t found_peak_i;
  float min_peak_val_v;

  index_of_peak_i=*index_of_peak;
  last_peak_indx_i=*last_peak_indx;
  first_peak_found_i=*first_peak_found;
  found_peak_i=*found_peak;

	assert(ncp!=0);

	//else sample_nb=tot_read-2*nfft+n1+2*rad_search_peak_v;
	// calculation of expected min correlation from ref levels of training seq autocorr
	calc_mean_variance(in_dmod,2*nfft+ncp,&av_re,&av_im,&sigma);
	sigma=(sigma>min_input_power_v)?sigma:min_input_power_v;      
	// compute timing (phi_sum) and freq offset (gm_sum)
	// phi_sul 0..2nfft-n1
	for (j=n1;j<2*nfft;j++) {
	  
#ifndef EMBED_PERF
	  if (0) printf("---%d ir %f ii %f\n", (int) sample_nb+j-n1-rad_search_peak_v,in_dmod[j-n1].r,in_dmod[j-n1].i);
#endif
	  phi.r=0;gm.r=0;gm.i=0;
	  for(i=0;i<ncp;i++){
	    
	    phi.r+=(in_dmod[j+i].r-av_re)*(in_dmod[j+i].r-av_re)+(in_dmod[j+i].i-av_im)*(in_dmod[j+i].i-av_im)+(in_dmod[j+i-n1].r-av_re)*(in_dmod[j+i-n1].r-av_re)+(in_dmod[j+i-n1].i-av_im)*(in_dmod[j+i-n1].i-av_im);
	    
	    gm.r+=((in_dmod[j+i].r-av_re)*(in_dmod[j+i-n1].r-av_re))+((in_dmod[j+i].i-av_im)*(in_dmod[j+i-n1].i-av_im));
							 
	    gm.i+=((in_dmod[j+i].r-av_re)*(in_dmod[j+i-n1].i-av_im))-((in_dmod[j+i].i-av_im)*(in_dmod[j+i-n1].r-av_re));
	  }
	  phi_sum[j-n1]=abs(sqrt(gm.r*gm.r+gm.i*gm.i)-(snr/(snr+1))*phi.r);
	  gm_sum[j-n1]= -atan(gm.i/gm.r)/(2.0*PI);
#ifndef EMBED_PERF
	  //printf("correlation score at %d : %f %f\n",j-n1,phi_sum[j-n1],gm_sum[j-n1]);
#endif
	}

#ifdef LINUX	
	if (1) {plot_double(dumpphisum,2*nfft-n1-rad_search_peak_v,phi_sum);}
#endif
	
#ifndef EMBED_PERF
	if (first_peak_found_i) printf ("search first peak\n");
	else printf("search next peak\n");
#endif
	found_peak_i=0;
	//printf("scan buffer for maximum, sample nb:  %d\n",tot_read);
	// phisum is defined from 0 to 2N-N1
	
	if ((!FIND_FIRST_PEAK_ONLY) || (FIND_FIRST_PEAK_ONLY&&first_peak_found_i)){
	  for (i=rad_search_peak_v;i<2*nfft-n1-rad_search_peak_v;i++){ 

	    // average power on a buffer of size POW_BUFF_SIZE
	    av_pow-=powinst[indx_avpow]/(float)(POW_BUFF_SIZE);
	    powinst[indx_avpow]=phi_sum[i];
	    av_pow+=powinst[indx_avpow++]/(float)(POW_BUFF_SIZE);
	    if (indx_avpow>=POW_BUFF_SIZE) indx_avpow=0;


	    if (mode_v)
	      //min_peak_val_v=(sigma/sigma_autocorr_ref[0])*(sigma/sigma_autocorr_ref[0])*(sigma_autocorr_ref[1]*0.6);
	      min_peak_val_v=(0.7/0.2)*sigma*(0.7/0.2)*sigma*0.6;
	    else 
	      //min_peak_val_v=(sigma/sigma_autocorr_ref[0])*(sigma/sigma_autocorr_ref[0])*(sigma_autocorr_ref[1]*0.6);
	      min_peak_val_v=(0.7/0.2)*sigma*(0.7/0.2)*sigma*0.6;

	    //min_peak_val_v=MIN_PEAK_VAL;
	      
	      // slow decay of max score (allow detection of successive peaks in buffer)
	    max_score-=(max_score-av_pow)*0.05; 
#ifndef EMBED_PERF
	    if (1) {
	      printf("%%%%%% min peak val %f sigma %f\n",min_peak_val_v,sigma);
	      printf("smpl %d >> maxscore=%f avpow=%f phisum=%f av_max_val %f\n", (int) sample_nb,max_score,av_pow,phi_sum[i],0.8*av_max_value);
	      printf("\t%f %f\n",in_dmod[i].r,in_dmod[i].i);
	    }
#endif
	    // sample counter
	    sample_nb++;
	    
	    if ((phi_sum[i]>max_score) && (phi_sum[i]>min_peak_val_v)) { // check this is a peak
	      max_score=phi_sum[i];
	      phase_for_max=gm_sum[i];
#ifndef EMBED_PERF
	      printf("### %f %f\n",phi_sum[i],min_peak_val_v);
#endif
	      // this to eliminate peak detection on not a local maximum (cause error when cthe actual local max is within the search radius)  
	      not_a_max=0;
	      for (j=i;j<i+rad_search_peak_v;j++) if (phi_sum[j]>max_score) not_a_max=1;
	      for (j=i;j>i-rad_search_peak_v;j--) if (phi_sum[j]>max_score) not_a_max=1;
	      
	      if (!not_a_max) {
#ifndef EMBED_PERF
		printf("enter peak detector %d\n", (int) i);
#endif
		for (j=i;(j>=0)&&((i-j)<rad_search_peak_v);j--) 
		  {
#ifndef EMBED_PERF
		    if (1) printf("%d %f,", (int) j,phi_sum[j]);
#endif
		    if (phi_sum[j]<=PEAK_DETECTOR_FACT*max_score) break;
		  }
#ifndef EMBED_PERF
		if (0)printf("\n");
#endif
		for (k=i;(k<2*nfft+rad_search_peak_v-n1)&&((k-i)<rad_search_peak_v);k++) 
		  {
#ifndef EMBED_PERF
		    if (1) printf("%d %f,", (int) k,phi_sum[k]);
#endif
		    if (phi_sum[k]<=PEAK_DETECTOR_FACT*max_score) break;
		  }
#ifndef EMBED_PERF
		if (1) printf("\n");
#endif
		if (((i-j)<rad_search_peak_v)&&((k-i)<rad_search_peak_v)&&
		    //&& (i!=j)&&(i!=k)&&
		    (max_score>(av_pow*FACTOR_COARSE_SYNC))
		    //&&(max_score>=0.8*last_max_peak_val)) 
		    )
		  {	// found a peak
		    
		    // peak index is either the middle of Max-10%, or the max according to AVERAGE_PEAK_POS
		    if (AVERAGE_PEAK_POSITION) 
		      index_of_peak_i=(j+k)>>1;
		    else index_of_peak_i=i;
		    
		    assert(found_peak_i<MAX_NB_PEAK);
		    list_of_peaks[found_peak_i].index=index_of_peak_i;
		    list_of_peaks[found_peak_i].val_max=max_score;	  
		    list_of_peaks[found_peak_i].smpl_indx=sample_nb;
		    list_of_peaks[found_peak_i].val_phase=phase_for_max;	  
#ifndef EMBED_PERF
		    printf("found peak at index %d(%d) val %f\n", (int) index_of_peak_i, (int) i,max_score);
#endif
		    // store detected max value
		    av_max[indx_max_average++]=max_score;
		    if (indx_max_average>=DEPTH_MAX_AVERAGE) indx_max_average=0;
		    
		    // compute average max value
		    av_max_value=0.0;
		    for(j=0;j<DEPTH_MAX_AVERAGE;j++) av_max_value+=av_max[j];
		    av_max_value=av_max_value/(float)DEPTH_MAX_AVERAGE;
		    
		    found_peak_i+=1;
		    //break;
		    // jump a little further to continue search, but take care when at end of buffer to count samples (sample_nb)
		    i+=2*rad_search_peak_v; 
		    if (i<2*nfft-n1-rad_search_peak_v) sample_nb+=2*rad_search_peak_v;
		    else sample_nb+=2*nfft-n1-rad_search_peak_v-(i-2*rad_search_peak_v);
		  } 
	      }
#ifndef EMBED_PERF
	      if (0) printf("i=%d j=%d k=%d\n", (int) i, (int) j, (int) k);
#endif
	    }
	  }
#ifndef EMBED_PERF
	  printf("max_score=%f avpower=%f\n\t\t index of peak found %d j=%d k=%d,nb_peaks %d\n",max_score,av_pow, (int) index_of_peak_i, (int) j, (int) k, (int) found_peak_i);
#endif
	} else {
	  sample_nb+=2*nfft-n1-2*rad_search_peak_v;
#ifndef EMBED_PERF
	  printf("!!!!! mode free running is active last smpl in  buff: %d lastpeak %d indx in buff %d\n", (int) sample_nb, (int) last_peak_indx_i, (int) last_index_value);
#endif
	  found_peak_i=0;
	  while (((last_index_value+(found_peak_i+1)*(nfft+ncp)) >= rad_search_peak_v) && (last_index_value+(found_peak_i+1)*(nfft+ncp)<(2*nfft-n1-rad_search_peak_v))) {
	    // index in buffer
	    list_of_peaks[found_peak_i].index=last_index_value+(found_peak_i+1)*(nfft+ncp);
	    // index of sample
	    list_of_peaks[found_peak_i].smpl_indx=last_peak_indx_i+nfft+ncp;
#ifndef EMBED_PERF
	    printf("!!!!! mode free running is active last smpl in  buff: %d lastpeak %d indx in buff %d\n", (int) sample_nb, (int) last_peak_indx_i, (int) last_index_value);
	    printf("free running: one start symbol is in buffer  indx=%d/smpl=%d\n", (int) list_of_peaks[found_peak_i].index, (int) list_of_peaks[found_peak_i].smpl_indx);
#endif
	    last_peak_indx_i+=nfft+ncp;
	    found_peak_i++;
	  }
	}

  *index_of_peak=index_of_peak_i;
  *last_peak_indx=last_peak_indx_i;
  *first_peak_found=first_peak_found_i;
  *found_peak=found_peak_i;
	

}// end of detect_peak


void process_peak_list(int8_t found_peak,int8_t first_peak_found,uint32_t *last_peak_indx) {

  // compute average distance between peaks
  
  int32_t distpeak;
  int32_t index_max1,index_max2,index_max;
  int32_t i,j;
  int32_t last_peak_indx_i;
  float maxpeakval1,maxpeakval2;	  

  last_peak_indx_i=*last_peak_indx;

	  maxpeakval1=0.0;
	  maxpeakval2=0.0;
	  index_max1=-1;
	  index_max2=-1;
	  //found_start_symbol=0;
	  
	  // detect max peak value 
	  for(i=0;i<found_peak;i++){
	    if (list_of_peaks[i].val_max>maxpeakval1 ) {
	      maxpeakval1=list_of_peaks[i].val_max;index_max1=i;
	    }
	  }
	  
	  // detect 2nd max peak value 
	  for(i=0;i<found_peak;i++){
	    if ((i!=index_max1)&&list_of_peaks[i].val_max>maxpeakval2  ) {
	      maxpeakval2=list_of_peaks[i].val_max;index_max2=i;
	    }
	  }
#ifndef EMBED_PERF
	  if (found_peak>=2) printf("2 higher peaks %d (%f) %d (%f)\n", (int) index_max1,maxpeakval1, (int) index_max2,maxpeakval2);
	  else if (found_peak==1) printf("one peak  %d (%f)\n", (int) index_max1,maxpeakval1);
	  else printf("no peak found in this buffer\n");
#endif
	  // if first peak => take the max
	  // else take the max with distance closest to the searched symbol length
	  if (first_peak_found) {
	    // select indexmax as first valid peak, (move to 0), invalidate index_max
	    if(!FIND_FIRST_PEAK_ONLY){
	      list_of_peaks[0].val_max=list_of_peaks[index_max].val_max;
	      list_of_peaks[0].index=list_of_peaks[index_max].index;
	      list_of_peaks[0].smpl_indx=list_of_peaks[index_max].smpl_indx;
	      list_of_peaks[0].val_phase=list_of_peaks[index_max].val_phase;
	      for(i=1;i<found_peak;i++){
		if (list_of_peaks[i].index<=list_of_peaks[0].index)
		  list_of_peaks[i].index=-1;
	      }
	    } else {
	      
	      // case double peak
	      if (found_peak>=2 && (((maxpeakval1-maxpeakval2)/(float)maxpeakval1)<0.2)){
		if (index_max1<index_max2) index_max=index_max1;
		else  index_max=index_max2;
	      } else index_max=index_max1;
	      
	      
	      list_of_peaks[0].val_max=list_of_peaks[index_max].val_max;
	      list_of_peaks[0].index=list_of_peaks[index_max].index;
	      list_of_peaks[0].smpl_indx=list_of_peaks[index_max].smpl_indx;
	      list_of_peaks[0].val_phase=list_of_peaks[index_max].val_phase;
	      last_peak_indx_i=sample_nb;
#ifndef EMBED_PERF
	      printf("\t free running : peak %d  indx %d first_peak  %d\n", (int) i, (int) list_of_peaks[i].index, (int) first_peak_found);
#endif
	      distpeak=nfft+ncp;
	      found_peak=1;
#ifndef EMBED_PERF
	      printf("free running: first peak found\n");
#endif
	      if (list_of_peaks[0].index+nfft+ncp<2*nfft-n1-rad_search_peak_v) {
		// next peak in buffer: force next peak posiiton 
#ifndef EMBED_PERF
		printf("free running: next start is in current buffer\n");
#endif
		list_of_peaks[1].index=list_of_peaks[0].index+nfft+ncp;
		list_of_peaks[1].smpl_indx=list_of_peaks[0].smpl_indx+nfft+ncp;
		list_of_peaks[1].val_max=list_of_peaks[0].val_max;
		list_of_peaks[1].val_phase=list_of_peaks[0].val_phase;
		found_peak++;
		last_peak_indx_i+=nfft+ncp;
	      }
	    }
#ifndef EMBED_PERF
	    printf("first peak found %d\n", (int) list_of_peaks[0].index);
#endif
	  } // first_peak_found
	  
	  // check peaks for qualification against criteria:
	  // estimated symbol length
	  
	  for(i=0;i<found_peak;i++){
#ifndef EMBED_PERF
	    printf(">>>detected peak at %d peakval=%f smplindx=%d phase=%f\n", (int) list_of_peaks[i].index,list_of_peaks[i].val_max, (int) list_of_peaks[i].smpl_indx,list_of_peaks[i].val_phase);
#endif
	    if ((i==0) && (!first_peak_found) &&(list_of_peaks[0].index!=-1)  ) {
	      distpeak=(list_of_peaks[i].index-last_index_value);
#ifndef EMBED_PERF
	      printf("distance btwn first peak and last peak (%d) %d\n", (int) last_index_value, (int) distpeak );
#endif
	    } else if(first_peak_found && (i>0) &&(list_of_peaks[i].index!=-1)) {
	      distpeak=(list_of_peaks[i].index-list_of_peaks[0].index);
#ifndef EMBED_PERF
	      printf("first peak:  distance btwn peak and first peak %d\n", (int) distpeak );
#endif
	    } else if(i==0 && first_peak_found){
	      // force first peak with distance
	      distpeak=nfft+ncp;
	    } else if ((i>0) && (list_of_peaks[i].index!=-1)) {
	      distpeak=(list_of_peaks[i].index-list_of_peaks[i-1].index);
#ifndef EMBED_PERF
	      printf("first peak:  distance btwn peak and first peak %d\n", (int) distpeak );
	      printf("distance between  peaks %d\n", (int) distpeak );
#endif
	    } else if (list_of_peaks[i].index==-1) {}
#ifndef EMBED_PERF
	    else {printf("i %d indx %d first_peak %d\n", (int) i, (int) list_of_peaks[i].index, (int) first_peak_found);assert(0);}
#endif
	    if (distpeak<(nfft+ncp-EPS_DIST)) { // disqualify the peak
#ifndef EMBED_PERF
	      printf("\t\t disqualified\n"); 
#endif
	      list_of_peaks[i].index=-1;
	    }	   
	  }
	  j=0;
	  // remove from list the disqualified peaks
	  for (i=0;i<found_peak;i++){
#ifndef EMBED_PERF
	    printf("%d %d\n", (int) i, (int) found_peak);
#endif
	    if (list_of_peaks[i].index!=-1) {
	      list_of_peaks[j].val_max=list_of_peaks[i].val_max;
	      list_of_peaks[j].val_phase=list_of_peaks[i].val_phase;
	      list_of_peaks[j].index=list_of_peaks[i].index;
	      list_of_peaks[j].smpl_indx=list_of_peaks[i].smpl_indx;
	      j++;
	    }
	  }
	  
	  found_peak=j;
	  
	  // check the list of peaks
#ifndef EMBED_PERF
	  printf("peak dump: left %d peaks in list after selection\n", (int) found_peak);
	  for (i=0;i<found_peak;i++){
	    printf("\t\tpeak dump %d val=%f indx=%d smpl=%d\n", (int) i,list_of_peaks[i].val_max, (int) list_of_peaks[i].index, (int) list_of_peaks[i].smpl_indx);
	  }
#endif
	  
	  // compute how much symbol to process
	  symb_to_process=found_peak;
#ifndef EMBED_PERF
	  printf("for this buffer: \n");
	  printf("\t%d peaks found in buffer\n", (int) symb_to_process);
#endif
	  if (partial_buffer) {
#ifndef EMBED_PERF
	    printf("\tone partial symbol to complete\n"); 
#endif
	    symb_to_process++;
	  }

	  *last_peak_indx=last_peak_indx_i;

} // end of process_peak_list

int8_t fill_in_buffer(struct complex_f *in_dmod_buff,struct complex_f *in_dmod,int8_t *first_peak_found,int32_t backoff_v) {

  int8_t first_peak_found_i;
  int8_t flag_break=0;

  first_peak_found_i=*first_peak_found;

	    assert(list_of_peaks[index_peak].index-backoff_v>=0);
	    
	    if (first_peak_found_i) {
	      // init last_index_val
	      assert(list_of_peaks[0].index!=-1);
#ifndef EMBED_PERF
	      printf("\tfirst peak detected at %d\n", (int) list_of_peaks[0].index);
#endif
	      last_index_value=list_of_peaks[0].index;
	      first_peak_found_i=0;
	    } else {
#ifndef EMBED_PERF
	      printf("\tindex peak %d last_index=%d index=%d detected symbol length= %d\n", (int) index_peak, (int) last_index_value, (int) list_of_peaks[index_peak].index, (int) list_of_peaks[index_peak].index-last_index_value);
#endif
	    }

	    //found_start_symbol=1;		

	    // fill in the data for demodulation in the dmod buffer
	    if(partial_buffer) {
#ifndef EMBED_PERF
	      printf("&&&partial symbol in buffer => copy the remaining samples \n");
#endif
	      memcpy(in_dmod_buff+nb_smples_in_buff,in_dmod+ncp+n1+2*rad_search_peak_v,(nfft+ncp+2*backoff_v-nb_smples_in_buff)*sizeof(struct complex_f));
#ifndef EMBED_PERF
	      printf("\t complete partial buffer with %d samples\n", (int) (nfft+ncp+2*backoff_v-nb_smples_in_buff));
#endif
	      partial_buffer=0;
	      if (index_peak+1==symb_to_process) last_index_value=-((2*nfft+ncp-last_index_value)-(n1+ncp+2*rad_search_peak_v));
	      //else last_index_value=list_of_peaks[0];
	    }
	    else 
	      if ((list_of_peaks[index_peak].index+backoff_v+nfft+ncp<2*nfft+ncp)){
		//if ((list_of_peaks[index_peak].index-backoff_v+nfft+ncp<2*nfft+ncp)){
#ifndef EMBED_PERF
		  printf("&&&full symbol in buffer (lastindex=%d)\n", (int) last_index_value);
#endif
		  memcpy(in_dmod_buff,in_dmod+list_of_peaks[index_peak].index-backoff_v,(nfft+ncp+2*backoff_v)*sizeof(struct complex_f));
		  if (index_peak+1==symb_to_process) last_index_value=-((2*nfft+ncp-list_of_peaks[index_peak].index)-(n1+ncp+2*rad_search_peak_v));
		  else last_index_value=list_of_peaks[index_peak].index;
#ifndef EMBED_PERF
		  printf("\t copy complete symbol to demod buffer last_index=%d\n", (int) last_index_value);
#endif
		  index_peak++;
		}

	      else {
#ifndef EMBED_PERF
		printf("&&& partial copy %d samples for demod, last_index=%d\n", (int) (2*nfft+ncp-list_of_peaks[index_peak].index+backoff_v), (int) list_of_peaks[index_peak].index);
#endif
		// fill in the partial symbol for future demodulation in the dmod buffer
		memcpy(in_dmod_buff,in_dmod+list_of_peaks[index_peak].index-backoff_v,(2*nfft+ncp-last_index_value+backoff_v)*sizeof(struct complex_f));
		//printf("&&& partial copy %d samples for demod\n",(2*nfft+ncp-last_index_value+backoff_v));
		partial_buffer=1;
		nb_smples_in_buff=(2*nfft+ncp-list_of_peaks[index_peak].index+backoff_v);
		last_index_value=-((2*nfft+ncp-list_of_peaks[index_peak].index)-(n1+ncp+2*rad_search_peak_v));
		flag_break=1;
	      }

	    *first_peak_found=first_peak_found_i;
	    return flag_break;

}// end of fill_in_buffer

float compute_papr(struct complex_f *symbol) {

  int32_t j;
  float mod,peak_mod=0.0,peak_r=0.0,peak_i=0.0;
  float av,papr;
  av=0.0;
    for(j=0;j<nfft;j++) {
#ifndef EMBED_PERF
      printf("$$$%d %f %f\n", (int) j,symbol[j].r,symbol[j].i);
#endif
      mod=(symbol[j].r*symbol[j].r+symbol[j].i*symbol[j].i);
      av+=mod;
      if (fabs(symbol[j].r)>peak_r) peak_r=fabs(symbol[j].r);
      if (fabs(symbol[j].i)>peak_i) peak_i=fabs(symbol[j].i);
      if (fabs(mod)>peak_mod) {peak_mod=fabs(mod);}	
    }

    av=av/(float)(nfft);
    papr=10*log10(peak_mod/av);

    return papr;
}

//  select best papr among nsub_sect sub sections
void select_best_papr(struct complex_f *symbol, int32_t *num_section) {

  int32_t i,j,indx;
  float mod,peak_mod=0.0,peak_r=0.0,peak_i=0.0;
  float av,papr,paprmin=(float) BIG_INT;
  int32_t num_sect_min=-1;

  for(i=0;i<nsub_sect;i++) {
    av=0.0;
    peak_mod=0.0;peak_r=0.0;peak_i=0.0;
    for(j=0;j<nfft;j++) {
#ifndef EMBED_PERF
      printf("$$$%d %f %f\n", (int) j,symbol[j+i*nfft].r,symbol[j+i*nfft].i);
#endif
      mod=(symbol[j+i*nfft].r*symbol[j+i*nfft].r+symbol[j+i*nfft].i*symbol[j+i*nfft].i);
      av+=mod;
      if (fabs(symbol[j+i*nfft].r)>peak_r) peak_r=fabs(symbol[j+i*nfft].r);
      if (fabs(symbol[j+i*nfft].i)>peak_i) peak_i=fabs(symbol[j+i*nfft].i);
      if (fabs(mod)>peak_mod) {peak_mod=fabs(mod);indx=j;}	
    }
    av=av/(float)(nfft);
    papr=10*log10(peak_mod/av);

    if (papr<paprmin) {paprmin=papr;num_sect_min=i;}
#ifndef EMBED_PERF
    printf("for section %d papr=%f min=%f peak=%f indx=%d\n", (int) i,papr,paprmin, (int) peak_mod,indx);
#endif
  }
#ifndef EMBED_PERF
  printf("found_sectmin=%d\n", (int) num_sect_min);
#endif

  *num_section=num_sect_min;

}// end of select_best_papr 

// log the result in the paprtaable
void histo_log_papr(float *histo,float val) {

  
}// end of histo_log_papr

void histo_log(float histo[MAX_HISTO_BINS][2],float val,int32_t *bin_nbi) {

  int32_t i;
  int32_t bin_nb;
  
  bin_nb=*bin_nbi;

  assert(bin_nb<MAX_HISTO_BINS);

  for (i=0;i<bin_nb;i++) {
    if ((val>(histo[i][0]*0.9)) && (val<(histo[i][0]*1.1))) {
      histo[i][0]=(histo[i][1]*histo[i][0]+val)/(histo[i][1]+1.0);histo[i][1]=histo[i][1]+1.0;    
#ifndef EMBED_PERF
      printf("i %d val %f av %f\n", (int) i,histo[i][0],histo[i][1]);
#endif
      break;}
  }
  // create a new bin
  if (i>=bin_nb) {
#ifndef EMBED_PERF
    printf("created bin %d\n", (int) bin_nb);
#endif
    histo[bin_nb][0]=val;histo[bin_nb][1]=1.0;bin_nb=bin_nb+1;
  }

  *bin_nbi=bin_nb;

}// end of histo_log

void merge_bins(float histo[MAX_HISTO_BINS][2],int32_t *bin_nbi){

  int32_t i,j;
  int32_t bin_nb;
  
  bin_nb=*bin_nbi;

  assert(bin_nb<MAX_HISTO_BINS);

  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for(j=i+1;j<bin_nb;j++) {
      if (histo[j][1]==-1.0) continue;
      if (((histo[i][0]<histo[j][0]) && (1.1*histo[i][0]>=0.9*histo[j][0])) || ((histo[i][0]>histo[j][0])&& (0.9*histo[i][0]<=1.1*histo[j][0]))) {
#ifndef EMBED_PERF
	printf("merge %d %d %f %f\n", (int) i, (int) j,histo[i][0],histo[j][0]);
#endif
	// merge bins
	histo[i][0]=(histo[i][0]*histo[i][1]+histo[j][0]*histo[j][1])/(histo[i][1]+histo[j][1]);
	histo[i][1]+=histo[j][1];histo[j][1]=-1.0;
      }
    }
  }

  *bin_nbi=bin_nb;

} // end of merge_bins

int32_t get_best_bins(float histo[MAX_HISTO_BINS][2],int32_t *bin_nbi, float *av1,int32_t *n01,  float *av2, int32_t *n02){
  int32_t i,j,bin_nb,indx1,indx2;
  float max1;

  bin_nb=*bin_nbi;
  max1=histo[0][1];indx1=0;

  // get max1
  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }

  indx2=-1;
  //get bin closer to the nb of symbols
  for (i=0;i<bin_nb;i++) {
    if  (histo[i][1]==-1.0) continue;
    if  ((abs(histo[i][1]-(float)NB_OF_SYMBOLS)<=1.0) && (i!=indx1)){
      indx2=i; break;
    }
  }

#ifndef EMBED_PERF
  printf("get_best_bins: %d %d\n", (int) indx1,  (int) indx2);
#endif
  *av1=histo[indx1][0];
  if (indx2!=-1) *av2=histo[indx2][0]; else return 0;
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];
  return 1;

}// end of get_best_bins

int32_t get_best_bins_2(float histo[MAX_HISTO_BINS][2],int32_t *bin_nbi, float *av1,int32_t *n01,  float *av2, int32_t *n02){
  int32_t i,j,bin_nb,indx1,indx2;
  float max1;

  bin_nb=*bin_nbi;
  max1=histo[0][1];indx1=0;

  // get max1 starting from smaller
  for (i=0;i<bin_nb;i++) {
    if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }

  indx2=-1;
  //get bin closer to the nb of symbols starting from higher
  for (i=bin_nb;i>indx1;i--) {
    if  (histo[i][1]==-1.0) continue;
    if  ((abs(histo[i][1]-(float)NB_OF_SYMBOLS)<=1.0) && (i!=indx1)){
      indx2=i; break;
    }
  }

#ifndef EMBED_PERF
  printf("get_best_bins(2): %d %d\n", (int) indx1,  (int) indx2);
#endif
  *av1=histo[indx1][0];
  if (indx2!=-1) *av2=histo[indx2][0]; else return 0;
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];
  return 1;

}// end of get_best_bins_2

void get_big_bins(float histo[MAX_HISTO_BINS][2],int32_t *bin_nbi, float *av1,int32_t *n01,  float *av2, int32_t *n02) {

  int32_t i,j,bin_nb,indx1,indx2;
  float max1, max2;

  bin_nb=*bin_nbi;

  max1=histo[0][1];indx1=0;

  // get max1
  for (i=0;i<bin_nb;i++) {
    //if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if (histo[j][1]>max1) {max1=histo[j][1];indx1=j;}
    }
  }

  indx2=0;
  if (indx1!=0) {indx2=0;max2=histo[0][1];}
  else max2=0.0;

  // get max2
  for (i=0;i<bin_nb;i++) {
    //if (histo[i][1]==-1.0) continue;
    for (j=i+1;j<bin_nb;j++) {
      if ((histo[j][1]>max2) && (histo[j][1]<max1)) {max2=histo[j][1];indx2=j;}
    }
  }
#ifndef EMBED_PERF
  printf("%d %d\n", (int) indx1,  (int) indx2);
#endif
  *av1=histo[indx1][0];
  *av2=histo[indx2][0];
  *n01=histo[indx1][1];
  *n02=histo[indx2][1];


}// end of get_big_bins

void sort_bins(float histo[MAX_HISTO_BINS][2],int32_t bin_nb) {

  int32_t i,j;
  int32_t indx;
  float min=(float) BIG_INT,tmp;

  for(i=0;i<bin_nb;i++){
    min=(float) histo[i][0];indx=i;
    for(j=i;j<bin_nb;j++){
      if (histo[j][0] < min) {min=histo[j][0];indx=j;}
    }
    tmp=histo[i][0];
    histo[i][0]=histo[indx][0];
    histo[indx][0]=tmp;
    tmp=histo[i][1];
    histo[i][1]=histo[indx][1];
    histo[indx][1]=tmp;
  }

#ifndef EMBED_PERF
  printf("sorted bins\n");
  for(i=0;i<bin_nb;i++) printf("%d %f %f\n", (int) i,histo[i][0],histo[i][1]);
#endif

}// end of sort bins

void init_pn(int8_t *seq,int8_t *initpn, int8_t *pol) {

  int32_t i;
  int8_t x;

  for(i=0;i<L_PNSEQ-1;i++) {
    seq[i]=initpn[i];
#ifndef EMBED_PERF
    printf("*%d",initpn[i]); 
#endif
  }
#ifndef EMBED_PERF
  printf("\n");
#endif

  x=seq[0];
  for(i=1;i<L_PNSEQ-1;i++) {
    if (pol[i]==1) x = (x^seq[i])&0x1;
  }

  seq[L_PNSEQ-1]=x;

#ifndef EMBED_PERF
  if (1) {
    for(i=0;i<L_PNSEQ;i++) printf("#%d", (int) seq[i]);
    printf("\n");
  }
#endif

}// end of init_pn

int8_t step_pn(int8_t *seq,int8_t* pol) {

  int32_t i;
  int8_t x;

  x=seq[0];

  for(i=1;i<L_PNSEQ-1;i++) {
    if (pol[i]==1) x = ((x^seq[i]))&0x1;
  }

  // shift
  for(i=0;i<L_PNSEQ-2;i++) seq[i]=seq[i+1];

  seq[L_PNSEQ-2]=x;

#ifdef TEST_PN
  #ifndef EMBED_PERF
  for(i=0;i<L_PNSEQ-1;i++) printf("%d", (int) seq[i]);
  printf("\n");
  #endif
#endif

  return (x==0)?-1:1;

}// end of step_pn

// compute ccdf using papr histog
int32_t nb_greater_val(int32_t *histog,int32_t val) {

  int32_t i;
  int32_t nb=0;

  for (i=0;i<NB_BINS_HISTO_PAPR;i++) {
    if (i>=val) nb+=histog[i];
    
  }

  return nb;

} // end of nb_greater_val


void encode_bit_symbol(uint8_t *bitstream, uint8_t *bitstreamo, uint8_t *last, uint32_t *indxbs, uint32_t nbitsymb, uint8_t *encstate, uint8_t *bits_left, uint32_t *nbits_left ) {
  // create a bitstream of size nbitsymb with encoded bits from the bitstream

  uint8_t symbols_enc[2];
  int32_t i,l=0,j=0;
  uint32_t lastpos;
  uint8_t mask,bits;
  int8_t nbit_select=8,shift_i;
  int32_t nbits_tot=0;
  uint8_t encstate_i;

  encstate_i=*encstate;

  // bit selector (6 bits selection)
  mask=0x0;

  if (ENC_EN) nbit_select=6; else nbit_select=8;

  for(i=0;i<nbit_select;i++) mask|=(1<<i);

  // pointer on the first data bit in the bitstream
  lastpos=*last;

  // manage the remaining bits
  nbits_tot=*nbits_left;
  shift_i=*nbits_left;
  bitstreamo[0]=*bits_left;
#ifndef EMBED_PERF
  printf("start bit2symbol: bits left %x nbitsleft %d lastpos %d\n",*bits_left, (int) nbits_tot, (int) lastpos);
#endif
  // fill in the bitstream for one symbol
  while(nbits_tot<nbitsymb) {

    // select nbit_select data bits from bitstream
    // lastpos is the position of the first bit to encode in the current byte of the data bitstream
    bits=(bitstream[l]>>lastpos)&mask;
    lastpos+=nbit_select;
    if(lastpos>=SIZEOF_BYTE) {
#ifndef EMBED_PERF
      printf("%x == ",bits);
#endif
      bits|=(bitstream[l+1]<<(SIZEOF_BYTE-(lastpos-nbit_select)));
      lastpos-=SIZEOF_BYTE;
      bits&=mask;
#ifndef EMBED_PERF
      printf("%x %x %x %d\n",bits,bitstream[l+1],bitstream[l+1]<<(SIZEOF_BYTE-(lastpos-nbit_select)), (int) lastpos);
#endif
      l++;
    }
    
    // encode and puncture according to code rate
    if (ENC_EN) {
#ifndef EMBED_PERF
      printf("\tinput enc: %x\n",bits);
#endif
      encstate_i=encode_bits(symbols_enc,bits,nbit_select,encstate_i);
#ifndef EMBED_PERF
      printf("\tinput enc:  sym0=%x sym1=%x \n",symbols_enc[0],symbols_enc[1]);
#endif
      puncture(symbols_enc,symbols_enc+1,&bits);
#ifndef EMBED_PERF
      printf("\tinput enc: bits=%x sym0=%x sym1=%x state=%x\n",bits,symbols_enc[0],symbols_enc[1],encstate_i);
#endif
    }

    //load a byte into the partial bitstream, shift by the initial offset
    bitstreamo[j]|=(bits<<shift_i);
    if (shift_i) bitstreamo[j+1]=bits>>(SIZEOF_BYTE-shift_i);
    nbits_tot+=SIZEOF_BYTE;
    j++;
    *encstate=encstate_i;
#ifndef EMBED_PERF
    printf("encode_bit_sy: j %d bits %x nbits_tot %d \n", (int) j, bits, (int) nbits_tot);
#endif
  }

  // what if nbits_tot>nbit_symb: the last bits will be mapped on the next symbol
  *nbits_left=nbits_tot-nbitsymb;
  mask=0;
  for (i=0;i<*nbits_left;i++) mask|=(1<<i);
  // bits left contains the non mapped bits of the last packet (8 bits)  
  *bits_left=((bits>>(SIZEOF_BYTE-*nbits_left))&mask);
  *last=lastpos;
  *indxbs=l; 
#ifndef EMBED_PERF
  printf("encode_bit_sy: last %d indxbs %d nbits_left %d laspos %d bitsleft %x\n", (int) *last, (int) *indxbs, (int) *nbits_left, (int) lastpos, *bits_left);
  for(i=0;i<j;i++) printf("%x ",bitstreamo[i]);printf("\n");
#endif

} // encode_bit_symbol

#ifdef LINUX// to be ported for EMBED mode once porting the dmod
void detect_weak_sig(FILE *in_dmod_file,
		FILE*ref_tx_file,
		int8_t mode_v) {

  int32_t k,i,j,l;
  int32_t nbits;
  struct complex_f accusymbol[N_MAX];
  int32_t smpl=0;
  struct complex_f delay_line[N_MAX*LENGTH_DEL_LINE];
  int8_t vote_delay_line[N_MAX*LENGTH_DEL_LINE];
  int32_t pdel_line=0;
  int8_t ref_bs[N_MAX];
  int8_t rx_bs[N_MAX];
  float dist;
  int8_t first=1;
  float confid_av_tot=0.0;
  //#define DETECT_WEAK_SIG 1
  float alpha_confid=0.01;
  float max_confid_av=0.0;
  float sigma_av=0.0,sigma_acc,sigma_acc_av=0.0;
  float max_confid_thresh=0.0,last_confid_av=0.0;
  struct complex_f in_dmod[N_MAX+NCP_MAX],in_dmod_buff[N_MAX+NCP_MAX];
  struct complex_f out_fft[N_MAX];
  //fftw_plan p;
  float av_re,av_im,sigma;
  //float distsymb;
  int8_t accuvote[N_MAX*(int32_t)exp2(MAX_NBIT_TONE)+1];
  struct complex_f tx_ref[N_MAX];
  int32_t tot_read=0,symbol_nb=0;
  int32_t time_shift;
  struct complex_f corr_facts[N_MAX];
  uint32_t ndiffnb,ndiff;
  float maxd;
  uint32_t nb_symb=0;
  int32_t timer=0;

#ifndef EMBED_PERF
  fprintf(stderr,"ENTERING WEAK SIG\n");
#endif

  memset(accusymbol,0,sizeof(struct complex_f)*N_MAX);
  memset(delay_line,0,sizeof(struct complex_f)*N_MAX*LENGTH_DEL_LINE);
  memset(vote_delay_line,-1,sizeof(int8_t)*N_MAX*LENGTH_DEL_LINE);
  memset(accuvote,0,sizeof(int8_t)*N_MAX*(int32_t)exp2(MAX_NBIT_TONE)+1);

  if (1) {

    // nb of bits in one frame
    nbits=ndata_tones*NBITSCONST[constel];
    // read the ref once
#ifdef LINUX
    fread(tx_ref,sizeof(struct complex_f),nfft,ref_tx_file);
#endif

    // extract ref bs
    k=0;
    for(i=0,l=0;i<nfft*nsub_sect;i+=nsub_sect,l++) {
      if (symb_struct[i]==DATA_TONE) {
	ref_bs[k]=demap_tone(tx_ref[l],constel,&dist);
#ifndef EMBED_PERF
	printf("%d %f %f\n", (int) ref_bs[k],tx_ref[l].r,tx_ref[l].i);
#endif
	k++;
      }
    }


    //accumulation 
    while(1) {
      if (first) {
	if (skip_csv(FIRST_SAMPLES_SKIPPED,in_dmod_file)!=FIRST_SAMPLES_SKIPPED){

#ifndef EMBED_PERF
	  printf("DMOD: (skip first CSV) cant read  %d samples from file", (int) FIRST_SAMPLES_SKIPPED);
#endif
	  exit(0);
	}
	else {
#ifndef EMBED_PERF
	  printf("skipped %d samples\n", (int) FIRST_SAMPLES_SKIPPED);
#endif
	  sample_nb=rad_search_peak_v+FIRST_SAMPLES_SKIPPED;
	}
	if (fread_csv(in_dmod,nfft+ncp,in_dmod_file)!=(nfft+ncp)) {
#ifndef EMBED_PERF
	  printf("DMOD: cant read any more samples from input %d", (int) nfft+ncp);
#endif
	  break;
	} else {
#ifndef EMBED_PERF
	  if (0)printf("reads %d samples\n", (int) 2*nfft+ncp);
#endif
	  //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft+ncp);
	  tot_read=nfft+ncp;
	}
	
      }
      else {
	if (fread_csv(in_dmod,nfft+ncp,in_dmod_file)!=(nfft+ncp)) {
#ifndef EMBED_PERF
	  printf("DMOD: cant read any more samples from input %d", (int) nfft+ncp);
	  if (symbol_nb==0) fprintf(stderr,"No OFDM sync found in  this stream\n");
#endif
	  break;
	} else  {
#ifndef EMBED_PERF
	  if (0)printf("reads %d samples\n", (int) nfft+ncp);
#endif
	  //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft-n1-2*rad_search_peak_v);
	  tot_read+=nfft+ncp;
	}
      }
      first=0;
#ifndef EMBED_PERF
      printf("ncp=%d\n", (int) ncp);
#endif
      calc_mean_variance(in_dmod,nfft,&av_re,&av_im,&sigma);   
      //substr_offset(in_dmod,av_re,av_im,nfft);
 
      
      // substract older contribution
      for(i=0;i<nfft+ncp;i++) {
#ifndef EMBED_PERF
	printf("accu0 %d %d == %f %f == %f %f == %f %f\n", (int) i, (int) smpl,accusymbol[i].r,accusymbol[i].i,in_dmod[i].r,in_dmod[i].i,delay_line[pdel_line*(nfft+ncp)+i].r,delay_line[pdel_line*(nfft+ncp)+i].i);
#endif
	accusymbol[i].r-=delay_line[pdel_line*(nfft+ncp)+i].r;
	accusymbol[i].i-=delay_line[pdel_line*(nfft+ncp)+i].i;
	delay_line[pdel_line*(nfft+ncp)+i]=in_dmod[i];
	accusymbol[i].r+=delay_line[pdel_line*(nfft+ncp)+i].r;
	accusymbol[i].i+=delay_line[pdel_line*(nfft+ncp)+i].i;
#ifndef EMBED_PERF
	printf("\taccu1 %d %d == %f %f == %f %f == %f %f\n", (int) i, (int) smpl++,accusymbol[i].r,accusymbol[i].i,in_dmod[i].r,in_dmod[i].i,delay_line[pdel_line*(nfft+ncp)+i].r,delay_line[pdel_line*(nfft+ncp)+i].i); 
#endif
      }

      j=0;
      if (0) for(i=0;i<(nfft+ncp)*LENGTH_DEL_LINE;i++) {
#ifndef EMBED_PERF
	  printf("dell %d %d == %f %f \n", (int) i, (int) j/nfft,delay_line[i].r,delay_line[i].i);
#endif
	  j++;
	}
      // store in_dmod in delay line
      //memcpy(delay_line+pdel_line*(nfft+ncp),in_dmod,(nfft+ncp)*sizeof(struct complex_f));
      
      // add most recent contributor
      if (0) for(i=0;i<nfft+ncp;i++) {
	  accusymbol[i].r+=delay_line[pdel_line*(nfft+ncp)+i].r;
	  accusymbol[i].i+=delay_line[pdel_line*(nfft+ncp)+i].i;
#ifndef EMBED_PERF
	  printf("accu1 %d == %f %f == %f %f\n", (int) i,accusymbol[i].r,accusymbol[i].i,in_dmod[i].r,in_dmod[i].i);
#endif
	}
      
      calc_mean_variance(in_dmod,nfft,&av_re,&av_im,&sigma);   
#ifndef EMBED_PERF
      printf("sigma input=%f\n",sigma);
#endif
      // average input signal
      sigma_av+=sigma;
      calc_mean_variance(accusymbol,nfft,&av_re,&av_im,&sigma);   
#ifndef EMBED_PERF
      printf("sigma accu=%f\n",sigma);
#endif

      // instantaneous accu value
      sigma_acc=sigma;

      // average accu value
      sigma_acc_av+=sigma_acc;

#ifndef EMBED_PERF
      printf("index delline=%d\n", (int) pdel_line);
#endif
      
      memcpy(in_dmod_buff,accusymbol,(nfft+ncp)*sizeof(struct complex_f));
      //memcpy(in_dmod_buff,in_dmod,(nfft+ncp)*sizeof(struct complex_f));
      if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)in_dmod_buff,nfft+ncp,1.0);
      
      //p = fftw_plan_dft_1d(nfft, (fftw_complex_f *)(in_dmod_buff+ncp), out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
     fft_code(nfft, in_dmod_buff+ncp, out_fft, FFT_FORWARD, FFT_NO_NORM); 
      //fftw_execute(p);
      
#ifndef EMBED_PERF
       for(i=0;i<ndata_tones;i++) printf("test bs2: %d %d\n", (int) i, (int) ref_bs[i]);
#endif
     
      // forget oldest value
      for(i=0;i<ndata_tones;i++) {
	if (vote_delay_line[i + pdel_line * ndata_tones]>=0) accuvote[i * NB_CONST + vote_delay_line[i + pdel_line * ndata_tones]]--;
#ifndef EMBED_PERF
	printf("vote %d indx %d accu %d\n", (int) i, (int) vote_delay_line[i + pdel_line * ndata_tones], (int) accuvote[i * NB_CONST + vote_delay_line[i + pdel_line * ndata_tones]]);
#endif
      }
      
      //distsymb=dump_tones((struct complex_f *)out_fft,tx_ref,0,corr_facts,1,&ndiffnb,0,&maxd,mode_v,&time_shift,vote_delay_line + pdel_line * ndata_tones);
      dump_tones((struct complex_f *)out_fft,tx_ref,0,corr_facts,1,&ndiffnb,0,&maxd,mode_v,&time_shift,vote_delay_line + pdel_line * ndata_tones);
      
      //  add current bits to voting poll
      for(i=0;i<ndata_tones;i++) {
	accuvote[i * NB_CONST + vote_delay_line[i + pdel_line * ndata_tones]]++;
#ifndef EMBED_PERF
	printf("vote %d indx %d accu %d\n", (int) i, (int) vote_delay_line[i + pdel_line * ndata_tones], (int) accuvote[i * NB_CONST + vote_delay_line[i + pdel_line * ndata_tones]]);
#endif
      }
      
      int32_t max_indx;
      int32_t max_val;
      float min_conf=(float) BIG_INT,max_conf=0.0;
      float confid_av=0.0,confid;
      int8_t ok_vote;
      

      ok_vote=0;
      confid_av=0.0;
      for(i=0;i<ndata_tones;i++) {
	max_val=0;
	max_indx=-1;

	for(j=0;j<NB_CONST;j++) {
	  // max confidence on all the const values
	  if (max_val<accuvote[i * NB_CONST + j ])  {
	    max_val=accuvote[i * NB_CONST + j ];
	    max_indx=j;
	  }

	}
	confid=((float)max_val/(float)LENGTH_DEL_LINE);
	if (0&& confid<CONF_THRESH) 
	  ok_vote=0;

	// average, min max over all datatones
	confid_av+=(float)confid;
	if (min_conf>accuvote[i * NB_CONST + max_indx ])  {
	  min_conf=accuvote[i * NB_CONST + max_indx ];
	}
	if (max_conf<accuvote[i * NB_CONST + max_indx ])  {
	  max_conf=accuvote[i * NB_CONST + max_indx ];
	}

#ifndef EMBED_PERF
	printf("$$$$>%d most voted bit: %d conf=%f (%d)\n", (int) i, (int) max_indx,confid, (int) ref_bs[i]);
#endif
	rx_bs[i]=max_indx;
	
      }

#ifndef EMBED_PERF
      for(i=0;i<ndata_tones;i++) {
	for (j=0;j<NB_CONST;j++) printf("%d ", (int) accuvote[i * NB_CONST + j]);
	printf("\n");
      }
#endif

      confid_av=confid_av/(float)ndata_tones;

      // compute global average confidence
      confid_av_tot=confid_av_tot+alpha_confid*((confid_av)-confid_av_tot);

      // get instantaneous peak value
      if (confid_av>max_confid_av) max_confid_av=confid_av;

      // global max decay => ready to detect next burst 
      if (max_confid_av>last_confid_av) max_confid_thresh=0.98*max_confid_av;

      // timer to activate ok only once per burst
      timer++;
      if ((timer>=NB_OF_SYMBOLS) && (max_confid_av>CONF_THRESH) && (max_confid_av<max_confid_thresh)  ) {ok_vote=1;timer=0;max_confid_thresh=0.0;}


      min_conf=min_conf/(float)LENGTH_DEL_LINE;
      max_conf=max_conf/(float)LENGTH_DEL_LINE;
     
#ifndef EMBED_PERF
      printf("!!!!!%d input av %f accu energy %f average  %f timer=%d\n", (int) nb_symb,sigma_av/(float) nb_symb,sigma_acc,sigma_acc_av/(float) nb_symb, (int) timer);
      printf("conf min %f conf av symb %f conf max %f conf av glob %f\n",min_conf,confid_av,max_conf,confid_av_tot);

      printf("global max %f local max %f\n",max_confid_thresh,max_confid_av);
#endif
      // decay of max_confid
      last_confid_av=max_confid_av;
      max_confid_av=0.98 * max_confid_av;   

      if (ok_vote) {
#ifndef EMBED_PERF
	printf("******OKOKOK %d\n", (int) nb_symb);
	fprintf(stderr,"******OKOKOK %d\n", (int) nb_symb);
#endif

	// compute ber
	ndiff=0;
	for (i=0;i<ndata_tones;i++) 
	  compare_char(rx_bs[i],ref_bs[i],&ndiff);
	  
#ifndef EMBED_PERF
	printf("ndiff %d\n", (int) ndiff);
	fprintf(stderr,"*****************************************************\n");
#endif
	//nbits+=NBITSCONST[constel];
#ifndef EMBED_PERF
	//fprintf(stderr," theta estimate %f noise %f\n",);
#endif
#ifndef EMBED_PERF
	fprintf(stderr,"&&&accu energy %f average  %f\n",sigma_acc,sigma_acc_av/(float) nb_symb);
	printf("&&&accu energy %f average  %f\n",sigma_acc,sigma_acc_av/(float) nb_symb);
	printf("ws mode fr#%d: BER=%f \n", (int) nb_symb,ndiff/(float) nbits);
#endif
	//break;
      } 


      // increment circular pointer
      pdel_line++;
      if (pdel_line>=LENGTH_DEL_LINE) pdel_line=0;
      nb_symb++;
    }
    exit(0);
  }

} // end detect weak_sig
#endif

void build_system_symbol(struct complex_f *syst_symb, uint8_t *bit_syst,int8_t mode_v) {

  uint8_t lastpos_i;
  uint32_t indxbs_i;

  lastpos_i=0;
  indxbs_i=0;

  map_bits_to_tones(bit_syst,syst_symb,&lastpos_i,&indxbs_i,BPSKC,0,symb_struct,pn_seq,mode_v,ndata_tones,0);


} // end of build system symbol

void encode_symbol(int8_t mode_v,
	      uint8_t *inbitstreamc,
	      uint8_t *lastpos,
	      uint32_t *lastbits,
	      uint32_t *indxbs,
	      uint8_t *bits_left,
	      uint8_t *encstate,
	      struct complex_f *out_mod,
	      struct complex_f *in_mod
	      )  {

  int32_t i,j,k,l;
  int32_t lmin,jmin;
  uint32_t indxbs_i,lastbits_i;
  uint8_t lastpos_i;
  uint8_t bits_left_i;
  uint8_t encstate_i;
  struct complex_f in_mod2[N_MAX*M_MAX],out_mod_comb[N_MAX*M_MAX],out_mod2[N_MAX*M_MAX];
  struct complex_f in_mod_native[N_MAX];
  uint8_t bitstream_symb[((N_MAX*MAX_NBIT_TONE)>>3)+1];
  float papr_sec,papr_sec1,papr_min;
  //fftw_complex  out_fft[N_MAX*M_MAX],out_fft2[N_MAX*M_MAX];
  struct complex_f out_fft[N_MAX*M_MAX], out_fft2[N_MAX*M_MAX];
  //fftw_plan p;

  uint8_t lastpos_symb=0;
  uint32_t indxbs_symb=0;


  indxbs_i=*indxbs;
  lastpos_i=*lastpos;
  lastbits_i=*lastbits;
  bits_left_i=*bits_left;
  encstate_i=*encstate;

  assert(mode_v!=GREEN_GREEN);

  if ((mode_v==GREEN_CONV)) {


    memset(bitstream_symb,0,nb_char_in_symbol);
#ifndef EMBED_PERF
    printf("you\n");
#endif
    encode_bit_symbol(inbitstreamc, bitstream_symb, &lastpos_i, &indxbs_i, nb_bits_in_symbol, &encstate_i, &bits_left_i, &lastbits_i);
#ifndef EMBED_PERF
    printf("you\n");
#endif

    map_bits_to_tones_native(bitstream_symb,in_mod_native,&lastpos_symb,&indxbs_symb,constel,0,symb_struct,pn_seq,mode_v,ndata_tones,0);

#ifndef EMBED_PERF
    for (i=0;i<nfft;i++) printf("<<<%d %f %f\n", (int) i,in_mod_native[i].r,in_mod_native[i].i);
#endif

    // loop on subsection
    if (0) for(j=0;j<nsub_sect;j++){ 
      
      
      if (mode_v==GREEN_CONV) {
	// map same bits to all symbol (no index update)
	// take care the char pointer (lastpos) is modified by the function: restore to previous value before call
	
	
	lastpos_symb=0;indxbs_symb=0;
	map_bits_to_tones(bitstream_symb,in_mod+j*nfft,&lastpos_symb,&indxbs_symb,constel,j,symb_struct,pn_seq,mode_v,ndata_tones,0);
	
	lastpos_symb=0;indxbs_symb=0;
	map_bits_to_tones(bitstream_symb ,in_mod2+j*nfft,&lastpos_symb,&indxbs_symb,constel,j,symb_struct,pn_seq2,mode_v,ndata_tones,1);
      }
      
#ifndef EMBED_PERF
      printf("lastpos=%d indx=%d\n", (int) lastpos_i, (int) indxbs_i);
#endif
      
    } // sub section
    
    
    
    // store first symbol
    //if (i==0) memcpy(learn_symbol,in_mod,nfft*sizeof(struct complex_f));
    
#ifndef EMBED_PERF
    for(j=0;j<nfft*nsub_sect;j++) printf("####%d %f %f -- %f %f\n", (int) j, in_mod[j].r,  in_mod[j].i,in_mod2[j].r,  in_mod2[j].i);  
#endif
    
    
    //plot(dumpoutfft,nfft,(struct complex_f *)out_fft);
    if (mode_v==GREEN_CONV){
      for (j=0;j<nsub_sect;j++) {

#ifndef EMBED_PERF
	//printf("1section %d\n",j);
#endif
	pn_seq_symb(nfft, in_mod,in_mod2,in_mod_native,j,mode_v,ndata_tones);

#ifndef EMBED_PERF
	for(k=0;k<nfft;k++) printf("####%d %f %f -- %f %f\n", (int) k, in_mod[k].r,  in_mod[k].i,in_mod2[k].r,  in_mod2[k].i);  
#endif

        fft_code(nfft, in_mod, out_fft + j*nfft, FFT_BACKWARD, FFT_NO_NORM);
	//p = fftw_plan_dft_1d(nfft, (fftw_complex *)(in_mod2+j*nfft), out_fft2+j*nfft, FFTW_BACKWARD, FFTW_ESTIMATE); 
	//fftw_execute(p);
	
	//lastpos_symb=0;indxbs_symb=0;	
	//map_bits_to_tones(bitstream_symb ,in_mod2,&lastpos_symb,&indxbs_symb,constel,j,symb_struct,pn_seq2,mode_v,ndata_tones,1);

        fft_code(nfft, in_mod2, out_fft2 + j*nfft, FFT_BACKWARD, FFT_NO_NORM);
      }
    }
    
    // DC is at middle of spectrum 
    
#ifndef EMBED_PERF
    for(j=0;j<nfft*nsub_sect;j++) {
      printf("1&&&&& %d %f %f\n", (int) j, ((struct complex_f *)out_fft)[j].r,  ((struct complex_f *)out_fft)[j].i);  
      printf("2&&&&& %d %f %f\n", (int) j, ((struct complex_f *)out_fft2)[j].r,  ((struct complex_f *)out_fft2)[j].i);  
    }
#endif
    
    
    if (CENTER_SPECTRUM_0 && (mode_v==GREEN_CONV)) {
      for (j=0;j<nsub_sect;j++) {
	shift_spectrum((struct complex_f *)out_fft+j*nfft,nfft, (float)-1.0/OVSMPL_FACTOR);
	shift_spectrum((struct complex_f *)out_fft2+j*nfft,nfft, (float)-1.0/OVSMPL_FACTOR);
      }
    }      
#ifndef EMBED_PERF
    //for(j=0;j<nfft*nsub_sect;j++) printf("%d %f %f\n",j, ((struct complex_f *)out_fft)[j].r,  ((struct complex_f *)out_fft)[j].i);  
#endif
    
    
    if (mode_v==GREEN_CONV){
      memcpy(out_mod+ncp,out_fft,nfft*nsub_sect*sizeof(struct complex_f));
      memcpy(out_mod2+ncp,out_fft2,nfft*nsub_sect*sizeof(struct complex_f));
    }
    
    for (j=0;j<nsub_sect;j++) {
      papr_sec=compute_papr(out_mod+ncp+j*nfft);
      papr_sec1=compute_papr(out_mod2+ncp+j*nfft);
#ifndef EMBED_PERF
      printf("for subsymbols %d papr=%f papr1=%f\n",(int) j, papr_sec, papr_sec1);
#endif
    }
    
#ifndef EMBED_PERF
    //for(j=0;j<nfft*nsub_sect;j++) {
    //  printf("3&&&&& %d %f %f\n",j, ((struct complex_f *)out_fft)[j].r,  ((struct complex_f *)out_fft)[j].i);  
    //  printf("4&&&&& %d %f %f\n",j, ((struct complex_f *)out_fft)[j].r,  ((struct complex_f *)out_fft)[j].i);  
    //}
#endif
    
    /* 
       note on combining: in case of QPSK tone=+/-a+/-j*a
       combining s1*t1+js2*t2 = (a+j*a)*(s1+js2)
       a*s1-a*s2 + j*( a*s2 + a*s1)
       if s1=s2 => real is 0, if s1=-s2 imag is 0
       same if a-ja or -a+ja
    */
    
    
    
    papr_min=(float) BIG_INT;
    jmin=-1;lmin=-1;
    // select best section and shift to 0
    for(j=0;j<nsub_sect;j++) {
      // combining
      for(l=0;l<nsub_sect;l++) { 
	for(k=0;k<nfft;k++) {
	  out_mod_comb[k].r=out_mod[ncp+k+l*nfft].r+out_mod2[ncp+k+j*nfft].r;
	  out_mod_comb[k].i=out_mod[ncp+k+l*nfft].i+out_mod2[ncp+k+j*nfft].i;
	}
	papr_sec=compute_papr(out_mod_comb);
#ifndef EMBED_PERF
	printf("for subsymbols %d %d papr=%f\n", (int) j, (int) l,papr_sec);
#endif
	if (papr_sec<papr_min) {jmin=j;lmin=l;papr_min=papr_sec;}
      }
    }
    
    // !!!!!force num section
    //lmin=1;
    //jmin=2;
    
#ifndef EMBED_PERF
    printf("selected subsymbols %d %d papr=%f\n",(int) jmin, (int) lmin, papr_min);
#endif
    assert((jmin!=-1) && (lmin!=-1));
    
	// combine the selected subsections => in out_mod_comb
    for(k=0;k<nfft;k++){ 
      out_mod_comb[k+ncp].r=out_mod[ncp+k+lmin*nfft].r+out_mod2[ncp+k+jmin*nfft].r;	
      out_mod_comb[k+ncp].i=out_mod[ncp+k+lmin*nfft].i+out_mod2[ncp+k+jmin*nfft].i;	
    }
    
    papr_sec=compute_papr(out_mod_comb+ncp);
    
    memcpy(out_mod,out_mod_comb,(ncp+nfft)*sizeof(struct complex_f));
#ifndef EMBED_PERF
    printf("###selected subsymbols %d %d papr=%f\n", (int) jmin, (int) lmin, papr_sec);
#endif
    
    //shift_spectrum((struct complex_f *)out_mod+ncp,nfft*nsub_sect,-2.0*(float)num_section/(float)(nfft*nsub_sect));
    
    // copy nfft samples of  good symbol as S0 
    //for(j=0;j<nfft;j++) out_mod[ncp+j]=out_mod[ncp+j+num_section*nfft];
    // reset the rest of symbols : in case we need to use the "terrain essai" below
    //for(j=nfft;j<nfft*nsub_sect;j++)  {out_mod[ncp+j].r=0.0;out_mod[ncp+j].i=0.0;}
    
    // now the section nb is known, write the corresponding nfft interleaved tones to ref file
    // IN CASE OF GOSSIC WHAT IS THE REF IN_MOD ?
    k=0;l=0;
#ifndef EMBED_PERF
    printf("ndata %d\n", (int) ndata_tones);
#endif
    
    if (mode_v==GREEN_CONV) {
      k=0;

      norm((struct complex_f *)out_mod,fft_norm_v);
      for(j=0;j<nfft;j++){
	// copy original (not mult by pn_seq)  set of tones 
	if (symb_struct[j]==DATA_TONE) {
	  in_mod[j].r=in_mod_native[j].r;
	  in_mod[j].i=in_mod_native[j].i;
	  k++;
	} else if (symb_struct[j]==SIG_TONE) {
	  // direct map the side info into ref file (to pass to rx)
	  //in_mod[j].r=in_mod[j+lmin*nfft].r+in_mod2[j+jmin*nfft].r;
	  //in_mod[j].i=in_mod[j+lmin*nfft].i+in_mod2[j+jmin*nfft].i;
	  in_mod[j].r=lmin;
	  in_mod[j].i=jmin;
	}
#ifndef EMBED_PERF
	printf("reftx: %d(%d) %f %f %d pnseq[%d]=%d\n", (int) j, (int) j, in_mod[j].r, in_mod[j].i, (int) symb_struct[j], (int) k, (int) pn_seq[k]);
#endif

      }
      //fwrite(in_mod,sizeof(struct complex_f),nfft,ref_tx_file);
    }
    
    // trial demodulation	
    //if (0) {
    //  shift_spectrum((struct complex_f *)out_mod_comb+ncp,nfft,1);
    ////  p = fftw_plan_dft_1d(nfft, (fftw_complex_f *)out_mod_comb+ncp, out_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    ////  fftw_execute(p);
    //  dump_tones((struct complex_f *)out_fft,in_mod,1,corr_facts,1,&ndiffnb,0,&maxd,mode_v,&time_shift,NULL);
    //  exit(0);	
    //}
    //#########################
    
    // copy CP
    for(j=0;j<ncp;j++) out_mod[j]=out_mod[nfft+j];
#ifndef EMBED_PERF
    if (1) for(j=ncp;j<nfft*nsub_sect+ncp;j++) printf("====%d %f %f\n", (int) j,out_mod[j].r,out_mod[j].i);
#endif
  }// green mod
  else {
    
    memset(bitstream_symb,0,nb_char_in_symbol);
    
    encode_bit_symbol(inbitstreamc, bitstream_symb, &lastpos_i, &indxbs_i, nb_bits_in_symbol, &encstate_i, &bits_left_i, &lastbits_i);
    lastpos_symb=0;indxbs_symb=0;
    //    map_bits_to_tones(bitstream_symb,in_mod,&lastpos_symb,&indxbs_symb,constel,0,symb_struct,pn_seq,mode_v,ndata_tones,0);
    in_mod=in_mod_native;
    pn_seq_symb(nfft, in_mod,in_mod2,in_mod,0,mode_v,0);
    
    
    //if (i==0) memcpy(learn_symbol,in_mod,nfft*sizeof(struct complex_f));
    //for (j=0;j<nfft*nsub_sect;j++) {
    //  fwrite(in_mod+j,sizeof(struct complex_f),1,ref_tx_file);
#ifndef EMBED_PERF
    //  printf("reftx: %d(%d) %f %f\n",j,j/nsub_sect,in_mod[j].r,in_mod[j].i);
#endif
    //}
#ifndef EMBED_PERF
    for(j=0;j<nfft;j++) printf("####1%d %f %f\n", (int) j, in_mod[j].r,  in_mod[j].i);
#endif
    
    //p = fftw_plan_dft_1d(nfft, (fftw_complex *)in_mod, out_fft, FFTW_BACKWARD, FFTW_ESTIMATE); 
    fft_code(nfft, in_mod, out_fft, FFT_BACKWARD, FFT_NO_NORM);
    //fftw_execute(p);
    
    if (CENTER_SPECTRUM_0) shift_spectrum((struct complex_f *)out_fft,nfft,(float)-1.0/OVSMPL_FACTOR);
    
    memcpy(out_mod+ncp,(struct complex_f*)out_fft,nfft*sizeof(struct complex_f));
    memcpy(out_mod,out_mod+nfft,ncp*sizeof(struct complex_f));
    norm((struct complex_f *)out_mod,fft_norm_v);
    
    
  }// conventional
  
  
  // if READ_SAME_BITLOAD = 1 the same portion of bitstream is read for each symbol => all symbols are the same
  if (!READ_SAME_BITLOAD) {
    // now update char pointer for next symbol
    *lastpos=lastpos_i;
    *lastbits=lastbits_i;
    *bits_left=bits_left_i;
    *encstate=encstate_i;
    // update the bitstream index for next symbol
    *inbitstreamc+=indxbs_i;
    *indxbs=indxbs_i;
#ifndef EMBED_PERF
    printf("indxbs_i %d\n", (int) indxbs_i);
#endif
  }

} // end of encode_symbol


void measure_power(int8_t mode_v,int8_t detect_weaksig)
{
  int32_t i;
  int32_t MAX_POW_INDX;
  int32_t MAX_IN_SMPLNB = (1<<24);
  int32_t tot_read=0,bin_nb;
  float max_pow;
  int32_t nsub1,nsub2;
  float av_sub1,av_sub2;
  float min_pow=(float) BIG_INT;
  //float *pow_tab;
  int32_t max_pow_indx;
  float histo[MAX_HISTO_BINS][2];
  static struct complex_f in_dmod[N_MAX*NCP_MAX];
  FILE *in_dmod_file;
  float rx_pow=0.0,av_re,av_im;

  MAX_POW_INDX=((int32_t)floor((MAX_IN_SMPLNB/(float)(nfft+ncp)))+1);
  static float pow_tab[MAX_MAX_POW_INDX];

  tot_read=0;bin_nb=0; max_pow=0.0;

    if(1) {
#ifdef LINUX
      if (mode_v!=CONV_OFDM){
	if ((in_dmod_file=fopen("indmod_green.csv","r"))==NULL) printf("DEMOD: impossible to access the rx green file\n");
      }
      else {
	if ((in_dmod_file=fopen("indmod_conv.csv","r"))==NULL) printf("DEMOD: impossible to access the rx conv file\n");
      }

      // measure power and offset in the input file
      i=0;
      while (fread_csv(in_dmod,(nfft+ncp),in_dmod_file)) {
	rx_pow=0.0;
	calc_mean_variance(in_dmod,nfft+ncp,&av_re,&av_im,&rx_pow);
  #ifndef EMBED_PERF
	if (tot_read>=MAX_IN_SMPLNB) 
	  printf("input file has more than %d smples\n", (int) MAX_IN_SMPLNB);
  #endif
	assert(i<MAX_POW_INDX);
	pow_tab[i++]=rx_pow;
	histo_log(histo,rx_pow,&bin_nb);
	if (rx_pow>max_pow&&rx_pow<MAX_POW_VAL) max_pow=rx_pow;
	if (rx_pow<min_pow) min_pow=rx_pow;
	printf(">>> read %d measured RX power: %f\n", (int) tot_read,rx_pow);
	tot_read+=nfft+ncp;
      }
      fclose(in_dmod_file);
#endif
      max_pow_indx=i;
      printf("%d %d\n", (int) max_pow_indx, (int) MAX_POW_INDX);
      assert(max_pow_indx<=MAX_POW_INDX);
      nsub1=1;nsub2=1;
      av_sub1=min_pow;av_sub2=max_pow;

      if (0) 
	for(i=0;i<max_pow_indx;i++) 
	  if (pow_tab[i]<MAX_POW_VAL) partition(pow_tab[i],&av_sub1,&nsub1,&av_sub2,&nsub2);

#ifndef EMBED_PERF
      // dump histog
      printf("DUMP HISTOG BEFORE MERGE; bin_nb=%d\n", (int) bin_nb);
      for(i=0;i<bin_nb;i++) {
	printf("bin %d av %f nb %f\n", (int) i,histo[i][0],histo[i][1]);
      }
#endif
     // merge bins in histogram
      merge_bins(histo,&bin_nb);


#ifndef EMBED_PERF
      // dump histog
      printf("DUMP HISTOG; bin_nb=%d\n", (int) bin_nb);
      for(i=0;i<bin_nb;i++) {
	printf("bin %d av %f nb %f\n", (int) i,histo[i][0],histo[i][1]);
      }
#endif

      // get the 2 largest for  bins
      sort_bins(histo,bin_nb);
      get_big_bins(histo,&bin_nb,&av_sub1,&nsub1,&av_sub2,&nsub2);
#ifndef EMBED_PERF
      fprintf(stderr,"big bins: av1: %f (%d) av2: %f (%d)\n",av_sub1, (int) nsub1,av_sub2, (int) nsub2);
      printf("big bins: av1:%f (%d) av2: %f (%d)\n",av_sub1, (int) nsub1,av_sub2, (int) nsub2);
      if (abs(av_sub1-av_sub2)<MIN_INPUT_POWER) {
	printf("big bins failed : now try best bins\n");
	if (!detect_weaksig&&(!get_best_bins_2(histo,&bin_nb,&av_sub1,&nsub1,&av_sub2,&nsub2)||(abs(av_sub1-av_sub2)<MIN_INPUT_POWER))) {
	  fprintf(stderr,"give up search for good detectio threshold: big bins/best bins have failed %f %f\n",av_sub1,av_sub2); exit(0); 
	}
      }
      printf("RXpower partitioning: n1=%d av1=%f  n2=%d av2=%f\n", (int) nsub1,av_sub1, (int) nsub2,av_sub2);
#endif

      if (max_pow<MIN_INPUT_POWER) {
#ifndef EMBED_PERF
	printf("!!!! rx power too small (%f): no signal !!!!\n",max_pow);
#endif
	exit(0);
      } else {
	// set the minimum detection level 
	min_input_power_v=0.95*(av_sub1+av_sub2)/2.0;
#ifndef EMBED_PERF
	printf("min level of detection is set to %f\n",min_input_power_v);
#endif
      }
      //Av_Re/=(Float)Tot_Read;
      //Av_im/=(float)tot_read;
#ifndef EMBED_PERF
      printf("measured offset RX: %f %f\n",av_re,av_im);
#endif
    }
}// end of measure power 

#ifdef LINUX
void set_parameters(int32_t argc,int8_t *argv[])
#else
  #ifdef FAKE_EMBED
void set_parameters(int32_t backoff, float gain, int32_t mode, int8_t dmod, int32_t nfft_v,
                    int32_t ncp_v, int32_t null_tones_v, int32_t nsub_sect_v, int8_t constell_v, int8_t ws)
#else
  #ifndef PC
void set_parameters(int32_t backoff, float gain, int32_t mode, int8_t dmod, int32_t nfft_v,
                    int32_t ncp_v, int32_t null_tones_v, int32_t nsub_sect_v, int8_t constell_v, int8_t ws)
  #endif
  #endif
#endif
{
  //int32_t k;
#ifdef LINUX
  int32_t nfft_v=-1, ncp_v=-1, null_tones_v=-1, nsub_sect_v=-1;
  int8_t  constell_v=-1;
  int8_t ws;
  int32_t backoff=0;
  float gain=-1;
  mode_ofdm mode;
  int8_t dmod=-1;
#endif
#ifndef IN_PARAM_PRINTF
  printf("\n");
#endif
  int nfft_sig;

#ifdef LINUX 
 parse_options(argc,argv,&backoff,&gain,&mode,&dmod,&nfft_v,&ncp_v,&null_tones_v,&nsub_sect_v,&constell_v,&ws);
#endif

 // compute the bits per constellation
 calc_nbit_const();


 if(mode!=DEFAULT){
   mode_v=mode;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"signal type specified is %s\n",(mode_v==GREEN_GREEN)?"green SLM":((mode_v==GREEN_CONV)?"conv SLM":"conv"));
#endif
 } else {
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"default modulation is used: %s\n",(mode_v==GREEN_GREEN)?"green SLM":((mode_v==GREEN_CONV)?"conv SLM":"conv"));
#endif
   mode_v=GREEN_CONV;
 }

 if(dmod!=-1){
   if (dmod==0) {
     skip_mod_v=0;
     force_symbol_detect_v=1;
#ifndef DIS_IN_PARAM_PRINTF
     fprintf(stderr,"TX file generation\n");
#endif
   } else {
     skip_mod_v=1;
     force_symbol_detect_v=0;    
#ifndef DIS_IN_PARAM_PRINTF
     fprintf(stderr,"RX file demodulation\n");
#endif
   }
 }
 else
 if(-1 == dmod)
 {
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"action is : %s\n",(SKIP_MOD&&!FORCE_SYMBOL_DETECTOR)?"demodulate rx file":"generate tx file");
#endif
   skip_mod_v=SKIP_MOD;
   force_symbol_detect_v=FORCE_SYMBOL_DETECTOR;
 }
 
 if(gain!=-1){
   tx_gain_v=gain;
#ifndef DIS_IN_PARAM_PRINTF
   if (!skip_mod_v) fprintf(stderr,"TX gain specified: %f\n",tx_gain_v);
#endif
 } else {
   tx_gain_v=TX_GAIN;
#ifndef DIS_IN_PARAM_PRINTF
   if (!skip_mod_v) fprintf(stderr,"default TX gain value used: %f\n",TX_GAIN);
#endif
 }

 if (nfft_v!=-1) {
   nfft=nfft_v*OVSMPL_FACTOR;
  #ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"FFT size is specified %d\n", (int) nfft);
  #endif
 } else {
   nfft=NFFT;
  #ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"FFT size is default %d\n",  (int) NFFT);
  #endif
 }
 
if (ncp_v!=-1) {
   ncp=ncp_v*OVSMPL_FACTOR;
  #ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"cp size is specified %d\n", (int) ncp);
  #endif
 } else {
   ncp=NCP;
  #ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"cp size is default %d\n", (int) NCP);
  #endif
 }

 if((backoff>=-ncp_v) && (backoff<=ncp_v) ){
   backoff_v=backoff;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"backoff specified: %d\n", (int) backoff_v);
#endif
   
 } else {
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"DEFAULT backoff index used: %d\n", (int) DEF_BACKOFF_INDX);
#endif
   backoff_v=DEF_BACKOFF_INDX;
 }

if (null_tones_v!=-1) {
   null_tones=null_tones_v;
#ifndef DIS_IN_PARAM_PRINTF
   if (null_tones_v%2) {fprintf(stderr,"nb of null tone must be even\n");exit(0);}
   fprintf(stderr,"nb of null tones  is specified %d\n", (int) null_tones);
#endif
 } else {
   null_tones=NB_TONES_OFF;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"nb of null tones  is default %d\n", (int) null_tones);
#endif
 }

if (nsub_sect_v!=-1) {
   nsub_sect=nsub_sect_v;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"nb of subsections is specified %d\n", (int) nsub_sect);
#endif
 } else {
   nsub_sect=MSUB_SECT;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"nb of subsections is default %d\n", (int) nsub_sect);
#endif
 }

if (constell_v!=-1) {
  // 0 bpsk, 1 qpsk, 2 qam16
   constel=constell_v;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"cp size is specified %d\n", (int) constel);
#endif
 } else {
   constel=CONSTELL;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"cp size is default %d\n", (int) nsub_sect);
#endif
 }

#ifndef DIS_IN_PARAM_PRINTF
 if (constel==QPSKC) fprintf(stderr,"modulation type is QPSK\n");
 else if (constel==QAM16C)  fprintf(stderr,"modulation type is QAM16\n");
 else  fprintf(stderr,"modulation type is BPSK\n");
#endif

 if (ws) {
   detect_weaksig=1;
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"activate weak signal rx mode\n");
#endif
   assert(ncp_v==0);
 } else detect_weaksig=0;

 float nb_sigtones_float;
 // number of signal tones to code sub section number
 // assuming QPSK modulation for sigtones, 2bits/sigtones: actual nb of sigtones is sigtones/2 for one symbol, but double it for Gossyc (left and right symbols both with sidetones)
 nb_sigtones_float =  log2(nsub_sect);
 nb_sigtones = (int32_t) floor(nb_sigtones_float);
 if (nb_sigtones_float-(float)nb_sigtones) {
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"the number of subsections (%d) is not a power of 2\n", (int) nsub_sect);
#endif
   exit(0);
 }

 nfft_sig=nfft/OVSMPL_FACTOR;

 // assuming QPSK 
 nb_sigtones= (nb_sigtones%2)?2*(nb_sigtones/2+1):nb_sigtones;
#ifndef DIS_IN_PARAM_PRINTF
 //printf("%f %d\n",nb_sigtones_float,nb_sigtones);
#endif
 assert(nb_sigtones<NB_SIGNAL_TONES);

 // set rad_search_peak for sync peak search
 rad_search_peak_v = ((nfft-2*RAD_SEARCH_PEAK)<0)?nfft/4:RAD_SEARCH_PEAK;


 assert(nfft<=N_MAX);
 // for case M=4
 nfft1=nfft/4;
 nfft2=2*nfft1;
 nfft3=3*nfft1;

#ifndef DIS_IN_PARAM_PRINTF
 if (null_tones>=nfft_sig)  {fprintf(stderr,"nb of null tones(%d) should be less than fft size(%d): try other parameters\n", (int) null_tones, (int) nfft); exit(0);}
#endif

if((nfft_sig+1-(null_tones))%PILOT_SPACING) {
#ifndef DIS_IN_PARAM_PRINTF
   printf("pilots should be right after (before) first (last) null tone\n");
#endif
   // compute the closest null tone number to specified in order to fulfill this requirement
   //exit(0);
   null_tones+=((nfft_sig-(null_tones))%PILOT_SPACING);
   null_tones-=1; // extra pilot at the end (nfft-null-tone)/5+1 pilot in frame
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"null tone nb is set to %d \n", (int) null_tones);
#endif
 }

// check DC is not on a pilot
 if (0&&!(((nfft>>1)-(null_tones>>1))%PILOT_SPACING)) {
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"pilot should not be on a DC: select an other value\n"); 
#endif
   exit(0);
 }

// set nsub_sect to 1 when in conventional
 if (mode_v==CONV_OFDM) nsub_sect=1;
#ifndef DIS_IN_PARAM_PRINTF
 else fprintf(stderr,"nb of subsections is specified %d\n", (int) nsub_sect);
#endif

 // null_tones+2 to account for pilots at the tone border (starts and ends w/  apilot)
 SPACE_BW_SIGNAL_TONES= ((int32_t)floor((float)(nfft_sig-(null_tones+2)-2*FIRST_SIGNAL_POS)/(float)(nb_sigtones-1)));

 // describes the symbol structure (pilot,signal,data,null)
 // build a M*N tones OFDM signal, with M subsymbol of N tones interleaved
 build_symbol_struct(mode_v);
 ndata_tones=count_tone(symb_struct,DATA_TONE);
#ifndef DIS_IN_PARAM_PRINTF
 fprintf(stderr,"nbdata tones = %d\n", (int) ndata_tones);
#endif
 // list of sig tone indexes
 //k=0;
 //for(i=0,j=0;i<nfft;i+=nsub_sect,j++) if(symb_struct[i]==SIG_TONE)  index_sig[k++]=j;
 
 // parameter consistency
 if((ndata_tones+count_tone(symb_struct,PILOT_TONE)+count_tone(symb_struct,NULL_TONE)+count_tone(symb_struct,SIG_TONE) != nfft)) { 
#ifndef DIS_IN_PARAM_PRINTF
   fprintf(stderr,"!!!!!!!!!check consistency!\n"); 
   fprintf(stderr,"nb  tones carying data %d nb pilots %d(%d) nb_tones_off %d(including DC)\n", (int) ndata_tones, (int) count_tone(symb_struct,PILOT_TONE), (int) count_tone(symb_struct,PILOT_TONE), (int) count_tone(symb_struct,NULL_TONE));
#endif
   exit(0);
 }
 

 // select position of prefix green/conventional
 if (0&&mode_v) 
   n1=nfft1;
 else
   n1=nfft;

 nb_bits_in_symbol_max=NB_BITS_IN_SYMBOL_MAX;
 //nb_char_in_symbol= NB_CHAR_IN_SYMBOL;
 nb_bits_in_symbol=((ndata_tones)*(NBITSCONST[constel]));
 nb_char_in_symbol= ((nb_bits_in_symbol_max>>3)+1);

#ifndef PC
  printf("C: end of set_parameters() backoff_v=%d tx_gain_v=%f mode=%d dmod=%d nfft=%d ncp=%d null_tones=%d nsub_sect=%d constel=%d GEN_RANDOM_BITSTREAM=%d nbr_symbol=%d\n", 
          (int) backoff_v, tx_gain_v, (int) mode, (int) dmod, (int) nfft, (int) ncp, (int) null_tones, (int) nsub_sect, (int) constel, (int) GEN_RANDOM_BITSTREAM, (int)  NB_OF_SYMBOLS);
#endif

}// end of set_parameters

#ifndef LINUX
  #ifdef FAKE_EMBED
     #define PARAM_FILE "parameters.dat"
void gwt_configure_options()
  #else
void gwt_configure_options(int32_t *pt)
  #endif
{
  //TODO manage ws, currently not passed from parameters.dat and only configured at 0
	// in FAKE_EMBED and EMBED
  int32_t gain_bef_pt = -1;
  int32_t gain_aft_pt = -1;
  int32_t tmp_dmod=0, tmp_constell_v=0;
  int32_t mode;
  int32_t null_tones_v;
  uint32_t nbr_bits_symbol_python; //TODO not use to see if to keep
  uint32_t nbr_bytes_tx; //TODO not use to see if to keep
  int8_t dmod=-1;
  int32_t constell_v=-1;
  int32_t nfft_v=-1, ncp_v=-1, ws=0, nsub_sect_v=-1;
  float gain=-1;
  #ifdef PC  
    FILE * fic;
    fic = fopen(PARAM_FILE, "r");
    if (NULL == fic)
    {
      fprintf(stderr, "fail to open %s\n", PARAM_FILE);
      exit(-1);
    }

    int32_t config_buf[14], *pt_buf;
    pt_buf = config_buf;

    fread(pt_buf, sizeof(int32_t), 14, fic);
 
    backoff_v = config_buf[0];
    gain_bef_pt = config_buf[1]; 
    gain_aft_pt = config_buf[2];
    mode = config_buf[3];
    tmp_dmod = config_buf[4];
    nfft_v = config_buf[5];
    ncp_v = config_buf[6];
    null_tones_v = config_buf[7];
    nsub_sect_v = config_buf[8];
    constell_v = config_buf[9];
    //GEN_RANDOM_BITSTREAM = config_buf[10];
    nbr_bits_symbol_python = config_buf[11];
    //if automatic bistream generation then define the number of symbols
    /*if (1 == GEN_RANDOM_BITSTREAM)
    {
      NB_OF_SYMBOLS = 1;
    } else
    {
      NB_OF_SYMBOLS = config_buf[12];
      }*/

    nbr_bytes_tx = config_buf[13];
  #else
  backoff_v = (int32_t) *pt;
  pt += 1;
  gain_bef_pt = (int32_t) *pt;
  gain_aft_pt = (int32_t) *(pt + 1);
  pt += 2;
  mode = (mode_ofdm) *pt;
  pt += 1;
  tmp_dmod = (int32_t) *pt;
  pt += 1;
  nfft_v = (int32_t) *pt;
  pt += 1;
  ncp_v = (int32_t) *pt;
  pt += 1;
  null_tones_v = (int32_t) *pt;
  pt += 1;
  nsub_sect_v = (int32_t) *pt;
  pt += 1;
  tmp_constell_v = (int32_t) *pt;
  constell_v = (int8_t) tmp_constell_v;
  pt += 1;
// to force to have 40 symbols if bistream autogenerate
  //GEN_RANDOM_BITSTREAM = (int32_t) *pt;
  pt += 1;
  nbr_bits_symbol_python = (int32_t) *pt;
  pt += 1;
  /*if (1 == GEN_RANDOM_BITSTREAM)
  {
    NB_OF_SYMBOLS = 40;
  } else
  {
    NB_OF_SYMBOLS = (int32_t) *pt;
  }*/
  //assert(RF_TX_DATA_FRAME_MAX_NBBYTES * 8 >= (nfft + ncp) * nbr_bits_symbol_python * NB_OF_SYMBOLS);

  //NB_OF_SYMBOLS = (int32_t) *pt;
  pt += 1;
  nbr_bytes_tx = (int32_t) *pt;
  #endif
  gain = (float) gain_bef_pt + ((float) gain_aft_pt/100);
  dmod = (int8_t) tmp_dmod;
  printf("C: backoff=%d gain_bef_pt=%d gain_aft_pt=%d mode=%d dmod=%d nfft=%d ncp=%d null_tones_v=%d nsub_sect=%d constell_v=%d GEN_RANDOM_BITSTREAM=%d nbr_bits_symbol=%d nbr_symbol=%d nbr_bytes_tx=%d\n", 
          (int) backoff_v, (int) gain_bef_pt, (int) gain_aft_pt, (int) mode, (int) dmod, (int) nfft_v, (int) ncp_v, (int) null_tones_v, (int) nsub_sect_v, (int) constell_v, (int) GEN_RANDOM_BITSTREAM, (int) nbr_bits_symbol_python, (int)  NB_OF_SYMBOLS, (int) nbr_bytes_tx);
  printf ("gain=%f\n", gain);
  #ifdef PC
  fclose(fic);
  #endif
  //TODO check if the parameter send by python, nbr_bytes_tx has an interest
  //LAST_SYMBS = ((int32_t)floor(0.1 * NB_OF_SYMBOLS));
/*
  if (nfft != -1)
  {
    fprintf(stderr,"FFT size is specified %d\n",nfft);
  } else
  {
    nfft = NFFT;
    fprintf(stderr,"FFT size is default %d\n",NFFT);
  }
 
  if (ncp != -1)
  {
     fprintf(stderr,"cp size is specified %d\n",ncp);
  } else
  {
   ncp = NCP;
   fprintf(stderr,"cp size is default %d\n",NCP);
  }

  if (nsub_sect != -1) {
    fprintf(stderr,"nb of subsections is specified %d\n",nsub_sect);
  } else {
    nsub_sect = MSUB_SECT; 
    fprintf(stderr,"nb of subsections is default %d\n",nsub_sect);
  }
  //fprintf(stderr, "%d %f %d %d %d %d %d %d %d %d\n", backoff_v, gain, mode, dmod, nfft, ncp, null_tones_v, nsub_sect, constel, ws);
*/
  set_parameters(backoff_v, gain, mode, dmod, nfft_v, ncp_v, null_tones_v, nsub_sect_v, constell_v, ws);
}
#endif

#ifndef PC
//structure to return the reference of the output modulation buffer, and number of data it contains
static struct ret_mod ret_m;
#endif

#ifdef PC
int32_t modulator_autotest(int32_t argc, int8_t *argv[]);
#endif

#ifdef PC
int main (int32_t argc, char *argv[]) 
{
#ifdef FAKE_EMBED
  fprintf(stderr, "FAKE_EMBED version");
  gwt_configure_options();
#endif
  modulator_autotest(argc, argv);
}

int32_t modulator_autotest(int32_t argc, int8_t *argv[])

#else
//modulator and perfect demodulator for autotest
struct ret_mod modulator_autotest(int8_t *inbitstream, int32_t len)
#endif
{

  //struct complex_f *in_mod;
  //struct complex_f *in_mod2;
  //struct complex_f *out_mod;
  //struct complex_f *out_mod2;
  //struct complex_f *out_mod_comb;
  //struct complex_f *out_dmod;
  //struct complex_f *in_dmod;
  //struct complex_f *out_mod_comb;
  //struct complex_f *out_dmod;
  //struct complex_f *in_dmod;
  //struct complex_f *in_mod,*in_mod2,*out_mod,*out_mod2,*out_mod_comb,*out_dmod,*in_dmod;
  //struct complex_f *in_dmod_buff;
  //struct complex_f *corr_facts;
  //struct complex_f *corr_facts,*pilots,*pilots_ref;
  static struct complex_f in_mod[N_MAX*M_MAX];
  static struct complex_f in_mod2[N_MAX*M_MAX];
  static struct complex_f in_dmod[2*N_MAX*M_MAX+NCP_MAX];
  static struct complex_f out_mod[(N_MAX*M_MAX+NCP_MAX) * NB_OF_SYMBOLS_MAX];
  //static struct complex_f out_mod2[(N_MAX*M_MAX+NCP_MAX) * NB_OF_SYMBOLS_MAX];
  static struct complex_f out_mod_comb[(N_MAX+NCP_MAX) * NB_OF_SYMBOLS_MAX];
  static struct complex_f out_dmod[(N_MAX*M_MAX+NCP_MAX)];
  static struct complex_f in_dmod_buff[N_MAX*M_MAX+NCP_MAX+MAXBACKOFF-MINBACKOFF];
  static struct complex_f corr_facts[N_MAX];
  //static struct complex_f pilots[NPILOTS];
  //static struct complex_f pilots_ref[NPILOTS];
  static struct complex_f tx_ref_ini[NB_OF_SYMBOLS_MAX * N_MAX * M_MAX * SIZEOF_COMPLEX_F];
  struct complex_f *tx_ref = tx_ref_ini;
  static struct complex_f out_csv_modf[RANDOM_START+N_MAX*M_MAX+NCP_MAX];
  static struct complex_f p_in_buf[(N_MAX + NCP_MAX) * NB_OF_SYMBOLS_MAX];
#ifdef PC
  #ifndef TEST_MATCH_PC_MOD
    static uint8_t inbitstream[BITSTREAM_MAX_SIZE];
  #else
    uint8_t * inbitstream;
  #endif
#endif
  //static uint8_t inbitstream_ref[BITSTREAM_MAX_SIZE];
  uint8_t *inbitstreamc;
  static uint8_t bitstream[BITSTREAM_MAX_SIZE];
  static uint8_t bitstream_ref[BITSTREAM_MAX_SIZE];

#ifdef LINUX
  FILE *file_csv, *berfile;
  FILE *in_bitstr_file,*out_mod_file,*in_dmod_file,*ref_tx_file;
  FILE *dumpout,*dump_papr;
  FILE *histo_file;
#endif

#ifndef PC
  assert(NB_OF_SYMBOLS_MAX >= NB_OF_SYMBOLS);
  //static struct complex_f tx_ref_ini[NB_OF_SYMBOLS_MAX * N_MAX * M_MAX * SIZEOF_COMPLEX_F];
  //struct complex_f *tx_ref = tx_ref_ini;
  struct complex_f preamble_buf[(PREAMBLE + PREAMBLE -1) * OVSMP];
#endif

  //struct complex_f *p_input_buf, *p_in_buf; //output of tx

  //p_in_buf is a pointer to keep the reference of the input buffer beginning
  //struct complex_f *p_input_buf, *p_in_buf; //output of tx
  struct complex_f *p_input_buf = p_in_buf; //output of tx

  /*
  int nfft_v=-1, ncp_v=-1, null_tones_v=-1, nsub_sect_v=-1;
  int8_t  constell_v=-1;
  int8_t ws;

  mode_ofdm mode_v,mode;
  int8_t dmod=-1;
  int8_t skip_mod_v,force_symbol_detect_v;
  */

  int32_t i,j;
  uint32_t index_of_peak, tot_read=0;
#ifndef EMBED_PERF
  uint32_t av_index = 0, nb_symb = 0;
#endif
  int8_t first=1; // flag detection coarse sync is ok
  // buffer to store average of maxima
  int32_t symbol_nb;
  struct complex_f *out_modf,*out_csv;
  struct complex_f *tmp;
  float tx_pow_symb=0.0,peak_r_symb,peak_i_symb,mod,tx_pow_dbm,peak_mod_symb;
  float tx_pow_tot=0.0,peak_r_tot,peak_i_tot,peak_mod_tot;
  int32_t nb_read;
  uint8_t lastpos;
  uint8_t *bitstream_c,*bitstream_ref_c;
  int32_t last_c,indxbs_c,indxbs_ref;
  int8_t last_ref;
  uint32_t last_peak_indx,indxbs_i=0,indx;
 
  //int32_t ncenter;
  int32_t ndiffnb,bckoffmin;
  int8_t flag_iter,first_peak_found=1,found_peak=0;
#ifdef LINUX
  struct complex_f gm, phi;
#endif
  float sigma;
  float av_re,av_im;
  int32_t nchar_stream;

  int8_t pn_reg[L_PNSEQ];
  float papr;
  // 2 pn seqs initialization vector: one is the reverse of the other
  int8_t pn_init2[L_PNSEQ-1]={1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0};
  int8_t pn_init[L_PNSEQ-1]={0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1};
  //polynom is (15,14,0)
  //int8_t pn_pol[L_PNSEQ]= {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
  int8_t pn_pol[L_PNSEQ]= {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1};
  int32_t bin_nb;
  int32_t histo_papr[NB_BINS_HISTO_PAPR];

  int32_t nbits_left;
  uint8_t encstate=0;
  uint32_t lastbits=0;
  uint8_t bits_left=0;
  float I_av=0.0,Q_av=0.0,I_av_s,Q_av_s;
  //int32_t nb_soft_bits=0;

  //fftw_complex *out_fft,*out_fft2;
 // out_fft: M sections of size nfft
  static struct complex_f out_fft[SIZEOF_COMPLEX_F * N_MAX * M_MAX];  

  // gen_met parameters
  int32_t amp = 0;
  float noise;		/* Es/N0 ratio in dB */
  float esn0, ebn0 = 0;

#ifdef LINUX
  #ifndef EMBED_PERF
      fprintf(stderr, "LINUX COMPILATION\n");
  #endif
#else
  #ifndef EMBED_PERF
    fprintf(stderr, "NOT IN LINUX COMPILATION\n");
  #endif
#endif

  memset(pn_reg,0,L_PNSEQ*sizeof(int8_t));
  memset(pn_seq,0,M_MAX*N_MAX*sizeof(int8_t));
  memset(pn_seq2,0,M_MAX*N_MAX*sizeof(int8_t));
  memset(histo_papr,0.0,NB_BINS_HISTO_PAPR*sizeof(int32_t));
 

#ifdef LINUX
  #ifdef INTERP_FIR
FILE *fcoeff;
  if(!(fcoeff=fopen("FIRx8_Coeffs","r"))) {
    printf("INTERP mode and NO COEFF FILE=> exit\n");
    exit(0);
  }
  indx_fir=0;
  memset(delline1,0,NMAX*sizeof(float));
  memset(delline2,0,NMAX*sizeof(float));
  // read coeff file
  i=0;
  while (1) {
    if (fscanf(fcoeff,"%f",coeff+i)!=1) break;
    printf("coeff[%d] %f\n",i,coeff[i]);
    i++;
  }
  FIR_ORDER=i;
  fclose(fcoeff);
  assert(FIR_ORDER<=NMAX);
  #endif
#endif
#ifndef EMBED_PERF
//printf("%f %d %f %d %f %d\n",3.5,(int) floor(3.5+0.PILOT_SPACING),-3.6,(int) floor(-3.6+0.5),3.6,(int)floor(3.6+0.5));
#endif
// exit(0);
 // test PN seq

#ifdef TEST_PN
 init_pn(pn_reg,pn_init,pn_pol);
 for (i=0;i<20;i++) step_pn(pn_reg,pn_pol);
 exit(0);
#endif
 
 /* check no more usefull since otne plan is adjusted by build_symb_struct to fit this requirement 
 if(CENTER_SPECTRUM_0&&(ncenter=((nfft/2-ZTONE_RAD_L-1-(null_tones>>1))%PILOT_SPACING))&& (ZTONE_RAD_L!=0) && (ZTONE_RAD_R!=0)) {
   printf("pilots should be rignt before center null tones %d\n",ncenter);exit(0);}
 if(CENTER_SPECTRUM_0&&(ncenter=((nfft/2+ZTONE_RAD_R+1-(null_tones>>1))%PILOT_SPACING))&& (ZTONE_RAD_L!=0) && (ZTONE_RAD_R!=0)) {
   printf("pilots should be rignt after center null tones %d\n",ncenter);exit(0);}
 */

#ifndef FAKE_EMBED
  #ifdef PC
    set_parameters(argc,argv);
  #endif
#endif

 av_re=0.0;av_im=0.0;
 // define FFT_NORM
 fft_norm_v=(1.0/sqrt((float)nfft));

 // allocate correlation scores
 static float phi_sum[2 * N_MAX + NCP_MAX - N_MAX + RAD_SEARCH_PEAK]; 

 tx_ref = tx_ref_ini;

 // allocate buffers for zync implementation
 int32_t size_input_buf = (N_MAX + NCP_MAX) * NB_OF_SYMBOLS_MAX;//p_input_buf
 memset(out_dmod,0,nfft*nsub_sect*sizeof(struct complex_f));
 
 if(DUMP_CSV) 
    out_csv = out_csv_modf;
 
 if (DUMP_FLOAT_TX||READ_FLOAT||DUMP_CSV)
    out_modf = out_csv_modf;
 

 // open the ber report file for demo data
#ifdef LINUX
 if(mode_v==CONV_OFDM) {
   berfile=fopen("berfile_conv","w");
   fprintf(berfile,"OK,");
 } else {
   berfile=fopen("berfile_green","w");
   fprintf(berfile,"OK,");
 }
#endif

 //PN seq generation
#ifdef LINUX
    FILE *in_pnseq_file;
    int8_t inchar;
    float  inbit;
   
    if (READ_PN_FROM_FILE) {
      printf("read pn seq from file\n");
      in_pnseq_file=fopen("PNseq.txt","r");
      for (i=0;i<nfft*nsub_sect;i++) {
	inchar=0x0;
	fscanf(in_pnseq_file,"%f",&inbit);
	if (inbit==1.0) 
	  inchar=1;
	else inchar=-1;
	pn_seq[i]=inchar;
	printf("%d %x\n", (int) i,inchar);
      }   
      fclose(in_pnseq_file);
    }
#endif

    if (!READ_PN_FROM_FILE) {
      // create the PN seq for 1st seq
#ifndef EMBED_PERF
      printf("generate the 2 pn sequences\n");
#endif
      init_pn(pn_reg,pn_init,pn_pol);
      for(i=0;i<L_PNSEQ-1;i++)
      {
        pn_seq[i]=(pn_init[i])?1:-1;
#ifndef EMBED_PERF
        printf("%d", (int) pn_seq[i]);
#endif
      }
      for(i=L_PNSEQ-1;i<nfft*nsub_sect;i++) {
	pn_seq[i]=step_pn(pn_reg,pn_pol);
#ifndef EMBED_PERF
	printf("%d", (int) pn_seq[i]);
	if (i&&(!(i%ndata_tones))) printf("\n");
#endif
      }
#ifndef EMBED_PERF
      printf("\n");
#endif
      // 2nd seq
      init_pn(pn_reg,pn_init2,pn_pol);
      for(i=0;i<L_PNSEQ-1;i++)
      {
        pn_seq2[i]=(pn_init2[i])?1:-1;
#ifndef EMBED_PERF
        printf("%d", (int) pn_seq2[i]);
#endif
      }
      for(i=L_PNSEQ-1;i<nfft*nsub_sect;i++) {
	pn_seq2[i]=step_pn(pn_reg,pn_pol);
#ifndef EMBED_PERF
	printf("%d", (int) pn_seq2[i]);
	if (i&&(!(i%ndata_tones))) printf("\n");
#endif
      }
#ifndef EMBED_PERF
      printf("\n");
#endif
    }
    
    build_pilot_seq(symb_struct,mode_v,pn_seq);
#ifndef EMBED_PERF
    for(i=0;i<NPILOTS;i++) printf("$$$$$> %f %f\n",pilot_seq[i].r,pilot_seq[i].i); 
    printf("skip_mod %d\n", (int) skip_mod_v);
#endif

    if (!skip_mod_v)
    {
#ifdef LINUX
      dumpout=fopen("outmod.dat","w");
      ref_tx_file=fopen("ref_tx","w");
#endif  
      if (DUMP_CSV)
      {
#ifdef LINUX
	if (mode_v!=CONV_OFDM) file_csv=fopen("outmod_green.csv","w");
	else file_csv=fopen("outmod_conv.csv","w");
#endif
      }

#ifdef GEN_PREAMBLE
      {
  #ifndef EMBED_PERF
        fprintf(stderr,"!!!! PREAMBLE GENERATION ACTIVATED\n");
        printf("preamble generation\n");
  #endif
        struct complex_f short_preamb_freq[BASIC_PRE*OVSMP];
        struct complex_f long_preamb_freq[BASIC_PRE*OVSMP];
        //fftw_complex short_preamb[161*OVSMP];
        //fftw_complex long_preamb[161*OVSMP];
        struct complex_f preamble[(PREAMBLE+PREAMBLE-1)*OVSMP];
        // preamble generation (like 802.11)
        //preamble=(struct complex_f*)malloc((161+161-1)*sizeof(struct complex_f));
        preamble_ifft(short_preamb_freq,long_preamb_freq,short_preamb,long_preamb,preamble);
        for (i=0;i<(PREAMBLE+PREAMBLE-1)*OVSMP;i++) gen_fixed_point_sym(preamble+i,preamble+i);
  #ifdef LINUX
        dump_csv(file_csv,(PREAMBLE+PREAMBLE-1)*OVSMP,preamble);
  #else
	p_in_buf = dump_csv_buff(p_in_buf, (PREAMBLE+PREAMBLE-1) * OVSMP, preamble);
  #endif
      }
#endif
#ifdef LINUX
     if (mode_v!=CONV_OFDM) out_mod_file = fopen("output_mod_green","w");
     else out_mod_file = fopen("output_mod_conv","w");
#endif
     // generate a (random) sequence for first symbol (to test the coarse sync) 
     assert(RANDOM_START<(nfft+ncp));
     for(i=0;i<RANDOM_START;i++)
     {
       out_mod[i].r = ((float)rand()/(float)(RAND_MAX)) * SCALE;
       out_mod[i].i = ((float)rand()/(float)(RAND_MAX)) * SCALE;
#ifndef EMBED_PERF
       if (0) printf(";;; %d %f %f\n", (int) i,out_mod[i].r,out_mod[i].i);
#endif
     }
#ifdef LINUX
    fwrite(out_mod,sizeof(struct complex_f),RANDOM_START,out_mod_file);
#endif
    //plot(dumpout,RANDOM_START,out_mod);
  

    if (GEN_RANDOM_BITSTREAM)
    {
      uint8_t data_to_enc;
      unsigned long rnd;
      int8_t nb_bytes;
      //int8_t nb_bits;
#ifdef LINUX
      FILE *fbso;
#endif

      // generation random bitstream
#ifndef EMBED_PERF
      printf("modulation w/ generated random data\n");
#endif
      // factor 2 provision for ecc rate 1/2 
      nchar_stream=2*nb_char_in_symbol*NB_OF_SYMBOLS*sizeof(int8_t);

      // 48 bits are considered 6*8 bits when no ECC or 8x6bis in ECC
      if (0)
      {
	nb_bytes=8;
      } else {
	nb_bytes=6;
      }
      // to have a nchar_stream multimple of nb_byte
      if (nchar_stream%nb_bytes) nchar_stream+=nb_bytes-(nchar_stream%nb_bytes);
#ifndef EMBED_PERF
      printf("allocate %d char in bitstream (modulo 48 =%d)\n", (int) nchar_stream, (int) nchar_stream%48);
#endif
#ifdef LINUX
      fbso=fopen("fbso_out","w");
#endif
#ifndef TEST_MATCH_PC_MOD
      for(i=0;i<nchar_stream;i+=nb_bytes) {

	for(j=0;j<nb_bytes;j++) {
	  rnd=lrand48();
	  data_to_enc=(rnd)&0xff;

	  inbitstream[i+j]=data_to_enc;
	}
  #ifndef EMBED_PERF
	if (1) printf("rnd %d 0x%x 0x%x 0x%x 0x%x 0X%x 0X%x\n", (int) i,inbitstream[i],inbitstream[i+1],inbitstream[i+2],inbitstream[i+3],inbitstream[i+4],inbitstream[i+5]);
  #endif
  #ifdef LINUX
	fprintf(fbso,"%x %x %x %x %x %x\n",inbitstream[i]&0xff,inbitstream[i+1]&0xff,inbitstream[i+2]&0xff,inbitstream[i+3]&0xff,inbitstream[i+4]&0xff,inbitstream[i+5]&0xff);
  #endif
      } 
  #ifdef LINUX
      fclose(fbso);
  #endif
#else
      inbitstream = golden_gen_bitstream;
      assert(nchar_stream <= NB_GOLDEN_BYTES);
      assert(6 == nb_bytes);
      #ifndef EMBED_PERF
      for(j=0; j < nchar_stream; j+=nb_bytes)
      {
        printf("rnd %d 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", (int) j, inbitstream[j],
              inbitstream[j+1], inbitstream[j+2], inbitstream[j+3],
              inbitstream[j+4], inbitstream[j+5]);
      }
      #endif
#endif
    }

#ifdef LINUX    
    if (READ_INPUT_FROM_FILE)
    {
      nchar_stream=nb_char_in_symbol*NB_OF_SYMBOLS*sizeof(int8_t);
      printf("modulation w/ random data from file\n");
      printf("allocate %d char in bitstream\n", (int) nchar_stream);
      if ((in_bitstr_file=fopen("binaries.txt","r"))==NULL)
      {
	printf("no input bitstream file binaries.txt found: exiting\n");
	exit(0);
      }
      for (i=0;i<nchar_stream;i++)
      {
	inchar=0x0;
	for (j=0;j<8;j++)
        {
	  fscanf(in_bitstr_file,"%f",&inbit);
	  if (inbit==1.0) 
	    inchar|=1<<j;
	  else inchar|=0<<j;
	}
	inbitstream[i]=inchar;
	//printf("%d %x ", (int) i,inchar);
      }
      fclose(in_bitstr_file);
      if (0&&fread(inbitstream,sizeof(int8_t),nchar_stream,in_bitstr_file)!=nchar_stream)
      {
	fprintf(stderr,"not enough data in input nitstream (%d requested)\n", (int) nchar_stream);
	exit(0);
      }
    }
#endif

    // metric table generation for viterbi decoder (!! to be reviewed)
    /* Compute noise voltage. The 0.5 factor accounts for BPSK seeing
   * only half the noise power, and the sqrt() converts power to
   * voltage.
   */
    // ebn0 Eb/N0 in db => from the Rx input power measurement
   esn0 = ebn0 - 10*log10((float)Rate);	/* Es/N0 in dB */	
   noise = sqrt(0.5/pow(10.,esn0/10.));
   //TODO uncomment as commented only to avoif compile error// gen_met(mettab,amp,noise,0.0,256);

    memset(in_mod,0,nfft*nsub_sect*sizeof(struct complex_f));
    memset(in_mod2,0,nfft*nsub_sect*sizeof(struct complex_f));

//label to simulate successive frames
algo_start: ;//add the ; to compile the code as Prior to C99, all declarations had to precede all statements within a block, so it wouldn't have made sense to have a label on a declaration.
    // init parameters for bitstream indexing
    indxbs_i=0;
    lastpos=0;
    inbitstreamc=inbitstream;

    // count data tones
    //for(i=0;i<nfft*nsub_sect;i+=nsub_sect)
    //  if (symb_struct[i]==DATA_TONE) data_tone_nb++;
#ifndef EMBED_PERF
    printf("nb of data tones: %d\n", (int) ndata_tones);
#endif
    if (TRY_WAVE) {
      // performs FFT by inversing thec imaginary
      for(i=0;i<nfft;i++) {
	in_mod[i].r=cos(2.0*PI*i/(float)nfft);
	in_mod[i].i=-sin(2.0*PI*i/(float)nfft);
      }
    }

    // init frame peak values
    peak_r_tot=0.0;
    peak_i_tot=0.0;
    peak_mod_tot=0.0;
    bits_left=0;
    encstate=0;

#ifndef EMBED_PERF
    if (ENC_EN) fprintf(stderr,"FEC ACTIVATED\n");
#endif

    // generate preamble for analyzer (first symbol)
#ifdef LINUX
    FILE *preamble_file;
    if (0) {
      preamble_file=fopen("preamble","w");
      //p = fftw_plan_dft_1d(nfft, (fftw_complex *)(out_mod_comb+ncp), out_fft, FFTW_FORWARD, FFTW_ESTIMATE); 
      fft_code(nfft, out_mod_comb + ncp, out_fft, FFT_FORWARD, FFT_NO_NORM);
      //fftw_execute(p);	
      for (j=0;j<nfft;j++) fprintf(preamble_file,"%1.3f %1.3f ",((struct complex_f *)out_fft)[j].r/((float)nfft),((struct complex_f *)out_fft)[j].i/((float)nfft));
      fclose(preamble_file);
    }
#endif

    for (i=0;i<NB_OF_SYMBOLS;i++)
    {
#ifndef EMBED_PERF
      printf("tx symbol %d  mode (%d)%s\n", (int) i, (int) mode_v,(mode_v==GREEN_GREEN)?"green SLM":((mode_v==GREEN_CONV)?"conv SLM":"conv"));
      printf("inbitstreamc=0x%x &lastpos=0x%p &lastbits=0x%p &indxbs_i=0x%p &bits_left=0x%p, &encstate=0x%p, out_mod=0x%p in_mod=0x%p\n", 		    inbitstreamc, &lastpos, &lastbits, &indxbs_i, &bits_left, &encstate, out_mod, in_mod);
      printf("inbitstreamc=0x%x lastpos=%d lastbits=%d indxbs_i=%d bits_left=%d, encstate=%d, out_mod=0x%p in_mod=0x%p\n", inbitstreamc, (int) lastpos, (int) lastbits, (int) indxbs_i, (int) bits_left, (int) encstate, out_mod, in_mod);
#endif
/*void encode_symbol(int8_t mode_v,
	      uint8_t *inbitstreamc,
	      uint8_t *lastpos,
	      uint32_t *lastbits,
	      uint32_t *indxbs,
	      uint8_t *bits_left,
	      uint8_t *encstate,
	      struct complex_f *out_mod,
	      struct complex_f *in_mod
	      )  {*/


      encode_symbol(mode_v,
		    inbitstreamc,
		    &lastpos,
		    &lastbits,
		    &indxbs_i,
		    &bits_left,
		    &encstate,
		    out_mod,
		    in_mod
		    );

      if (!READ_SAME_BITLOAD) inbitstreamc+=indxbs_i;

      // save in ref file
      if (mode_v==GREEN_CONV)
      { 
#ifdef LINUX
	fwrite(in_mod,sizeof(struct complex_f),nfft,ref_tx_file);
#else
	uint32_t cnt_cp;
	for(cnt_cp=0; cnt_cp < nfft; cnt_cp++)
        {
	  tx_ref[cnt_cp].r = in_mod[cnt_cp].r;
	  tx_ref[cnt_cp].i = in_mod[cnt_cp].i;
        }
	tx_ref += nfft; 
#endif
      }
      else
      {
#ifdef LINUX
	for (j=0;j<nfft*nsub_sect;j++) {
	  fwrite(in_mod+j,sizeof(struct complex_f),1,ref_tx_file);
	  printf("reftx: %d(%d) %f %f\n", (int) j, (int) j/nsub_sect,in_mod[j].r,in_mod[j].i);
	}
#else
	uint32_t cnt_cp2;
	for(cnt_cp2=0; cnt_cp2 < nfft * nsub_sect; cnt_cp2++)
        {
	  tx_ref[cnt_cp2].r = in_mod[cnt_cp2 + j].r;
	  tx_ref[cnt_cp2].i = in_mod[cnt_cp2 + j].i;
        }
	tx_ref += nfft * nsub_sect; 
#endif
      }

      substr_offset(out_mod,-OFF_RE,-OFF_IM,nfft+ncp);

      // apply scaling gain
      norm((struct complex_f *)out_mod,tx_gain_v);
      // fixed point transform and store into buffer
      tmp=(struct complex_f *)out_mod;
      for(j=0;j<nfft+ncp;j++) {
	out_modf[j].r=(float) tmp[j].r;
	//printf("%f \n",out_modf[j].r);
	out_modf[j].i=(float) tmp[j].i;
	gen_fixed_point_sym(out_modf+j,out_csv+j);
      }

      p_input_buf = dump_csv_buff(p_input_buf, nfft + ncp, out_csv);
    
      // compute average Tx power for this symbol
      tx_pow_symb=0.0;
      calc_mean_variance(out_mod,nfft,&av_re,&av_im,&sigma);
      peak_r_symb=0.0;
      peak_i_symb=0.0;
      peak_mod_symb=0.0;

      // average on symbol only (not include the CP), because it would modify the average values
      for(j=0;j<nfft;j++) {
	mod=(out_mod[j+ncp].r*out_mod[j+ncp].r+out_mod[j+ncp].i*out_mod[j+ncp].i);
	if (fabs(out_mod[j+ncp].r)>peak_r_symb) peak_r_symb=fabs(out_mod[j+ncp].r);
	if (fabs(out_mod[j+ncp].i)>peak_i_symb) peak_i_symb=fabs(out_mod[j+ncp].i);
	if (fabs(mod)>peak_mod_symb) {peak_mod_symb=fabs(mod);indx=j;}	
	tx_pow_symb+=mod;
      }

      //if (i==0) {plot(dumpout,nfft,out_mod);exit(0);}

      // total power and peak values on frame
      tx_pow_tot+=tx_pow_symb;
      if(peak_r_symb>peak_r_tot) peak_r_tot=peak_r_symb;
      if(peak_i_symb>peak_i_tot) peak_i_tot=peak_i_symb;
      if(peak_mod_symb>peak_mod_tot) peak_mod_tot=peak_mod_symb;


      tx_pow_symb=tx_pow_symb/(float) (nfft); 

      papr= 10.0*log10(peak_mod_symb/tx_pow_symb);

      //printf("you %f %f\n",papr,tx_pow_symb);

      // quantize symbol papr to nb of bins
      //assert((papr>=MIN_PAPR) && (papr<=MAX_PAPR));
      bin_nb= (int32_t) floor((papr-(float)MIN_PAPR)/BIN_WIDTH);
      histo_papr[bin_nb]++;
      
#ifndef EMBED_PERF
      printf("for symbol %d: av %f peakr %f peaki %f peakmod %f indx %d papr=%f\n", (int) i,tx_pow_symb,peak_r_symb,peak_i_symb,peak_mod_symb, (int) indx,papr);
#endif

      if (0)for(j=0;j<ncp;j++) {
	out_mod[j].r = out_mod[j+nfft1].r;
	out_mod[j].i = out_mod[j+nfft1].i; 
      }      

      //assert((DUMP_CSV==0) || (DUMP_FLOAT_TX==0));
      //plot(dumpout,N+ncp,out_mod);
      //plot(dump_in_mod,N,in_mod);
      int32_t nb_clamp=0;
      if (DUMP_FLOAT_TX||DUMP_CSV) {
	// dump output 
	tmp=(struct complex_f *)out_mod;
	I_av_s=0.0;Q_av_s=0.0;
	for(j=0;j<nfft+ncp;j++) {
	  out_modf[j].r=(float) tmp[j].r;
#ifndef EMBED_PERF
	  //printf("%f \n",out_modf[j].r);
#endif
	  out_modf[j].i=(float) tmp[j].i;
	  if (DUMP_CSV)  {
	    gen_fixed_point_sym(out_modf+j,out_csv+j);
	    I_av_s+=out_csv[j].r*out_csv[j].r;
	    Q_av_s+=out_csv[j].i*out_csv[j].i;
	  }
	  if ((abs(out_csv[j].r)>(1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1))) || ((abs(out_csv[j].i)>(1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1))))) {
	    nb_clamp++;
#ifndef EMBED_PERF
	    printf("clamp %f %f\n",out_csv[j].r,out_csv[j].i);
#endif
	  }
	  if ((mode_v==GREEN_CONV) && (j>=(nfft+ncp)-1)) break;
	}

#ifndef EMBED_PERF
	//fprintf(stderr," Iav=%f Qav=%f  %d\n",I_av_s,Q_av_s,n (int) sub_sect);
	printf(" Iav=%f Qav=%f  %d\n",I_av_s,Q_av_s, (int) nsub_sect);
#endif
	I_av_s=I_av_s/(float)(nfft+ncp);
	Q_av_s=Q_av_s/(float)(nfft+ncp);
	I_av_s=0.5*log10(I_av_s)/log10(2);
	Q_av_s=0.5*log10(Q_av_s)/log10(2);
	I_av+=I_av_s;Q_av+=Q_av_s;
#ifndef EMBED_PERF
	if (nb_clamp) fprintf(stderr,"WARNING: %d data will be clamped to %d bits by AFE\n", (int) nb_clamp, (int) (FIXED_POINT_FRAC+FIXED_POINT_INT));
#endif
#ifdef LINUX
	if (DUMP_FLOAT_TX) fwrite(out_modf,sizeof(struct complex_f),nfft+ncp,out_mod_file);
	//if (DUMP_CSV) fwrite(out_csv,sizeof(struct complex_f),nfft+ncp,out_mod_file);
#endif
      }
#ifdef LINUX
      else 
	fwrite(out_mod,sizeof(struct complex_f),nfft+ncp,out_mod_file);
#endif


#ifdef LINUX
      if (DUMP_CSV) dump_csv(file_csv,nfft+ncp,out_csv);

      if (DUMP_FLOAT_TX) plot_cpxf(dumpout,nfft+ncp,out_modf); 
      else plot(dumpout,nfft+ncp,out_mod);
      if (1) for(j=ncp;j<nfft*nsub_sect+ncp;j++) printf("(((())))%d %f %f\n", (int) j,out_mod[j].r,out_mod[j].i);
      // measure the correlation energy(to be used in the receiver) on first symbol
      if (i==0) {
	phi.r=0.0;gm.r=0.0;gm.i=0.0;
	for(j=0;j<ncp;j++){
	  
	  phi.r+=out_mod[n1+j].r*out_mod[n1+j].r+out_mod[n1+j].i*out_mod[n1+j].i+out_mod[j].r*out_mod[j].r+out_mod[j].i*out_mod[j].i;
	  
	  gm.r+=(out_mod[n1+j].r*out_mod[j].r)+(out_mod[n1+j].i*out_mod[j].i);
	  gm.i+=(out_mod[n1+j].r*out_mod[j].i)-(out_mod[n1+j].i*out_mod[j].r);
	}
	phi_sum[0]=sqrt(gm.r*gm.r+gm.i*gm.i)-(snr/(snr+1))*phi.r;   
      }
#else
  #ifndef EMBED_PERF
      printf("==> symb %d out of %d\n", (int) i, (int) NB_OF_SYMBOLS);

    if (0) for(j=ncp;j<nfft*nsub_sect+ncp;j++) printf("@@@@%d %f %f\n", (int) j,out_mod[j].r,out_mod[j].i);
  #endif
  #ifndef EMBED_PERF
      printf("from ncp to nfft*nsub_sect+ncp, out_mod[j].r,out_mod[j].i :\n");
      if (1) for(j=ncp;j<nfft*nsub_sect+ncp;j++) printf("(((())))%d %f %f\n", (int) j,out_mod[j].r,out_mod[j].i);
  #endif
#endif
    }// symbol loop

#ifndef EMBED_PERF
    printf("==> last symbol generated (%d)\n", (int) NB_OF_SYMBOLS);

    // dump histog
    printf("dump papr histog\n");
#endif
#ifdef LINUX
    histo_file=(mode_v==CONV_OFDM)?fopen("histo_file_conv","w"):fopen("histo_file_green","w");
    for(i=0;i<NB_BINS_HISTO_PAPR;i++) fprintf(histo_file,"%f %d\n",MIN_PAPR+i*BIN_WIDTH, (int) histo_papr[i]);
    printf("dump proba values\n");
    
    dump_papr=(mode_v==CONV_OFDM)?fopen("plot_papr_conv","w"):fopen("plot_papr_green","w");
    for(i=0;i<NB_BINS_HISTO_PAPR;i++) fprintf(dump_papr,"%f %f\n",MIN_PAPR+i*BIN_WIDTH,(float) nb_greater_val(histo_papr,i)/(float)NB_OF_SYMBOLS );
    fclose(dump_papr);

    fclose(out_mod_file);
    fclose(dumpout);
    fclose(ref_tx_file);
#endif
    tx_pow_tot = tx_pow_tot/(float)((nfft+ncp)*NB_OF_SYMBOLS);
    I_av = I_av/(float)NB_OF_SYMBOLS;
    Q_av = Q_av/(float)NB_OF_SYMBOLS;
    tx_pow_dbm = 10.0*log10(tx_pow_tot/0.001);
    papr = 10.0*log10(peak_mod_tot/tx_pow_tot);

#ifndef DIS_END_MOD_PRINTF
    fprintf(stderr,"finished modulator\n\n");
    fprintf(stderr,"\t***** power in dbm : %f (sigma=%f) nbit_I %f nbit_Q %f\n",tx_pow_dbm,sigma,I_av,Q_av);
    fprintf(stderr,"\t***** peak value (Re): %f\n",peak_r_tot);
    fprintf(stderr,"\t***** peak value (Im): %f\n",peak_i_tot);
    fprintf(stderr,"\t***** peak module (mod): %f papr=%f\n",sqrt(peak_mod_tot),papr);
    fprintf(stderr,"\t***** CP correlation energy: CP=> %d N=> %d constell=> %s corr energy=> %f", (int) ncp, (int) nfft,(constel==QAM16C)?"QAM16":"QPSK", phi_sum[0]);
    fprintf(stderr,"\n\n");
#endif
 
    //exit(0);
#ifdef LINUX
    if (TRY_WAVE) exit(0);
     if (0) for(j=ncp;j<nfft*nsub_sect+ncp;j++) printf("$$$$%d %f %f\n", (int) j,out_mod[j].r,out_mod[j].i);
#endif
 } // modulator

#ifdef ADD_NULL_TONES
//this is to add the same quantity of null data symbol that there is of data in a fram
//objective is to ease the rx refinement by looping a frame with data alternance with no data
  assert(size_input_buf >= 2 * (nfft + ncp) * NB_OF_SYMBOLS);
  if (size_input_buf >= 2 * (nfft + ncp) * NB_OF_SYMBOLS)
  {
    uint32_t cnt_nl;
    for(cnt_nl=(nfft + ncp) * NB_OF_SYMBOLS; cnt_nl < 2 * (nfft + ncp) * NB_OF_SYMBOLS; cnt_nl++)
    {
  #ifndef PC
      p_in_buf[cnt_nl].r = 0;
      p_in_buf[cnt_nl].i = 0;
  #endif
  #ifdef LINUX
      //only DUMP_CSV mode supported
      if (DUMP_CSV) //dump_csv(file_csv,nfft+ncp,out_csv);
      {
        fprintf(file_csv,"%d, %d\n", (int) 0, (int) 0);
      }
  #endif
    }
  }
  else
  {
  #ifdef EMBED_PERF
    fprintf(stderr, "MODE ADD_NULL_TONES FAILED, TOO MUCH SYMBOLS\n");
  #endif
  }
#endif

#ifdef LINUX
    if (DUMP_CSV) fclose(file_csv);   
#endif

#ifndef EMBED_PERF
 uint32_t dbg_lp;
 for(dbg_lp=0; dbg_lp < (nfft + ncp) * NB_OF_SYMBOLS; dbg_lp++)
 {
   printf("out mod, p_in_buf[%d].r=%f p_in_buf[%d].i=%f\n", (int) dbg_lp, p_in_buf[dbg_lp].r, (int) dbg_lp, p_in_buf[dbg_lp].i);
 }
#endif
  //
  //III) start the demodulator
  //

  uint32_t nbits=0,nbits_ref=0,ndiff=0;
#ifndef EMBED_PERF
  float tot_distsymb = 0;
#endif
 
  av_re=0.0;av_im=0.0;
#ifdef LINUX
  dumpphisum=fopen("phisum.dat","w");
  dumpout=fopen("outmod.dat","w");

  if (mode_v!=CONV_OFDM) 
    dump_in_mod=fopen("inmod_green.dat","w");
  else dump_in_mod=fopen("inmod_conv.dat","w");

  dump_pilots=fopen("pilots.dat","w");
#endif
  // init min backoff index
  if (mode_v) bckoffmin=backoff_v;bckoffmin=0;
  /*  
  // specific to measure power
  int nsub1,nsub2;
  float av_sub1,av_sub2;
  float min_pow=(float) BIG_INT;
  float *pow_tab;
  int max_pow_indx;
  float histo[MAX_HISTO_BINS][2];
  int MAX_POW_INDX;
  int MAX_IN_SMPLNB = (1<<24);

  // declarations specific to detect weak
  float confid_av_tot;
  //#define DETECT_WEAK_SIG 1
  float alpha_confid=0.01;
  float max_confid_av=0.0;
  float sigma_av=0.0,sigma_acc,sigma_acc_av=0.0;
  int timer;
  float max_confid_thresh=0.0,last_confid_av=0.0;
  
  */

    if (MEASURE_RX_POWER&&!force_symbol_detect_v) 
      measure_power(mode_v,detect_weaksig);
#ifdef LINUX
    // open file for demodulation
    if (!force_symbol_detect_v){
      if(mode_v!=CONV_OFDM){
	if ((in_dmod_file=fopen("indmod_green.csv","r"))==NULL) 
	  printf("DEMOD: impossible to access the rx green  file\n");
      }
      else 
	if ((in_dmod_file=fopen("indmod_conv.csv","r"))==NULL) 
	  printf("DEMOD: impossible to access the rx conv file\n");
    }
    else  
      if (READ_CSV){// get the file generated by modulator 
	if (mode_v!=CONV_OFDM) {
	  if ((in_dmod_file=fopen("outmod_green.csv","r"))==NULL) {
	    //    if ((in_dmod_file=fopen("indmod_green_shortened.csv","r"))==NULL) { 
	    printf("DEMOD: impossible to access the input file\n");exit(0);
	  }
	}
	else 
	  {
	    if ((in_dmod_file=fopen("outmod_conv.csv","r"))==NULL) 
	      //if ((in_dmod_file=fopen("rx_10.000000_conv_shortened.csv","r"))==NULL) 
	      //if ((in_dmod_file=fopen("indmod_conv.csv","r"))==NULL) 
	      printf("DEMOD: impossible to access the input file\n");
	    printf("read file outmodcon\n");
	  }
      }
      else 
	if(mode_v==CONV_OFDM)	{
	  if ((in_dmod_file=fopen("output_mod_conv","r"))==NULL) 
	    printf("DEMOD: impossible to access the input file\n");}
	else  {
	  if ((in_dmod_file=fopen("output_mod_green","r"))==NULL) 
	    printf("DEMOD: impossible to access the input file\n");}

    // open tx tones reference file
    if ((ref_tx_file=fopen("ref_tx","r"))==NULL) {printf("DMOD: cant open ref tx file\n");exit(0);}
#endif
    // init the reference tables for autocorrelation values
    init_autocorr_ref(mode_v);

  found_peak=0;
#ifndef EMBED_PERF
  printf ("starting demodulator %d\n",(int) sizeof(struct complex_f));
#endif
  first=1;
#ifndef EMBED_PERF
  nb_symb=0;
#endif
  indx_avpow=0;
  av_pow=0.0;
  symbol_nb=0;
  av_max_value=0.0;
  for (i=0;i<POW_BUFF_SIZE;i++) powinst[i]=0.0;
  for (i=0;i<DEPTH_MAX_AVERAGE;i++) av_max[i]=0.0;
  indx_max_average=0;
  partial_buffer=0;
  last_index_value=0;
  last_peak_indx=0;  
  first_peak_found=1;

  // bitstream pointer management
  last_c=0;
  last_ref=0;
  indxbs_c=0;
  indxbs_ref=0;
  bitstream[0]=0;
  bitstream_ref[0]=0;
  bitstream_c=bitstream;
  bitstream_ref_c=bitstream_ref;
  index_of_peak=0;
#ifndef EMBED_PERF
  for(j=0;j<100;j++) printf("sss %d %d\n", (int) pn_seq[j] , (int) pn_seq2[j]);
#endif

#ifndef EMBED_PERF
  printf("size %d\n",(int)exp2((float)NBITSCONST[constel])*ndata_tones);
#endif

#ifdef LINUX//TODO when porting dmod, port this in buffer
  if (detect_weaksig) {
    detect_weak_sig(in_dmod_file,
		    ref_tx_file,
		    mode_v);
  }
#endif

#ifndef LINUX
  tx_ref = tx_ref_ini;
#endif
  
  while(1) {
//LINUX
#ifdef LINUX
    if (!force_symbol_detect_v) {
      if (!READ_CSV)
	if (first) {
  #ifdef LINUX
	  if (!fseek(in_dmod_file,FIRST_SAMPLES_SKIPPED*sizeof(struct complex_f),SEEK_SET))
	    printf("DMOD: (skip first) cant read  %d samples from file", (int) FIRST_SAMPLES_SKIPPED);
	  if (fread(in_dmod,sizeof(struct complex_f),(2*nfft+ncp),in_dmod_file)!=(2*nfft+ncp)) {
	    printf("DMOD: cant read any more samples from input %d", (int) 2*nfft+ncp);
	    break;
	  } else {
  #endif
  #ifndef EMBED_PERF
	    if (0)printf("reads %d samples\n", (int) 2*nfft+ncp);
  #endif
	    //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft+ncp);
	  tot_read=2*nfft+ncp;
	  }
	}
	else {
  #ifdef LINUX
	  if (fread(in_dmod+n1+ncp+rad_search_peak_v,sizeof(struct complex_f),2*nfft-n1-rad_search_peak_v,in_dmod_file)!=(2*nfft-n1-rad_search_peak_v)) {
	    printf("DMOD: cant read any more samples from input %d", (int) 2*nfft-n1-rad_search_peak_v);
  #endif
	    break;
	    
	  } else  {
  #ifndef EMBED_PERF
	    if (0)printf("reads %d samples\n", (int) 2*nfft-n1-rad_search_peak_v);
  #endif
	    //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft-n1-rad_search_peak_v);
	    tot_read=2*nfft-n1-rad_search_peak_v;
	  }
	}
      else {// READ_CSV
	if (first) {
	  if (skip_csv(FIRST_SAMPLES_SKIPPED,in_dmod_file)!=FIRST_SAMPLES_SKIPPED){
  #ifndef EMBED_PERF
	    printf("DMOD: (skip first CSV) cant read  %d samples from file", (int) FIRST_SAMPLES_SKIPPED);
  #endif
	    exit(0);
	  }
	  else {
  #ifndef EMBED_PERF
	    printf("skipped %d samples\n", (int) FIRST_SAMPLES_SKIPPED);
  #endif
	    sample_nb=rad_search_peak_v+FIRST_SAMPLES_SKIPPED;
	  }
	  if (fread_csv(in_dmod,(2*nfft+ncp),in_dmod_file)!=(2*nfft+ncp)) {
  #ifndef EMBED_PERF
	    printf("DMOD: cant read any more samples from input %d", (int) 2*nfft+ncp);
  #endif
	    break;
	  } else {
  #ifndef EMBED_PERF
	    if (0)printf("reads %d samples\n", (int) 2*nfft+ncp);
  #endif
	    //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft+ncp);
	    tot_read=2*nfft+ncp;
	  }
	}
	else {
  #ifdef LINUX
	  if (fread_csv(in_dmod+n1+ncp+2*rad_search_peak_v,2*nfft-n1-2*rad_search_peak_v,in_dmod_file)!=(2*nfft-n1-2*rad_search_peak_v)) {
	    printf("DMOD: cant read any more samples from input %d", (int) 2*nfft-n1-2*rad_search_peak_v);
	    printf("%d %d %d\n", (int) nfft, (int) n1, (int) rad_search_peak_v);
	    if (symbol_nb==0) fprintf(stderr,"No OFDM sync found in  this stream\n");
	    break;
	  } else  {
	    if (0)printf("reads %d samples\n", (int) 2*nfft-n1-2*rad_search_peak_v);
	    //if (SUBSTR_OFF) substr_offset(in_dmod,av_re,av_im,2*nfft-n1-2*rad_search_peak_v);
	    tot_read+=2*nfft-n1-2*rad_search_peak_v;
	  }
  #endif
	}
      }
      //plot(dump_in_mod,2*nfft+ncp,(struct complex_f *)in_dmod);
      //
      //  coarse sync
      //
 
      // reset detection flag && calc variance and average signal in this buffer
      if (1||!first) calc_mean_variance(in_dmod+n1+ncp+2*rad_search_peak_v,2*nfft-n1-2*rad_search_peak_v,&av_re,&av_im,&sigma);
      else  calc_mean_variance(in_dmod,2*nfft+ncp,&av_re,&av_im,&sigma);
  #ifndef EMBED_PERF
      printf("111for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);
  #endif

      // compensate offset
      if (SUBSTR_OFF) {
	if (1||!first) {
	  substr_offset(in_dmod+n1+ncp+2*rad_search_peak_v,av_re,av_im,2*nfft-n1-2*rad_search_peak_v);
	  calc_mean_variance(in_dmod+n1+ncp+2*rad_search_peak_v,2*nfft-n1-2*rad_search_peak_v,&av_re,&av_im,&sigma);
	} else {
	  substr_offset(in_dmod,av_re,av_im,2*nfft+ncp);
	  calc_mean_variance(in_dmod,2*nfft+ncp,&av_re,&av_im,&sigma);
	}
  #ifndef EMBED_PERF
	printf("222for this buffer: average %f %f  variance %f\n",av_re,av_im,sigma);
  #endif
      }

      if (DETECT_PEAK)
      {
	// sample counter set
	//if (first) sample_nb=rad_search_peak_v;

	detect_peak(in_dmod,&index_of_peak,&last_peak_indx,&first_peak_found,&found_peak,mode_v);

	if (found_peak||partial_buffer)
        {
	  process_peak_list(found_peak,first_peak_found,&last_peak_indx);
	  index_peak=0;

	  while( partial_buffer || symb_to_process>0 )
          {
  #ifndef EMBED_PERF
	    printf("update buffer\n");
  #endif
	    if (fill_in_buffer(in_dmod_buff,in_dmod,&first_peak_found,backoff_v)) break;
  #ifndef EMBED_PERF
	    printf("process symbol %d\n", (int) symbol_nb);
	    // dump input buffer
	    if (1) for(i=0;i<nfft+ncp;i++) printf("rrr5 %d %f %f\n", (int) i,in_dmod_buff[i].r,in_dmod_buff[i].i);
  #endif
  #ifdef LINUX
	    // read reference for this symbol (!!! not same nb of samples if greengreen or greenconv)
	    if (mode_v==GREEN_GREEN)
            {
              fread(tx_ref,sizeof(struct complex_f),nfft*nsub_sect,ref_tx_file);
            }	
	    else
            { 
              fread(tx_ref,sizeof(struct complex_f),nfft,ref_tx_file);
            }
/*
#ifndef EMBED_PERF
            uint32_t cnt_dbg;
            for(cnt_dbg=0; cnt_dbg < nfft * nsub_sect; cnt_dbg++);
               fprintf(stderr, "%d txref.r=%f txref.i=%f",  (int) cnt_dbg, (txref + cnt_dbg).r, (txref + cnt_dbg).i);
#endif
*/
  #endif
	    flag_iter=(SEARCH_START&&mode_v&&((symbol_nb==0)||!FIND_FIRST_PEAK_ONLY));
	    // !! force flag iter to 0 for gossyc and after
	    flag_iter=0;

	    ndiffnb = demod_symbol(in_dmod_buff,tx_ref,symbol_nb,backoff_v,flag_iter,out_dmod,corr_facts,out_fft,mode_v,&bckoffmin,symb_struct,ndata_tones,pn_seq,pn_seq2);

	    // test synchro: if symbol has too many errors, assume bad sync: restart peak detection
	    if ((symbol_nb==0)&&(ndiffnb>=RATIO_BITS_ERR*(float)nb_bits_in_symbol)) {
  #ifndef EMBED_PERF
	      printf("WARNING: reset sync after bad symbol detection (err=%d)\n", (int) ndiffnb);
  #endif
	      first_peak_found=1;
	      found_peak=0;
	      last_peak_indx=0;
	      rewind(ref_tx_file);
	      //rewind(dump_in_mod);
	      symb_to_process=0;
	      partial_buffer=0;
	      index_of_peak=0;
	      break;
	    }

	    // the ref_file contains the pnseq modulated data: so no need to demodulate the received by the pnseq
	    //bit_extract((struct complex_f *)out_fft,bitstream_c+=indxbs_c,&nbits,&last_c,&indxbs_c,constel,symb_struct,pn_seq,mode_v);
	    //soft_bit_extract((struct complex_f *)out_fft,bitstream_c+=indxbs_c,&nbits,&last_c,&indxbs_c,constel,symb_struct,mode_v,&nb_soft_bits);

	    bit_extract(tx_ref,bitstream_ref_c+=indxbs_ref,&nbits_ref,&last_ref,&indxbs_ref,constel,symb_struct,pn_seq,mode_v);
  #ifndef EMBED_PERF
	    printf("finished  dmod symbol %d\n", (int) symbol_nb);
  #endif
	    symb_to_process--;
	    symbol_nb++;
	    if (symbol_nb>=(NB_OF_SYMBOLS-LAST_SYMBS)) break;

	    //
	  }// while
  #ifndef EMBED_PERF
	  if (symbol_nb>=(NB_OF_SYMBOLS-LAST_SYMBS)) {printf("finished demodulating %d symbols, totdist=%f\n", (int) symbol_nb,tot_distsymb);break;}
  #endif
	  // last index at end of buffer (account for first peak found in next symbol
	  //last_index_value=-((tot_read-last_index_value)-798);
	  //last_index_value=-((2*nfft+ncp-last_index_value)-(n1+ncp+2*rad_search_peak_v));
	  //nb_symb+=found_peak;
  #ifndef EMBED_PERF
	  printf("average symbol length = %f\n",(float)av_index/(float)nb_symb);
  #endif
      } else {
	// case in conventional, the last loaded data dont contain a start symbol (load only nfft-RAD samples each pass
  #ifndef EMBED_PERF
	printf("no symbol data in this buffer!\n");
  #endif
	last_index_value=-((2*nfft+ncp-last_index_value)-(n1+ncp+2*rad_search_peak_v));
	assert(partial_buffer==0);
      }

      // shift the last n1+ncp+rad_search_peak_v samples at start of  ncp+2nfft buffer
  #ifndef EMBED_PERF
      printf("shift %d samples\n", (int) n1+ncp+2*rad_search_peak_v);
  #endif
      for (i=0;i<n1+ncp+2*rad_search_peak_v;i++) in_dmod[i]=in_dmod[2*nfft-n1+i-2*rad_search_peak_v]; 
      // shift last RAD samles in phi_sum
      if (0)for(i=0;i<rad_search_peak_v;i++) phi_sum[i]=phi_sum[2*nfft-n1-rad_search_peak_v+i];
      
     }// detectpeak
      first=0;

    }// force_symbol_detect_v
//end ordinary demod
//beg of the perfect demodulator
    else {
#endif
#ifndef EMBED_PERF
      printf("direct demodulation of input stream assuming start at index RANDOM_START%d\n", (int) RANDOM_START);
#endif

#ifdef INTERP_FIR
      // get decimation filter init for RX
      indx_fir=0;
      memset(delline1,0,NMAX*sizeof(float));
      memset(delline2,0,NMAX*sizeof(float));
#endif

      if (first) {
	// outdmod is a nsub_sect section symbols, only first section is read from input file, rest is reset
	memset(out_dmod,0,(nfft*nsub_sect_sig+ncp)*sizeof(struct complex_f));

	if (READ_FLOAT){

  #ifndef EMBED_PERF
	  if (1)printf("read float\n");
  #endif
  #ifdef LINUX //TODO when porting dmod transform here in buffer
	  nb_read=fread(out_modf,sizeof(struct complex_f),RANDOM_START+nfft+ncp,in_dmod_file) ;
	  conv_float2double(out_modf,in_dmod,RANDOM_START+nfft+ncp);
  #else
	  if (p_input_buf + RANDOM_START + nfft + ncp <= p_input_buf + size_input_buf)
          {
            //print_dmod(p_in_demod, RANDOM_START + nfft + ncp, __LINE__);
	    fread_csv_buf(in_dmod, RANDOM_START + nfft + ncp, p_input_buf);
            nb_read = RANDOM_START + nfft + ncp;
	  }
	  else
            fprintf(stderr, "Line %d Issue !(p_in_demod + RANDOM_START + nfft + ncp <= p_in_demod + size_input_demod_buf)\n", __LINE__);
  #endif
	}
	else if (READ_CSV)
        {
  #ifdef LINUX
	  if (skip_csv(FIRST_SAMPLES_SKIPPED,in_dmod_file) != FIRST_SAMPLES_SKIPPED)
          {
	    printf("DMOD: (skip first CSV) cant read  %d samples from file", (int) FIRST_SAMPLES_SKIPPED);
	    exit(0);
  /*#else TODO check how to do this check for embedded
          p_input_buf += FIRST_SAMPLES_SKIPPED;*/
	  }
	  else {
	    printf("skipped %d samples\n", (int) FIRST_SAMPLES_SKIPPED);
	    sample_nb=rad_search_peak_v+FIRST_SAMPLES_SKIPPED;
	  }
  #endif
#ifndef EMBED_PERF
	  if (1)printf("read CSV(0) %d\n", (int) size_input_buf);
#endif
#ifdef LINUX
	  nb_read=fread_csv(in_dmod,RANDOM_START+nfft+ncp,in_dmod_file);
	  // better when init p_input_buff
	  p_input_buf=p_in_buf;
	  if (p_input_buf + RANDOM_START + nfft + ncp < p_in_buf + size_input_buf) {
	    p_input_buf = fread_csv_buf(in_dmod, RANDOM_START + nfft + ncp, p_input_buf);
	  } else break;
#else
	  // better when init p_input_buff
	  p_input_buf = p_in_buf;
  #ifndef EMBED_PERF
/*          dbg_p_input_buf(__LINE__, p_input_buf);
          fprintf(stderr, "LINE=%d p_input_buf=%p\n", __LINE__, p_input_buf);
          fprintf(stderr, "RANDOM_START=%d nfft=%d ncp=%d\n", RANDOM_START, nfft, ncp);*/
  #endif
	  if (p_input_buf + RANDOM_START + nfft + ncp <= p_in_buf + size_input_buf)
          {
	    p_input_buf = fread_csv_buf(in_dmod, RANDOM_START + nfft + ncp, p_input_buf);
            nb_read = RANDOM_START + nfft + ncp;
	  }
	  else
          {
            nb_read = 0;
            fprintf(stderr, "Line %d Issue !(p_input_buf + RANDOM_START + nfft + ncp <= p_input_buf + size_input_buf)\n", __LINE__);
            fprintf(stderr, "\t RANDOM_START + nfft + ncp=%d size_input_buf=%d (p_input_buf - p_in_buf)=%d\n", 
                            (int) (RANDOM_START + nfft + ncp), (int) size_input_buf, (int) (p_input_buf - p_in_buf));
            break;
          }
  #ifndef EMBED_PERF
          /*fprintf(stderr, "LINE=%d p_input_buf=%p\n", __LINE__, p_input_buf);
          dbg_p_input_buf(__LINE__, p_input_buf);*/
  #endif
#endif
        }
#ifdef LINUX
	else
        { 
	  nb_read = fread(in_dmod,sizeof(struct complex_f),RANDOM_START+nfft+ncp,in_dmod_file) ;
        }
#endif

	if (nb_read != (RANDOM_START + nfft + ncp)) {
#ifndef EMBED_PERF
	  printf("DMOD: cant read any more samples from input %d", (int) RANDOM_START+nfft+ncp);
#endif
	  break;
	} else {
#ifndef EMBED_PERF
	  if (0)printf("reads %d samples\n", (int) RANDOM_START+nfft+ncp);
#endif
	  memcpy(in_dmod_buff,in_dmod+RANDOM_START,(nfft+ncp)*sizeof(struct complex_f));
	  first=0;
	}
      }
      else{
	if (READ_FLOAT)
        {
#ifdef LINUX
	  nb_read=fread(out_modf,sizeof(struct complex_f),nfft+ncp,in_dmod_file) ;
#else
	  if (p_input_buf + nfft + ncp <= p_in_buf + size_input_buf)
          {
            //print_dmod(p_in_demod, nfft + ncp, __LINE__);
	    p_input_buf = fread_csv_buf(out_modf + (nfft + ncp) * symbol_nb, nfft + ncp, p_input_buf);
            nb_read = nfft + ncp;
	  }
	  else
          {
            fprintf(stderr, "Line %d Issue !(p_input_buf + nfft + ncp <= p_input_buf + size_input_buf)\n", __LINE__);
            fprintf(stderr, "\t nfft + ncp=%d size_input_buf=%d (p_in_buf - p_in_buf)=%d\n", 
                            (int) (nfft + ncp), (int) size_input_buf, (int) (p_input_buf - p_in_buf));
          }
#endif

	  conv_float2double(out_modf,in_dmod_buff,nfft+ncp);
	}
	else if (READ_CSV){
/*	  printf("LINE=%d READ_CSV\n", __LINE__);
          fprintf(stderr, "LINE=%d p_input_buf=%p\n", __LINE__, p_input_buf);
          dbg_p_input_buf(__LINE__, p_input_buf);
*/
#ifdef LINUX
	  if (1) nb_read=fread_csv(in_dmod_buff,nfft+ncp,in_dmod_file);
	  if (p_input_buf + nfft + ncp < p_in_buf + size_input_buf) {
	    //???????????? in_dmod_buff = p_input_buf;
	    p_input_buf = fread_csv_buf(in_dmod_buff, nfft + ncp, p_input_buf);
	  }	  else {
	  	printf("input_buf overflow in READ_CSV");
	  	break;
          }
#else
	  if (p_input_buf + nfft + ncp <= p_in_buf + size_input_buf) {
	    //???????????? in_dmod_buff = p_input_buf;
	    p_input_buf = fread_csv_buf(in_dmod_buff, nfft + ncp, p_input_buf);
            nb_read = nfft + ncp;
	  }
          else {
  #ifndef EMBED_PERF
	  	printf("input_buf overflow in READ_CSV");
                fprintf(stderr, "Line %d Issue !(p_input_buf + nfft + ncp <= p_input_buf + size_input_buf)\n", __LINE__);
                fprintf(stderr, "\t nfft + ncp=%d size_input_buf=%d (p_in_buf - p_in_buf)=%d\n", 
                                (int) (nfft + ncp), (int) size_input_buf, (int) (p_input_buf - p_in_buf));
  #endif
                nb_read = 0;//so < nfft+ncp
	  	break;
	  }
#endif
/*
          fprintf(stderr, "LINE=%d p_input_buf=%p\n", __LINE__, p_input_buf);
          dbg_p_input_buf(__LINE__, p_input_buf);
*/
	}
	else
        { 	  
#ifdef LINUX
	  nb_read = fread(in_dmod_buff,sizeof(struct complex_f),nfft+ncp,in_dmod_file);
#else //TODO same action as READ_CSV so should be merge on 1 else only
	  if (p_input_buf + nfft + ncp <= p_in_buf + size_input_buf)
          {
	    p_input_buf = fread_csv_buf(in_dmod_buff, nfft + ncp, p_input_buf);
            nb_read = nfft + ncp;
          } else
          {
                fprintf(stderr, "Line %d Issue !(p_input_buf + nfft + ncp <= p_input_buf + size_input_buf)\n", __LINE__);
                fprintf(stderr, "\t nfft + ncp=%d size_input_buf=%d (p_in_buf - p_in_buf)=%d\n", 
                                (int) (nfft + ncp), (int) size_input_buf, (int) (p_input_buf - p_in_buf));
          }
#endif
        }

	if (nb_read != nfft + ncp)
        {
#ifndef EMBED_PERF
	  printf("DMOD: cant read any more samples from input %d",  (int) nfft + ncp);
#endif
	  break;
	  //exit(0);
	} else
        {
#ifndef EMBED_PERF
	  if (0)printf("reads %d samples\n",  (int) nfft + ncp);
#endif
	}
      }

#ifndef EMBED_PERF
      if (1) for(j=ncp;j<ncp+nfft;j++) printf("rrrxx%d %f %f-- %f %f -- %f %f\n", (int) j,in_dmod_buff[j].r,in_dmod_buff[j].i,out_mod[j].r,out_mod[j].i,out_mod[j].r/in_dmod_buff[j].r,out_mod[j].i/in_dmod_buff[j].i);
#endif

      // normalize fixed point
      if (READ_CSV) norm_v(in_dmod_buff,1.0/(float)(1<<(FIXED_POINT_FRAC+FIXED_POINT_INT-1)),nfft+ncp);
      // 
      assert(backoff_v>=0);

#ifndef EMBED_PERF
      printf("now demodulating symbol %d\n",  (int) symbol_nb);
#endif
      //plot(dump_in_mod,nfft+ncp,in_dmod_buff);
#ifndef EMBED_PERF
      if (0) for(j=ncp;j<ncp+nfft;j++) printf("rrrr%d %f %f\n", (int) j,in_dmod_buff[j].r,in_dmod_buff[j].i);
#endif
      
      // compensate the attenuation in case of multiple sections
      norm_v(in_dmod_buff,sqrt((float) nsub_sect_sig),nfft+ncp);
      //normalize fft
      norm_v(in_dmod_buff,fft_norm_v,nfft+ncp);
      // apply inverse of Tx gain
      norm_v(in_dmod_buff,1.0/(float) tx_gain_v,nfft+ncp);

#ifndef EMBED_PERF
      if (1) for(j=ncp;j<ncp+nfft;j++) printf("rrrr%d %f %f-- %f %f -- %f %f\n", (int) j,in_dmod_buff[j].r,in_dmod_buff[j].i,out_mod[j].r,out_mod[j].i,out_mod[j].r/in_dmod_buff[j].r,out_mod[j].i/in_dmod_buff[j].i);
#endif

      // force decode in conventional mode

      memcpy(out_dmod,in_dmod_buff+ncp-backoff_v,nfft*sizeof(struct complex_f));

      // read reference
#ifdef LINUX
      fread(tx_ref,sizeof(struct complex_f),nfft,ref_tx_file);
#endif
/*
      fprintf(stderr, "ref_tx=%p rf_tx_ini=%p\n", tx_ref, tx_ref_ini);
      print_complex(tx_ref, (nfft + ncp) * NB_OF_SYMBOLS, "txref before compare", __LINE__);
*/
      perfect_decoder(mode_v,out_dmod,out_mod,tx_ref,corr_facts,bitstream_c+=indxbs_c,bitstream_ref_c+=indxbs_ref,&last_c,&last_ref,&indxbs_c,&indxbs_ref,&nbits_left,&nbits,&nbits_ref); 
      tx_ref += nfft;
#ifndef EMBED_PERF
      printf("finished  dmod symb %d nbits=%d\n", (int) symbol_nb, (int) nbits);
#endif
      symbol_nb++;
      first=0;	
      if (symbol_nb>=(NB_OF_SYMBOLS-LAST_SYMBS)) break;
#ifdef LINUX
    }
#endif
  }
  
#ifdef LINUX
  FILE *brfile;
#endif
  float br;

  compare_bs(bitstream,bitstream_ref,nbits,&ndiff);
#ifndef DIS_END_MOD_PRINTF
  fprintf(stderr,"******************************************************************\n");
 #ifdef INTERP
  fprintf(stderr,"interpolator activated: OVSMPL FACTOR = %d\n",OVSMPL_FACTOR);
 #endif
  if (force_symbol_detect_v) fprintf(stderr,"DEMODULATOR W/ PERFECT CHANNEL\n"); else fprintf(stderr,"\t DEMODULATE RX SIGNAL FROM BLADERF\n");

  fprintf(stderr,"\t signal is %s\n",(mode_v==GREEN_GREEN)?"green SLM":((mode_v==GREEN_CONV)?"conv SLM":"conv"));

  if (constel==QPSKC) fprintf(stderr,"\t modulation type is QPSK\n");
  else if (constel==QAM16C)  fprintf(stderr,"\t modulation type is QAM16\n");
  else  fprintf(stderr,"\t modulation type is BPSK\n");

  fprintf(stderr,"\t time backoff is %d\n", (int) backoff_v);
  fprintf(stderr,"\t nb active tones %d nb pilots %d nb_tones_off %d (including DC)\n", (int) ndata_tones, (int) count_tone(symb_struct,PILOT_TONE), (int) count_tone(symb_struct,NULL_TONE));
  fprintf(stderr,"\t Nb symbol %d Nb bit total %d Nb bit per symbol %d => bit rate %4.1fkb/s\n", (int) symbol_nb, (int) nb_bits_in_symbol*symbol_nb, (int) nb_bits_in_symbol,br=(float)nb_bits_in_symbol/(float)((nfft+ncp)*TS/OVSMPL_FACTOR));

 #ifdef LINUX
  // output bitrate
  brfile=fopen("bitrate_file","w");
  fprintf(brfile,"%4.1f",br);
  fclose(brfile);
 #endif

  if (force_symbol_detect_v) {
    fprintf(stderr,"\t\tbitstream compare: %d bits different (out of  %d)\n", (int) ndiff, (int) nbits);
    if (ndiff) fprintf(stderr,"ERROR MOD: perfect demodulation reports errors (%d/%d)\n", (int) ndiff, (int) nbits);}
  else {
    fprintf(stderr,"\t\tbitstream compare: %d bits different (out of  %d)\n", (int) ndiff, (int) nbits);
    fprintf(stderr,"BER%s %f\n",(mode_v==GREEN_GREEN)?"green SLM":((mode_v==GREEN_CONV)?"conv SLM":"conv"),(float)ndiff/(float)nbits);
 #ifdef LINUX
    fprintf(berfile,"%f",(float)ndiff/(float)nbits);
 #endif
  }

  fprintf(stderr,"******************************************************************\n");
#endif


 static int32_t loop_algo = 0;
  if (-1 <= loop_algo)
  {
#ifdef LINUX
    fclose(berfile);
    fclose(dump_in_mod);
    fclose(dumpphisum);
    fclose(ref_tx_file);
    fclose(dumpout);
#endif
#ifdef PC
   return 0;
#endif
  }
else
 {
#ifdef EMBED_PERF
   fprintf(stderr, "loop_algo nbr%d\n", (int) loop_algo);
#endif
   loop_algo++;
   p_input_buf = p_in_buf;
   tx_ref = tx_ref_ini;
   memset(tx_ref_ini, 0, sizeof(struct complex_f) * NB_OF_SYMBOLS_MAX * N_MAX * M_MAX);
   memset(p_in_buf, 0, sizeof(struct complex_f) * (N_MAX + NCP_MAX) * NB_OF_SYMBOLS_MAX);
   goto algo_start;
 }

#ifndef PC
  #ifdef ADD_NULL_TONES
    ret_m.length = (uint32_t) (2* (nfft + ncp) * NB_OF_SYMBOLS); //* OVSMPL_FACTOR);
  #else
    ret_m.length = (uint32_t) ((nfft + ncp) * NB_OF_SYMBOLS); //* OVSMPL_FACTOR);
  #endif
  ret_m.pt_out = p_in_buf;
  return (ret_m);
#endif
} // end main
