/*
 * fft.c
 * 
 * Copyright 2015 gulfomoj <gulfomoj@gulfomoj-X550JK>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "fft.h"
#define PI 3.14159265359
#define MAXPOW 24

int32_t pow_2[MAXPOW];
int32_t pow_4[MAXPOW];

void bit_reverse_reorder(struct complex_f *, int32_t, int32_t);

void twiddle(struct complex_f *, int32_t, int32_t);

void conjugate_imag(struct complex_f *buf, int32_t size);

// FFT CODE WITH RADIX-2 DIF IMPLEMENTATION
void radix2(struct complex_f *, int32_t);

void bit_reverse_reorder(struct complex_f *W, int32_t N, int32_t rad){	
	
    int32_t bits, i, j, k;
    float tempr, tempi;
	pow_2[0] = 1;
	for (j=1;j < MAXPOW; j++){		
		pow_2[j] = pow_2[j-1]*2;
	}
	pow_4[0] = 1;
	for (j=1;j < MAXPOW; j++){		
		pow_4[j] = pow_4[j-1]*4;
	}	
    
    for (i=0; i<MAXPOW; i++)
	if (pow_2[i]==N) bits=i;

    for (i=0; i<N; i++)
    {
	j=0;
	for (k=0; k<bits; k++)
		if (rad == 2){
			if (i&pow_2[k]) j+=pow_2[bits-k-1];
		}else{
			if (rad == 4){
				if (i&pow_2[k]) j+=pow_2[bits-k-2];
				if (i&pow_2[k+1]) j+=pow_2[bits-k-1];
			}
		}

	if (j>i)  /** Only make "up" swaps */
	{
	    tempr=W[i].r;
	    tempi=W[i].i;
	    W[i].r=W[j].r;W[i].i= W[j].i;
	    W[j].r = tempr;W[j].i = tempi;
	}
    }
}

void twiddle(struct complex_f *W, int32_t N, int32_t stuff){
    W->r = (float) cos((double) (stuff*2.0*PI/(float)N));
    W->i = (float) sin((double) (stuff*2.0*PI/(float)N));
}

// FFT CODE WITH RADIX-2 DIF IMPLEMENTATION
void radix2(struct complex_f *data, int32_t N){
	int32_t n2, k1, N1, N2;
    struct complex_f W, bfly[4];
    N1 = 2;
    N2 = N/2;
		/** Do 2 Point DFT */
		for (n2=0; n2<N2; n2++){
			/** Don't hurt the butterfly */
			twiddle(&W, N, n2);
			bfly[0].r = data[n2].r + data[N2 + n2].r;
			bfly[0].i = data[n2].i + data[N2+n2].i;		
			bfly[1].r = (data[n2].r - data[N2 + n2].r) * W.r - 
				((data[n2].i - data[N2 + n2].i) * W.i);
			bfly[1].i = (data[n2].i - data[N2 + n2].i) * W.r +
				(data[n2].r - data[N2 + n2].r) * W.i;

			/** In-place results */
				data[n2].r = bfly[0].r;
				data[n2].i = bfly[0].i;
				data[n2 + N2].r = bfly[1].r;
				data[n2 + N2].i = bfly[1].i;				

		}
		    
    /** Don't recurse if we're down to one butterfly */
    if (N2!=1)
	for (k1=0; k1<N1; k1++)
	    radix2(&data[N2*k1], N2);	    
}        

void conjugate_imag(struct complex_f *buf, int32_t size) {
	int32_t i;
	for (i = 0; i < size; i++) {
		buf[i].i = - buf[i].i;
        }
}

void fft_code(int32_t n, struct complex_f *in, struct complex_f *out, int32_t sign, int32_t norm) {
	int32_t i = 1;
	memcpy(out, in, n * sizeof(struct complex_f));	
	if (1 == sign) {
		//IFFT
		radix2(out, n);
		bit_reverse_reorder(out, n, (int32_t) 2);
	} else {
		//FFT
		conjugate_imag(out, n);
		radix2(out,n);
		bit_reverse_reorder(out, n, (int32_t) 2);		
		conjugate_imag(out, n);
	}
	// normalisation
	if (1 == norm) {
	  for (i = 0; i < n; i++) {
	    out[i].r = out[i].r / sqrt(n);
	    out[i].i = out[i].i / sqrt(n);
	  }
	}
}
