/*
 * \file gwt_modulator.cpp GWT algorithm to modulate data
 * Copyright (c) ASYGN S.A.S 2015
 */

/*
 * includes
 */
#include "gwt_modulator.h"

extern int16_t characterizationPattern[12800][2];

/*
 * @brief
 */
gwt_modulator::gwt_modulator()
{
    memset(this->modulated_data, 0, sizeof(this->modulated_data[0][0]) * RF_TX_BUFFER_WR_MAX_DEPTH * 2);
	this->modulated_data_len = 0;
	this->modulation_en = ZYNQ_RF_TX_GWT_MOD_ENABLE;
	this->modulation_ready = true;
	this->mode_python = true;
}

/*
 * @brief
 */
gwt_modulator::~gwt_modulator()
{

}

/*
 * @brief Modulate the data with GWT algorithm
 * @param mode - enable GWT algorithm or by pass it
 * @param data - signed integers to modulate
 * @param len - signed integers number to modulate
 * @return return a modulated data pointer
 */
void gwt_modulator::modulation(int8_t mode, int32_t* data, uint32_t len)
{
	this->modulation_en = mode;

	if (this->modulation_en == ZYNQ_RF_TX_GWT_MOD_ENABLE)
	{
		// GWT algorithm
		/*struct {
			struct complex_f * pt_out;
			uint32_t length;
			//this->out_mod;
			//this->modulated_data_len;
		} mod;*/
		struct ret_mod mod;
		mod = modulator_autotest((int8_t *) data, len);
		this->out_mod = mod.pt_out;
		this->modulated_data_len = mod.length;
			//printf("fake gwt mod");
	} else
	{
		for (uint32_t i = 0; i < GWT_MOD_INPUT_TO_OUTPUT_LEN_FACTOR; ++i) {
			for (uint32_t index = 0; index < len; ++index)
			{
				this->modulated_data[index][0] = (int16_t) (data[index] & 0xFFFF);
				this->modulated_data[index][1] = (int16_t) ((data[index] >> 16) & 0xFFFF);
			}
		}
		this->modulated_data_len = len*GWT_MOD_INPUT_TO_OUTPUT_LEN_FACTOR;
	}
}

void gwt_modulator::configure(uint32_t* pt_data, uint32_t len)
{
	gwt_configure_options((int32_t *) pt_data);
}
