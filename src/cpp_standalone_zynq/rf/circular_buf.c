#include "circular_buf.h"

uint32_t available_circ(struct circ *pt_cbuf)
{
  if (pt_cbuf->head == pt_cbuf->tail)
  {
     if (SIZE_BITSTREAM_BUF == pt_cbuf->count)
       return 0;
     else
       return SIZE_BITSTREAM_BUF;
  }
  return (SIZE_BITSTREAM_BUF - pt_cbuf->count);
}

int32_t push_circ(struct circ *pt_cbuf, uint8_t *buf, uint32_t len)
{
  if (len > available_circ(pt_cbuf))
    return CIRC_ERR_OVERFLOW;

  if (SIZE_BITSTREAM_BUF - 1 < pt_cbuf->head + len)
  {
    int32_t calc = SIZE_BITSTREAM_BUF - pt_cbuf->head;
    memcpy(pt_cbuf->circ_buf + pt_cbuf->head, buf, (size_t) calc);
    int32_t calc2 = len - calc;
    memcpy(pt_cbuf->circ_buf, buf + calc, (size_t) calc2);
    pt_cbuf->head = (pt_cbuf->head + len) % SIZE_BITSTREAM_BUF;
  } 
  else
  {
    memcpy(pt_cbuf->circ_buf + pt_cbuf->head, buf, (size_t) len);
    pt_cbuf->head = (pt_cbuf->head + len) % SIZE_BITSTREAM_BUF;
  }
  pt_cbuf->count += len;
  return 0;
}

int32_t pop_circ(struct circ *pt_cbuf, uint8_t * buf, uint32_t len)
{
  if (len > pt_cbuf->count)
    return CIRC_ERR_TOOMUCH;

  if (SIZE_BITSTREAM_BUF - 1 < pt_cbuf->tail + len)
  {
    int32_t calc = SIZE_BITSTREAM_BUF - pt_cbuf->tail;
    memcpy(buf, pt_cbuf->circ_buf, (size_t) calc);
    int32_t calc2 = len - calc;
    memcpy(buf, pt_cbuf->circ_buf + calc, (size_t) calc2);
    pt_cbuf->tail = (pt_cbuf->tail + len) % SIZE_BITSTREAM_BUF;
  } 
  else
  {
    memcpy(buf, pt_cbuf->circ_buf, (size_t) len);
    pt_cbuf->tail = (pt_cbuf->tail + len) % SIZE_BITSTREAM_BUF;
  }
  pt_cbuf->count -= len;
  return 0;
}

/*
*use to reset and initialize the buffer. Mandatory before to use it.
*/
void reset_circ(struct circ *pt_cbuf)
{
  uint32_t i;
  pt_cbuf->tail = 0;
  pt_cbuf->head = 0;
  pt_cbuf->count = 0;

  for(i=0; i < SIZE_BITSTREAM_BUF; i++)
  {
    pt_cbuf->circ_buf[i] = 0;
  }
}

void print_circ(struct circ *pt_cbuf)
{
  uint32_t i;
  printf("tail=%d head=%d count=%d\n", 
          (int) pt_cbuf->tail, (int) pt_cbuf->head, (int) pt_cbuf->count);
  for(i=0; i < SIZE_BITSTREAM_BUF; i++)
  {
    printf("cbuf[%d]=%d\n", (int) i, (int) pt_cbuf->circ_buf[i]);
  }
}

/*
* uncomment main to test the circular buffer
*/
/*
int main()
{
  struct circ cbuf;
  struct circ *pt_cbuf = &cbuf;
  int32_t err;
  uint8_t consumer[7];

  reset_circ(pt_cbuf);
  //print_circ(pt_cbuf);

  uint8_t tab[4] = { 0, 1, 2, 3 };
  uint8_t tab2[4] = { 10, 11, 12, 13 };
  uint8_t tab3[7] = { 20, 21, 22, 23, 24, 25, 26 };

  printf("1)Fill cbuf[7] with 4 elements: expecting ok\n");
  printf("============================\n");
  err = push_circ(pt_cbuf, tab, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("2)Fill cbuf[7] with 4 more elements: expecting error\n");
  printf("================================\n");
  err = push_circ(pt_cbuf, tab2, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("3)Consume 2 elements of cbuf[7]: expecting ok\n");
  printf("=============================\n");
  err = pop_circ(pt_cbuf, consumer, 2);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("4)Fill cbuf[7] with 4 more element: expecting ok\n");
  printf("================================\n");
  err = push_circ(pt_cbuf, tab3, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);


  printf("5)Fill cbuf[7] with 4 more elements: expecting error\n");
  printf("================================\n");
  err = push_circ(pt_cbuf, tab2, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("6)Consume 4 elements of cbuf[7]: expecting ok\n");
  printf("=============================\n");
  err = pop_circ(pt_cbuf, consumer, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);
  
  printf("7)Fill cbuf[7] with 4 more element: expecting ok\n");
  printf("================================\n");
  err = push_circ(pt_cbuf, tab2, 4);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("8)Consume count+1 elements of cbuf[7]: expecting error\n");
  printf("=============================\n");
  err = pop_circ(pt_cbuf, consumer, cbuf.count + 1);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("9)Consume count elements of cbuf[7]: expecting ok\n");
  printf("=============================\n");
  err = pop_circ(pt_cbuf, consumer, cbuf.count);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("10)Fill max available elements of cbuf[7]: expecting ok\n");
  printf("=============================\n");
  err = push_circ(pt_cbuf, consumer, available_circ(pt_cbuf));
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  printf("11)Consume SIZE_BITSTREAM_BUF elements of cbuf[7]: expecting ok\n");
  printf("=============================\n");
  err = pop_circ(pt_cbuf, consumer, SIZE_BITSTREAM_BUF);
  if (err)
    printf("circular buffer error=%d\n", (int) err);
  print_circ(pt_cbuf);

  return 0;
}
*/
