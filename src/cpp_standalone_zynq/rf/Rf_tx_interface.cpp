/*
 * \file Rf_tx_interface.cpp Methods to control RF TX interface
 * Copyright (c) ASYGN S.A.S 2015
 */

#include "Rf_tx_interface.h"
#include "../config.h"
#include "sleep.h"
#include "pattern.h"

/*
 * @brief Constructor
 */
Rf_tx_interface::Rf_tx_interface()
{
	this->i_bit_0 = i_bit_0_stop;
	this->q_bit_0 = q_bit_0_set_low;
	this->buffer_index = 0;
	this->buffer_full_counter = 0;
	this->transmit_enable = 0;
	this->check_data_send = false;
	this->reset_data_frame();
}

/*
 * @brief Destructor
 */
Rf_tx_interface::~Rf_tx_interface()
{
	this->rf_tx_fifo.~Rf_tx_fifo();
}

/*
 * @brief reset all RF TX elements
 */
void Rf_tx_interface::reset()
{
	// register is overwritten, only reset signal is set to '1'
	AXI_REG_RF_TX_FIFO_CTRL = 0x1;
	AXI_REG_RF_TX_CONTROLLER = 0x1;
	usleep(1);
	// release the reset
	AXI_REG_RF_TX_CONTROLLER = 0x0;
	AXI_REG_RF_TX_FIFO_CTRL = 0x0;

	this->reset_data_frame();
}

/*
 * @brief reset data frame which is sent to GWT modulator
 */
void Rf_tx_interface::reset_data_frame()
{
	this->frame_to_modulate_len = 0;
    memset(this->frame_to_modulate, 0, sizeof(this->frame_to_modulate[0]) * RF_TX_BUFFER_WR_MAX_DEPTH);
}
/*
 * @brief generate request to start to send sample within FIFO to RF module
 */
void Rf_tx_interface::start_transmit()
{
	AXI_REG_RF_TX_CONTROLLER |= AXI_REG_RF_TX_CONTROLLER_rf_tx_controller_enable_MASK;
	this->transmit_enable = 1;
}

/*
 * @brief generate request to stop to send sample within FIFO to RF module
 */
void Rf_tx_interface::stop_transmit()
{
	AXI_REG_RF_TX_CONTROLLER &= ~AXI_REG_RF_TX_CONTROLLER_rf_tx_controller_enable_MASK;
	this->transmit_enable = 0;
}

/*
 * @brief fill the FIFO with IQ samples to send for the next transmission
 */
void Rf_tx_interface::fill_fifo(int16_t* samples, uint32_t sample_nb, uint32_t zero_samples_nb)
{
	uint16_t i = 0;
	uint16_t q = 0;

	// Fill FIFO with zeros samples
	for (uint32_t index = 0; index < zero_samples_nb; ++index)
	{

		if (index < (zero_samples_nb/2))
		{
			this->i_bit_0 = i_bit_0_stop;
		} else
		{
			this->i_bit_0 = i_bit_0_start;
		}

		q = 0x0;//this->q_bit_0;
		i = 0x0;//this->i_bit_0;

		while(this->rf_tx_fifo.is_full());
		this->rf_tx_fifo.write_sample_iq(i, q);
	}

	// Fill FIFO with samples
	for (uint32_t index = 0; index < (sample_nb*2); index+=2)
	{
		this->i_bit_0 = i_bit_0_start;

		// i[0] and q[0] are control bits and are not payload bits
		// use pointer casts to maintain exact bit pattern
		q = (( (*(uint16_t*) &(samples + index)[1]) << 1) | this->q_bit_0);
		i = (( (*(uint16_t*) &(samples + index)[0]) << 1) | this->i_bit_0);

		while(this->rf_tx_fifo.is_full())
		{
			this->buffer_full_counter ++;
		}
		this->rf_tx_fifo.write_sample_iq(i, q);

		int16_t it = i >> 1;
		int16_t qt = q >> 1;

		/*{
			//IQ, I is the real part, Q is the imaginary part
			printf("csv data send to RF : index=%d expected_I=%d effective_I=%d expected_Q=%d effective_Q=%d\n",
					(int) index, (*(int16_t*) &(samples + index)[0]), (int16_t) it, (*(int16_t*) &(samples + index)[1]), (int16_t) qt);
		}*/
	}//*/
}

/*
 * @brief fill the FIFO with IQ samples to send for the next transmission
 */
void Rf_tx_interface::fill_fifo_GWT(complex_f * mod, uint32_t sample_nb, uint32_t zero_samples_nb)
{
	int16_t i = 0;
	int16_t q = 0;

	// Fill FIFO with zero_samples_nb at a fixed valuez, here zero
	for (uint32_t index = 0; index < zero_samples_nb; ++index)
	{

		if (index < (zero_samples_nb/2))
		{
			this->i_bit_0 = i_bit_0_stop;
		} else
		{
			this->i_bit_0 = i_bit_0_start;
		}

		q = 0;//this->q_bit_0;
		i = 0;//this->i_bit_0;

		while(this->rf_tx_fifo.is_full());
		this->rf_tx_fifo.write_sample_iq(i, q);
	}

	// Fill FIFO with samples
	for (uint32_t index = 0; index < (sample_nb); index++)
	{
		this->i_bit_0 = i_bit_0_start;

		// i[0] and q[0] are control bits and are not payload bits
		// use pointer casts to maintain exact bit pattern
		q = ( (((int16_t) (mod + index)->i) << 1) | this->q_bit_0);
		i = ( (((int16_t) (mod + index)->r) << 1) | this->i_bit_0);

		while(this->rf_tx_fifo.is_full())
		{
			this->buffer_full_counter ++;
		}
		this->rf_tx_fifo.write_sample_iq(i, q);
		/*if (check_data_send)
		{
			int16_t it = i >> 1;
			int16_t qt = q >> 1;

			{
				//IQ, I is the real part, Q is the imaginary part
				printf("mod data send to RF : index=%d expected_I=%f effective_I=%f expected_Q=%f effective_Q=%f\n",
						(int) index, (mod + index)->r, (float) it, (mod + index)->i, (float) qt);
			}
		}*/
	}
}

/*
 * @brief Returns TRANSMIT_DONE if transmission is ended else it returns TRANSMIT_PROCESSING
 */
uint32_t Rf_tx_interface::is_enable()
{
	return (AXI_REG_RF_TX_CONTROLLER & AXI_REG_RF_TX_CONTROLLER_rf_tx_controller_enable_MASK);
}

/*
 * @brief Returns TRANSMIT_DONE if transmission is ended else it returns TRANSMIT_PROCESSING
 */
uint32_t Rf_tx_interface::transmit_status()
{
	uint32_t status = 0x0;

	if ( (this->rf_tx_fifo.is_empty() == RF_TX_FIFO_IS_EMPTY) && (this->is_enable() == RF_TX_INTERFACE_IS_ENABLE) )
	{
		status = TRANSMIT_DONE;
	} else {
		status = TRANSMIT_PROCESSING;
	}

	return status;
}

/*
 * @brief fill FIFO from samples store in pattern.h and launch a transmission
 */
void Rf_tx_interface::self_test()
{
	// set burst mode to fill buffer
	this->loop_burst_mode(ZYNQ_RF_TX_BURST_MODE);
	this->fill_fifo(characterizationPattern[0], CHARACTERIZATION_PATTERN_SIZE, ZERO_SAMPLE_NB);
	// set loop mode before the start to play in loop the transmission
	this->loop_burst_mode(ZYNQ_RF_TX_LOOP_MODE);
	this->start_transmit();
}

/*
 * @brief set RF TX mode as loop (each sample read from FIFO is rewritten into the FIFO) or burst mode (each sample read from FIFO is sent once only)
 * @param mode - '0' = burst, '1' = loop
 */
void Rf_tx_interface::loop_burst_mode(uint8_t mode)
{
	switch (mode) {
		case ZYNQ_RF_TX_LOOP_MODE:
			AXI_REG_RF_TX_CONTROLLER |= AXI_REG_RF_TX_CONTROLLER_rf_tx_loop_burst_mode_MASK;
			break;
		case ZYNQ_RF_TX_BURST_MODE:
			AXI_REG_RF_TX_CONTROLLER &= ~AXI_REG_RF_TX_CONTROLLER_rf_tx_loop_burst_mode_MASK;
			break;
		default:
			// do nothing
			break;
	}
}

/*
 * @brief return RF_TX_BUFFER_IS_FULL if the buffer is full, return RF_TX_BUFFER_IS_NOT_FULL else
 */
uint32_t buffer_is_full()
{
	return ((AXI_REG_RF_TX_BUFFER_STATUS & AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_empty_flag_MASK) >> AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_empty_flag_OFFSET);
}

/*
 * @brief return RF_TX_BUFFER_IS_EMPTY if the buffer is full, return RF_TX_BUFFER_IS_NOT_EMPTY else
 */
uint32_t buffer_is_empty()
{
	return ((AXI_REG_RF_TX_BUFFER_STATUS & AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_full_flag_MASK) >> AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_full_flag_OFFSET);
}

/*
 * @brief provide the number of words in buffer
 */
uint32_t Rf_tx_interface::get_data_count()
{
	return ((AXI_REG_RF_TX_BUFFER_STATUS & AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_data_count_MASK) >> AXI_REG_RF_TX_BUFFER_STATUS_rf_tx_buffer_data_count_OFFSET);
}

/*
 * @brief enable or disable the check of the validation of the data send to the buffer
 */
void Rf_tx_interface::set_check_data_send(bool en)
{
	this->check_data_send = en;
}

/*
 * @brief get the value of check_data_send
 */
bool Rf_tx_interface::get_check_data_send()
{
	return this->check_data_send;
}
