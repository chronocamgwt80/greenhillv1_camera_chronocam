#include <stdint.h>

#ifndef GOLDEN_FIR_BITSTREAM_FBSO_OUT_320x4
  #define GOLDEN_FIR_BITSTREAM_FBSO_OUT_320x4 1

  #define NB_GOLDEN_BYTES 320 * 4 //320 lines of 32 bits

  extern int8_t golden_FIR_bitstream[];
 
#endif //eof GOLDEN_FIR_BITSTREAM_FBSO_OUT_320x4
