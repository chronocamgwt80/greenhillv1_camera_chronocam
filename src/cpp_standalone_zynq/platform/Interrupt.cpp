/*
 * \File Interrupt.cpp
 *
 *  Created on: Apr 1, 2015
 *      Author: avialletelle
 */

/*
 * includes
 */
#include "Interrupt.h"
#include <stdio.h>

/*
 * @brief Initialize System On Chip Interrupt
 */
int interrupt_init( unsigned gic_id, XScuGic *gic )
{
	int status;
	XScuGic_Config *gic_config;

	printf("Initialize global interrupt system...\r\n");

	gic_config = XScuGic_LookupConfig( gic_id );
	if ( NULL == gic_config )
	{
		return XST_FAILURE;
	}

	status = XScuGic_CfgInitialize( gic, gic_config, gic_config->CpuBaseAddress );
	if ( status != XST_SUCCESS )
	{
		return XST_FAILURE;
	}

	status = interrupt_setup( gic );
	if ( status != XST_SUCCESS )
	{
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

/*
 *  @brief Set up interrupt system
 */
int interrupt_setup( XScuGic *gic )
{

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler( XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, gic );
	Xil_ExceptionEnable();
	return XST_SUCCESS;
}
