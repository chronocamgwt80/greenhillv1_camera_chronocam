/*
 * \file platform.c
 */

#include "xparameters.h"
#include "xil_cache.h"
#include <stdio.h>
#include "platform.h"

void enable_caches()
{
	printf("Caches are enabled...\r\n");
	Xil_DCacheEnable();
	Xil_ICacheEnable();
}

void disable_caches()
{
	printf("Caches are disabled...\r\n");
    Xil_DCacheDisable();
    Xil_ICacheDisable();
}
