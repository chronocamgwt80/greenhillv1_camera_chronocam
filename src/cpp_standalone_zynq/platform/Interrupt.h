/*
 * interrupt.h
 *
 *  Created on: Apr 1, 2015
 *      Author: avialletelle
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

/*
 * Includes
 */
#include "xscugic.h"

/*
 * Prototypes
 */
int interrupt_init( unsigned gic_id, XScuGic *gic );
int interrupt_setup( XScuGic *gic );


#endif /* INTERRUPT_H_ */
