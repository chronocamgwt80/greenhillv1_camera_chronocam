/*
 * \file platform.h
 */


#ifndef __PLATFORM_H_
#define __PLATFORM_H_

void enable_caches();
void disable_caches();

#endif
