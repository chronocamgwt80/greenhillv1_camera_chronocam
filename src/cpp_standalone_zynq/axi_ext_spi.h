/*
 * axi_ext_spi.h
 *
 *  Created on: May 4, 2016
 *      Author: sbreysse
 */

#ifndef AXI_EXT_SPI_H_
#define AXI_EXT_SPI_H_

#define EXT_SPI_BASEADDR XPAR_AXI_EXT_SPI_BASEADDR
#define EXT_SPI_CR_CLK_DIVIDER_128_MASK 0x00008000
#define EXT_SPI_CR_SS_SEL_1_MASK 0x00010000



#include "Axi_spi.h"

class Axi_ext_spi
{
public:
	Axi_ext_spi(uint32_t mode, uint32_t phase, uint32_t polarity);
	virtual ~Axi_ext_spi();
	void ext_write(uint8_t *data, uint32_t nbbytes);
	void ext_read(uint8_t *wr_data, uint8_t*rd_data, uint32_t nbbytes);
	void ext_read_all(uint8_t baseaddr, uint32_t nbaddr);

	void ext_spi_write(uint32_t data);
	uint32_t ext_spi_read(uint64_t data);

protected:
	uint32_t _mode;
	uint32_t _phase;
	uint32_t _polarity;
	uint32_t _config_cr;
	uint32_t _config_eof_reg;
	uint32_t _config_ext_spi_cr;
	void set_ext_as_master();
	void ext_enable_fifo();
	void ext_enable_master_transaction();
	void ext_enable_loopback();
	void ext_disable_loopback();
	void ext_wait_data_transmit();
	void ext_wait_eof();
	void write_eof_reg(uint32_t data);
	uint32_t read_eof_reg();
	void write_eof_reset();
	void write_eof_cntr(uint8_t data);

	void write_ext_spi_cr(uint32_t data);
	uint32_t read_ext_spi_cr();
	void write_ext_spi_fifo_in_data(uint64_t data);
	uint32_t read_ext_spi_fifo_out_data();
	uint32_t read_ext_spi_fifo_in_data();
	void set_ext_spi_as_master();
	void write_ext_spi_cr_rst();
	void write_ext_spi_cr_send_frame();
	void write_ext_spi_cr_send_frame_disable();
	void wait_ext_spi_eof();
	void wait_ext_spi_fifo_in_not_empty();
	void wait_ext_spi_fifo_out_not_empty();
	void wait_ext_spi_ss_low();
};

#endif /* AXI_EXT_SPI_H_ */
