/**
 * \file Axi_spi.h
 * Copyright (c) ASYGN S.A.S 2015
 */

#ifndef AXI_SPI_H_
#define AXI_SPI_H_

/*
 * Includes
 */
#include "xparameters.h"
#include "frame.h"
#include <vector>

typedef std::vector<uint8_t> uchar_container;

/*
 * Defines
 */
// base address
#define AXI_SPI_BASEADDR_0							XPAR_AXI_QUAD_SPI_0_BASEADDR
#define AXI_SPI_BASEADDR_1							XPAR_AXI_QUAD_SPI_1_BASEADDR
// register offset
#define AXI_SPI_DGIER_OFFSET						0x1C // Global Intr Enable Reg
#define AXI_SPI_IPISR_OFFSET						0x20 // Interrupt status Reg
#define AXI_SPI_IPIER_OFFSET						0x28 // Interrupt Enable Reg
#define AXI_SPI_SRR_OFFSET							0x40 // Software Reset register
#define AXI_SPI_SPICR_OFFSET						0x60 // Control register
#define AXI_SPI_SPISR_OFFSET						0x64 // Status register
#define AXI_SPI_DTR_OFFSET							0x68 // Data transmit
#define AXI_SPI_DRR_OFFSET							0x6C // Data receive
#define AXI_SPI_SSR_OFFSET							0x70 // 32-bit slave select
#define AXI_SPI_TFO_OFFSET							0x74 // Tx FIFO occupancy
#define AXI_SPI_RFO_OFFSET							0x78 // Rx FIFO occupancy
// Software Reset register flag
#define AXI_SRR_RESET_OP							0xa
#define AXI_SRR_CS_ENABLE                           0x00000000
#define AXI_SRR_CS_DISABLE                          0x00000001
#define AXI_SSR_SELECT_SLAVE						0x0000FFFE
#define AXI_SSR_UNSELECT_SLAVE						0x0000FFFF
// Control register flag
#define AXI_SPICR_LOOP_NORMAL_OP					0x00
#define AXI_SPICR_LOOP_LOOPBACK_MODE				0x00000001
#define AXI_SPICR_SPI_SYSTEM_DISABLE				0x00
#define AXI_SPICR_SPI_SYSTEM_ENABLE					0x02
#define AXI_SPICR_MASTER_DISABLE					0x00
#define AXI_SPICR_MASTER_ENABLE						0x04
#define AXI_SPICR_CLOCK_POLARITY_OFFSET				0x03
#define AXI_SPICR_CLOCK_POLARITY_0					0x00
#define AXI_SPICR_CLOCK_POLARITY_1					0x08
#define AXI_SPICR_CLOCK_POLARITY_0					0x00
#define AXI_SPICR_CLOCK_PHASE_OFFSET				0x04
#define AXI_SPICR_CLOCK_PHASE_0						0x00
#define AXI_SPICR_CLOCK_PHASE_1						0x10
#define AXI_SPICR_TX_FIFO_RESET_POINTER				0x20
#define AXI_SPICR_RX_FIFO_RESET_POINTER				0x40
#define AXI_SPICR_AUTO_SLAVE_SELECT					0x00
#define AXI_SPICR_MANUAL_SLAVE_SELECT				0x80
#define AXI_SPICR_MASTER_TRANSACTIONS_ENABLE		0x000
#define AXI_SPICR_MASTER_TRANSACTIONS_DISABLE		0x100
#define AXI_SPICR_MSB_FIRST							0x000
#define AXI_SPICR_LSB_FIRST							0x200
#define AXI_SPICR_TX_FIFO_EN                        0xFFFFFFDF
#define AXI_SPICR_RX_FIFO_EN                        0xFFFFFFBF
#define AXI_SPICR_MASTER_TRANSACTION_DISABLE_MASK   0xFFFFFEFF
#define AXI_SPICR_LOOP_LOOPBACK_MODE_DISABLE     	0xFFFFFFFE
// Status register mask
#define AXI_SPI_RX_FIFO_EMPTY_MASK					0x00000001
#define AXI_SPI_RX_FIFO_FULL_MASK					0x00000002
#define AXI_SPI_TX_FIFO_EMPTY_MASK					0x00000004
#define AXI_SPI_TX_FIFO_FULL_MASK					0x00000008
// Interrupt status flag
#define AXI_SPI_DDR_NOT_EMPTY						0x100	// RX FIFO not empty
#define ZERO_PHASE									0x0
#define ZERO_POLARITY								0x0
#define ONE_PHASE									0x1
#define ONE_POLARITY								0x1
// Interrupt status register mask
#define AXI_ISR_DTR_EMPTY                           0x00000004

enum
{
	is_slave,
	is_master,
	rx_fifo_empty,
	rx_fifo_not_empty,
	rx_fifo_full,
	rx_fifo_not_full,
	tx_fifo_empty,
	tx_fifo_not_empty,
	tx_fifo_full,
	tx_fifo_not_full
};

/*
 * @brief
 */
class Axi_spi
{
public:
	Axi_spi(uint32_t mode, uint32_t base_addr, uint32_t phase, uint32_t polarity);
	virtual ~Axi_spi();

	// read attributes
	uint32_t get_mode();

	// action
	void transfer(uint8_t *data, uint32_t nbbytes);
	void transfer_slow(uint8_t *data, uint32_t nbbytes);

	void receive(FRAME_STRUCTURE *frame);
	void select_slave();
	void unselect_slave();
	void slave_mode_test();
	void wait_for_n_bytes(FRAME_STRUCTURE *frame, unsigned nbbytes);
//	virtual void callback();

	// reset device/register
	void reset_device();
	void reset_rxtx_fifo();
	void reset_tx_fifo();

	// config
	void set_as_master();
	void set_as_slave();
	void global_irq_enable();
	void global_irq_disable();

	// status
	uint32_t rx_fifo_is_empty();
	uint32_t rx_fifo_is_full();
	uint32_t tx_fifo_is_empty();
	uint32_t tx_fifo_is_full();

protected:
	uint32_t mode;
	uint32_t base_addr;
	uint32_t phase;
	uint32_t polarity;
	// read register
	uint32_t read_ctrl_reg();
	uint32_t read_status_reg();
	uint8_t read_rx_fifo_reg();
	uint32_t read_slave_select_reg();
	uint32_t read_global_irq_en_reg();
	uint32_t read_irq_status_reg();
	uint32_t read_irq_en_reg();
	uint32_t read_rx_fifo_occupancy_reg();
	uint32_t read_tx_fifo_occupancy_reg();

	// set register
	void write_sw_reset_reg(uint32_t data);
	void write_ctrl_reg(uint32_t data);
	void write_tx_fifo_reg(uint8_t data);
	void write_slave_select_reg(uint32_t data);
	void write_global_irq_en_reg(uint32_t data);
	void write_irq_status_reg(uint32_t data);
	void write_irq_en_reg(uint32_t data);

};

#endif /* AXI_SPI_H_ */
