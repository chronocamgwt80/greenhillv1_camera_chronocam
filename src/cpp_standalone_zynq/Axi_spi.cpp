/**
 * \file Axi_spi.cpp
 * Copyright (c) ASYGN S.A.S 2015
 */

/*
 * Includes
 */
#include "Axi_spi.h"
#include "sleep.h"
#include "xspi.h"
#include <stdio.h>
#include <stdint.h>

/*
 * @brief
 */
Axi_spi::Axi_spi(uint32_t mode, uint32_t base_addr, uint32_t phase, uint32_t polarity)
{
	this->reset_device();
	this->mode = mode;
	this->base_addr = base_addr;
	this->phase = phase;
	this->polarity = polarity;

	// set spi config (master/slave, clock phase/polarity etc.)
	switch (this->mode) {
		case is_master:
			this->set_as_master();
			break;
		case is_slave:
			this->set_as_slave();
			break;
		default:
			break;
	}
}

/*
 * @brief
 */
Axi_spi::~Axi_spi()
{
	this->reset_device();
}

/*
 * @brief
 */
uint32_t Axi_spi::get_mode()
{
	return this->mode;
}

/*
 * @brief
 */
void Axi_spi::transfer(uint8_t *data, uint32_t nbbytes)
{
	uint32_t first_one_discard = 1;
	uint32_t index = 0;
	this->select_slave();
	usleep(1);
	for (index = 0; index < nbbytes; ++index)
	{
		this->write_tx_fifo_reg(*(data + index));
		// sleep required to sequence spi transfer with slave
		usleep(250);
		while(this->rx_fifo_is_empty() == rx_fifo_empty);
		if (first_one_discard == 1)
		{
			this->read_rx_fifo_reg();
			first_one_discard = 0;
		} else
		{
			*(data + index - 1) = this->read_rx_fifo_reg();
		}
	}

	// Null transfer to get last byte replied by the slave spi
	this->write_tx_fifo_reg(0x0);
	while(this->rx_fifo_is_empty() == rx_fifo_empty);
	*(data + index - 1) = this->read_rx_fifo_reg();

	usleep(10);
	this->unselect_slave();
}

/*
 * @brief
 */
void Axi_spi::transfer_slow(uint8_t *data, uint32_t nbbytes)
{
	this->select_slave();
	usleep(1);
	for (uint32_t index = 0; index < nbbytes; ++index)
	{
		this->write_tx_fifo_reg(*(data + index));
		// sleep required to sequence spi transfer with slave
		usleep(1000);
	}

	// Null transfer to get last byte replied by the slave spi
	this->write_tx_fifo_reg(0x0);

	usleep(1000);
	this->unselect_slave();
	// read response
	// discard the first one
	this->read_rx_fifo_reg();
	for (uint32_t index = 0; index < nbbytes; ++index)
	{
		*(data + index) = this->read_rx_fifo_reg();
	}
//	usleep(100);

}

/*
 * @brief
 */
void Axi_spi::receive(FRAME_STRUCTURE *frame)
{
	uint8_t din;
	if(this->rx_fifo_is_empty() == rx_fifo_not_empty)
	{
		din = this->read_rx_fifo_reg();
		if (frame->index == 3)
		{
			frame->rd_wrb = (din & 0x80) >>7;
			*((uint8_t*)frame+frame->index) = din & 0x7F;
		} else
		{
			*((uint8_t*)frame+frame->index) = din;
		}
		//printf("RX=0x%x,%d\n", *((uint8_t*)frame+frame->index),frame->index);
		frame->index++;
		if ((frame->rd_wrb) && (frame->index == 6))
		{
			frame->to_decode = 1;
		} else if ((!frame->rd_wrb) && (frame->index > 6) && (frame->index == (6+frame->nbbytes+2) ))
		{
			frame->to_decode = 1;
		} else
		{
			frame->to_decode = 0;
		}
	}
}

void  Axi_spi::wait_for_n_bytes(FRAME_STRUCTURE *frame, unsigned nbbytes)
{
	unsigned k;
	for (k=0;k<nbbytes;k++)
	{
		while(this->rx_fifo_is_empty() == rx_fifo_empty);
		this->read_rx_fifo_reg();
		frame->index ++;
	}
}

/*
 * @brief test the device as slave with a remote master SPI
 */
void Axi_spi::slave_mode_test()
{
	uint8_t data = 0;

	printf("SPI device as slave is testing... \n\r");
	printf("Use it to test your SPI communication with a remote master SPI\n\r");

	this->global_irq_disable();
	// you have to fill TX fifo before start transaction to respond something to the SPI master
	// if it is not done, it goes to data abort handler
	//this->write_tx_fifo_reg(0xAA);

	while(1)
	{
		printf("-------------------------------------------\n\r");
		printf("Control register = 0x%x\n\r", (unsigned int) this->read_ctrl_reg());
		printf("Status register = 0x%x\n\r", (unsigned int) this->read_status_reg());
		printf("IRQ Status register = 0x%x\n\r", (unsigned int) this->read_irq_status_reg());

		while( this->rx_fifo_is_empty() != rx_fifo_empty)
		{
			data = this->read_rx_fifo_reg();
			printf("RX fifo = 0x%x\n\r", data);

			//this->write_tx_fifo_reg(0xAA);
		}
		sleep(1);
	}
}

/*
 * @brief
 */
void Axi_spi::reset_device()
{
	this->write_sw_reset_reg(AXI_SRR_RESET_OP);
	usleep(100);
	this->reset_rxtx_fifo();
}

/*
 * @brief
 */
void Axi_spi::reset_rxtx_fifo()
{
	// reset fifo
	this->write_ctrl_reg( this->read_ctrl_reg()
						| XSP_CR_TXFIFO_RESET_MASK
						| XSP_CR_RXFIFO_RESET_MASK
						| XSP_CR_ENABLE_MASK);
}

/*
 * @brief
 */
void Axi_spi::reset_tx_fifo()
{
	// reset fifo
	this->write_ctrl_reg( this->read_ctrl_reg()
						| XSP_CR_TXFIFO_RESET_MASK);
}
/*
 * @brief
 */
void Axi_spi::set_as_master()
{
	this->write_ctrl_reg( AXI_SPICR_MSB_FIRST
						| AXI_SPICR_MASTER_TRANSACTIONS_ENABLE
						| AXI_SPICR_MANUAL_SLAVE_SELECT
						| AXI_SPICR_RX_FIFO_RESET_POINTER
						| AXI_SPICR_TX_FIFO_RESET_POINTER
						| ((this->phase & 0x1) << AXI_SPICR_CLOCK_PHASE_OFFSET)
						| ((this->polarity & 0x1) << AXI_SPICR_CLOCK_POLARITY_OFFSET)
						| AXI_SPICR_MASTER_ENABLE
						| AXI_SPICR_SPI_SYSTEM_ENABLE
						| AXI_SPICR_LOOP_NORMAL_OP);
}

/*
 * @brief
 */
void Axi_spi::set_as_slave()
{
	this->write_ctrl_reg( AXI_SPICR_MSB_FIRST
						| AXI_SPICR_MASTER_TRANSACTIONS_DISABLE
						| AXI_SPICR_MANUAL_SLAVE_SELECT
						| AXI_SPICR_RX_FIFO_RESET_POINTER
						| AXI_SPICR_TX_FIFO_RESET_POINTER
						| ((this->phase & 0x1) << AXI_SPICR_CLOCK_PHASE_OFFSET)
						| ((this->polarity & 0x1) << AXI_SPICR_CLOCK_POLARITY_OFFSET)
						| AXI_SPICR_MASTER_DISABLE
						| AXI_SPICR_SPI_SYSTEM_ENABLE
						| AXI_SPICR_LOOP_NORMAL_OP);
}

/*
 * @brief
 */
void Axi_spi::select_slave()
{
	write_slave_select_reg(AXI_SSR_SELECT_SLAVE);
}

/*
 * @brief
 */
void Axi_spi::unselect_slave()
{
	write_slave_select_reg(AXI_SSR_UNSELECT_SLAVE);
}


/*
 * @brief
 */
void Axi_spi::global_irq_enable()
{
	// Interrupts enabled using XSpi_IntrEnable() will not occur until the global
	// interrupt enable bit is set by using this function.
	this->write_global_irq_en_reg(XSP_GINTR_ENABLE_MASK);
}

/*
 * @brief
 */
void Axi_spi::global_irq_disable()
{
	this->write_global_irq_en_reg(0);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_ctrl_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_SPICR_OFFSET);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_status_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_SPISR_OFFSET);
}

/*
 * @brief
 */
uint8_t Axi_spi::read_rx_fifo_reg()
{
	return (uint8_t) (*(uint32_t*) (this->base_addr + AXI_SPI_DRR_OFFSET));
}

/*
 * @brief
 */
uint32_t Axi_spi::read_slave_select_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_SSR_OFFSET);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_global_irq_en_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_DGIER_OFFSET);
}


/*
 * @brief
 * @return
 * 		rx_fifo_empty
 * 		rx_fifo_not_empty
 */
uint32_t Axi_spi::rx_fifo_is_empty()
{
	uint32_t ret = AXI_SPI_RX_FIFO_EMPTY_MASK;
	if (ret == (this->read_status_reg() & AXI_SPI_RX_FIFO_EMPTY_MASK))
	{
		ret = rx_fifo_empty;
	} else
	{
		ret = rx_fifo_not_empty;
	}
	return ret;
}

/*
 * @brief
 * @return
 * 		rx_fifo_full
 * 		rx_fifo_not_full
 */
uint32_t Axi_spi::rx_fifo_is_full()
{
	uint32_t ret = AXI_SPI_RX_FIFO_EMPTY_MASK;
	if (ret == (this->read_status_reg() & AXI_SPI_RX_FIFO_FULL_MASK))
	{
		ret = rx_fifo_full;
	} else
	{
		ret = rx_fifo_not_full;
	}
	return ret;
}

/*
 * @brief
 * @return
 * 		tx_fifo_empty
 * 		tx_fifo_not_empty
 */
uint32_t Axi_spi::tx_fifo_is_empty()
{
	uint32_t ret = AXI_SPI_TX_FIFO_EMPTY_MASK;
	if (ret == (this->read_status_reg() & AXI_SPI_TX_FIFO_EMPTY_MASK))
	{
		ret = tx_fifo_empty;
	} else
	{
		ret = tx_fifo_not_empty;
	}
	return ret;
}

/*
 * @brief
 * 		tx_fifo_full
 * 		tx_fifo_not_full
 */
uint32_t Axi_spi::tx_fifo_is_full()
{
	uint32_t ret = AXI_SPI_TX_FIFO_FULL_MASK;
	if (ret == (this->read_status_reg() & AXI_SPI_TX_FIFO_FULL_MASK))
	{
		ret = tx_fifo_full;
	} else
	{
		ret = tx_fifo_not_full;
	}
	return ret;
}

/*
 * @brief
 */
uint32_t Axi_spi::read_irq_status_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_IPISR_OFFSET);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_irq_en_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_IPIER_OFFSET);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_rx_fifo_occupancy_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_IPIER_OFFSET);
}

/*
 * @brief
 */
uint32_t Axi_spi::read_tx_fifo_occupancy_reg()
{
	return *(uint32_t*) (this->base_addr + AXI_SPI_IPIER_OFFSET);
}

/*
 * @brief
 */
void Axi_spi::write_sw_reset_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_SRR_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_ctrl_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_SPICR_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_tx_fifo_reg(uint8_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_DTR_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_slave_select_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_SSR_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_global_irq_en_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_DGIER_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_irq_status_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_IPISR_OFFSET) = data;
}

/*
 * @brief
 */
void Axi_spi::write_irq_en_reg(uint32_t data)
{
	*(uint32_t*) (this->base_addr + AXI_SPI_IPIER_OFFSET) = data;
}


