/*
 * cdma.c
 *
 *  Created on: Mar 20, 2015
 *      Author: avialletelle
 */

#include "cdma.h"
#include "stdio.h"
#include "bram_controller.h"

extern BramSystemConfiguration BramSystemConfig;
CdmaSRregister cdmaSRregister;

/*
 * Initialize the CDMA in simple transfer mode
 */
int cdma_init(XScuGic *gic) {

	int ret = 0;

    printf("Initialize CDMA...\n\r");

    cdma_structures_init();

	// verify CDMASR.IDLE = 1
	//ret = Xil_In32( XPAR_AXI_CDMA_0_BASEADDR + 0x4 );
	//asygn_debug_msg(2, "CDMASR.IDLE  = 0x%x\n\r", ret);


	// CDMACR.IOC_IrqEN and CMDACR.ERR_IrqEn if desired
	ret = Xil_In32( XPAR_AXI_CDMA_0_BASEADDR + 0x0 );
	ret = ret | 0x1000;
	Xil_Out32( XPAR_AXI_CDMA_0_BASEADDR + 0x0, ret );
	// CMDACR.ERR_IrqEn  is not set yet


	// CDMA interrupt binding
    if( cdma_interrupt_init(gic, XPAR_FABRIC_AXI_CDMA_0_CDMA_INTROUT_INTR) != ASYGN_SUCCESS ) {
	   xil_printf("CDMA interrupt initialization failure !\r\n");
	   return ASYGN_FAILURE;
    }


	return ASYGN_SUCCESS;
}

/*
 * @brief
 */
void cdma_structures_init()
{
    cdmaSRregister.delay_irq_enable = 0;
    cdmaSRregister.ilde = 0;
    cdmaSRregister.irq_on_complete_enable = 0;
    cdmaSRregister.irq_on_complete_enable = 0;
    cdmaSRregister.normal_mode_decode_err = 0;
    cdmaSRregister.normal_mode_irq_err = 0;
    cdmaSRregister.normal_mode_slave_err = 0;
    cdmaSRregister.scatter_gather_decode_err = 0;
    cdmaSRregister.scatter_gather_include = 0;
    cdmaSRregister.scatter_gather_irq_delay_status = 0;
    cdmaSRregister.scatter_gather_irq_err = 0;
    cdmaSRregister.scatter_gather_irq_threshold_status = 0;
    cdmaSRregister.scatter_gather_slave_err = 0;
}

/**
 * Start the cdma transfer
 * - write source address in SA register
 * - write destination address to DA register
 * - write the number of bytes to transfer in BTT register
 * => the CDMA starts when the number of bytes to transfer is set
 */
void cdma_start_transfer( uint32_t srcAddress, uint32_t dstAddress, uint32_t len) {

	BramSystemConfig.cdma_is_running = 1;

	*(u32*) (XPAR_AXI_CDMA_0_BASEADDR + 0x18) = (u32) srcAddress;
	*(u32*) (XPAR_AXI_CDMA_0_BASEADDR + 0x20) = (u32) dstAddress;
	*(u32*) (XPAR_AXI_CDMA_0_BASEADDR + 0x28) = (u32) len;


//	printf("srcAddress = 0x%08x\r\n", *(u32*) (XPAR_AXI_CDMA_0_BASEADDR + 0x18));
//	printf("dstAddress = 0x%08x\r\n", *(u32*) (XPAR_AXI_CDMA_0_BASEADDR + 0x20));

//	Xil_Out32( XPAR_AXI_CDMA_0_BASEADDR + 0x18, srcAddress );

//	Xil_Out32( XPAR_AXI_CDMA_0_BASEADDR + 0x20, dstAddress );
//	Xil_Out32( XPAR_AXI_CDMA_0_BASEADDR + 0x28, len );
}


/**
 * Initialize and enable CDMA interruption
 */
int cdma_interrupt_init(XScuGic *IntcInstancePtr, u16 IntrId) {

	int ret = 0;
	ret = XScuGic_Connect( IntcInstancePtr, IntrId, (Xil_ExceptionHandler) cmda_interrupt_handler, NULL );

	if ( ret != XST_SUCCESS ) {
		return ASYGN_FAILURE;
	}


	u8 priority = 0;
	u8 trigger = 1;

	XScuGic_SetPriorityTriggerType(IntcInstancePtr,  IntrId, priority, trigger);
	XScuGic_GetPriorityTriggerType(IntcInstancePtr, IntrId , &priority, &trigger);
	XScuGic_Enable(IntcInstancePtr, IntrId);

	return ASYGN_SUCCESS;
}


/**
 * CDMA interrupt handler
 */
void cmda_interrupt_handler (void) {

//	BramSystemConfig.cdma_irq_counter++;

	cdma_clear_interrupt();
	AXI_CDMA_SR_REG |= 0x1000;
	AXI_BRAM_CTRL_REG1  |= AXI_REG_BRAM_CTRL_REG_1_cdma_transfert_done_MASK;
	BramSystemConfig.cdma_is_running = 0;

//	// error checking
//	uint32_t reg = AXI_CDMA_SR_REG;
//	// check DMAIntErr
//	if ( (reg & 0x0010) == 1 ) {
//		//error
//		BramSystemConfig.cdma_error++;
//	}
//
//	// check DMASlvErr
//	if ( (reg & 0x0020) == 1 ) {
//		//error
//		BramSystemConfig.cdma_error++;
//	}
//
//	// check DMADecErr
//	if ( (reg & 0x0040) == 1 ) {
//		//error
//		BramSystemConfig.cdma_error++;
//	}
//
//	// check Err_Irq
//	if ( (reg & 0x4000) == 1 ) {
//		//error
//		BramSystemConfig.cdma_error++;
//	}

//	printf(">>>> CDMA done <<<<\n\r");
//	printf("buff addr : 0x%x\r\n", ((uint32_t*)(BramSystemConfig.ddr_buf_start_address)));
//	for (uint32_t index = 0; index < 2*8192+1; ++index )
//	{
//		if (index %10 == 0){
//			printf("\n\r");
//		}
//
//		if(index == 8192){
//			printf("Start second bram\n\r");
//		}
//
//		printf("0x%08x\t", *(((uint32_t*)BramSystemConfig.ddr_buf_start_address)+index));
//
//	}
//	printf("\r\n");
//	printf("\r\n");
//	printf("\r\n");
//	printf("\r\n");



}

/*
 * @brief
 */
void cdma_clear_interrupt() {
	// clear interrupt, just perform a write to bit no. 12 of CDMASR.IOC_Irq
	AXI_CDMA_SR_REG |= 0x1000;

	// send cdma done to the bram ctrl
	bram_controller_read_config();
	BramSystemConfig.cdma_transfert_done = 1;
	bram_controller_apply_config();

}

/*
 * @brief
 */
void cdma_read_sr_register()
{
	uint32_t reg = 0;

	reg = AXI_CDMA_SR_REG;
    cdmaSRregister.delay_irq_enable = reg & AXI_CDMA_SR_IDLE_MASK;
    cdmaSRregister.ilde = reg & AXI_CDMA_SR_SG_INCLD_MASK;
    cdmaSRregister.irq_on_complete_enable = reg & AXI_CDMA_SR_IRQ_ERR_MASK;
    cdmaSRregister.irq_on_complete_enable = reg & AXI_CDMA_SR_SLAVE_ERR_MASK;
    cdmaSRregister.normal_mode_decode_err = reg & AXI_CDMA_SR_DEC_ERR_MASK;
    cdmaSRregister.normal_mode_irq_err = reg & AXI_CDMA_SR_SG_IRQ_ERR_MASK;
    cdmaSRregister.normal_mode_slave_err = reg & AXI_CDMA_SR_SG_SLAVE_ERR_MASK;
    cdmaSRregister.scatter_gather_decode_err = reg & AXI_CDMA_SR_SG_DEC_ERR_MASK;
    cdmaSRregister.scatter_gather_include = reg & AXI_CDMA_SR_IOC_IRQ_MASK;
    cdmaSRregister.scatter_gather_irq_delay_status = reg & AXI_CDMA_SR_DELAY_IRQ_MASK;
    cdmaSRregister.scatter_gather_irq_err = reg & AXI_CDMA_SR_ERR_IRQ_MASK;
    cdmaSRregister.scatter_gather_irq_threshold_status = reg & AXI_CDMA_SR_IRQ_THRESHOLD_STS_MASK;
    cdmaSRregister.scatter_gather_slave_err = reg &AXI_CDMA_SR_IRQ_DELAY_STS_MASK;

    printf("*** CDMA STATUS REGISTER ***\r\n");
    printf("delay_irq_enable = 0x%x\r\n", (unsigned int) cdmaSRregister.delay_irq_enable);
    printf("ilde = 0x%x\r\n", (unsigned int) cdmaSRregister.ilde);
    printf("irq_on_complete_enable = 0x%x\r\n", (unsigned int) cdmaSRregister.irq_on_complete_enable);
    printf("normal_mode_decode_err = 0x%x\r\n", (unsigned int) cdmaSRregister.normal_mode_decode_err);
    printf("normal_mode_irq_err = 0x%x\r\n", (unsigned int) cdmaSRregister.normal_mode_irq_err);
    printf("normal_mode_slave_err = 0x%x\r\n", (unsigned int) cdmaSRregister.normal_mode_slave_err);
    printf("scatter_gather_decode_err = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_decode_err);
    printf("scatter_gather_include = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_include);
    printf("scatter_gather_irq_delay_status = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_irq_delay_status);
    printf("scatter_gather_irq_err = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_irq_err);
    printf("scatter_gather_irq_threshold_status = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_irq_threshold_status);
    printf("scatter_gather_slave_err = 0x%x\r\n", (unsigned int) cdmaSRregister.scatter_gather_slave_err);
}

