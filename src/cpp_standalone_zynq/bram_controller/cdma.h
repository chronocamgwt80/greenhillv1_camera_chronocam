/*
 * cdma.h
 *
 *  Created on: Mar 20, 2015
 *      Author: avialletelle
 */

#ifndef CDMA_H_
#define CDMA_H_

#include "xaxicdma.h"
#include "../platform/Interrupt.h"
#include <stdint.h>
/**
 * Prototypes
 */
int cdma_init(XScuGic *gic);
void cdma_start_transfer( uint32_t srcAddress, uint32_t dstAddress, uint32_t len);
int cdma_interrupt_init(XScuGic *IntcInstancePtr, u16 IntrId);
void cmda_interrupt_handler (void);
void cdma_clear_interrupt();
void cdma_read_sr_register();
void cdma_structures_init();

#define AXI_CDMA_CR_REG 			(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_CR_OFFSET)) 		//Control register
#define AXI_CDMA_SR_REG 			(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_SR_OFFSET))  		// Status register
#define AXI_CURDESC_PNTR_REG 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_CDESC_OFFSET)) 	// Current descriptor pointer
#define AXI_CURDESC_PNTR_MSB_REG 	(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + 0xC))
#define AXI_TAILDESC_PNT_REG 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_TDESC_OFFSET))	// Tail descriptor pointer
#define AXI_SC_PNTR_MSB_REG 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + 0x14))
#define AXI_CDMA_SA_REG 			(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_SRCADDR_OFFSET))	// Source address register
#define AXI_CDMA_SA_MSB_REG 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + 0x1C))
#define AXI_CDMA_DA_REG 			(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_DSTADDR_OFFSET))	// Destination address register
#define AXI_CDMA_DA_MSB_REG 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + 0x24))
#define AXI_CDMA_BTT_REG	 		(*(uint32_t*)(XPAR_AXI_CDMA_0_BASEADDR + XAXICDMA_BTT_OFFSET))		// Bytes to transfer


typedef struct CdmaSRregister CdmaSRregister;
struct CdmaSRregister {
	uint32_t ilde;
	uint32_t scatter_gather_include;
	uint32_t normal_mode_irq_err;
	uint32_t normal_mode_slave_err;
	uint32_t normal_mode_decode_err;
	uint32_t scatter_gather_irq_err;
	uint32_t scatter_gather_slave_err;
	uint32_t scatter_gather_decode_err;
	uint32_t irq_on_complete_enable;
	uint32_t delay_irq_enable;
	uint32_t scatter_gather_irq_threshold_status;
	uint32_t scatter_gather_irq_delay_status;
};

#define AXI_CDMA_SR_IDLE_MASK				0x2			// 0 = Not Idle – Simple or SG DMA operations are in progress.
														// 1 = Idle – Simple or SG operations completed or not started
#define AXI_CDMA_SR_SG_INCLD_MASK			0x8			// 0 = Scatter Gather not included. Only Simple DMA operations are supported.
														// 1 = Scatter Gather is included. Both Simple DMA and Scatter Gather operations are supported
#define AXI_CDMA_SR_IRQ_ERR_MASK			0x10		// 0 = No CDMA Internal Errors.
														// 1 = CDMA Internal Error detected. CDMA Engine halts
#define AXI_CDMA_SR_SLAVE_ERR_MASK			0x20		// 0 = No CDMA Slave Errors.
														// 1 = CDMA Slave Error detected. CDMA Engine halts
#define AXI_CDMA_SR_DEC_ERR_MASK			0x40		// 0 = No CDMA Decode Errors.
														// 1 = CDMA Decode Error detected. CDMA Engine halts
#define AXI_CDMA_SR_SG_IRQ_ERR_MASK			0x100		// 0 = No SG Internal Errors
														// 1 = SG Internal Error detected. CDMA Engine halts
#define AXI_CDMA_SR_SG_SLAVE_ERR_MASK		0x200   	// 0 = No SG Slave Errors
														// 1 = SG Slave Error detected. CDMA Engine halts
#define AXI_CDMA_SR_SG_DEC_ERR_MASK			0x400		// 0 = No SG Decode Errors
														// 1 = SG Decode Error detected. CDMA Engine halts
#define AXI_CDMA_SR_IOC_IRQ_MASK			0x1000  	// 0 = No IOC Interrupt
														// 1 = IOC Interrupt active
#define AXI_CDMA_SR_DELAY_IRQ_MASK			0x2000		// 0 = No Delay Interrupt
														// 1 = Delay Interrupt active
#define AXI_CDMA_SR_ERR_IRQ_MASK			0x4000		// 0 = No error Interrupt
														// 1 = Error interrupt active
#define AXI_CDMA_SR_IRQ_THRESHOLD_STS_MASK	0xFF000000	// Interrupt Threshold Status: This field reflects the current interrupt threshold value in the SG Engine.
#define AXI_CDMA_SR_IRQ_DELAY_STS_MASK		0x00FF0000	// Interrupt Delay Time Status: This field reflects the current interrupt delay timer value in the SG Engine



#endif /* CDMA_H_ */
