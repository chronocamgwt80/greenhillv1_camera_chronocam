/*
 * bram_controller.c
 *
 *  Created on: Mar 25, 2015
 *      Author: avialletelle
 */

#include "bram_controller.h"
#include "cdma.h"
#include <stdlib.h>
#include <stdio.h>
#include "xil_types.h"
#include "xscugic.h"
#include <math.h>

/*
 * Structures
 */
BramSystemConfiguration BramSystemConfig;
uint32_t bram_buffer[RF_RX_ACQ_SAMPLES_NB_MAX];

/*
 * Initialize the BRAM Greyscale:
 *   - perform a write to bit no. 0 of AXI_REG__BRAM_CTRL_REG register to release the reset
 *   - bind the interrupt number with the Greyscale interrupt handler
 */
int bram_controller_init(XScuGic *gic) {

	// structure initializing
	printf("Initialize BRAM controller...\r\n");

	BramSystemConfig.nb_half_bram_per_acq = (unsigned)(ceilf(((double)(RF_RX_ACQ_SAMPLES_NB_MAX)/(double)HALF_BRAM_SIZE_IN_SAMPLES)));
	BramSystemConfig.last_half_bram_samples_nb = (RF_RX_ACQ_SAMPLES_NB_MAX % HALF_BRAM_SIZE_IN_SAMPLES);
	BramSystemConfig.bram_resetb = 1;
	BramSystemConfig.bram_acquisition_en = 0;
	BramSystemConfig.bram_overflow_flag_clear = 0;
	BramSystemConfig.bram_overflow_flag = 0;
	BramSystemConfig.bram_irq_clear = 0;
	BramSystemConfig.bram_polarity = 0;
	BramSystemConfig.bram_packet_timestamp = 0;
	BramSystemConfig.cdma_transfert_done = 0;
	BramSystemConfig.bram_fill_threshold = (((BramSystemConfig.nb_half_bram_per_acq - 1) * 2) << 14) | BramSystemConfig.last_half_bram_samples_nb;
	BramSystemConfig.emulator_en = 0;
	BramSystemConfig.bram_src_address = BRAM_FIRST_HALF_BASE_ADDRESS;
	BramSystemConfig.bram_controller_irq_counter = 0;
	BramSystemConfig.cdma_is_running = 0;
	BramSystemConfig.cdma_error = 0;
	BramSystemConfig.bram_timestamp = 0;
	BramSystemConfig.dma_transfert_cnt = 0;
	BramSystemConfig.ddr_buf_start_address = (uint32_t)(&bram_buffer[0]);
	BramSystemConfig.bram_error = 0;
	BramSystemConfig.cdma_irq_counter = 0;
	BramSystemConfig.bram_previous_polarity = READ_SECOND_HALF_BRAM;

	bram_buffers_init();

	// release the reset
	bram_controller_apply_config();


	// interrupt binding
    if( bram_controller_initialize_interrupt(gic, XPAR_FABRIC_SYSTEM_IRQ_PL_INTR) != ASYGN_SUCCESS ) {
	   printf("BRAM Greyscale interrupt initialization failure !\n\r");
	   return ASYGN_FAILURE;
    }

    bram_controller_read_config();

	return ASYGN_SUCCESS;
}


void bram_buffers_init(void)
{

	memset((uint8_t*)bram_buffer, 0x55, sizeof(bram_buffer[0]) * RF_RX_ACQ_SAMPLES_NB_MAX);
}

/*
 * Start the BRAM Greyscale
 * - perform a write to bit no. 1 to AXI_REG__BRAM_CTRL_REG register
 */
void bram_controller_start() {
	printf("*** Start Bram Controller, current configuration : \r\n");

	// get current config
	bram_controller_read_config();

	// update config
//	BramSystemConfig.nb_half_bram_per_acq = (unsigned)(ceilf(((double)(RF_RX_ACQ_SAMPLES_NB_MAX)/(double)HALF_BRAM_SIZE_IN_SAMPLES)));
//	BramSystemConfig.last_half_bram_size_bytes = (RF_RX_ACQ_SAMPLES_NB_MAX % HALF_BRAM_SIZE_IN_SAMPLES);
//	BramSystemConfig.bram_resetb = 1;
	BramSystemConfig.bram_acquisition_en = 1;
	BramSystemConfig.bram_overflow_flag_clear = 0;
	BramSystemConfig.bram_overflow_flag = 0;
	BramSystemConfig.bram_irq_clear = 0;
	BramSystemConfig.bram_polarity = 0;
//	BramSystemConfig.bram_packet_timestamp = 0; // do not reset it, because rtl signal is never reseted
	BramSystemConfig.cdma_transfert_done = 0;
//	BramSystemConfig.bram_fill_threshold = ((BramSystemConfig.nb_half_bram_per_acq * 2) << 14) | BramSystemConfig.last_half_bram_size_bytes;
//	BramSystemConfig.emulator_en = 0;
	BramSystemConfig.bram_src_address = BRAM_FIRST_HALF_BASE_ADDRESS;
	BramSystemConfig.bram_controller_irq_counter = 0;
	BramSystemConfig.cdma_is_running = 0;
	BramSystemConfig.cdma_error = 0;
//	BramSystemConfig.bram_timestamp = 0; // do not reset it, because rtl signal is never reseted
	BramSystemConfig.dma_transfert_cnt = 0;
//	BramSystemConfig.ddr_buf_start_address = (uint32_t)bram_buffer;
	BramSystemConfig.bram_error = 0;
	BramSystemConfig.cdma_irq_counter  = 0;
	BramSystemConfig.bram_previous_polarity = READ_SECOND_HALF_BRAM;

	// display config
	bram_controller_log();

	// init bram buffer
	bram_buffers_init();

	// apply config and start the bram controller
	bram_controller_apply_config();
}

/*
 * Stop the BRAM Greyscale
 * - perform a write to bit no. 1 to AXI_REG__BRAM_CTRL_REG register
 */
void bram_controller_stop()
{
	// stop bram controller
//	BramSystemConfig.nb_half_bram_per_acq = (unsigned)(ceilf(((double)(RF_RX_ACQ_SAMPLES_NB_MAX-1)/(double)HALF_BRAM_SIZE_IN_SAMPLES)));
//	BramSystemConfig.last_half_bram_size_bytes = (RF_RX_ACQ_SAMPLES_NB_MAX % HALF_BRAM_SIZE_IN_SAMPLES);
//	BramSystemConfig.bram_resetb = 1;
	BramSystemConfig.bram_acquisition_en = 0;
//	BramSystemConfig.bram_overflow_flag_clear = 0;
//	BramSystemConfig.bram_overflow_flag = 0;
//	BramSystemConfig.bram_irq_clear = 0;
//	BramSystemConfig.bram_polarity = 0;
//	BramSystemConfig.bram_packet_timestamp = 0; // do not reset it, because rtl signal is never reseted
//	BramSystemConfig.cdma_transfert_done = 0; //oneshot
//	BramSystemConfig.bram_fill_threshold = ((BramSystemConfig.nb_half_bram_per_acq * 2) << 14) | BramSystemConfig.last_half_bram_size_bytes ;
	BramSystemConfig.emulator_en = 0;
//	BramSystemConfig.bram_src_address = BRAM_FIRST_HALF_BASE_ADDRESS;
//	BramSystemConfig.bram_controller_irq_counter = 0;
//	BramSystemConfig.cdma_is_running = 0;
//	BramSystemConfig.bram_timestamp = 0; // do not reset it, because rtl signal is never reseted
//	BramSystemConfig.dma_transfert_cnt = 0;
//	BramSystemConfig.ddr_buf_start_address = (uint32_t)bram_buffer;
//	BramSystemConfig.bram_error = 0;
//	BramSystemConfig.cdma_irq_counter = 0;
//	BramSystemConfig.bram_previous_polarity = READ_SECOND_HALF_BRAM;
	bram_controller_apply_config();

	// display config
	bram_controller_read_config();
	printf("*** Stop Bram Controller, current configuration : \r\n");
	bram_controller_log();
}

/*
 * Initialize and enable BRAM Greyscale interrupt
 */
int bram_controller_initialize_interrupt(XScuGic *IntcInstancePtr, u16 IntrId)
{
	int ret = 0;

	ret = XScuGic_Connect(IntcInstancePtr, IntrId, (Xil_ExceptionHandler) bram_controller_interrupt_handler, NULL );
	if ( ret != XST_SUCCESS ) {
		return XST_FAILURE;
	}

	u8 priority = 8;
	u8 trigger = 1;

	XScuGic_SetPriorityTriggerType(IntcInstancePtr, IntrId, priority, trigger);
	XScuGic_GetPriorityTriggerType(IntcInstancePtr, IntrId , &priority, &trigger);
	XScuGic_Enable(IntcInstancePtr, IntrId);

	return ASYGN_SUCCESS;
}

/*
 * Callback function when BRAM Greyscale interrupt appears
 */
void bram_controller_interrupt_handler()
{
	uint32_t reg0 = AXI_BRAM_CTRL_REG0;

	// clear interrupt
	reg0 = reg0 | AXI_REG_BRAM_CTRL_REG_0_bram_irq_clear_MASK;
	AXI_BRAM_CTRL_REG0 = reg0;
	BramSystemConfig.bram_controller_irq_counter++;

	// get config
	BramSystemConfig.bram_overflow_flag = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_OFFSET);
	BramSystemConfig.bram_polarity = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_polarity_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_polarity_OFFSET);
	BramSystemConfig.bram_packet_timestamp = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_packet_timestamp_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_packet_timestamp_OFFSET);

	// Check if CDMA is not running
	if ( BramSystemConfig.cdma_is_running == 1) {
		BramSystemConfig.cdma_error++;
		printf(">>>> Error : CDMA still running <<<<\n");
		return;
	}

	// check bram overflow
	if( BramSystemConfig.bram_overflow_flag != 0)
	{
		AXI_BRAM_CTRL_REG0 |= AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_clear_MASK;
		BramSystemConfig.bram_error++;
		printf(">>>> Error : Bram overflow detected : %d <<<<\n", (int) BramSystemConfig.bram_overflow_flag);
	}

	// check  timestamp
	if (BramSystemConfig.bram_packet_timestamp - BramSystemConfig.bram_timestamp != 1 )
	{
		BramSystemConfig.bram_error++;
		printf(">>>> Error : Bram timestamp wrong value detected : %d <<<<\n", (int) BramSystemConfig.bram_packet_timestamp);
	}
	BramSystemConfig.bram_timestamp = BramSystemConfig.bram_packet_timestamp;

	// check polarity
	if (BramSystemConfig.bram_polarity != BramSystemConfig.bram_previous_polarity)
	{
		// no error, good polarity
	} else
	{
		BramSystemConfig.bram_error++;
		printf(">>>> Error : Bad bram polarity detected : %d, Bram timestamp : %d<<<<\n", (int) BramSystemConfig.bram_polarity, (int) BramSystemConfig.bram_packet_timestamp);
	}
	BramSystemConfig.bram_previous_polarity = BramSystemConfig.bram_polarity;

	// set the section of the BRAM to read
	if( BramSystemConfig.bram_polarity == READ_SECOND_HALF_BRAM)
	{
		BramSystemConfig.bram_src_address = BRAM_SECOND_HALF_BASE_ADDRESS;
	}
	else if( BramSystemConfig.bram_polarity == READ_FIRST_HALF_BRAM)
	{
		BramSystemConfig.bram_src_address = BRAM_FIRST_HALF_BASE_ADDRESS;
	}
	else
	{
		bram_controller_stop();
		return;
	}

	// start DMA
	if (BramSystemConfig.dma_transfert_cnt == BramSystemConfig.nb_half_bram_per_acq-1 ) //
	{
		bram_controller_stop();

		cdma_start_transfer(BramSystemConfig.bram_src_address, BramSystemConfig.ddr_buf_start_address + (BramSystemConfig.dma_transfert_cnt * HALF_BRAM_SIZE_IN_BYTES), (BramSystemConfig.last_half_bram_samples_nb*4));
		BramSystemConfig.dma_transfert_cnt++;
	}
	else
	{
		cdma_start_transfer(BramSystemConfig.bram_src_address, BramSystemConfig.ddr_buf_start_address + (BramSystemConfig.dma_transfert_cnt * HALF_BRAM_SIZE_IN_BYTES), HALF_BRAM_SIZE_IN_BYTES);
		BramSystemConfig.dma_transfert_cnt++;
	}
}


void bram_controller_apply_config()
{

	AXI_BRAM_CTRL_REG0 = ((BramSystemConfig.bram_resetb << AXI_REG_BRAM_CTRL_REG_0_bram_resetb_OFFSET) & AXI_REG_BRAM_CTRL_REG_0_bram_resetb_MASK) \
						| ((BramSystemConfig.bram_acquisition_en  << AXI_REG_BRAM_CTRL_REG_0_bram_acquisition_en_OFFSET) & AXI_REG_BRAM_CTRL_REG_0_bram_acquisition_en_MASK) \
						| ((BramSystemConfig.bram_overflow_flag_clear << AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_clear_OFFSET) & AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_clear_MASK) \
						| ((BramSystemConfig.bram_irq_clear << AXI_REG_BRAM_CTRL_REG_0_bram_irq_clear_OFFSET) & AXI_REG_BRAM_CTRL_REG_0_bram_irq_clear_MASK);
	AXI_BRAM_CTRL_REG1  = ((BramSystemConfig.cdma_transfert_done << AXI_REG_BRAM_CTRL_REG_1_cdma_transfert_done_OFFSET) & AXI_REG_BRAM_CTRL_REG_1_cdma_transfert_done_MASK)  \
						| ((BramSystemConfig.bram_fill_threshold  << AXI_REG_BRAM_CTRL_REG_1_bram_fill_threshold_OFFSET) & AXI_REG_BRAM_CTRL_REG_1_bram_fill_threshold_MASK) \
						| ((BramSystemConfig.emulator_en << AXI_REG_BRAM_CTRL_REG_1_emulator_en_OFFSET) & AXI_REG_BRAM_CTRL_REG_1_emulator_en_MASK);

}

void bram_controller_read_config()
{
	uint32_t reg0 = AXI_BRAM_CTRL_REG0;
	uint32_t reg1 = AXI_BRAM_CTRL_REG1;

	BramSystemConfig.bram_resetb = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_resetb_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_resetb_OFFSET);
	BramSystemConfig.bram_acquisition_en = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_acquisition_en_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_acquisition_en_OFFSET);
	BramSystemConfig.bram_overflow_flag_clear = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_clear_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_clear_OFFSET);
	BramSystemConfig.bram_overflow_flag = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_overflow_flag_OFFSET);
	BramSystemConfig.bram_irq_clear = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_irq_clear_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_irq_clear_OFFSET);
	BramSystemConfig.bram_polarity = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_polarity_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_polarity_OFFSET);
	BramSystemConfig.bram_packet_timestamp = ((reg0 & AXI_REG_BRAM_CTRL_REG_0_bram_packet_timestamp_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_packet_timestamp_OFFSET);
	BramSystemConfig.cdma_transfert_done = ((reg1 & AXI_REG_BRAM_CTRL_REG_1_cdma_transfert_done_MASK) >> AXI_REG_BRAM_CTRL_REG_1_cdma_transfert_done_OFFSET);
	BramSystemConfig.bram_fill_threshold = ((reg1 & AXI_REG_BRAM_CTRL_REG_1_bram_fill_threshold_MASK) >> AXI_REG_BRAM_CTRL_REG_1_bram_fill_threshold_OFFSET);
	BramSystemConfig.emulator_en = ((reg1 & AXI_REG_BRAM_CTRL_REG_1_emulator_en_MASK) >> AXI_REG_BRAM_CTRL_REG_1_emulator_en_OFFSET);
}

void bram_controller_log()
{
	printf("bram_acquisition_en = 0x%x\r\n", (int) BramSystemConfig.bram_acquisition_en);
	printf("bram_controller_irq_counter = 0x%x\r\n", (int) BramSystemConfig.bram_controller_irq_counter);
	printf("bram_fill_threshold = 0x%x\r\n", (int) BramSystemConfig.bram_fill_threshold);
	printf("bram_irq_clear = 0x%x\r\n", (int) BramSystemConfig.bram_irq_clear);
	printf("bram_overflow_flag = 0x%x\r\n", (int) BramSystemConfig.bram_overflow_flag);
	printf("bram_overflow_flag_clear = 0x%x\r\n", (int) BramSystemConfig.bram_overflow_flag_clear);
	printf("bram_packet_timestamp = 0x%x\r\n", (int) BramSystemConfig.bram_packet_timestamp);
	printf("bram_polarity = 0x%x\r\n", (int) BramSystemConfig.bram_polarity);
	printf("bram_resetb = 0x%x\r\n", (int) BramSystemConfig.bram_resetb);
	printf("bram_src_address = 0x%x\r\n", (int) BramSystemConfig.bram_src_address);
	printf("bram_timestamp = 0x%x\r\n", (int) BramSystemConfig.bram_timestamp);
	printf("cdma_error = 0x%x\r\n", (int) BramSystemConfig.cdma_error);
	printf("cdma_is_running = 0x%x\r\n", (int) BramSystemConfig.cdma_is_running);
	printf("cdma_transfert_done = 0x%x\r\n", (int) BramSystemConfig.cdma_transfert_done);
	printf("ddr_buf_start_address = 0x%x\r\n", (int) BramSystemConfig.ddr_buf_start_address);
	printf("dma_transfert_cnt = 0x%x\r\n", (int) BramSystemConfig.dma_transfert_cnt);
	printf("emulator_en = 0x%x\r\n", (int) BramSystemConfig.emulator_en);
	printf("last_half_bram_samples_nb = 0x%x\r\n", (int) BramSystemConfig.last_half_bram_samples_nb);
	printf("nb_half_bram_per_acq = 0x%x\r\n", (int) BramSystemConfig.nb_half_bram_per_acq);
	printf("bram_error = 0x%x\r\n", (int) BramSystemConfig.bram_error);
	printf("cdma_irq_counter = 0x%x\r\n", (int) BramSystemConfig.cdma_irq_counter);
}
