/*
 * bram_controller.h
 *
 *  Created on: Mar 25, 2015
 *      Author: avialletelle
 */

#ifndef BRAM_GREYSCALE_H_
#define BRAM_GREYSCALE_H_


/**
 * Includes
 */
#include "../axi_registers.h"
#include <stdint.h>
#include "../platform/Interrupt.h"
#include "xparameters.h"
/**
 * Structures
 */

// device frame response
typedef struct ddrImageBufferCtrl ddrImageBufferCtrl;
struct ddrImageBufferCtrl{
    char Buff0_status;
    char Buff0_CDMA_lock;
    char Buff0_COM_lock;
};

typedef struct BramSystemConfiguration BramSystemConfiguration;
struct BramSystemConfiguration {
	uint32_t bram_resetb;
	uint32_t bram_acquisition_en;
	uint32_t bram_overflow_flag_clear;
	uint32_t bram_overflow_flag;
	uint32_t bram_irq_clear;
	uint32_t bram_polarity;
	uint32_t bram_packet_timestamp;
	uint32_t cdma_transfert_done;
	uint32_t bram_fill_threshold;
	uint32_t emulator_en;
	uint32_t bram_src_address;
	uint32_t bram_controller_irq_counter;
	uint32_t cdma_is_running;
	uint32_t cdma_error;
	uint32_t bram_timestamp;
	uint32_t dma_transfert_cnt;
	uint32_t ddr_buf_start_address;
	uint32_t last_half_bram_samples_nb;
	uint32_t nb_half_bram_per_acq;
	uint32_t bram_error;
	uint32_t cdma_irq_counter;
	uint32_t bram_previous_polarity;
};


/**
 * registers
 */
#define AXI_BRAM_CTRL_REG0 	AXI_REG_BRAM_CTRL_REG_0
#define AXI_BRAM_CTRL_REG1 	AXI_REG_BRAM_CTRL_REG_1


/**
 * Prototypes
 */
int bram_controller_init(XScuGic *gic);
void bram_buffers_init(void);
void bram_controller_start();
void bram_controller_stop();
int bram_controller_initialize_interrupt(XScuGic *IntcInstancePtr, u16 IntrId);
void bram_controller_interrupt_handler();
void bram_controller_apply_config();
void bram_controller_read_config();
void bram_controller_log();

#endif /* BRAM_GREYSCALE_H_ */
