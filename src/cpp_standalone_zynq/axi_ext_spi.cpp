/*
 * axi_ext_spi.cpp
 *
 *  Created on: May 4, 2016
 *      Author: sbreysse
 */

#include "axi_ext_spi.h"
#include <stdio.h>
#include "sleep.h"
#include "axi_registers.h"

Axi_ext_spi::Axi_ext_spi(uint32_t mode, uint32_t phase, uint32_t polarity)
{
	// Reset SPI IP (software reset):
	//this->write_sw_reset_reg(AXI_SRR_RESET_OP);
	_mode = mode;
	_phase = phase;
	_polarity = polarity;

	//Global Interrupt Enable:
	//Enables all individually enabled interrupts to be passed to the interrupt controller.
	//this->write_global_irq_en_reg(0x80000000);

	//Data Transmit Register/FIFO Empty Interrupt Enable:
	//this->write_irq_en_reg(AXI_ISR_DTR_EMPTY);

	// Set SPI default configuration (master/slave, clock phase/polarity etc.)
	switch (_mode) {
		case is_master:
			//this->set_ext_as_master();
			this->set_ext_spi_as_master();
			break;
		case is_slave:
			//this->set_as_slave();
			break;
		default:
			break;
		}

}

Axi_ext_spi::~Axi_ext_spi()
{

}

/*
 * @brief SPI EOF Detector module, write register
 */
void Axi_ext_spi::write_eof_reg(uint32_t data)
{
	AXI_REG_EXT_SPI_CTRL = data;
}

/*
 * @brief SPI EOF Detector module, write register
 */
uint32_t Axi_ext_spi::read_eof_reg()
{
	return (AXI_REG_EXT_SPI_CTRL);
}

/*
 * @brief SPI EOF Detector module, reset module (one shot)
 */
void Axi_ext_spi::write_eof_reset()
{
	_config_eof_reg = this->read_eof_reg();
	_config_eof_reg = _config_eof_reg | AXI_REG_EXT_SPI_CTRL_ext_spi_ctrl_reset_MASK;
	this->write_eof_reg(_config_eof_reg);
}

/*
 * @brief SPI EOF Detector module, write counter value = frame number of bytes
 */
void Axi_ext_spi::write_eof_cntr(uint8_t data)
{
	_config_eof_reg = this->read_eof_reg();
	_config_eof_reg = _config_eof_reg | AXI_REG_EXT_SPI_CTRL_ext_spi_byte_cntr_MASK;
	_config_eof_reg  =_config_eof_reg & (data<<16);
	this->write_eof_reg(_config_eof_reg);
}

/*
 * @brief Write external SPI Control Register
 */
void Axi_ext_spi::write_ext_spi_cr(uint32_t data)
{
	AXI_REG_EXT_SPI_CR = data;
}

/*
 * @brief Read external SPI control register
 */
uint32_t Axi_ext_spi::read_ext_spi_cr()
{
	return (AXI_REG_EXT_SPI_CR);
}

/*
 * @brief Read external SPI FIFO out register 24 bits length
 */
uint32_t Axi_ext_spi::read_ext_spi_fifo_out_data()
{
	return (AXI_REG_EXT_SPI_FIFO_OUT);
}


uint32_t Axi_ext_spi::read_ext_spi_fifo_in_data()
{
	return (AXI_REG_EXT_SPI_FIFO_IN);
}


/*
 * @brief Write external SPI FIFO in register 24 bits length
 */
void Axi_ext_spi::write_ext_spi_fifo_in_data(uint64_t data)
{
	AXI_REG_EXT_SPI_FIFO_IN = (data | 0x01000000);
}

/*
 * @brief: Master default configuration
 * - MSB First
 * - RX and TX FIFO reset activated
 * - Master transaction disabled
 */
void Axi_ext_spi::set_ext_spi_as_master()
{
	_config_ext_spi_cr =  0x00000000 | AXI_REG_EXT_SPI_CR_ext_spi_cr_rst_DEFAULT
						             | AXI_REG_EXT_SPI_CR_ext_spi_cr_cpol_MASK
						             | AXI_REG_EXT_SPI_CR_ext_spi_cr_cpha_MASK
						             | AXI_REG_EXT_SPI_CR_ext_spi_cr_endian_DEFAULT
									 | AXI_REG_EXT_SPI_CR_ext_spi_cr_fifo_in_empty_DEFAULT
									 | AXI_REG_EXT_SPI_CR_ext_spi_cr_fifo_in_full_DEFAULT
									 | AXI_REG_EXT_SPI_CR_ext_spi_cr_fifo_out_empty_DEFAULT
									 | AXI_REG_EXT_SPI_CR_ext_spi_cr_send_frame_DEFAULT
									 | EXT_SPI_CR_CLK_DIVIDER_128_MASK
									 | EXT_SPI_CR_SS_SEL_1_MASK;

	this->write_ext_spi_cr(_config_ext_spi_cr);
	this->write_ext_spi_cr_rst();
	//printf("Results, addr: 0, 0x%x \n", (unsigned)_config_ext_spi_cr);
}

/*
 * @brief
 */
void Axi_ext_spi::write_ext_spi_cr_rst()
{
	_config_ext_spi_cr = _config_ext_spi_cr | AXI_REG_EXT_SPI_CR_ext_spi_cr_rst_MASK;
	this->write_ext_spi_cr(_config_ext_spi_cr);
	_config_ext_spi_cr = _config_ext_spi_cr & 0xFFFFFFFE;
}

/*
 * @brief
 */
void Axi_ext_spi::write_ext_spi_cr_send_frame()
{
	_config_ext_spi_cr = _config_ext_spi_cr | AXI_REG_EXT_SPI_CR_ext_spi_cr_send_frame_MASK;
	this->write_ext_spi_cr(_config_ext_spi_cr);
}

/*
 * @brief
 */
void Axi_ext_spi::write_ext_spi_cr_send_frame_disable()
{
	_config_ext_spi_cr = _config_ext_spi_cr & 0xFFFFFF7F;
	this->write_ext_spi_cr(_config_ext_spi_cr);
}



/*
 * @brief:
 */
void Axi_ext_spi::wait_ext_spi_eof()
{
	uint32_t flag_fifo_in_empty = 0x0;
	uint32_t flag_ss_high = 0x0;

	while( (flag_fifo_in_empty != 0x00000010) && (flag_ss_high != 0xff000000) )
	{
		flag_fifo_in_empty = this->read_ext_spi_cr() & 0x00000010;
		flag_ss_high = this->read_ext_spi_cr() & 0xff000000;

	}
}

void Axi_ext_spi::wait_ext_spi_fifo_in_not_empty()
{
	uint32_t flag_fifo_in_not_empty = 0x00000010;

	while(flag_fifo_in_not_empty == 0x00000010 )
	{
		flag_fifo_in_not_empty = this->read_ext_spi_cr() & 0x00000010;
	}
}

void Axi_ext_spi::wait_ext_spi_fifo_out_not_empty()
{
	uint32_t flag_fifo_not_empty = 0x00000040;

	while(flag_fifo_not_empty == 0x00000040 )
	{
		flag_fifo_not_empty = this->read_ext_spi_cr() & 0x00000040;

	}
}

void Axi_ext_spi::wait_ext_spi_ss_low()
{
	uint32_t flag_ss_high = 0xff000000;

	while(flag_ss_high == 0xff000000 )
	{
		flag_ss_high = this->read_ext_spi_cr() & 0xff000000;

	}
}

/*
 * @brief: Send data through external SPI
 */
void Axi_ext_spi::ext_spi_write(uint32_t data)
{
	this->write_ext_spi_cr_rst();

	this->write_ext_spi_fifo_in_data(data);

	this->wait_ext_spi_fifo_in_not_empty();

	this->write_ext_spi_cr_send_frame();

	this->wait_ext_spi_ss_low();

	this->wait_ext_spi_eof();

	this->write_ext_spi_cr_send_frame_disable();
}

/*
 * @brief: Send data through external SPI
 */


uint32_t Axi_ext_spi::ext_spi_read(uint64_t data)

{
	        this->wait_ext_spi_ss_low();

	        this->write_ext_spi_cr_rst();

	    	this->write_ext_spi_fifo_in_data(data);

	    	this->wait_ext_spi_fifo_in_not_empty();

	    	this->write_ext_spi_cr_send_frame();

	    	this->wait_ext_spi_eof();

	    	this->write_ext_spi_cr_send_frame_disable();

	    	this->wait_ext_spi_fifo_out_not_empty();

	    	return this->read_ext_spi_fifo_out_data();

//	this->write_ext_spi_cr_rst();
//	uint32_t data1 = data & 0xFFFFFF;
//	this->write_ext_spi_fifo_in_data(data);
//	this->wait_ext_spi_fifo_in_not_empty();
//	this->write_ext_spi_cr_send_frame();
//	uint32_t read1 = this->read_ext_spi_fifo_out_data();
//
//	uint32_t data2 = (data >> 24) & 0xFFFFFF;
//	this->write_ext_spi_fifo_in_data(data2);
//	this->write_ext_spi_cr_send_frame();
//	uint32_t read2 = this->read_ext_spi_fifo_out_data();
//
//	uint32_t data3 = (data >> 48) & 0xFFFFFF;
//	this->write_ext_spi_fifo_in_data(data3);
//	this->write_ext_spi_cr_send_frame();
//	uint32_t read3 = this->read_ext_spi_fifo_out_data();
//
//	this->wait_ext_spi_ss_low();
//	this->wait_ext_spi_eof();
//	this->write_ext_spi_cr_send_frame_disable();
//
//	printf("read1=0x%lx\nread2=0x%lx\nread3=0x%lx\n", read1, read2, read3);
//	return this->read_ext_spi_fifo_out_data();
}

