/*
 * Copyright 2008 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

/* The path memory for each state is 32 bits. This is slightly shorter
 * than we'd like for K=7, especially since we chain back every 8 bits.
 * But it fits so nicely into a 32-bit machine word...
 */

//#include "api.h"
#ifndef _DEFINE_VITERBI_
#define _DEFINE_VITERBI_

#include <stdint.h>
#include "tab.h"
#define	K 7			/* Constraint length */
#define N 2			/* Number of symbols per data bit */
struct viterbi_state {
  uint32_t path;	/* Decoded path to this state */
  int32_t metric;		/* Cumulative metric to this state */
};

//FEC_API
void gen_met(int32_t mettab[2][256],	/* Metric table */
	    int32_t amp,		/* Signal amplitude */
	    float esn0,	/* Es/N0 ratio in dB */
	    float bias, 	/* Metric bias */
	    int32_t scale);		/* Scale factor */

//FEC_API 
uint8_t
encode(uint8_t *symbols, uint8_t *data,
       uint32_t nbytes,uint8_t encstate);

//FEC_API 
void
viterbi_chunks_init(struct viterbi_state* state);

//FEC_API 
void
viterbi_butterfly2(uint8_t *symbols, int32_t mettab[2][256],
		   struct viterbi_state *state0, struct viterbi_state *state1);

//FEC_API 
uint8_t
viterbi_get_output(struct viterbi_state *state, uint8_t *outbuf);

//
uint8_t
encode_bits(uint8_t *symbols,
       uint8_t data,
       uint32_t nbits,
       uint8_t encstate);

int32_t
viterbi(uint32_t *metric,	/* Final path metric (returned value) */
	uint8_t *data,	/* Decoded output data */
	uint8_t *symbols,	/* Raw deinterleaved input symbols */
	uint32_t nbits,	/* Number of output bits */
	int32_t mettab[2][256]	/* Metric table, [sent sym][rx symbol] */
	);

#endif //_DEFINE_VITERBI_
