/*
 * command.h
 *
 *  Created on: Dec 29, 2015
 *      Author: avialletelle
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include <stdint.h>

void get_hw_version();
void get_sw_version();
void get_modulation_ready_flag();
void rf_tx_start_self_test();
void rf_tx_start_transmission();
void rf_tx_stop_transmission();
void rf_tx_reset();
void rf_tx_loop_burst_mode(uint8_t mode);
void prepare_data_to_modulate(int8_t* data, uint32_t len);
void start_modulation();
void get_set_cmd_from_python();
void get_modulated_data_length();
void rf_tx_get_buffer_index();
void gwt_configure();
void rf_rx_acq_config();
void rf_rx_acq_start();
void rf_rx_acq_read_samples();
void cmd_user_led();
void rf_rx_sync_flag();
void drive_gw_power_user_leds();
void rf_rx_force_emulator(uint8_t value);
void rf_rx_reset(uint8_t value);

#endif /* COMMAND_H_ */
