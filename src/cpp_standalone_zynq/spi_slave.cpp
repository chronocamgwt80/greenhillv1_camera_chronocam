/*
 * spi.cpp
 *
 *  Created on: Oct 5, 2015
 *      Author: cleblanc
 */


#include "spi_slave.h"
#include "config.h"
#include "axi_registers.h"
#include "stdint.h"

/*
 * @brief
 */
spi_slave::spi_slave()
{
	reset_tx_fifo();
	reset_rx_fifo();
}
/*
 * @brief
 */
spi_slave::~spi_slave()
{

}

/*
 * @brief
 */
void spi_slave::wait_for_data(unsigned nbbytes, FRAME_STRUCTURE *frame)
{
	for(unsigned k=0;k<nbbytes;k++)
	{
		while (AXI_REG_CONFIG_SPI_STATUS & AXI_REG_CONFIG_SPI_STATUS_config_spi_rx_empty_flag_MASK);
		this->get_data(frame);
	}
}

/*
 * @brief
 */
void spi_slave::get_data(FRAME_STRUCTURE *frame)
{
	uint8_t din;

	// get byte
	din = AXI_REG_CONFIG_SPI_RD;
	if (frame->index == 3)
	{
		// check msb bit: 1 = read, 0 = write
		frame->rd_wrb = (din & 0x80) >>7;
		// get data, msb bit is masked
		*((uint8_t*)frame+frame->index) = din & 0x7F;
	} else
	{
		*((uint8_t*)frame+frame->index) = din;
	}

	// increment counter
	frame->index++;

	// test if command decoding is necessary, slave answer making will proceeded in command decoding
	if ((frame->rd_wrb) && (frame->index == 6))
	{
		frame->to_decode = 1;
	} else if ((!frame->rd_wrb) && (frame->index > 6) && (frame->index == (FRAME_HEADER_SIZE+frame->nbbytes+FRAME_FOOTER_SIZE) ))
	{
		frame->to_decode = 1;
	} else
	{
		frame->to_decode = 0;
	}
}

/*
 * @brief
 */
void spi_slave::reset_tx_fifo(void)
{
	AXI_REG_CONFIG_SPI_CONFIG |= AXI_REG_CONFIG_SPI_CONFIG_config_spi_tx_fifo_reset_MASK;
}

/*
 * @brief
 */
void spi_slave::reset_rx_fifo(void)
{
	AXI_REG_CONFIG_SPI_CONFIG |= AXI_REG_CONFIG_SPI_CONFIG_config_spi_rx_fifo_reset_MASK;
}

/*
 * @brief
 */
void spi_slave::tx_fifo_write(uint8_t data)
{
	AXI_REG_CONFIG_SPI_WR = (data & 0xFF) | AXI_REG_CONFIG_SPI_WR_config_spi_wr_MASK;
}
