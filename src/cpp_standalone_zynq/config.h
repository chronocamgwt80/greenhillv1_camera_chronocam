/*
 * \file config.h
 *
 *  Created on: Sep 25 2015
 *      Author: avialletelle
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include "xparameters.h"
#include "axi_registers.h"
#include "Ps_gpio.h"
#include "Axi_spi.h"
/*
 * Platform
 */
#define STDOUT_IS_PS7_UART
// ARM_FIRMWARE_VERSION
#define ARM_FIRMWARE_VERSION 	 						0x2
// PLATFROM
#define UART_DEVICE_ID 			 						0
#define INTC_BASE_ADDR 			 						XPAR_SCUGIC_CPU_BASEADDR
#define INTC_DIST_BASE_ADDR 	 						XPAR_SCUGIC_DIST_BASEADDR
#define TIMER_IRPT_INTR			 						XPAR_SCUTIMER_INTR
#define RESET_RX_CNTR_LIMIT 	 						400
// IRQ GLOBAL SYSTEM
#define INTERRUPT_CONTROLLER_ID  						(XPAR_SCUGIC_SINGLE_DEVICE_ID)
// TIMER
#define TIMER_DEVICE_ID			 						XPAR_SCUTIMER_DEVICE_ID
#define TIMER_IRQ_ID 			 						XPAR_SCUTIMER_INTR
// GPIO
#define USER_LED 				 						MIO_0
// AXI REGISTER MAP
#define AXI_REGMAP_BASEADDR 	 						XPAR_S_AXI_BASEADDR
// CONFIG SPI
#define CONFIG_SPI_BASEADDR 							AXI_SPI_BASEADDR_0
// USB
#define USB_COM_ID 										XPAR_XUSBPS_0_DEVICE_ID
// PS user LED
#define ON												0x0
#define OFF												0x1
// MODULES ADDRESS
#define NUCLEO_CMD_BASEADDR 							0x00000000
#define NUCLEO_CMD_ADDR_RANGE 							0x0000FFFF
#define NUCLEO_CMD_HIGHADDR 							NUCLEO_CMD_BASEADDR + NUCLEO_CMD_ADDR_RANGE
#define RF_CMD_BASEADDR                         		0x00010000
#define RF_CMD_ADDR_RANGE                       		0x0001FFFF
#define RF_CMD_HIGHADDR                         		RF_CMD_BASEADDR + RF_CMD_ADDR_RANGE
#define RF0_CMD_BASEADDR                        		0x00010000
#define RF0_CMD_ADDR_RANGE                      		0x0000FFFF
#define RF0_CMD_HIGHADDR                        		RF0_CMD_BASEADDR + RF0_CMD_ADDR_RANGE
#define RF1_CMD_BASEADDR                        		0x00020000
#define RF1_CMD_ADDR_RANGE                      		0x0000FFFF
#define RF1_CMD_HIGHADDR                        		RF1_CMD_BASEADDR + RF1_CMD_ADDR_RANGE
#define ZYNQ_CMD_BASEADDR 								0x000100000
#define ZYNQ_CMD_ADDR_RANGE 							0x0000FFFFF
#define ZYNQ_CMD_HIGHADDR 								ZYNQ_CMD_BASEADDR + ZYNQ_CMD_ADDR_RANGE
// COMMANDS
#define ZYNQ_CMD_GET_HARDWARE_VERSION       			ZYNQ_CMD_BASEADDR + 1
#define ZYNQ_CMD_GET_SOFTWARE_VERSION      				ZYNQ_CMD_BASEADDR + 2
#define ZYNQ_CMD_USER_LED                   			ZYNQ_CMD_BASEADDR + 3
#define ZYNQ_CMD_RF_TX_START_SELF_TEST      			ZYNQ_CMD_BASEADDR + 4
#define ZYNQ_CMD_RF_TX_FILL_BUFFER          			ZYNQ_CMD_BASEADDR + 5
#define ZYNQ_CMD_START_RF_TX_TRANSMISSION  			 	ZYNQ_CMD_BASEADDR + 6
#define ZYNQ_CMD_STOP_RF_TX_TRANSMISSION   			 	ZYNQ_CMD_BASEADDR + 7
#define ZYNQ_CMD_GET_IQ_FROM_FILE          			 	ZYNQ_CMD_BASEADDR + 8
#define ZYNQ_CMD_ADVISE_DATA_ON_FEW_FRAMES 			 	ZYNQ_CMD_BASEADDR + 9
#define ZYNQ_CMD_RF_TX_RESET							ZYNQ_CMD_BASEADDR + 10
#define ZYNQ_CMD_RF_TX_LOOP_BURST_MODE					ZYNQ_CMD_BASEADDR + 11
#define ZYNQ_CMD_RF_TX_PREPARE_DATA_TO_MODULATE			ZYNQ_CMD_BASEADDR + 12
#define ZYNQ_CMD_RF_TX_START_MODULATION				    ZYNQ_CMD_BASEADDR + 13
#define ZYNQ_CMD_RF_TX_GET_BUFFER_INDEX					ZYNQ_CMD_BASEADDR + 14
#define ZYNQ_CMD_GWT_CONFIGURE							ZYNQ_CMD_BASEADDR + 15
#define ZYNQ_CMD_GET_MODULATION_READY_FLAG				ZYNQ_CMD_BASEADDR + 16
#define ZYNQ_CMD_RF_TX_MODULATED_DATA_LENGTH			ZYNQ_CMD_BASEADDR + 17
#define ZYNQ_CMD_RF_RX_ACQ_CONFIG						ZYNQ_CMD_BASEADDR + 18
#define ZYNQ_CMD_RF_RX_ACQ_START						ZYNQ_CMD_BASEADDR + 19
#define ZYNQ_CMD_RF_RX_ACQ_READ_SAMPLES					ZYNQ_CMD_BASEADDR + 20
#define ZYNQ_CMD_RF_RX_SYNC_FLAG						ZYNQ_CMD_BASEADDR + 21
#define ZYNQ_CMD_GW_POWER_USER_LEDS              		ZYNQ_CMD_BASEADDR + 22
#define ZYNQ_CMD_EN_CHECK_MODULATION_DATA				ZYNQ_CMD_BASEADDR + 23
//beg debug1.part1.sect3 added for debug TX loop mode, to be deleted
#define ZYNQ_CMD_READ_CSV_BUFFER                        ZYNQ_CMD_BASEADDR + 24
//end debug1.part1.sect3
#define ZYNQ_CMD_RF_RX_FORCE_EMULATOR					ZYNQ_CMD_BASEADDR + 25
#define ZYNQ_CMD_RF_RX_RESET							ZYNQ_CMD_BASEADDR + 26
#define ZYNQ_CMD_FROM_PYTHON                            ZYNQ_CMD_BASEADDR + 27

/*
 * Status
 */
#define success 										0x0
#define failure											0x1
#define ASYGN_SUCCESS									0xAA
#define ASYGN_WARNING									0xA5
#define ASYGN_FAILURE									0x55
/*
 * RF TX
 */
#define RF_TX_BUFFER_WR_MAX_DEPTH						131072
#define RF_TX_DATA_FRAME_MAX_NBBYTES					(RF_TX_BUFFER_WR_MAX_DEPTH * 4)
#define RF_TX_NO_ZERO_SAMPLES							0
#define ZYNQ_RF_TX_LOOP_MODE 							1
#define ZYNQ_RF_TX_BURST_MODE 							0
#define ZYNQ_RF_TX_GWT_MOD_ENABLE 						1
#define ZYNQ_RF_TX_GWT_MOD_DISABLE 						0
/*
 * RF RX
 */
// CDMA/BRAM Controller constants
#define XPAR_BRAM_0_BASEADDR							0xC0000000
#define XPAR_BRAM_0_HIGHADDR							0xC000FFFF
#define BRAM_ADDRESS_RANGE								( XPAR_BRAM_0_HIGHADDR - XPAR_BRAM_0_BASEADDR )
#define BRAM_SIZE_IN_BYTES								( BRAM_ADDRESS_RANGE+1 )
#define BRAM_SIZE_IN_PIXELS								( BRAM_SIZE_IN_BYTES/2)
#define HALF_BRAM_SIZE_IN_BYTES							( BRAM_SIZE_IN_BYTES / 2 )
#define HALF_BRAM_SIZE_IN_SAMPLES						( HALF_BRAM_SIZE_IN_BYTES / 4 )
#define BRAM_FIRST_HALF_BASE_ADDRESS					( XPAR_BRAM_0_BASEADDR )
#define BRAM_SECOND_HALF_BASE_ADDRESS					( XPAR_BRAM_0_BASEADDR + HALF_BRAM_SIZE_IN_BYTES )
#define LOCK  											0x4C // "L"
#define OPEN  											0x4F // "O"
#define EMPTY 											0x45 // "E"
#define FULL 											0x46 // "F"
#define READ_BUFF_0     								1
#define READ_BUFF_1     								3
#define READ_BUFF_2    					 				5
#define READ_ONCE_AGAIN 								7
#define GRAB_NB_FRAME 									1     // to be defined by the user
#define RF_RX_ACQ_TIME_MAX_NS							250000000
#define RF_RX_SAMPLE_TRANSFERT_TIME_NS					250
#define RF_RX_ACQ_SAMPLES_NB_MAX						(RF_RX_ACQ_TIME_MAX_NS/RF_RX_SAMPLE_TRANSFERT_TIME_NS) //250ms (1 samples  32bits  250ns)
#define READ_FIRST_HALF_BRAM							1
#define READ_SECOND_HALF_BRAM							0
#define USB_PACKET_DIVIDER								8192*4
#define SERDES_FORCE_EMULATOR_STATE						1
#define SERDES_RELEASE_EMULATOR_STATE					0
#define RF_RX_FORCE_RESET								0
#define RF_RX_RELEASE_RESET								1
/*
 * USB CONFIG
 */
#define ZYNQ_DEVICE_VID   								0x5151
#define ZYNQ_DEVICE_PID   								0x0001
#define ZYNQ_EP_OUT       								0x01
#define ZYNQ_EP_IN        								0x81
#define ZYNQ_MANUFACTURER 								"ASYGN"
#define ZYNQ_PRODUCT_NAME 								"ASYGN - greenhill"
#define ISERIAL     	   								"000000000001"
#define BCDDEVICE          								0x200
#define ICONFIG            								0x00
#define MAXPOWER           								0x32
#define BINTERFACECLASS    								0xFF
#define BINTERFACESUBCLASS 								0xFF
#define BINTERFACEPROTOCOL 								0xFF
#define IINTERFACE		   								0x00
#endif /* CONFIG_H_ */
