/*
 * frame.h
 *
 *  Created on: Sep 30, 2015
 *      Author: cleblanc
 */

#ifndef FRAME_H_
#define FRAME_H_

#include <stdint.h>

#define MAX_FRAME_NBBYTES 				1024 * 1024 * 4


#define FRAME_HEADER_ADDRESS_SIZE		4
#define FRAME_HEADER_NBBYTES_SIZE		4
#define FRAME_FOOTER_CHECKSUM_SIZE		1
#define FRAME_FOOTER_STOP_BYTE_SIZE		1
#define FRAME_HEADER_SIZE				(FRAME_HEADER_ADDRESS_SIZE + FRAME_HEADER_NBBYTES_SIZE)
#define FRAME_FOOTER_SIZE				(FRAME_FOOTER_CHECKSUM_SIZE + FRAME_FOOTER_STOP_BYTE_SIZE)
#define FRAME_OVERHEAD_SIZE 			(FRAME_HEADER_SIZE+FRAME_FOOTER_SIZE)
#define FRAME_DEFAULT_CHECKSUM			0
#define FRAME_STOP_BYTE					0x55
#define FRAME_RD_WRB_BIT_MASK           0x80000000

// frame structure
typedef struct {
	uint32_t  	address;
	uint32_t  	nbbytes;
	uint8_t 	data[MAX_FRAME_NBBYTES];
	uint8_t  	rd_wrb;
	uint32_t  	index;
	uint8_t 	full;
	uint8_t 	to_decode;
} FRAME_STRUCTURE;


#endif /* FRAME_H_ */
