/*
 * \file Ps_gpio.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: avialletelle
 */

#include "Ps_gpio.h"

/*
 * @brief
 */
Ps_gpio::Ps_gpio(uint32_t gpio_id)
{
	this->gpio_id = gpio_id;
	this->value = 0;
	this->init();
}

/*
 * @brief
 */
Ps_gpio::~Ps_gpio()
{

}


/**
 * @brief initializing
 */
void Ps_gpio::init() {
	this->ps_gpio.GpioConfig.DeviceId = XPAR_PS7_GPIO_0_DEVICE_ID;
	this->ps_gpio.GpioConfig.BaseAddr = XPAR_PS7_GPIO_0_BASEADDR;
	XGpioPs_CfgInitialize(&this->ps_gpio, &this->ps_gpio.GpioConfig, XPAR_PS7_GPIO_0_BASEADDR);
}

/**
 * @brief set gpio as output
 */
void Ps_gpio::set_as_output() {
	XGpioPs_SetDirectionPin(&this->ps_gpio, this->gpio_id, GPIO_AS_OUTPUT);
	XGpioPs_SetOutputEnablePin(&this->ps_gpio, this->gpio_id, GPIO_OUPUT_ENABLE);
}

/**
 * @brief set gpio as input
 */
void Ps_gpio::set_as_input()  {
	XGpioPs_SetDirectionPin(&this->ps_gpio, this->gpio_id, GPIO_AS_INPUT);
	XGpioPs_SetOutputEnablePin(&this->ps_gpio, this->gpio_id, GPIO_OUPUT_DISABLE);
}

/**
 * @brief perform a write (check if gpio is set as input ?)
 */
void Ps_gpio::write(uint32_t value) {
	this->value = value;
	XGpioPs_WritePin(&this->ps_gpio, this->gpio_id, this->value);
}

/**
 * @brief perform a read
 */
uint32_t Ps_gpio::read() {
	return XGpioPs_ReadPin(&this->ps_gpio, this->gpio_id);
}
