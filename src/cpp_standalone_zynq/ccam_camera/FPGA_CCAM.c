///*
// * FPGA.c
// *
// *  Created on: 30/10/2015
//  */
//
//#include "stdio.h"
//#include "cyu3system.h"
//#include "cyu3os.h"
//#include "cyu3dma.h"
//#include "cyu3error.h"
//#include "cyu3usb.h"
//#include "cyu3spi.h"
//#include "cyu3types.h"
//#include "cyu3utils.h"
//#include "cyu3gpio.h"
//
//// #include "FPGA.h"
//// #include "GPIOManager.h"
//
//
//
//
///* Set default SPI configuration to address FPGA device on SPI
// * Must be called before any data transfer
// * Mode 01
// * MSB first
// * 10Mhz Clock
// * GPIO CS
// */
//CyU3PReturnStatus_t _FPGA_SetSPIConfig(void)
//{
//	// Prepare config :
//    CyU3PSpiConfig_t spiConfig;
//    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
//
//    /* Start the SPI master block. Run the SPI clock at 20MHz
//     * and configure the word length to 8 bits. Also configure
//     * the slave select using FW. */
//    CyU3PMemSet ((uint8_t *)&spiConfig, 0, sizeof(spiConfig));
//    spiConfig.isLsbFirst = CyFalse;
//    spiConfig.cpol       = CyTrue;
//    spiConfig.ssnPol     = CyFalse;	/**< No used, GPIO instead */
//    spiConfig.cpha       = CyTrue;
//    spiConfig.leadTime   = CY_U3P_SPI_SSN_LAG_LEAD_HALF_CLK;
//    spiConfig.lagTime    = CY_U3P_SPI_SSN_LAG_LEAD_HALF_CLK;
//    spiConfig.ssnCtrl    = CY_U3P_SPI_SSN_CTRL_NONE;	/**< Use GPIO */
//    spiConfig.clock      = FPGA_SPI_CLOCK_MHZ * 1e6;
//    spiConfig.wordLen    = 8;
//    // Set it
//    status = CyU3PSpiSetConfig (&spiConfig, NULL);
//    return status;
//
//}
//
//
///**  Write Data to Register
// *
// * FPGA is 16bit register size.
// * pauCmd : FPGA register address
// * pauValues : register values buffer
// * paNbValues : register number to write
// * Address in this function is 1 for 16bit register
// */
//CyU3PReturnStatus_t FPGA_WriteReg(uint8_t pauCmd, uint8_t* pauValues, uint8_t paNbValues)
//{
//    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
//    uint8_t* pCmd = ((uint8_t*)&pauCmd) ;
//    uint8_t puVal[2];
//
//
//	// Set SPI config
//	_FPGA_SetSPIConfig();
//	// Low FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyFalse);
//
//	// Transmit Addr
//	status = CyU3PSpiTransmitWords(pCmd, 1);
//
//	// No error, Send buffer
//	if (status == CY_U3P_SUCCESS)
//	{
//		puVal[0]=pauValues[1];
//		puVal[1]=pauValues[0];
//		status = CyU3PSpiTransmitWords( (uint8_t*)puVal, paNbValues );
//	}
//	// High FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyTrue);
//
//	return status;
//}
//
//
///**  Read Data from Register
// *
// * FPGA is 8bit register size.
// * pauAddr : FPGA register address
// * pauValues : register values buffer
// * paNbValues : register number to read
// * Address in 8bit word (+1 means next 8bit register)
// */
//CyU3PReturnStatus_t FPGA_ReadReg(uint8_t pauCmd, uint16_t* pauValues, uint8_t paNbValues)
//{
//    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
//    uint8_t* vpStartAddr = ((uint8_t*)&pauCmd) ;
//    uint8_t puVal[2];
//
//
//
//
//	// Set SPI config
//	_FPGA_SetSPIConfig();
//	// Low FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyFalse);
//
//	status = CyU3PSpiTransmitWords(vpStartAddr, 1);
//
//
//	// No error, Read SPI word
//	if (status == CY_U3P_SUCCESS)
//	{
//
//		status = CyU3PSpiReceiveWords( puVal, 2 );
//		*pauValues = (puVal[0] << 8)|(puVal[1]);
//
//	}
//	// High FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyTrue);
//
//	//
//	return status;
//}
//
//
//CyU3PReturnStatus_t FPGA_WriteReg_32bits(uint8_t *pauCmd, uint8_t * pauValues)
//{
//    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
////    uint8_t vpStartAddr[4];
////    uint8_t puVal[4];
//
//    pauCmd[0] &= ~(1<<6);
//    pauCmd[0] &= ~(1<<7);
//
////    vpStartAddr[3] = pauCmd & 0xFF;
////    vpStartAddr[2] = (pauCmd>>8) & 0xFF;
////    vpStartAddr[1] = (pauCmd>>16) & 0xFF;
////    vpStartAddr[0] = (pauCmd>>24) & 0xFF;
//
////    puVal[3] = (pauValues) & 0xFF;
////    puVal[2] = (pauValues>>8) & 0xFF;
////    puVal[1] = (pauValues>>16) & 0xFF;
////    puVal[0] = (pauValues>>24) & 0xFF;
//
//    // Set SPI config
//	_FPGA_SetSPIConfig();
//	// Low FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyFalse);
//
//	// Transmit Addr
//	status = CyU3PSpiTransmitWords(pauCmd, 4);
//
//	// No error, Send buffer
//	if (status == CY_U3P_SUCCESS)
//	{
//		status = CyU3PSpiTransmitWords( pauValues, 4 );
//	}
//	// High FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyTrue);
//
//	return status;
//}
//
//
///**  Read Data from Register
// *
// * FPGA is 8bit register size.
// * pauAddr : FPGA register address
// * pauValues : register values buffer
// * paNbValues : register number to read
// * Address in 8bit word (+1 means next 8bit register)
// */
//CyU3PReturnStatus_t FPGA_ReadReg_32bits(uint8_t * pauCmd, uint8_t* pauValues)
//{
//    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
////    uint8_t vpStartAddr[4];
////    uint8_t puVal[4];
//
//    pauCmd[0] |= (1<< 6);
//    pauCmd[0] &= ~(1<<7);
//
////    vpStartAddr[3] = pauCmd & 0xFF;
////    vpStartAddr[2] = (pauCmd>>8) & 0xFF;
////    vpStartAddr[1] = (pauCmd>>16) & 0xFF;
////    vpStartAddr[0] = (pauCmd>>24) & 0xFF;
//
//	// Set SPI config
//	_FPGA_SetSPIConfig();
//	// Low FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyFalse);
//
//	status = CyU3PSpiTransmitWords(pauCmd, 4);
//
//
//	// No error, Read SPI word
//	if (status == CY_U3P_SUCCESS)
//	{
//
//		status = CyU3PSpiReceiveWords( pauValues, 4 );
////		*pauValue = (puVal[0] << 24) | (puVal[1] << 16) | (puVal[2] << 8) | puVal[3];
//	}
//	// High FPGA CS
//	CyU3PGpioSimpleSetValue(GPIOSPI_CS_0, CyTrue);
//
//	//
//	return status;
//}
//
//
//
