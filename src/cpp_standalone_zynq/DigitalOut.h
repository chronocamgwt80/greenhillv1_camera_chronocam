/*
 * \file DigitalOut.h
 *
 *  Created on: Sep 25, 2015
 *      Author: avialletelle
 */

#ifndef DIGITALOUT_H_
#define DIGITALOUT_H_

/*
 * Includes
 */
#include "Ps_gpio.h"

/*
 * @brief
 */
class DigitalOut : public Ps_gpio
{
public:
	DigitalOut(uint32_t gpio_id);
	virtual ~DigitalOut();


	// A shorthand for write()
	DigitalOut& operator= (int value)
	{
	    this->write(value);
	    return *this;
	}


    DigitalOut& operator= (DigitalOut& io_out) {
        write(io_out.read());
        return *this;
    }
	// A shorthand for read()
    operator int() {
        return read();
    }
};



#endif /* DIGITALOUT_H_ */
