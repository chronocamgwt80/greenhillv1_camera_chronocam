/*
 * command.cpp
 *
 *  Created on: Dec 29, 2015
 *      Author: avialletelle
 */

#include "command.h"
#include "frame.h"
#include "DigitalOut.h"
#include "rf/Rf_tx_interface.h"
#include "bram_controller/bram_controller.h"
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ps_usb/usb_ps.h"
#include "config.h"
#include "rf/gwt_modulator.h"

extern FRAME_STRUCTURE frame;
extern uint32_t hw_version;
extern uint32_t sw_version;
extern DigitalOut led;
extern Rf_tx_interface rf_tx_controller;
extern BramSystemConfiguration BramSystemConfig;
extern XUsbPs usb_com;
extern uint32_t bram_buffer[RF_RX_ACQ_SAMPLES_NB_MAX];
extern gwt_modulator modulator;

/*
 * @brief Get FPGA firmware version
 */
void get_hw_version()
{
	if (frame.rd_wrb)
	{
		// read command
		frame.nbbytes = 4;
		*((uint32_t*) &frame.data[0]) = (uint32_t) hw_version;

	} else
	{
		// write command
		// command is read only
	}
}

void drive_gw_power_user_leds()
{
	if (frame.rd_wrb)
	{
		// read command
		// command is write only


	} else
	{
		// write command
		AXI_REG_GW_POWER_USER_LEDS = ((frame.data[0]) & (0xf));
	}
}

/*
 * @brief Get ARM firmware version
 */
void get_sw_version()
{
	if (frame.rd_wrb)
	{
		// read command
		frame.nbbytes = 4;
		*((uint32_t*) &frame.data[0]) = (uint32_t) sw_version;

	} else
	{
		// write command
		// command is read only
	}
}

/*
 * @brief Get the flag indicating if previous modulation is over
 * 			and the data length of the previous output of the modulation
 */
void get_modulation_ready_flag()
{
	if (frame.rd_wrb)
	{
		// read command
		frame.nbbytes = 8;
		*((uint32_t*) &frame.data[0]) = (uint32_t) modulator.modulation_ready;
		*((uint32_t*) &frame.data[4]) = (uint32_t) modulator.previous_modulated_data_len;

	} else
	{
		// write command, reset the modulation ready flag
		modulator.modulation_ready = true;
	}
}

/*
 * @brief Get/Set the flag that indicate commands comes from python
 * 			C modulation take time and current USB management doesn't manage
 * 			properly a 2nd usb command while one is not over as such could be start_modulation().
 * 			So this flag allow a printf which indicate modulation over and the execution could
 * 			continued in python by pressing enter instead of having a sleep time in the python
 * 			command file.
 */
void get_set_cmd_from_python()
{
	if (frame.rd_wrb)
	{
		// read command
		frame.nbbytes = 1;
		//mode_python value should be 1 or 0
		frame.data[0] = (uint8_t) modulator.mode_python;

	} else
	{
		// write command, reset the modulation ready flag
		modulator.mode_python = (uint32_t) frame.data[0];
	}
}

/*
 * @brief Turn on/off user led
 */
void cmd_user_led()
{
	if (frame.rd_wrb)
	{
		// read command
		frame.nbbytes = 1;
		frame.data[0] = (uint8_t) led;

	} else
	{
		// write command
		led = (frame.data[0] & 0x1);
	}
}

/*
 * @brief launch a RF TX transmission with pattern.c IQ samples
 */
void rf_tx_start_self_test()
{
	// turn on user led to indicate that transmission is processing
	led = ON;
	rf_tx_controller.self_test();
}

/*
 * @brief start RF TX transmission with sample stored in RF TX Buffer
 */
void rf_tx_start_transmission()
{
	// turn off user led to indicate that transmission is ended
	led = ON;
	rf_tx_controller.start_transmit();
}

/*
 * @brief stop current RF TX transmission
 */
void rf_tx_stop_transmission()
{
	// turn off user led to indicate that transmission is ended
	led = OFF;
	rf_tx_controller.stop_transmit();
}

/*
 * @brief reset all RF TX components
 */
void rf_tx_reset()
{
	// turn off user led to indicate that transmission is ended
	led = OFF;
	rf_tx_controller.reset();
}

/*
 * @brief set RF TX mode as loop (each sample read from FIFO is rewritten into the FIFO) or burst mode (each sample read from FIFO is sent once only)
 * @param mode - '0' = burst, '1' = loop
 */
void rf_tx_loop_burst_mode(uint8_t mode)
{
	rf_tx_controller.loop_burst_mode(mode);
}

/*
 * @brief prepare data which will be sent to GWT modulator or send to remote API data which will be sent to GWT modulator
 */
void prepare_data_to_modulate(int8_t* data, uint32_t len)
{
	if (frame.rd_wrb)
	{
		// read command
		if (frame.nbbytes > RF_TX_DATA_FRAME_MAX_NBBYTES)
		{
			frame.nbbytes = RF_TX_DATA_FRAME_MAX_NBBYTES;
		}


		memcpy((uint8_t*) frame.data, (int8_t*) rf_tx_controller.frame_to_modulate, sizeof(uint8_t) * frame.nbbytes);

	} else
	{
		// write command
		memcpy((int8_t*) rf_tx_controller.frame_to_modulate, (int8_t*) data, sizeof(uint8_t) * len);
		rf_tx_controller.frame_to_modulate_len = len/4; // words number
	}
}

/*
 * @brief read or enable the transfer of modulation data in the DDR and print of values before load it to the RF
 */
void enable_modulation_check_data()
{
	if (frame.rd_wrb) //read command
	{
		frame.data[0] = (uint8_t) rf_tx_controller.get_check_data_send();
	}
	else //write command
	{
		rf_tx_controller.set_check_data_send((bool) 1);
	}
}

/*
 * @brief start modulation with data stored in rf_tx.dataframe and fill RF TX buffer
 */
void start_modulation()
{
	if (frame.rd_wrb)
	{
		// read command
		if (modulator.modulated_data_len > RF_TX_BUFFER_WR_MAX_DEPTH)
		{
			modulator.modulated_data_len = RF_TX_BUFFER_WR_MAX_DEPTH;
		}

		/* read done when no modulation implemented and that data_in = data_out
		for (uint32_t index = 0; index < frame.nbbytes; ++index)
		{
			frame.data[index] = *(((uint8_t*) &this->modulator.modulated_data[0]) + index) ;
		}
		*/
		//TODO PC need to know previous modulator.modulated_data_len, could be a variable or send back after modulation
		for (uint32_t index = 0; index < frame.nbbytes; ++index)
		{
			//frame.data[index] = *(((uint8_t*) &this->modulator.modulated_data[0]) + index) ;
			frame.data[index] = *((uint8_t*)(modulator.res_mod)+index);
		}
		//at first read the mod output without a specific copy in the DDR


	} else
	{
		// write command

		// start modulation and wait for modulation end
		modulator.modulation_ready = false;
		modulator.modulation(frame.data[0], rf_tx_controller.frame_to_modulate, rf_tx_controller.frame_to_modulate_len);
		//TODO analyze if possible conflict in case new modulation started before transfer to fifo done
		modulator.previous_modulated_data_len = modulator.modulated_data_len;
		//printf("modulator.previous_modulated_data_len=%d\n", (int) modulator.previous_modulated_data_len);

		// fill fifo
		//rf_tx_controller.fill_fifo(this->modulator.modulated_data[0], this->modulator.modulated_data_len, RF_TX_NO_ZERO_SAMPLES);

		if (0 != modulator.modulated_data_len)
		{
			//rf_tx_controller.set_check_data_send((bool) 1);
			//fill the buffer, put 1 as last parameter to have 1 32bits word à 0 to synchronize
			rf_tx_controller.fill_fifo_GWT(modulator.out_mod, modulator.modulated_data_len, 1);
		}
		//modulation_ready here once the buffer is filled in order to ensure loop mode will work
		modulator.modulation_ready = true;
		if (modulator.mode_python)
		{
			printf("MODULATION IS OVER\n");
		}

		//copy the mod output in the DDR if check mode requested
		if (rf_tx_controller.get_check_data_send())
		{
			for (uint32_t index = 0; index < modulator.modulated_data_len; index++)
			{
				modulator.res_mod[index] = ( ((modulator.out_mod + index)->r));
				modulator.res_mod[index + 1] = ( ((modulator.out_mod + index)->i));
				//printf("output_mod_float%d r=%f i=%f in_DDR_int r=%d i=%d\n", (unsigned int) index, (modulator.out_mod + index)->r, (modulator.out_mod + index)->i,
				//		(int) modulator.res_mod[index], (int) modulator.res_mod[index + 1]);
			}
		}
	}
}

/*
 * @brief configure before running it, the GWT algorithm
 */
void gwt_configure()
{
	modulator.configure((uint32_t *) &frame.data[0], frame.nbbytes);
}

/*
 * @brief Get the modulated data length to check GWT algorithm generated data number
 */
void get_modulated_data_length()
{
	if (frame.rd_wrb)
	{
		// read command
		rf_tx_controller.buffer_index = rf_tx_controller.get_data_count();
		for (uint32_t index = 0; index < sizeof(rf_tx_controller.buffer_index); ++index)
		{
			frame.data[index] = (modulator.modulated_data_len >> (index*8)) & 0xFF;
		}

	} else
	{
		// read only command
	}
}
/*
 * @brief prepare a command which will be return the RF TX buffer index
 */
void rf_tx_get_buffer_index()
{
	if (frame.rd_wrb)
	{
		// read command
		rf_tx_controller.buffer_index = rf_tx_controller.get_data_count();
		for (uint32_t index = 0; index < sizeof(rf_tx_controller.buffer_index); ++index)
		{
			frame.data[index] = (rf_tx_controller.buffer_index >> (index*8)) & 0xFF;
		}

	} else
	{
		// read only command
	}
}

/*
 * @brief
 */
void rf_rx_acq_config()
{
	if (frame.rd_wrb)
	{
		// read command
		uint32_t index = 0;
		// read the RF RX acquisition status
		bram_controller_read_config();

		memcpy(&frame.data[index], &BramSystemConfig.bram_acquisition_en, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_controller_irq_counter, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_fill_threshold, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_irq_clear, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_overflow_flag, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_overflow_flag_clear, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_packet_timestamp, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_polarity, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_resetb, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_src_address, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_timestamp, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.cdma_error, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.cdma_is_running, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.cdma_transfert_done, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.ddr_buf_start_address, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.dma_transfert_cnt, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.emulator_en, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.last_half_bram_samples_nb, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.nb_half_bram_per_acq, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.bram_error, 4);
		index +=4;
		memcpy(&frame.data[index], &BramSystemConfig.cdma_irq_counter, 4);
//		index +=4;

	} else
	{
		bram_controller_read_config();
		// write command
		uint32_t module_selected = frame.data[0]; // 1 for atmel_emulator, 0 for real atmel
		uint32_t samples_nb = (frame.data[1] | (frame.data[2] << 8) | (frame.data[3] << 16) | (frame.data[4] << 24));

		// update config
		BramSystemConfig.emulator_en = module_selected;
		BramSystemConfig.nb_half_bram_per_acq = (unsigned)(ceilf(((double)(samples_nb)/(double)HALF_BRAM_SIZE_IN_SAMPLES)));
		if(samples_nb == 0)
		{
			BramSystemConfig.last_half_bram_samples_nb = 0;
			BramSystemConfig.nb_half_bram_per_acq = 0;
		}
		else if ((samples_nb % HALF_BRAM_SIZE_IN_SAMPLES)  == 0)
		{
			BramSystemConfig.last_half_bram_samples_nb = HALF_BRAM_SIZE_IN_SAMPLES;
		} else
		{
			BramSystemConfig.last_half_bram_samples_nb = (samples_nb % HALF_BRAM_SIZE_IN_SAMPLES);
		}
		BramSystemConfig.bram_fill_threshold = samples_nb - 1;//(((BramSystemConfig.nb_half_bram_per_acq - 1) / 2) << 14) | (BramSystemConfig.last_half_bram_samples_nb - 1); // threshold = sample_nb+1

		BramSystemConfig.cdma_error = 0;
		BramSystemConfig.bram_error = 0;
		BramSystemConfig.bram_controller_irq_counter = 0;
		BramSystemConfig.cdma_irq_counter = 0;

		bram_controller_apply_config();
	}
}

/*
 * @brief
 */
void rf_rx_acq_start()
{
	if (frame.rd_wrb)
	{
		// read command
		// command is write only

	} else
	{
		// write command
		bram_controller_start();
	}
}

/*
 * @brief
 */
void rf_rx_acq_read_samples()
{
	if (frame.rd_wrb)
	{
		// read command
		// command is write only

	} else
	{
		// write command
		uint32_t nbbytes = frame.data[0] + (frame.data[1] << 8) + (frame.data[2] << 16) + (frame.data[3] << 24);
		uint32_t index = 0;


		while( (nbbytes - index) >= USB_PACKET_DIVIDER)
		{
			XUsbPs_EpBufferSend(&usb_com, ZYNQ_BULK_ENDPOINT, (((uint8_t*)bram_buffer) + index), USB_PACKET_DIVIDER);
			index += USB_PACKET_DIVIDER;
		}

		if((nbbytes - index) != 0)
		{
			XUsbPs_EpBufferSend(&usb_com, ZYNQ_BULK_ENDPOINT, (((uint8_t*)bram_buffer) + index), (nbbytes - index));
		}
	}
}

/*
 * @brief
 */
void rf_rx_sync_flag()
{
	if (frame.rd_wrb)
	{
		// read command: read the deserializer sync flag from RF RX controller register
		frame.data[0] = ((AXI_REG_RF_RX_CONTROLLER & AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_sync_flag_MASK) >> AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_sync_flag_OFFSET);
	} else
	{
		// read only
	}
}

/*
 * @brief
 */
void rf_rx_force_emulator(uint8_t value)
{
	if (frame.rd_wrb)
	{
		// read command: read the deserializer sync flag from RF RX controller register
		frame.data[0] = ((AXI_REG_RF_RX_CONTROLLER & AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_force_emulator_MASK) >> AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_force_emulator_OFFSET);
	} else
	{
		switch (value) {
			case SERDES_FORCE_EMULATOR_STATE:
				AXI_REG_RF_RX_CONTROLLER |= AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_force_emulator_MASK;
				break;
			case SERDES_RELEASE_EMULATOR_STATE:
				AXI_REG_RF_RX_CONTROLLER &= ~AXI_REG_RF_RX_CONTROLLER_rf_rx_serdes_force_emulator_MASK;
				break;
			default:
				// do nothing
				break;
		}
	}
}

/*
 * @brief
 */
void rf_rx_reset(uint8_t value)
{
	if (frame.rd_wrb)
	{
		// read command: read the deserializer sync flag from RF RX controller register
		frame.data[0] = ((AXI_REG_BRAM_CTRL_REG_0 & AXI_REG_BRAM_CTRL_REG_0_bram_resetb_MASK) >> AXI_REG_BRAM_CTRL_REG_0_bram_resetb_OFFSET);
	} else
	{
		switch (value) {
			case RF_RX_RELEASE_RESET:
				AXI_REG_BRAM_CTRL_REG_0 |= AXI_REG_BRAM_CTRL_REG_0_bram_resetb_MASK;
				break;
			case RF_RX_FORCE_RESET:
				AXI_REG_BRAM_CTRL_REG_0 &= ~AXI_REG_BRAM_CTRL_REG_0_bram_resetb_MASK;
				break;
			default:
				// do nothing
				break;
		}
	}
}

