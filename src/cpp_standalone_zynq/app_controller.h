/*
 * app_controller.h
 *
 *  Created on: Dec 29, 2015
 *      Author: avialletelle
 */

#ifndef APP_CONTROLLER_H_
#define APP_CONTROLLER_H_

class App_controller
{
public:
	App_controller();
	virtual ~App_controller();

	void init();
	void frame_init();
	void start();
	void decode(void);
	void send_rd_cmd_response();
};

#endif /* APP_CONTROLLER_H_ */
