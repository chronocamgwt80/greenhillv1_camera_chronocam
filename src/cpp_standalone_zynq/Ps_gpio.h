/*
 * \file Ps_gpio.h
 *
 *  Created on: Sep 25, 2015
 *      Author: avialletelle
 */

#ifndef PS_GPIO_H_
#define PS_GPIO_H_

/*
 * includes
 */
#include "xgpiops.h"

/*
 * defines
 */

// MIO
enum
{
	MIO_0,
	MIO_1,
	MIO_2,
	MIO_3,
	MIO_4,
	MIO_5,
	MIO_6,
	MIO_7,
	MIO_8,
	MIO_9,
	MIO_10,
	MIO_11,
	MIO_12,
	MIO_13,
	MIO_14,
	MIO_15,
	MIO_16,
	MIO_17,
	MIO_18,
	MIO_19
	// to be continued
};

#define GPIO_AS_INPUT			0x0
#define GPIO_AS_OUTPUT			0x1
#define GPIO_OUPUT_DISABLE		0x0
#define GPIO_OUPUT_ENABLE		0x1

/*
 * @brief
 */
class Ps_gpio
{
public:
	Ps_gpio(uint32_t gpio_id);
	virtual ~Ps_gpio();

	void init();
	void set_as_output();
	void set_as_input();
	void write(uint32_t value);
	uint32_t read();

protected:
	uint32_t gpio_id;
	uint32_t value;
	XGpioPs ps_gpio;
};

#endif /* PS_GPIO_H_ */
