--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief top RF RX block design, specific block for output (ODDR, OBUF etc.)
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
------------------------------------------------------------------------------
-- RF RX entity
------------------------------------------------------------------------------
entity rf_rx is
  port (
    rf_rx_clk   : out std_logic;  -- RF RXCLK to prepare for LVDS
    rf_rx_clk_p : in  std_logic;  -- positive LVDS RXCLK
    rf_rx_clk_n : in  std_logic;  -- negative LVDS RXCLK
    rf_tx_arst  : in  std_logic   -- asynchronous reset active high
    );               
end entity rf_rx;

------------------------------------------------------------------------------
-- RF RX behavior
------------------------------------------------------------------------------
architecture behavior of rf_rx is
  ------------------------------------------------------------------------------
  -- RF RX in signals
  ------------------------------------------------------------------------------
  signal rf_rx_in_clk   : std_logic;  -- RF RXCLK to prepare for LVDS
  signal rf_rx_in_clk_p : std_logic;  -- positive LVDS RXCLK
  signal rf_rx_in_clk_n : std_logic;  -- negative LVDS RXCLK
  
begin  -- architecture behavior
  ------------------------------------------------------------------------------
  -- Component outputs updating
  ------------------------------------------------------------------------------
  rf_rx_clk      <= rf_rx_in_clk;
  ------------------------------------------------------------------------------
  -- RF RX in binding
  ------------------------------------------------------------------------------
  rf_rx_in_clk_p <= rf_rx_clk_p;
  rf_rx_in_clk_n <= rf_rx_clk_n;
  ------------------------------------------------------------------------------
  -- LVDS Intput packaging: prepare input signal received from RF chip through lvds with
  -- double data rate
  ------------------------------------------------------------------------------
  rf_rx_in_1 : entity work.rf_rx_in
    port map (
      rf_rx_in_clk   => rf_rx_in_clk,     -- RF RXCLK to prepare for LVDS
      rf_rx_in_clk_p => rf_rx_in_clk_p,   -- positive LVDS RXCLK
      rf_rx_in_clk_n => rf_rx_in_clk_n);  -- negative LVDS RXCLK

end architecture behavior;
