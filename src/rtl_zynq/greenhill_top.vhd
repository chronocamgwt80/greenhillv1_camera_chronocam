--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief GREENHILL top module
--! @details
--! This module instanciates PS/ARM project, GWT block design
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;

------------------------------------------------------------------------------
--! Entity
------------------------------------------------------------------------------
entity greenhill_top is
  generic (
    --! Width of S_AXI data bus
    C_S_AXI_DATA_WIDTH : integer := 32;
    --! Width of S_AXI address bus
    C_S_AXI_ADDR_WIDTH : integer := 32
    );
  port (
    DDR_addr          : inout std_logic_vector (14 downto 0);
    DDR_ba            : inout std_logic_vector (2 downto 0);
    DDR_cas_n         : inout std_logic;
    DDR_ck_n          : inout std_logic;
    DDR_ck_p          : inout std_logic;
    DDR_cke           : inout std_logic;
    DDR_cs_n          : inout std_logic;
    DDR_dm            : inout std_logic_vector (3 downto 0);
    DDR_dq            : inout std_logic_vector (31 downto 0);
    DDR_dqs_n         : inout std_logic_vector (3 downto 0);
    DDR_dqs_p         : inout std_logic_vector (3 downto 0);
    DDR_odt           : inout std_logic;
    DDR_ras_n         : inout std_logic;
    DDR_reset_n       : inout std_logic;
    DDR_we_n          : inout std_logic;
    FIXED_IO_ddr_vrn  : inout std_logic;
    FIXED_IO_ddr_vrp  : inout std_logic;
    FIXED_IO_mio      : inout std_logic_vector (53 downto 0);
    FIXED_IO_ps_clk   : inout std_logic;
    FIXED_IO_ps_porb  : inout std_logic;
    FIXED_IO_ps_srstb : inout std_logic;
    --config_spi_ss_io  : inout std_logic_vector (0 to 0);
    --! data spi interface
    --data_spi_io0_io   : inout std_logic;
    --data_spi_io1_io   : inout std_logic;
    --data_spi_sck_io   : inout std_logic;
    --data_spi_spisel   : in    std_logic;
    --data_spi_ss_io    : inout std_logic_vector (0 to 0);
    --! RF interface
    rf_rx_clk_p       : in    std_logic;
    rf_rx_clk_n       : in    std_logic;
    rf_tx_clk_p       : out   std_logic;
    rf_tx_clk_n       : out   std_logic;
    --rf_rx24_data_p      : in   std_logic;
    --rf_rx24_data_n      : in   std_logic;
    rf_rx09_data_p    : in    std_logic;
    rf_rx09_data_n    : in    std_logic;
    rf_tx_data_p      : out   std_logic;
    rf_tx_data_n      : out   std_logic;
--    rf_mon_clk        : out   std_logic;
--    rf_mon_txd        : out   std_logic_vector(1 downto 0);
    --! CONFIG SPI
    config_spi_clk    : inout std_logic;
    config_spi_mosi   : inout std_logic;
    config_spi_miso   : inout std_logic;
    config_spi_ss     : inout std_logic_vector(0 downto 0);
    external_spi_clk  : inout std_logic;
    external_spi_mosi : inout std_logic;
    external_spi_miso : inout std_logic;
    external_spi_ss   : inout std_logic_vector(0 downto 0);

    gw_power_user_leds : out std_logic_vector(3 downto 0)
    );
end entity greenhill_top;

------------------------------------------------------------------------------
--! Arch
------------------------------------------------------------------------------
architecture Behavioral of greenhill_top is
  ------------------------------------------------------------------------------
  --! Component signals
  ------------------------------------------------------------------------------
  --! firmware version
  constant FIRMWARE_VERSION  : std_logic_vector(31 downto 0) := x"00000004";
  --! clock & reset
  signal FPGA_CLK            : std_logic;
  signal FPGA_ARST           : std_logic;
  signal FPGA_ARSTN          : std_logic;
  ------------------------------------------------------------------------------
  --! Vivado block design interface
  ------------------------------------------------------------------------------
  signal AXI_EXT_SPI_araddr  : std_logic_vector (31 downto 0);
  signal AXI_EXT_SPI_aresetn : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_arprot  : std_logic_vector (2 downto 0);
  signal AXI_EXT_SPI_arready : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_arvalid : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_awaddr  : std_logic_vector (31 downto 0);
  signal AXI_EXT_SPI_awprot  : std_logic_vector (2 downto 0);
  signal AXI_EXT_SPI_awready : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_awvalid : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_bready  : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_bresp   : std_logic_vector (1 downto 0);
  signal AXI_EXT_SPI_bvalid  : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_rdata   : std_logic_vector (31 downto 0);
  signal AXI_EXT_SPI_rready  : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_rresp   : std_logic_vector (1 downto 0);
  signal AXI_EXT_SPI_rvalid  : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_wdata   : std_logic_vector (31 downto 0);
  signal AXI_EXT_SPI_wready  : std_logic_vector (0 downto 0);
  signal AXI_EXT_SPI_wstrb   : std_logic_vector (3 downto 0);
  signal AXI_EXT_SPI_wvalid  : std_logic_vector (0 downto 0);
  signal BRAM_PORTB_addr     : std_logic_vector (31 downto 0);
  signal BRAM_PORTB_clk      : std_logic;
  signal BRAM_PORTB_din      : std_logic_vector (31 downto 0);
  signal BRAM_PORTB_dout     : std_logic_vector (31 downto 0);
  signal BRAM_PORTB_en       : std_logic;
  signal BRAM_PORTB_rst      : std_logic;
  signal BRAM_PORTB_we       : std_logic_vector (3 downto 0);
  signal IRQ_PL              : std_logic_vector (0 to 0);
  signal PL_CLK              : std_logic;
  signal PL_RSTN             : std_logic;
  signal S_AXI_araddr        : std_logic_vector (31 downto 0);
  signal S_AXI_arprot        : std_logic_vector (2 downto 0);
  signal S_AXI_arready       : std_logic_vector (0 to 0);
  signal S_AXI_arvalid       : std_logic_vector (0 to 0);
  signal S_AXI_awaddr        : std_logic_vector (31 downto 0);
  signal S_AXI_awprot        : std_logic_vector (2 downto 0);
  signal S_AXI_awready       : std_logic_vector (0 to 0);
  signal S_AXI_awvalid       : std_logic_vector (0 to 0);
  signal S_AXI_bready        : std_logic_vector (0 to 0);
  signal S_AXI_bresp         : std_logic_vector (1 downto 0);
  signal S_AXI_bvalid        : std_logic_vector (0 to 0);
  signal S_AXI_rdata         : std_logic_vector (31 downto 0);
  signal S_AXI_rready        : std_logic_vector (0 to 0);
  signal S_AXI_rresp         : std_logic_vector (1 downto 0);
  signal S_AXI_rvalid        : std_logic_vector (0 to 0);
  signal S_AXI_wdata         : std_logic_vector (31 downto 0);
  signal S_AXI_wready        : std_logic_vector (0 to 0);
  signal S_AXI_wstrb         : std_logic_vector (3 downto 0);
  signal S_AXI_wvalid        : std_logic_vector (0 to 0);
  signal config_spi_io0_io   : std_logic;
  signal config_spi_io1_io   : std_logic;
  signal config_spi_sck_io   : std_logic;
  signal config_spi_ss_io    : std_logic_vector (0 to 0);
  signal rf_tx_fifo_clk_read : std_logic;
  signal rf_tx_fifo_din      : std_logic_vector (27 downto 0);
  signal rf_tx_fifo_dout     : std_logic_vector (27 downto 0);
  signal rf_tx_fifo_empty    : std_logic;
  signal rf_tx_fifo_full     : std_logic;
  signal rf_tx_fifo_rd_en    : std_logic;
  signal rf_tx_fifo_rst      : std_logic;
  signal rf_tx_fifo_valid    : std_logic;
  signal rf_tx_fifo_wr_en    : std_logic;

  ------------------------------------------------------------------------------
  --! Register map interface signals
  ------------------------------------------------------------------------------
  signal S_AXI_ACLK           : std_logic;  --! Global Clock Signal
  signal S_AXI_ARESETN        : std_logic;  --! Global Reset Signal.
  signal axi_reg_out          : register_bus_out;
  signal axi_reg_in           : register_bus_in;
  ------------------------------------------------------------------------------
  --! RF signals
  ------------------------------------------------------------------------------
  signal rf_clk               : std_logic;  -- ! RF CLK
  signal rf_fpga_arst         : std_logic;  -- ! asynchronous reset active high, from FPGA
  signal rf_txrst             : std_logic;  -- ! reset active high, from register
  signal rf_rxrst             : std_logic;  -- ! reset active high, from register
  signal rf_txen              : std_logic;  -- ! enable RF TX
  signal rf_txrd_din_en       : std_logic;  -- ! flag to generate a writing request
  signal rf_txdin_valid       : std_logic;  -- ! flag indicating a new sample IQ is ready
  signal rf_txdin             : std_logic_vector(27 downto 0);  -- ! sample IQ to write
  signal rf_txfull            : std_logic;  -- ! flag indicating buffer is full
  signal rf_txempty           : std_logic;  -- ! flag indicating buffer is empty
  signal rf_txout_data_vector : std_logic_vector(1 downto 0);
  signal rf_txloopburst_mode  : std_logic;  -- ! to set in loop or burst mode
  signal rf_txdata_count      : std_logic_vector(17 downto 0);  -- ! provide the
  signal atmel_serial_data_0  : std_logic;
  signal atmel_serial_data_1  : std_logic;
  signal serdes_fsm_state     : std_logic_vector(1 downto 0);


  ------------------------------------------------------------------------------
  --! LVDS signals
  ------------------------------------------------------------------------------
  signal rf_tx_out_data : std_logic;
  signal rf_rx_clk      : std_logic;
  signal rf_rx09_data   : std_logic;

  constant C_SLAVES_NB_WIDTH : integer := 8;
  constant C_DATA_WIDTH      : integer := 24;
  constant C_ADDR_WIDTH      : integer := 8;
  constant C_DIV_WIDTH       : integer := 8;

  signal spi_sck, spi_mosi, spi_miso : std_logic                                      := '0';
  signal spi_ss                      : std_logic_vector(C_SLAVES_NB_WIDTH-1 downto 0) := (others => '0');

  
begin
  ------------------------------------------------------------------------------
  --! Component outputs updating
  ------------------------------------------------------------------------------
  --! RF interface
  --  rf_mon_clk <= rf_rx_clk;
  --  rf_mon_txd <= rf_txout_data_vector;

  ------------------------------------------------------------------------------
  --! Internal signals binding
  ------------------------------------------------------------------------------
  FPGA_CLK   <= PL_CLK;
  FPGA_ARST  <= not(PL_RSTN);
  FPGA_ARSTN <= PL_RSTN;
  ------------------------------------------------------------------------------
  --! System wrapper signals binding
  ------------------------------------------------------------------------------

  --! RF TX FIFO
  rf_tx_fifo_clk_read                <= rf_rx_clk;
  rf_tx_fifo_din                     <= axi_reg_out.rf_tx_fifo_din_i & axi_reg_out.rf_tx_fifo_din_q;
  rf_tx_fifo_wr_en                   <= axi_reg_out.rf_tx_fifo_write_en;
  rf_tx_fifo_rd_en                   <= rf_txrd_din_en;
  rf_tx_fifo_rst                     <= axi_reg_out.rf_tx_fifo_rst;
  --! CONFIG SPI
  config_spi_mosi                    <= config_spi_io0_io;
  config_spi_io1_io                  <= config_spi_miso;
  config_spi_clk                     <= config_spi_sck_io;
  config_spi_ss                      <= config_spi_ss_io;
  ------------------------------------------------------------------------------
  --! Register Map interface binding
  ------------------------------------------------------------------------------
  --! AXI signals binding
  S_AXI_ACLK                         <= FPGA_CLK;
  S_AXI_ARESETN                      <= FPGA_ARSTN;
  axi_reg_in.rf_tx_fifo_empty_flag   <= rf_tx_fifo_empty;
  axi_reg_in.rf_tx_fifo_full_flag    <= rf_tx_fifo_full;
  axi_reg_in.rf_tx_buffer_empty_flag <= rf_txfull;
  axi_reg_in.rf_tx_buffer_full_flag  <= rf_txempty;
  axi_reg_in.rf_tx_buffer_data_count <= rf_txdata_count;
  --gw_power_user_leds                 <= axi_reg_out.gw_power_user_leds;
  gw_power_user_leds                 <= '1' & not(atmel_serial_data_0) & serdes_fsm_state;
  --! Hardware version
  axi_reg_in.FIRMWARE_VERSION        <= FIRMWARE_VERSION;
  ------------------------------------------------------------------------------
  --! RF signals binding
  ------------------------------------------------------------------------------
  rf_clk                             <= rf_rx_clk;
  rf_fpga_arst                       <= FPGA_ARST;
  rf_txrst                           <= axi_reg_out.rf_tx_controller_rst;
  rf_txen                            <= axi_reg_out.rf_tx_controller_enable;
  rf_txdin_valid                     <= rf_tx_fifo_valid;
  rf_txdin                           <= rf_tx_fifo_dout;
  rf_txloopburst_mode                <= axi_reg_out.rf_tx_loop_burst_mode;
  ------------------------------------------------------------------------------
  --! Vivado block design interface
  ------------------------------------------------------------------------------
  system_wrapper_2 : entity work.system_wrapper
    port map (
      AXI_EXT_SPI_araddr  => AXI_EXT_SPI_araddr,
      AXI_EXT_SPI_aresetn => AXI_EXT_SPI_aresetn,
      AXI_EXT_SPI_arprot  => AXI_EXT_SPI_arprot,
      AXI_EXT_SPI_arready => AXI_EXT_SPI_arready,
      AXI_EXT_SPI_arvalid => AXI_EXT_SPI_arvalid,
      AXI_EXT_SPI_awaddr  => AXI_EXT_SPI_awaddr,
      AXI_EXT_SPI_awprot  => AXI_EXT_SPI_awprot,
      AXI_EXT_SPI_awready => AXI_EXT_SPI_awready,
      AXI_EXT_SPI_awvalid => AXI_EXT_SPI_awvalid,
      AXI_EXT_SPI_bready  => AXI_EXT_SPI_bready,
      AXI_EXT_SPI_bresp   => AXI_EXT_SPI_bresp,
      AXI_EXT_SPI_bvalid  => AXI_EXT_SPI_bvalid,
      AXI_EXT_SPI_rdata   => AXI_EXT_SPI_rdata,
      AXI_EXT_SPI_rready  => AXI_EXT_SPI_rready,
      AXI_EXT_SPI_rresp   => AXI_EXT_SPI_rresp,
      AXI_EXT_SPI_rvalid  => AXI_EXT_SPI_rvalid,
      AXI_EXT_SPI_wdata   => AXI_EXT_SPI_wdata,
      AXI_EXT_SPI_wready  => AXI_EXT_SPI_wready,
      AXI_EXT_SPI_wstrb   => AXI_EXT_SPI_wstrb,
      AXI_EXT_SPI_wvalid  => AXI_EXT_SPI_wvalid,
      BRAM_PORTB_addr     => BRAM_PORTB_addr,
      BRAM_PORTB_clk      => BRAM_PORTB_clk,
      BRAM_PORTB_din      => BRAM_PORTB_din,
      BRAM_PORTB_dout     => BRAM_PORTB_dout,
      BRAM_PORTB_en       => BRAM_PORTB_en,
      BRAM_PORTB_rst      => BRAM_PORTB_rst,
      BRAM_PORTB_we       => BRAM_PORTB_we,
      DDR_addr            => DDR_addr,
      DDR_ba              => DDR_ba,
      DDR_cas_n           => DDR_cas_n,
      DDR_ck_n            => DDR_ck_n,
      DDR_ck_p            => DDR_ck_p,
      DDR_cke             => DDR_cke,
      DDR_cs_n            => DDR_cs_n,
      DDR_dm              => DDR_dm,
      DDR_dq              => DDR_dq,
      DDR_dqs_n           => DDR_dqs_n,
      DDR_dqs_p           => DDR_dqs_p,
      DDR_odt             => DDR_odt,
      DDR_ras_n           => DDR_ras_n,
      DDR_reset_n         => DDR_reset_n,
      DDR_we_n            => DDR_we_n,
      FIXED_IO_ddr_vrn    => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp    => FIXED_IO_ddr_vrp,
      FIXED_IO_mio        => FIXED_IO_mio,
      FIXED_IO_ps_clk     => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb    => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb   => FIXED_IO_ps_srstb,
      IRQ_PL              => IRQ_PL,
      PL_CLK              => PL_CLK,
      PL_RSTN             => PL_RSTN,
      S_AXI_araddr        => S_AXI_araddr,
      S_AXI_arprot        => S_AXI_arprot,
      S_AXI_arready       => S_AXI_arready,
      S_AXI_arvalid       => S_AXI_arvalid,
      S_AXI_awaddr        => S_AXI_awaddr,
      S_AXI_awprot        => S_AXI_awprot,
      S_AXI_awready       => S_AXI_awready,
      S_AXI_awvalid       => S_AXI_awvalid,
      S_AXI_bready        => S_AXI_bready,
      S_AXI_bresp         => S_AXI_bresp,
      S_AXI_bvalid        => S_AXI_bvalid,
      S_AXI_rdata         => S_AXI_rdata,
      S_AXI_rready        => S_AXI_rready,
      S_AXI_rresp         => S_AXI_rresp,
      S_AXI_rvalid        => S_AXI_rvalid,
      S_AXI_wdata         => S_AXI_wdata,
      S_AXI_wready        => S_AXI_wready,
      S_AXI_wstrb         => S_AXI_wstrb,
      S_AXI_wvalid        => S_AXI_wvalid,
      config_spi_io0_io   => config_spi_io0_io,
      config_spi_io1_io   => config_spi_io1_io,
      config_spi_sck_io   => config_spi_sck_io,
      config_spi_ss_io    => config_spi_ss_io,
      rf_tx_fifo_clk_read => rf_tx_fifo_clk_read,
      rf_tx_fifo_din      => rf_tx_fifo_din,
      rf_tx_fifo_dout     => rf_tx_fifo_dout,
      rf_tx_fifo_empty    => rf_tx_fifo_empty,
      rf_tx_fifo_full     => rf_tx_fifo_full,
      rf_tx_fifo_rd_en    => rf_tx_fifo_rd_en,
      rf_tx_fifo_rst      => rf_tx_fifo_rst,
      rf_tx_fifo_valid    => rf_tx_fifo_valid,
      rf_tx_fifo_wr_en    => rf_tx_fifo_wr_en);

  ------------------------------------------------------------------------------
  --! Register map interface
  ------------------------------------------------------------------------------
  axi_register_interface_2 : entity work.axi_register_interface
    port map (
      S_AXI_ACLK    => S_AXI_ACLK,      --! Global Clock Signal
      S_AXI_ARESETN => S_AXI_ARESETN,  --! Global Reset Signal. This Signal is Active LOW
      S_AXI_AWADDR  => S_AXI_AWADDR,  --! Write address (issued by master, acceped by Slave)
      S_AXI_AWPROT  => S_AXI_AWPROT,    --! Write channel Protection type.
      S_AXI_AWVALID => S_AXI_AWVALID(0),  --! Write address valid.
      S_AXI_AWREADY => S_AXI_AWREADY(0),  --! Write address ready.
      S_AXI_WDATA   => S_AXI_WDATA,  --! Write data (issued by master, acceped by Slave)
      S_AXI_WSTRB   => S_AXI_WSTRB,     --! Write strobes.
      S_AXI_WVALID  => S_AXI_WVALID(0),   --! Write valid
      S_AXI_WREADY  => S_AXI_WREADY(0),   --! Write ready.
      S_AXI_BRESP   => S_AXI_BRESP,     --! Write response.
      S_AXI_BVALID  => S_AXI_BVALID(0),   --! Write response valid.
      S_AXI_BREADY  => S_AXI_BREADY(0),   --! Response ready.
      S_AXI_ARADDR  => S_AXI_ARADDR,  --! Read address (issued by master, acceped by Slave)
      S_AXI_ARPROT  => S_AXI_ARPROT,    --! Protection type.
      S_AXI_ARVALID => S_AXI_ARVALID(0),  --! Read address valid.
      S_AXI_ARREADY => S_AXI_ARREADY(0),  --! Read address ready.
      S_AXI_RDATA   => S_AXI_RDATA,     --! Read data (issued by slave)
      S_AXI_RRESP   => S_AXI_RRESP,     --! Read response.
      S_AXI_RVALID  => S_AXI_RVALID(0),   --! Read valid.
      S_AXI_RREADY  => S_AXI_RREADY(0),   --! Read ready.
      axi_reg_out   => axi_reg_out,
      axi_reg_in    => axi_reg_in);

  bram_inst : entity work.bram_custom_ctrl(rtl)
    port map(
      clk_200                  => PL_CLK,
      clk_64                   => rf_clk,
      resetb                   => axi_reg_out.bram_resetb,
      ctrl_acquisition_en      => axi_reg_out.bram_acquisition_en,
      ctrl_overflow_flag_clear => axi_reg_out.bram_overflow_flag_clear,
      ctrl_overflow_flag       => axi_reg_in.bram_overflow_flag,
      ctrl_irq_clear           => axi_reg_out.bram_irq_clear,
      ctrl_irq                 => IRQ_PL(0),
      ctrl_polarity            => axi_reg_in.bram_polarity,
      ctrl_packet_timestamp    => axi_reg_in.bram_packet_timestamp,
      ctrl_cdma_done           => axi_reg_out.cdma_transfert_done,
      ctrl_bram_fill_threshold => axi_reg_out.bram_fill_threshold,
      atmel_serial_data_0      => atmel_serial_data_0,
      atmel_serial_data_1      => atmel_serial_data_1,
      emulator_en              => axi_reg_out.emulator_en,
      serdes_force_emulator    => axi_reg_out.rf_rx_serdes_force_emulator,
      serdes_sync_flag         => axi_reg_in.rf_rx_serdes_sync_flag,
      BRAM_CLK                 => BRAM_PORTB_clk,
      BRAM_Addr                => BRAM_PORTB_addr,
      BRAM_WrData              => BRAM_PORTB_din,
      BRAM_WE                  => BRAM_PORTB_we,
      BRAM_rst                 => BRAM_PORTB_rst,
      BRAM_En                  => BRAM_PORTB_en,
      serdes_fsm_state         => serdes_fsm_state);


  ------------------------------------------------------------------------------
  --! GWT block design
  ------------------------------------------------------------------------------

  ------------------------------------------------------------------------------
  --! RF block design
  ------------------------------------------------------------------------------
  rf_1 : entity work.rf
    port map (
      rf_clk               => rf_clk,   -- ! RF CLK
      rf_fpga_arst         => rf_fpga_arst,  -- ! asynchronous reset active high, from FPGA
      rf_txrst             => rf_txrst,  -- ! reset active high, from register
      rf_txen              => rf_txen,  -- ! enable RF TX
      rf_txrd_din_en       => rf_txrd_din_en,  -- ! flag to generate a writing request
      rf_txdin_valid       => rf_txdin_valid,  -- ! flag indicating a new sample IQ is ready
      rf_txdin             => rf_txdin,  -- ! sample IQ to write
      rf_txfull            => rf_txfull,   -- ! flag indicating buffer is full
      rf_txempty           => rf_txempty,  -- ! flag indicating buffer is empty
      rf_txout_data_vector => rf_txout_data_vector,
      rf_txloopburst_mode  => rf_txloopburst_mode,  -- ! to set in loop or burst mode
      rf_txdata_count      => rf_txdata_count);     -- ! provide the

  ------------------------------------------------------------------------------
  --! prepare RF CLK received through LVDS
  ------------------------------------------------------------------------------
  LVDS_TO_RF_RX_CLK_IBUFDS_inst : IBUFDS
    generic map (
      DIFF_TERM    => true,             --! Differential Termination 
      IBUF_LOW_PWR => false,  --! Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS_25")
    port map (
      O  => rf_rx_clk,                  --! Buffer input 
      I  => rf_rx_clk_p,  --! Diff_p output (connect directly to top-level port)
      IB => rf_rx_clk_n  --! Diff_n output (connect directly to top-level port)
      );

  ------------------------------------------------------------------------------
  --! prepare RF CLK received through LVDS
  ------------------------------------------------------------------------------
  LVDS_TO_RF_RX_DATA_IBUFDS_inst : IBUFDS
    generic map (
      DIFF_TERM    => true,             --! Differential Termination 
      IBUF_LOW_PWR => false,  --! Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS_25")
    port map (
      O  => rf_rx09_data,               --! Buffer input 
      I  => rf_rx09_data_p,  --! Diff_p output (connect directly to top-level port)
      IB => rf_rx09_data_n  --! Diff_n output (connect directly to top-level port)
      );

  ------------------------------------------------------------------------------
  --! prepare RF CLK for LVDS
  ------------------------------------------------------------------------------
  RF_TX_CLK_TO_LVDS_OBUFDS_inst : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25")
    port map (
      OB => rf_tx_clk_n,  --! Diff_n output (connect directly to top-level port)
      O  => rf_tx_clk_p,  --! Diff_p output (connect directly to top-level port)
      I  => rf_rx_clk                   --! Buffer output
      );

  ------------------------------------------------------------------------------
  --! sequence data for Double Data Rate
  ------------------------------------------------------------------------------
  RF_TX_DATA_VECTOR_TO_DOUBLE_DATA_RATE_inst_d : ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",      --! "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT         => '0',            --! Initial value for Q port ('1' or '0')
      SRTYPE       => "SYNC")           --! Reset Type ("ASYNC" or "SYNC")
    port map (
      Q  => rf_tx_out_data,             --! 1-bit DDR output
      C  => rf_rx_clk,                  --! 1-bit clock input
      CE => '1',                        --! 1-bit clock enable input
      D1 => rf_txout_data_vector(1),    --! 1-bit data input (positive edge)
      D2 => rf_txout_data_vector(0),    --! 1-bit data input (negative edge)
      R  => '0',                        --! 1-bit reset input
      S  => '0'                         --! 1-bit set input
      );

  DOUBLE_DATA_RATE_TO_RF_RX09_DATA_VECTOR_inst_d : IDDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",  -- "OPPOSITE_EDGE", "SAME_EDGE" 
                                              -- or "SAME_EDGE_PIPELINED" 
      INIT_Q1      => '0',              -- Initial value of Q1: '0' or '1'
      INIT_Q2      => '0',              -- Initial value of Q2: '0' or '1'
      SRTYPE       => "SYNC")           -- Set/Reset type: "SYNC" or "ASYNC" 
    port map (
      Q1 => atmel_serial_data_0,  -- 1-bit output for positive edge of clock 
      Q2 => atmel_serial_data_1,  -- 1-bit output for negative edge of clock
      C  => rf_rx_clk,                  -- 1-bit primary clock input
      CE => '1',                        -- 1-bit clock enable input
      D  => rf_rx09_data,               -- 1-bit DDR data input
      R  => '0',                        -- 1-bit reset
      S  => '0'                         -- 1-bit set
      );
  ------------------------------------------------------------------------------
  --! prepare data for LVDS
  ------------------------------------------------------------------------------
  RF_TX_DATA_TO_LVDS_OBUFDS_inst : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25")
    port map (
      O  => rf_tx_data_p,  --! Diff_p output (connect directly to top-level port)
      OB => rf_tx_data_n,  --! Diff_n output (connect directly to top-level port)
      I  => rf_tx_out_data              --! Buffer output
      );
  ------------------------------------------------------------------------------ 
  --! External SPI Master 
  ------------------------------------------------------------------------------ 
  --! Connected to both J23 and J24 SATA connectors.
  --! Chonocam camera application.
  spi_master_ip_1 : entity work.spi_master_ip
    generic map (
      C_SLAVES_NB_WIDTH => C_SLAVES_NB_WIDTH,
      C_DATA_WIDTH      => C_DATA_WIDTH,
      C_ADDR_WIDTH      => C_ADDR_WIDTH,
      C_DIV_WIDTH       => C_DIV_WIDTH)
    port map (
      clk            => PL_CLK,
      rst            => axi_reg_out.ext_spi_cr_rst,
      clk_divider    => axi_reg_out.ext_spi_cr_clk_divider,
      spi_cpol       => axi_reg_out.ext_spi_cr_cpol,
      spi_cpha       => axi_reg_out.ext_spi_cr_cpha,
      spi_endian     => axi_reg_out.ext_spi_cr_endian,
      spi_ss_sel     => axi_reg_out.ext_spi_cr_ss_sel,
      spi_ss         => spi_ss,
      spi_sck        => spi_sck,
      spi_mosi       => spi_mosi,
      spi_miso       => spi_miso,
      send_frame     => axi_reg_out.ext_spi_cr_send_frame,
      fifo_in_wr     => axi_reg_out.ext_spi_fifo_in_wr_en,
      fifo_in_din    => axi_reg_out.ext_spi_fifo_in_data,
      fifo_in_empty  => axi_reg_in.ext_spi_cr_fifo_in_empty,
      fifo_in_full   => axi_reg_in.ext_spi_cr_fifo_in_full,
      fifo_out_rd    => axi_reg_out.ext_spi_fifo_out_fifo_en,
      fifo_out_dout  => axi_reg_in.ext_spi_fifo_out_data,
      fifo_out_empty => axi_reg_in.ext_spi_cr_fifo_out_empty);
  
  --! External SPI slave select busy, active low:
  axi_reg_in.ext_spi_cr_ss_busy <= spi_ss;
  
  --! External SPI interface
  external_spi_clk   <= spi_sck;
  external_spi_mosi  <= spi_mosi;
  spi_miso           <= external_spi_miso when axi_reg_out.ext_spi_fifo_loopback = '0' else spi_mosi;
  external_spi_ss(0) <= spi_ss(0);

end Behavioral;
