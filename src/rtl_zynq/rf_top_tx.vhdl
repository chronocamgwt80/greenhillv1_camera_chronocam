--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief RF top module(TX)
--! @details
--! This module consists of a RF interface TX and a RF controller TX
--------------------------------------------------------------------------------
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------
-- RF module TX entity
------------------------------------------------------------------------------
entity rf_top_tx is
  port (
    clk       : in  std_logic;  -- RF module supplies it
    rstb      : in  std_logic;  -- FPGA reset
    -- control
    start     : in  std_logic;  -- sw emulated oneshot (if stop signal is no sent then
                                -- module will stop at the end)
    stop      : in  std_logic;  -- sw emulated oneshot
    sample_nb : in  std_logic_vector(31 downto 0);  -- number of sample (I,Q) to
                                                    -- read in FIFO
    -- fifo interface
    dout      : in  std_logic_vector(27 downto 0);  -- fifo output to read the
                                                    -- sample
    rd_en     : out std_logic;  -- reading request
    -- rf interface
    data_tx   : out std_logic_vector(1 downto 0);  -- serialized bit of the sample
    -- monitoring
    fsm_state : out std_logic_vector(3 downto 0);  -- to read the current state
    sel_axis  : out std_logic;  -- select between I or Q to serialize
    counter   : out std_logic_vector(3 downto 0)  -- 16/2 cycles to count to serialize
                              -- all I or Q (double data rate)
    );
end entity rf_top_tx;
------------------------------------------------------------------------------
-- RF module TX behavior
------------------------------------------------------------------------------
architecture behavioral of rf_top_tx is
  signal rf_enable                     : std_logic;  -- to enable/disable rf
  signal data_i                        : std_logic_vector(13 downto 0);  -- I data of the sample to send
  signal data_q                        : std_logic_vector(13 downto 0);  -- Q data of the sample to send
  signal internal_fsm_state            : std_logic_vector(3 downto 0);  -- to read the current state
  signal internal_sel_axis             : std_logic;  -- select between I or Q to serialize
  signal internal_counter              : std_logic_vector(3 downto 0);  -- 16/2 cycles to count to serialize
  signal internal_start_shift_register : std_logic_vector(2 downto 0);
  signal internal_stop_shift_register  : std_logic_vector(2 downto 0);
begin

  -- sync start stop with "triplette de resyncho" three stages registers
  start_sync : process(clk, rstb)
  begin
    if clk'event and clk = '1' then
      if rstb = '0' then
        internal_start_shift_register <= (others => '0');
      else
        internal_start_shift_register <= start & internal_start_shift_register(2 downto 1);
      end if;
    end if;
  end process;

  -- sync stop stop with "triplette de resyncho" three stages register
  stop_sync : process(clk, rstb)
  begin    if clk'event and clk = '1' then
      if rstb = '0' then
        internal_stop_shift_register <= (others => '0');
      else
        internal_stop_shift_register <= stop & internal_stop_shift_register(2 downto 1);
      end if;
    end if;
  end process;

  rf_controller_tx_1 : entity work.rf_controller_tx
    port map (
      clk       => clk,
      rstb      => rstb,
      start     => internal_start_shift_register(0),
      stop      => internal_stop_shift_register(0),                 -- to enable/disable module
      sample_nb => sample_nb,            -- number of sample (I,Q) to
      dout      => dout,                 -- fifo output to read the
      rd_en     => rd_en,                -- reading request
      rf_enable => rf_enable,            -- to enable/disable rf
      data_i    => data_i,               -- I data of the sample to send
      data_q    => data_q,               -- Q data of the sample to send
      fsm_state => internal_fsm_state);  -- to read the current state

  rf_interface_rx_1 : entity work.rf_interface_rx
    port map (
      clk      => clk,                -- RF module supplies it
      rstb     => rstb,               -- FPGA reset
      data_tx  => data_tx,            -- serialized bit of the sample
      enable   => rf_enable,          -- to enable/disable module
      data_i   => data_i,             -- I data of the sample to send
      data_q   => data_q,             -- Q data of the sample to send
      sel_axis => internal_sel_axis,  -- select between I or Q to serialize
      counter  => internal_counter);  -- 16/2 cycles to count to serialize

end behavioral;
