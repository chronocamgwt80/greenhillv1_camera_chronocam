--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief RF interface module(TX)
--! @details
--! This module gets a sample to send to RF module. First, it gets I and Q of the
--! sample and then serializes them with a shift register.
--! - Output = 32 bits serialized word
--! - I(15:0) = 2 sync bits & 14 data bits
--! - Q(15:0) = 2 sync bits & 14 data bits
--! - Serialized word(31:0) = I(15:0) & Q(15:0)
--------------------------------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------
-- RF TX interface entity
------------------------------------------------------------------------------
entity rf_tx_interface is
  port (
    clk             : in  std_logic;  -- RF module supplies it
    rstb            : in  std_logic;  -- FPGA reset
    -- rf_interface
    data_tx         : out std_logic_vector(1 downto 0);  -- serialized bit of the sample
    -- fpga interface
    enable          : in  std_logic;  -- to enable/disable module
    fifo_dout_valid : in  std_logic;  -- flag indicating valid data on dout fifo
                                      -- bus
    fifo_rd_en      : out std_logic;  -- data read request
    sample_i_q      : in  std_logic_vector(27 downto 0);  -- data of the sample to send
    -- monitoring
    counter         : out std_logic_vector(3 downto 0)  -- 16/2 cycles to count to serialize
                              -- all I or Q (double data rate)
    );
end entity rf_tx_interface;

------------------------------------------------------------------------------
-- RF TX interface behavior
------------------------------------------------------------------------------
architecture behavior of rf_tx_interface is
  signal internal_sample_i_q_valid      : std_logic;
  signal internal_fifo_rd_en            : std_logic;
  signal internal_shift_register        : std_logic_vector(31 downto 0);
  signal internal_counter               : unsigned(3 downto 0);
  constant i_sync                       : std_logic_vector(1 downto 0) := "10";
  constant q_sync                       : std_logic_vector(1 downto 0) := "01";
  signal internal_enable_shift_register : unsigned(2 downto 0);
  signal enable_sync                    : std_logic;
  signal internal_data_tx_shift_register : std_logic_vector(2 downto 0);
begin

  -- outputs updating
  internal_sample_i_q_valid <= fifo_dout_valid;
  fifo_rd_en                <= internal_fifo_rd_en;
  data_tx                   <= internal_data_tx_shift_register(2) & internal_data_tx_shift_register(1);
  counter                   <= std_logic_vector(internal_counter);
  enable_sync               <= internal_enable_shift_register(0);

  -- sync enable with "triplette de resyncho" three stages registers
  start_sync : process(clk, rstb)
  begin
    if rstb = '0' then
      internal_enable_shift_register <= (others => '0');
    elsif clk'event and clk = '1' then
      internal_enable_shift_register <= enable & internal_enable_shift_register(2 downto 1);
    else
      internal_enable_shift_register <= internal_enable_shift_register;
    end if;
  end process;


  -- delay to align uneven bits on RF CLK rising edge and even bits on
  -- RF CLK falling on the ODDR output (not on this component output)
  --data_tx_delaying : process(clk, rstb)
  --begin
  --  if rstb = '0' then
  --    internal_data_tx_shift_register <= (others => '0');
  --  elsif clk'event and clk = '1' then
  --    internal_data_tx_shift_register <= internal_data_tx_shift_register(0) & internal_shift_register(31) & internal_shift_register(30);
  --  else
  --    internal_data_tx_shift_register <= internal_data_tx_shift_register;
  --  end if;
  --end process;

-- The counter is incremented after each clk event (double data rate),
-- a bit is sent for each counter value.
  serializer_counter : process(rstb, clk, enable)
  begin
    if rstb = '0' then
      internal_counter <= (others => '0');
    elsif clk'event and clk = '1' then
      if enable_sync = '1' then
        -- module is active
        case internal_counter is
          when x"F" =>
            internal_counter <= (others => '0');
          when others =>
            internal_counter <= internal_counter + 1;
        end case;
      else
        -- module is sleeping
        internal_counter <= (others => '0');
      end if;
    else
      internal_counter <= internal_counter;
    end if;
  end process serializer_counter;



-- Serialize bit on each clk event
  serializer : process (rstb, clk) is
  begin
    if rstb = '0' then
      internal_fifo_rd_en     <= '0';
      internal_shift_register <= (others => '0');
    elsif clk'event and clk = '1' then
      if enable_sync = '1' then
        -- serialize I data
        case internal_counter is
          when x"0" =>
            if internal_sample_i_q_valid = '1' then
              internal_fifo_rd_en     <= '1';
              internal_shift_register <= i_sync & sample_i_q(27 downto 14) & q_sync & sample_i_q(13 downto 0);
            else
              internal_fifo_rd_en     <= '0';
              internal_shift_register <= (others => '0');
            end if;
          when others =>
            internal_fifo_rd_en     <= '0';
            internal_shift_register <= internal_shift_register(29 downto 0) & "00";
        end case;
      else
        internal_fifo_rd_en     <= '0';
        internal_shift_register <= (others => '0');
      end if;
    else
      internal_fifo_rd_en     <= internal_fifo_rd_en;
      internal_shift_register <= internal_shift_register;
    end if;
  end process serializer;

end behavior;
