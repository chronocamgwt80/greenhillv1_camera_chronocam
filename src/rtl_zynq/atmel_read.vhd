
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity atmel_read is
  generic (POS_EDGE_CAPTURE : boolean := true);
  port (
    clk              : in  std_logic;
    resetb           : in  std_logic;
    data_in          : in  std_logic_vector(31 downto 0);  -- to be resync
    data_rdy         : in  std_logic;                      -- to be resync
    bram_data_out    : out std_logic_vector(31 downto 0);
    bram_data_out_en : out std_logic);
end atmel_read;



architecture rtl of atmel_read is

  signal data_rdy_resync : std_logic_vector(3 downto 0);

begin


  bram_data_out <= data_in;  -- no need to be catched, false path required

  data_rdy_in_resync_p : process(clk, resetb)
  begin
    if clk'event and clk = '1' then
      if resetb = '0' then
        data_rdy_resync <= (others => '0');
      else
        data_rdy_resync <= data_rdy_resync(2 downto 0) & data_rdy;
      end if;
    end if;
  end process;

  READ_POLARITY_POS : if POS_EDGE_CAPTURE = true generate
    bram_data_out_en <= '1' when data_rdy_resync(3) = '0' and data_rdy_resync(2) = '1' else '0';
  end generate READ_POLARITY_POS;

  READ_POLARITY_NEG : if POS_EDGE_CAPTURE = false generate
    bram_data_out_en <= '1' when data_rdy_resync(3) = '1' and data_rdy_resync(2) = '0' else '0';
  end generate READ_POLARITY_NEG;

end rtl;
