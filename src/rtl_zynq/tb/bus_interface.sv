`ifndef VHDL_PACKAGE_LOAD
`define VHDL_PACKAGE_LOAD

 // reg address list :
`define FIRMWARE_VERSION_REG_ADDR 16'h0000
`define RF_TX_FIFO_CTRL_REG_ADDR 16'h0004
`define RF_TX_FIFO_STATUS_REG_ADDR 16'h0008
`define RF_TX_BUFFER_STATUS_REG_ADDR 16'h000C
`define RF_TX_CONTROLLER_REG_ADDR 16'h0010
`define RF_RX_CONTROLLER_REG_ADDR 16'h0014
`define CONFIG_SPI_STATUS_REG_ADDR 16'h0018
`define CONFIG_SPI_CONFIG_REG_ADDR 16'h001C
`define CONFIG_SPI_WR_REG_ADDR 16'h0020
`define CONFIG_SPI_RD_REG_ADDR 16'h0024
`define BRAM_CTRL_REG_0_REG_ADDR 16'h0028
`define BRAM_CTRL_REG_1_REG_ADDR 16'h002C
`define GW_POWER_USER_LEDS_REG_ADDR 16'h0030
`define EXT_SPI_CTRL_REG_ADDR 16'h0034
`define EXT_SPI_CR_REG_ADDR 16'h0038
`define EXT_SPI_FIFO_IN_REG_ADDR 16'h003C
`define EXT_SPI_FIFO_OUT_REG_ADDR 16'h0040

interface vhdl_package();
   import axi_register_package::*;

   register_bus_out         reg_bus_out;
   register_bus_in          reg_bus_in ;

endinterface : vhdl_package

`endif