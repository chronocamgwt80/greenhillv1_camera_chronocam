`ifndef TBENCH_TASK_INCLUDED
`define TBENCH_TASK_INCLUDED

task init_axi_cmd;
  begin
  cmd_mode        = 0;
  cmd_addr        = 0;
  cmd_data_in     = 0;
  cmd_data_in_rdy = 0;
  end
endtask; 
        
task write_axi_cmd;
  input[`C_AXI_ADDR_WIDTH/2-1:0] task_cmd_addr;
  input[`C_AXI_DATA_WIDTH-1:0] task_cmd_data_in;
  begin
      
  @(posedge clk_200);
  cmd_mode        = 0; // 0 =write mode
  cmd_addr        = {16'h0000,task_cmd_addr};
  cmd_data_in     = task_cmd_data_in;
  cmd_data_in_rdy = 1;
     
  @(posedge clk_200);
  cmd_mode        = 0;
  cmd_addr        = 0;
  cmd_data_in     = 0;
  cmd_data_in_rdy = 0;
  #(`CLOCK_200_PERIOD*10);
 
  end
endtask;       

task run_new_acq;
   input start_with_32bit_set_to_zero;
   
  begin
  $display("\nNew acq");    
  update_bram_config();
  start_bram_acq();
  enableClk64();
  if (start_with_32bit_set_to_zero == `TRUE)
    begin
    atmel_burst_en = 0;
    #(`CLOCK_64_PERIOD*32);
    end
  atmel_burst_en = 1;

  // missing sync detection
  #1000;
  if( (bram_write_cnt == 0 && start_with_32bit_set_to_zero == `TRUE) || (bram_write_cnt == 1 && start_with_32bit_set_to_zero == `FALSE) )
    begin
    $display("** Error: Nothing sent to the Bram on time, check sync flag or sample number request");       
    $stop;
    end

  if (start_with_32bit_set_to_zero == `TRUE)
    begin
    @(half_bram_requested == half_bram_cnt);
    #400000;  // minimum delay to wait between two acq due to CDMA processing
    end

    
  end
endtask; // write_axi_cmd

task update_bram_config;
  begin   
  $display(" -- update bram config");    
  write_axi_cmd(`BRAM_CTRL_REG_1_REG_ADDR,  {5'd0, atmel_emulator_en, ctrl_bram_fill_threshold, 4'd0});
  config_cnt = config_cnt +1;
  end
endtask; // write_axi_cmd

task start_bram_acq;
  begin
  $display(" -- start bram acq and release bram resetb"); 
  bram_resetb    = 1'b1; 
  acquisition_en = 1'b1; 
  write_axi_cmd(`BRAM_CTRL_REG_0_REG_ADDR,  {0,acquisition_en,bram_resetb} ); // release reset
  end
endtask; // write_axi_cmd

task stop_bram_acq;
  begin
  $display(" -- stop bram acq and reset bram"); 
  bram_resetb    = 1'b0; 
  acquisition_en = 1'b0;   
  write_axi_cmd(`BRAM_CTRL_REG_0_REG_ADDR,  {0,acquisition_en,bram_resetb} ); // release reset
  end
endtask; // write_axi_cmd

task clearBramIrq;
  begin
  $display(" -- clear bram irq");  
  write_axi_cmd(`BRAM_CTRL_REG_0_REG_ADDR,  {1,2'b00,acquisition_en,bram_resetb} ); // send clear IRQ sig
  end
endtask; //

task sendCDMAdone;
  begin
  $display(" -- send CDMA done");  
  write_axi_cmd(`BRAM_CTRL_REG_1_REG_ADDR,  {5'd0, atmel_emulator_en, ctrl_bram_fill_threshold, 4'd1}); // send cdma done sig
  end
endtask; //


task initBramEmul;
  begin      
  file = $fopen("bram.txt","w+"); 
  $fwrite(file, "Sample \tAddress \tData\n");
  $fclose(file);        
  end
endtask; 

task writeIntoBram;
  begin
  bram_data_cnt = bram_data_cnt + 1 ;   
  file = $fopen("bram.txt","a+"); 
  $fwrite(file, "%d\t%08h\t%08h\t%02b %03h %02b    %02b %03h %02b\n",bram_data_cnt,BRAM_Addr,BRAM_WrData, BRAM_WrData[31:30], BRAM_WrData[29:18], BRAM_WrData[17:16], BRAM_WrData[15:14], BRAM_WrData[13:2], BRAM_WrData[1:0]);
  $fclose(file);    
  end
endtask; 

task writeNewAcqToBram;
  begin
  bram_data_cnt = 0;
  file = $fopen("bram.txt","a+"); 
  $fwrite(file, "-- Start a new acquisition test --\n");
  $fclose(file);    
  end
endtask; 
    


task disableClk64;
   begin
   clk_64_delay_en = 0;
   end
endtask;

task enableClk64;
   begin
   clk_64_delay_en = 1;
   end
endtask;

task enable_serdes_emul;
  begin   
  $display(" -- set serdes emulator");  
  serdes_force_emulator = 1'b1;
  write_axi_cmd(`RF_RX_CONTROLLER_REG_ADDR,  {31'd0,serdes_force_emulator}); // send cdma done sig

     
  end
endtask; 


task disalbe_serdes_emul;  
  begin   
  $display(" -- release serdes emulator");  
  serdes_force_emulator = 1'b0;
  write_axi_cmd(`RF_RX_CONTROLLER_REG_ADDR,  {31'd0,serdes_force_emulator}); // send cdma done sig
  end
endtask; 




`endif
