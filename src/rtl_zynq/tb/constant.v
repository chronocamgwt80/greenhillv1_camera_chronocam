// Constant declaration for simulation environnement
// Author : M.BARRE

`ifndef CONSTANT_INCLUDED
`define CONSTANT_INCLUDED

`define C_AXI_DATA_WIDTH 32
`define C_AXI_ADDR_WIDTH 32

`define CLOCK_200_PERIOD 5.0
`define CLOCK_64_PERIOD  15.625
`define CLK_START_VALUE    0


`define READ_EN          1
`define WRITE_EN         0

`define TRUE 1
`define FALSE 0

`endif
