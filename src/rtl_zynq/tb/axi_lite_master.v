///////////////////////////////////////////////////////////////////////////////
//
// AXI4-Lite Master
//
////////////////////////////////////////////////////////////////////////////
//
// Structure:
//   axi_lite_master
//
// Last Update:
//   7/8/2010
////////////////////////////////////////////////////////////////////////////
/*
 AXI4-Lite Master Example
 
 The purpose of this design is to provide a simple AXI4-Lite example.

 The distinguishing characteristics of AXI4-Lite are the single-beat transfers,
 limited data width, and limited other transaction qualifiers. These make it
 best suited for low-throughput control functions. 
 
 The example user application will perform a set of writes from a lookup
 table. This may be useful for initial register configurations, such as
 setting the AXI_VDMA register settings. After completing all the writes,
 the example design will perform reads and attempt to verify the values.
 
 If the reads match the write values and no error responses were captured,
 the DONE_SUCCESS output will be asserted.
 
 To modify this example for other applications, edit/remove the logic
 associated with the 'Example' section comments. Generally, this example
 works by the user providing a 'push_write' or 'pop_read' command to initiate
 a command and data transfer.
 
 The latest version of this file can be found in Xilinx Answer 37425
 http://www.xilinx.com/support/answers/37425.htm
*/
`timescale 1ns/1ps

module axi_lite_master #
   (
    parameter integer C_M_AXI_ADDR_WIDTH = 10,
    parameter integer C_M_AXI_DATA_WIDTH = 32
    )
   (
    // System Signals
    input wire 				   M_AXI_ACLK,
    input wire 				   M_AXI_ARESETN,

    // Master Interface Write Address
    output wire [C_M_AXI_ADDR_WIDTH-1:0]   M_AXI_AWADDR,
    output wire [3-1:0] 		   M_AXI_AWPROT,
    output wire 			   M_AXI_AWVALID,
    input wire 				   M_AXI_AWREADY,

    // Master Interface Write Data
    output wire [C_M_AXI_DATA_WIDTH-1:0]   M_AXI_WDATA,
    output wire [C_M_AXI_DATA_WIDTH/8-1:0] M_AXI_WSTRB,
    output wire 			   M_AXI_WVALID,
    input wire 				   M_AXI_WREADY,

    // Master Interface Write Response
    input wire [2-1:0] 			   M_AXI_BRESP,
    input wire 				   M_AXI_BVALID,
    output wire 			   M_AXI_BREADY,

    // Master Interface Read Address
    output wire [C_M_AXI_ADDR_WIDTH-1:0]   M_AXI_ARADDR,
    output wire [3-1:0] 		   M_AXI_ARPROT,
    output wire 			   M_AXI_ARVALID,
    input wire 				   M_AXI_ARREADY,

    // Master Interface Read Data 
    input wire [C_M_AXI_DATA_WIDTH-1:0]    M_AXI_RDATA,
    input wire [2-1:0] 			   M_AXI_RRESP,
    input wire 				   M_AXI_RVALID,
    output wire 			   M_AXI_RREADY,
    
    /// ASYGN command bus
    input wire 				   cmd_mode, // 1 = read_en // 0 = write_en
    input wire [C_M_AXI_DATA_WIDTH-1:0]    cmd_addr,
    input wire [C_M_AXI_DATA_WIDTH-1:0]    cmd_data_in,
    input wire 				   cmd_data_in_rdy,
    output wire [C_M_AXI_DATA_WIDTH-1:0]   cmd_data_out,
    output wire 			   cmd_data_out_rdy
    
    );

   // AXI4 signals
   reg 		awvalid;
   reg 		wvalid;
   wire 	push_write;
   wire 	pop_read;
   reg          arvalid;
   reg          rready;
   reg          bready;
   wire [31:0] 	awaddr;
   wire [31:0] 	wdata;
   wire [31:0] 	araddr;
   wire 	write_resp_error;
   wire         read_resp_error;


   
//Write Address (AW)
assign M_AXI_AWADDR  = awaddr;
assign M_AXI_WDATA   = wdata;
assign M_AXI_AWPROT  = 3'h0;
assign M_AXI_AWVALID = awvalid;

//Write Data(W)
assign M_AXI_WVALID = wvalid;
assign M_AXI_WSTRB  = -1;//Set all byte strobes in this example

//Write Response (B)
assign M_AXI_BREADY = bready;

///////////////////   
//Read Address (AR)
///////////////////
assign M_AXI_ARADDR = araddr;
assign M_AXI_ARVALID = arvalid;
assign M_AXI_ARPROT = 3'b0;

////////////////////////////
//Read and Read Response (R)
////////////////////////////
assign M_AXI_RREADY = rready;


   
//Write Address Channel
always @(posedge M_AXI_ACLK)
  begin
     if (M_AXI_ARESETN == 0 )
       awvalid <= 1'b0;
     else if (M_AXI_AWREADY && awvalid)
       awvalid <= 1'b0;
     else if (push_write)
       awvalid <= 1'b1;
     else
       awvalid <= awvalid;
  end 

//Write Data Channel
   always @(posedge M_AXI_ACLK)
  begin
      if (M_AXI_ARESETN == 0 )
	wvalid <= 1'b0;
      else if (M_AXI_WREADY && wvalid)
	wvalid <= 1'b0;
     else if (push_write)
       wvalid <= 1'b1;
     else
       wvalid <= awvalid;
  end 

//Always accept write responses
always @(posedge M_AXI_ACLK)
  begin
     if (M_AXI_ARESETN == 0)
 	  bready <= 1'b0;
      else
 	  bready <= 1'b1;
  end

//Flag write errors
assign write_resp_error = bready & M_AXI_BVALID & M_AXI_BRESP[1];
    
//Read Address Channel
always @(posedge M_AXI_ACLK)
  begin
     if (M_AXI_ARESETN == 0 )
       arvalid <= 1'b0;
     else if (M_AXI_ARREADY && arvalid)
       arvalid <= 1'b0;
     else if (pop_read)
       arvalid <= 1'b1;
     else
       arvalid <= arvalid;
  end 
 
//Read Data (and Response) Channel
always @(posedge M_AXI_ACLK)
  begin
     if (M_AXI_ARESETN == 0)
 	  rready <= 1'b0;
      else
 	  rready <= 1'b1;
   end

//Flag write errors
assign read_resp_error = rready & M_AXI_RVALID & M_AXI_RRESP[1];  


///////////////////////
//Main write controller
///////////////////////

reg internal_cmd_mode; // 1 = read_en // 0 = write_en
reg internal_new_cmd_rdy; 
reg [C_M_AXI_ADDR_WIDTH-1:0]    internal_cmd_addr;
reg [C_M_AXI_DATA_WIDTH-1:0]    internal_cmd_data_in;
   
always @(posedge M_AXI_ACLK) // latch process
  begin
      if (M_AXI_ARESETN == 0 )
	begin
           internal_cmd_mode    <= 0;
           internal_cmd_addr    <= 0;
           internal_cmd_data_in <= 0;
           internal_new_cmd_rdy <= 0;
	end
      else if(cmd_data_in_rdy == 1)
	begin
           internal_cmd_mode    <= cmd_mode;
           internal_cmd_addr    <= cmd_addr ;
           internal_cmd_data_in <= cmd_data_in;
           internal_new_cmd_rdy <= 1;
	end
      else
	begin
           internal_cmd_mode    <= internal_cmd_mode;
           internal_cmd_addr    <= internal_cmd_addr ;
           internal_cmd_data_in <= internal_cmd_data_in;
           internal_new_cmd_rdy <= 0;
	end 
  end

assign awaddr           = internal_cmd_addr;   
assign wdata            = internal_cmd_data_in;
assign push_write       = (internal_new_cmd_rdy && (!internal_cmd_mode)); 
assign araddr           = internal_cmd_addr;   
assign pop_read         = (internal_new_cmd_rdy && (internal_cmd_mode)); 
assign cmd_data_out     = M_AXI_RDATA;
assign cmd_data_out_rdy = M_AXI_RVALID;
      





   
endmodule
