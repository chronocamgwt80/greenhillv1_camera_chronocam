
`timescale 1ns/1fs

`include "constant.v"
`include "bus_interface.sv" 

module top_tbench ();
   
   vhdl_package design_bus();

   // AXI CMDS
   reg                        cmd_mode;
   reg[`C_AXI_ADDR_WIDTH-1:0] cmd_addr;   
   reg[`C_AXI_DATA_WIDTH-1:0] cmd_data_in;
   reg                        cmd_data_in_rdy;
   reg[`C_AXI_DATA_WIDTH-1:0] cmd_data_out;
   reg                        cmd_data_out_rdy;
   // CLK
   reg                        clk_64_delay_en;   
   reg                        clk_200;   
   reg                        clk_64;
   // RST
   reg                        resetb;
   // BRAM
   reg                        BRAM_CLK;
   reg[31:0]                  BRAM_Addr;
   reg[31:0]                  BRAM_WrData;
   reg[3:0]                   BRAM_WE;
   reg                        BRAM_En;                  
   reg                        BRAM_rst;                 
   reg                        bram_irq;
   reg[21:0]                  ctrl_bram_fill_threshold;
   reg                        atmel_emulator_en;
   reg[8:0]                   half_bram_cnt;
   reg[8:0]                   half_bram_requested;
   integer                    config_cnt,file,bram_data_cnt;
   reg                        acquisition_en;
   reg                        bram_resetb;
   reg                        atmel_data_en;
   reg                        atmel_burst_en;
   reg                        serdes_force_emulator;                    
   reg[3:0]                   atmel_serial_data_cnt;
   reg[31:0]                  atmel_serial_data_buff;
   reg[31:0]                  atmel_serial_data_buff_save;
   reg[31:0]                  atmel_serial_data_buff_save_prev;
   reg[11:0]                  atmel_serial_data_counter;
   wire                       atmel_serial_data_1;
   wire                       atmel_serial_data_0;
   reg[1:0]                   serdes_fsm_state;
   integer                    bram_write_cnt;
      
   
 
`include "task.v"
   
   
register_model  register_inst(
               .clk              (clk_200),
               .resetb           (resetb),
               .axi_reg_out      (design_bus.reg_bus_out),
               .axi_reg_in       (design_bus.reg_bus_in),
                .cmd_mode        (cmd_mode),
               .cmd_addr         (cmd_addr),
               .cmd_data_in      (cmd_data_in),
               .cmd_data_in_rdy  (cmd_data_in_rdy),
               .cmd_data_out     (cmd_data_out),
               .cmd_data_out_rdy (cmd_data_out_rdy));
   
bram_custom_ctrl bram_custom_ctrl_inst(
               .clk_200                  (clk_200),
               .clk_64                   (clk_64),
               .resetb                   (design_bus.reg_bus_out.bram_resetb),
               .ctrl_acquisition_en      (design_bus.reg_bus_out.bram_acquisition_en),
               .ctrl_overflow_flag_clear (design_bus.reg_bus_out.bram_overflow_flag_clear),
               .ctrl_overflow_flag       (design_bus.reg_bus_in.bram_overflow_flag),
               .ctrl_irq_clear           (design_bus.reg_bus_out.bram_irq_clear),
               .ctrl_irq                 (bram_irq),
               .ctrl_polarity            (design_bus.reg_bus_in.bram_polarity),
               .ctrl_packet_timestamp    (design_bus.reg_bus_in.bram_packet_timestamp),
               .ctrl_cdma_done           (design_bus.reg_bus_out.cdma_transfert_done),
               .ctrl_bram_fill_threshold (design_bus.reg_bus_out.bram_fill_threshold),
               .atmel_serial_data_0      (atmel_serial_data_0),
               .atmel_serial_data_1      (atmel_serial_data_1),
               .emulator_en              (design_bus.reg_bus_out.emulator_en),
               .serdes_force_emulator    (design_bus.reg_bus_out.rf_rx_serdes_force_emulator),
               .serdes_sync_flag         (design_bus.reg_bus_in.rf_rx_serdes_sync_flag),
               .BRAM_CLK                 (BRAM_CLK),
               .BRAM_Addr                (BRAM_Addr),
               .BRAM_WrData              (BRAM_WrData),
               .BRAM_WE                  (BRAM_WE),
               .BRAM_rst                 (BRAM_rst),
               .BRAM_En                  (BRAM_En),
               .serdes_fsm_state         (serdes_fsm_state));

   
initial// register input init
  begin
  design_bus.reg_bus_in.firmware_version         = 0;
  design_bus.reg_bus_in.rf_tx_fifo_empty_flag    = 0;
  design_bus.reg_bus_in.rf_tx_fifo_full_flag     = 0;
  design_bus.reg_bus_in.rf_tx_buffer_empty_flag  = 0;
  design_bus.reg_bus_in.rf_tx_buffer_full_flag   = 0;
  design_bus.reg_bus_in.rf_tx_buffer_data_count  = 0;
  design_bus.reg_bus_in.config_spi_rx_full_flag  = 0;
  design_bus.reg_bus_in.config_spi_rx_empty_flag = 0;
  design_bus.reg_bus_in.config_spi_rx_thres_flag = 0;
  design_bus.reg_bus_in.config_spi_rx_data_count = 0;
  design_bus.reg_bus_in.config_spi_tx_full_flag  = 0;
  design_bus.reg_bus_in.config_spi_tx_empty_flag = 0;
  design_bus.reg_bus_in.config_spi_tx_thres_flag = 0;
  design_bus.reg_bus_in.config_spi_tx_data_count = 0;
  design_bus.reg_bus_in.config_spi_data_rd       = 0;
  end 

initial // tbench init
  begin
  initBramEmul();
  bram_data_cnt                    = 0;
  atmel_serial_data_cnt            = 0;
  atmel_serial_data_buff           = 0;
  atmel_serial_data_buff_save      = 0;
  atmel_serial_data_buff_save_prev = 0;
  atmel_serial_data_counter        = 0;
  atmel_data_en                    = 1'b0;
  atmel_burst_en                   = 0;
  acquisition_en                   = 0;
  bram_resetb                      = 0;
  config_cnt                       = 0;     
  clk_64_delay_en                  = 1'b0 ;
  clk_64                           = `CLK_START_VALUE ;
  clk_200                          = `CLK_START_VALUE ;
  resetb                           = 1'b0;
  serdes_force_emulator            = 1'b0;
  #30			           
  resetb                           = 1'b1;
  end

always #(`CLOCK_64_PERIOD/2)  clk_64  = (clk_64_delay_en == 1) ? !clk_64 : 0;
always #(`CLOCK_200_PERIOD/2) clk_200 = !clk_200;

always@(posedge clk_200) 
  if(BRAM_En == 1)
    writeIntoBram();

always@(posedge acquisition_en)
  writeNewAcqToBram();
   
   
// IDDR output emulation
// settings : same_edge_pipelined
always@(posedge clk_64)
  begin
    atmel_serial_data_cnt = atmel_serial_data_cnt + 1 ;
    if (atmel_burst_en == 1)
      begin
      atmel_serial_data_counter        = atmel_serial_data_counter + 1;
      if(atmel_serial_data_cnt == 0)
        begin
           atmel_serial_data_buff           = {2'b10,atmel_serial_data_counter,2'b00,2'b01,~atmel_serial_data_counter,2'b00};
           atmel_serial_data_buff_save_prev = atmel_serial_data_buff_save ;
           atmel_serial_data_buff_save      = atmel_serial_data_buff ;
        end
      else 
        begin
           atmel_serial_data_buff           = {atmel_serial_data_buff[29:0] ,2'b00};
           atmel_serial_data_buff_save_prev = atmel_serial_data_buff_save_prev ;
           atmel_serial_data_buff_save      = atmel_serial_data_buff_save ;
        end
      end // if (atmel_burst_en == 1)
    else
      begin
        atmel_serial_data_buff           = {atmel_serial_data_buff[29:0] ,2'b00};
        atmel_serial_data_buff_save_prev = atmel_serial_data_buff_save_prev ;
        atmel_serial_data_buff_save      = atmel_serial_data_buff_save ;
      end // else: !if(atmel_burst_en == 1)
     
  end
      
assign  atmel_serial_data_0 = atmel_serial_data_buff[31];
assign  atmel_serial_data_1 = atmel_serial_data_buff[30];

// assert section

initial // tbench init
  begin
  bram_write_cnt = 0 ;
  end
  
always@(negedge BRAM_En)
  if (BRAM_WrData != atmel_serial_data_buff_save_prev)
    begin
    $display("** Error: Wrong value sent to the BRAM");
    $stop;
    end

always@(negedge BRAM_En or negedge bram_resetb)
  if (bram_resetb == 0)
    bram_write_cnt = 0;
  else
    bram_write_cnt = bram_write_cnt + 1 ;

   
   
////////////////////////////////////////////////
////////////// firmware emulation //////////////
////////////////////////////////////////////////  

//assign  half_bram_requested = ( ctrl_bram_fill_threshold[12:0] == 0) ? ctrl_bram_fill_threshold[21:13] : ctrl_bram_fill_threshold[21:13] + 1; // estimates the number of half bram to read for the current acq
assign  half_bram_requested = ctrl_bram_fill_threshold[21:13] + 1; 
  
always@(posedge clk_200 or negedge resetb or negedge acquisition_en) // bram irq management
  if(resetb == 0 || acquisition_en == 0)
    begin
    half_bram_cnt = 0;
    end
  else if(bram_irq == 1)
    begin
    clearBramIrq();
    half_bram_cnt = half_bram_cnt+1;
    if (half_bram_cnt == half_bram_requested)
      begin
      #1 // 0 delay simulation issue
      half_bram_cnt = 0;
      disableClk64();   
      stop_bram_acq();
      end
       
    #300000; // estimated delay (CDMA processing)
    sendCDMAdone();
 
    end
  else
    begin
    half_bram_cnt = half_bram_cnt;
    end


initial // sending command through the axi bus 
  begin
     init_axi_cmd();     
@(posedge resetb);
     atmel_emulator_en = 1'b0;
  
     ctrl_bram_fill_threshold = {8'h01,14'b00000000000000}; // unit = sample (equivalent to {bram, sample}
     //run_new_acq(`TRUE);


     //enable_serdes_emul(); --

     ctrl_bram_fill_threshold = {8'h00,14'b01111111111110}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);
     ctrl_bram_fill_threshold = {8'h00,14'b01111111111111}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);
     
     ctrl_bram_fill_threshold = {8'h00,14'b10000000000000}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);

     ctrl_bram_fill_threshold = {8'h00,14'b10000000000001}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);
     ctrl_bram_fill_threshold = {8'h00,14'b10000000000010}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);


     $finish(0);
     
     run_new_acq(`FALSE);
  
     ctrl_bram_fill_threshold = {8'h01,14'b00000000000001}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);
     
     ctrl_bram_fill_threshold = {8'h00,14'b11111111111111}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);
     
     ctrl_bram_fill_threshold = {8'h0F,14'b00000000000000}; // unit = sample (equivalent to {bram, sample}
     run_new_acq(`TRUE);

     #1000;
     $finish(0);
  end

endmodule 


