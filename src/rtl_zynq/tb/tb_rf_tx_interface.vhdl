--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief Test bench for RF interface module(TX)
--! @details
--! to check:
--! - word(31:0) is serialized
--! - uneven bits serialized on rising edge
--! - even bits serialized on falling edge
--! - no serialized bit when module is disabled
--! - behavior when
--!     => enable device while rf module does not supply its clock
--!     => enable device before a rising edge while rf module supplies already its clock
--!     => enable device before a falling edge while rf module supplies already its clock
--!     => rf module supplies already its clock while device is already enable
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity tb_rf_tx_interface is
end tb_rf_tx_interface;

architecture behavior of tb_rf_tx_interface is

  -- Constant
  constant clk_period         : time := 15.625 ns;
  constant rst_time           : time := 50 ns;
  constant burst_time         : time := 300 ns;  -- more than 1 sample transfer (250ns)
  -- remote RF module enable signal
  signal rf_rx_clk_en         : std_logic;  -- enable rf_rx_clk emulating
  -- Main signals
  signal rf_clk               : std_logic;  -- ! RF CLK
  signal rf_fpga_arst         : std_logic;  -- ! asynchronous reset active high, from FPGA
  signal rf_txrst             : std_logic;  -- ! reset active high, from register
  signal rf_rxrst             : std_logic;  -- ! reset active high, from register
  signal rf_txen              : std_logic;  -- ! enable RF TX
  signal rf_txrd_din_en       : std_logic;  -- ! flag to generate a writing request
  signal rf_txdin_valid       : std_logic;  -- ! flag indicating a new sample IQ is ready
  signal rf_txdin             : std_logic_vector(27 downto 0);  -- ! sample IQ to write
  signal rf_txfull            : std_logic;  -- ! flag indicating buffer is full
  signal rf_txempty           : std_logic;  -- ! flag indicating buffer is empty
  signal rf_txout_data_vector : std_logic_vector(1 downto 0);
  signal rf_txloopburst_mode  : std_logic;  -- ! to set in loop or burst mode
begin
  -- Instantiate the Unit Under Test (UUT)
  rf_1 : entity work.rf
    port map (
      rf_clk               => rf_clk,    -- ! RF CLK
      rf_fpga_arst         => rf_fpga_arst,  -- ! asynchronous reset active high, from FPGA
      rf_txrst             => rf_txrst,  -- ! reset active high, from register
      rf_rxrst             => rf_rxrst,  -- ! reset active high, from register
      rf_txen              => rf_txen,   -- ! enable RF TX
      rf_txrd_din_en       => rf_txrd_din_en,  -- ! flag to generate a writing request
      rf_txdin_valid       => rf_txdin_valid,  -- ! flag indicating a new sample IQ is ready
      rf_txdin             => rf_txdin,  -- ! sample IQ to write
      rf_txfull            => rf_txfull,    -- ! flag indicating buffer is full
      rf_txempty           => rf_txempty,   -- ! flag indicating buffer is empty
      rf_txout_data_vector => rf_txout_data_vector,
      rf_txloopburst_mode  => rf_txloopburst_mode);  -- ! to set in loop or burst mode

  -- Clock process definitions
  clk_process : process
  begin
    rf_clk <= '0';
    wait for clk_period/2;
    if rf_rx_clk_en = '1' then
      rf_clk <= '1';
    else
      rf_clk <= '0';
    end if;
    wait for clk_period/2;
  end process;

  -- Stimulus process
  stim_proc : process
  begin

    -- init
    rf_fpga_arst        <= '1';
    rf_rx_clk_en        <= '0';
    rf_txen             <= '0';
    rf_txrst            <= '0';
    rf_rxrst            <= '0';
    rf_txloopburst_mode <= '0';
    
    -- hold reset
    wait for rst_time;
    rf_fpga_arst        <= '0';
    wait for rst_time;


    -- RF RXCLK enable
    rf_rx_clk_en <= '1';
    wait for burst_time;



    --signal rf_txen              : std_logic;  -- enable RF TX
    --signal rf_txrd_din_en       : std_logic;  -- flag to generate a writing request
    --signal rf_txdin_valid       : std_logic;  -- flag indicating a new sample IQ is ready
    --signal rf_txdin             : std_logic_vector(27 downto 0);  -- sample IQ to write
    --signal rf_txfull            : std_logic;  -- flag indicating buffer is full
    --signal rf_txempty           : std_logic;  -- flag indicating buffer is empty
    --signal rf_txout_data_vector : std_logic_vector(1 downto 0);



    -- Fill FIFO
    rf_txdin_valid <= '1';
    rf_txdin       <= x"0000000";
    wait for clk_period;
    rf_txdin       <= x"FFFFFFF";
    wait for clk_period;
    --rf_txdin       <= x"0000003";
    --wait for clk_period;
    --rf_txdin       <= x"0000004";
    --wait for clk_period;
    --rf_txdin       <= x"0000005";
    --wait for clk_period;
    --rf_txdin       <= x"0000006";
    --wait for clk_period;
    --rf_txdin       <= x"0000007";
    --wait for clk_period;
    --rf_txdin       <= x"0000008";
    --wait for clk_period;
    --rf_txdin       <= x"0000009";
    --wait for clk_period;
    --rf_txdin       <= x"000000a";
    --wait for clk_period;
    --rf_txdin       <= x"000000b";
    --wait for clk_period;
    rf_txdin_valid <= '0';
    wait for burst_time;

    -- start transmission
    rf_txen <= '1';
    wait for burst_time;
    wait for burst_time;
    wait for burst_time;
    wait for burst_time;

    -- stop transmission
    rf_txen <= '0';
    wait for burst_time;


    -- RF RXCLK disable
    rf_rx_clk_en <= '0';
    wait for burst_time;


    -- start transmission
    rf_txen <= '1';
    wait for burst_time;

    -- RF RXCLK enable
    rf_rx_clk_en <= '1';
    wait for burst_time;
    wait for burst_time;
    wait for burst_time;
    wait for burst_time;
    wait for 1000 ns;
    wait;
  end process;
end behavior;
