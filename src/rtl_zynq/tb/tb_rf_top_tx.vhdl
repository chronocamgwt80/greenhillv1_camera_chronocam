--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief Test bench for RF top module(TX)
--! @details
--! to check:
--! - several word(31:0) are serialized
--! - uneven bits serialized on rising edge
--! - even bits serialized on falling edge
--! - no serialized bit when module is disabled
--! - behavior when
--!     => enable device while rf module does not supply its clock
--!     => enable device before a rising edge while rf module supplies already its clock
--!     => enable device before a falling edge while rf module supplies already its clock
--!     => rf module supplies already its clock while device is already enable
--!     => start transmission with zero sample to send
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity tb_rf_top_tx is
end tb_rf_top_tx;

architecture behavior of tb_rf_top_tx is

  -- Constant
  constant clk_period     : time      := 15.625 ns;
  constant rst_time       : time      := 200 ns;
  constant burst_time     : time      := 1 us;
  signal clk              : std_logic;  -- RF module supplies it
  signal rstb             : std_logic;  -- FPGA reset
  -- remote RF module ctrl
  signal RF_RX_clk_enable : std_logic := '0';              -- enable/disable clk
  -- ctrl
  signal start            : std_logic;  -- oneshot (if stop signal is no sent then
                                        -- module will stop at the end)
  signal stop             : std_logic;  -- oneshot
  signal sample_nb        : std_logic_vector(31 downto 0);  -- number of sample (I,Q) to
  -- memory interface
  signal dout             : std_logic_vector(27 downto 0);  -- fifo output to read the
  signal rd_en            : std_logic;  -- reading request
  -- rf_interface
  signal data_tx          : std_logic_vector(1 downto 0);  -- serialized bit of the sample
  -- monitoring
  signal fsm_state        : std_logic_vector(3 downto 0);  -- to read the current state
  signal sel_axis         : std_logic;  -- select between I or Q to serialize
  signal counter          : std_logic_vector(3 downto 0);  -- 16/2 cycles to count to serialize

begin

  rf_top_tx_1 : entity work.rf_top_tx
    port map (
      clk       => clk,        -- RF module supplies it
      rstb      => rstb,       -- FPGA reset
      start     => start,      -- oneshot (if stop signal is no sent then
      stop      => stop,       -- oneshot
      sample_nb => sample_nb,  -- number of sample (I,Q) to
      dout      => dout,       -- fifo output to read the
      rd_en     => rd_en,      -- reading request
      data_tx   => data_tx,    -- serialized bit of the sample
      fsm_state => fsm_state,  -- to read the current state
      sel_axis  => sel_axis,   -- select between I or Q to serialize
      counter   => counter);   -- 16/2 cycles to count to serialize


  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    if RF_RX_clk_enable = '1' then
      clk <= '1';
    else
      clk <= '0';
    end if;
    wait for clk_period/2;
  end process;

  -- reset holding
  rstb <= '0', '1' after rst_time;



  -- Stimulus process
  stim_proc : process
  begin
    -- init
    start            <= '0';
    stop             <= '0';
    RF_RX_clk_enable <= '0';
    dout             <= x"0000000";
    sample_nb        <= x"00000000";
    wait for rst_time;


    -- enable device while rf module does not supply its clock
    RF_RX_clk_enable <= '0';
    wait for 10 ns;
    start            <= '1';
    wait for 40 ns;
    start            <= '0';
    wait for burst_time;

    -- enable device before a rising edge while rf module supplies already its clock
    RF_RX_clk_enable <= '1';
    dout             <= x"5555555";
    sample_nb        <= x"00000003";
    wait for 60 ns;
    wait until clk'event and clk = '0';
    wait for 1 ns;
    start            <= '1';
    wait for 15.872 ns;
    start            <= '0';
    wait for 250 ns;
    dout             <= x"FFFFFFF";
    wait for 250 ns;
    dout             <= x"5555555";
    wait until fsm_state = x"1";
    wait for burst_time;
    RF_RX_clk_enable <= '0';
    wait for burst_time;

    -- enable device before a falling edge while rf module supplies already its clock
    RF_RX_clk_enable <= '1';
    dout             <= x"5555555";
    sample_nb        <= x"00000003";
    wait for 60 ns;
    wait until clk'event and clk = '1';
    wait for 1 ns;
    start            <= '1';
    wait for 15.872 ns;
    start            <= '0';
    wait for 250 ns;
    dout             <= x"FFFFFFF";
    wait for 250 ns;
    dout             <= x"5555555";
    wait until fsm_state = x"1";
    wait for burst_time;
    RF_RX_clk_enable <= '0';
    wait for burst_time;

    -- start transmission with zero sample to send
    RF_RX_clk_enable <= '1';
    dout             <= x"5555555";
    sample_nb        <= x"00000000";
    wait for 60 ns;
    wait until clk'event and clk = '1';
    wait for 1 ns;
    start            <= '1';
    wait for 15.872 ns;
    start            <= '0';
    wait for 250 ns;
    dout             <= x"FFFFFFF";
    wait for 250 ns;
    dout             <= x"5555555";
    wait for burst_time;
    RF_RX_clk_enable <= '0';
    wait for burst_time;

    -- rf module supplies its clock while device is already enable
    RF_RX_clk_enable <= '0';
    dout             <= x"5555555";
    sample_nb        <= x"00000003";
    wait for 60 ns;
    wait until clk'event and clk = '1';
    wait for 1 ns;
    start            <= '1';
    wait for 15.872 ns;
    RF_RX_clk_enable <= '1';
    start            <= '0';
    wait for 250 ns;
    dout             <= x"FFFFFFF";
    wait for 250 ns;
    dout             <= x"5555555";
    wait until fsm_state = x"1";
    RF_RX_clk_enable <= '0';
    wait for burst_time;
    RF_RX_clk_enable <= '0';
    wait for burst_time;


    wait for 1000 ns;
    wait;
  end process;
end behavior;
