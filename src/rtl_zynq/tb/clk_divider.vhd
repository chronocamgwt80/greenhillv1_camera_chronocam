----------------------------------------------------------------------------------
-- Company: Asygn
-- 
-- Create Date: 05/14/2014 04:01:59 PM
-- Design Name: dpodL
-- Module Name: clk_dvider
-- Description:		clock divider
-- 
-- Revision:
-- Revision 1 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
--! @file
--! @brief Clock division
--------------------------------------------------------------------------------
--! @details
--! Mode of operation :\n
--! \f$ f_{clk\_out}={1 \over divider} f_{clk\_in}\f$ \n
--! \f$ T_{clk\_out}= divider T_{clk\_in}\f$
--------------------------------------------------------------------------------

entity clk_divider is
	generic (
		DIVIDER_NBBIT : positive	:= 16;	--! clock division number of bits
		CLK_RST_VALUE : std_logic := '0'	--! clock reset value
		);
	port (
		clk_in			: in	std_logic;			--! clock input
		rstb				: in	std_logic;			--! async reset (active low)
		enable			: in	std_logic;  --! enable
		divider			: in	unsigned(DIVIDER_NBBIT-1 downto 0);	 --! clock division ratio
		counter_out : out unsigned(DIVIDER_NBBIT-1 downto 0);	 --! clock division internal counter
		clk_out			: out std_logic				--! clock output
		);
end clk_divider;

architecture rtl of clk_divider is
	signal counter	 : unsigned(DIVIDER_NBBIT-1 downto 0) := (others => '0');
	signal clk_out_q : std_logic													:= '0';

	
begin


	-- purpose: Clock divider main process
	-- type		: sequential
	-- inputs : clk_in, rstb
	-- outputs: counter, clk_out_q
	clk_gen : process(clk_in, rstb)
	begin
		if rstb = '0' then
			counter		<= to_unsigned(1, DIVIDER_NBBIT);
			clk_out_q <= CLK_RST_VALUE;
		elsif clk_in'event and clk_in = '1' then
			if enable = '1' then
				-- test if counter threshold is reached : clk set to default value
				if counter = divider(DIVIDER_NBBIT-1 downto 0) then
					counter		<= to_unsigned(1, DIVIDER_NBBIT);
					clk_out_q <= CLK_RST_VALUE;
				-- test if counter threshold/2 is reached
				elsif counter(DIVIDER_NBBIT-2 downto 0) = divider(DIVIDER_NBBIT-1 downto 1) then
					counter		<= counter + 1;
					clk_out_q <= not CLK_RST_VALUE;
				-- else counter normal operation
				else
					counter		<= counter + 1;
					clk_out_q <= clk_out_q;
				end if;
			else
				counter		<= counter;
				clk_out_q <= clk_out_q;
			end if;
		end if;
	end process clk_gen;
	------------------------------------------------------------------------------- 
	-- to output
	-----------------------------------------------------------------------------
	clk_out			<= clk_out_q;
	counter_out <= counter;

	
end rtl;

