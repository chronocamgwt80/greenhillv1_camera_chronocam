-------------------------------------------------------------------------------
-- Title      : Testbench for design "generic_spi_master"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : generic_spi_master_tb.vhd
-- Author     : Christophe Leblanc  <cleblanc@delhi.lin.asygn.com>
-- Company    : 
-- Created    : 2014-06-26
-- Last update: 2015-10-05
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2014 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-06-26  1.0      cleblanc  Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------

entity tb_generic_spi_master is

end entity tb_generic_spi_master;

--------------------------------------------------------------------------------

architecture beh of tb_generic_spi_master is
  constant bit0                     : std_logic                                             := '0';
  constant bit1                     : std_logic                                             := '1';
  constant TRANSACTION_NBBYTES      : integer                                               := 1;
  constant CLK_TO_SPICLK_RATE_NBBIT : integer                                               := 8;
  constant FIFO_SIZE_NBBIT          : integer                                               := 4;
  signal clk                        : std_logic                                             := '0';
  signal rstb                       : std_logic                                             := '0';
  signal clk_to_spiclk_rate         : std_logic_vector(CLK_TO_SPICLK_RATE_NBBIT-1 downto 0) := x"08";  --
  --x"0050";
  signal data_wr                    : std_logic                                             := '0';
  signal data_in                    : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0)    := (others => '0');
  signal data_rd                    : std_logic                                             := '0';
  signal data_out                   : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0)    := (others => '0');
  signal SPI_CLK                    : std_logic                                             := '0';
  signal SPI_CS                     : std_logic                                             := '0';
  signal SPI_MOSI                   : std_logic                                             := '0';
  signal SPI_MISO                   : std_logic                                             := '0';
  signal data_to_send               : std_logic;
  signal data_to_read               : std_logic;
  -- slave interface
  signal slave_data_wr              : std_logic;  -- ! user fifo write
  signal slave_data_in              : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0)    := (others => '0');
  signal slave_data_rd              : std_logic;  -- ! user fifo read
  signal slave_data_out             : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0)    := (others => '0');
  signal slave_rx_full_flag         : std_logic;
  signal slave_rx_empty_flag        : std_logic;
  signal slave_rx_thres_flag        : std_logic;
  signal slave_rx_data_count        : std_logic_vector(10 downto 0);
  signal slave_tx_full_flag         : std_logic;
  signal slave_tx_empty_flag        : std_logic;
  signal slave_tx_thres_flag        : std_logic;
  signal slave_tx_data_count        : std_logic_vector(10 downto 0);
  signal fifo_rx_rst                : std_logic                                             := '0';
  signal fifo_tx_rst                : std_logic                                             := '0';
  -- simulation
  constant clk_period               : time                                                  := 10 ns;
begin  -- architecture beh

  generic_spi_master_1 : entity work.generic_spi_master
    generic map (
      TRANSACTION_NBBYTES      => TRANSACTION_NBBYTES,
      CLK_TO_SPICLK_RATE_NBBIT => CLK_TO_SPICLK_RATE_NBBIT)
    port map (
      clk                => clk,
      rstb               => rstb,
      spi_config_NEGCLK  => bit0,
      clk_to_spiclk_rate => clk_to_spiclk_rate,
      data_wr            => data_wr,
      data_in            => data_in,
      data_rd            => data_rd,
      data_out           => data_out,
      data_to_send       => data_to_send,
      data_to_read       => data_to_read,
      SPI_CLK            => SPI_CLK,
      SPI_CS             => SPI_CS,
      SPI_MOSI           => SPI_MOSI,
      SPI_MISO           => SPI_MISO);


  generic_spi_slave_1 : entity work.generic_spi_slave
    generic map (
      FIFO_SIZE_NBBIT     => FIFO_SIZE_NBBIT,
      TRANSACTION_NBBYTES => 1)  -- ! fifo depth,  size = 2^FIFO_SIZE_NBBIT
    port map (
      clk           => clk,   -- ! clock
      rstb          => rstb,  -- ! async reset (active low)
      data_wr       => slave_data_wr,   -- ! user fifo write
      data_in       => slave_data_in,   -- !user data write
      data_rd       => slave_data_rd,   -- ! user fifo read
      data_out      => slave_data_out,  -- !user data read
      rx_full_flag  => slave_rx_full_flag,
      rx_empty_flag => slave_rx_empty_flag,
      rx_thres_flag => slave_rx_thres_flag,
      rx_data_count => slave_rx_data_count,
      tx_full_flag  => slave_tx_full_flag,
      tx_empty_flag => slave_tx_empty_flag,
      tx_thres_flag => slave_tx_thres_flag,
      tx_data_count => slave_tx_data_count,
      rx_fifo_rst   => fifo_rx_rst,
      tx_fifo_rst   => fifo_tx_rst,
      SPI_CLK       => SPI_CLK,  -- ! SPI clock
      SPI_CS        => SPI_CS,   -- ! SPI Chip Select
      SPI_MOSI      => SPI_MOSI,        -- ! SPI MOSI
      SPI_MISO      => SPI_MISO);       -- ! SPI MISO

  clk_gen_p : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  rstb <= '0', '1' after 55 ns;

  data_wr_p : process is
  begin  -- process data_wr_p
    data_wr       <= '0';
    slave_data_wr <= '0';
    wait for 1 us;
    slave_data_wr <= '1';
    slave_data_in <= x"87";
    wait for 1*clk_period;
    -- temp
    slave_data_wr <= '0';
    wait for 1*clk_period;
    fifo_tx_rst <= '1';
    wait for clk_period;
    fifo_tx_rst <= '0';
    wait for clk_period;
    slave_data_wr <= '1';
    slave_data_in <= x"78";
    wait for clk_period;

    -- end temp
    slave_data_wr <= '1';
    slave_data_in <= x"32";
    wait for clk_period;
    slave_data_wr <= '1';
    slave_data_in <= x"43";
    wait for clk_period;
    slave_data_wr <= '1';
    slave_data_in <= x"54";
    wait for clk_period;
    slave_data_wr <= '1';
    slave_data_in <= x"65";
    wait for clk_period;
    slave_data_wr <= '0';
    wait for 10 us;
    data_wr       <= '1';
    data_in       <= x"12";
    wait for clk_period;
    data_wr       <= '1';
    data_in       <= x"56";
    wait for clk_period;
    data_wr       <= '1';
    data_in       <= x"9a";
    wait for clk_period;
    data_wr       <= '1';
    data_in       <= x"de";
    wait for clk_period;
    data_wr       <= '1';
    data_in       <= x"55";
    wait for clk_period;
    data_wr       <= '1';
    data_in       <= x"33";
    wait for clk_period;
    -- end
    data_wr       <= '0';
    wait for 400 us;
    data_wr       <= '1';
    wait for 1*clk_period;
    data_wr       <= '0';
    wait;
  end process data_wr_p;

  rx_fifo_rd_p : process is
  begin  -- process rx_fifo_rd_p
    data_rd       <= '0';
    slave_data_rd <= '0';
    wait for 40 us;
    if data_to_read = '1' then
      data_rd       <= '1';
      slave_data_rd <= '1';
      wait for clk_period;
      data_rd       <= '0';
      slave_data_rd <= '0';
      
    end if;
  end process rx_fifo_rd_p;
end architecture beh;
