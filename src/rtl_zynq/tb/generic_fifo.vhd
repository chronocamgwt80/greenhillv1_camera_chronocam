----------------------------------------------------------------------------------
-- Company: Asygn
-- 
-- Create Date: 05/14/2014 04:01:59 PM
-- Design Name: dpodL
-- Module Name: generic_fifo
-- Description: generic fifo
-- 
-- Revision:
-- Revision 1 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief Generic FIFO
--! @details
--! standard dual port FIFO, data size and  mem_size as generic parameters
--------------------------------------------------------------------------------

entity generic_fifo is
	
	generic (
		DATA_NBBIT		 : integer := 16;  --! data size
		MEM_SIZE_NBBIT : integer := 4  --! memory size = 2^MEM_SIZE_NBBIT
		);
	port (
		rstb			 : in	 std_logic;	--! async reset (active low)
		clk_wr		 : in	 std_logic;	--! write clock
		wr				 : in	 std_logic;  --! write
		data_wr		 : in	 std_logic_vector(DATA_NBBIT-1 downto 0);  --! data write
		clk_rd		 : in	 std_logic;  --! read clock
		rd				 : in	 std_logic; --! write
		data_rd		 : out std_logic_vector(DATA_NBBIT-1 downto 0);  --! data read
		empty_flag : out std_logic; --! empty flag
		full_flag	 : out std_logic --! full flag
		);
end entity generic_fifo;

architecture rtl of generic_fifo is
	type mem_array is array (0 to 2**MEM_SIZE_NBBIT-1) of std_logic_vector(DATA_NBBIT-1 downto 0);
	signal mem				: mem_array;
	signal wrpt, rdpt : unsigned(MEM_SIZE_NBBIT-1 downto 0) := to_unsigned (0, MEM_SIZE_NBBIT);
	signal ef, ff			: std_logic														:= '0';
begin	 -- architecture rtl

	-- purpose: fifo memory
	-- type		: sequential
	-- inputs : clk, rstb
	-- outputs: 
	process (clk_wr, rstb) is
	begin	 -- process
		if rstb = '0' then												-- asynchronous reset (active low)
		elsif clk_wr'event and clk_wr = '1' then	-- rising clock edge
			-- write process
			if wr = '1' and ff = '0' then
				mem(to_integer(wrpt)) <= data_wr;
			end if;
			data_rd <= mem(to_integer(rdpt));
		end if;
	end process;
	-- purpose: fifo write control
	-- type		: sequential
	-- inputs : clk_wr, rstb
	-- outputs: 
	wr_ctrl_p : process (clk_wr, rstb) is
	begin	 -- process wr_ctrl_p
		if rstb = '0' then												-- asynchronous reset (active low)
			wrpt <= (others => '0');
			ef	 <= '1';
		elsif clk_wr'event and clk_wr = '1' then	-- rising clock edge
			-- write pointer operation
			if wr = '1' then
				if ff = '0' then											-- first data in the fifo
					ef	 <= '0';
					wrpt <= wrpt + to_unsigned(1, 2);
				else
					ef	 <= '0';
					wrpt <= wrpt;
				end if;
			else
				wrpt <= wrpt;
				if rdpt = wrpt and ff = '0' then
					ef <= '1';
				else
					ef <= '0';
				end if;
			end if;
		end if;
	end process wr_ctrl_p;
	-- purpose: fifo read control
	-- type		: sequential
	-- inputs : clk_wr, rstb
	-- outputs: 
	rd_ctrl_p : process (clk_rd, rstb) is
	begin	 -- process wr_ctrl_p
		if rstb = '0' then												-- asynchronous reset (active low)
			rdpt <= (others => '0');
			ff	 <= '0';
		elsif clk_rd'event and clk_rd = '1' then	-- rising clock edge
			-- full flag control
			if ef = '0' then
				if rdpt = wrpt then
					ff <= '1';
				else
					ff <= '0';
				end if;
			else
				ff <= ff;
			end if;
			-- read pointer operation
			if rd = '1' then
				if ef = '1' then											-- nothing to read
					rdpt <= rdpt;
				elsif ef = '0' then										-- 
					rdpt <= rdpt + to_unsigned(1, 2);
				else
				end if;
			end if;
		end if;
	end process rd_ctrl_p;
	-- to output
	empty_flag <= ef;
	full_flag	 <= ff;
end architecture rtl;
