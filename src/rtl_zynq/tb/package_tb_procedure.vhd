library std;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;
use std.env.all;

package package_tb_procedure is

  --! Write a first word fall through fifo input port:
  procedure write_fifo(
    signal CLK          : in  std_logic;
    DATA                :     std_logic_vector;
    signal FIFO_IN_DATA : out std_logic_vector;
    signal FIFO_IN_WR   : out std_logic
    );

  --! Convert a string vector to a std logic vector:
  procedure str_2_vector(
    str : in  string(1 to 4);
    lv  : out std_logic_vector(3 downto 0)
    );

  --! Convert a string character (hexadecimal value 0 to F)  to std_logic_vector:
  procedure char_2_vector(
    str : in  string(1 to 1);
    lv  : out std_logic_vector(3 downto 0)
    );

  --! Convert a std _logic vector to a string character (hexadecimal value 0 to F):
  procedure vector_2_char(
    lv  : in  std_logic_vector(3 downto 0);
    str : out string(1 to 1)
    );

  --! Write a command to  AXI bus using axi_lite_master.v module:
  procedure AXI_CMD_WR
    (signal CLK             : in  std_logic;
     constant ADDR          : in  std_logic_vector;
     constant DATA          : in  std_logic_vector;
     signal cmd_data_in_rdy : out std_logic;
     signal cmd_mode        : out std_logic;
     signal cmd_addr        : out std_logic_vector;
     signal cmd_data_in     : out std_logic_vector);
  
  --! Read a command from the AXI bus using axi_lite_master.v module:
  procedure AXI_CMD_RD
    (signal CLK              : in  std_logic;
     constant ADDR           : in  std_logic_vector;
     signal DATA             : out std_logic_vector;
     signal cmd_data_in_rdy  : out std_logic;
     signal cmd_mode         : out std_logic;
     signal cmd_addr         : out std_logic_vector;
     signal cmd_data_out     : in  std_logic_vector;
     signal cmd_data_out_rdy : in  std_logic);


end package_tb_procedure;

package body package_tb_procedure is

  procedure write_fifo(
    signal CLK          : in  std_logic;
    DATA                :     std_logic_vector;
    signal FIFO_IN_DATA : out std_logic_vector;
    signal FIFO_IN_WR   : out std_logic
    ) is
  begin
    wait until rising_edge(CLK);
    wait for 1 ns;
    FIFO_IN_DATA <= DATA;
    FIFO_IN_WR   <= '1';
    wait until rising_edge(CLK);
    wait for 1 ns;
    FIFO_IN_WR   <= '0';
  end write_fifo;

  procedure str_2_vector(
    str : in  string(1 to 4);
    lv  : out std_logic_vector(3 downto 0)
    ) is 
  begin
    case str is
      when "0000" => lv := "0000";
      when "0001" => lv := "0001";
      when "0010" => lv := "0010";
      when "0011" => lv := "0011";
      when "0100" => lv := "0100";
      when "0101" => lv := "0101";
      when "0110" => lv := "0110";
      when "0111" => lv := "0111";
      when "1000" => lv := "1000";
      when "1001" => lv := "1001";
      when "1010" => lv := "1010";
      when "1011" => lv := "1011";
      when "1100" => lv := "1100";
      when "1101" => lv := "1101";
      when "1110" => lv := "1110";
      when "1111" => lv := "1111";
      when others => lv := "0000";
    end case;
  end str_2_vector;

  procedure char_2_vector(
    str : in  string(1 to 1);
    lv  : out std_logic_vector(3 downto 0)
    ) is 
  begin
    case str is
      when "0"    => lv := "0000";
      when "1"    => lv := "0001";
      when "2"    => lv := "0010";
      when "3"    => lv := "0011";
      when "4"    => lv := "0100";
      when "5"    => lv := "0101";
      when "6"    => lv := "0110";
      when "7"    => lv := "0111";
      when "8"    => lv := "1000";
      when "9"    => lv := "1001";
      when "A"    => lv := "1010";
      when "B"    => lv := "1011";
      when "C"    => lv := "1100";
      when "D"    => lv := "1101";
      when "E"    => lv := "1110";
      when "F"    => lv := "1111";
      when others => lv := "0000";
    end case;
  end char_2_vector;

  procedure vector_2_char(
    lv  : in  std_logic_vector(3 downto 0);
    str : out string(1 to 1)
    ) is 
  begin
    case lv is
      when "0000" => str := "0";
      when "0001" => str := "1";
      when "0010" => str := "2";
      when "0011" => str := "3";
      when "0100" => str := "4";
      when "0101" => str := "5";
      when "0110" => str := "6";
      when "0111" => str := "7";
      when "1000" => str := "8";
      when "1001" => str := "9";
      when "1010" => str := "A";
      when "1011" => str := "B";
      when "1100" => str := "C";
      when "1101" => str := "D";
      when "1110" => str := "E";
      when "1111" => str := "F";
      when others => str := "0";
    end case;
  end vector_2_char;

  procedure AXI_CMD_WR
    (
      signal CLK             : in  std_logic;
      constant ADDR          : in  std_logic_vector;
      constant DATA          : in  std_logic_vector;
      signal cmd_data_in_rdy : out std_logic;
      signal cmd_mode        : out std_logic;
      signal cmd_addr        : out std_logic_vector;
      signal cmd_data_in     : out std_logic_vector) is  
  begin
    wait until rising_edge(CLK);
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= '0';
    cmd_addr        <= ADDR;
    cmd_data_in     <= DATA;
    wait until rising_edge(CLK);
    wait for 1 ns;
    cmd_data_in_rdy <= '0';
  end AXI_CMD_WR;

  procedure AXI_CMD_RD
    (
      signal CLK              : in  std_logic;
      constant ADDR           : in  std_logic_vector;
      signal DATA             : out std_logic_vector;
      signal cmd_data_in_rdy  : out std_logic;
      signal cmd_mode         : out std_logic;
      signal cmd_addr         : out std_logic_vector;
      signal cmd_data_out     : in  std_logic_vector;
      signal cmd_data_out_rdy : in  std_logic) is
  begin
    wait until rising_edge(CLK);
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= '1';
    cmd_addr        <= ADDR;
    wait until rising_edge(CLK);
    wait for 1 ns;
    cmd_data_in_rdy <= '0';
    wait until cmd_data_out_rdy = '1';
    wait for 1 ns;
    DATA            <= cmd_data_out;
  end AXI_CMD_RD;
  
end package_tb_procedure;
