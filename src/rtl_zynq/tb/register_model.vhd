
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;

entity register_model is
  port(
    clk              : in  std_logic;
    resetb           : in  std_logic;
    axi_reg_out      : out register_bus_out;
    axi_reg_in       : in  register_bus_in;
    cmd_mode         : in  std_logic;
    cmd_addr         : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    cmd_data_in      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    cmd_data_in_rdy  : in  std_logic;
    cmd_data_out     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    cmd_data_out_rdy : out std_logic);
end register_model;

architecture rtl of register_model is

  signal AXI_ACLK     : std_logic;
  signal AXI_ARESETN  : std_logic;
  signal AXI_AWADDR   : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_AWPROT   : std_logic_vector(2 downto 0);
  signal AXI_AWVALID  : std_logic;
  signal AXI_AWREADY  : std_logic;
  signal AXI_WDATA    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_WSTRB    : std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
  signal AXI_WVALID   : std_logic;
  signal AXI_WREADY   : std_logic;
  signal AXI_BRESP    : std_logic_vector(1 downto 0);
  signal AXI_BVALID   : std_logic;
  signal AXI_BREADY   : std_logic;
  signal AXI_ARADDR   : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_ARPROT   : std_logic_vector(2 downto 0);
  signal AXI_ARVALID  : std_logic;
  signal AXI_ARREADY  : std_logic;
  signal AXI_RDATA    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_RRESP    : std_logic_vector(1 downto 0);
  signal AXI_RVALID   : std_logic;
  signal AXI_RREADY   : std_logic;

  component axi_lite_master is
    generic(
      C_M_AXI_DATA_WIDTH : integer;
      C_M_AXI_ADDR_WIDTH : integer);
    port(
      M_AXI_ACLK       : in  std_logic;
      M_AXI_ARESETN    : in  std_logic;
      M_AXI_AWADDR     : out std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_AWPROT     : out std_logic_vector(3-1 downto 0);
      M_AXI_AWVALID    : out std_logic;
      M_AXI_AWREADY    : in  std_logic;
      M_AXI_WDATA      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_WSTRB      : out std_logic_vector(C_S_AXI_DATA_WIDTH/8-1 downto 0);
      M_AXI_WVALID     : out std_logic;
      M_AXI_WREADY     : in  std_logic;
      M_AXI_BRESP      : in  std_logic_vector(2-1 downto 0);
      M_AXI_BVALID     : in  std_logic;
      M_AXI_BREADY     : out std_logic;
      M_AXI_ARADDR     : out std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_ARPROT     : out std_logic_vector(3-1 downto 0);
      M_AXI_ARVALID    : out std_logic;
      M_AXI_ARREADY    : in  std_logic;
      M_AXI_RDATA      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_RRESP      : in  std_logic_vector(2-1 downto 0);
      M_AXI_RVALID     : in  std_logic;
      M_AXI_RREADY     : out std_logic;
      -- asygn protocol
      cmd_mode         : in  std_logic;
      cmd_addr         : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      cmd_data_in      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      cmd_data_in_rdy  : in  std_logic;
      cmd_data_out     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      cmd_data_out_rdy : out std_logic);
  end component;

begin


  AXI_ACLK    <= clk;
  AXI_ARESETN <= resetb;

  reg_bank : entity work.axi_register_interface
    port map(
      S_AXI_ACLK    => AXI_ACLK,
      S_AXI_ARESETN => AXI_ARESETN,
      S_AXI_AWADDR  => AXI_AWADDR,
      S_AXI_AWPROT  => AXI_AWPROT,
      S_AXI_AWVALID => AXI_AWVALID,
      S_AXI_AWREADY => AXI_AWREADY,
      S_AXI_WDATA   => AXI_WDATA,
      S_AXI_WSTRB   => AXI_WSTRB,
      S_AXI_WVALID  => AXI_WVALID,
      S_AXI_WREADY  => AXI_WREADY,
      S_AXI_BRESP   => AXI_BRESP,
      S_AXI_BVALID  => AXI_BVALID,
      S_AXI_BREADY  => AXI_BREADY,
      S_AXI_ARADDR  => AXI_ARADDR,
      S_AXI_ARPROT  => AXI_ARPROT,
      S_AXI_ARVALID => AXI_ARVALID,
      S_AXI_ARREADY => AXI_ARREADY,
      S_AXI_RDATA   => AXI_RDATA,
      S_AXI_RRESP   => AXI_RRESP,
      S_AXI_RVALID  => AXI_RVALID,
      S_AXI_RREADY  => AXI_RREADY,
      axi_reg_out   => axi_reg_out,
      axi_reg_in    => axi_reg_in);

  axi_master_inst : axi_lite_master
    generic map(
      C_M_AXI_ADDR_WIDTH => C_S_AXI_DATA_WIDTH,
      C_M_AXI_DATA_WIDTH => C_S_AXI_ADDR_WIDTH)
    port map(
      M_AXI_ACLK       => AXI_ACLK,
      M_AXI_ARESETN    => AXI_ARESETN,
      M_AXI_AWADDR     => AXI_AWADDR,
      M_AXI_AWPROT     => AXI_AWPROT,
      M_AXI_AWVALID    => AXI_AWVALID,
      M_AXI_AWREADY    => AXI_AWREADY,
      M_AXI_WDATA      => AXI_WDATA,
      M_AXI_WSTRB      => AXI_WSTRB,
      M_AXI_WVALID     => AXI_WVALID,
      M_AXI_WREADY     => AXI_WREADY,
      M_AXI_BRESP      => AXI_BRESP,
      M_AXI_BVALID     => AXI_BVALID,
      M_AXI_BREADY     => AXI_BREADY,
      M_AXI_ARADDR     => AXI_ARADDR,
      M_AXI_ARPROT     => AXI_ARPROT,
      M_AXI_ARVALID    => AXI_ARVALID,
      M_AXI_ARREADY    => AXI_ARREADY,
      M_AXI_RDATA      => AXI_RDATA,
      M_AXI_RRESP      => AXI_RRESP,
      M_AXI_RVALID     => AXI_RVALID,
      M_AXI_RREADY     => AXI_RREADY,
      cmd_mode         => cmd_mode,
      cmd_addr         => cmd_addr,
      cmd_data_in      => cmd_data_in,
      cmd_data_in_rdy  => cmd_data_in_rdy,
      cmd_data_out     => cmd_data_out,
      cmd_data_out_rdy => cmd_data_out_rdy);

end rtl;

