
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;

entity tb_bram_ctrl is
end entity;

architecture tb of tb_bram_ctrl is

  -- axi 
  constant C_AXI_DATA_WIDTH       : integer   := 32;
  constant C_AXI_ADDR_WIDTH       : integer   := 32;
  signal AXI_ACLK                 : std_logic;
  signal AXI_ARESETN              : std_logic;
  signal AXI_AWADDR               : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_AWPROT               : std_logic_vector(2 downto 0);
  signal AXI_AWVALID              : std_logic;
  signal AXI_AWREADY              : std_logic;
  signal AXI_WDATA                : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_WSTRB                : std_logic_vector((C_AXI_DATA_WIDTH/8)-1 downto 0);
  signal AXI_WVALID               : std_logic;
  signal AXI_WREADY               : std_logic;
  signal AXI_BRESP                : std_logic_vector(1 downto 0);
  signal AXI_BVALID               : std_logic;
  signal AXI_BREADY               : std_logic;
  signal AXI_ARADDR               : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_ARPROT               : std_logic_vector(2 downto 0);
  signal AXI_ARVALID              : std_logic;
  signal AXI_ARREADY              : std_logic;
  signal AXI_RDATA                : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_RRESP                : std_logic_vector(1 downto 0);
  signal AXI_RVALID               : std_logic;
  signal AXI_RREADY               : std_logic;
  signal DONE_SUCCESS             : std_logic;
  signal axi_reg_out              : register_bus_out;
  signal axi_reg_in               : register_bus_in;
  signal cmd_mode                 : std_logic;
  signal cmd_addr                 : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal cmd_data_in              : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal cmd_data_in_rdy          : std_logic;
  constant READ_EN                : std_logic := '1';
  constant WRITE_EN               : std_logic := '0';
  -- bram     
  constant CLOCK_200_PERIOD       : time      := 5 ns;
  constant CLOCK_64_PERIOD        : time      := 7.8125 ns;
  signal clk_64_delay_en          : std_logic := '0';
  signal clk_200                  : std_logic;
  signal clk_64                   : std_logic;
  signal resetb                   : std_logic;
  signal BRAM_CLK                 : std_logic;
  signal BRAM_Addr                : std_logic_vector(31 downto 0);
  signal BRAM_WrData              : std_logic_vector(31 downto 0);
  signal BRAM_WE                  : std_logic_vector(6 downto 0);
  signal BRAM_En                  : std_logic;
  signal BRAM_rst                 : std_logic;
  signal bram_irq                 : std_logic;
  signal bram_irq_clear           : std_logic;
  signal ctrl_cdma_done           : std_logic := '0';
  signal ctrl_bram_fill_threshold : std_logic_vector(21 downto 0);

  signal atmel_read_data     : std_logic_vector(31 downto 0) := (others => '0');
  signal atmel_read_data_rdy : std_logic                     := '0';
  signal half_bram_cnt       : unsigned(8 downto 0);
  signal half_bram_requested : unsigned(8 downto 0);
  signal enable_flag         : std_logic;


  component axi_lite_master is
    generic(
      C_M_AXI_DATA_WIDTH : integer;
      C_M_AXI_ADDR_WIDTH : integer);
    port(
      M_AXI_ACLK      : in  std_logic;
      M_AXI_ARESETN   : in  std_logic;
      M_AXI_AWADDR    : out std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_AWPROT    : out std_logic_vector(3-1 downto 0);
      M_AXI_AWVALID   : out std_logic;
      M_AXI_AWREADY   : in  std_logic;
      M_AXI_WDATA     : out std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_WSTRB     : out std_logic_vector(C_AXI_DATA_WIDTH/8-1 downto 0);
      M_AXI_WVALID    : out std_logic;
      M_AXI_WREADY    : in  std_logic;
      M_AXI_BRESP     : in  std_logic_vector(2-1 downto 0);
      M_AXI_BVALID    : in  std_logic;
      M_AXI_BREADY    : out std_logic;
      M_AXI_ARADDR    : out std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_ARPROT    : out std_logic_vector(3-1 downto 0);
      M_AXI_ARVALID   : out std_logic;
      M_AXI_ARREADY   : in  std_logic;
      M_AXI_RDATA     : in  std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_RRESP     : in  std_logic_vector(2-1 downto 0);
      M_AXI_RVALID    : in  std_logic;
      M_AXI_RREADY    : out std_logic;
      DONE_SUCCESS    : out std_logic;
      -- asygn protocol
      cmd_mode        : in  std_logic;
      cmd_addr        : in  std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
      cmd_data_in     : in  std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
      cmd_data_in_rdy : in  std_logic);
  end component;


begin

  
  reg_bank : entity work.axi_register_interface
    port map(
      S_AXI_ACLK    => AXI_ACLK,
      S_AXI_ARESETN => AXI_ARESETN,
      S_AXI_AWADDR  => AXI_AWADDR,
      S_AXI_AWPROT  => AXI_AWPROT,
      S_AXI_AWVALID => AXI_AWVALID,
      S_AXI_AWREADY => AXI_AWREADY,
      S_AXI_WDATA   => AXI_WDATA,
      S_AXI_WSTRB   => AXI_WSTRB,
      S_AXI_WVALID  => AXI_WVALID,
      S_AXI_WREADY  => AXI_WREADY,
      S_AXI_BRESP   => AXI_BRESP,
      S_AXI_BVALID  => AXI_BVALID,
      S_AXI_BREADY  => AXI_BREADY,
      S_AXI_ARADDR  => AXI_ARADDR,
      S_AXI_ARPROT  => AXI_ARPROT,
      S_AXI_ARVALID => AXI_ARVALID,
      S_AXI_ARREADY => AXI_ARREADY,
      S_AXI_RDATA   => AXI_RDATA,
      S_AXI_RRESP   => AXI_RRESP,
      S_AXI_RVALID  => AXI_RVALID,
      S_AXI_RREADY  => AXI_RREADY,
      DONE_SUCCESS  => DONE_SUCCESS,
      axi_reg_out   => axi_reg_out,
      axi_reg_in    => axi_reg_in);

  axi_master_inst : axi_lite_master
    generic map(
      C_M_AXI_ADDR_WIDTH => C_AXI_DATA_WIDTH,
      C_M_AXI_DATA_WIDTH => C_AXI_ADDR_WIDTH)
    port map(
      M_AXI_ACLK      => AXI_ACLK,
      M_AXI_ARESETN   => AXI_ARESETN,
      M_AXI_AWADDR    => AXI_AWADDR,
      M_AXI_AWPROT    => AXI_AWPROT,
      M_AXI_AWVALID   => AXI_AWVALID,
      M_AXI_AWREADY   => AXI_AWREADY,
      M_AXI_WDATA     => AXI_WDATA,
      M_AXI_WSTRB     => AXI_WSTRB,
      M_AXI_WVALID    => AXI_WVALID,
      M_AXI_WREADY    => AXI_WREADY,
      M_AXI_BRESP     => AXI_BRESP,
      M_AXI_BVALID    => AXI_BVALID,
      M_AXI_BREADY    => AXI_BREADY,
      M_AXI_ARADDR    => AXI_ARADDR,
      M_AXI_ARPROT    => AXI_ARPROT,
      M_AXI_ARVALID   => AXI_ARVALID,
      M_AXI_ARREADY   => AXI_ARREADY,
      M_AXI_RDATA     => AXI_RDATA,
      M_AXI_RRESP     => AXI_RRESP,
      M_AXI_RVALID    => AXI_RVALID,
      M_AXI_RREADY    => AXI_RREADY,
      cmd_mode        => cmd_mode,
      cmd_addr        => cmd_addr,
      cmd_data_in     => cmd_data_in,
      cmd_data_in_rdy => cmd_data_in_rdy);

  dut : entity work.bram_custom_ctrl
    port map(
      clk_200                  => clk_200,
      clk_64                   => clk_64,
      resetb                   => axi_reg_out.bram_resetb,
      ctrl_acquisition_en      => axi_reg_out.bram_acquisition_en,
      ctrl_overflow_flag_clear => axi_reg_out.bram_overflow_flag_clear,
      ctrl_overflow_flag       => axi_reg_in.bram_overflow_flag,
      ctrl_irq_clear           => bram_irq_clear,
      ctrl_irq                 => bram_irq,
      ctrl_polarity            => axi_reg_in.bram_polarity,
      ctrl_packet_timestamp    => axi_reg_in.bram_packet_timestamp,
      ctrl_cdma_done           => ctrl_cdma_done,
      ctrl_bram_fill_threshold => axi_reg_out.bram_fill_threshold,
      atmel_read_data          => atmel_read_data,
      atmel_read_data_rdy      => atmel_read_data_rdy,
      emulator_en              => axi_reg_out.emulator_en,
      BRAM_CLK                 => BRAM_CLK,
      BRAM_Addr                => BRAM_Addr,
      BRAM_WrData              => BRAM_WrData,
      BRAM_WE                  => BRAM_WE,
      BRAM_rst                 => BRAM_rst,
      BRAM_En                  => BRAM_En);


  axi_reg_in.FIRMWARE_version         <= (others => '0');
  axi_reg_in.rf_tx_fifo_empty_flag    <= '0';
  axi_reg_in.rf_tx_fifo_full_flag     <= '0';
  axi_reg_in.rf_tx_buffer_empty_flag  <= '0';
  axi_reg_in.rf_tx_buffer_full_flag   <= '0';
  axi_reg_in.config_spi_rx_full_flag  <= '0';
  axi_reg_in.config_spi_rx_empty_flag <= '0';
  axi_reg_in.config_spi_rx_thres_flag <= '0';
  axi_reg_in.config_spi_rx_data_count <= (others => '0');
  axi_reg_in.config_spi_tx_full_flag  <= '0';
  axi_reg_in.config_spi_tx_empty_flag <= '0';
  axi_reg_in.config_spi_tx_thres_flag <= '0';
  axi_reg_in.config_spi_tx_data_count <= (others => '0');
  axi_reg_in.config_spi_data_rd       <= (others => '0');



  clk_64_p : process
  begin  -- process clk_p
    clk_64 <= '0';
    if clk_64_delay_en = '0' then
      clk_64_delay_en <= '1';
      wait for 10 us;
    end if;
    clk_64 <= '1';
    wait for CLOCK_64_PERIOD/2;
    clk_64 <= '0';
    wait for CLOCK_64_PERIOD/2;
  end process clk_64_p;

  clk_200_p : process
  begin  -- process clk_p
    clk_200 <= '1';
    wait for CLOCK_200_PERIOD/2;
    clk_200 <= '0';
    wait for CLOCK_200_PERIOD/2;
  end process clk_200_p;

  resetb      <= '0', '1' after 10*CLOCK_200_PERIOD;
  AXI_ACLK    <= clk_200;
  AXI_ARESETN <= resetb;






  cmd_p : process is
  begin  -- process cmd_p
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= (others => '0');
    cmd_data_in     <= (others => '0');
    cmd_data_in_rdy <= '0';




    wait for 2 us;
    -- release reset bram ctrl
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000001";     -- release reset
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';


    -- /// send new config /// --
    -- set configuration 
    ctrl_bram_fill_threshold <= x"00800" & "01";  -- half BRAM + 1 sample


    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_1_REG_ADDR;
    cmd_data_in     <= "00000"& '1' & ctrl_bram_fill_threshold & x"0";  -- set adas number
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    -- start bram ctrl
    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000003";     -- start counter
    enable_flag     <= '1';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait until half_bram_cnt = half_bram_requested;

    -- stop acq
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000001";     -- release reset
    enable_flag     <= '0';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait for 2 ms;


    -- /// send new config /// --
    -- set configuration 
    ctrl_bram_fill_threshold <= x"00800" & "01";  -- half BRAM + 1 sample


    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_1_REG_ADDR;
    cmd_data_in     <= "00000"& '1' & ctrl_bram_fill_threshold & x"0";  -- set adas number
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    -- start bram ctrl
    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000003";     -- start counter
    enable_flag     <= '1';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait until half_bram_cnt = half_bram_requested;

    -- stop acq
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000001";     -- release reset
    enable_flag     <= '0';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait for 2 ms;


    -- /// send new config /// --
    -- set configuration 
    ctrl_bram_fill_threshold <= x"007FF" & "11";  -- half BRAM - 1 sample


    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_1_REG_ADDR;
    cmd_data_in     <= "00000"& '1' & ctrl_bram_fill_threshold & x"0";  -- set adas number
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    -- start bram ctrl
    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000003";     -- start counter
    enable_flag     <= '1';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait until half_bram_cnt = half_bram_requested;

    -- stop acq
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000001";     -- release reset
    enable_flag     <= '0';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait for 2 ms;




    -- /// send new config /// --
    -- set configuration 
    ctrl_bram_fill_threshold <= x"00800" & "00";  -- half BRAM


    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_1_REG_ADDR;
    cmd_data_in     <= "00000"& '1' & ctrl_bram_fill_threshold & x"0";  -- set adas number
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    -- start bram ctrl
    wait for 40 ns;
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000003";     -- start counter
    enable_flag     <= '1';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait until half_bram_cnt = half_bram_requested;

    -- stop acq
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '1';
    cmd_mode        <= WRITE_EN;
    cmd_addr        <= x"0000" & BRAM_CTRL_REG_0_REG_ADDR;
    cmd_data_in     <= x"00000001";     -- release reset
    enable_flag     <= '0';
    wait until clk_200'event and clk_200 = '1';
    wait for 1 ns;
    cmd_data_in_rdy <= '0';

    wait for 20 ms;









    wait;

    
  end process cmd_p;




  half_bram_requested <= unsigned(ctrl_bram_fill_threshold(21 downto 13)) when ctrl_bram_fill_threshold(12 downto 0) = "0000000000000" else
                         unsigned(ctrl_bram_fill_threshold(21 downto 13)) + 1;




  
  irq_clear_p : process(clk_200, resetb)
  begin
    if clk_200'event and clk_200 = '1' then
      if resetb = '0' then
        bram_irq_clear <= '0';
      else
        if bram_irq = '1' and bram_irq_clear = '0' then
          bram_irq_clear <= '1';
        else
          bram_irq_clear <= '0';
        end if;
      end if;
    end if;
  end process;

  irq_cnt_p : process(bram_irq, resetb, enable_flag)
  begin
    if resetb = '0' or enable_flag = '0'then
      half_bram_cnt <= (others => '0');
    elsif bram_irq'event and bram_irq = '1' then
      half_bram_cnt <= half_bram_cnt+1;
    end if;
  end process;




  overflow_clear_p : process(bram_irq_clear)
  begin
    ctrl_cdma_done <= transport bram_irq_clear after 300 us;
  end process;





end tb;
