

LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use IEEE.std_logic_textio.all;
use STD.textio.all;

--use work.txt_util.all;


entity bram2file is
  port(
    BRAM_CLK    : in std_logic;
    BRAM_Addr   : in std_logic_vector(31 downto 0);
    BRAM_WrData : in std_logic_vector(63 downto 0);
    BRAM_WE     : in std_logic_vector(7 downto 0);
    BRAM_rst    : in std_logic;
    BRAM_En     : in std_logic
    );
end bram2file;





architecture behav of bram2file is
  file wf : TEXT open write_mode is "/tmp/bram";
  signal d0  : std_logic_vector(15 downto 0);
  signal d1  : std_logic_vector(15 downto 0);
  signal d2  : std_logic_vector(15 downto 0);
  signal d3  : std_logic_vector(15 downto 0);
begin
  d0 <= BRAM_WrData(15 downto 0);
  d1 <= BRAM_WrData(31 downto 16);
  d2 <= BRAM_WrData(47 downto 32);
  d3 <= BRAM_WrData(63 downto 48);
  
  BRAM2file_p : process (BRAM_CLK, BRAM_rst) is
    variable l : line;
  begin  -- process BRAM2file_p
    if BRAM_rst = '1' then    -- asynchronous reset (active low)

    elsif BRAM_CLK'event and BRAM_CLK = '1' then  -- rising clock edge
      if BRAM_En = '1' then
  --      write(l,"0x" & hstr(BRAM_Addr) &", 0x" & hstr(BRAM_WrData));
        write(l,conv_integer(BRAM_Addr));
        writeline(wf, l);
        write(l,conv_integer(d0));
        writeline(wf, l);
        write(l,conv_integer(d1));
        writeline(wf, l);
        write(l,conv_integer(d2));
        writeline(wf, l);
        write(l,conv_integer(d3));
        writeline(wf, l);
      end if;
    end if;
  end process BRAM2file_p;
end behav;
