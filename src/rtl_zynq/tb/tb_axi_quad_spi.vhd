--! by stephane breysse, 05/2016
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;
use work.package_tb_procedure.all;

entity tb_axi_quad_spi is
end entity;

architecture tb of tb_axi_quad_spi is

  constant C_AXI_ADDR_WIDTH       : integer                                       := 32;
  constant C_AXI_DATA_WIDTH       : integer                                       := 32;
  constant ADDR_AXI_EXT_SPI_SRR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10040";
  constant ADDR_AXI_EXT_SPI_CR    : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10060";
  constant ADDR_AXI_EXT_SPI_SR    : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10064";
  constant ADDR_AXI_EXT_SPI_SSR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10070";
  constant ADDR_AXI_EXT_SPI_DTR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10068";
  constant ADDR_AXI_EXT_SPI_ISR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10020";
  constant ADDR_AXI_EXT_SPI_DGIER : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C10028";
  constant ADDR_AXI_EXT_SPI_IER   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C1001C";

  constant ADDD_AXI_REG_EXT_SPI_CTRL : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C00034";

  constant MSB_FIRST   : std_logic := '0';
  constant LSB_FIRST   : std_logic := '1';
  constant CPHA_HIGH   : std_logic := '1';
  constant CPHA_LOW    : std_logic := '0';
  constant CPOL_HIGH   : std_logic := '1';
  constant CPOL_LOW    : std_logic := '0';
  constant MODE_MASTER : std_logic := '1';
  constant MODE_SLAVE  : std_logic := '0';
  constant SPI_ENB     : std_logic := '1';
  constant SPI_DIS     : std_logic := '0';
  constant LOOP_ENB    : std_logic := '1';
  constant LOOP_DIS    : std_logic := '0';

  signal AXI_ACLK, AXI_REG_ACLK       : std_logic;
  signal AXI_ARESETN, AXI_REG_ARESETN : std_logic;
  signal AXI_AWADDR, AXI_REG_AWADDR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_AWPROT, AXI_REG_AWPROT   : std_logic_vector(2 downto 0);
  signal AXI_AWVALID, AXI_REG_AWVALID : std_logic;
  signal AXI_AWREADY, AXI_REG_AWREADY : std_logic;
  signal AXI_WDATA, AXI_REG_WDATA     : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_WSTRB, AXI_REG_WSTRB     : std_logic_vector((C_AXI_DATA_WIDTH/8)-1 downto 0);
  signal AXI_WVALID, AXI_REG_WVALID   : std_logic;
  signal AXI_WREADY, AXI_REG_WREADY   : std_logic;
  signal AXI_BRESP, AXI_REG_BRESP     : std_logic_vector(1 downto 0);
  signal AXI_BVALID, AXI_REG_BVALID   : std_logic;
  signal AXI_BREADY, AXI_REG_BREADY   : std_logic;
  signal AXI_ARADDR, AXI_REG_ARADDR   : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_ARPROT, AXI_REG_ARPROT   : std_logic_vector(2 downto 0);
  signal AXI_ARVALID, AXI_REG_ARVALID : std_logic;
  signal AXI_ARREADY, AXI_REG_ARREADY : std_logic;
  signal AXI_RDATA, AXI_REG_RDATA     : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_RRESP, AXI_REG_RRESP     : std_logic_vector(1 downto 0);
  signal AXI_RVALID, AXI_REG_RVALID   : std_logic;
  signal AXI_RREADY, AXI_REG_RREADY   : std_logic;

  signal io0_i        : std_logic                    := '0';
  signal io0_o        : std_logic;
  signal io0_t        : std_logic;
  signal io1_i        : std_logic                    := '0';
  signal io1_o        : std_logic;
  signal io1_t        : std_logic;
  signal sck_i        : std_logic                    := '0';
  signal sck_o        : std_logic;
  signal sck_t        : std_logic;
  signal ss_i         : std_logic_vector(0 downto 0) := "0";
  signal ss_o         : std_logic_vector(0 downto 0);
  signal ss_t         : std_logic;
  signal ip2intc_irpt : std_logic;

  signal clk : std_logic := '0';
  signal clk_200            : std_logic;
  constant CLOCK_200_PERIOD : time := 5 ns;

  --! Asygn protocol
  signal cmd_mode, cmd_axi_reg_mode                 : std_logic := '0';
  signal cmd_addr, cmd_axi_reg_addr                 : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
  signal cmd_data_in, cmd_axi_reg_data_in           : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
  signal cmd_data_in_rdy, cmd_axi_reg_data_in_rdy   : std_logic := '0';
  signal cmd_data_out, cmd_axi_reg_data_out         : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
  signal cmd_data_out_rdy, cmd_axi_reg_data_out_rdy : std_logic := '0';

  --! Asygn axi registers records:
  signal axi_reg_out          : register_bus_out;
  signal axi_reg_in           : register_bus_in;
  
  signal debug_read : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => 'Z');
  signal debug_log  : string(1 to 5)                                := "wait ";
  
begin

  --! Master AXI protocol module: for SPI IP 
  axi_master_inst_1 : entity work.axi_lite_master
    generic map(
      C_M_AXI_ADDR_WIDTH => C_AXI_ADDR_WIDTH,
      C_M_AXI_DATA_WIDTH => C_AXI_DATA_WIDTH)
    port map(
      M_AXI_ACLK       => AXI_ACLK,
      M_AXI_ARESETN    => AXI_ARESETN,
      M_AXI_AWADDR     => AXI_AWADDR,
      M_AXI_AWPROT     => AXI_AWPROT,
      M_AXI_AWVALID    => AXI_AWVALID,
      M_AXI_AWREADY    => AXI_AWREADY,
      M_AXI_WDATA      => AXI_WDATA,
      M_AXI_WSTRB      => AXI_WSTRB,
      M_AXI_WVALID     => AXI_WVALID,
      M_AXI_WREADY     => AXI_WREADY,
      M_AXI_BRESP      => AXI_BRESP,
      M_AXI_BVALID     => AXI_BVALID,
      M_AXI_BREADY     => AXI_BREADY,
      M_AXI_ARADDR     => AXI_ARADDR,
      M_AXI_ARPROT     => AXI_ARPROT,
      M_AXI_ARVALID    => AXI_ARVALID,
      M_AXI_ARREADY    => AXI_ARREADY,
      M_AXI_RDATA      => AXI_RDATA,
      M_AXI_RRESP      => AXI_RRESP,
      M_AXI_RVALID     => AXI_RVALID,
      M_AXI_RREADY     => AXI_RREADY,
      cmd_mode         => cmd_mode,
      cmd_addr         => cmd_addr,
      cmd_data_in      => cmd_data_in,
      cmd_data_in_rdy  => cmd_data_in_rdy,
      cmd_data_out     => cmd_data_out,
      cmd_data_out_rdy => cmd_data_out_rdy);


  --! Master xilinx SPI IP: UUT 1
  axi_external_spi_1 : entity work.axi_external_spi
    port map (
      ext_spi_clk   => AXI_ACLK,
      s_axi_aclk    => AXI_ACLK,
      s_axi_aresetn => AXI_ARESETN,
      s_axi_awaddr  => AXI_AWADDR(6 downto 0),  --! 7 address bits required by SPI IP
      s_axi_awvalid => AXI_AWVALID,
      s_axi_awready => AXI_AWREADY,
      s_axi_wdata   => AXI_WDATA,
      s_axi_wstrb   => AXI_WSTRB,
      s_axi_wvalid  => AXI_WVALID,
      s_axi_wready  => AXI_WREADY,
      s_axi_bresp   => AXI_BRESP,
      s_axi_bvalid  => AXI_BVALID,
      s_axi_bready  => AXI_BREADY,
      s_axi_araddr  => AXI_ARADDR(6 downto 0),  --! 7 address bits required by SPI IP
      s_axi_arvalid => AXI_ARVALID,
      s_axi_arready => AXI_ARREADY,
      s_axi_rdata   => AXI_RDATA,
      s_axi_rresp   => AXI_RRESP,
      s_axi_rvalid  => AXI_RVALID,
      s_axi_rready  => AXI_RREADY,
      io0_i         => '0',
      io0_o         => io0_o,
      io0_t         => io0_t,
      io1_i         => io1_i,
      io1_o         => io1_o,
      io1_t         => io1_t,
      sck_i         => '0',
      sck_o         => sck_o,
      sck_t         => sck_t,
      ss_i          => "0",
      ss_o          => ss_o,
      ss_t          => ss_t,
      ip2intc_irpt  => ip2intc_irpt);

  spi_eof_detector_1: entity work.spi_eof_detector
    port map (
      clk         => clk,
      sck_o       => sck_o,
      ss_o        => ss_o(0),
      axi_reset   => axi_reg_out.ext_spi_ctrl_reset,
      axi_byte_ctrl=> axi_reg_out.ext_spi_byte_cntr,
      axi_tx_end  => axi_reg_in.ext_spi_status_tx_end);

  --! Master AXI protocol module: for axi register interface
  axi_master_inst_2 : entity work.axi_lite_master
    generic map(
      C_M_AXI_ADDR_WIDTH => C_AXI_ADDR_WIDTH,
      C_M_AXI_DATA_WIDTH => C_AXI_DATA_WIDTH)
    port map(
      M_AXI_ACLK       => AXI_REG_ACLK,
      M_AXI_ARESETN    => AXI_REG_ARESETN,
      M_AXI_AWADDR     => AXI_REG_AWADDR,
      M_AXI_AWPROT     => AXI_REG_AWPROT,
      M_AXI_AWVALID    => AXI_REG_AWVALID,
      M_AXI_AWREADY    => AXI_REG_AWREADY,
      M_AXI_WDATA      => AXI_REG_WDATA,
      M_AXI_WSTRB      => AXI_REG_WSTRB,
      M_AXI_WVALID     => AXI_REG_WVALID,
      M_AXI_WREADY     => AXI_REG_WREADY,
      M_AXI_BRESP      => AXI_REG_BRESP,
      M_AXI_BVALID     => AXI_REG_BVALID,
      M_AXI_BREADY     => AXI_REG_BREADY,
      M_AXI_ARADDR     => AXI_REG_ARADDR,
      M_AXI_ARPROT     => AXI_REG_ARPROT,
      M_AXI_ARVALID    => AXI_REG_ARVALID,
      M_AXI_ARREADY    => AXI_REG_ARREADY,
      M_AXI_RDATA      => AXI_REG_RDATA,
      M_AXI_RRESP      => AXI_REG_RRESP,
      M_AXI_RVALID     => AXI_REG_RVALID,
      M_AXI_RREADY     => AXI_REG_RREADY,
      cmd_mode         => cmd_axi_reg_mode,
      cmd_addr         => cmd_axi_reg_addr,
      cmd_data_in      => cmd_axi_reg_data_in,
      cmd_data_in_rdy  => cmd_axi_reg_data_in_rdy,
      cmd_data_out     => cmd_axi_reg_data_out,
      cmd_data_out_rdy => cmd_axi_reg_data_out_rdy);
  
  --! Register map interface
  axi_register_interface_2 : entity work.axi_register_interface
    port map (
      S_AXI_ACLK    => AXI_REG_ACLK,      --! Global Clock Signal
      S_AXI_ARESETN => AXI_REG_ARESETN,  --! Global Reset Signal. This Signal is Active LOW
      S_AXI_AWADDR  => AXI_REG_AWADDR,  --! Write address (issued by master, acceped by Slave)
      S_AXI_AWPROT  => AXI_REG_AWPROT,    --! Write channel Protection type.
      S_AXI_AWVALID => AXI_REG_AWVALID,  --! Write address valid.
      S_AXI_AWREADY => AXI_REG_AWREADY,  --! Write address ready.
      S_AXI_WDATA   => AXI_REG_WDATA,  --! Write data (issued by master, acceped by Slave)
      S_AXI_WSTRB   => AXI_REG_WSTRB,     --! Write strobes.
      S_AXI_WVALID  => AXI_REG_WVALID,   --! Write valid
      S_AXI_WREADY  => AXI_REG_WREADY,   --! Write ready.
      S_AXI_BRESP   => AXI_REG_BRESP,     --! Write response.
      S_AXI_BVALID  => AXI_REG_BVALID,   --! Write response valid.
      S_AXI_BREADY  => AXI_REG_BREADY,   --! Response ready.
      S_AXI_ARADDR  => AXI_REG_ARADDR,  --! Read address (issued by master, acceped by Slave)
      S_AXI_ARPROT  => AXI_REG_ARPROT,    --! Protection type.
      S_AXI_ARVALID => AXI_REG_ARVALID,  --! Read address valid.
      S_AXI_ARREADY => AXI_REG_ARREADY,  --! Read address ready.
      S_AXI_RDATA   => AXI_REG_RDATA,     --! Read data (issued by slave)
      S_AXI_RRESP   => AXI_REG_RRESP,     --! Read response.
      S_AXI_RVALID  => AXI_REG_RVALID,   --! Read valid.
      S_AXI_RREADY  => AXI_REG_RREADY,   --! Read ready.
      axi_reg_out   => axi_reg_out,
      axi_reg_in    => axi_reg_in);


  clk_200_p : process
  begin  -- process clk_p
    clk_200 <= '1';
    wait for CLOCK_200_PERIOD/2;
    clk_200 <= '0';
    wait for CLOCK_200_PERIOD/2;
  end process clk_200_p;

  AXI_ACLK    <= clk_200;
  AXI_REG_ACLK  <= clk_200;
  clk <= clk_200;
  AXI_ARESETN <= '0', '1' after 10*CLOCK_200_PERIOD;
  AXI_REG_ARESETN <= AXI_ARESETN;

  my_tb_1 : process is
  begin

    --! Signals init:
    cmd_mode        <= '0';
    cmd_addr        <= (others => '0');
    cmd_data_in     <= (others => '0');
    cmd_data_in_rdy <= '0';
    
    cmd_axi_reg_mode        <= '0';
    cmd_axi_reg_addr        <= (others => '0');
    cmd_axi_reg_data_in     <= (others => '0');
    cmd_axi_reg_data_in_rdy <= '0';

    wait for 100 ns;

    --! RESET SPI IP:
    -------------------------------
    debug_log <= "reset";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SRR, x"0000000A", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;

    --! Write Enable Command Sequence
    ---------------------------------
    --1. Disable the master transaction by asserting the master inhibit bit of SPICR (60h),
    --   and reset the RX and TX FIFOs through SPICR.
    --   Example: write 0x1E6 to SPICR
    --2. Issue the write enable command by writing 0x06 into SPIDTR.
    --3. Issue chip select by writing 0x00 to SPISSR(70h).
    --4. Enable master transaction by deasserting the SPICR master inhibit bit.
    --5. Deassert chip select by writing 0x01 to SPISSR.
    --6. Disable master transaction by asserting the SPICR master inhibit bit.

    --! --! WRITE ENABLE COMMAND SEQUENCE
    --! 1. Write SPI IP control register:
    -- -- Disable master transaction, reset RX and TX fifo
    -- -- MSB, CPHA = CPOL = 1
    debug_log <= "dis m";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"000001FE", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --! 2.: 
    debug_log <= "wr en";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_DTR, x"00000006", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --! 3.:
    debug_log <= "cs en";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SSR, x"00000000", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --! 4.:
    -- -- Enable master transaction by deasserting the SPICR master inhibit bit.
    wait for 200 ns;
    debug_log <= "en TR";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"000000FE", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --! 5.:
    wait for 200 ns;
    debug_log <= "discs";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SSR, x"00000001", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --! 6.:
    wait for 200 ns;
    debug_log <= "disTR";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"000001FE", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);

    wait for 500 ns;
    debug_log <= "RCTRL";
    AXI_CMD_WR(clk_200, ADDD_AXI_REG_EXT_SPI_CTRL, x"00000001", cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_in);
    wait for 200 ns;
    debug_log <= "RCTRL";
    AXI_CMD_WR(clk_200, ADDD_AXI_REG_EXT_SPI_CTRL, x"00030001", cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_in);
    wait for 200 ns;
    
    --! RESET SPI IP:
    -------------------------------
    debug_log <= "reset";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SRR, x"0000000A", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    
    --! Write Data Command Sequence
    -------------------------------
    --1. Reset RX and TX FIFOs through SPICR.
    --2. Issue the write data command into SPIDTR, to write data into any specific
    --   sector followed by the flash sector address.
    --3. Fill SPIDTR with the data to be written to flash; the maximum data size depends
    --   upon the configured QSPI FIFO size.
    --4. Issue chip select by writing 0x00 to SPISSR.
    --5. Enable master transaction by deasserting the SPICR master inhibit bit.
    --6. Deassert chip select by writing 0x01 to SPISSR.
    --7. Disable master transaction by asserting the SPICR master inhibit bit.

    --! --! WRITE DATA COMMAND SEQUENCE
    --! 1. Write SPI IP control register:
    -- -- Disable master transaction, reset RX and TX fifo
    -- -- MSB, CPHA = CPOL = 1
    debug_log <= "dis m";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"0000019E", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_DGIER, x"80000000", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --wait for 200 ns;
    --AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_IER, x"0000000C", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --wait for 200 ns;
    --! 2.: Write command byte:
    debug_log <= "wrCMD";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_DTR, x"0000005A", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --! 3.1: Write first data byte: 
    debug_log <= "wrDAT";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_DTR, x"00000096", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --! 3.2: Write second data byte: 
    debug_log <= "wrDAT";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_DTR, x"000000AA", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    wait for 200 ns;
    --! 4.: Issue chip select (0)
    debug_log <= "cs en";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SSR, x"00000000", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --! 5.1:
    -- -- Enable master transaction by deasserting the SPICR master inhibit bit.
    wait for 200 ns;
    debug_log <= "en tr";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"0000009E", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);

    --! 5.2:
    lp_wait_data_transmit : loop
      
      --AXI_CMD_RD(clk_200, ADDR_AXI_EXT_SPI_ISR, debug_read, cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_out, cmd_data_out_rdy);
      --wait until falling_edge(cmd__data_out_rdy);
      
      AXI_CMD_RD(clk_200, ADDD_AXI_REG_EXT_SPI_CTRL, debug_read, cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_out, cmd_axi_reg_data_out_rdy);
      wait until falling_edge(cmd_axi_reg_data_out_rdy);

      --exit lp_wait_data_transmit when (debug_read(2) = '1');  --! Check data transmit...
      
      exit lp_wait_data_transmit when (debug_read(8)  = '1');  --! Check data transmit...
    end loop;

    --! 6.: Deassert chip select:
    --wait for 5 us;
    debug_log <= "discs";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_SSR, x"00000001", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);
    --! 7.: Disable master transaction:
    wait for 200 ns;
    debug_log <= "disTR";
    AXI_CMD_WR(clk_200, ADDR_AXI_EXT_SPI_CR, x"000001FE", cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_in);

    wait for 500 ns;

    --! --! External SPI read register example
    --! Read SPI IP control register:
    AXI_CMD_RD(clk_200, ADDR_AXI_EXT_SPI_CR, debug_read, cmd_data_in_rdy, cmd_mode, cmd_addr, cmd_data_out, cmd_data_out_rdy);
    wait for 200 ns;

    --! end of testbench.
    wait;
    
  end process my_tb_1;

  end tb;
