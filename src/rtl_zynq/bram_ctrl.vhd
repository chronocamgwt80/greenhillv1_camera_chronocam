

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_package.all;

entity bram_ctrl is
  port(

    clk                      : in  std_logic;
    resetb                   : in  std_logic;
    -- from registers
    ctrl_acquisition_en      : in  std_logic;
    ctrl_overflow_flag_clear : in  std_logic;  -- one shot
    ctrl_overflow_flag       : out std_logic;
    ctrl_irq_clear           : in  std_logic;  -- one shot        
    ctrl_irq                 : out std_logic;
    ctrl_polarity            : out std_logic;
    ctrl_packet_timestamp    : out std_logic_vector(25 downto 0);
    ctrl_cdma_done           : in  std_logic;  -- oneshot !
    ctrl_bram_fill_threshold : in  std_logic_vector(21 downto 0);
    read_data                : in  std_logic_vector(31 downto 0);
    read_data_rdy            : in  std_logic;
    BRAM_CLK                 : out std_logic;
    BRAM_Addr                : out std_logic_vector(31 downto 0);
    BRAM_WrData              : out std_logic_vector(31 downto 0);
    BRAM_WE                  : out std_logic_vector(3 downto 0);
    BRAM_En                  : out std_logic;
    BRAM_rst                 : out std_logic);
end bram_ctrl;





architecture rtl of bram_ctrl is

  
  signal current_state          : bram_ctrl_state;
  signal next_state             : bram_ctrl_state;
  signal bram_data_out          : std_logic_vector(31 downto 0);
  signal bram_data_out_en       : std_logic;
  signal addr_cnt               : unsigned(13 downto 0);
  signal bram_cnt               : unsigned (7 downto 0);
  signal bram_en_buff           : std_logic;
  signal polarity_buff          : std_logic;
  signal polarity_buff_previous : std_logic;
  signal packet_timestamp       : unsigned(25 downto 0);
  signal irq_buff               : std_logic;
  signal lock_prev_section      : std_logic;
  signal overflow_flag_buff     : std_logic;
  signal end_of_frame           : std_logic;
  signal end_of_frame_delay     : std_logic;
  signal reverse_polarity       : std_logic;
  signal addr_first_start       : std_logic;
  
begin

  BRAM_CLK                <= clk;
  BRAM_Addr(31 downto 16) <= (others => '0');
  BRAM_Addr(15 downto 0)  <= std_logic_vector(addr_cnt)&"00";
  BRAM_WrData             <= read_data;
  BRAM_WE                 <= (others => '1');
  BRAM_rst                <= not(resetb);
  BRAM_En                 <= bram_en_buff;
  ctrl_packet_timestamp   <= std_logic_vector(packet_timestamp);
  ctrl_irq                <= irq_buff;
  ctrl_overflow_flag      <= overflow_flag_buff;
  ctrl_polarity           <= not(polarity_buff) when reverse_polarity = '1' and current_state = st_reset else
                             polarity_buff;

  atmel_read_inst : entity work.atmel_read(rtl)
    generic map(POS_EDGE_CAPTURE => false)
    port map(
      clk              => clk,
      resetb           => resetb,
      data_in          => read_data,
      data_rdy         => read_data_rdy,
      bram_data_out    => bram_data_out,
      bram_data_out_en => bram_data_out_en);


  
  bram_ctrl_fsm_p : process(resetb, ctrl_acquisition_en, current_state, lock_prev_section, bram_cnt, addr_cnt, ctrl_bram_fill_threshold)
  begin
    if resetb = '0' then
      next_state   <= st_reset;
      end_of_frame <= '0';
    else
      
      case current_state is
        when st_reset =>
          end_of_frame <= '0';
          if ctrl_acquisition_en = '1' and lock_prev_section = '0' then
            next_state <= st_write;
          else
            next_state <= st_reset;
          end if;
          
        when st_write =>
          if (bram_cnt & addr_cnt) = unsigned(ctrl_bram_fill_threshold) then
            end_of_frame <= '1';
            next_state   <= st_wait_cdma;
          else
            end_of_frame <= '0';
            next_state   <= st_write;
          end if;
          
        when st_wait_cdma=>
          end_of_frame         <= '0';
          if lock_prev_section <= '0' then
            next_state <= st_reset;
          else
            next_state <= st_wait_cdma;
          end if;
          
        when others =>
          next_state   <= st_reset;
          end_of_frame <= '0';
      end case;
      
    end if;
  end process;

  bram_ctrl_fsm_sync_p : process(clk, resetb)
  begin
    if clk'event and clk = '1' then
      if resetb = '0' then
        current_state <= st_reset;
      else
        current_state <= next_state;
      end if;
    end if;
  end process;






  polarity_buff <= addr_cnt(13);

  bram_ctrl_addr_cnt_p : process(clk, resetb)
  begin
    if clk'event and clk = '1' then
      if resetb = '0' then
        addr_cnt         <= (others => '0');
        bram_cnt         <= (others => '0');
        bram_en_buff     <= '0';
        addr_first_start <= '0';
      else
        if current_state = st_reset and lock_prev_section = '0' then
          addr_cnt         <= (others => '0');
          bram_cnt         <= (others => '0');
          addr_first_start <= '0';
          bram_en_buff     <= '0';
        elsif current_state = st_write then
          if bram_data_out_en = '1' then  -- new data
            
            bram_en_buff     <= '1';
            addr_first_start <= '1';

            if addr_first_start = '0' then
              addr_cnt <= addr_cnt;
            else
              addr_cnt <= addr_cnt + 1;
            end if;
            if (addr_cnt+ 1) = 0 then
              bram_cnt <= bram_cnt+1;
            else
              bram_cnt <= bram_cnt;
            end if;
          else                          -- check for the end of frame
            addr_cnt         <= addr_cnt;
            bram_cnt         <= bram_cnt;
            bram_en_buff     <= '0';
            addr_first_start <= addr_first_start;
          end if;  -- check for the end of frame
        else
          bram_cnt         <= bram_cnt;
          addr_cnt         <= addr_cnt;
          bram_en_buff     <= '0';
          addr_first_start <= addr_first_start;
        end if;
      end if;
    end if;
  end process;


  reverse_polarity_p : process(clk, resetb)
  begin
    if clk'event and clk = '1' then
      if resetb = '0' then
        reverse_polarity   <= '1';
        end_of_frame_delay <= '0';
      else
        end_of_frame_delay <= end_of_frame;
        if polarity_buff_previous = not(polarity_buff) and end_of_frame_delay = '1' then
          reverse_polarity <= '0';
        elsif current_state = st_reset and next_state = st_write then
          reverse_polarity <= '1';
        else
          reverse_polarity <= reverse_polarity;
        end if;
      end if;
    end if;
  end process;



  bram_ctrl_irq_p : process(clk, resetb)
  begin
    if clk'event and clk = '1' then
      if resetb = '0' then

        packet_timestamp       <= (others => '0');
        polarity_buff_previous <= '0';
        irq_buff               <= '0';
        lock_prev_section      <= '0';
        overflow_flag_buff     <= '0';
      else

        

        if ctrl_irq_clear = '1' then
          packet_timestamp       <= packet_timestamp;
          irq_buff               <= '0';
          lock_prev_section      <= lock_prev_section;
          overflow_flag_buff     <= overflow_flag_buff;
          polarity_buff_previous <= polarity_buff;
          
        elsif ctrl_cdma_done = '1' then
          packet_timestamp       <= packet_timestamp;
          irq_buff               <= irq_buff;
          lock_prev_section      <= '0';
          overflow_flag_buff     <= overflow_flag_buff;
          polarity_buff_previous <= polarity_buff;
          
          
        elsif ctrl_overflow_flag_clear = '1' then
          packet_timestamp       <= packet_timestamp;
          irq_buff               <= irq_buff;
          lock_prev_section      <= lock_prev_section;
          overflow_flag_buff     <= '0';
          polarity_buff_previous <= polarity_buff;


        elsif current_state = st_reset then
          packet_timestamp       <= packet_timestamp;
          irq_buff               <= irq_buff;
          lock_prev_section      <= lock_prev_section;
          overflow_flag_buff     <= overflow_flag_buff;
          polarity_buff_previous <= '0';
          


        elsif current_state = st_wait_cdma and next_state = st_reset then
          packet_timestamp       <= packet_timestamp + 1;
          irq_buff               <= '1';
          lock_prev_section      <= '1';
          polarity_buff_previous <= polarity_buff;

          if lock_prev_section = '1' then
            overflow_flag_buff <= '1';
          else
            overflow_flag_buff <= overflow_flag_buff;
          end if;
          
        elsif current_state = st_write and polarity_buff_previous = not(polarity_buff) and end_of_frame_delay = '0' then
          
          packet_timestamp       <= packet_timestamp + 1;
          irq_buff               <= '1';
          lock_prev_section      <= '1';
          polarity_buff_previous <= polarity_buff;

          if lock_prev_section = '1' then
            overflow_flag_buff <= '1';
          else
            overflow_flag_buff <= overflow_flag_buff;
          end if;
          
        else
          packet_timestamp       <= packet_timestamp;
          irq_buff               <= irq_buff;
          lock_prev_section      <= lock_prev_section;
          overflow_flag_buff     <= overflow_flag_buff;
          polarity_buff_previous <= polarity_buff;

        end if;
      end if;
    end if;
  end process;





end rtl;
