###############################################################
# NUCLEO INTERFACE
###############################################################
# SPI CONFIG MASTER
set_property IOSTANDARD LVCMOS33 [get_ports config_spi_mosi]
set_property PACKAGE_PIN V13 [get_ports config_spi_mosi]
set_property IOSTANDARD LVCMOS33 [get_ports config_spi_miso]
set_property PACKAGE_PIN U13 [get_ports config_spi_miso]
set_property IOSTANDARD LVCMOS33 [get_ports config_spi_clk]
set_property PACKAGE_PIN T10 [get_ports config_spi_clk]
set_property IOSTANDARD LVCMOS33 [get_ports {config_spi_ss[0]}]
set_property PACKAGE_PIN U12 [get_ports {config_spi_ss[0]}]

###############################################################
# USER LED
###############################################################
set_property IOSTANDARD LVCMOS33 [get_ports {gw_power_user_leds[0]}]
set_property PACKAGE_PIN Y7 [get_ports {gw_power_user_leds[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gw_power_user_leds[1]}]
set_property PACKAGE_PIN Y6 [get_ports {gw_power_user_leds[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gw_power_user_leds[2]}]
set_property PACKAGE_PIN V8 [get_ports {gw_power_user_leds[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gw_power_user_leds[3]}]
set_property PACKAGE_PIN W8 [get_ports {gw_power_user_leds[3]}]

###############################################################
# RF INTERFACE
###############################################################
# SATA_1=J19; SATA_2=J20; SATA_3=J21; ATM0=J5
###############################################################

# rx / tx clk are swapped

# RF RX CLK
set_property IOSTANDARD LVDS_25 [get_ports rf_rx_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports rf_rx_clk_n]
#set_property PACKAGE_PIN D19 [get_ports rf_rx_clk_p]
#set_property PACKAGE_PIN D20 [get_ports rf_rx_clk_n]
set_property PACKAGE_PIN K17 [get_ports rf_rx_clk_p]
set_property PACKAGE_PIN K18 [get_ports rf_rx_clk_n]


# RF DATA RX_09
set_property IOSTANDARD LVDS_25 [get_ports rf_rx09_data_p]
set_property IOSTANDARD LVDS_25 [get_ports rf_rx09_data_n]
set_property PACKAGE_PIN F16 [get_ports rf_rx09_data_p]
set_property PACKAGE_PIN F17 [get_ports rf_rx09_data_n]



# RF TX CLK
set_property IOSTANDARD LVDS_25 [get_ports rf_tx_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports rf_tx_clk_n]
#set_property PACKAGE_PIN K17 [get_ports rf_tx_clk_p]
#set_property PACKAGE_PIN K18 [get_ports rf_tx_clk_n]
set_property PACKAGE_PIN D19 [get_ports rf_tx_clk_p]
set_property PACKAGE_PIN D20 [get_ports rf_tx_clk_n]

# RF DATA TX
set_property IOSTANDARD LVDS_25 [get_ports rf_tx_data_p]
set_property IOSTANDARD LVDS_25 [get_ports rf_tx_data_n]
#set_property PACKAGE_PIN K19 [get_ports rf_tx_data_p]
#set_property PACKAGE_PIN J19 [get_ports rf_tx_data_n]
set_property PACKAGE_PIN B19 [get_ports rf_tx_data_p]
set_property PACKAGE_PIN A20 [get_ports rf_tx_data_n]



# SPI EXTERNAL MASTER
set_property IOSTANDARD LVCMOS25 [get_ports external_spi_mosi]
set_property PACKAGE_PIN K14 [get_ports external_spi_mosi]
set_property IOSTANDARD LVCMOS25 [get_ports external_spi_miso]
set_property PACKAGE_PIN L14 [get_ports external_spi_miso]
set_property IOSTANDARD LVCMOS25 [get_ports external_spi_clk]
set_property PACKAGE_PIN L15 [get_ports external_spi_clk]
set_property IOSTANDARD LVCMOS25 [get_ports {external_spi_ss[0]}]
set_property PACKAGE_PIN J14 [get_ports {external_spi_ss[0]}]

###############################################################
# CLK CONSTRAINTS
###############################################################
create_clock -period 15.625 -name rf_rx_in_clk -waveform {0.000 7.813} [get_pins LVDS_TO_RF_RX_CLK_IBUFDS_inst/O]

###############################################################
# FALSE PATH
###############################################################

set_false_path -from [get_clocks rf_rx_in_clk] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks rf_rx_in_clk]

#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rf_clk]
