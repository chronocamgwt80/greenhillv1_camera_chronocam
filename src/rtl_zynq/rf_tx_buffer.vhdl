--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief top Consist of FIFO and control block, this buffer is filled either
--! by the higher level FIFO (when ARM core is preparing the frame to send or by
--! itself when the serializer is enable. The last case guarantee to keep always
--! the frame within the FIFO: each read element by serializer is rewritten inside
--! the FIFO.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
------------------------------------------------------------------------------
--! RF TX buffer entity
------------------------------------------------------------------------------
entity rf_tx_buffer is
  
  port (
    rf_tx_buffer_clk        : in  std_logic;  --! RF CLK
    rf_tx_buffer_arst       : in  std_logic;  --! asynchronous reset active high
    --! control I/O
    rf_tx_buffer_polarity   : in  std_logic;  --! flag to choose who has to fill FIFO
    --! PL FIFO interface I/O
    rf_tx_buffer_din_valid  : in  std_logic;
    rf_tx_buffer_din        : in  std_logic_vector(27 downto 0);
    rf_tx_buffer_rd_din_en  : out std_logic;
    --! Serializer interface I/O
    rf_tx_buffer_dout_valid : out std_logic;
    rf_tx_buffer_dout       : out std_logic_vector(27 downto 0);
    rf_tx_buffer_rd_en      : in  std_logic;
    --! status I/O
    rf_tx_buffer_full       : out std_logic;  --! flag indicating fifo 28b is full
    rf_tx_buffer_empty      : out std_logic;  --! flag indicating fifo 28b is empty
    rf_tx_buffer_data_count : out std_logic_vector(17 downto 0)  --! provide the
                                                                 --number of
                                                                 --words in FIFO
    );
end entity rf_tx_buffer;

------------------------------------------------------------------------------
--! RF TX buffer behavior
------------------------------------------------------------------------------
architecture behavior of rf_tx_buffer is
  ------------------------------------------------------------------------------
  --! FIFO 28 bits signals
  ------------------------------------------------------------------------------
  signal fifo_28b_clk        : std_logic;
  signal fifo_28b_arst       : std_logic;
  signal fifo_28b_din        : std_logic_vector(27 downto 0);
  signal fifo_28b_wr_en      : std_logic;
  signal fifo_28b_rd_en      : std_logic;
  signal fifo_28b_dout       : std_logic_vector(27 downto 0);
  signal fifo_28b_full       : std_logic;
  signal fifo_28b_empty      : std_logic;
  signal fifo_28b_valid      : std_logic;
  signal fifo_28b_data_count : std_logic_vector(17 downto 0);
begin  --! architecture behavior
  ------------------------------------------------------------------------------
  --! Component outputs updating
  ------------------------------------------------------------------------------
  rf_tx_buffer_dout_valid <= fifo_28b_valid;
  rf_tx_buffer_rd_din_en  <= rf_tx_buffer_din_valid;
  rf_tx_buffer_dout       <= fifo_28b_dout;
  rf_tx_buffer_full       <= fifo_28b_full;
  rf_tx_buffer_empty      <= fifo_28b_empty;
  rf_tx_buffer_data_count <= fifo_28b_data_count;
  ------------------------------------------------------------------------------
  --! FIFO 28b signals binding
  ------------------------------------------------------------------------------
  fifo_28b_clk            <= rf_tx_buffer_clk;
  fifo_28b_arst           <= rf_tx_buffer_arst;
  ------------------------------------------------------------------------------
  --! Mux to control who has to fill the FIFO (either PL FIFO or FIFO_28b itself)
  ------------------------------------------------------------------------------
  fifo_28b_wr_en          <= rf_tx_buffer_din_valid when (rf_tx_buffer_polarity = '0') else rf_tx_buffer_rd_en;
  fifo_28b_rd_en          <= rf_tx_buffer_rd_en;
  fifo_28b_din            <= rf_tx_buffer_din       when (rf_tx_buffer_polarity = '0') else fifo_28b_dout;

  ------------------------------------------------------------------------------
  --! FIFO 28 bits
  ------------------------------------------------------------------------------
  fifo_28b_1 : entity work.fifo_28b
    port map (
      clk        => fifo_28b_clk,
      rst       => fifo_28b_arst,
      din        => fifo_28b_din,
      wr_en      => fifo_28b_wr_en,
      rd_en      => fifo_28b_rd_en,
      dout       => fifo_28b_dout,
      full       => fifo_28b_full,
      empty      => fifo_28b_empty,
      valid      => fifo_28b_valid,
      data_count => fifo_28b_data_count);

end architecture behavior;

