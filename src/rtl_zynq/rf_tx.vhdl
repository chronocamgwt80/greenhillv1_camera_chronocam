--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief top RF TX block design, consist of FIFO, serializer, resync, and
--! specific block for output (ODDR, OBUF etc.)
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
------------------------------------------------------------------------------
--! RF TX entity
------------------------------------------------------------------------------
entity rf_tx is
  port (
    rf_tx_clk             : in  std_logic;  --! RF CLK received from RF chip
    rf_tx_arst            : in  std_logic;  --! asynchronous reset active high
    rf_tx_rd_din_en       : out std_logic;  --! flag to generate a writing request
    rf_tx_din_valid       : in  std_logic;  --! flag indicating a new data is
                                            --! ready on input
    rf_tx_din             : in  std_logic_vector(27 downto 0);  --! data to write 
    rf_tx_enable          : in  std_logic;  --! enable RF TX
    rf_tx_full            : out std_logic;  --! flag indicating buffer is full
    rf_tx_empty           : out std_logic;  --! flag indicating buffer is empty
    rf_tx_out_data_vector : out std_logic_vector(1 downto 0);  --! data vector to
    rf_tx_loop_burst_mode : in  std_logic;   --! to set in loop or burst mode
    rf_tx_data_count      : out std_logic_vector(17 downto 0)  --! provide the
                                                               --number of
                                                               --words in FIFO
    );               
end entity rf_tx;

------------------------------------------------------------------------------
--! RF TX behavior
------------------------------------------------------------------------------
architecture behavior of rf_tx is

  ------------------------------------------------------------------------------
  --! Buffer signals
  ------------------------------------------------------------------------------
  signal rf_tx_buffer_clk             : std_logic;  -- ! RF CLK
  signal rf_tx_buffer_arst            : std_logic;  -- ! asynchronous reset active high
  signal rf_tx_buffer_polarity        : std_logic;  -- ! flag to choose who has to fill FIFO
  signal rf_tx_buffer_din_valid       : std_logic;
  signal rf_tx_buffer_din             : std_logic_vector(27 downto 0);
  signal rf_tx_buffer_rd_din_en       : std_logic;
  signal rf_tx_buffer_dout_valid      : std_logic;
  signal rf_tx_buffer_dout            : std_logic_vector(27 downto 0);
  signal rf_tx_buffer_rd_en           : std_logic;
  signal rf_tx_buffer_full            : std_logic;  -- ! flag indicating fifo 28b is full
  signal rf_tx_buffer_empty           : std_logic;  -- ! flag indicating fifo 28b is empty
  signal rf_tx_buffer_data_count      : std_logic_vector(17 downto 0);  -- ! provide the
  ------------------------------------------------------------------------------
  --! Serializer signals
  ------------------------------------------------------------------------------
  signal rf_tx_serializer_clk         : std_logic;  --! RF module supplies it
  signal rf_tx_serializer_arst        : std_logic;  --! FPGA reset
  signal rf_tx_serializer_en          : std_logic;  --! to enable/disable module
  signal rf_tx_serializer_din_valid   : std_logic;  --! flag indicating valid data is ready to
  signal rf_tx_serializer_rd_en       : std_logic;  --! data read request
  signal rf_tx_serializer_din         : std_logic_vector(27 downto 0);  --! data of the sample to send
  signal rf_tx_serializer_dout_vector : std_logic_vector(1 downto 0);  --! serialized bit of the sample
  ------------------------------------------------------------------------------
  --! LVDS Output packaging signals
  ------------------------------------------------------------------------------


begin  --! architecture behavior
  ------------------------------------------------------------------------------
  --! Component outputs updating
  ------------------------------------------------------------------------------
  rf_tx_rd_din_en            <= rf_tx_buffer_rd_din_en;
  rf_tx_full                 <= rf_tx_buffer_full;
  rf_tx_empty                <= rf_tx_buffer_empty;
  rf_tx_data_count           <= rf_tx_buffer_data_count;
  ------------------------------------------------------------------------------
  --! Buffer signals binding
  ------------------------------------------------------------------------------
  rf_tx_buffer_clk           <= rf_tx_clk;
  rf_tx_buffer_arst          <= rf_tx_arst;
  rf_tx_buffer_polarity      <= rf_tx_loop_burst_mode;
  rf_tx_buffer_din_valid     <= rf_tx_din_valid;
  rf_tx_buffer_din           <= rf_tx_din;
  rf_tx_buffer_rd_en         <= rf_tx_serializer_rd_en;
  ------------------------------------------------------------------------------
  --! Serializer signals binding
  ------------------------------------------------------------------------------
  rf_tx_serializer_clk       <= rf_tx_clk;
  rf_tx_serializer_arst      <= rf_tx_arst;
  rf_tx_serializer_en        <= rf_tx_enable;
  rf_tx_serializer_din_valid <= rf_tx_buffer_dout_valid;
  rf_tx_serializer_din       <= rf_tx_buffer_dout;
  ------------------------------------------------------------------------------
  --! LVDS Output packaging binding
  ------------------------------------------------------------------------------
  rf_tx_out_data_vector      <= rf_tx_serializer_dout_vector;

  ------------------------------------------------------------------------------
  --! Buffer to store the frame to send
  ------------------------------------------------------------------------------
  rf_tx_buffer_1 : entity work.rf_tx_buffer
    port map (
      rf_tx_buffer_clk        => rf_tx_buffer_clk,   -- ! RF CLK
      rf_tx_buffer_arst       => rf_tx_buffer_arst,  -- ! asynchronous reset active high
      rf_tx_buffer_polarity   => rf_tx_buffer_polarity,  -- ! flag to choose who has to fill FIFO
      rf_tx_buffer_din_valid  => rf_tx_buffer_din_valid,
      rf_tx_buffer_din        => rf_tx_buffer_din,
      rf_tx_buffer_rd_din_en  => rf_tx_buffer_rd_din_en,
      rf_tx_buffer_dout_valid => rf_tx_buffer_dout_valid,
      rf_tx_buffer_dout       => rf_tx_buffer_dout,
      rf_tx_buffer_rd_en      => rf_tx_buffer_rd_en,
      rf_tx_buffer_full       => rf_tx_buffer_full,  -- ! flag indicating fifo 28b is full
      rf_tx_buffer_empty      => rf_tx_buffer_empty,  -- ! flag indicating fifo 28b is empty
      rf_tx_buffer_data_count => rf_tx_buffer_data_count);  -- ! provide the
  ------------------------------------------------------------------------------
  --! Serializer to bits of each sample
  ------------------------------------------------------------------------------
  rf_tx_serializer_1 : entity work.rf_tx_serializer
    port map (
      rf_tx_serializer_clk         => rf_tx_serializer_clk,  --! RF module supplies it
      rf_tx_serializer_arst        => rf_tx_serializer_arst,   --! FPGA reset
      rf_tx_serializer_en          => rf_tx_serializer_en,  --! to enable/disable module
      rf_tx_serializer_din_valid   => rf_tx_serializer_din_valid,  --! flag indicating valid data is ready to
      rf_tx_serializer_rd_en       => rf_tx_serializer_rd_en,  --! data read request
      rf_tx_serializer_din         => rf_tx_serializer_din,  --! data of the sample to send
      rf_tx_serializer_dout_vector => rf_tx_serializer_dout_vector);  --! serialized bit of the sample
------------------------------------------------------------------------------
--! LVDS Output packaging : prepare output signals to send to RF chip through lvds with
--! double data rate
------------------------------------------------------------------------------

end architecture behavior;
