--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief RF controller module(TX)
--! @details
--! This module gets samples I then Q stored in fifo successively and sends them
--! to the serializer
--------------------------------------------------------------------------------
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
------------------------------------------------------------------------------
-- RF controller entity
------------------------------------------------------------------------------
entity rf_controller_tx is
  port (
    clk       : in  std_logic;
    rstb      : in  std_logic;
    -- control
    start     : in  std_logic;
    stop      : in  std_logic;                      -- to enable/disable module
    sample_nb : in  std_logic_vector(31 downto 0);  -- number of sample (I,Q) to
                                                    -- read in FIFO
    -- fifo interface
    dout      : in  std_logic_vector(27 downto 0);  -- fifo output to read the
                                                    -- sample
    rd_en     : out std_logic;                      -- reading request
    -- rf interface
    rf_enable : out std_logic;                      -- to enable/disable rf
                                                    -- interface module (set
                              -- high when first sample (I,Q) is
                              -- ready to be sent
    data_i    : out std_logic_vector(13 downto 0);  -- I data of the sample to send
    data_q    : out std_logic_vector(13 downto 0);  -- Q data of the sample to send
    -- monitoring
    fsm_state : out std_logic_vector(3 downto 0)    -- to read the current state
    );
end entity rf_controller_tx;

------------------------------------------------------------------------------
-- RF controller TX behavior
------------------------------------------------------------------------------
architecture behavior of rf_controller_tx is
  type state is(st_rst, st_sleep, st_read_data, st_ready, st_transfer_data, st_disable_rf_module);
  signal current_state : state;
  signal next_state    : state;

  signal internal_start     : std_logic_vector(1 downto 0);
  signal internal_stop      : std_logic_vector(1 downto 0);
  signal internal_rd_en     : std_logic;
  signal internal_rf_enable : std_logic;
  signal internal_sample_nb : unsigned(31 downto 0);
  signal internal_data_i    : std_logic_vector(13 downto 0);
  signal internal_data_q    : std_logic_vector(13 downto 0);
  signal internal_counter   : unsigned(3 downto 0);  -- waiting for a cycles
                                                     -- number before sample
                                                     -- sending to sequence the
                                                     -- transfer
begin

  -- outputs updating
  rd_en     <= internal_rd_en;
  rf_enable <= internal_rf_enable;
  data_i    <= internal_data_i;
  data_q    <= internal_data_q;

  -- fsm state monitoring
  fsm_state <= x"0" when (current_state = st_rst) else
               x"1" when (current_state = st_sleep) else
               x"2" when (current_state = st_read_data) else
               x"3" when (current_state = st_ready) else
               x"4" when (current_state = st_transfer_data) else
               x"F";

  -- update state of the FSM
  state_update : process(clk, rstb)
  begin
    if clk'event and clk = '1' then
      if rstb = '0' then
        current_state <= st_rst;
      else
        current_state <= next_state;
      end if;
    end if;
  end process;

  -- state machine
  state_machine : process(rstb, current_state, internal_start, internal_stop, internal_counter)
  begin
    if rstb = '0' then
      next_state <= st_rst;
    elsif internal_stop(0) = '0' and internal_stop(1) = '1' then
      next_state <= st_disable_rf_module;
    else
      case current_state is

        -- reset
        when st_rst =>
          next_state <= st_sleep;

        -- waiting for start signal
        when st_sleep =>
          if internal_start(0) = '0' and internal_start(1) = '1' and internal_sample_nb < unsigned(sample_nb)  then
            next_state <= st_read_data;
          else
            next_state <= st_sleep;
          end if;

        -- get a new data to transfer in bram
        when st_read_data =>
          next_state <= st_ready;

        -- waiting for n cycles to sequence data on RF module sampling frequency
        when st_ready =>
          if internal_counter = x"C" then  -- waiting for 14 cycles + 2 cycles
                                           -- before sending an other word
            next_state <= st_transfer_data;
          else
            next_state <= st_ready;
          end if;

        -- transfer data
        when st_transfer_data =>
          -- go on
          if internal_sample_nb = unsigned(sample_nb) then
            next_state <= st_sleep;
          else
            next_state <= st_read_data;
          end if;

        -- default case
        when others =>
          next_state <= st_sleep;
          null;
         
      end case;
    end if;
  end process;


-- generate a FIFO read request
  read_sample_request : process (clk, rstb) is
  begin
    if clk'event and clk = '1' then
      if current_state = st_rst then
        internal_rd_en <= '0';
      elsif current_state = st_read_data then
        internal_rd_en <= '1';
      else
        internal_rd_en <= '0';
      end if;
    else
      internal_rd_en <= internal_rd_en;
    end if;
  end process read_sample_request;

-- read a new FIFO sample
  read_sample : process (clk, rstb) is
  begin  -- process
    if clk'event and clk = '1' then
      if current_state = st_rst then
        internal_data_i <= (others => '0');
        internal_data_q <= (others => '0');
      else
        if current_state = st_sleep then
          internal_data_i <= (others => '0');
          internal_data_q <= (others => '0');
        elsif current_state = st_ready and internal_rd_en = '1' then
          internal_data_i <= dout(27 downto 14);
          internal_data_q <= dout(13 downto 0);
        else
          internal_data_i <= internal_data_i;
          internal_data_q <= internal_data_q;
        end if;
      end if;
    else
      internal_data_i <= internal_data_i;
      internal_data_q <= internal_data_q;
    end if;
  end process read_sample;

-- update remaining number of sample to read
  remaining_samples_update : process (clk, rstb) is
  begin
    if clk'event and clk = '1' then
      if current_state = st_rst then
        internal_sample_nb <= (others => '0');
      elsif current_state = st_sleep then
        internal_sample_nb <= (others => '0');
      elsif current_state = st_read_data then
        internal_sample_nb <= internal_sample_nb + 1;
      else
        internal_sample_nb <= internal_sample_nb;
      end if;
    else
      internal_sample_nb <= internal_sample_nb;
    end if;
  end process remaining_samples_update;

-- enable RF interface
  enable_rf_interface : process (clk, rstb) is
  begin
    if clk'event and clk = '1' then
      if current_state = st_rst then
        internal_rf_enable <= '0';
      elsif (current_state /= st_sleep) and (current_state /= st_rst) then
        internal_rf_enable <= '1';
      else
        internal_rf_enable <= '0';
      end if;
    else
      internal_rf_enable <= internal_rf_enable;
    end if;
  end process enable_rf_interface;

-- cycles number to wait for before transfer data
  counter_update : process (clk, rstb) is
  begin
    if clk'event and clk = '1' then
      if current_state = st_rst then
        internal_counter <= (others => '0');
      elsif current_state = st_ready then
        internal_counter <= internal_counter + 1;
      else
        internal_counter <= (others => '0');
      end if;
    else
      internal_counter <= internal_counter;
    end if;
  end process counter_update;


-- start rising edge detector
  start_rising_edge_detector : process (clk, rstb) is
  begin  -- process shift_register
    if rstb = '0' then
      internal_start <= (others => '0');
    else
      if clk'event and clk = '1' then
        internal_start <= start & internal_start(1);
      else
        internal_start <= internal_start;
      end if;
    end if;
  end process start_rising_edge_detector;


-- stop rising edge detector
  stop_rising_edge_detector : process (clk, rstb) is
  begin  -- process shift_register
    if rstb = '0' then
      internal_stop <= (others => '0');
    else
      if clk'event and clk = '1' then
        internal_stop <= stop & internal_stop(1);
      else
        internal_stop <= internal_stop;
      end if;
    end if;
  end process stop_rising_edge_detector;

end behavior;


