--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief Resynch signals from an other clock domain to the current clock domain
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
------------------------------------------------------------------------------
-- RF entity
------------------------------------------------------------------------------
entity rf_resync is
  
  port (
    rf_resync_clk  : in  std_logic;
    rf_resync_arst : in  std_logic;
    rf_async_bit   : in  std_logic;
    rf_sync_bit    : out std_logic
    );
end entity rf_resync;
------------------------------------------------------------------------------
-- RF behavior
------------------------------------------------------------------------------
architecture behavior of rf_resync is
  signal internal_shift_register : unsigned(2 downto 0);
  
begin  -- architecture behavior

  -- output updated
  rf_sync_bit <= internal_shift_register(0);

  -- sync bit with "triplette de resyncho" three stages registers
  start_sync : process(rf_resync_clk, rf_resync_arst)
  begin
    if rf_resync_arst = '1' then
      internal_shift_register <= (others => '0');
    elsif rf_resync_clk'event and rf_resync_clk = '1' then
        internal_shift_register <= rf_async_bit & internal_shift_register(2 downto 1);
     else
       internal_shift_register<=internal_shift_register;
    end if;
  end process;
  
end architecture behavior;
