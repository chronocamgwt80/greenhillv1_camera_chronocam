--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief top RF block design, consist of TX and RX block designs
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
------------------------------------------------------------------------------
--! RF entity
------------------------------------------------------------------------------
entity rf is
  port (
    rf_clk               : in  std_logic;  --! RF CLK
    rf_fpga_arst         : in  std_logic;  --! asynchronous reset active high, from FPGA
    rf_txrst             : in  std_logic;  --! reset active high, from register
    --! RF RX
    --! RF TX
    rf_txen              : in  std_logic;  --! enable RF TX
    rf_txrd_din_en       : out std_logic;  --! flag to generate a writing request
    rf_txdin_valid       : in  std_logic;  --! flag indicating a new sample IQ is ready
    rf_txdin             : in  std_logic_vector(27 downto 0);  --! sample IQ to write
    rf_txfull            : out std_logic;  --! flag indicating buffer is full
    rf_txempty           : out std_logic;  --! flag indicating buffer is empty
    rf_txout_data_vector : out std_logic_vector(1 downto 0);
    rf_txloopburst_mode  : in  std_logic;  --! to set in loop or burst mode
    rf_txdata_count      : out std_logic_vector(17 downto 0)   --! provide the
                                                               --number of
                                                               --words in FIFO
    );
end entity rf;

------------------------------------------------------------------------------
--! RF behavior
------------------------------------------------------------------------------
architecture behavior of rf is
  ------------------------------------------------------------------------------
  --! Component signals
  ------------------------------------------------------------------------------
  signal internal_rf_clk          : std_logic;
  ------------------------------------------------------------------------------
  --! TX signals
  ------------------------------------------------------------------------------
  signal rf_tx_clk                : std_logic;  -- ! RF CLK received from RF chip
  signal rf_tx_arst               : std_logic;  -- ! asynchronous reset active high
  signal rf_tx_rd_din_en          : std_logic;  -- ! flag to generate a writing request
  signal rf_tx_din_valid          : std_logic;  -- ! flag indicating a new data is
  signal rf_tx_din                : std_logic_vector(27 downto 0);  -- ! data to write
  signal rf_tx_enable             : std_logic;  -- ! enable RF TX
  signal rf_tx_full               : std_logic;  -- ! flag indicating buffer is full
  signal rf_tx_empty              : std_logic;  -- ! flag indicating buffer is empty
  signal rf_tx_out_data_vector    : std_logic_vector(1 downto 0);  -- ! data vector to
  signal rf_tx_loop_burst_mode    : std_logic;  -- ! to set in loop or burst mode
  signal rf_tx_data_count         : std_logic_vector(17 downto 0);  -- ! provide the
  ------------------------------------------------------------------------------
  --! Re-sync signals
  ------------------------------------------------------------------------------
  signal rf_txen_sync             : std_logic;
  signal rf_txrst_sync            : std_logic;
  signal rf_rxrst_sync            : std_logic;
  signal rf_txloopburst_mode_sync : std_logic;
  
begin  --! architecture behavior
  ------------------------------------------------------------------------------
  --! Component outputs updating
  ------------------------------------------------------------------------------
  rf_txrd_din_en        <= rf_tx_rd_din_en;
  rf_txfull             <= rf_tx_full;
  rf_txempty            <= rf_tx_empty;
  rf_txout_data_vector  <= rf_tx_out_data_vector;
  rf_txdata_count       <= rf_tx_data_count;
  ------------------------------------------------------------------------------
  --! Internal component signals binding
  ------------------------------------------------------------------------------
  internal_rf_clk       <= rf_clk;
  ------------------------------------------------------------------------------
  --! TX signals binding
  ------------------------------------------------------------------------------
  rf_tx_clk             <= internal_rf_clk;
  rf_tx_arst            <= rf_fpga_arst or rf_txrst_sync;
  rf_tx_din_valid       <= rf_txdin_valid;
  rf_tx_din             <= rf_txdin;
  rf_tx_enable          <= rf_txen_sync;
  rf_tx_loop_burst_mode <= rf_txloopburst_mode_sync;

  ------------------------------------------------------------------------------
  --! TX 
  ------------------------------------------------------------------------------
  rf_tx_1 : entity work.rf_tx
    port map (
      rf_tx_clk             => rf_tx_clk,    -- ! RF CLK received from RF chip
      rf_tx_arst            => rf_tx_arst,   -- ! asynchronous reset active high
      rf_tx_rd_din_en       => rf_tx_rd_din_en,  -- ! flag to generate a writing request
      rf_tx_din_valid       => rf_tx_din_valid,  -- ! flag indicating a new data is
      rf_tx_din             => rf_tx_din,    -- ! data to write
      rf_tx_enable          => rf_tx_enable,     -- ! enable RF TX
      rf_tx_full            => rf_tx_full,   -- ! flag indicating buffer is full
      rf_tx_empty           => rf_tx_empty,  -- ! flag indicating buffer is empty
      rf_tx_out_data_vector => rf_tx_out_data_vector,  -- ! data vector to
      rf_tx_loop_burst_mode => rf_tx_loop_burst_mode,  -- ! to set in loop or burst mode
      rf_tx_data_count      => rf_tx_data_count);      -- ! provide the
  ------------------------------------------------------------------------------
  --! Re-sync
  ------------------------------------------------------------------------------
  -- TX enable from registers map
  rf_txen_resync_1 : entity work.rf_resync
    port map (
      rf_resync_clk  => internal_rf_clk,
      rf_resync_arst => rf_fpga_arst,
      rf_async_bit   => rf_txen,
      rf_sync_bit    => rf_txen_sync);

  -- TX reset from registers map
  rf_txrst_resync_1 : entity work.rf_resync
    port map (
      rf_resync_clk  => internal_rf_clk,
      rf_resync_arst => rf_fpga_arst,
      rf_async_bit   => rf_txrst,
      rf_sync_bit    => rf_txrst_sync);

  -- TX loop/burst mode from registers map
  rf_txloopburst_resync_1 : entity work.rf_resync
    port map (
      rf_resync_clk  => internal_rf_clk,
      rf_resync_arst => rf_fpga_arst,
      rf_async_bit   => rf_txloopburst_mode,
      rf_sync_bit    => rf_txloopburst_mode_sync);

end architecture behavior;
