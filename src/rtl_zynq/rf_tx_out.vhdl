--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief prepare output signals to send to RF chip through lvds with double
--! data rate
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;
------------------------------------------------------------------------------
-- RF TX out entity
------------------------------------------------------------------------------
entity rf_tx_out is
  
  port (
    rf_tx_out_clk         : in  std_logic;  -- RF TXCLK to prepare for LVDS
    rf_tx_out_clk_p       : out std_logic;  -- positive LVDS TXCLK
    rf_tx_out_clk_n       : out std_logic;  -- negative LVDS TXCLK
    rf_tx_out_data_vector : in  std_logic_vector(1 downto 0);  -- data vector to
                                                               -- bit on double
                                                               -- data rate
    rf_tx_out_data_p      : out std_logic;  -- positive LVDS TXD
    rf_tx_out_data_n      : out std_logic   -- negative LCDS TXD
    );

end entity rf_tx_out;

------------------------------------------------------------------------------
-- RF TX out behavior
------------------------------------------------------------------------------
architecture behavior of rf_tx_out is
  signal rf_tx_out_data : std_logic;
begin  -- architecture behavior

  ------------------------------------------------------------------------------
  -- prepare RF CLK for LVDS
  ------------------------------------------------------------------------------
  RF_TX_CLK_TO_LVDS_OBUFDS_inst : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25")
    port map (
      OB => rf_tx_out_clk_n,  -- Diff_n output (connect directly to top-level port)
      O  => rf_tx_out_clk_p,  -- Diff_p output (connect directly to top-level port)
      I  => rf_tx_out_clk     -- Buffer output
      );

  ------------------------------------------------------------------------------
  -- sequence data for Double Data Rate
  ------------------------------------------------------------------------------
  RF_TX_DATA_VECTOR_TO_DOUBLE_DATA_RATE_inst_d : ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",     -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT         => '0',             -- Initial value for Q port ('1' or '0')
      SRTYPE       => "SYNC")          -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q  => rf_tx_out_data,            -- 1-bit DDR output
      C  => rf_tx_out_clk,             -- 1-bit clock input
      CE => '1',                       -- 1-bit clock enable input
      D1 => rf_tx_out_data_vector(1),  -- 1-bit data input (positive edge)
      D2 => rf_tx_out_data_vector(0),  -- 1-bit data input (negative edge)
      R  => '0',                       -- 1-bit reset input
      S  => '0'                        -- 1-bit set input
      );

  ------------------------------------------------------------------------------
  -- prepare data for LVDS
  ------------------------------------------------------------------------------
  RF_TX_DATA_TO_LVDS_OBUFDS_inst : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25")
    port map (
      O  => rf_tx_out_data_p,  -- Diff_p output (connect directly to top-level port)
      OB => rf_tx_out_data_n,  -- Diff_n output (connect directly to top-level port)
      I  => rf_tx_out_data    -- Buffer output
      );
end architecture behavior;
