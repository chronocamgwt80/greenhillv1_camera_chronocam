--! by stephane breysse, 05/2016
--! @brief: SPI End Of Frame (EOF) detector, detects the number of SPI clock edge.
--!         Each byte requires 8 SPI clock edge.
--!         So, the number of SPI cLock edge to detect before a EOF is
--!         nbbytes*8 (by shifting counter bit vector << 3). 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;

entity spi_eof_detector is
  port(
    clk         : in  std_logic;
    sck_o       : in  std_logic;
    ss_o        : in  std_logic;
    axi_reset   : in  std_logic;
    axi_byte_ctrl:in std_logic_vector(7 downto 0);
    axi_tx_end : out std_logic
    );
end spi_eof_detector;


architecture rtl of spi_eof_detector is
  
  signal sck_o_1               : std_logic                    := '0';
  signal sck_o_2               : std_logic                    := '0';
  signal ext_spi_ctrl_reset    : std_logic                    := '0';
  signal ext_spi_byte_cntr     : std_logic_vector(7 downto 0) := (others => '0');
  signal ext_spi_status_tx_end : std_logic                    := '0';
  signal ss_o_i                : std_logic                    := '0';         
  signal reset                 : std_logic                    := '1';
  signal cntr                  : unsigned(10 downto 0)        := (others => '0');
  signal byte_cntr             : std_logic_vector(7 downto 0) := (others => '0');
  signal tx_end                : std_logic                    := '0';
begin

  ss_o_i <= ss_o;
  reset <= axi_reset;
  byte_cntr <= axi_byte_ctrl;

  axi_tx_end <= tx_end;

  p_latch : process(clk)
  begin
    if rising_edge(clk) then
      sck_o_1 <= sck_o;
      sck_o_2 <= sck_o_1;
    end if;
  end process;

  p_eof_detector : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' or ss_o_i = '1' then
        cntr   <= unsigned(byte_cntr)&"000";
        tx_end <= '0';
      else
        if cntr = 0 then
          cntr   <= (others => '0');
          tx_end <= '1';
        elsif sck_o_1 = '1' and sck_o_2 = '0' then  --! SPI clock rising edge detector
          cntr   <= cntr - 1;
          tx_end <= '0';
        else
          cntr   <= cntr;
          tx_end <= '0';
        end if;
      end if;
    end if;
  end process;
  
end rtl;
