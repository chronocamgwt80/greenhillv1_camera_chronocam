--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief prepare input signal received from RF chip through lvds with
--! double data rate
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;
------------------------------------------------------------------------------
-- RF TX out entity
------------------------------------------------------------------------------
entity rf_rx_in is
  
  port (
    rf_rx_in_clk   : out std_logic;  -- RF RXCLK to prepare for LVDS
    rf_rx_in_clk_p : in  std_logic;  -- positive LVDS RXCLK
    rf_rx_in_clk_n : in  std_logic   -- negative LVDS RXCLK
   --rf_rx_in_data_p      : out std_logic;  -- positive LVDS RXD
   --rf_rx_in_data_n      : out std_logic   -- negative LCDS RXD
    );

end entity rf_rx_in;

------------------------------------------------------------------------------
-- RF TX out behavior
------------------------------------------------------------------------------
architecture behavior of rf_rx_in is

begin  -- architecture behavior

  ------------------------------------------------------------------------------
  -- prepare RF CLK received through LVDS
  ------------------------------------------------------------------------------
  LVDS_TO_RF_RX_CLK_IBUFDS_inst : IBUFDS
    generic map (
      DIFF_TERM    => true,   -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS_25")
    port map (
      O  => rf_rx_in_clk,     -- Buffer input 
      I  => rf_rx_in_clk_p,  -- Diff_p output (connect directly to top-level port)
      IB => rf_rx_in_clk_n  -- Diff_n output (connect directly to top-level port)
      );

end architecture behavior;
