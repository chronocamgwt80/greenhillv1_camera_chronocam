--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief RF TX Serializer module(TX)
--! @details
--! This module gets a sample to send to RF module. First, it gets I and Q of the
--! sample and then serializes them with a shift register.
--! - Output = 32 bits serialized word
--! - I(15:0) = 2 sync bits & 14 data bits
--! - Q(15:0) = 2 sync bits & 14 data bits
--! - Serialized word(31:0) = I(15:0) & Q(15:0)
--------------------------------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------
-- RF TX Serializer entity
------------------------------------------------------------------------------
entity rf_tx_serializer is
  port (
    rf_tx_serializer_clk         : in  std_logic;  -- RF module supplies it
    rf_tx_serializer_arst        : in  std_logic;  -- FPGA reset
    -- fpga interface
    rf_tx_serializer_en          : in  std_logic;  -- to enable/disable module
    rf_tx_serializer_din_valid   : in  std_logic;  -- flag indicating valid data is ready to
                                                   -- read
    rf_tx_serializer_rd_en       : out std_logic;  -- data read request
    rf_tx_serializer_din         : in  std_logic_vector(27 downto 0);  -- data of the sample to send
    -- rf interface
    rf_tx_serializer_dout_vector : out std_logic_vector(1 downto 0)  -- serialized bit of the sample
    );
end entity rf_tx_serializer;

------------------------------------------------------------------------------
-- RF TX serializer behavior
------------------------------------------------------------------------------
architecture behavior of rf_tx_serializer is
  signal internal_warm_up               : std_logic;
  signal internal_sample_i_q_valid      : std_logic;
  signal internal_fifo_rd_en            : std_logic;
  signal internal_shift_register        : std_logic_vector(31 downto 0);
  signal internal_counter               : unsigned(3 downto 0);
  constant i_sync                       : std_logic_vector(1 downto 0) := "10";
  constant q_sync                       : std_logic_vector(1 downto 0) := "01";
  signal internal_enable_shift_register : unsigned(2 downto 0);
  signal enable_sync                    : std_logic;
begin
  ------------------------------------------------------------------------------
  -- Component outputs updating
  ------------------------------------------------------------------------------
  internal_sample_i_q_valid    <= rf_tx_serializer_din_valid;
  rf_tx_serializer_rd_en       <= internal_fifo_rd_en;
  rf_tx_serializer_dout_vector <= internal_shift_register(31) & internal_shift_register(30);
  ------------------------------------------------------------------------------
  -- Internal component signals binding
  ------------------------------------------------------------------------------
  enable_sync                  <= rf_tx_serializer_en;

-- The counter is incremented after each clk event (double data rate),
-- a bit is sent for each counter value.
  serializer_counter : process(rf_tx_serializer_arst, rf_tx_serializer_clk, enable_sync)
  begin
    if rf_tx_serializer_arst = '1' then
      internal_counter <= (others => '0');
      internal_warm_up <= '0';
    else
      if rf_tx_serializer_clk'event and rf_tx_serializer_clk = '1' then
        if enable_sync = '1' then
          -- module is active
          case internal_counter is
            when x"F" =>
              internal_counter <= (others => '0');
              internal_warm_up <= '1';
            when others =>
              internal_counter <= internal_counter + 1;
              internal_warm_up <= internal_warm_up;
          end case;
        else
          -- module is sleeping
          internal_counter <= (others => '0');
          internal_warm_up <= '0';
        end if;
      end if;
    end if;
  end process serializer_counter;



-- Serialize bit on each clk event
  serializer : process (rf_tx_serializer_arst, rf_tx_serializer_clk) is
  begin
    if rf_tx_serializer_arst = '1' then
      internal_fifo_rd_en     <= '0';
      internal_shift_register <= (others => '0');
    else
      if rf_tx_serializer_clk'event and rf_tx_serializer_clk = '1' then
        if internal_warm_up = '1' then
          if enable_sync = '1' then
            -- serialize I data
            case internal_counter is
              -- get a new sample after 16 cycles to send 32 zeros to chip RF at
              -- the start
              when x"F" =>
                if internal_sample_i_q_valid = '1' then
                  internal_fifo_rd_en     <= '1';
                  internal_shift_register <= i_sync & rf_tx_serializer_din(27 downto 14) & q_sync & rf_tx_serializer_din(13 downto 0);
                else
                  internal_fifo_rd_en     <= '0';
                  internal_shift_register <= (others => '0');
                end if;
              when others =>
                internal_fifo_rd_en     <= '0';
                internal_shift_register <= internal_shift_register(29 downto 0) & "00";
            end case;
          else
            internal_fifo_rd_en     <= '0';
            internal_shift_register <= (others => '0');
          end if;
        else
          internal_fifo_rd_en     <= '0';
          internal_shift_register <= (others => '0');
        end if;
      end if;
    end if;
  end process serializer;

end behavior;
