----------------------------------------------------------------------------------
-- Company: Asygn
-- 
-- Create Date: 05/14/2014 04:01:59 PM
-- Design Name: dpodL
-- Module Name: generic_spi_master
-- Description: generic spi master module
-- 
-- Revision:
-- Revision 1 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief Generic SPI master
--! @details
--! User is writing SPI output sequence to TX FIFO. This module is driving MOSI, CLK and CS
--! and decoding MISO data to RX FIFO for user read access
--------------------------------------------------------------------------------

entity generic_spi_slave is
  
  generic (
    FIFO_SIZE_NBBIT     : integer := 8;  --! fifo depth, size=2^FIFO_SIZE_NBBIT
    TRANSACTION_NBBYTES : integer := 1   --! transaction nb bytes
    );
  port (
    clk           : in  std_logic;       --! clock
    rstb          : in  std_logic;       --! async reset (active low)
    -- control interface
    data_wr       : in  std_logic;       --! user fifo write 
    data_in       : in  std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);  --!user data write 
    data_rd       : in  std_logic;       --! user fifo read
    data_out      : out std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);  --!user data read
    rx_full_flag  : out std_logic;
    rx_empty_flag : out std_logic;
    rx_thres_flag : out std_logic;
    rx_data_count : out std_logic_vector(9 downto 0);  --!user data read
    tx_full_flag  : out std_logic;
    tx_empty_flag : out std_logic;
    tx_thres_flag : out std_logic;
    tx_data_count : out std_logic_vector(9 downto 0);  --!user data read
    rx_fifo_rst   : in  std_logic;
    tx_fifo_rst   : in  std_logic;
    -- SPI interface itself
    SPI_CLK       : in  std_logic;       --! SPI clock
    SPI_CS        : in  std_logic;       --! SPI Chip Select
    SPI_MOSI      : in  std_logic;       --! SPI MOSI
    SPI_MISO      : out std_logic        --! SPI MISO
    );

end entity generic_spi_slave;

architecture rtl of generic_spi_slave is
  type spi_stm is (st_reset, st_idle, st_wait_sclk_rising_edge, st_rd, st_spi_seq, st_latch);
  signal st_current, st_next : spi_stm;
  signal output_shift_reg    : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);
  signal input_shift_reg     : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);
  signal ctrl_shift_reg      : std_logic_vector(TRANSACTION_NBBYTES*8 downto 0);
  -- fifo(s) interface(s)
  signal TX_buffer_data      : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);
  signal TX_buffer_rd        : std_logic;
  signal RX_buffer_data      : std_logic_vector(TRANSACTION_NBBYTES*8-1 downto 0);
  signal RX_buffer_wr        : std_logic;
  -- others
  signal rx_rst              : std_logic;
  signal tx_rst              : std_logic;
  -- sync
  signal spi_clk_q1          : std_logic;
  signal spi_clk_q2          : std_logic;
  signal spi_clk_q3          : std_logic;
  signal spi_clk_fallingedge : std_logic;
  signal spi_clk_risingedge  : std_logic;
begin  -- architecture rtl
  ------------------------------------------------------------------------------
  -- sync
  ------------------------------------------------------------------------------
  sync_spi_p : process (clk, rstb) is
  begin  -- process sync_spi_p
    if rstb = '0' then                  -- asynchronous reset (active low)
      spi_clk_q1 <= '0';
      spi_clk_q2 <= '0';
      spi_clk_q3 <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      spi_clk_q1 <= spi_clk;
      spi_clk_q2 <= spi_clk_q1;
      spi_clk_q3 <= spi_clk_q2;
    end if;
  end process sync_spi_p;
  spi_clk_fallingedge <= '1' when (spi_clk_q1 = '0' and spi_clk_q2 = '1') else
                         '0';
  spi_clk_risingedge <= '1' when (spi_clk_q1 = '1' and spi_clk_q2 = '0') else
                        '0';
  ------------------------------------------------------------------------------
  -- SPI IO fifos
  ------------------------------------------------------------------------------
  rx_rst <= (not rstb) or rx_fifo_rst;
  rx_fifo_8b : entity work.fifo_8b
    port map (
      clk        => clk,
      rst        => rx_rst,
      din        => RX_buffer_data,
      wr_en      => RX_buffer_wr,
      rd_en      => data_rd,
      dout       => data_out,
      full       => rx_full_flag,
      empty      => rx_empty_flag,
      prog_full  => rx_thres_flag,
      data_count => rx_data_count);

  tx_rst <= (not rstb) or tx_fifo_rst;
  tx_fifo_8b : entity work.fifo_8b
    port map (
      clk        => clk,
      rst        => tx_rst,
      din        => data_in,
      wr_en      => data_wr,
      rd_en      => TX_buffer_rd,
      dout       => TX_buffer_data,
      full       => tx_full_flag,
      empty      => tx_empty_flag,
      prog_full  => tx_thres_flag,
      data_count => tx_data_count);
  ------------------------------------------------------------------------------
  -- SPI state machine
  ------------------------------------------------------------------------------
  state_m : process(rstb,st_current, ctrl_shift_reg, SPI_CS, spi_clk_risingedge)
  begin
    if rstb = '0' then
      st_next <= st_reset;
    else
      case st_current is
        when st_reset =>
          st_next <= st_idle;
        when st_idle =>
          if SPI_CS = '0' then  -- wait until CS goes low
            st_next <= st_wait_sclk_rising_edge;
          else
            st_next <= st_idle;
          end if;
        when st_wait_sclk_rising_edge =>
          if spi_clk_risingedge = '1' then  -- wait for SCLK rising edge
            st_next <= st_rd;
          else
            st_next <= st_wait_sclk_rising_edge;
          end if;
        when st_rd =>
          st_next <= st_spi_seq;
        when st_spi_seq =>
          if ctrl_shift_reg(TRANSACTION_NBBYTES*8) = '0' then  -- wait until
                                                               -- frame end
            st_next <= st_spi_seq;
          else
            st_next <= st_latch;
          end if;
        when st_latch =>
          st_next <= st_idle;
        when others =>
          st_next <= st_reset;
      end case;
    end if;
  end process;

  -- purpose: state machine sync
  -- type   : sequential
  -- inputs : clk, rstb, st_next
  -- outputs: st_current
  stm_sync_p : process (clk, rstb) is
  begin  -- process stm_sync_p
    if rstb = '0' then        -- asynchronous reset (active low)
      st_current <= st_reset;
    elsif clk'event and clk = '1' then                    -- rising clock edge
      st_current <= st_next;
    end if;
  end process stm_sync_p;
  ------------------------------------------------------------------------------
  -- SPI core
  ------------------------------------------------------------------------------
  -- purpose: spi TX and RX
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  spi_core_p : process (clk, rstb) is
  begin  -- process spi_core_p
    if rstb = '0' then        -- asynchronous reset (active low)
      ctrl_shift_reg <= std_logic_vector(to_unsigned(1, TRANSACTION_NBBYTES*8+1));
      -- RX
      RX_buffer_data <= (others => '0');
      RX_buffer_wr   <= '0';
    elsif clk'event and clk = '1' then                    -- rising clock edge
      if st_current = st_spi_seq then
        if spi_clk_fallingedge = '1' then
          ctrl_shift_reg <= ctrl_shift_reg(TRANSACTION_NBBYTES*8-1 downto 0) & '1';
        else
          ctrl_shift_reg <= ctrl_shift_reg;
        end if;
      elsif st_current = st_latch then
        ctrl_shift_reg <= std_logic_vector(to_unsigned(1, TRANSACTION_NBBYTES*8+1));
        RX_buffer_wr   <= '1';
        RX_buffer_data <= input_shift_reg(TRANSACTION_NBBYTES*8-1 downto 0);
      else
        RX_buffer_wr   <= '0';
        ctrl_shift_reg <= std_logic_vector(to_unsigned(1, TRANSACTION_NBBYTES*8+1));
      end if;
    end if;
  end process spi_core_p;
  -- purpose: tx process
  -- type   : sequential
  -- inputs : spi_clkq, rstb
  -- outputs: 
  spi_tx_p : process (clk, rstb) is
  begin  -- process spi_tx_p
    if rstb = '0' then
      output_shift_reg <= (others => '0');--TX_buffer_data;
    elsif clk'event and clk = '0' then                    -- rising clock edge
      if st_current = st_wait_sclk_rising_edge then  -- asynchronous reset (active low)
        output_shift_reg <= TX_buffer_data;
      elsif spi_clk_fallingedge = '1' then
        output_shift_reg <= output_shift_reg(TRANSACTION_NBBYTES*8-2 downto 0) & '0';
      else
        output_shift_reg <= output_shift_reg;
      end if;
    end if;
  end process spi_tx_p;
  -- purpose: rx process
  -- type   : sequential
  -- inputs : spi_clkq, rstb
  -- outputs: 
  spi_rx_p : process (clk, rstb) is
  begin  -- process spi_rx_p
    if rstb = '0' then        -- asynchronous reset (active low)
      input_shift_reg <= (others => '0');
    elsif clk'event and clk = '1' then                    -- rising clock edge
      if spi_clk_risingedge = '1' then
        input_shift_reg <= input_shift_reg(TRANSACTION_NBBYTES*8-2 downto 0) & SPI_MOSI;
      else
        input_shift_reg <= input_shift_reg;
      end if;
    end if;
  end process spi_rx_p;
  -- to output
  SPI_MISO <= output_shift_reg(TRANSACTION_NBBYTES*8-1);

  TX_buffer_rd <= '1' when (st_current = st_rd) else
                  '0';
end architecture rtl;
