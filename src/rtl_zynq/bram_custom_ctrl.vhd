
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bram_custom_ctrl is
  port(
    clk_200                  : in  std_logic;
    clk_64                   : in  std_logic;
    resetb                   : in  std_logic;
    ctrl_acquisition_en      : in  std_logic;
    ctrl_overflow_flag_clear : in  std_logic;  -- oneshot !
    ctrl_overflow_flag       : out std_logic;
    ctrl_irq_clear           : in  std_logic;  -- oneshot !
    ctrl_irq                 : out std_logic;
    ctrl_polarity            : out std_logic;
    ctrl_packet_timestamp    : out std_logic_vector(25 downto 0);
    ctrl_cdma_done           : in  std_logic;  -- oneshot !
    ctrl_bram_fill_threshold : in  std_logic_vector(21 downto 0);
    atmel_serial_data_0      : in  std_logic;
    atmel_serial_data_1      : in  std_logic;
    emulator_en              : in  std_logic;
    serdes_force_emulator    : in  std_logic;
    serdes_sync_flag         : out std_logic;
    BRAM_CLK                 : out std_logic;
    BRAM_Addr                : out std_logic_vector(31 downto 0);
    BRAM_WrData              : out std_logic_vector(31 downto 0);
    BRAM_WE                  : out std_logic_vector(3 downto 0);
    BRAM_rst                 : out std_logic;
    BRAM_En                  : out std_logic;
    serdes_fsm_state         : out std_logic_vector(1 downto 0));
end entity;


architecture rtl of bram_custom_ctrl is

  signal atmel_read_data     : std_logic_vector(31 downto 0);
  signal atmel_read_data_rdy : std_logic;
  signal emul_read_data      : std_logic_vector(31 downto 0);
  signal emul_read_data_rdy  : std_logic;
  signal read_data           : std_logic_vector(31 downto 0);
  signal read_data_rdy       : std_logic;
  signal emulator_en_resync  : std_logic_vector(2 downto 0);

begin

  bram_ctrl_fsm_sync_p : process(clk_64, resetb)
  begin
    if resetb = '0' then
      emulator_en_resync <= (others => '0');
    elsif clk_64'event and clk_64 = '1' then
      emulator_en_resync <= emulator_en_resync(1 downto 0) & emulator_en;
    end if;
  end process;

  emul : entity work.atmel_model(rtl)
    port map(
      clk                => clk_64,
      resetb             => resetb,
      model_en           => emulator_en_resync(2),
      emul_read_data     => emul_read_data,
      emul_read_data_rdy => emul_read_data_rdy);

  atmel_serdes : entity work.atmel_rx_serdes(rtl)
    port map(
      resetb         => resetb,
      atmel_clk      => clk_64,
      atmel_data_0   => atmel_serial_data_0,
      atmel_data_1   => atmel_serial_data_1,
      force_emulator => serdes_force_emulator,
      read_data      => atmel_read_data,
      read_data_rdy  => atmel_read_data_rdy,
      sync_flag      => serdes_sync_flag,
      fsm_state      => serdes_fsm_state);

  read_data     <= emul_read_data     when (emulator_en_resync(2) = '1') else atmel_read_data;
  read_data_rdy <= emul_read_data_rdy when (emulator_en_resync(2) = '1') else atmel_read_data_rdy;

  bram_ctrl_inst : entity work.bram_ctrl(rtl)
    port map(
      clk                      => clk_200,
      resetb                   => resetb,
      ctrl_acquisition_en      => ctrl_acquisition_en,
      ctrl_overflow_flag_clear => ctrl_overflow_flag_clear,
      ctrl_overflow_flag       => ctrl_overflow_flag,
      ctrl_irq_clear           => ctrl_irq_clear,
      ctrl_irq                 => ctrl_irq,
      ctrl_polarity            => ctrl_polarity,
      ctrl_packet_timestamp    => ctrl_packet_timestamp,
      ctrl_cdma_done           => ctrl_cdma_done,
      ctrl_bram_fill_threshold => ctrl_bram_fill_threshold,
      read_data                => read_data,
      read_data_rdy            => read_data_rdy,
      BRAM_CLK                 => BRAM_CLK,
      BRAM_Addr                => BRAM_Addr,
      BRAM_WrData              => BRAM_WrData,
      BRAM_WE                  => BRAM_WE,
      BRAM_rst                 => BRAM_rst,
      BRAM_En                  => BRAM_En);

end rtl;
