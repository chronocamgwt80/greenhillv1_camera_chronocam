
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity atmel_model is
  port(
    clk                : in  std_logic;
    resetb             : in  std_logic;
    model_en           : in  std_logic;
    emul_read_data     : out std_logic_vector(31 downto 0);
    emul_read_data_rdy : out std_logic);
end entity;

architecture rtl of atmel_model is

  signal emul_read_data_internal : unsigned(31 downto 0);
  signal internal_cnt            : unsigned(4 downto 0);
  
begin

  emul_read_data     <= std_logic_vector(emul_read_data_internal);
  emul_read_data_rdy <= not(internal_cnt(4));

  emul_p : process(clk, resetb)
  begin
    if resetb = '0' then
      emul_read_data_internal <= (others => '0');
      internal_cnt            <= (others => '0');
    elsif clk'event and clk = '1' then


      if model_en = '1' then
        
        internal_cnt <= internal_cnt +1;

        if (internal_cnt = "11111") then
          emul_read_data_internal <= emul_read_data_internal +1;
        else
          emul_read_data_internal <= emul_read_data_internal;
        end if;

      else

        emul_read_data_internal <= (others => '0');
        internal_cnt            <= (others => '0');

      end if;
      
    end if;
  end process;



  
end rtl;
