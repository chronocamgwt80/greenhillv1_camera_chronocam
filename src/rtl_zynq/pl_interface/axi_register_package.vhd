library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package axi_register_package is

  -- Output signals bus :
  type register_bus_out is record
    rf_tx_fifo_rst : std_logic;
    rf_tx_fifo_write_en : std_logic;
    rf_tx_fifo_din_q : std_logic_vector(13 downto 0);
    rf_tx_fifo_din_i : std_logic_vector(13 downto 0);
    rf_tx_controller_rst : std_logic;
    rf_tx_controller_enable : std_logic;
    rf_tx_loop_burst_mode : std_logic;
    rf_tx_controller_constant : std_logic_vector(27 downto 0);
    rf_rx_serdes_force_emulator : std_logic;
    config_spi_rx_fifo_reset : std_logic;
    config_spi_tx_fifo_reset : std_logic;
    config_spi_data_wr : std_logic_vector(7 downto 0);
    config_spi_wr : std_logic;
    bram_resetb : std_logic;
    bram_acquisition_en : std_logic;
    bram_overflow_flag_clear : std_logic;
    bram_irq_clear : std_logic;
    cdma_transfert_done : std_logic;
    bram_fill_threshold : std_logic_vector(21 downto 0);
    emulator_en : std_logic;
    gw_power_user_leds : std_logic_vector(3 downto 0);
    ext_spi_ctrl_reset : std_logic;
    ext_spi_byte_cntr : std_logic_vector(7 downto 0);
    ext_spi_cr_rst : std_logic;
    ext_spi_cr_cpol : std_logic;
    ext_spi_cr_cpha : std_logic;
    ext_spi_cr_endian : std_logic;
    ext_spi_cr_send_frame : std_logic;
    ext_spi_cr_clk_divider : std_logic_vector(7 downto 0);
    ext_spi_cr_ss_sel : std_logic_vector(7 downto 0);
    ext_spi_fifo_in_data : std_logic_vector(23 downto 0);
    ext_spi_fifo_in_wr_en : std_logic;
    ext_spi_fifo_loopback : std_logic;
    config_spi_rd_fifo_en : std_logic;
    ext_spi_fifo_out_fifo_en : std_logic;
  end record;

  -- Input signals bus :
  type register_bus_in is record
    FIRMWARE_VERSION : std_logic_vector(31 downto 0);
    rf_tx_fifo_empty_flag : std_logic;
    rf_tx_fifo_full_flag : std_logic;
    rf_tx_buffer_empty_flag : std_logic;
    rf_tx_buffer_full_flag : std_logic;
    rf_tx_buffer_data_count : std_logic_vector(17 downto 0);
    rf_rx_serdes_sync_flag : std_logic;
    config_spi_rx_full_flag : std_logic;
    config_spi_rx_empty_flag : std_logic;
    config_spi_rx_thres_flag : std_logic;
    config_spi_rx_data_count : std_logic_vector(9 downto 0);
    config_spi_tx_full_flag : std_logic;
    config_spi_tx_empty_flag : std_logic;
    config_spi_tx_thres_flag : std_logic;
    config_spi_tx_data_count : std_logic_vector(9 downto 0);
    config_spi_data_rd : std_logic_vector(7 downto 0);
    bram_overflow_flag : std_logic;
    bram_polarity : std_logic;
    bram_packet_timestamp : std_logic_vector(25 downto 0);
    ext_spi_status_tx_end : std_logic;
    ext_spi_cr_fifo_in_empty : std_logic;
    ext_spi_cr_fifo_in_full : std_logic;
    ext_spi_cr_fifo_out_empty : std_logic;
    ext_spi_cr_ss_busy : std_logic_vector(7 downto 0);
    ext_spi_fifo_out_data : std_logic_vector(23 downto 0);
  end record;

end axi_register_package;