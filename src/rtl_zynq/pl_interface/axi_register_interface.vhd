library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_register_package.all;
use work.axi_register_constant.all;

entity axi_register_interface is
    port(
       -- Axis BUS interface --
       S_AXI_ACLK    : in  std_logic;                                           -- Global Clock Signal
       S_AXI_ARESETN : in  std_logic;                                           -- Global Reset Signal. This Signal is Active LOW
       S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);     -- Write address (issued by master, acceped by Slave)
       S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);                        -- Write channel Protection type. 
       S_AXI_AWVALID : in  std_logic;                                           -- Write address valid.
       S_AXI_AWREADY : out std_logic;                                           -- Write address ready.
       S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);     -- Write data (issued by master, acceped by Slave) 
       S_AXI_WSTRB   : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0); -- Write strobes.
       S_AXI_WVALID  : in  std_logic;                                           -- Write valid
       S_AXI_WREADY  : out std_logic;                                           -- Write ready. 
       S_AXI_BRESP   : out std_logic_vector(1 downto 0);                        -- Write response.
       S_AXI_BVALID  : out std_logic;                                           -- Write response valid.
       S_AXI_BREADY  : in  std_logic;                                           -- Response ready.
       S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);     -- Read address (issued by master, acceped by Slave)
       S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);                        -- Protection type.
       S_AXI_ARVALID : in  std_logic;                                           -- Read address valid.
       S_AXI_ARREADY : out std_logic;                                           -- Read address ready.
       S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);     -- Read data (issued by slave)
       S_AXI_RRESP   : out std_logic_vector(1 downto 0);                        -- Read response. 
       S_AXI_RVALID  : out std_logic;                                           -- Read valid.
       S_AXI_RREADY  : in  std_logic;                                           -- Read ready.

       -- Registers interface --
       axi_reg_out   : out register_bus_out;
       axi_reg_in    : in  register_bus_in
        );
end axi_register_interface;

architecture architecture_axi_register_interface of axi_register_interface is

    -- AXI4LITE signals
    signal axi_awaddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_awready : std_logic;
    signal axi_wready  : std_logic;
    signal axi_bresp   : std_logic_vector(1 downto 0);
    signal axi_bvalid  : std_logic;
    signal axi_araddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_arready : std_logic;
    signal axi_rdata   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal axi_rresp   : std_logic_vector(1 downto 0);
    signal axi_rvalid  : std_logic;

    -- Internal buf, used for write sequence 
    signal FIRMWARE_VERSION_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rf_tx_fifo_ctrl_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rf_tx_fifo_status_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rf_tx_buffer_status_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rf_tx_controller_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rf_rx_controller_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal config_spi_status_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal config_spi_config_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal config_spi_wr_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal config_spi_rd_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal BRAM_CTRL_REG_0_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal BRAM_CTRL_REG_1_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal GW_POWER_USER_LEDS_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal EXT_SPI_CTRL_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal EXT_SPI_CR_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal EXT_SPI_FIFO_IN_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal EXT_SPI_FIFO_OUT_reg_buf : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);

    -- Internal signals 
    signal FIRMWARE_VERSION_internal : std_logic_vector(31 downto 0);
    signal rf_tx_fifo_rst_internal : std_logic;
    signal rf_tx_fifo_write_en_internal : std_logic;
    signal rf_tx_fifo_din_q_internal : std_logic_vector(13 downto 0);
    signal rf_tx_fifo_din_i_internal : std_logic_vector(13 downto 0);
    signal rf_tx_fifo_empty_flag_internal : std_logic;
    signal rf_tx_fifo_full_flag_internal : std_logic;
    signal rf_tx_buffer_empty_flag_internal : std_logic;
    signal rf_tx_buffer_full_flag_internal : std_logic;
    signal rf_tx_buffer_data_count_internal : std_logic_vector(17 downto 0);
    signal rf_tx_controller_rst_internal : std_logic;
    signal rf_tx_controller_enable_internal : std_logic;
    signal rf_tx_loop_burst_mode_internal : std_logic;
    signal rf_tx_controller_constant_internal : std_logic_vector(27 downto 0);
    signal rf_rx_serdes_force_emulator_internal : std_logic;
    signal rf_rx_serdes_sync_flag_internal : std_logic;
    signal config_spi_rx_full_flag_internal : std_logic;
    signal config_spi_rx_empty_flag_internal : std_logic;
    signal config_spi_rx_thres_flag_internal : std_logic;
    signal config_spi_rx_data_count_internal : std_logic_vector(9 downto 0);
    signal config_spi_tx_full_flag_internal : std_logic;
    signal config_spi_tx_empty_flag_internal : std_logic;
    signal config_spi_tx_thres_flag_internal : std_logic;
    signal config_spi_tx_data_count_internal : std_logic_vector(9 downto 0);
    signal config_spi_rx_fifo_reset_internal : std_logic;
    signal config_spi_tx_fifo_reset_internal : std_logic;
    signal config_spi_data_wr_internal : std_logic_vector(7 downto 0);
    signal config_spi_wr_internal : std_logic;
    signal config_spi_data_rd_internal : std_logic_vector(7 downto 0);
    signal bram_resetb_internal : std_logic;
    signal bram_acquisition_en_internal : std_logic;
    signal bram_overflow_flag_clear_internal : std_logic;
    signal bram_overflow_flag_internal : std_logic;
    signal bram_irq_clear_internal : std_logic;
    signal bram_polarity_internal : std_logic;
    signal bram_packet_timestamp_internal : std_logic_vector(25 downto 0);
    signal cdma_transfert_done_internal : std_logic;
    signal bram_fill_threshold_internal : std_logic_vector(21 downto 0);
    signal emulator_en_internal : std_logic;
    signal gw_power_user_leds_internal : std_logic_vector(3 downto 0);
    signal ext_spi_ctrl_reset_internal : std_logic;
    signal ext_spi_status_tx_end_internal : std_logic;
    signal ext_spi_byte_cntr_internal : std_logic_vector(7 downto 0);
    signal ext_spi_cr_rst_internal : std_logic;
    signal ext_spi_cr_cpol_internal : std_logic;
    signal ext_spi_cr_cpha_internal : std_logic;
    signal ext_spi_cr_endian_internal : std_logic;
    signal ext_spi_cr_fifo_in_empty_internal : std_logic;
    signal ext_spi_cr_fifo_in_full_internal : std_logic;
    signal ext_spi_cr_fifo_out_empty_internal : std_logic;
    signal ext_spi_cr_send_frame_internal : std_logic;
    signal ext_spi_cr_clk_divider_internal : std_logic_vector(7 downto 0);
    signal ext_spi_cr_ss_sel_internal : std_logic_vector(7 downto 0);
    signal ext_spi_cr_ss_busy_internal : std_logic_vector(7 downto 0);
    signal ext_spi_fifo_in_data_internal : std_logic_vector(23 downto 0);
    signal ext_spi_fifo_in_wr_en_internal : std_logic;
    signal ext_spi_fifo_loopback_internal : std_logic;
    signal ext_spi_fifo_out_data_internal : std_logic_vector(23 downto 0);

    signal config_spi_rd_fifo_en : std_logic;
    signal ext_spi_fifo_out_fifo_en : std_logic;

    signal slv_reg_rden : std_logic;
    signal slv_reg_wren : std_logic;

begin

    -- I/O Connections assignments
    S_AXI_AWREADY <= axi_awready;
    S_AXI_WREADY  <= axi_wready;
    S_AXI_BRESP   <= axi_bresp;
    S_AXI_BVALID  <= axi_bvalid;
    S_AXI_ARREADY <= axi_arready;
    S_AXI_RDATA   <= axi_rdata;
    S_AXI_RRESP   <= axi_rresp;
    S_AXI_RVALID  <= axi_rvalid;

    -- IOs to internal signals
    FIRMWARE_VERSION_internal <= axi_reg_in.FIRMWARE_VERSION;
    rf_tx_fifo_empty_flag_internal <= axi_reg_in.rf_tx_fifo_empty_flag;
    rf_tx_fifo_full_flag_internal <= axi_reg_in.rf_tx_fifo_full_flag;
    rf_tx_buffer_empty_flag_internal <= axi_reg_in.rf_tx_buffer_empty_flag;
    rf_tx_buffer_full_flag_internal <= axi_reg_in.rf_tx_buffer_full_flag;
    rf_tx_buffer_data_count_internal <= axi_reg_in.rf_tx_buffer_data_count;
    rf_rx_serdes_sync_flag_internal <= axi_reg_in.rf_rx_serdes_sync_flag;
    config_spi_rx_full_flag_internal <= axi_reg_in.config_spi_rx_full_flag;
    config_spi_rx_empty_flag_internal <= axi_reg_in.config_spi_rx_empty_flag;
    config_spi_rx_thres_flag_internal <= axi_reg_in.config_spi_rx_thres_flag;
    config_spi_rx_data_count_internal <= axi_reg_in.config_spi_rx_data_count;
    config_spi_tx_full_flag_internal <= axi_reg_in.config_spi_tx_full_flag;
    config_spi_tx_empty_flag_internal <= axi_reg_in.config_spi_tx_empty_flag;
    config_spi_tx_thres_flag_internal <= axi_reg_in.config_spi_tx_thres_flag;
    config_spi_tx_data_count_internal <= axi_reg_in.config_spi_tx_data_count;
    config_spi_data_rd_internal <= axi_reg_in.config_spi_data_rd;
    bram_overflow_flag_internal <= axi_reg_in.bram_overflow_flag;
    bram_polarity_internal <= axi_reg_in.bram_polarity;
    bram_packet_timestamp_internal <= axi_reg_in.bram_packet_timestamp;
    ext_spi_status_tx_end_internal <= axi_reg_in.ext_spi_status_tx_end;
    ext_spi_cr_fifo_in_empty_internal <= axi_reg_in.ext_spi_cr_fifo_in_empty;
    ext_spi_cr_fifo_in_full_internal <= axi_reg_in.ext_spi_cr_fifo_in_full;
    ext_spi_cr_fifo_out_empty_internal <= axi_reg_in.ext_spi_cr_fifo_out_empty;
    ext_spi_cr_ss_busy_internal <= axi_reg_in.ext_spi_cr_ss_busy;
    ext_spi_fifo_out_data_internal <= axi_reg_in.ext_spi_fifo_out_data;

    -- Internal signals to IOs
    axi_reg_out.rf_tx_fifo_rst <= rf_tx_fifo_rst_internal;
    axi_reg_out.rf_tx_fifo_write_en <= rf_tx_fifo_write_en_internal;
    axi_reg_out.rf_tx_fifo_din_q <= rf_tx_fifo_din_q_internal;
    axi_reg_out.rf_tx_fifo_din_i <= rf_tx_fifo_din_i_internal;
    axi_reg_out.rf_tx_controller_rst <= rf_tx_controller_rst_internal;
    axi_reg_out.rf_tx_controller_enable <= rf_tx_controller_enable_internal;
    axi_reg_out.rf_tx_loop_burst_mode <= rf_tx_loop_burst_mode_internal;
    axi_reg_out.rf_tx_controller_constant <= rf_tx_controller_constant_internal;
    axi_reg_out.rf_rx_serdes_force_emulator <= rf_rx_serdes_force_emulator_internal;
    axi_reg_out.config_spi_rx_fifo_reset <= config_spi_rx_fifo_reset_internal;
    axi_reg_out.config_spi_tx_fifo_reset <= config_spi_tx_fifo_reset_internal;
    axi_reg_out.config_spi_data_wr <= config_spi_data_wr_internal;
    axi_reg_out.config_spi_wr <= config_spi_wr_internal;
    axi_reg_out.bram_resetb <= bram_resetb_internal;
    axi_reg_out.bram_acquisition_en <= bram_acquisition_en_internal;
    axi_reg_out.bram_overflow_flag_clear <= bram_overflow_flag_clear_internal;
    axi_reg_out.bram_irq_clear <= bram_irq_clear_internal;
    axi_reg_out.cdma_transfert_done <= cdma_transfert_done_internal;
    axi_reg_out.bram_fill_threshold <= bram_fill_threshold_internal;
    axi_reg_out.emulator_en <= emulator_en_internal;
    axi_reg_out.gw_power_user_leds <= gw_power_user_leds_internal;
    axi_reg_out.ext_spi_ctrl_reset <= ext_spi_ctrl_reset_internal;
    axi_reg_out.ext_spi_byte_cntr <= ext_spi_byte_cntr_internal;
    axi_reg_out.ext_spi_cr_rst <= ext_spi_cr_rst_internal;
    axi_reg_out.ext_spi_cr_cpol <= ext_spi_cr_cpol_internal;
    axi_reg_out.ext_spi_cr_cpha <= ext_spi_cr_cpha_internal;
    axi_reg_out.ext_spi_cr_endian <= ext_spi_cr_endian_internal;
    axi_reg_out.ext_spi_cr_send_frame <= ext_spi_cr_send_frame_internal;
    axi_reg_out.ext_spi_cr_clk_divider <= ext_spi_cr_clk_divider_internal;
    axi_reg_out.ext_spi_cr_ss_sel <= ext_spi_cr_ss_sel_internal;
    axi_reg_out.ext_spi_fifo_in_data <= ext_spi_fifo_in_data_internal;
    axi_reg_out.ext_spi_fifo_in_wr_en <= ext_spi_fifo_in_wr_en_internal;
    axi_reg_out.ext_spi_fifo_loopback <= ext_spi_fifo_loopback_internal;
    axi_reg_out.config_spi_rd_fifo_en <= config_spi_rd_fifo_en;
    axi_reg_out.ext_spi_fifo_out_fifo_en <= ext_spi_fifo_out_fifo_en;

    -- Write buf decoding
    rf_tx_fifo_rst_internal <= rf_tx_fifo_ctrl_reg_buf (0);
    rf_tx_fifo_write_en_internal <= rf_tx_fifo_ctrl_reg_buf (1);
    rf_tx_fifo_din_q_internal <= rf_tx_fifo_ctrl_reg_buf (15 downto 2);
    rf_tx_fifo_din_i_internal <= rf_tx_fifo_ctrl_reg_buf (29 downto 16);
    rf_tx_controller_rst_internal <= rf_tx_controller_reg_buf (0);
    rf_tx_controller_enable_internal <= rf_tx_controller_reg_buf (1);
    rf_tx_loop_burst_mode_internal <= rf_tx_controller_reg_buf (2);
    rf_tx_controller_constant_internal <= rf_tx_controller_reg_buf (30 downto 3);
    rf_rx_serdes_force_emulator_internal <= rf_rx_controller_reg_buf (0);
    config_spi_rx_fifo_reset_internal <= config_spi_config_reg_buf (0);
    config_spi_tx_fifo_reset_internal <= config_spi_config_reg_buf (1);
    config_spi_data_wr_internal <= config_spi_wr_reg_buf (7 downto 0);
    config_spi_wr_internal <= config_spi_wr_reg_buf (8);
    bram_resetb_internal <= BRAM_CTRL_REG_0_reg_buf (0);
    bram_acquisition_en_internal <= BRAM_CTRL_REG_0_reg_buf (1);
    bram_overflow_flag_clear_internal <= BRAM_CTRL_REG_0_reg_buf (2);
    bram_irq_clear_internal <= BRAM_CTRL_REG_0_reg_buf (4);
    cdma_transfert_done_internal <= BRAM_CTRL_REG_1_reg_buf (0);
    bram_fill_threshold_internal <= BRAM_CTRL_REG_1_reg_buf (25 downto 4);
    emulator_en_internal <= BRAM_CTRL_REG_1_reg_buf (26);
    gw_power_user_leds_internal <= GW_POWER_USER_LEDS_reg_buf (3 downto 0);
    ext_spi_ctrl_reset_internal <= EXT_SPI_CTRL_reg_buf (0);
    ext_spi_byte_cntr_internal <= EXT_SPI_CTRL_reg_buf (23 downto 16);
    ext_spi_cr_rst_internal <= EXT_SPI_CR_reg_buf (0);
    ext_spi_cr_cpol_internal <= EXT_SPI_CR_reg_buf (1);
    ext_spi_cr_cpha_internal <= EXT_SPI_CR_reg_buf (2);
    ext_spi_cr_endian_internal <= EXT_SPI_CR_reg_buf (3);
    ext_spi_cr_send_frame_internal <= EXT_SPI_CR_reg_buf (7);
    ext_spi_cr_clk_divider_internal <= EXT_SPI_CR_reg_buf (15 downto 8);
    ext_spi_cr_ss_sel_internal <= EXT_SPI_CR_reg_buf (23 downto 16);
    ext_spi_fifo_in_data_internal <= EXT_SPI_FIFO_IN_reg_buf (23 downto 0);
    ext_spi_fifo_in_wr_en_internal <= EXT_SPI_FIFO_IN_reg_buf (24);
    ext_spi_fifo_loopback_internal <= EXT_SPI_FIFO_IN_reg_buf (28);

    -- Implement axi_awready generation and axi_awaddr latching
    process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
              axi_awaddr <= (others => '0');
              axi_awready <= '0';
            else
              if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1') then
                axi_awaddr <= S_AXI_AWADDR;
                axi_awready <= '1';
              else
                axi_awaddr <= axi_awaddr;
                axi_awready <= '0';
              end if;
            end if;
          end if;
    end process;

    -- Implement axi_wready generation
    process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
              axi_wready <= '0';
            else
          if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1') then
            axi_wready <= '1';
          else
            axi_wready <= '0';
          end if;
        end if;
      end if;
    end process;

----------------------------
-- Write register process
----------------------------

    slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID;

    process (S_AXI_ACLK, S_AXI_ARESETN)
         variable loc_addr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
       begin
         if rising_edge(S_AXI_ACLK) then
           if S_AXI_ARESETN = '0' then
                       FIRMWARE_VERSION_reg_buf <= FIRMWARE_VERSION_REG_DEFAULT ;
                       rf_tx_fifo_ctrl_reg_buf <= RF_TX_FIFO_CTRL_REG_DEFAULT ;
                       rf_tx_fifo_status_reg_buf <= RF_TX_FIFO_STATUS_REG_DEFAULT ;
                       rf_tx_buffer_status_reg_buf <= RF_TX_BUFFER_STATUS_REG_DEFAULT ;
                       rf_tx_controller_reg_buf <= RF_TX_CONTROLLER_REG_DEFAULT ;
                       rf_rx_controller_reg_buf <= RF_RX_CONTROLLER_REG_DEFAULT ;
                       config_spi_status_reg_buf <= CONFIG_SPI_STATUS_REG_DEFAULT ;
                       config_spi_config_reg_buf <= CONFIG_SPI_CONFIG_REG_DEFAULT ;
                       config_spi_wr_reg_buf <= CONFIG_SPI_WR_REG_DEFAULT ;
                       config_spi_rd_reg_buf <= CONFIG_SPI_RD_REG_DEFAULT ;
                       BRAM_CTRL_REG_0_reg_buf <= BRAM_CTRL_REG_0_REG_DEFAULT ;
                       BRAM_CTRL_REG_1_reg_buf <= BRAM_CTRL_REG_1_REG_DEFAULT ;
                       GW_POWER_USER_LEDS_reg_buf <= GW_POWER_USER_LEDS_REG_DEFAULT ;
                       EXT_SPI_CTRL_reg_buf <= EXT_SPI_CTRL_REG_DEFAULT ;
                       EXT_SPI_CR_reg_buf <= EXT_SPI_CR_REG_DEFAULT ;
                       EXT_SPI_FIFO_IN_reg_buf <= EXT_SPI_FIFO_IN_REG_DEFAULT ;
                       EXT_SPI_FIFO_OUT_reg_buf <= EXT_SPI_FIFO_OUT_REG_DEFAULT ;
           else
             loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
             if (slv_reg_wren = '1') then
               case loc_addr is
              when FIRMWARE_VERSION_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       FIRMWARE_VERSION_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when RF_TX_FIFO_CTRL_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       rf_tx_fifo_ctrl_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when RF_TX_FIFO_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       rf_tx_fifo_status_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when RF_TX_BUFFER_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       rf_tx_buffer_status_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when RF_TX_CONTROLLER_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       rf_tx_controller_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when RF_RX_CONTROLLER_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       rf_rx_controller_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when CONFIG_SPI_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       config_spi_status_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when CONFIG_SPI_CONFIG_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       config_spi_config_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when CONFIG_SPI_WR_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       config_spi_wr_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when CONFIG_SPI_RD_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       config_spi_rd_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when BRAM_CTRL_REG_0_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       BRAM_CTRL_REG_0_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when BRAM_CTRL_REG_1_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       BRAM_CTRL_REG_1_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when GW_POWER_USER_LEDS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       GW_POWER_USER_LEDS_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when EXT_SPI_CTRL_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       EXT_SPI_CTRL_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when EXT_SPI_CR_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       EXT_SPI_CR_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when EXT_SPI_FIFO_IN_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       EXT_SPI_FIFO_IN_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
              when EXT_SPI_FIFO_OUT_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) => 
                 for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                   if (S_AXI_WSTRB(byte_index) = '1') then
                       EXT_SPI_FIFO_OUT_reg_buf(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                   end if;
                 end loop;
               when others =>
                       FIRMWARE_VERSION_reg_buf <= FIRMWARE_VERSION_reg_buf ;
                       rf_tx_fifo_ctrl_reg_buf <= rf_tx_fifo_ctrl_reg_buf ;
                       rf_tx_fifo_status_reg_buf <= rf_tx_fifo_status_reg_buf ;
                       rf_tx_buffer_status_reg_buf <= rf_tx_buffer_status_reg_buf ;
                       rf_tx_controller_reg_buf <= rf_tx_controller_reg_buf ;
                       rf_rx_controller_reg_buf <= rf_rx_controller_reg_buf ;
                       config_spi_status_reg_buf <= config_spi_status_reg_buf ;
                       config_spi_config_reg_buf <= config_spi_config_reg_buf ;
                       config_spi_wr_reg_buf <= config_spi_wr_reg_buf ;
                       config_spi_rd_reg_buf <= config_spi_rd_reg_buf ;
                       BRAM_CTRL_REG_0_reg_buf <= BRAM_CTRL_REG_0_reg_buf ;
                       BRAM_CTRL_REG_1_reg_buf <= BRAM_CTRL_REG_1_reg_buf ;
                       GW_POWER_USER_LEDS_reg_buf <= GW_POWER_USER_LEDS_reg_buf ;
                       EXT_SPI_CTRL_reg_buf <= EXT_SPI_CTRL_reg_buf ;
                       EXT_SPI_CR_reg_buf <= EXT_SPI_CR_reg_buf ;
                       EXT_SPI_FIFO_IN_reg_buf <= EXT_SPI_FIFO_IN_reg_buf ;
                       EXT_SPI_FIFO_OUT_reg_buf <= EXT_SPI_FIFO_OUT_reg_buf ;
               end case;
             else
             FIRMWARE_VERSION_reg_buf <= FIRMWARE_VERSION_reg_buf(31 downto 0);
             rf_tx_fifo_ctrl_reg_buf <= "00" & rf_tx_fifo_ctrl_reg_buf(29 downto 16) & rf_tx_fifo_ctrl_reg_buf(15 downto 2) & RF_TX_FIFO_CTRL_REG_DEFAULT(1) & rf_tx_fifo_ctrl_reg_buf(0);
             rf_tx_fifo_status_reg_buf <= "000000000000000000000000000000" & rf_tx_fifo_status_reg_buf(1) & rf_tx_fifo_status_reg_buf(0);
             rf_tx_buffer_status_reg_buf <= "000000000000" & rf_tx_buffer_status_reg_buf(19 downto 2) & rf_tx_buffer_status_reg_buf(1) & rf_tx_buffer_status_reg_buf(0);
             rf_tx_controller_reg_buf <= "0" & rf_tx_controller_reg_buf(30 downto 3) & rf_tx_controller_reg_buf(2) & rf_tx_controller_reg_buf(1) & rf_tx_controller_reg_buf(0);
             rf_rx_controller_reg_buf <= "000000000000000000000000000000" & rf_rx_controller_reg_buf(1) & rf_rx_controller_reg_buf(0);
             config_spi_status_reg_buf <= "00" & config_spi_status_reg_buf(29 downto 20) & "0" & config_spi_status_reg_buf(18) & config_spi_status_reg_buf(17) & config_spi_status_reg_buf(16) & "00" & config_spi_status_reg_buf(13 downto 4) & "0" & config_spi_status_reg_buf(2) & config_spi_status_reg_buf(1) & config_spi_status_reg_buf(0);
             config_spi_config_reg_buf <= "000000000000000000000000000000" & CONFIG_SPI_CONFIG_REG_DEFAULT(1) & CONFIG_SPI_CONFIG_REG_DEFAULT(0);
             config_spi_wr_reg_buf <= "00000000000000000000000" & CONFIG_SPI_WR_REG_DEFAULT(8) & config_spi_wr_reg_buf(7 downto 0);
             config_spi_rd_reg_buf <= "000000000000000000000000" & config_spi_rd_reg_buf(7 downto 0);
             BRAM_CTRL_REG_0_reg_buf <= BRAM_CTRL_REG_0_reg_buf(31 downto 6) & BRAM_CTRL_REG_0_reg_buf(5) & BRAM_CTRL_REG_0_REG_DEFAULT(4) & BRAM_CTRL_REG_0_reg_buf(3) & BRAM_CTRL_REG_0_REG_DEFAULT(2) & BRAM_CTRL_REG_0_reg_buf(1) & BRAM_CTRL_REG_0_reg_buf(0);
             BRAM_CTRL_REG_1_reg_buf <= "00000" & BRAM_CTRL_REG_1_reg_buf(26) & BRAM_CTRL_REG_1_reg_buf(25 downto 4) & "000" & BRAM_CTRL_REG_1_REG_DEFAULT(0);
             GW_POWER_USER_LEDS_reg_buf <= "0000000000000000000000000000" & GW_POWER_USER_LEDS_reg_buf(3 downto 0);
             EXT_SPI_CTRL_reg_buf <= "00000000" & EXT_SPI_CTRL_reg_buf(23 downto 16) & "0000000" & EXT_SPI_CTRL_reg_buf(8) & "0000000" & EXT_SPI_CTRL_REG_DEFAULT(0);
             EXT_SPI_CR_reg_buf <= EXT_SPI_CR_reg_buf(31 downto 24) & EXT_SPI_CR_reg_buf(23 downto 16) & EXT_SPI_CR_reg_buf(15 downto 8) & EXT_SPI_CR_reg_buf(7) & EXT_SPI_CR_reg_buf(6) & EXT_SPI_CR_reg_buf(5) & EXT_SPI_CR_reg_buf(4) & EXT_SPI_CR_reg_buf(3) & EXT_SPI_CR_reg_buf(2) & EXT_SPI_CR_reg_buf(1) & EXT_SPI_CR_REG_DEFAULT(0);
             EXT_SPI_FIFO_IN_reg_buf <= "000" & EXT_SPI_FIFO_IN_reg_buf(28) & "000" & EXT_SPI_FIFO_IN_REG_DEFAULT(24) & EXT_SPI_FIFO_IN_reg_buf(23 downto 0);
             EXT_SPI_FIFO_OUT_reg_buf <= "00000000" & EXT_SPI_FIFO_OUT_reg_buf(23 downto 0);
             end if;
           end if;
         end if;
    end process;

    -- Implement write response logic generation

    process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
              axi_bvalid <= '0';
              axi_bresp <= "00";--need to work more on the responses
            else
              if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0') then
                axi_bvalid <= '1';
                axi_bresp <= "00";
              elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then--check if bready is asserted while bvalid is high)
                axi_bvalid <= '0';-- (there is a possibility that bready is always asserted high)
              end if;
            end if;
          end if;
    end process;

---------------------------
-- Read register process
---------------------------

    process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
              axi_arready <= '0';
              axi_araddr<= (others => '1');
            else
              if (axi_arready = '0' and S_AXI_ARVALID = '1') then
                -- indicates that the slave has acceped the valid read address
                axi_arready <= '1';
                -- Read Address latching 
                axi_araddr<= S_AXI_ARADDR;
              else
                axi_arready <= '0';
              end if;
            end if;
          end if;
    end process;

    -- Implement axi_arvalid generation 
    process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
              axi_rvalid <= '0';
              axi_rresp <= "00";
            else
              if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
                -- Valid read data is available at the read data bus
                axi_rvalid <= '1';
                axi_rresp <= "00"; -- 'OKAY' response
              elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
                -- Read data is accepted by the master
                axi_rvalid <= '0';
              end if;
            end if;
          end if;
    end process;

    -- Implement memory mapped register select and read logic generation

    slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid);

    process (S_AXI_ACLK, S_AXI_ARESETN, FIRMWARE_VERSION_internal, rf_tx_fifo_rst_internal, rf_tx_fifo_write_en_internal, rf_tx_fifo_din_q_internal, rf_tx_fifo_din_i_internal, rf_tx_fifo_empty_flag_internal, rf_tx_fifo_full_flag_internal, rf_tx_buffer_empty_flag_internal, rf_tx_buffer_full_flag_internal, rf_tx_buffer_data_count_internal, rf_tx_controller_rst_internal, rf_tx_controller_enable_internal, rf_tx_loop_burst_mode_internal, rf_tx_controller_constant_internal, rf_rx_serdes_force_emulator_internal, rf_rx_serdes_sync_flag_internal, config_spi_rx_full_flag_internal, config_spi_rx_empty_flag_internal, config_spi_rx_thres_flag_internal, config_spi_rx_data_count_internal, config_spi_tx_full_flag_internal, config_spi_tx_empty_flag_internal, config_spi_tx_thres_flag_internal, config_spi_tx_data_count_internal, config_spi_rx_fifo_reset_internal, config_spi_tx_fifo_reset_internal, config_spi_data_wr_internal, config_spi_wr_internal, config_spi_data_rd_internal, bram_resetb_internal, bram_acquisition_en_internal, bram_overflow_flag_clear_internal, bram_overflow_flag_internal, bram_irq_clear_internal, bram_polarity_internal, bram_packet_timestamp_internal, cdma_transfert_done_internal, bram_fill_threshold_internal, emulator_en_internal, gw_power_user_leds_internal, ext_spi_ctrl_reset_internal, ext_spi_status_tx_end_internal, ext_spi_byte_cntr_internal, ext_spi_cr_rst_internal, ext_spi_cr_cpol_internal, ext_spi_cr_cpha_internal, ext_spi_cr_endian_internal, ext_spi_cr_fifo_in_empty_internal, ext_spi_cr_fifo_in_full_internal, ext_spi_cr_fifo_out_empty_internal, ext_spi_cr_send_frame_internal, ext_spi_cr_clk_divider_internal, ext_spi_cr_ss_sel_internal, ext_spi_cr_ss_busy_internal, ext_spi_fifo_in_data_internal, ext_spi_fifo_in_wr_en_internal, ext_spi_fifo_loopback_internal, ext_spi_fifo_out_data_internal, axi_araddr, slv_reg_rden)
          variable loc_addr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
        begin
          if (rising_edge (S_AXI_ACLK)) then
            if S_AXI_ARESETN = '0' then
              axi_rdata <= (others => '1');
              config_spi_rd_fifo_en <= '0';
              ext_spi_fifo_out_fifo_en <= '0';
            else
              if (slv_reg_rden = '1') then
                -- Address decoding for reading registers
                loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
                case loc_addr is
                  when FIRMWARE_VERSION_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= FIRMWARE_VERSION_internal;
                  when RF_TX_FIFO_CTRL_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00" & rf_tx_fifo_din_i_internal & rf_tx_fifo_din_q_internal & rf_tx_fifo_write_en_internal & rf_tx_fifo_rst_internal;
                  when RF_TX_FIFO_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000000000000000000000000000000" & rf_tx_fifo_full_flag_internal & rf_tx_fifo_empty_flag_internal;
                  when RF_TX_BUFFER_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000000000000" & rf_tx_buffer_data_count_internal & rf_tx_buffer_full_flag_internal & rf_tx_buffer_empty_flag_internal;
                  when RF_TX_CONTROLLER_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "0" & rf_tx_controller_constant_internal & rf_tx_loop_burst_mode_internal & rf_tx_controller_enable_internal & rf_tx_controller_rst_internal;
                  when RF_RX_CONTROLLER_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000000000000000000000000000000" & rf_rx_serdes_sync_flag_internal & rf_rx_serdes_force_emulator_internal;
                  when CONFIG_SPI_STATUS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00" & config_spi_tx_data_count_internal & "0" & config_spi_tx_thres_flag_internal & config_spi_tx_empty_flag_internal & config_spi_tx_full_flag_internal & "00" & config_spi_rx_data_count_internal & "0" & config_spi_rx_thres_flag_internal & config_spi_rx_empty_flag_internal & config_spi_rx_full_flag_internal;
                  when CONFIG_SPI_CONFIG_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000000000000000000000000000000" & config_spi_tx_fifo_reset_internal & config_spi_rx_fifo_reset_internal;
                  when CONFIG_SPI_WR_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00000000000000000000000" & config_spi_wr_internal & config_spi_data_wr_internal;
                  when CONFIG_SPI_RD_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000000000000000000000000" & config_spi_data_rd_internal;
                     config_spi_rd_fifo_en <= '1';
                  when BRAM_CTRL_REG_0_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= bram_packet_timestamp_internal & bram_polarity_internal & bram_irq_clear_internal & bram_overflow_flag_internal & bram_overflow_flag_clear_internal & bram_acquisition_en_internal & bram_resetb_internal;
                  when BRAM_CTRL_REG_1_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00000" & emulator_en_internal & bram_fill_threshold_internal & "000" & cdma_transfert_done_internal;
                  when GW_POWER_USER_LEDS_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "0000000000000000000000000000" & gw_power_user_leds_internal;
                  when EXT_SPI_CTRL_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00000000" & ext_spi_byte_cntr_internal & "0000000" & ext_spi_status_tx_end_internal & "0000000" & ext_spi_ctrl_reset_internal;
                  when EXT_SPI_CR_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= ext_spi_cr_ss_busy_internal & ext_spi_cr_ss_sel_internal & ext_spi_cr_clk_divider_internal & ext_spi_cr_send_frame_internal & ext_spi_cr_fifo_out_empty_internal & ext_spi_cr_fifo_in_full_internal & ext_spi_cr_fifo_in_empty_internal & ext_spi_cr_endian_internal & ext_spi_cr_cpha_internal & ext_spi_cr_cpol_internal & ext_spi_cr_rst_internal;
                  when EXT_SPI_FIFO_IN_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "000" & ext_spi_fifo_loopback_internal & "000" & ext_spi_fifo_in_wr_en_internal & ext_spi_fifo_in_data_internal;
                  when EXT_SPI_FIFO_OUT_REG_ADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) =>
                     axi_rdata <= "00000000" & ext_spi_fifo_out_data_internal;
                     ext_spi_fifo_out_fifo_en <= '1';
                  when others =>
                     axi_rdata <= (others => '0');
                     config_spi_rd_fifo_en <= '0';
                     ext_spi_fifo_out_fifo_en <= '0';
                end case;
              else
                axi_rdata <= axi_rdata;
                config_spi_rd_fifo_en <= '0';
                ext_spi_fifo_out_fifo_en <= '0';
              end if;
            end if;
          end if;
    end process;

end architecture_axi_register_interface;