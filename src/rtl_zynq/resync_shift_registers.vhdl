--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief resync signal between two clock domains
--------------------------------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------
-- resync shift register entity
------------------------------------------------------------------------------
entity resync_shift_register is
  port (
    resync_clk     : in  std_logic;  -- clk required to resync the
                                     -- signal on it
    rstb           : in  std_logic;  -- FPGA reset
    bit_in         : in  std_logic;  -- input to resync
    bit_out_resync : out std_logic   -- bit resync
    );
end entity resync_shift_register;

------------------------------------------------------------------------------
-- resync shift register behavior
------------------------------------------------------------------------------
architecture behavior of resync_shift_register is
  signal internal_shift_register : unsigned(2 downto 0);
begin

  -- output updated
  bit_out_resync <= internal_shift_register(0);

  -- sync bit with "triplette de resyncho" three stages registers
  start_sync : process(resync_clk, rstb)
  begin
    if rstb = '0' then
      internal_shift_register <= (others => '0');
    elsif resync_clk'event and resync_clk = '1' then
      internal_shift_register <= bit_in & internal_shift_register(2 downto 1);
    end if;
  end process;
end behavior;
