--! @brief: Stephane breysse 05/2016

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
  
--library work;
--    use work.package_global.all;

entity spi_master_ip is
    generic(
        C_SLAVES_NB_WIDTH : integer := 8;
        C_ADDR_WIDTH : integer := 8;
        C_DATA_WIDTH : integer := 8;
        C_DIV_WIDTH : integer := 8
    );
    port (  
        clk : in std_logic := 'X';                                                             
        rst : in std_logic := 'X';    

        clk_divider : in std_logic_vector(C_DIV_WIDTH-1 downto 0);

        spi_cpol : in std_logic := '0';
        spi_cpha : in std_logic := '0';
        spi_endian : in std_logic := '0';

        spi_ss_sel : in std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0);   
        spi_ss : out std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0);           
        spi_sck : out std_logic;                                          
        spi_mosi : out std_logic;                                         
        spi_miso : in std_logic := '0';                                   
                                    
        send_frame : in std_logic := 'X';

        fifo_in_wr : in std_logic := 'X';               
        fifo_in_din : in  std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => 'X'); 
        fifo_in_empty : out std_logic; 
        fifo_in_full : out std_logic;

        fifo_out_rd : in std_logic := '0';                           
        fifo_out_dout : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
        fifo_out_empty : out std_logic                  
    );                      
end spi_master_ip; 

architecture rtl of spi_master_ip is
    signal tick : std_logic := '0';

    signal fifo_in_rd : std_logic := '0';
    signal fifo_in_dout : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0');
    signal fifo_in_empty_t : std_logic := '0';

    signal fifo_out_wr : std_logic := '0'; 
    signal fifo_out_din : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0'); 
    signal fifo_out_full : std_logic := '0'; 
begin 

    clk_divider_1 : entity work.clk_divider
    generic map(
         C_DIV_WIDTH => C_DIV_WIDTH
    )        
    port map(
        rst => rst,
        clk => clk,
        clk_divider => clk_divider, -- to divide by 8 
        tick => tick);


    fifo_in_1 : entity work.fifo_fwft
    generic map(
        DATA_WIDTH => C_DATA_WIDTH,
        ADDR_WIDTH => C_ADDR_WIDTH 
        )
    port map(
        CLK        => clk,
        SRST       => rst,
        WR_EN      => fifo_in_wr,
        RD_EN      => fifo_in_rd,
        DIN        => fifo_in_din,
        DOUT       => fifo_in_dout,
        FULL       => fifo_in_full,
        EMPTY      => fifo_in_empty_t,
        RD_COUNT   => open
        );

    spi_master_1 : entity work.spi_master
    generic map(
        C_SLAVES_NB_WIDTH => C_SLAVES_NB_WIDTH,
        C_DATA_WIDTH => C_DATA_WIDTH
    )                                 
    port map(  
        clk => clk,
        tick => tick,                                                            
        rst => rst,   

        cpol => spi_cpol,
        cpha => spi_cpha,
        endian => spi_endian,

        spi_ss_sel => spi_ss_sel,   
        spi_ss => spi_ss,         
        spi_sck => spi_sck,                                  
        spi_mosi => spi_mosi,                                        
        spi_miso => spi_miso,                                  
                                    
        send_frame => send_frame,
        fifo_in_dout => fifo_in_dout,    
        fifo_in_empty => fifo_in_empty_t,
        fifo_in_rd => fifo_in_rd,                                    
        fifo_out_din => fifo_out_din,
        fifo_out_full => fifo_out_full,
        fifo_out_wr => fifo_out_wr                        
    );

    fifo_out_1 : entity work.fifo_fwft
    generic map(
        DATA_WIDTH => C_DATA_WIDTH,
        ADDR_WIDTH => C_ADDR_WIDTH 
        )
    port map(
        CLK        => clk,
        SRST       => rst,
        WR_EN      => fifo_out_wr,
        RD_EN      => fifo_out_rd,
        DIN        => fifo_out_din,
        DOUT       => fifo_out_dout,
        FULL       => fifo_out_full,
        EMPTY      => fifo_out_empty,
        RD_COUNT   => open
        );

    fifo_in_empty <= fifo_in_empty_t;
end rtl;
