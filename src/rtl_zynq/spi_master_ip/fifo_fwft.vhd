--! @brief: Sstephane breysse 05/2016
--! First Word Fall Through fifo
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity fifo_fwft is
  generic (
    DATA_WIDTH : integer := 8;
    ADDR_WIDTH : integer := 8
    );
  port (
    CLK      : in  std_logic;
    SRST     : in  std_logic;
    WR_EN    : in  std_logic;
    RD_EN    : in  std_logic;
    DIN      : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    DOUT     : out std_logic_vector(DATA_WIDTH-1 downto 0);
    FULL     : out std_logic;
    EMPTY    : out std_logic;
    RD_COUNT : out std_logic_vector(ADDR_WIDTH-1 downto 0)
    );
end fifo_fwft;

architecture rtl of fifo_fwft is

  --RAM block definition
  type RAM_BLOCK is array(0 to ((2**ADDR_WIDTH)-1)) of std_logic_vector(DATA_WIDTH-1 downto 0);
  signal ram0 : RAM_BLOCK;

  signal addr_rd : unsigned(ADDR_WIDTH-1 downto 0) := (others => '0');
  signal addr_wr : unsigned(ADDR_WIDTH-1 downto 0) := (others => '0');
  signal count   : unsigned(ADDR_WIDTH-1 downto 0) := (others => '0');
  signal full_i  : std_logic                       := '0';
  signal empty_i : std_logic                       := '1';
  signal push    : std_logic                       := '0';
  signal pop     : std_logic                       := '0';

  constant COUNT_MAX  : unsigned(ADDR_WIDTH-1 downto 0) := (others => '1');
  constant COUNT_NULL : unsigned(ADDR_WIDTH-1 downto 0) := (others => '0');

begin

  --**************************************************************************
  -- Single Clock RAM management
  --**************************************************************************
  push <= WR_EN and not full_i;
  pop  <= RD_EN and not empty_i;

  p_rd : process (CLK)
  begin
    if rising_edge(CLK) then
      DOUT <= ram0(to_integer(addr_rd));
    end if;
  end process;


  p_wr : process (CLK)
  begin
    if rising_edge(CLK) then
      if (push = '1') then
        ram0(to_integer(addr_wr)) <= DIN;
      end if;
    end if;
  end process;


  --**************************************************************************
  -- RAM pointers management
  --**************************************************************************
  p_addr_wr : process(CLK)
  begin
    if rising_edge(CLK) then
      if SRST = '1' then
        addr_wr <= (others => '0');
      else
        if (push = '1') then
          addr_wr <= addr_wr + to_unsigned(1, ADDR_WIDTH);
        end if;
      end if;
    end if;
  end process;

  p_addr_rd : process(CLK)
  begin
    if rising_edge(CLK) then
      if SRST = '1' then
        addr_rd <= (others => '0');
      else
        if (pop = '1') then
          addr_rd <= addr_rd + to_unsigned(1, ADDR_WIDTH);
        end if;
      end if;
    end if;
  end process;

  p_cnt : process(CLK)
  begin
    if rising_edge(CLK) then
      if SRST = '1' then
        count <= (others => '0');
      else
        if push = '1' and pop = '0' then
          count <= count + to_unsigned(1, ADDR_WIDTH);
        elsif push = '0' and pop = '1' then
          count <= count - to_unsigned(1, ADDR_WIDTH);
        else
          count <= count;
        end if;
      end if;
    end if;
  end process;

  RD_COUNT <= std_logic_vector(count);
  FULL     <= full_i;
  EMPTY    <= empty_i;
  full_i   <= '1' when count = COUNT_MAX  else '0';
  empty_i  <= '1' when count = COUNT_NULL else '0';

end rtl;
