--! @brief: Stephane breysse 05/2016

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
  
--library work;
--    use work.package_global.all;
    
entity spi_master is
    generic(
        C_SLAVES_NB_WIDTH : integer := 8;
        C_DATA_WIDTH : integer := 8
    );
    port (  
        clk : in std_logic := 'X';  
        tick : in std_logic := 'X';                                                             
        rst : in std_logic := 'X';    

        cpol : in std_logic := '0';
        cpha : in std_logic := '0';
        endian : in std_logic := '0';

        spi_ss_sel : in std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0);   
        spi_ss : out std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0);           
        spi_sck : out std_logic;                                          
        spi_mosi : out std_logic;                                         
        spi_miso : in std_logic := '0';                                   
                                    
        send_frame : in std_logic := 'X';
        fifo_in_dout : in  std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => 'X');      
        fifo_in_empty : in std_logic := '0';
        fifo_in_rd : out std_logic;                                       
        fifo_out_din : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
        fifo_out_full : in std_logic := '0';
        fifo_out_wr : out std_logic                         
    );                      
end spi_master;

architecture rtl of spi_master is

    type state_type is (st_idle, st_read_fifo_1, st_ref_1, st_ref_0, st_end_1, st_end_2);
    signal state, next_state : state_type;

    signal spi_sck_t : std_logic := '0';
    signal spi_sck_t_1 : std_logic := '0';
    signal spi_sck_t_2 : std_logic := '0';
    signal spi_sck_t_3 : std_logic := '0';
    signal spi_sck_t_4 : std_logic := '0';
    signal spi_ss_i : std_logic := '1';
    signal spi_ss_o : std_logic := '1';
    signal spi_ss_t : std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0) := (others => '1'); 
    signal spi_ss_t_1 : std_logic_vector((C_SLAVES_NB_WIDTH-1) downto 0) := (others => '1');
    signal spi_mosi_t : std_logic := '0';
    signal spi_ref_i : std_logic := '0';
    signal spi_ref_o : std_logic := '0';
    signal spi_ref_1 : std_logic := '0';
    signal spi_ref_2 : std_logic := '0';
    signal spi_ref_re : std_logic := '0';
    signal spi_ref_fe : std_logic := '0';
    signal last_bit : std_logic := '0';
    signal fifo_in_rd_i : std_logic := '0';
    signal fifo_in_rd_o : std_logic := '0';
    signal fifo_in_dout_1 : std_logic_vector (C_DATA_WIDTH-1 downto 0) := (others => '0');      
    signal fifo_in_dout_2 : std_logic_vector (C_DATA_WIDTH-1 downto 0) := (others => '0');      
    signal fifo_in_empty_1 : std_logic := '1';
    signal fifo_in_empty_2 : std_logic := '1';
    signal fifo_in_empty_t : std_logic := '1';
    signal fifo_out_din_t : std_logic_vector (C_DATA_WIDTH-1 downto 0) := (others => '0');
    signal fifo_out_wr_t, fifo_out_wr_t_1 : std_logic := '0';
    signal bits_cntr : integer range 0 to (C_DATA_WIDTH-1) := 0;
    signal bits_cntr_1 : integer range 0 to (C_DATA_WIDTH-1) := 0;
    signal bits_index : integer range 0 to (C_DATA_WIDTH-1) := 0;
    signal bits_index_rx : integer range 0 to (C_DATA_WIDTH-1) := 0; 
   
begin 
     
    p_fifo_in_empty_sync: process (clk)
    begin
        if rising_edge(clk) then
            fifo_in_empty_1 <= fifo_in_empty;
            fifo_in_empty_2 <= fifo_in_empty_1;

            if tick = '1' then
                fifo_in_empty_t <= fifo_in_empty;
            else
                fifo_in_empty_t <= fifo_in_empty_t;
            end if;
        end if;
    end process;

    SYNC_PROC: process (clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                state <= st_idle;
                spi_ss_o <= '1';
                spi_ref_o <= '0';
                fifo_in_rd_o <= '0';
            else
                state <= next_state;
                spi_ss_o <= spi_ss_i;
                spi_ref_o <= spi_ref_i;
                fifo_in_rd_o <= fifo_in_rd_i;
            end if;
        end if;
    end process;

    --MOORE State-Machine - Outputs based on state only
    OUTPUT_DECODE: process (state)
    begin
        if state = st_idle then
            spi_ss_i <= '1';
            spi_ref_i <= '0';
            fifo_in_rd_i <= '0';
        elsif state = st_read_fifo_1 then
            spi_ss_i <= '0';
            spi_ref_i <= '1';
            fifo_in_rd_i <= '1';
        elsif state = st_ref_1 then
            spi_ss_i <= '0';
            spi_ref_i <= '1';
            fifo_in_rd_i <= '0';
        elsif state = st_ref_0 then
            spi_ss_i <= '0';
            spi_ref_i <= '0';
            fifo_in_rd_i <= '0';
        elsif state = st_end_1 then
            spi_ss_i <= '0';
            spi_ref_i <= '0';
            fifo_in_rd_i <= '0';
        elsif state = st_end_2 then
            spi_ss_i <= '0';
            spi_ref_i <= '0';
            fifo_in_rd_i <= '0';
        end if;
    end process;

    NEXT_STATE_DECODE: process (state, tick, fifo_in_empty_t, last_bit, send_frame)
    begin
        next_state <= state;  

        case state is
            when st_idle =>
                if (fifo_in_empty_t = '0') and (send_frame = '1') and (tick = '1') then
                    next_state <= st_read_fifo_1;
                else
                    next_state <= st_idle;
                end if;

            when st_read_fifo_1 =>
                next_state <= st_ref_1;

            when st_ref_1 =>
                if (tick = '1') and (last_bit = '1') then
                    next_state <= st_end_1;
                elsif (tick = '1') then
                    next_state <= st_ref_0;
                else 
                    next_state <= st_ref_1;
                end if;

            when st_ref_0 => 
                if (tick = '1') and (fifo_in_empty_t = '0') and (bits_cntr = (C_DATA_WIDTH-1)) then
                    next_state <= st_read_fifo_1;
                elsif (tick = '1') then
                    next_state <= st_ref_1;
                else
                    next_state <= st_ref_0;
                end if;

            when st_end_1 => 
                if (tick = '1') then
                    next_state <= st_end_2;
                else
                    next_state <= st_end_1;
                end if;
            when st_end_2 =>
                if (tick = '1') then
                    next_state <= st_idle;
                else
                    next_state <= st_end_2;
                end if; 

            when others =>
                next_state <= st_idle;

        end case;
    end process;

    p_fifo_in_dout_latch : process (clk)
    begin
        if rising_edge(clk) then
            bits_cntr_1 <= bits_cntr;
            if ((fifo_in_rd_o = '1') and (bits_cntr_1 = (C_DATA_WIDTH-1))) or ((fifo_in_empty_1 = '0') and (fifo_in_empty_2 = '1')) then
                fifo_in_dout_1 <= fifo_in_dout;
            else
                fifo_in_dout_1 <= fifo_in_dout_1;
            end if;
        end if;
    end process;

    spi_ref_re <= '1' when spi_ref_1 = '1' and spi_ref_2 = '0' else '0';
    spi_ref_fe <= '1' when spi_ref_1 = '0' and spi_ref_2 = '1' else '0';

    p_bits_cntr: process (clk)
    begin
        if rising_edge(clk) then
            spi_ref_1 <= spi_ref_o;
            spi_ref_2 <= spi_ref_1;

            --! Wait a spi ref rising edge:
            if spi_ss_o = '1' then 
                bits_cntr <= 0;
            elsif (cpha = '0' and spi_ref_re = '1') or (cpha = '1' and spi_ref_fe = '1') then 
                if bits_cntr = (C_DATA_WIDTH-1) then
                    bits_cntr <= 0;
                else
                    bits_cntr <= bits_cntr + 1;
                end if;
            else
                bits_cntr <= bits_cntr;
            end if;

        end if;
    end process;

    p_bits_index: process (clk)
    begin
        if rising_edge(clk) then
            if endian = '0' then
                bits_index <= C_DATA_WIDTH-1-bits_cntr;
            else
                bits_index <= bits_cntr;
            end if;

            if (cpha = '0' and spi_ref_re = '1') or (cpha = '1' and spi_ref_fe = '1') then
                bits_index_rx <= bits_index;
            else
                bits_index_rx <= bits_index_rx;
            end if;
        end if;
    end process;

    p_last_bit_sync: process (clk)
    begin
        if rising_edge(clk) then
            if spi_ss_o = '0' then
                if tick = '1' and (fifo_in_empty_t = '1') and (bits_cntr = (C_DATA_WIDTH-1)) then
                    last_bit <= '1';
                else
                    last_bit <= last_bit;
                end if;
            else
                last_bit <= '0';
            end if;
        end if;
    end process;

    p_clock_out: process (clk)
    begin
        if rising_edge(clk) then
            if tick = '1' then
                --! SPI clock following clock polarity input parameter.
                if cpol = '0' then
                    spi_sck_t <= spi_ref_o;
                else
                    spi_sck_t <= not spi_ref_o;
                end if;
            end if;
        end if;
    end process;

    p_mosi_out: process (clk)
    begin
        if rising_edge(clk) then
            if spi_ss_o = '0' then
                --! SPI clock following clock polarity input parameter.
                if cpha = '0' and spi_ref_re = '1' then
                    spi_mosi_t <= fifo_in_dout_2(bits_index);
                    fifo_in_dout_2 <= fifo_in_dout_1;
                elsif cpha = '1' and spi_ref_fe = '1' then
                    spi_mosi_t <= fifo_in_dout_2(bits_index);
                    fifo_in_dout_2 <= fifo_in_dout_1;
                else 
                    spi_mosi_t <= spi_mosi_t;
                    fifo_in_dout_2 <= fifo_in_dout_2;
                end if;
            else
                spi_mosi_t <= 'Z';
                fifo_in_dout_2 <= fifo_in_dout_1;
            end if;
        end if;
    end process;

    gen_ss: for index in 0 to (C_SLAVES_NB_WIDTH-1) generate
        p_ss_sel: process (clk)
        begin
            if rising_edge(clk) then
                if spi_ss_sel(index) = '1' then
                    spi_ss_t(index) <= spi_ss_o;
                else
                    spi_ss_t(index) <= '1';
                end if;
            end if;
        end process;
    end generate gen_ss;

    p_output_sync: process (clk)
    begin
        if rising_edge(clk) then
            spi_ss_t_1 <= spi_ss_t;
            spi_sck_t_1 <= spi_sck_t;
            spi_sck_t_2 <= spi_sck_t_1;
            spi_sck_t_3 <= spi_sck_t_2;
            spi_sck_t_4 <= spi_sck_t_3;
        end if;
    end process;

    p_get_miso: process (clk)
    begin
        if rising_edge(clk) then
            fifo_out_wr_t_1 <= fifo_out_wr_t;

            if (spi_ss_o = '0') and (fifo_out_full = '0') and ((spi_sck_t_3 = '1' and spi_sck_t_4 = '0' and cpol = cpha) or (spi_sck_t_3 = '0' and spi_sck_t_4 = '1' and cpol /= cpha)) then
                if (endian = '0') and (bits_index_rx = 0) then
                    fifo_out_wr_t <= '1';
                elsif (endian = '1') and (bits_index_rx = (C_DATA_WIDTH-1)) then
                    fifo_out_wr_t <= '1';
                else
                    fifo_out_wr_t <= '0';
                end if;
                fifo_out_din_t(bits_index_rx) <= spi_miso;
            else
                fifo_out_wr_t <= '0' ;
                fifo_out_din_t <= fifo_out_din_t;
            end if;  
        end if;
    end process;

    spi_ss <= spi_ss_t_1;
    spi_sck <= spi_sck_t_3;
    spi_mosi <= spi_mosi_t;
    fifo_in_rd <= fifo_in_rd_o;
    fifo_out_wr <= fifo_out_wr_t_1;
    fifo_out_din <= fifo_out_din_t;

end rtl;
