library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library STD;
use STD.textio.all;
use STD.env.all;

library work;
use work.axi_register_package.all;
use work.axi_register_constant.all;
use work.package_tb_procedure.all;

entity tb_spi is
end tb_spi;

architecture rtl of tb_spi is
  signal clk_200 : std_logic := '1';

  constant C_AXI_ADDR_WIDTH : integer := 32;
  constant C_AXI_DATA_WIDTH : integer := 32;

  constant ADDR_AXI_REG_EXT_SPI_CR       : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C00038";
  constant ADDR_AXI_REG_EXT_SPI_FIFO_IN  : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C0003C";
  constant ADDR_AXI_REG_EXT_SPI_FIFO_OUT : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"43C00040";

  signal C_SLAVES_NB_WIDTH : integer := 8;
  signal C_DATA_WIDTH      : integer := 24;
  signal C_ADDR_WIDTH      : integer := 8;
  signal C_DIV_WIDTH       : integer := 8;

  signal spi_sck, spi_mosi, spi_miso : std_logic                                      := '0';
  signal spi_ss                      : std_logic_vector(C_SLAVES_NB_WIDTH-1 downto 0) := (others => '0');

  signal AXI_REG_ACLK    : std_logic;
  signal AXI_REG_ARESETN : std_logic;
  signal AXI_REG_AWADDR  : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_REG_AWPROT  : std_logic_vector(2 downto 0);
  signal AXI_REG_AWVALID : std_logic;
  signal AXI_REG_AWREADY : std_logic;
  signal AXI_REG_WDATA   : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_REG_WSTRB   : std_logic_vector((C_AXI_DATA_WIDTH/8)-1 downto 0);
  signal AXI_REG_WVALID  : std_logic;
  signal AXI_REG_WREADY  : std_logic;
  signal AXI_REG_BRESP   : std_logic_vector(1 downto 0);
  signal AXI_REG_BVALID  : std_logic;
  signal AXI_REG_BREADY  : std_logic;
  signal AXI_REG_ARADDR  : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
  signal AXI_REG_ARPROT  : std_logic_vector(2 downto 0);
  signal AXI_REG_ARVALID : std_logic;
  signal AXI_REG_ARREADY : std_logic;
  signal AXI_REG_RDATA   : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
  signal AXI_REG_RRESP   : std_logic_vector(1 downto 0);
  signal AXI_REG_RVALID  : std_logic;
  signal AXI_REG_RREADY  : std_logic;

  --! Asygn protocol
  signal cmd_axi_reg_mode         : std_logic                                     := '0';
  signal cmd_axi_reg_addr         : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
  signal cmd_axi_reg_data_in      : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
  signal cmd_axi_reg_data_in_rdy  : std_logic                                     := '0';
  signal cmd_axi_reg_data_out     : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
  signal cmd_axi_reg_data_out_rdy : std_logic                                     := '0';

  --! Asygn axi registers records:
  signal axi_reg_out : register_bus_out;
  signal axi_reg_in  : register_bus_in;

  signal debug_read : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0) := (others => 'Z');
  signal debug_log  : string(1 to 5)                                := "wait ";
  
begin

  clk_200 <= not clk_200 after 2.5 ns;

  AXI_REG_ACLK <= clk_200;

  my_tb : process
  begin
    AXI_REG_ARESETN <= '0';             --! active low reset
    wait for 200 ns;
    AXI_REG_ARESETN <= '1';
    wait for 200 ns;

    AXI_CMD_WR(clk_200, ADDR_AXI_REG_EXT_SPI_CR, x"00018007", cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_in);
    wait for 200 ns;

    AXI_CMD_WR(clk_200, ADDR_AXI_REG_EXT_SPI_FIFO_IN, x"0140A522", cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_in);
    wait for 200 ns;

    AXI_CMD_WR(clk_200, ADDR_AXI_REG_EXT_SPI_CR, x"00010786", cmd_axi_reg_data_in_rdy, cmd_axi_reg_mode, cmd_axi_reg_addr, cmd_axi_reg_data_in);

    wait for 200 ns;

    wait;
  end process;


  --! Master AXI protocol module: for axi register interface
  axi_master_inst_2 : entity work.axi_lite_master
    generic map(
      C_M_AXI_ADDR_WIDTH => C_AXI_ADDR_WIDTH,
      C_M_AXI_DATA_WIDTH => C_AXI_DATA_WIDTH)
    port map(
      M_AXI_ACLK       => AXI_REG_ACLK,
      M_AXI_ARESETN    => AXI_REG_ARESETN,
      M_AXI_AWADDR     => AXI_REG_AWADDR,
      M_AXI_AWPROT     => AXI_REG_AWPROT,
      M_AXI_AWVALID    => AXI_REG_AWVALID,
      M_AXI_AWREADY    => AXI_REG_AWREADY,
      M_AXI_WDATA      => AXI_REG_WDATA,
      M_AXI_WSTRB      => AXI_REG_WSTRB,
      M_AXI_WVALID     => AXI_REG_WVALID,
      M_AXI_WREADY     => AXI_REG_WREADY,
      M_AXI_BRESP      => AXI_REG_BRESP,
      M_AXI_BVALID     => AXI_REG_BVALID,
      M_AXI_BREADY     => AXI_REG_BREADY,
      M_AXI_ARADDR     => AXI_REG_ARADDR,
      M_AXI_ARPROT     => AXI_REG_ARPROT,
      M_AXI_ARVALID    => AXI_REG_ARVALID,
      M_AXI_ARREADY    => AXI_REG_ARREADY,
      M_AXI_RDATA      => AXI_REG_RDATA,
      M_AXI_RRESP      => AXI_REG_RRESP,
      M_AXI_RVALID     => AXI_REG_RVALID,
      M_AXI_RREADY     => AXI_REG_RREADY,
      cmd_mode         => cmd_axi_reg_mode,
      cmd_addr         => cmd_axi_reg_addr,
      cmd_data_in      => cmd_axi_reg_data_in,
      cmd_data_in_rdy  => cmd_axi_reg_data_in_rdy,
      cmd_data_out     => cmd_axi_reg_data_out,
      cmd_data_out_rdy => cmd_axi_reg_data_out_rdy);

  --! Register map interface
  axi_register_interface_2 : entity work.axi_register_interface
    port map (
      S_AXI_ACLK    => AXI_REG_ACLK,    --! Global Clock Signal
      S_AXI_ARESETN => AXI_REG_ARESETN,  --! Global Reset Signal. This Signal is Active LOW
      S_AXI_AWADDR  => AXI_REG_AWADDR,  --! Write address (issued by master, acceped by Slave)
      S_AXI_AWPROT  => AXI_REG_AWPROT,  --! Write channel Protection type.
      S_AXI_AWVALID => AXI_REG_AWVALID,  --! Write address valid.
      S_AXI_AWREADY => AXI_REG_AWREADY,  --! Write address ready.
      S_AXI_WDATA   => AXI_REG_WDATA,  --! Write data (issued by master, acceped by Slave)
      S_AXI_WSTRB   => AXI_REG_WSTRB,   --! Write strobes.
      S_AXI_WVALID  => AXI_REG_WVALID,  --! Write valid
      S_AXI_WREADY  => AXI_REG_WREADY,  --! Write ready.
      S_AXI_BRESP   => AXI_REG_BRESP,   --! Write response.
      S_AXI_BVALID  => AXI_REG_BVALID,  --! Write response valid.
      S_AXI_BREADY  => AXI_REG_BREADY,  --! Response ready.
      S_AXI_ARADDR  => AXI_REG_ARADDR,  --! Read address (issued by master, acceped by Slave)
      S_AXI_ARPROT  => AXI_REG_ARPROT,  --! Protection type.
      S_AXI_ARVALID => AXI_REG_ARVALID,  --! Read address valid.
      S_AXI_ARREADY => AXI_REG_ARREADY,  --! Read address ready.
      S_AXI_RDATA   => AXI_REG_RDATA,   --! Read data (issued by slave)
      S_AXI_RRESP   => AXI_REG_RRESP,   --! Read response.
      S_AXI_RVALID  => AXI_REG_RVALID,  --! Read valid.
      S_AXI_RREADY  => AXI_REG_RREADY,  --! Read ready.
      axi_reg_out   => axi_reg_out,
      axi_reg_in    => axi_reg_in);

  spi_master_ip_1 : entity work.spi_master_ip
    generic map (
      C_SLAVES_NB_WIDTH => C_SLAVES_NB_WIDTH,
      C_DATA_WIDTH      => C_DATA_WIDTH,
      C_ADDR_WIDTH      => C_ADDR_WIDTH,
      C_DIV_WIDTH       => C_DIV_WIDTH)
    port map (
      clk            => AXI_REG_ACLK,
      rst            => axi_reg_out.ext_spi_cr_rst,
      clk_divider    => axi_reg_out.ext_spi_cr_clk_divider,
      spi_cpol       => axi_reg_out.ext_spi_cr_cpol,
      spi_cpha       => axi_reg_out.ext_spi_cr_cpha,
      spi_endian     => axi_reg_out.ext_spi_cr_endian,
      spi_ss_sel     => axi_reg_out.ext_spi_cr_ss_sel,
      spi_ss         => spi_ss,
      spi_sck        => spi_sck,
      spi_mosi       => spi_mosi,
      spi_miso       => spi_mosi,
      send_frame     => axi_reg_out.ext_spi_cr_send_frame,
      fifo_in_wr     => axi_reg_out.ext_spi_fifo_in_wr_en,
      fifo_in_din    => axi_reg_out.ext_spi_fifo_in_data,
      fifo_in_empty  => axi_reg_in.ext_spi_cr_fifo_in_empty,
      fifo_in_full   => axi_reg_in.ext_spi_cr_fifo_in_full,
      fifo_out_rd    => axi_reg_out.ext_spi_fifo_out_fifo_en,
      fifo_out_dout  => axi_reg_in.ext_spi_fifo_out_data,
      fifo_out_empty => axi_reg_in.ext_spi_cr_fifo_out_empty);

end rtl;
