--! @brief: Sstephane breysse 05/2016
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

--library work;
--    use work.package_global.all;

entity clk_divider is
    generic(
        C_DIV_WIDTH : integer := 8
    );
    port
    (
        rst : in std_logic;
        clk : in  std_logic;
        clk_divider : std_logic_vector(C_DIV_WIDTH-1 downto 0);
        tick : out std_logic
    );
end clk_divider;

architecture rtl of clk_divider is

    
    signal clk_divider_i: unsigned((C_DIV_WIDTH-1) downto 0) := (others => '0');
    signal cntr : unsigned((C_DIV_WIDTH-1) downto 0) := (others => '0');
    signal tick_o : std_logic := '0';

begin
    process(CLK)
    begin
        if rising_edge(CLK) then
            if rst = '1' then
                clk_divider_i <= unsigned(clk_divider);
            else
                clk_divider_i <= clk_divider_i;
            end if;
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            if cntr = 0 then
                cntr <= clk_divider_i;
                tick_o <= '1';
            else
                cntr <= cntr-1;
                tick_o <= '0';
            end if;
        end if;
    end process;

    tick <= tick_o;

end rtl;
