--------------------------------------------------------------------------------
--! @file
--------------------------------------------------------------------------------
--! @brief atmel serializer/deserializer
--! @details
--! This module deserialises the RX data clocked on double data rated to build a
--! modulated sampe
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------
--! Entity
------------------------------------------------------------------------------
entity atmel_rx_serdes is
  port (
    resetb         : in  std_logic;
    atmel_clk      : in  std_logic;
    atmel_data_0   : in  std_logic;
    atmel_data_1   : in  std_logic;
    force_emulator : in  std_logic;
    read_data      : out std_logic_vector(31 downto 0);
    read_data_rdy  : out std_logic;
    sync_flag      : out std_logic;
    fsm_state      : out std_logic_vector(1 downto 0));
end atmel_rx_serdes;

------------------------------------------------------------------------------
--! Arch
------------------------------------------------------------------------------
architecture rtl of atmel_rx_serdes is

  constant I_SYNC    : std_logic_vector(1 downto 0) := "10";
  constant Q_sync    : std_logic_vector(1 downto 0) := "01";
  signal i_sync_buff : std_logic_vector(1 downto 0);
  signal q_sync_buff : std_logic_vector(1 downto 0);

  type state is(st_rst, st_idle, st_sync, st_acq);  --state machine data type
  signal current_state : state;
  signal next_state    : state;

  signal receive_shift_reg : std_logic_vector(31 downto 0);
  signal read_data_buff    : std_logic_vector(31 downto 0);
  signal receive_bit_cnt   : unsigned(3 downto 0);

  -- emulator
  constant EMULATOR_WORD : std_logic_vector(31 downto 0) := "10" & "01111000011110" & "01" & "00111100001111";  -- 0x8F0F4F0F
  signal data_0          : std_logic;
  signal data_1          : std_logic;
  signal emul_oneshot    : std_logic;
  signal emulator_buff   : std_logic_vector(63 downto 0);

begin
  ------------------------------------------------------------------------------
  --! Component outputs updating
  ------------------------------------------------------------------------------
  read_data     <= read_data_buff;
  read_data_rdy <= not(std_logic(receive_bit_cnt(3)));

  fsm_state <= "11" when (current_state = st_rst) else
               "10" when (current_state = st_idle) else
               "01" when (current_state = st_sync) else
               "00" when (current_state = st_acq) else
               "11";

  
  data_0 <= emulator_buff(63) when force_emulator = '1' else atmel_data_0;
  data_1 <= emulator_buff(62) when force_emulator = '1' else atmel_data_1;

  i_sync_buff <= receive_shift_reg(29 downto 28);
  q_sync_buff <= receive_shift_reg(13 downto 12);


  emul_p : process(atmel_clk, resetb)
  begin
    if resetb = '0' then
      emulator_buff <= (others => '1');
      emul_oneshot  <= '0';
    elsif atmel_clk'event and atmel_clk = '1' then
      emul_oneshot <= '1';
      if emul_oneshot = '0' then
        emulator_buff <= x"00000000" & EMULATOR_WORD;
      else
        emulator_buff <= emulator_buff(61 downto 0) & emulator_buff(63) & emulator_buff(62);
      end if;
    end if;
  end process;



  sync_64_p : process(atmel_clk, resetb)
  begin
    if resetb = '0' then
      receive_shift_reg <= (others => '1');
      current_state     <= st_rst;
    elsif atmel_clk'event and atmel_clk = '1' then
      receive_shift_reg <= receive_shift_reg(29 downto 0) & data_0 & data_1;
      current_state     <= next_state;
    end if;
  end process;


  --! state transistions
  fsm_p : process(resetb, current_state, receive_shift_reg, i_sync_buff, q_sync_buff)
  begin
    if resetb = '0' then
      next_state <= st_rst;
    else
      case current_state is
        when st_rst =>                  --! Reset state
          next_state <= st_idle;
        when st_idle =>                 --! Idle state
          if receive_shift_reg = x"00000000" then  --! wait for zero word
            next_state <= st_sync;
          else
            next_state <= st_idle;
          end if;
        when st_sync =>                 --! Sync state              
          if i_sync_buff = I_SYNC and q_sync_buff = Q_sync then  --! wait for sync pattern detecting
            next_state <= st_acq;
          else
            next_state <= st_sync;
          end if;
        when st_acq =>                  --! Acq State
          next_state <= st_acq;
        when others =>                  -- default case
          next_state <= st_idle;
      end case;
    end if;
  end process fsm_p;


  --! counter management
  deserializer_counter : process(resetb, atmel_clk, current_state)
  begin
    if resetb = '0' then
      receive_bit_cnt <= (others => '1');
      sync_flag       <= '0';
    elsif atmel_clk'event and atmel_clk = '1' then
      if current_state = st_acq then    --! module is active
        receive_bit_cnt <= receive_bit_cnt + 1;
        sync_flag       <= '1';
      else                              --! module is sleeping
        receive_bit_cnt <= (others => '1');
        sync_flag       <= '0';
      end if;
    end if;
  end process deserializer_counter;






  --! manage read_data and read_data_rdy
  acquisition_p : process(resetb, atmel_clk, current_state, receive_bit_cnt)
  begin
    if resetb = '0' then
      read_data_buff <= (others => '0');
    elsif atmel_clk'event and atmel_clk = '1' then
      if current_state = st_acq and receive_bit_cnt = x"F" then
        read_data_buff <= receive_shift_reg;
      else
        read_data_buff <= read_data_buff;
      end if;
    end if;
  end process acquisition_p;
  
end rtl;

