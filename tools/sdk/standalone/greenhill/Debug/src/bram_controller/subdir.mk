################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/bram_controller/bram_controller.cpp \
../src/bram_controller/cdma.cpp 

OBJS += \
./src/bram_controller/bram_controller.o \
./src/bram_controller/cdma.o 

CPP_DEPS += \
./src/bram_controller/bram_controller.d \
./src/bram_controller/cdma.d 


# Each subdirectory must supply rules for building sources it contributes
src/bram_controller/%.o: ../src/bram_controller/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


