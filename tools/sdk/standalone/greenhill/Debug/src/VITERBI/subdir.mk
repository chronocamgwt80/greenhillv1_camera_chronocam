################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/VITERBI/metrics.c \
../src/VITERBI/tab.c \
../src/VITERBI/viterbi.c 

OBJS += \
./src/VITERBI/metrics.o \
./src/VITERBI/tab.o \
./src/VITERBI/viterbi.o 

C_DEPS += \
./src/VITERBI/metrics.d \
./src/VITERBI/tab.d \
./src/VITERBI/viterbi.d 


# Each subdirectory must supply rules for building sources it contributes
src/VITERBI/%.o: ../src/VITERBI/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


