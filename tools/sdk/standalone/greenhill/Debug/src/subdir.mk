################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

CPP_SRCS += \
../src/Axi_spi.cpp \
../src/DigitalOut.cpp \
../src/Ps_gpio.cpp \
../src/app_controller.cpp \
../src/axi_ext_spi.cpp \
../src/command.cpp \
../src/main.cpp \
../src/spi_slave.cpp 

OBJS += \
./src/Axi_spi.o \
./src/DigitalOut.o \
./src/Ps_gpio.o \
./src/app_controller.o \
./src/axi_ext_spi.o \
./src/command.o \
./src/main.o \
./src/spi_slave.o 

CPP_DEPS += \
./src/Axi_spi.d \
./src/DigitalOut.d \
./src/Ps_gpio.d \
./src/app_controller.d \
./src/axi_ext_spi.d \
./src/command.d \
./src/main.d \
./src/spi_slave.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


