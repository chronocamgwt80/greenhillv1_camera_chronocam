################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ccam_camera/FPGA_CCAM.c 

OBJS += \
./src/ccam_camera/FPGA_CCAM.o 

C_DEPS += \
./src/ccam_camera/FPGA_CCAM.d 


# Each subdirectory must supply rules for building sources it contributes
src/ccam_camera/%.o: ../src/ccam_camera/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


