################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ps_usb/xusbps_ch9.c \
../src/ps_usb/xusbps_ch9_storage.c \
../src/ps_usb/xusbps_class_storage.c 

CPP_SRCS += \
../src/ps_usb/usb_ps.cpp 

OBJS += \
./src/ps_usb/usb_ps.o \
./src/ps_usb/xusbps_ch9.o \
./src/ps_usb/xusbps_ch9_storage.o \
./src/ps_usb/xusbps_class_storage.o 

C_DEPS += \
./src/ps_usb/xusbps_ch9.d \
./src/ps_usb/xusbps_ch9_storage.d \
./src/ps_usb/xusbps_class_storage.d 

CPP_DEPS += \
./src/ps_usb/usb_ps.d 


# Each subdirectory must supply rules for building sources it contributes
src/ps_usb/%.o: ../src/ps_usb/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ps_usb/%.o: ../src/ps_usb/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


