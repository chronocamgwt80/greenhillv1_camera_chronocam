################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/rf/circular_buf.c \
../src/rf/fft.c \
../src/rf/golden_FIR_bitstream_fbso_out_320x4.c \
../src/rf/golden_gen_rand_40_sym.c \
../src/rf/gw_mod_gossyc.c 

CPP_SRCS += \
../src/rf/Rf_tx_fifo.cpp \
../src/rf/Rf_tx_interface.cpp \
../src/rf/gwt_modulator.cpp 

OBJS += \
./src/rf/Rf_tx_fifo.o \
./src/rf/Rf_tx_interface.o \
./src/rf/circular_buf.o \
./src/rf/fft.o \
./src/rf/golden_FIR_bitstream_fbso_out_320x4.o \
./src/rf/golden_gen_rand_40_sym.o \
./src/rf/gw_mod_gossyc.o \
./src/rf/gwt_modulator.o 

C_DEPS += \
./src/rf/circular_buf.d \
./src/rf/fft.d \
./src/rf/golden_FIR_bitstream_fbso_out_320x4.d \
./src/rf/golden_gen_rand_40_sym.d \
./src/rf/gw_mod_gossyc.d 

CPP_DEPS += \
./src/rf/Rf_tx_fifo.d \
./src/rf/Rf_tx_interface.d \
./src/rf/gwt_modulator.d 


# Each subdirectory must supply rules for building sources it contributes
src/rf/%.o: ../src/rf/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/rf/%.o: ../src/rf/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM g++ compiler'
	arm-xilinx-eabi-g++ -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -fpermissive -I../../standalone_bsp_0/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


