--Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2014.4_63036 (lin64) Build 1071353 Tue Nov 18 16:47:07 MST 2014
--Date        : Tue May  3 11:56:17 2016
--Host        : deauville.lin.asygn.com running 64-bit Fedora release 19 (Schrödinger’s Cat)
--Command     : generate_target system_wrapper.bd
--Design      : system_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_wrapper is
  port (
    AXI_EXT_SPI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_EXT_SPI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_EXT_SPI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_EXT_SPI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_EXT_SPI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_EXT_SPI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTB_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_clk : in STD_LOGIC;
    BRAM_PORTB_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_en : in STD_LOGIC;
    BRAM_PORTB_rst : in STD_LOGIC;
    BRAM_PORTB_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    IRQ_PL : in STD_LOGIC_VECTOR ( 0 to 0 );
    PL_CLK : out STD_LOGIC;
    PL_RSTN : out STD_LOGIC;
    S_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    config_spi_io0_io : inout STD_LOGIC;
    config_spi_io1_io : inout STD_LOGIC;
    config_spi_sck_io : inout STD_LOGIC;
    config_spi_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    rf_tx_fifo_clk_read : in STD_LOGIC;
    rf_tx_fifo_din : in STD_LOGIC_VECTOR ( 27 downto 0 );
    rf_tx_fifo_dout : out STD_LOGIC_VECTOR ( 27 downto 0 );
    rf_tx_fifo_empty : out STD_LOGIC;
    rf_tx_fifo_full : out STD_LOGIC;
    rf_tx_fifo_rd_en : in STD_LOGIC;
    rf_tx_fifo_rst : in STD_LOGIC;
    rf_tx_fifo_valid : out STD_LOGIC;
    rf_tx_fifo_wr_en : in STD_LOGIC
  );
end system_wrapper;

architecture STRUCTURE of system_wrapper is
  component system is
  port (
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    config_spi_io0_i : in STD_LOGIC;
    config_spi_io0_o : out STD_LOGIC;
    config_spi_io0_t : out STD_LOGIC;
    config_spi_io1_i : in STD_LOGIC;
    config_spi_io1_o : out STD_LOGIC;
    config_spi_io1_t : out STD_LOGIC;
    config_spi_sck_i : in STD_LOGIC;
    config_spi_sck_o : out STD_LOGIC;
    config_spi_sck_t : out STD_LOGIC;
    config_spi_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    config_spi_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    config_spi_ss_t : out STD_LOGIC;
    BRAM_PORTB_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_clk : in STD_LOGIC;
    BRAM_PORTB_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_en : in STD_LOGIC;
    BRAM_PORTB_rst : in STD_LOGIC;
    BRAM_PORTB_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PL_RSTN : out STD_LOGIC;
    PL_CLK : out STD_LOGIC;
    rf_tx_fifo_din : in STD_LOGIC_VECTOR ( 27 downto 0 );
    rf_tx_fifo_wr_en : in STD_LOGIC;
    rf_tx_fifo_dout : out STD_LOGIC_VECTOR ( 27 downto 0 );
    rf_tx_fifo_rd_en : in STD_LOGIC;
    rf_tx_fifo_valid : out STD_LOGIC;
    rf_tx_fifo_full : out STD_LOGIC;
    rf_tx_fifo_empty : out STD_LOGIC;
    rf_tx_fifo_clk_read : in STD_LOGIC;
    rf_tx_fifo_rst : in STD_LOGIC;
    IRQ_PL : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_EXT_SPI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_EXT_SPI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_EXT_SPI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_EXT_SPI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_EXT_SPI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_EXT_SPI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_EXT_SPI_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal config_spi_io0_i : STD_LOGIC;
  signal config_spi_io0_o : STD_LOGIC;
  signal config_spi_io0_t : STD_LOGIC;
  signal config_spi_io1_i : STD_LOGIC;
  signal config_spi_io1_o : STD_LOGIC;
  signal config_spi_io1_t : STD_LOGIC;
  signal config_spi_sck_i : STD_LOGIC;
  signal config_spi_sck_o : STD_LOGIC;
  signal config_spi_sck_t : STD_LOGIC;
  signal config_spi_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal config_spi_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal config_spi_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal config_spi_ss_t : STD_LOGIC;
begin
config_spi_io0_iobuf: component IOBUF
    port map (
      I => config_spi_io0_o,
      IO => config_spi_io0_io,
      O => config_spi_io0_i,
      T => config_spi_io0_t
    );
config_spi_io1_iobuf: component IOBUF
    port map (
      I => config_spi_io1_o,
      IO => config_spi_io1_io,
      O => config_spi_io1_i,
      T => config_spi_io1_t
    );
config_spi_sck_iobuf: component IOBUF
    port map (
      I => config_spi_sck_o,
      IO => config_spi_sck_io,
      O => config_spi_sck_i,
      T => config_spi_sck_t
    );
config_spi_ss_iobuf_0: component IOBUF
    port map (
      I => config_spi_ss_o_0(0),
      IO => config_spi_ss_io(0),
      O => config_spi_ss_i_0(0),
      T => config_spi_ss_t
    );
system_i: component system
    port map (
      AXI_EXT_SPI_araddr(31 downto 0) => AXI_EXT_SPI_araddr(31 downto 0),
      AXI_EXT_SPI_aresetn(0) => AXI_EXT_SPI_aresetn(0),
      AXI_EXT_SPI_arprot(2 downto 0) => AXI_EXT_SPI_arprot(2 downto 0),
      AXI_EXT_SPI_arready(0) => AXI_EXT_SPI_arready(0),
      AXI_EXT_SPI_arvalid(0) => AXI_EXT_SPI_arvalid(0),
      AXI_EXT_SPI_awaddr(31 downto 0) => AXI_EXT_SPI_awaddr(31 downto 0),
      AXI_EXT_SPI_awprot(2 downto 0) => AXI_EXT_SPI_awprot(2 downto 0),
      AXI_EXT_SPI_awready(0) => AXI_EXT_SPI_awready(0),
      AXI_EXT_SPI_awvalid(0) => AXI_EXT_SPI_awvalid(0),
      AXI_EXT_SPI_bready(0) => AXI_EXT_SPI_bready(0),
      AXI_EXT_SPI_bresp(1 downto 0) => AXI_EXT_SPI_bresp(1 downto 0),
      AXI_EXT_SPI_bvalid(0) => AXI_EXT_SPI_bvalid(0),
      AXI_EXT_SPI_rdata(31 downto 0) => AXI_EXT_SPI_rdata(31 downto 0),
      AXI_EXT_SPI_rready(0) => AXI_EXT_SPI_rready(0),
      AXI_EXT_SPI_rresp(1 downto 0) => AXI_EXT_SPI_rresp(1 downto 0),
      AXI_EXT_SPI_rvalid(0) => AXI_EXT_SPI_rvalid(0),
      AXI_EXT_SPI_wdata(31 downto 0) => AXI_EXT_SPI_wdata(31 downto 0),
      AXI_EXT_SPI_wready(0) => AXI_EXT_SPI_wready(0),
      AXI_EXT_SPI_wstrb(3 downto 0) => AXI_EXT_SPI_wstrb(3 downto 0),
      AXI_EXT_SPI_wvalid(0) => AXI_EXT_SPI_wvalid(0),
      BRAM_PORTB_addr(31 downto 0) => BRAM_PORTB_addr(31 downto 0),
      BRAM_PORTB_clk => BRAM_PORTB_clk,
      BRAM_PORTB_din(31 downto 0) => BRAM_PORTB_din(31 downto 0),
      BRAM_PORTB_dout(31 downto 0) => BRAM_PORTB_dout(31 downto 0),
      BRAM_PORTB_en => BRAM_PORTB_en,
      BRAM_PORTB_rst => BRAM_PORTB_rst,
      BRAM_PORTB_we(3 downto 0) => BRAM_PORTB_we(3 downto 0),
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      IRQ_PL(0) => IRQ_PL(0),
      PL_CLK => PL_CLK,
      PL_RSTN => PL_RSTN,
      S_AXI_araddr(31 downto 0) => S_AXI_araddr(31 downto 0),
      S_AXI_arprot(2 downto 0) => S_AXI_arprot(2 downto 0),
      S_AXI_arready(0) => S_AXI_arready(0),
      S_AXI_arvalid(0) => S_AXI_arvalid(0),
      S_AXI_awaddr(31 downto 0) => S_AXI_awaddr(31 downto 0),
      S_AXI_awprot(2 downto 0) => S_AXI_awprot(2 downto 0),
      S_AXI_awready(0) => S_AXI_awready(0),
      S_AXI_awvalid(0) => S_AXI_awvalid(0),
      S_AXI_bready(0) => S_AXI_bready(0),
      S_AXI_bresp(1 downto 0) => S_AXI_bresp(1 downto 0),
      S_AXI_bvalid(0) => S_AXI_bvalid(0),
      S_AXI_rdata(31 downto 0) => S_AXI_rdata(31 downto 0),
      S_AXI_rready(0) => S_AXI_rready(0),
      S_AXI_rresp(1 downto 0) => S_AXI_rresp(1 downto 0),
      S_AXI_rvalid(0) => S_AXI_rvalid(0),
      S_AXI_wdata(31 downto 0) => S_AXI_wdata(31 downto 0),
      S_AXI_wready(0) => S_AXI_wready(0),
      S_AXI_wstrb(3 downto 0) => S_AXI_wstrb(3 downto 0),
      S_AXI_wvalid(0) => S_AXI_wvalid(0),
      config_spi_io0_i => config_spi_io0_i,
      config_spi_io0_o => config_spi_io0_o,
      config_spi_io0_t => config_spi_io0_t,
      config_spi_io1_i => config_spi_io1_i,
      config_spi_io1_o => config_spi_io1_o,
      config_spi_io1_t => config_spi_io1_t,
      config_spi_sck_i => config_spi_sck_i,
      config_spi_sck_o => config_spi_sck_o,
      config_spi_sck_t => config_spi_sck_t,
      config_spi_ss_i(0) => config_spi_ss_i_0(0),
      config_spi_ss_o(0) => config_spi_ss_o_0(0),
      config_spi_ss_t => config_spi_ss_t,
      rf_tx_fifo_clk_read => rf_tx_fifo_clk_read,
      rf_tx_fifo_din(27 downto 0) => rf_tx_fifo_din(27 downto 0),
      rf_tx_fifo_dout(27 downto 0) => rf_tx_fifo_dout(27 downto 0),
      rf_tx_fifo_empty => rf_tx_fifo_empty,
      rf_tx_fifo_full => rf_tx_fifo_full,
      rf_tx_fifo_rd_en => rf_tx_fifo_rd_en,
      rf_tx_fifo_rst => rf_tx_fifo_rst,
      rf_tx_fifo_valid => rf_tx_fifo_valid,
      rf_tx_fifo_wr_en => rf_tx_fifo_wr_en
    );
end STRUCTURE;
