// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (lin64) Build 1071353 Tue Nov 18 16:47:07 MST 2014
// Date        : Mon Aug 29 12:08:03 2016
// Host        : adel-OptiPlex-5040 running 64-bit Ubuntu 14.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/gwt/Greenwaves_Project/tools/vivado/greenhill_zynq/greenhill_zynq.srcs/sources_1/ip/fifo_28b/fifo_28b_stub.v
// Design      : fifo_28b
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v12_0,Vivado 2014.4_63036" *)
module fifo_28b(clk, rst, din, wr_en, rd_en, dout, full, empty, valid, data_count)
/* synthesis syn_black_box black_box_pad_pin="clk,rst,din[27:0],wr_en,rd_en,dout[27:0],full,empty,valid,data_count[17:0]" */;
  input clk;
  input rst;
  input [27:0]din;
  input wr_en;
  input rd_en;
  output [27:0]dout;
  output full;
  output empty;
  output valid;
  output [17:0]data_count;
endmodule
