/*
 * \file Monitoring.h
 *
 *  Created on: Sep 24, 2015
 *      Author: avialletelle
 */

#ifndef MONITORING_H_
#define MONITORING_H_


/*
 * Includes
 */
#include "mbed.h"
#include "config.h"
#include "io.hpp"


class Control_Monitoring
{
public:
	Control_Monitoring();
    virtual ~Control_Monitoring();

    // monitoring
    void start_acq(uint32_t dots_threshold);
    void stop_acq();
    uint8_t get_status();
    void get_adc_output();
    void monitoring_table_reset();
    AnalogIn _imon_rf;
    AnalogIn _imon_pa;
    AnalogIn _vmon_pa;
    uint8_t _acq_flag;
    uint16_t _monitoring_table[MONITORING_DOT_NB_MAX][MONITORING_NB_OUTPUT];
    uint32_t _dot_index;
    uint32_t _dots_threshold;
protected:
    // power
    void power_enable(DigitalOut output);
    void power_disable(DigitalOut output);
    DigitalOut _rf_power;
    DigitalOut _pa_power;
};

#endif /* MONITORING_H_ */
