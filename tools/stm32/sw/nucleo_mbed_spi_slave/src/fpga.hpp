/*
 * fpga.hpp
 *
 *  Created on: Sep 28, 2015
 *      Author: cleblanc
 */

#ifndef FPGA_HPP_
#define FPGA_HPP_

#include "io.hpp"
#include "mbed.h"
#include "frame.h"
#include "config.h"
#include "Pa.h"
#include "Control_Monitoring.h"
#include "Antenna_switch.h"

class fpga
{
	public :
		fpga();					// constructor procedure
		virtual ~fpga();
		void start(void);
		void wait_for_data_on_spi(void);
		void frame_init(void);
		void frame_decode(void);
		void frame_reply(void);

	protected :
		void command();
		void get_firmware_version();
		void start_monitoring_acq();
		void read_monitoring_acq();
        SPISlave _spi_ctrl;         // interface with SPI bus
        FRAME_STRUCTURE _frame;

		// Power Amplifier
        void access_pa_io(Pa pa);
        Pa _pa_0;
        Pa _pa_1;

		// switch rf
        void antenna_switch_reset();
        void antenna_switch_config(Antenna_switch as);
        Antenna_switch _antenna_switch_0;
        Antenna_switch _antenna_switch_1;
};


#endif /* FPGA_HPP_ */
