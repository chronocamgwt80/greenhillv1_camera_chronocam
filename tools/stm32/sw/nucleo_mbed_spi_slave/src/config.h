/*
 * config.h
 *
 *  Created on: Nov 9 2015
 *      Author: avialletelle
 */

// Main firmware defines
#define NUCLEO_FIRMWARE_VERSION                 0x02
#define NUCLEO_CMD_BASEADDR                     0x00000000
#define NUCLEO_CMD_ADDR_RANGE                   0x0000FFFF
#define NUCLEO_CMD_HIGHADDR                     NUCLEO_CMD_BASEADDR + NUCLEO_CMD_ADDR_RANGE
#define RF_CMD_BASEADDR                         0x00010000
#define RF_CMD_ADDR_RANGE                       0x0001FFFF
#define RF_CMD_HIGHADDR                         RF_CMD_BASEADDR + RF_CMD_ADDR_RANGE
#define RF0_CMD_BASEADDR                        0x00010000
#define RF0_CMD_ADDR_RANGE                      0x0000FFFF
#define RF0_CMD_HIGHADDR                        RF0_CMD_BASEADDR + RF0_CMD_ADDR_RANGE
#define RF1_CMD_BASEADDR                        0x00020000
#define RF1_CMD_ADDR_RANGE                      0x0000FFFF
#define RF1_CMD_HIGHADDR                        RF1_CMD_BASEADDR + RF1_CMD_ADDR_RANGE
#define ZYNQ_CMD_BASEADDR                       0x000100000
#define ZYNQ_CMD_ADDR_RANGE                     0x0000FFFFF
#define ZYNQ_CMD_HIGHADDR                       ZYNQ_CMD_BASEADDR + ZYNQ_CMD_ADDR_RANGE
#define BYTE_SIZE                               0x8
#define SPI_TIMEOUT_MS								100
// RF modules
#define RF_MODULE_NB                            0x2
#define RF_MODULE_ID_0                          0x0
#define RF_MODULE_ID_1                          0x1
// PA modules
#define PA_MODULE_ID_0                          0x0
#define PA_MODULE_ID_1                          0x1
#define PA_MODULE_NB                            0x2
// Antenna modules
#define ANTENNA_SWITCH_NB                       0x2
#define ANTENNA_SWITCH_PIN_NB                   0x2
#define NUCLEO_ANTENNA_SWITCH_RX_MODE           0x1
#define NUCLEO_ANTENNA_SWITCH_TX_MODE           0x2
// Monitoring
#define MONITORING_DOT_NB_MAX                   1000
#define MONITORING_NB_OUTPUT                    3
#define MONITORING_ACQ_DISABLE                  0x0
#define MONITORING_ACQ_ENABLE                   0x1
// COMMAND
#define NUCLEO_CMD_GET_FIRMWARE_VERSION         0x1
#define NUCLEO_CMD_ENABLE_MON_ACQ               0x2
#define NUCLEO_CMD_READ_MON_ACQ                 0x3
#define NUCLEO_CMD_ANTENNA_SWITCH_0_CONFIG      0x4
#define NUCLEO_CMD_ANTENNA_SWITCH_1_CONFIG      0x5
#define NUCLEO_CMD_PA0_IO                       0x6
#define NUCLEO_CMD_PA1_IO                       0x7
#define NUCLEO_CMD_OFDM_BUFFER                  0x8
