/*
 * \file Pa.h
 *
 *  Created on: Dec 10, 2015
 *      Author: avialletelle
 */

#ifndef PA_H_
#define PA_H_

/*
 * Includes
 */
#include "mbed.h"

/*
 * Defines
 */
#define PA_SLEEP               0x0
#define PA_RECEIVE_BYPASS      0x2
#define PA_RECEIVE_LNA         0x6
#define PA_TRANSMIT            0x3 // or 0x7
#define PA_ANT1_EN             0x0
#define PA_ANT2_EN             0x1

/*
 * @brief
 */
class Pa
{
public:
    Pa(PinName ialarm, PinName cps, PinName csd, PinName ctx, PinName ant_sel);
    virtual ~Pa();
    void io_reset();
    void apply_mode(uint8_t mode);
    void select_antenna(uint8_t antenna);
    uint8_t read_ialarm();
    uint8_t read_mode();
    uint8_t read_selected_antenna();
private:
    uint8_t _mode;
    DigitalIn _ialarm;
    DigitalOut _cps;
    DigitalOut _csd;
    DigitalOut _ctx;
    DigitalOut _ant_sel;
};

#endif /* PA_H_ */
