/*
 * \file Monitoring.cpp
 *
 *  Created on: Sep 24, 2015
 *      Author: avialletelle
 */

#include "Control_Monitoring.h"
extern Ticker t;
extern Control_Monitoring ctrl_mon;

/*
 * @brief
 */
Control_Monitoring::Control_Monitoring() :
    _imon_rf(IMON_RF_PIN),
    _imon_pa(IMON_PA_PIN),
    _vmon_pa(VMON_PA_PIN),
    _rf_power(POWER_ENABLE_RF_PIN),
    _pa_power(POWER_ENABLE_PA_PIN)
{
    _acq_flag = MONITORING_ACQ_DISABLE;
    _dot_index = 0;
    _dots_threshold = 0;
    this->monitoring_table_reset();
    this->power_enable(_rf_power);
    this->power_enable(_pa_power);
}

/*
 * @brief
 */
Control_Monitoring::~Control_Monitoring()
{

}

/*
 * @brief
 */
void Control_Monitoring::monitoring_table_reset()
{
    _dot_index = 0;
    for (uint32_t dot_index = 0; dot_index < MONITORING_DOT_NB_MAX; ++dot_index)
    {
        for (uint32_t output_index = 0; output_index < MONITORING_NB_OUTPUT; ++output_index)
        {
            _monitoring_table[dot_index][output_index] = 0;
        }
    }
}

/*
 * @brief
 */
void Control_Monitoring::start_acq(uint32_t dots_threshold)
{
//    this->power_enable(_rf_power);
//    this->power_enable(_pa_power);
    this->monitoring_table_reset();
    _acq_flag = MONITORING_ACQ_ENABLE;
    _dots_threshold = dots_threshold;
    t.attach_us(&ctrl_mon, &Control_Monitoring::get_adc_output, 1000);
}

/*
 * @brief
 */
void Control_Monitoring::stop_acq()
{
    t.detach();
    _acq_flag = MONITORING_ACQ_DISABLE;
//    this->power_enable(_rf_power);
//    this->power_enable(_pa_power);
}
/*
 * @brief
 */
uint8_t Control_Monitoring::get_status()
{
    return _acq_flag;
}

/*
 * @brief
 */
void Control_Monitoring::get_adc_output()
{
    if (_dot_index >= _dots_threshold)
    {
        this->stop_acq();
    } else
    {
        _monitoring_table[_dot_index][0] = _imon_rf.read_u16();
        _monitoring_table[_dot_index][1] = _imon_pa.read_u16();
        _monitoring_table[_dot_index][2] = _vmon_pa.read_u16();
        _dot_index++;
    }
}


/*
 * @brief
 */
void Control_Monitoring::power_enable(DigitalOut output)
{
    output = 1;
}

/*
 * @brief
 */
void Control_Monitoring::power_disable(DigitalOut output)
{
    output = 0;
}
