/*
 * Antenna_switch.h
 *
 *  Created on: Dec 16, 2015
 *      Author: avialletelle
 */

#ifndef ANTENNA_SWITCH_H_
#define ANTENNA_SWITCH_H_

#include "mbed.h"
#include "config.h"

class Antenna_switch
{
public:
    Antenna_switch(PinName _v1_pin, PinName _v2_pin);
    virtual ~Antenna_switch();
    void set_mode(uint8_t mode);
    uint8_t get_mode();

protected:
    DigitalOut _v1;
    DigitalOut _v2;
};

#endif /* ANTENNA_SWITCH_H_ */
