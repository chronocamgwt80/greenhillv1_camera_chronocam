/*
 * Antenna_switch.cpp
 *
 *  Created on: Dec 16, 2015
 *      Author: avialletelle
 */

#include "Antenna_switch.h"

Antenna_switch::Antenna_switch(PinName _v1_pin, PinName _v2_pin):
_v1(_v1_pin),
_v2(_v2_pin)
{


}

Antenna_switch::~Antenna_switch()
{

}


/*
 * @brief
 */
void Antenna_switch::set_mode(uint8_t mode)
{
    switch (mode)
    {
        case NUCLEO_ANTENNA_SWITCH_TX_MODE:
            _v1 = (NUCLEO_ANTENNA_SWITCH_TX_MODE & 0x1);
            _v2 = ((NUCLEO_ANTENNA_SWITCH_TX_MODE >> 1) & 0x1);
            break;
        case NUCLEO_ANTENNA_SWITCH_RX_MODE:
            _v1 = (NUCLEO_ANTENNA_SWITCH_RX_MODE & 0x1);
            _v2 = ((NUCLEO_ANTENNA_SWITCH_RX_MODE >> 1) & 0x1);
            break;
        default:
            _v1 = (NUCLEO_ANTENNA_SWITCH_TX_MODE & 0x1);
            _v2 = ((NUCLEO_ANTENNA_SWITCH_TX_MODE >> 1) & 0x1);
            break;
    }
}

/*
 * @brief
 */
uint8_t Antenna_switch::get_mode()
{
    uint8_t mode = 0;
    mode = _v1;
    mode |= (_v2 << 1);
    return mode;

}
