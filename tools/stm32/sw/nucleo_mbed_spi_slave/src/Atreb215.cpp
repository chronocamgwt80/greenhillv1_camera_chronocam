/*
 * Atreb215.cpp
 *
 *  Created on: Aug 10, 2015
 *      Author: jroudier
 */

#include "Atreb215.hpp"
#include "Atreb215_def.h"
#include "io.hpp"
#include "fpga.hpp"

extern Serial pc;
extern fpga zynq;
/**
 * generic constructor
 */
Atreb215::Atreb215(unsigned index) : _spi(AT_SPI_MOSI, AT_SPI_MISO, AT_SPI_SCLK), _cs(index ? AT1_SPI_CS : AT0_SPI_CS)
{
	spi_init(1e6, 8, 0, 0);
}

/**
 * generic destructor
 */
Atreb215::~Atreb215()
{

}

/**
 * initialization of the SPI bus :
 * - frequency
 * - number of bits
 * - polarity
 * - phase
 */
void Atreb215::spi_init(int spi_frequency, unsigned char nb_bits, bool spi_pol, bool spi_pha)
{
	_cs=1;
	// chip deselection
	_spi.format(nb_bits, (2*spi_pol)+spi_pha);
	// size and mode configuration
	_spi.frequency(spi_frequency);
	// clock frequency in Hz
}

/**
 * debug procedure, for test only
 */
void Atreb215::spi_rw(int data_write, int *ptr_data_read)
{
	_cs=0;
	// chip selection
	(*ptr_data_read)=_spi.write(data_write);
	// both read and write operations
	_cs=1;
	// chip deselection
}

/**
 * atreb215 register WRITE single access
 * - address
 * - data
 */
void Atreb215::spi_register_single_write(uint16_t addr, uint8_t data)
{
	uint8_t cmd_1, cmd_0;

	cmd_1=((addr>>8)&0x3F)|0x80;
	// 10XXXXXX : most significant command byte, write mode
	cmd_0=addr&0xFF;
	// XXXXXXXX : least significant command byte

//	wait(0.000001);
	// generic delay (1 us)
	_cs=0;
	// chip selection
	_spi.write((int)cmd_1);
	// write, most significant command byte
	_spi.write((int)cmd_0);
	// write, least significant command byte
	_spi.write((int)data);
	// write, data
//	wait(0.000001);
	// generic delay (1 us)
	_cs=1;
	// chip deselection
}

/**
 * atreb215 register READ single access
 * - address
 * - data
 */
void Atreb215::spi_register_single_read(uint16_t addr, uint8_t *ptr_data)
{
	uint8_t cmd_1, cmd_0;

	cmd_1=(addr>>8)&0x3F;
	// 00XXXXXX : most significant command byte, read mode
	cmd_0=addr&0xFF;
	// XXXXXXXX : least significant command byte

//	wait(0.000001);
	// generic delay (1 us)
	_cs=0;
	// chip selection
	_spi.write((int)cmd_1);
	// write, most significant command byte
	_spi.write((int)cmd_0);
	// write, least significant command byte
	ptr_data[0]=(uint8_t)_spi.write(0x55);
	// write a dummy byte, 0x55, data is recovered on MISO pin
//	wait(0.000001);
	// generic delay (1 us)
	_cs=1;
	// chip deselection
}

/**
 * atreb215 register WRITE burst access
 * - address
 * - data array
 * - number of bytes to be write
 */
void Atreb215::spi_register_burst_write(uint16_t addr, uint8_t *ptr_data, uint16_t nb_bytes)
{
	uint16_t cpt_bytes;
	uint8_t cmd_1, cmd_0;

	cmd_1=((addr>>8)&0x3F)|0x80;
	// 10XXXXXX : most significant command byte, write mode
	cmd_0=addr&0xFF;
	// XXXXXXXX : least significant command byte

//	wait(0.000001);
	// generic delay (1 us)
	_cs=0;
	// chip selection
	_spi.write((int)cmd_1);
	// write, most significant command byte
	_spi.write((int)cmd_0);
	// write, least significant command byte

	for(cpt_bytes=0 ; cpt_bytes<nb_bytes ; cpt_bytes++)
	{
		_spi.write((int)ptr_data[cpt_bytes]);
		// write, data
	}

//	wait(0.000001);
	// generic delay (1 us)
	_cs=1;
	// chip deselection
}

/**
 * atreb215 register READ burst access
 * - address
 * - data array
 * - number of bytes to be read
 */
void Atreb215::spi_register_burst_read(uint16_t addr, uint8_t *ptr_data, uint32_t nb_bytes)
{
	uint16_t cpt_bytes;
	uint8_t cmd_1, cmd_0;

	cmd_1=(addr>>8)&0x3F;
	// 00XXXXXX : most significant command byte, read mode
	cmd_0=addr&0xFF;
	// XXXXXXXX : least significant command byte

	_cs=0;
	// chip selection
	_spi.write((int)cmd_1);
	// write, most significant command byte
	_spi.write((int)cmd_0);
	// write, least significant command byte

	for(cpt_bytes=0 ; cpt_bytes<nb_bytes ; cpt_bytes++)
	{
		ptr_data[cpt_bytes]=(uint8_t)_spi.write(0x55);
		// write a dummy byte, 0x55, data is recovered on MISO pin
	}
	// generic delay (1 us)
	_cs=1;
	// chip deselection
}

/**
 * atreb215 reset via special register access (dedicated pin is not used to perform a reset operation)
 */
void Atreb215::at86rf215_reset_register(void)
{
	uint8_t tab[1]={0};

	spi_register_single_write(AT86RF215_RF_RST, 0x07);
	// [2:0] : CMD, writing 0x07 in this register triggers the reset procedure

	do
	{
		spi_register_single_read(AT86RF215_RF09_IRQS, tab);
	}while((tab[0]&0x01)!=0x01);
	// [5:0] : interruption request indicator (end of reset is expected)
}

/**
 * atreb215 general initialization
 * - I/Q operation mode without using the baseband cores
 * - both radio cores are put in TRXOFF state (idle)
 * - frontend configuration
 * - frequency settings for both radio cores (local oscillator, channel settings)
 */
void Atreb215::at86rf215_general_init(void)
{
	uint8_t tab[1]={0};

	spi_register_single_write(AT86RF215_RF_IQIFC1, 0x12);
	// [7] : FAILSF, indicates that the LVDS receiver is in failsafe mode, READ only
	// [6:4] : CHPM, working mode of the chip (I/Q interface only, both baseband cores are disabled)
	// [1:0] : SKEWDRV, alignement of the I/Q data interface RXD signal edges relative to the RXCLK clock edges
	spi_register_single_write(AT86RF215_RF_IQIFC0, 0x16);
	// [7] : EXTLB, enables the loopback functionality (disabled)
	// [6] : SF, indicates whether the data stream of the I/Q data interface is synchronized correctlty, READ only
	// [5:4] : DRV, configures the I/Q data interface driver output current (2 mA)
	// [3:2] : CMV, common mode voltage of the I/Q data interface signals (has no effect here, according to [1])
	// [1] : CMV1V2, common mode voltage of the I/Q data interface signals is set to 1.2 V
	// [0] : EEC, embedded control (disabled)
	spi_register_single_write(AT86RF215_RF09_CMD, 0x02);
	// [2:0] : CMD, transceiver state order (go to TRXOFF)

	do
	{
		spi_register_single_read(AT86RF215_RF09_STATE, tab);
	}while(tab[0]!=0x02);
	// [2:0] : current transceiver state (TRXOFF is expected)

	spi_register_single_write(AT86RF215_RF24_CMD, 0x02);
	// [2:0] : CMD, transceiver state order (go to TRXOFF)

	do
	{
		spi_register_single_read(AT86RF215_RF24_STATE, tab);
	}while(tab[0]!=0x02);
	// [2:0] : current transceiver state (TRXOFF is expected)

	spi_register_single_write(AT86RF215_RF09_AUXS, 0x02);
	// [7] : EXTLNABYP, AGC scheme where the external LNA can be bypassed using FEAn/FEBn (disabled)
	// [6:5] : AGCMAP, specific AGC gain mapping according to the external LNA gain (disabled)
	// [4] : AVEXT, external AVDD supply (disabled)
	// [3] : AVEN, analog voltage regulator turns on in state TRXOFF (disabled)
	// [2] : AVS, this bit indicates that the analog voltage has settled, READ only
	// [1:0] : PAVC, supply voltage of the internal power amplifier (2.4 V)
	spi_register_single_write(AT86RF215_RF09_PADFE, 0x00);
	// [7:6] : PADFE, configuration of the frontend control pins FEAn/FEBn (configuration 0)
	spi_register_single_write(AT86RF215_RF09_IRQM, 0x00);
	// [5:0] : see IRQS register, this is a mask to display the interrupts on the IRQ pin (output is disabled)
	spi_register_single_write(AT86RF215_RF09_CS, 0x08);
	// [7:0] : CS, channel spacing (0x08 * 25 kHz) (200 kHz)
	spi_register_single_write(AT86RF215_RF09_CCF0H, 0x43);
	// [7:0] : CCF0H, local oscillator frequency (433 MHz)
	spi_register_single_write(AT86RF215_RF09_CCF0L, 0xA8);
	// [7:0] : CCF0L, local oscillator frequency (433 MHz)
	spi_register_single_write(AT86RF215_RF09_CNL, 0x00);
	// [7:0] : CNL, channel index (0)
	spi_register_single_write(AT86RF215_RF09_CNM, 0x00);
	// [7:6] : CM, channel mode (IEEE compliant scheme)
	// [0] : CNH, channel index (0)

	spi_register_single_write(AT86RF215_RF24_AUXS, 0x02);
	// [7] : EXTLNABYP, AGC scheme where the external LNA can be bypassed using FEAn/FEBn (disabled)
	// [6:5] : AGCMAP, specific AGC gain mapping according to the external LNA gain (disabled)
	// [4] : AVEXT, external AVDD supply (disabled)
	// [3] : AVEN, analog voltage regulator turns on in state TRXOFF (disabled)
	// [2] : AVS, this bit indicates that the analog voltage has settled, READ only
	// [1:0] : PAVC, supply voltage of the internal power amplifier (2.4 V)
	spi_register_single_write(AT86RF215_RF24_PADFE, 0x00);
	// [7:6] : PADFE, configuration of the frontend control pins FEAn/FEBn (configuration 0)
	spi_register_single_write(AT86RF215_RF24_IRQM, 0x00);
	// [5:0] : see IRQS register, this is a mask to display the interrupts on the IRQ pin (output is disabled)
	spi_register_single_write(AT86RF215_RF24_CS, 0x08);
	// [7:0] : CS, channel spacing (0x08 * 25 kHz) (200 kHz)
	spi_register_single_write(AT86RF215_RF24_CCF0H, 0x8C);
	// [7:0] : CCF0H, local oscillator frequency (2404 MHz)
	spi_register_single_write(AT86RF215_RF24_CCF0L, 0xF8);
	// [7:0] : CCF0L, local oscillator frequency (2404 MHz)
	spi_register_single_write(AT86RF215_RF24_CNL, 0x00);
	// [7:0] : CNL, channel index (0)
	spi_register_single_write(AT86RF215_RF24_CNM, 0x00);
	// [7:6] : CM, channel mode (IEEE compliant scheme)
	// [0] : CNH, channel index (0)
}

/**
 * atreb215 sub-1 GHz radio transmitter initialization
 * - various PA settings
 * - radio core is put in TXPREP state (ready to transmit)
 * - digital link configuration
 */
void Atreb215::at86rf215_tx09_init(uint8_t *ptr_data)
{
	uint8_t tab[1]={0};

	spi_register_single_write(AT86RF215_RF09_CMD, 0x02);
	// [2:0] : CMD, transceiver state order (go to TRXOFF)

	do
	{
		spi_register_single_read(AT86RF215_RF09_STATE, tab);
	}while(tab[0]!=0x02);
	// [2:0] : current transceiver state (TRXOFF is expected)

	spi_register_single_write(AT86RF215_RF09_TXCUTC, 0x08);
	// [7:6] : PARAMP, ramp up/down time of the PA (4 us)
	// [3:0] : LPFCUT, transmitter LP filter cut-off frequency (500 kHz)
	spi_register_single_write(AT86RF215_RF09_TXDFE, 0x01);
	// [7:5] : RCUT, relative cut-off frequency
	// [4] : DM, direct modulation (disabled)
	// [3:0] : SR, sampling frequency of I/Q data stream (4000 kHz)
	spi_register_single_write(AT86RF215_RF09_PAC, 0x7F);
	// [6:5] : PACUR, PA DC current (no current reduction)
	// [4:0] : TXPWR, transmitter output power (maximum power)

	spi_register_single_write(AT86RF215_RF09_CMD, 0x03);
	// [2:0] : CMD, transceiver state order (go to TXPREP)

	do
	{
		spi_register_single_read(AT86RF215_RF09_IRQS, ptr_data);
	}while((ptr_data[0]&0x12)==0);
	// [5:0] : interruption request indicator (error or TXPREP transisition expected)
}

/**
 * atreb215 sub-1 GHz radio receiver initialization
 * - various LNA settings
 * - radio core is put in TXPREP state (ready to receive)
 * - digital link configuration
 */
void Atreb215::at86rf215_rx09_init(uint8_t *ptr_data)
{
	uint8_t tab[1]={0};

	spi_register_single_write(AT86RF215_RF09_CMD, 0x02);
	// [2:0] : CMD, transceiver state order (go to TRXOFF)

	do
	{
		spi_register_single_read(AT86RF215_RF09_STATE, tab);
	}while(tab[0]!=0x02);
	// [2:0] : current transceiver state (TRXOFF is expected)

	spi_register_single_write(AT86RF215_RF09_RXBWC, 0x0B);
	// [5] : IFI, inverted-sign IF frequency (disabled)
	// [4] : IFS, the receiver shifts the IF frequency by factor of 1.25 (disabled)
	// [3:0] : BW, receiver filter bandwidth (fbw = 2000 kHz and fif = 2000 kHz)
	spi_register_single_write(AT86RF215_RF09_RXDFE, 0x01);
	// [7:5] : RCUT, relative cut-off frequency
	// [3:0] : SR, sampling frequency of I/Q data stream (4000 kHz)
	spi_register_single_read(AT86RF215_RF09_AGCC, tab);
	spi_register_single_write(AT86RF215_RF09_AGCC, (tab[0]&0x80)|0x01);
	// [6] : AGCI, input signal of the AGC, if zero, the filtered frontend signal is used
	// [5:4] : AVGS, time of averaging RX data samples of the AGC values (8 samples)
	// [3] : RST, resets the AGC and sets the maximum receiver gain (disabled)
	// [2] : FRZS, indicates that the AGC is on hold, READ only
	// [1] : FRZC, forces the AGC to freeze to its current value (disabled)
	// [0] : EN, enables the AGC (enabled)
	spi_register_single_write(AT86RF215_RF09_AGCS, 0x77);
	// [7:5] : TGT, AGC target level relative to ADC full scale (-30 dB)
	// [4:0] : GCW, receiver gain setting (maximum gain)

	spi_register_single_write(AT86RF215_RF09_CMD, 0x03);
	// [2:0] : CMD, transceiver state order (go to TXPREP)

	do
	{
		spi_register_single_read(AT86RF215_RF09_IRQS, ptr_data);
	}while((ptr_data[0]&0x12)==0);
	// [5:0] : interruption request indicator (error or TXPREP transisition expected)
}

void Atreb215::spi_access(FRAME_STRUCTURE *spi_packet)
{
	if(spi_packet->rd_wrb)			// read access
	{
		spi_register_burst_read((uint16_t)spi_packet->address, spi_packet->data, spi_packet->nbbytes);
//		zynq.frame_reply();
	}
	else			// write access
	{
		spi_register_burst_write((uint16_t)spi_packet->address, spi_packet->data, spi_packet->nbbytes);
	}
}
