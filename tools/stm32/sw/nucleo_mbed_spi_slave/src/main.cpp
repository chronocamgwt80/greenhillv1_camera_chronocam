/*
 * mbed Application program / Frequency Counter with GPS 1PPS Compensation
 *
 * Copyright (c) 2014 Kenji Arai / JH1PJL
 *  http://www.page.sannet.ne.jp/kenjia/index.html
 *  http://mbed.org/users/kenjiArai/
 *      Created: Nobember   2nd, 2014
 *      Revised: Nobember   2nd, 2014
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "mbed.h"
#include "Atreb215.hpp"
#include "io.hpp"
#include "fpga.hpp"
#include "Control_Monitoring.h"

//  Object ----------------------------------------------------------------------------------------
Timer myTimer;
Serial pc(USBTX, USBRX);
Atreb215 at0(AT0);
Atreb215 at1(AT1);
fpga zynq;
Ticker t;
Control_Monitoring ctrl_mon;
int main(void)
{
//    t.attach_us(&ctrl_mon, &Control_Monitoring::get_adc_output, 1000);
	myTimer.reset();
	pc.baud(115200);
	pc.printf("Init Done\n\r");

	while(1)
	{
		zynq.start();
	}


}
