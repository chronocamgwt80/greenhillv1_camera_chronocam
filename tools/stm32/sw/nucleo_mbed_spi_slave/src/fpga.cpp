/*
 * fpga.cpp
 *
 *  Created on: Sep 28, 2015
 *      Author: cleblanc
 */


#include "fpga.hpp"
#include "Atreb215.hpp"

extern Serial pc;
extern Atreb215 at0;
extern Atreb215 at1;
extern Control_Monitoring ctrl_mon;
extern Timer myTimer;

/*
 * @brief
 */
fpga::fpga() :
_spi_ctrl(ZYNQ_CONFIG_SPI_MOSI, ZYNQ_CONFIG_SPI_MISO, ZYNQ_CONFIG_SPI_SCLK, ZYNQ_CONFIG_SPI_NSS),
_pa_0(PA0_IALARM, PA0_CPS, PA0_CSD, PA0_CTX, PA0_ANT_SEL),
_pa_1(PA1_IALARM, PA1_CPS, PA1_CSD, PA1_CTX, PA1_ANT_SEL),
_antenna_switch_0(SW1_V1, SW1_V2),
_antenna_switch_1(SW2_V1, SW2_V2)
{
    //	_spi_ctrl.frequency(1e6);
    _spi_ctrl.format(8,0);
    // Fill the transmission buffer with the value to be written out
    // as slave on the next received message from the master.
    // This bit will be discarded by the master (Zynq code)
    _spi_ctrl.reply(0x55);
    this->frame_init();
    this->antenna_switch_reset();
}

fpga::~fpga()
{
}

/*
 * @brief
 */
void fpga::start()
{
    while(1)
    {
//    	_userDbgIO = 0x1;
        this->wait_for_data_on_spi();
        if (this->_frame.to_decode)
        {
            this->frame_decode();
            this->frame_init();
        }
//        _userDbgIO = 0x0;
    }
}

/*
 * @brief
 */
void fpga::frame_init()
{
    this->_frame.address = 0x0;
    this->_frame.nbbytes = 0x0;
    this->_frame.rd_wrb = 0x0;
    this->_frame.index = 0x0;
    this->_frame.full = 0x0;
    this->_frame.to_decode = 0x0;
	myTimer.stop();
	myTimer.reset();
}

/*
 * @brief
 */
void fpga::wait_for_data_on_spi(void)
{
    uint8_t din;

	// waiting for master spi byte transfer
	while(_spi_ctrl.receive() == 0)
	{
		if ((myTimer.read_ms() > SPI_TIMEOUT_MS) && ((_frame.address & 0x7FFFFFFF) > NUCLEO_CMD_HIGHADDR))
		{
            this->frame_init();
		}
	}

	if(_frame.index == 0)
	{
		myTimer.start();
	}
	din = _spi_ctrl.read();


	_spi_ctrl.reply((int) din);

	// update current frame command
    *(((uint8_t*)&_frame) + _frame.index) = din;
	_frame.index++;

    // update rd_wrb flag
    if(_frame.index == FRAME_HEADER_ADDRESS_SIZE)
    {
        _frame.rd_wrb = (_frame.address & FRAME_RD_WRB_BIT_MASK) >> (BYTE_SIZE * FRAME_HEADER_ADDRESS_SIZE - 1);
    }

    if (_frame.rd_wrb != 1)
    {
    	// set flag to indicate that command has to be decoded, write command = wait for all data before command decoding
        if (_frame.index == (_frame.nbbytes + FRAME_OVERHEAD_SIZE))
		{
			// wr command to decode
			_frame.to_decode = 1;
			// discard last byte
			while(_spi_ctrl.receive() == 0);
			_spi_ctrl.read();
		}
    } else
    {
        // when read command is received, the reply will be proceeded when the command will be decoded

    	// set flag to indicate that command has to be decoded, read command = do not wait for data before command decoding
        if (_frame.index == FRAME_HEADER_SIZE)
        {
            _frame.to_decode = 1;
        }
    }
}

/*
 * @brief
 */
void fpga::frame_decode(void)
{


    uint32_t frame_address = (_frame.address & 0x7FFFFFFF);
    if (frame_address <= NUCLEO_CMD_HIGHADDR)
    {
        this->command();
        // frame for the nucleo
        // todo should be called by a function
        //	    pc.printf("adc at0_imon = %f V\n\r",ctrl.read_AT0_imon());
        //        _frame.nbbytes = 0;

    } else if( (RF0_CMD_BASEADDR <= frame_address) &&  (frame_address <= RF0_CMD_HIGHADDR) )
    {
        // frame for the RF module
        at0.spi_access(&_frame);

    } else if( (RF1_CMD_BASEADDR <= frame_address) &&  (frame_address <= RF1_CMD_HIGHADDR) )
    {
        // frame for the RF module
        at1.spi_access(&_frame);

    }  else
    {
        // frame for anyone, will be discarded
    }

    if (_frame.rd_wrb == 1)
    {
        this->frame_reply();
    }
}

/*
 * @brief
 */
void fpga::frame_reply(void)
{
    for (unsigned k=0;k<_frame.nbbytes;k++)
    {
    	while(_spi_ctrl.receive() == 0);
    	_spi_ctrl.read();
        _spi_ctrl.reply(_frame.data[k]);
    }

    // fill with checksum and stop byte
    while(_spi_ctrl.receive() == 0);
	_spi_ctrl.read();
    _spi_ctrl.reply((int) FRAME_DEFAULT_CHECKSUM);
    while(_spi_ctrl.receive() == 0);
	_spi_ctrl.read();
    _spi_ctrl.reply((int) FRAME_STOP_BYTE);


    while(_spi_ctrl.receive() == 0);
    // discard last byte
    _spi_ctrl.read();

	// Fill the transmission buffer with the value to be written out
	// as slave on the next received message from the master.
	// This bit will be discarded by the master (Zynq code)
	_spi_ctrl.reply(0x55);

}

/*
 * @brief Apply Nucleo Command
 */
void fpga::command()
{
    uint32_t frame_address = (_frame.address & 0x7FFFFFFF);
    switch (frame_address)
    {
        case NUCLEO_CMD_GET_FIRMWARE_VERSION:
            get_firmware_version();
            break;
        case NUCLEO_CMD_ENABLE_MON_ACQ:
            this->start_monitoring_acq();
            break;
        case NUCLEO_CMD_READ_MON_ACQ:
            this->read_monitoring_acq();
            break;
        case NUCLEO_CMD_PA0_IO:
            this->access_pa_io(_pa_0);
            break;
        case NUCLEO_CMD_PA1_IO:
            this->access_pa_io(_pa_1);
            break;
        case NUCLEO_CMD_ANTENNA_SWITCH_0_CONFIG:
            this->antenna_switch_config(_antenna_switch_0);
            break;
        case NUCLEO_CMD_ANTENNA_SWITCH_1_CONFIG:
            this->antenna_switch_config(_antenna_switch_1);
            break;
        default:
            // todo
            break;
    }
}

/*
 * @brief Send to Slave spi master its own firmware version
 */
void fpga::get_firmware_version()
{
    if (_frame.rd_wrb)
    {
        _frame.data[0] = ((NUCLEO_FIRMWARE_VERSION >> (0*BYTE_SIZE)) & 0xFF);
        _frame.data[1] = ((NUCLEO_FIRMWARE_VERSION >> (1*BYTE_SIZE)) & 0xFF);
        _frame.data[2] = ((NUCLEO_FIRMWARE_VERSION >> (2*BYTE_SIZE)) & 0xFF);
        _frame.data[3] = ((NUCLEO_FIRMWARE_VERSION >> (3*BYTE_SIZE)) & 0xFF);

    } else
    {
        // write command
        // command is read only
    }
}

/*
 * @brief
 */
void fpga::antenna_switch_reset()
{
    _antenna_switch_0.set_mode(NUCLEO_ANTENNA_SWITCH_TX_MODE);
    _antenna_switch_1.set_mode(NUCLEO_ANTENNA_SWITCH_TX_MODE);
}
/*
 * @brief
 * Bit -   GPIO
 * ---------------
 * [0] - RF SW V1
 * [1] - RF SW V2
 */
void fpga::antenna_switch_config(Antenna_switch as)
{
    switch (_frame.rd_wrb)
    {
        case READ_CMD:
            _frame.data[0] = as.get_mode() & 0x3;
            break;

        case WR_CMD:
        	as.set_mode(_frame.data[0] & 0x3);
            break;

        default:
            // this case should never happened
            //            this->antenna_switch_reset();
            break;
    }
}

/*
 * @brief
 * Bit -   IO
 * ---------------
 * [0]     - ANT_SEL (R/W)
 * [1 . 3] - MODE    (R/W)
 * [4]     - IALARM  (read only)
 * ---------------
 * PA_SLEEP = 0x0
 * PA_RECEIVE_BYPASS = 0x2
 * PA_RECEIVE_LNA = 0x6
 * PA_TRANSMIT = 0x3 or 0x7
 * ---------------
 * PA_ANT1_EN = 0x0
 * PA_ANT2_EN = 0x1
 */
void fpga::access_pa_io(Pa pa)
{
    if (_frame.rd_wrb)
    {
        // read command
        uint8_t config = 0;
        config = (pa.read_selected_antenna() & 0x1)
                                                                        |((pa.read_mode() & 0x7) << 1)
                                                                        |((pa.read_ialarm() & 0x1) << 4);
        _frame.data[0] = (config & 0xFF);
    } else
    {
        // write command
        uint8_t ant_sel = (_frame.data[0] & 0x1);
        uint8_t mode = ((_frame.data[0] >> 1) & 0x7);
        pa.select_antenna(ant_sel);
        pa.apply_mode(mode);
    }
}

/*
 * @brief
 */
void fpga::start_monitoring_acq()
{
    uint32_t dots_threshold = 0;

    switch (_frame.rd_wrb)
    {
        case READ_CMD:
            _frame.data[0] = ctrl_mon._acq_flag;
            _frame.data[1] = ctrl_mon._dot_index;
            _frame.data[2] = (ctrl_mon._dot_index >> 8);
            _frame.data[3] = (ctrl_mon._dot_index >> 16);
            _frame.data[4] = (ctrl_mon._dot_index >> 24);
            break;

        case WR_CMD:
            dots_threshold = (_frame.data[0]) | (_frame.data[1] << 8) | (_frame.data[2] << 16) | (_frame.data[3] << 24);
            ctrl_mon.start_acq(dots_threshold);
            break;
        default:
            // this case should never happened
            break;
    }
}

/*
 * @brief
 */
void fpga::read_monitoring_acq()
{
    switch (_frame.rd_wrb)
    {
        case READ_CMD:
            for (uint32_t index = 0; index < ctrl_mon._dot_index; ++index)
            {
                memcpy( &_frame.data[index * 6], ctrl_mon._monitoring_table[index], sizeof(uint16_t) * MONITORING_NB_OUTPUT);
            }
            break;

        case WR_CMD:
            // command is read only
            break;
        default:
            // this case should never happened
            break;
    }
}
