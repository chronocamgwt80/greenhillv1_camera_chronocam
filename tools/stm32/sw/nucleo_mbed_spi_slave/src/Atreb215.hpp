/*
 * Atreb215.hpp
 *
 *  Created on: Aug 10, 2015
 *      Author: jroudier
 */

#ifndef ATREB215_HPP_
#define ATREB215_HPP_

#include "mbed.h"
#include "fpga.hpp"

#define AT0	0
#define AT1 1

class Atreb215
{
	public :
		Atreb215(unsigned index = AT0);					// constructor procedure
		virtual ~Atreb215();			// destructor procedure
		void spi_init(int spi_frequency, unsigned char nb_bits, bool spi_pol, bool spi_pha);		// spi bus initialization
		void spi_rw(int data_write, int *ptr_data_read);				// write and meanwhile read data with the spi bus
		void spi_register_single_write(uint16_t addr, uint8_t data);	// write, single access
		void spi_register_single_read(uint16_t addr, uint8_t *ptr_data);	// read, single access
		void spi_register_burst_write(uint16_t addr, uint8_t *ptr_data, uint16_t nb_bytes);	// write, burst access
		void spi_register_burst_read(uint16_t addr, uint8_t *ptr_data, uint32_t nb_bytes);	// read, burst access
		void spi_access(FRAME_STRUCTURE *spi_packet);
		void at86rf215_reset_register(void);				// chip reset using the reset register
		void at86rf215_general_init(void);					// chip initialization
		void at86rf215_tx09_init(uint8_t *ptr_data);		// transmitter initialization
		void at86rf215_rx09_init(uint8_t *ptr_data);		// receiver initialization

	protected :
		SPI _spi;			// interface with SPI bus
		DigitalOut _cs;		// chip select signal
};

#endif /* ATREB215_HPP_ */
