/*
 * \file Pa.cpp
 *
 *  Created on: Dec 10, 2015
 *      Author: avialletelle
 */

#include "Pa.h"

Pa::Pa(PinName ialarm, PinName cps, PinName csd, PinName ctx, PinName ant_sel):
 _ialarm(ialarm),
 _cps(cps),
 _csd(csd),
 _ctx(ctx),
 _ant_sel(ant_sel)
{
    _mode = PA_SLEEP;
    this->apply_mode(_mode);
}

Pa::~Pa()
{

}

/*
 * @brief
 */
void Pa::io_reset()
{
    this->apply_mode(PA_SLEEP);
}

/*
 * @brief
 */
void Pa::apply_mode(uint8_t mode)
{
    _mode = mode;
    // see Table 9. SKY65362-11 Mode Control Logic (Note 1)
    switch (mode)
    {
        case PA_SLEEP:
            _ctx = 0x0;
            _csd = 0x0;
            _cps = 0x0;
            break;
        case PA_RECEIVE_BYPASS:
            _ctx = 0x0;
            _csd = 0x1;
            _cps = 0x0;
            break;
        case PA_RECEIVE_LNA:
            _ctx = 0x0;
            _csd = 0x1;
            _cps = 0x1;
            break;
        case PA_TRANSMIT:
            _ctx = 0x1;
            _csd = 0x1;
//            _cps = 0x0; // don't care
            break;
        default:
            _ctx = 0x0;
            _csd = 0x0;
            _cps = 0x0;
            break;
    }
}

/*
 * @brief
 */
void Pa::select_antenna(uint8_t antenna)
{
    _ant_sel = antenna;
}

/*
 * @brief
 */
uint8_t Pa::read_ialarm()
{
    return _ialarm.read();
}

/*
 * @brief
 */
uint8_t Pa::read_mode()
{
    _mode = ((_cps.read() & 0x1) << 2) | ((_csd.read() & 0x1) << 1) | ((_ctx.read() & 0x1) << 0);
    return _mode;
}

/*
 * @brief
 */
uint8_t Pa::read_selected_antenna()
{
    return _ant_sel.read();
}
