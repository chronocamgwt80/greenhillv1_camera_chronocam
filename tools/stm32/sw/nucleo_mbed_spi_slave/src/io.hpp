/*
 * io.hpp
 *
 *  Created on: Sep 25, 2015
 *      Author: cleblanc
 */

#ifndef IO_HPP_
#define IO_HPP_

// data SPI
//#define ZYNQ_DATA_SPI_SCLK		    PA_5
//#define ZYNQ_DATA_SPI_MOSI 		    PA_7
//#define ZYNQ_DATA_SPI_MISO 		    PA_6
//#define ZYNQ_DATA_SPI_CS 		        PD_2
// config SPI
#define ZYNQ_CONFIG_SPI_SCLK        PA_5
#define ZYNQ_CONFIG_SPI_MOSI        PA_7
#define ZYNQ_CONFIG_SPI_MISO        PA_6
//#define ZYNQ_CONFIG_SPI_CS 		PA_4
#define ZYNQ_CONFIG_SPI_NSS         PA_4
#define AT_SPI_SCLK 			    PB_13
#define AT_SPI_MOSI 			    PB_15
#define AT_SPI_MISO 			    PB_14
#define AT0_SPI_CS 				    PB_6
#define AT1_SPI_CS 				    PC_7
// current monitoring
#define IMON_RF_PIN			        PC_0
#define IMON_PA_PIN			        PC_1
#define VMON_PA_PIN			        PC_4
// power enable
#define POWER_ENABLE_RF_PIN	        PB_7
#define POWER_ENABLE_PA_PIN	        PC_14
// switch_antenna_pin
#define SW1_V1                      PC_9
#define SW1_V2                      PB_9
#define SW2_V1                      PC_8
#define SW2_V2                      PC_6
// PA
#define PA1_IALARM                  PA_8
#define PA1_CPS                     PB_5
#define PA1_CSD                     PB_4
#define PA1_CTX                     PB_10
#define PA1_ANT_SEL                 PB_3
#define PA0_IALARM                  PB_12
#define PA0_CPS                     PB_8
#define PA0_CSD                     PC_5
#define PA0_CTX                     PB_1
#define PA0_ANT_SEL                 PA_10

#endif /* IO_HPP_ */
